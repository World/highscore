// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.Atari2600.Server : PlatformServer {
    public async Hs.Atari2600Controller get_controller (uint player) throws Error {
        var atari_core = core as Hs.Atari2600Core;
        return atari_core.get_controller (player);
    }

    public async Hs.Atari2600Difficulty get_default_difficulty (uint player) throws Error {
        var atari_core = core as Hs.Atari2600Core;
        return atari_core.get_default_difficulty (player);
    }

    public async void flick_select () throws Error {
        runner.schedule_adjust_input_state (state => {
            state->atari_2600.select_switch = true;
        });
    }
}
