// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.PlayStation.Server : PlatformServer {
    public signal void dualshock_mode_changed (uint player);

    construct {
        var psx_core = core as Hs.PlayStationCore;
        psx_core.dualshock_mode_changed.connect (player => {
            dualshock_mode_changed (player);
        });
    }

    public async uint get_players () throws Error {
        var psx_core = core as Hs.PlayStationCore;
        return psx_core.get_players ();
    }

    public async Hs.PlayStationDualShockMode get_dualshock_mode (uint player) throws Error {
        var psx_core = core as Hs.PlayStationCore;
        return psx_core.get_dualshock_mode (player);
    }

    public async bool set_dualshock_mode (uint player, Hs.PlayStationDualShockMode mode) throws Error {
        var psx_core = core as Hs.PlayStationCore;
        return psx_core.set_dualshock_mode (player, mode);
    }

    public async void set_bios_path (Hs.PlayStationBios bios, string path) throws Error {
        var psx_core = core as Hs.PlayStationCore;
        psx_core.set_bios_path (bios, path);
    }

    public async Hs.PlayStationBios get_used_bios () throws Error {
        var psx_core = core as Hs.PlayStationCore;
        return psx_core.get_used_bios ();
    }
}
