// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.PcEngineCd.Server : PlatformServer {
    public async void set_bios_path (string path) throws Error {
        var pce_cd_core = core as Hs.PcEngineCdCore;
        pce_cd_core.set_bios_path (path);
    }
}
