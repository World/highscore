// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.SegaSaturn.Server : PlatformServer {
    public async uint get_players () throws Error {
        var saturn_core = core as Hs.SegaSaturnCore;
        return saturn_core.get_players ();
    }

    public async void set_controller (uint player, Hs.SegaSaturnController controller) throws Error {
        var saturn_core = core as Hs.SegaSaturnCore;
        saturn_core.set_controller (player, controller);
    }

    public async void set_bios_path (Hs.SegaSaturnBios bios, string path) throws Error {
        var saturn_core = core as Hs.SegaSaturnCore;
        saturn_core.set_bios_path (bios, path);
    }

    public async Hs.SegaSaturnBios get_used_bios () throws Error {
        var saturn_core = core as Hs.SegaSaturnCore;
        return saturn_core.get_used_bios ();
    }
}
