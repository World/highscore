// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.PcEngine.Server : PlatformServer {
    public async uint get_players () throws Error {
        var pce_core = core as Hs.PcEngineCore;
        return pce_core.get_players ();
    }
}
