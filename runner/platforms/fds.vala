// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.Fds.Server : PlatformServer {
    public async void set_bios_path (string path) throws Error {
        var fds_core = core as Hs.FdsCore;
        fds_core.set_bios_path (path);
    }

    public async uint get_n_sides () throws Error {
        var fds_core = core as Hs.FdsCore;
        return fds_core.get_n_sides ();
    }

    public async uint get_side () throws Error {
        var fds_core = core as Hs.FdsCore;
        return fds_core.get_side ();
    }

    public async void set_side (uint side) throws Error {
        var fds_core = core as Hs.FdsCore;
        fds_core.set_side (side);
    }
}
