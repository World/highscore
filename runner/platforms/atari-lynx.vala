// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.AtariLynx.Server : PlatformServer {
    public async void set_bios_path (string path) throws Error {
        var lynx_core = core as Hs.AtariLynxCore;
        lynx_core.set_bios_path (path);
    }
}
