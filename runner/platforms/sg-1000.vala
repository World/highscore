// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.Sg1000.Server : PlatformServer {
    public async uint get_players () throws Error {
        var sms_core = core as Hs.MasterSystemCore;
        return sms_core.get_players ();
    }
}
