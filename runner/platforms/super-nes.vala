// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.SuperNes.Server : PlatformServer {
    public async uint get_players () throws Error {
        var snes_core = core as Hs.SuperNesCore;
        return snes_core.get_players ();
    }
}
