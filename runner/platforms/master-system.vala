// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.MasterSystem.Server : PlatformServer {
    public async uint get_players () throws Error {
        var sms_core = core as Hs.MasterSystemCore;
        return sms_core.get_players ();
    }

    public async void set_enable_fm_audio (bool enable_fm_audio) throws Error {
        var sms_core = core as Hs.MasterSystemCore;
        sms_core.set_enable_fm_audio (enable_fm_audio);
    }

    public async void set_enable_light_phaser (bool enable_light_phaser) throws Error {
        var sms_core = core as Hs.MasterSystemCore;
        sms_core.set_enable_light_phaser (enable_light_phaser);
    }
}
