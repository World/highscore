// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.Nintendo64.Server : PlatformServer {
    public async uint get_players () throws Error {
        var n64_core = core as Hs.Nintendo64Core;
        return n64_core.get_players ();
    }

    public async void set_controller (uint player, bool present, Hs.Nintendo64Pak pak) throws Error {
        var n64_core = core as Hs.Nintendo64Core;
        n64_core.set_controller (player, present, pak);
    }
}
