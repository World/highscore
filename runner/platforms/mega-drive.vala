// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.MegaDrive.Server : PlatformServer {
    public async uint get_players () throws Error {
        var md_core = core as Hs.MegaDriveCore;
        return md_core.get_players ();
    }
}
