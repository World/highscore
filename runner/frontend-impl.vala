// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.FrontendImpl : Object, Hs.Frontend {
    private weak Runner runner;

    public FrontendImpl (Runner runner) {
        this.runner = runner;
    }

    public void play_samples (int16[] samples) {
        runner.play_samples (samples);
    }

    public void rumble (uint player, double strong_magnitude, double weak_magnitude, uint16 milliseconds) {
        runner.rumble (player, strong_magnitude, weak_magnitude, milliseconds);
    }

    public Hs.SoftwareContext create_software_context (uint width, uint height, Hs.PixelFormat format) {
        var renderer = new SoftwareRenderer (
            runner.video_buffer, width, height, format
        );

        runner.set_renderer (renderer);
        runner.set_dmabuf (null);

        return renderer;
    }

    public Hs.GLContext create_gl_context (
        Hs.GLProfile profile,
        int major_version,
        int minor_version,
        Hs.GLFlags flags
    ) {
        var renderer = new OpenGLRenderer (
            runner.video_buffer, profile, major_version, minor_version, flags
        );

        renderer.set_dmabuf.connect (dmabuf => {
            runner.set_dmabuf (dmabuf);
        });

        runner.set_renderer (renderer);
        return renderer;
    }

    public string get_cache_path () {
        return Path.build_filename (
            Environment.get_user_cache_dir (),
            "highscore",
            runner.core.name,
            null
        );
    }

    public void log (Hs.LogLevel level, string message) {
        LogLevelFlags flags;

        switch (level) {
            case DEBUG:
                flags = LEVEL_DEBUG;
                break;
            case INFO:
                flags = LEVEL_INFO;
                break;
            case MESSAGE:
                flags = LEVEL_MESSAGE;
                break;
            case WARNING:
                flags = LEVEL_WARNING;
                break;
            case CRITICAL:
                flags = LEVEL_CRITICAL;
                break;
            default:
                assert_not_reached ();
        }

        log_structured (runner.core.name, flags, "MESSAGE", message);
    }
}
