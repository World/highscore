// This file is part of Highscore. License: GPL-3.0-or-later

public interface Highscore.Renderer : Object {
    public abstract void start_frame ();
    public abstract bool end_frame ();
}
