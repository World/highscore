// This file is part of Highscore. License: GPL-3.0-or-later

#include <glib.h>

#include <signal.h>

#ifdef __linux__
#include <sys/prctl.h>
#endif

#ifdef __FreeBSD__
#include <sys/procctl.h>
#endif

void
highscore_process_utils_set_parent_death_signal (int signal)
{
#ifdef __linux__
  prctl (PR_SET_PDEATHSIG, signal);
#elif defined(__FreeBSD__)
  procctl (P_PID, 0, PROC_PDEATHSIG_CTL, &(int){ signal });
#else
#error "Please submit a patch to support parent-death signal on your OS"
#endif
}

static void
print_backtrace_on_crash_cb (int        sig,
                             siginfo_t *si,
                             void      *unused)
{
  switch (sig) {
  case SIGABRT:
    g_critical ("Received signal SIGABRT, abnormal termination:");
    break;
  case SIGILL:
    g_critical ("Received signal SIGILL, illegal instruction:");
    break;
  case SIGSEGV:
    g_critical ("Received signal SIGSEGV, segmentation fault:");
    break;
  default:
    g_critical ("Received unexpected signal %d:", sig);
    break;
  }

  g_on_error_stack_trace ("highscore-runner");

  exit (EXIT_FAILURE);
}

void
highscore_process_utils_enable_print_backtrace_on_crash (void)
{
  struct sigaction sa;

  sa.sa_flags = SA_SIGINFO;
  sigemptyset (&sa.sa_mask);
  sa.sa_sigaction = print_backtrace_on_crash_cb;

  if (G_UNLIKELY (sigaction (SIGILL, &sa, NULL) == -1))
    g_critical ("Couldn't set a SIGILL handler.");

  if (G_UNLIKELY (sigaction (SIGSEGV, &sa, NULL) == -1))
    g_critical ("Couldn't set a SIGSEGV handler.");

  if (G_UNLIKELY (sigaction (SIGABRT, &sa, NULL) == -1))
    g_critical ("Couldn't set a SIGABRT handler.");
}
