// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Runner : Object {
    public delegate void AdjustInputStateFunc (Hs.InputState* state);

    public signal void redraw ();
    public signal void set_dmabuf (Dmabuf? dmabuf);
    public signal void rumble (uint player, double strong_magnitude, double weak_magnitude, uint16 milliseconds);

    public Hs.Core core { get; construct; }
    public SharedVideoBuffer video_buffer { get; construct; }
    public SharedInputBuffer input_buffer { get; construct; }
    public string game_title { get; construct; }

    public bool running { get; private set; }
    public uint current_media { get; set; }

    public bool flipped { get; private set; }

    private uint main_loop;

    private Renderer? renderer;

    private PaPlayer player;
    private uint32 sample_rate;
    private uint8 channels;

    private FrontendImpl frontend;

    private List<void*> scheduled_adjusts;

    public Runner (
        Hs.Core core,
        SharedVideoBuffer video_buffer,
        SharedInputBuffer input_buffer,
        string game_title
    ) {
        Object (
            core: core,
            video_buffer: video_buffer,
            input_buffer: input_buffer,
            game_title: game_title
        );
    }

    construct {
        core.bind_property (
            "current-media", this, "current-media", SYNC_CREATE | BIDIRECTIONAL
        );

        frontend = new FrontendImpl (this);
        core.frontend = frontend;

        player = new PaPlayer (game_title);

        scheduled_adjusts = new List<void*> ();
    }

    ~Runner () {
        stop ();
    }

    public void load_rom (string[] rom_paths, string save_path) throws Error {
        assert (!running);

        core.load_rom (rom_paths, save_path);
    }

    public void start () {
        assert (!running);

        core.start ();

        player.start ();

        channels = (uint8) core.get_channels ();
        sample_rate = (uint32) core.get_sample_rate ();
        player.setup (channels, sample_rate);

        var source = new FrameSource (core.get_frame_rate ());
        source.set_callback (run_frame);

        main_loop = source.attach ();

        running = true;
    }

    public void stop () {
        if (!running)
            return;

        core.stop ();

        if (main_loop > 0) {
            Source.remove (main_loop);
            main_loop = 0;
        }

        player.stop ();

        running = false;
    }

    public void reset (bool hard) {
        core.reset (hard);
    }

    private bool run_frame () {
        if (main_loop == 0)
            return false;

        var state = input_buffer.copy_input_state ();

        foreach (var data in scheduled_adjusts) {
            var func = (AdjustInputStateFunc) data;
            func (&state);
        }

        scheduled_adjusts = new List<void*> ();

        if (renderer != null)
            renderer.start_frame ();

        core.poll_input (state);
        core.run_frame ();

        if (renderer != null && renderer.end_frame ()) {
            video_buffer.lock ();
            video_buffer.set_aspect_ratio (core.get_aspect_ratio ());
            video_buffer.unlock ();

            redraw ();
        }

        uint8 channels = (uint8) core.get_channels ();
        uint32 sample_rate = (uint32) core.get_sample_rate ();

        if (channels != this.channels || sample_rate != this.sample_rate) {
            this.channels = channels;
            this.sample_rate = sample_rate;
            player.setup (channels, sample_rate);
        }

        return true;
    }

    public void pause () {
        if (!running || main_loop == 0)
            return;

        Source.remove (main_loop);
        main_loop = 0;

        core.pause ();

        player.paused = true;
    }

    public void resume () {
        if (!running || main_loop > 0)
            return;

        var source = new FrameSource (core.get_frame_rate ());
        source.set_callback (run_frame);

        main_loop = source.attach ();

        player.paused = false;

        core.resume ();
    }

    public File get_save_dir () {
        var data_dir = Environment.get_user_data_dir ();
        var save_dir_path = Path.build_filename (data_dir, "highscore", "saves");

        return File.new_for_path (save_dir_path);
    }

    public void reload_save (string path) throws Error {
        core.reload_save (path);
    }

    public void sync_save () throws Error {
        core.sync_save ();
    }

    public async void save_state (string path) throws Error {
        yield core.save_state (path, null);
    }

    public async void load_state (string path) throws Error {
        yield core.load_state (path, null);
    }

    public Hs.Region get_region () {
        return core.get_region ();
    }

    public void schedule_adjust_input_state (AdjustInputStateFunc func) {
        scheduled_adjusts.append ((void*) func);
    }

    public void play_samples (int16[] samples) {
        player.add_samples (samples);
    }

    public void set_renderer (Renderer renderer) {
        this.renderer = renderer;
    }
}
