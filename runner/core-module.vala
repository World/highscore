// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.CoreModule : TypeModule {
    private string module_path;
    private string name;

    private Module module;
    private bool loaded;

    private bool openmp_policy_hack;

    private Type type;

    private delegate Type GetCoreTypeFunc ();

    public CoreModule (string core_filename) throws CoreModuleError {
        assert (Module.supported ());

        try {
            var keyfile_path = Path.build_filename (
                Config.CORES_DIR,
                @"$core_filename.highscore"
            );

            var keyfile = new KeyFile ();
            keyfile.load_from_file (keyfile_path, KeyFileFlags.NONE);
            var module_name = keyfile.get_string ("Highscore", "Module");
            module_path = Path.build_filename (Config.CORES_DIR, module_name);
            name = keyfile.get_string ("Highscore", "Name");

            if (keyfile.has_group ("Hacks")) {
                openmp_policy_hack = keyfile.get_boolean ("Hacks", "OpenMPPolicyHack");
            }
        }
        catch (Error e) {
            throw new CoreModuleError.INVALID_CORE_DESCRIPTOR ("Invalid core descriptor: %s", e.message);
        }

        loaded = false;
    }

    public Hs.Core get_core (Hs.Platform platform) throws CoreModuleError {
        if (!loaded) {
            // See https://github.com/bsnes-emu/bsnes/issues/254
            if (openmp_policy_hack)
                Environment.set_variable ("OMP_WAIT_POLICY", "passive", true);

            if (!load ())
                throw new CoreModuleError.NOT_A_CORE ("Failed to load the core module: %s", Module.error ());
        }

        var object = Object.new (
            type,
            name: name,
            platform: platform
        );

        if (!(object is Hs.Core))
            throw new CoreModuleError.NOT_A_CORE ("Couldn’t create a new instance of core in “%s”", module_path);

        return object as Hs.Core;
    }

    public override bool load () {
        if (loaded)
            return true;

#if VALA_0_58
        try {
            module = new Module (module_path, ModuleFlags.LAZY);
        } catch (ModuleError e) {
            return false;
        }
#else
        module = Module.open (module_path, ModuleFlags.LAZY);
        if (module == null)
            return false;
#endif

        loaded = true;

        void* function;
        module.symbol ("hs_get_core_type", out function);
        if (function == null)
            return false;

        unowned GetCoreTypeFunc get_core_type = (GetCoreTypeFunc) function;

        type = get_core_type ();

        return true;
    }
}

private errordomain Highscore.CoreModuleError {
    INVALID_CORE_DESCRIPTOR,
    NOT_A_CORE,
}
