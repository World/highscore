// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public class Highscore.RunnerServer : Object {
    public signal void redraw ();
    public signal void render_mode_changed (bool dmabuf);
    public signal void rumble (uint player, double strong_magnitude, double weak_magnitude, uint16 milliseconds);
    public signal void current_media_changed (uint media);

    private Runner runner;
    private SharedVideoBuffer video_buffer;
    private SharedInputBuffer input_buffer;
    private Dmabuf? dmabuf;

    private CoreModule module;

    public RunnerServer (string game_title, string core_name, string platform_name) {
        var platform = Hs.Platform.get_from_name (platform_name);

        Hs.Core core = null;

        try {
            module = new CoreModule (core_name);
            core = module.get_core (platform);
        } catch (Error e) {
            error ("Failed to load core: %s", e.message);
        }

        video_buffer = new SharedVideoBuffer ();
        input_buffer = new SharedInputBuffer (platform);

        runner = new Runner (core, video_buffer, input_buffer, game_title);

        runner.redraw.connect (() => redraw ());

        runner.set_dmabuf.connect (dmabuf => {
            this.dmabuf = dmabuf;
            render_mode_changed (dmabuf != null);
        });

        runner.rumble.connect ((player, strong_magnitude, weak_magnitude, milliseconds) => {
            rumble (player, strong_magnitude, weak_magnitude, milliseconds);
        });

        runner.notify["current-media"].connect (() => {
            current_media_changed (runner.current_media);
        });
    }

    internal void register_platform (DBusConnection connection) throws Error {
        var platform = runner.core.platform;

        register_platform_server (
            runner, platform, connection,
            "/app/drey/Highscore/Runner/"
        );

        var base_platform = platform.get_base_platform ();
        if (base_platform != platform) {
            register_platform_server (
                runner, base_platform, connection,
                "/app/drey/Highscore/Runner/"
            );
        }
    }

    public async void load_rom (string[] rom_paths, string save_path) throws Error {
        runner.load_rom (rom_paths, save_path);
    }

    public async void start () throws Error {
        runner.start ();
    }

    public async UnixInputStream get_input_buffer () throws Error {
        return wrap_fd (input_buffer);
    }

    public async UnixInputStream get_video_buffer () throws Error {
        return wrap_fd (video_buffer);
    }

    public async UnixInputStream get_dmabuf (out Variant metadata) throws Error {
        metadata = dmabuf.serialize_metadata ();

        return wrap_fd (dmabuf);
    }

    public async void stop () throws Error {
        runner.stop ();
    }

    public async void reset (bool hard) throws Error {
        runner.reset (hard);
    }

    public async void pause () throws Error {
        runner.pause ();
    }

    public async void resume () throws Error {
        runner.resume ();
    }

    public async void reload_save (string path) throws Error {
        runner.reload_save (path);
    }

    public async void sync_save () throws Error {
        runner.sync_save ();
    }

    public async void save_state (string path) throws Error {
        yield runner.save_state (path);
    }

    public async void load_state (string path) throws Error {
        yield runner.load_state (path);
    }

    public async Hs.Region get_region () throws Error {
        return runner.get_region ();
    }

    public async uint get_current_media () throws Error {
        return runner.current_media;
    }

    public async void set_current_media (uint media) throws Error {
        runner.current_media = media;
    }
}
