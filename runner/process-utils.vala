// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.ProcessUtils {
    public extern void set_parent_death_signal (int signal);

    public extern void enable_print_backtrace_on_crash ();
}
