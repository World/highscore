// This file is part of Highscore. License: GPL-3.0-or-later

using Highscore;

int main (string[] args) {
    Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.GNOMELOCALEDIR);
    Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain (Config.GETTEXT_PACKAGE);

    Environment.set_prgname ("highscore-runner");
    Environment.set_application_name (_("Highscore"));
    Environment.set_variable ("PULSE_PROP_media.role", "game", true);
    Environment.set_variable ("PULSE_PROP_application.icon_name", Config.APPLICATION_ID, true);
    Environment.set_variable ("SDL_VIDEO_WAYLAND_ALLOW_LIBDECOR", "0", true);

    ProcessUtils.set_parent_death_signal (Posix.Signal.TERM);

    Debug.parse (true);

    if (BACKTRACE in Debug.get_flags ())
        ProcessUtils.enable_print_backtrace_on_crash ();

    debug ("Starting runner process");

    var loop = new MainLoop ();

    try {
        Unix.set_fd_nonblocking (3, true);

        var socket = new Socket.from_fd (3);
        var stream = SocketConnection.factory_create_connection (socket);

        assert (stream is UnixConnection);

        var guid = DBus.generate_guid ();
        var connection = new DBusConnection.sync (
            stream, guid,
            DELAY_MESSAGE_PROCESSING | AUTHENTICATION_SERVER
        );

        debug ("Connected");

        connection.exit_on_close = false;
        connection.on_closed.connect (loop.quit);

        var server = new RunnerServer (args[1], args[2], args[3]);

        connection.register_object ("/app/drey/Highscore/Runner", server);

        server.register_platform (connection);

        connection.start_message_processing ();

        debug ("Running main loop");

        loop.run ();
    } catch (Error e) {
        critical ("Failed to initialize runner process: %s", e.message);

        return 1;
    }

    debug ("Stopping runner process");

    return 0;
}
