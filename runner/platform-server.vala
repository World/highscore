// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.PlatformServer : Object {
    public Hs.Core core { get; construct; }
    public Runner runner { get; construct; }
}
