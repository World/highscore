// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public class Highscore.OpenGLRenderer : Object, Hs.GLContext, Renderer {
    public signal void set_dmabuf (Dmabuf? dmabuf);

    private SharedVideoBuffer buffer;
    private Dmabuf? dmabuf;

    private Hs.GLProfile profile;
    private int major_version;
    private int minor_version;
    private Hs.GLFlags flags;

    private SDL.Video.Window? window;
    private SDL.Video.GL.Context? context;
    private GLuint framebuffer;
    private GLuint texture;
    private GLuint renderbuffer;

    private int width;
    private int height;

    private int invalidated;

    public OpenGLRenderer (
        SharedVideoBuffer buffer,
        Hs.GLProfile profile,
        int major_version,
        int minor_version,
        Hs.GLFlags flags
    ) {
        this.buffer = buffer;
        this.profile = profile;
        this.major_version = major_version;
        this.minor_version = minor_version;
        this.flags = flags;
    }

    public bool realize () throws Error {
        SDL.Video.GL.ProfileType profile_type;

        switch (profile) {
            case CORE:
                profile_type = CORE;
                break;
            case LEGACY:
                profile_type = COMPATIBILITY;
                break;
            case ES:
                profile_type = ES;
                break;
            default:
                assert_not_reached ();
        }

        SDL.Video.GL.set_attribute (CONTEXT_PROFILE_MASK, profile_type);
        SDL.Video.GL.set_attribute (CONTEXT_MAJOR_VERSION, major_version);
        SDL.Video.GL.set_attribute (CONTEXT_MINOR_VERSION, minor_version);

        SDL.init_subsystem (SDL.InitFlag.VIDEO);

        width = 1;
        height = 1;

        window = new SDL.Video.Window (
            "Highscore OpenGL renderer",
            0, 0, width, height,
            SDL.Video.WindowFlags.OPENGL |
            SDL.Video.WindowFlags.HIDDEN |
            SDL.Video.WindowFlags.BORDERLESS
        );

        context = SDL.Video.GL.Context.create (window);
        SDL.Video.GL.make_current (window, context);

        int version = Epoxy.gl_version ();

        if (major_version * 10 + minor_version > version) {
            unrealize ();

            throw new Hs.GLContextError.INCOMPATIBLE_VERSION (
                "Mismatching GL version: requested %d.%d, available %d.%d",
                major_version, minor_version,
                version / 10, version % 10
            );
        }

        glGenFramebuffers (1, (GLuint[]) &framebuffer);

        return true;
    }

    public void set_size (uint width, uint height) {
        if ((int) width == this.width && (int) height == this.height)
            return;

        this.width = (int) width;
        this.height = (int) height;

        window.set_size (this.width, this.height);

        glBindFramebuffer (GL_FRAMEBUFFER, framebuffer);

        if (texture > 0)
            glDeleteTextures (1, (GLuint[]) &texture);

        glGenTextures (1, (GLuint[]) &texture);
        glBindTexture (GL_TEXTURE_2D, texture);

        glTexImage2D (
            GL_TEXTURE_2D, 0, GL_RGBA8,
            this.width, this.height,
            0, GL_RGBA, GL_UNSIGNED_BYTE, null
        );
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        if (!(DIRECT_FB_ACCESS in flags)) {
            try {
                dmabuf = new Dmabuf.export (texture);
            } catch (Error e) {
                debug (e.message);
                dmabuf = null;
            }
        } else {
            dmabuf = null;
        }

        glFramebufferTexture2D (
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
            GL_TEXTURE_2D, texture, 0
        );

        if (DEPTH in flags) {
            GLenum format, attachment;

            if (STENCIL in flags) {
                format = GL_DEPTH24_STENCIL8;
                attachment = GL_DEPTH_STENCIL_ATTACHMENT;
            } else {
                format = GL_DEPTH_COMPONENT24;
                attachment = GL_DEPTH_ATTACHMENT;
            }

            glGenRenderbuffers (1, (GLuint[]) &renderbuffer);
            glBindRenderbuffer (GL_RENDERBUFFER, renderbuffer);
            glRenderbufferStorage (GL_RENDERBUFFER, format, this.width, this.height);
            glBindRenderbuffer (GL_RENDERBUFFER, 0);

            glFramebufferRenderbuffer (
                GL_FRAMEBUFFER, attachment,
                GL_RENDERBUFFER, renderbuffer
            );
        } else {
            renderbuffer = 0;
        }

        var status = glCheckFramebufferStatus (GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE)
            critical ("Framebuffer not complete: %d", (int) status);

        glBindTexture (GL_TEXTURE_2D, 0);

        glClearColor (0, 0, 0, 1);
        glClear (
            GL_COLOR_BUFFER_BIT |
            GL_DEPTH_BUFFER_BIT |
            GL_STENCIL_BUFFER_BIT
        );

        buffer.lock ();

        if (dmabuf != null)
            buffer.resize (0);
        else
            buffer.resize (height * width * 4);

        buffer.unlock ();
        set_dmabuf (dmabuf);
    }

    public void unrealize () {
        if (texture > 0) {
            glDeleteTextures (1, (GLuint[]) &texture);
            texture = 0;
        }

        if (renderbuffer > 0) {
            glDeleteRenderbuffers (1, (GLuint[]) &renderbuffer);
            renderbuffer = 0;
        }

        if (framebuffer > 0) {
            glDeleteFramebuffers (1, (GLuint[]) &framebuffer);
            framebuffer = 0;
        }

        window.destroy ();
        window = null;
        context = null;
        SDL.quit_subsystem (SDL.InitFlag.VIDEO);
    }

    public uint get_default_framebuffer () {
        return framebuffer;
    }

    public void* get_proc_address (string name) {
        return SDL.Video.GL.get_proc_address (name);
    }

    public void* acquire_framebuffer () {
        if (dmabuf != null) {
            critical ("Direct framebuffer access is not allowed when using dmabuf");
            return null;
        }

        buffer.lock ();

        return buffer.get_pixels ();
    }

    public void release_framebuffer () {
        if (dmabuf != null) {
            critical ("Direct framebuffer access is not allowed when using dmabuf");
            return;
        }

        buffer.unlock ();
    }

    public void swap_buffers () {
        if (window == null)
            return;

        glFlush ();

        if (dmabuf != null || DIRECT_FB_ACCESS in flags) {
            // UI process already got the data, nothing to do here
        } else {
            // Slow path, read pixels into the same framebuffer as for
            // software rendering
            glBindFramebuffer (GL_FRAMEBUFFER, framebuffer);

            var buffer = acquire_framebuffer ();
            glReadPixels (0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, (GL.GLvoid[]) buffer);
            release_framebuffer ();
        }

        AtomicInt.set (ref invalidated, 1);

        SDL.Video.GL.swap_window (window);
    }

    // Frontend functions

    public void start_frame () {
    }

    public bool end_frame () {
        bool invalidated = AtomicInt.compare_and_exchange (ref this.invalidated, 1, 0);

        if (invalidated) {
            Hs.Rectangle area = { 0, 0, width, height };
            buffer.lock ();
            buffer.set_frame_data (R8G8B8X8, area, width * 4, FLIPPED in flags);
            buffer.unlock ();
        }

        return invalidated;
    }
}
