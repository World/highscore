# Running Highscore from Builder

Clone the project with [GNOME Builder](https://flathub.org/apps/org.gnome.Builder),
and run it with the ▶️ button.

# Hacking on libhighscore and cores

## Toolbox method

Highscore has a lot of dependencies, so this process is fairly convoluted.

First, install [Toolbx (toolbox)](https://containertoolbx.org/) or
[Distrobox](https://distrobox.it/).

Create a container with reasonably new system. The rest of the guide will assume
Fedora 42, but use whatever you're comfortable with.

Install dependencies:

```sh
sudo dnf install git git-remote-hg meson cmake gcc gcc-c++ valac gi-docgen \
    intltool nasm ccache "pkgconfig(gee-0.8)" "pkgconfig(sqlite3)" \
    "pkgconfig(libpulse-simple)" "pkgconfig(sdl2)" "pkgconfig(librsvg-2.0)" \
    "pkgconfig(libpcap)"
```

Switch to a directory you'll use for the dependencies.

### libmirage

Build or install libmirage. Fedora doesn't have it, so we need to build it:

```sh
git clone https://github.com/cdemu/cdemu -b libmirage-3.2.8
cd cdemu/libmirage
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release
cmake --build . --parallel
sudo cmake --install .
```

### libfeedback

Build or install libfeedback. Fedora doesn't have it, so we need to build it:

```sh
sudo dnf install "pkgconfig(json-glib-1.0)" "pkgconfig(gsound)"
git clone https://github.com/agx/feedbackd
cd feedbackd
meson setup build --prefix=/usr -Dtests=false
ninja -C build
sudo ninja -C build install
```

### libmanette

Build libmanette. Highscore will depend on an unreleased version, so we need to
build it:

```sh
sudo dnf install "pkgconfig(hidapi-hidraw)" "pkgconfig(libevdev)" "pkgconfig(gudev-1.0)"
git clone https://gitlab.gnome.org/GNOME/libmanette.git
cd libmanette
meson setup build --prefix=/usr
ninja -C build
sudo ninja -C build install
```

### libadwaita

Build or install libadwaita. Fedora 42 has new enough version, so just install it:

```sh
sudo dnf install "pkgconfig(libadwaita-1)"
```

For 41 you'll have to build it:

```sh
sudo dnf build-dep libadwaita
git clone https://gitlab.gnome.org/GNOME/libadwaita.git
cd libadwaita
meson setup build --prefix=/usr -Dtests=false -Dexamples=false
ninja -C build
sudo ninja -C build install
```

(in some cases you may have to build GTK as well)

### libhighscore

```sh
git clone https://gitlab.gnome.org/alicem/libhighscore.git
meson setup build --prefix=/usr
ninja -C build
sudo ninja -C build install
```

### Highscore

Assuming the project is already cloned, switch to its directory, then run:

```sh
meson setup build --prefix=/usr
ninja -C build
sudo ninja -C build install
```

### Cores

Highscore has scripts for cloning and building cores in the `tools/cores`
directory:

- `clone-cores.sh` to clone main cores
- `build-cores.sh` to build and install them
- `clone-extra-cores.sh` to clone unstable cores
- `build-extra-cores.sh` to build and install them

Copy them into a directory where you want to place the checkouts (e.g. the same
directory as for dependencies), then run:

```sh
./clone-cores.sh
./build-cores.sh
```

If you want to hack on unstable cores (currently melonDS and Kronos), run:

```sh
./clone-extra-cores.sh
./build-extra-cores.sh
```

## Builder method

Find the module you want to hack on in the flatpak manifest
(`app.drey.Highscore.Devel.json` as well as files in `flatpak/`), then clone
that module manually.

Change the source of the module you want to modify to `dir` type.
For example, for `libhighscore, change the following:

```json
{
    "type" : "git",
    "url" : "https://gitlab.gnome.org/alicem/libhighscore.git",
    "branch" : "main"
}
```

to:

```json
{
    "type" : "dir",
    "path" : "../libhighscore.git"
}
```

This assumes you have libhighscore checked out next to highscore directory.

Note: the paths are relative to the current json file. For cores it would be
`../../../` instead of `../`.

Then, every time you modify the component, hit Rebuild in Builder, instead of
just building.

This is not recommended, however, as it will cause Builder to rebuild all the
subsequent dependencies every time, which can be slow. If using this approach,
rearrange the manifest to place dependencies you don't plan on changing before
yours (e.g. libmanette or libfeedback) or remove them entirely (when working on
a single core you usually don't need other cores).

# Hacking on cores

Refer to `./tools/cores/build-cores.sh` and `./tools/cores/build-extra-cores.sh`
for the initial build steps.

Below are incremental build steps, as well as notes for individual cores.

## Stable cores

### BlastEm

Incremental build + install (run from `highscore` subdir):

```sh
ninja -C build && sudo ninja -C build install
```

Notes: BlastEm's upstream repository uses hg instead of git, so the fork has a
mirror for its main branch. When syncing, make sure to push the `master` branch
as well.

BlastEm can optionally use new experiment CPU core. It's always using the new
core on non-x86 architectures such as aarch64, but on x86_64 it can be enabled
using the `-Dnew_core=true` build option.

The old and new core have incompatible savestates and use different state
versions because of that.

### bsnes

Incremental build + install (run from `bsnes` subdir):

```sh
make -j 4 target=highscore binary=library build=performance local=false platform=linux
sudo make target=highscore binary=library build=performance libdir=/usr/lib64 install local=false platform=linux
```

Cleaning build files:

```sh
rm -rf obj out
```

### DeSmuME

Incremental build + install (run from `desmume/src/frontend/highscore` subdir):

```sh
ninja -C build && sudo ninja -C build install
```

Note: DeSmuME uses three renderers: OpenGL 3.2, legacy OpenGL and software, used
depending on what's available on the current system. Make sure to test all of
them when making changes to the rendering.

### Gearsystem

Incremental build + install (run from `platforms/highscore` subdir):

```sh
ninja -C build && sudo ninja -C build install
```

### Mednafen

Incremental build + install (run from `highscore` subdir):

```sh
ninja -C build && sudo ninja -C build install
```

Note: Mednafen does not have an upstream repository. The latest release is
listed on their website: https://mednafen.github.io/releases/

### mGBA

Incremental build + install (run from the root dir):

```sh
cmake --build build && sudo cmake --install build
```

### Mupen64Plus

Incremental build + install (rjun from the root dir):

```sh
ninja -C build && sudo ninja -C build install
```

Mupen64Plus core is just glue code, it requires Mupen64Plus-Core, as well as
plugins:
- paraLLEl-RSP and paraLLEl-RDP when possible
- Mupen64Plus-RSP-HLE and GLideN64 as a fallback

The `clone-deps.sh` and `build-deps.sh` can be used to build the dependencies.
The `clone-cores.sh` and `build-cores.sh` scripts do it automatically.

paraLLEl-RSP and paraLLEl-RDP require manual syncing at the moment.

Set the `HIGHSCORE_M64P_FORCE_FALLBACK` environment variable to `1` when running
Highscore to force the core to use fallback plugins even when paraLLEl ones can
be used.

### Nestopia JG

Incremental build + install (run from `highscore` subdir):

```sh
ninja -C build && sudo ninja -C build install
```

### ProSystem JG

Incremental build + install (run from `highscore` subdir):

```sh
ninja -C build && sudo ninja -C build install
```

### Stella

Incremental build + install (run from `src/os/highscore` subdir):

```sh
ninja -C build && sudo ninja -C build install
```

## Unstable cores

### Kronos

Incremental build + install (run from `yabause/src/highscore` subdir):

```sh
ninja -C build && sudo ninja -C build install
```

Note: this port is very barebones and doesn't have input, save or savestate
support.

### melonDS

Incremental build + install (run from the root dir):

```sh
cmake --build build && sudo cmake --install build
```

melonDS has 3 renderers: OpenGL, compute OpenGL and software. Currently, the
port defaults to OpenGL and falls back to software if it's unavailable.

To switch to the compute renderer, change `USE_COMPUTE` to `1` in
`src/frontend/highscore/melonds-highscore.cpp`.

Note: while the port is fully functional, it has rendering issues, so we're
shipping DeSmuME instead: https://gitlab.gnome.org/World/highscore/-/issues/323

# Syncing cores with upstream

In the `tools/cores` directory there are `sync-cores.sh` and `sync-extra-cores.sh`
scripts. They switch to the upstream main branch for each core, pull from it, and
merge it into the highscore branch. To finish syncing:

- Build and install the core to verify that it still works
- Fix any regressions if there are any
- If the savestate format has changed, bump the `StateVersion` key in the
  descriptor file, or add `StateVersion=1` if it's missing
- Push the `highscore` branch

# Porting a new core

Each port needs a `HsCore` implementation and a core descriptor file.
Libhighscore provides detailed documentation for both:

- [Core descriptors](https://alicem.pages.gitlab.gnome.org/libhighscore/core-descriptors.html)
- [`HsCore`](https://alicem.pages.gitlab.gnome.org/libhighscore/class.Core.html)

Porting guidelines:

- Keep the port as unintrusive as possible, to make updating it easier. The best
  case scenario is having the entire port in a subdir and not modifying any of
  the original files.
- Often it makes sense to ignore the upstream build system and use a meson setup
  specifically for the port.
- Use existing cores as a reference.

# Switching the used core

When running in toolbox, use the `gsettings` CLI:

```sh
gsettings set app.drey.Highscore.platform:/app/drey/Highscore/platform/(platform ID)/ preferred-core (core name)
```

For example, to use melonDS instead of DeSmuME for Nintendo DS:

```sh
gsettings set app.drey.Highscore.platform:/app/drey/Highscore/platform/nintendo-ds/ preferred-core melonds
```

To reset to default:

```sh
gsettings reset app.drey.Highscore.platform:/app/drey/Highscore/platform/nintendo-ds/ preferred-core
```

For Flatpak, edit `~/.var/app/app.drey.Highscore.Devel/config/glib-2.0/settings/keyfile`:
add a group matching your platform, and set the `preferred-core` key in it. For
example:

```ini
[app/drey/Highscore/platform/nintendo-ds]
preferred-core=melonds
```

# Debug variables

Highscore allows to set debug flags via the `HIGHSCORE_DEBUG` variable. The
following flags are available:

- `backtrace` - print backtrace when the runner process crashes
- `valgrind` - run the runner process in Valgrind
- `welcome` - always show the welcome dialog
- `no-dmabuf` - disable dmabuf rendering
- `filters` - show internal video filters in preferences
- `parsers` - print information about game parsing

The available flags can also be seen by passing `HIGHSCORE_DEBUG=help`.

# Symbolic icons

The icons used by the app are placed in `icons/scalable/actions` subdir. Edit
the `highscore-icons.svg` file in Inkscape, then use
[Symbolic Preview](https://flathub.org/apps/org.gnome.design.SymbolicPreview)
to export them.

Note: since most icons in there are 24x24, they will look blurry in Symbolic
Preview. Make sure to test them directly in the app.

# Adding a platform

First, it needs to be added to libhighscore.

- Update `hs-platform-c/h` - the enum, docs and all of the functions
- Create an interface for your platform: `src/platforms/hs-(platform)-core.c/h`
- Add them to `meson.build` and `libhighscore.h`
- Update `hs-input-state.h` - definition and docs
- Update `doc/core-descriptors.md` to list the new platform

Then, list the new platform in `src/platforms/platform-register.vala`. Create a
matching directory in `src/platforms`, add overrides for your platform there.

The possible overrides:

- `PlatformControls` - describes available controllers
- `GameParser` - runs when adding games to the library or opening them from a file.
  Parser decides what file is or isn't a game, how to combine multi-file games,
  and parses ROM metadata when available
- `GameMetadata` - holds platform-specific metadata received from the parser.
  Can be serialized and deserialized, so that it can be stored in the database
- `PlatformProperties` - a widget that will be embedded into the properties
  dialog. Typically one or more `AdwPreferencesGroup`s.
- `ScreenSet` - describes the screen type, as well as platform filter and
  available screens for multi-screen systems (such as Nintendo DS).
- `FirmwareList` - lists available firmware for this platform, and contains the
  code for loading it (which involves platform-specific core API)
- `SnapshotPlatformMetadata` - allows to save and load extra data to snapshots,
  such as switch positions on Atari consoles. Access the data from the `save_state`
  and `load_state` functions in your `RunnerDelegate` subclass
- `RunnerDelegate` - glue code that binds together everything else - allows to
  run code when the game loads, resets, on savestate saving and loading, etc.

The overrides should use the platform name as a namespace with `Highscore`, and
standard names for each of them:

- `Highscore.(Platform).Controls`
- `Highscore.(Platform).Parser`
- `Highscore.(Platform).Metadata`
- `Highscore.(Platform).Properties`
- `Highscore.(Platform).ScreenSet`
- `Highscore.(Platform).FirmwareList`
- `Highscore.(Platform).SnapshotMetadata`
- `Highscore.(Platform).RunnerDelegate`

For add-on platforms (such as FDS), inherit from the corresponding override for
the parent platform. Otherwise, inherit directly from the base class.

Cores can additionally provide a header bar widget and an overlay menu add-in,
it's currently set up from `RunnerDelegate`.

Update `src/meson.build` for any newly created files, and register the overrides
in `src/platforms/platform-register.vala`.

Add the new platform to `search-provider/platform-list.vala` - since search
provider is independent of the rest of the app, it contains a separate list of
platform names.

Add the MIME types for the new platform's ROMs/images to `mime_types` in
`data/meson.build`.

Draw and export an icon representing your platform (as well as any additional
controllers it may have).

Create touch overlays for its controllers (use similar controllers as a
reference).

Select a matching accurate filter (e.g. NTSC, PAL or LCD). If it doesn't exist,
create it, and add a corresponding type to `src/video-filter/screen-type.vala`,
then specify that type in your `ScreenSet` override.

Note: the specifics _will_ differ between platforms. NTSC output on NES vs Mega
Drive is drastically different, as well as Game Boy vs Neo Geo Pocket LCDs.
These differences should be represented accurately whenever possible.

If your platform needs settings (either global or per-game), create a schemas
file in `data/schemas/platforms` and add it there. Update
`data/schemas/meson.build`. To access settings, use `GameSettings` class.

Any new platforms need a matching core. Refer to the "porting a new core" section
above.

## Adding platform-specific API

Assuming the API is already in libhighscore and needs to be called from the UI
process, the steps are the following:

If your platform already has platform-specific API, update the
`runner/platforms/(platform).vala` and `src/platforms/(platform)/(platform)-proxy.vala`
files.

If it doesn't, create those files, and update `src/meson.build`,
`runner/meson.build` and `shared/platform-server-factory.vala` (in 2 places).

To call the new API from your runner delegate, access the `runner.platform_proxy`
property and cast it to your proxy type.

Note: when dealing with an add-in platforms, use `runner.addon_proxy` instead.
For example, for FDS `platform_proxy` will be the NES proxy, while `addon_proxy`
will be the FDS proxy.

# Use of Generative AI

This project does not allow contributions generated by large languages models
(LLMs) and chatbots. This ban includes tools like ChatGPT, Claude, Copilot,
DeepSeek, and Devin AI. We are taking these steps as precaution due to the
potential negative influence of AI generated content on quality, as well as
likely copyright violations.

This ban of AI generated content applies to all parts of the projects,
including, but not limited to, code, documentation, issues, and artworks. An
exception applies for purely translating texts for issues and comments to
English.

AI tools can be used to answer questions and find information. However, we
encourage contributors to avoid them in favor of using
[existing documentation](https://developer.gnome.org) and our
[chats and forums](https://welcome.gnome.org). Since AI generated information is
frequently misleading or false, we cannot supply support on anything referencing
AI output.
