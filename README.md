# Highscore

This is a rewrite of Highscore, formerly gnome-games.

See the `pre-rewrite` branch for the old codebase.

## Code of Conduct

When interacting with the project, the
[GNOME Code of Conduct](https://conduct.gnome.org/) applies.
