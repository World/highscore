// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlatformList : Object {
    private HashTable<string, string> platform_names;

    construct {
        platform_names = new HashTable<string, string> (str_hash, str_equal);

        platform_names["atari-2600"]       = _("Atari 2600");
        platform_names["atari-7800"]       = _("Atari 7800");
        platform_names["atari-lynx"]       = _("Atari Lynx");
        platform_names["fds"]              = _("Famicom Disk System");
        platform_names["game-boy"]         = _("Game Boy");
        platform_names["game-boy-advance"] = _("Game Boy Advance");
        platform_names["game-gear"]        = _("Game Gear");
        platform_names["master-system"]    = _("Master System");
        platform_names["mega-drive"]       = _("Sega Genesis");
        platform_names["neo-geo-pocket"]   = _("Neo Geo Pocket");
        platform_names["nes"]              = _("Nintendo (NES)");
        platform_names["nintendo-64"]      = _("Nintendo 64");
        platform_names["nintendo-ds"]      = _("Nintendo DS");
        platform_names["pc-engine"]        = _("TurboGrafx-16");
        platform_names["pc-engine-cd"]     = _("TurboGrafx-CD");
        platform_names["playstation"]      = _("PlayStation");
        platform_names["sega-saturn"]      = _("Sega Saturn");
        platform_names["sg-1000"]          = _("SG-1000");
        platform_names["snes"]             = _("Super Nintendo (SNES)");
        platform_names["virtual-boy"]      = _("Virtual Boy");
        platform_names["wonderswan"]       = _("WonderSwan");
    }

    public string get_name (string id) {
        return platform_names[id];
    }
}
