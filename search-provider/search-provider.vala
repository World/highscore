// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "org.gnome.Shell.SearchProvider2")]
public class Highscore.SearchProvider : Gtk.Application {
    private const string DB_QUERY = """
        SELECT
            games.uid,
            platform,
            COALESCE (titles.title, games.title) as the_title
        FROM games LEFT JOIN titles ON games.uid == titles.uid
        WHERE the_title LIKE $TERMS;
    """;

    private struct GameData {
        string platform;
        string title;
    }

    private HashTable<string, GameData?> game_data;
    private string[]? sizes;
    private Sqlite.Database db;
    private Sqlite.Statement statement;
    // HACK—see get_subsearch_result_set ()
    private bool is_first_subsearch;

    internal SearchProvider () {
        Object (
            application_id: Config.APPLICATION_ID + ".SearchProvider",
            flags: ApplicationFlags.IS_SERVICE,
            inactivity_timeout: 10000
        );
    }

    construct {
        is_first_subsearch = false;

        var db_path = Path.build_filename (
            Environment.get_user_data_dir (),
            "highscore",
            "library.sqlite3"
        );

        var db_file = File.new_for_path (db_path);
        if (!db_file.query_exists ())
            return;

        if (Sqlite.Database.open (db_path, out db) != Sqlite.OK) {
            critical ("Couldn’t open the database for '%s'", db_path);
            return;
        }

        if (db.prepare_v2 (DB_QUERY, DB_QUERY.length, out statement) != Sqlite.OK) {
            critical ("Preparation failed: %s", db.errmsg ());
            return;
        }
    }

    public string[] get_initial_result_set (string[] terms) throws Error {
        if (statement == null)
            return {};

        statement.reset ();

        var position = statement.bind_parameter_index ("$TERMS");
        var terms_str = "%%" + string.joinv (" ", terms) + "%%";

        if (statement.bind_text (position, terms_str) != Sqlite.OK) {
            critical ("Couldn't bind value: %s", db.errmsg ());
            return {};
        }

        string[] results = {};
        game_data = new HashTable<string, GameData?> (str_hash, str_equal);

        var platform_list = new PlatformList ();

        while (statement.step () == Sqlite.ROW) {
            var uid = statement.column_text (0);
            var platform = statement.column_text (1);
            var title = statement.column_text (2);

            game_data[uid] = {
                platform_list.get_name (platform),
                title
            };

            results += uid;
        }

        qsort_with_data<string> (results, sizeof (string), (a, b) => {
            var a_data = game_data[a];
            var b_data = game_data[b];

            int ret = a_data.title.collate (b_data.title);
            if (ret != 0)
                return ret;

            ret = a_data.platform.collate (b_data.platform);
            if (ret != 0)
                return ret;

            return strcmp (a, b);
        });

        is_first_subsearch = true;

        return results;
    }

    public string[] get_subsearch_result_set (string[] previous_results, string[] terms) throws Error {
        /*
         * HACK: This bypasses a bug where GNOME Shell will discard the results
         * returned by the first invocation of get_initial_result_set () on
         * startup.
         */
        if (is_first_subsearch) {
            is_first_subsearch = false;
            if (previous_results.length == 0)
                return get_initial_result_set (terms);
        }

        var terms_str = string.joinv (" ", terms).casefold ();

        string[] results = {};
        foreach (var uid in previous_results) {
            if (terms_str in game_data[uid].title.casefold ())
                results += uid;
        }

        return results;
    }

    private async File? get_cover (string uid) throws Error {
        var cache_path = Path.build_filename (
            Environment.get_user_cache_dir (), "highscore", "covers"
        );

        var cache_dir = File.new_for_path (cache_path);
        if (!cache_dir.query_exists ())
            return null;

        if (sizes == null) {
            sizes = {};

            var enumerator = yield cache_dir.enumerate_children_async (
                FileAttribute.STANDARD_NAME + "," + FileAttribute.STANDARD_TYPE,
                FileQueryInfoFlags.NONE
            );

            FileInfo info;
            while ((info = enumerator.next_file (null)) != null) {
                if (info.get_file_type () != FileType.DIRECTORY)
                    continue;

                sizes += info.get_name ();
            }

            qsort_with_data<string> (sizes, sizeof (string), (a, b) => {
                return a.collate (b);
            });
        }

        foreach (var size in sizes) {
            var file = cache_dir.get_child (size).get_child (@"$uid.png");
            if (file.query_exists ())
                return file;
        }

        return null;
    }

    public async HashTable<string, Variant>[] get_result_metas (string[] results) throws Error {
        hold ();

        var result = new GenericArray<HashTable<string, Variant>> ();

        foreach (var uid in results) {
            var game_data = game_data[uid];

            var cover = yield get_cover (uid);

            GLib.Icon icon;
            if (cover != null)
                icon = new FileIcon (cover);
            else
                icon = new ThemedIcon ("%s-symbolic".printf (Config.APPLICATION_ID));

            var metadata = new HashTable<string, Variant> (str_hash, str_equal);

            metadata.insert ("id", uid);
            metadata.insert ("name", game_data.title);
            metadata.insert ("description", game_data.platform);
            metadata.insert ("icon", icon.to_string ());

            result.add (metadata);
        }

        release ();

        return result.data;
    }

    public void activate_result (string uid, string[] terms, uint32 timestamp) throws Error {
        run_with_args ({ "--uid", uid, "--search", string.joinv (" ", terms) });
    }

    public void launch_search (string[] terms, uint32 timestamp) throws Error {
        run_with_args ({ "--search", string.joinv (" ", terms) });
    }

    private void run_with_args (string[] run_args) {
        try {
            string[] args = { "/app/bin/highscore" };

            foreach (var arg in run_args)
                args += arg;

            Process.spawn_async (
                null, args, null, SpawnFlags.SEARCH_PATH, null, null
            );
        } catch (Error error) {
            critical ("Couldn't run Highscore: %s", error.message);
        }
    }

    protected override bool dbus_register (DBusConnection connection, string object_path) {
        try {
            connection.register_object (object_path, this);
        } catch (IOError e) {
            warning ("Could not register search provider: %s", e.message);
            quit ();
        }

        return true;
    }
}
