// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.ColorUtils {
    public Graphene.Vec3 rgb_to_vec3 (int rgb) {
        Graphene.Vec3 vec = {};

        float r = ((rgb >> 16) & 0xff) / 255f;
        float g = ((rgb >>  8) & 0xff) / 255f;
        float b = (rgb         & 0xff) / 255f;

        vec.init (r, g, b);

        return vec;
    }

    public int vec3_to_rgb (Graphene.Vec3 vec) {
        int r = (int) Math.round (vec.get_x () * 255).clamp (0, 255);
        int g = (int) Math.round (vec.get_y () * 255).clamp (0, 255);
        int b = (int) Math.round (vec.get_z () * 255).clamp (0, 255);

        return r << 16 | g << 8 | b;
    }

    public Gdk.RGBA color_to_rgba (int rgb) {
        float r = ((rgb >> 16) & 0xFF) / 255.0f;
        float g = ((rgb >> 8) & 0xFF) / 255.0f;
        float b = (rgb & 0xFF) / 255.0f;

        return { r, g, b, 1.0f };
    }

    public int rgba_to_color (Gdk.RGBA rgba) {
        int r = (int) Math.round (rgba.red * 255).clamp (0, 255);
        int g = (int) Math.round (rgba.green * 255).clamp (0, 255);
        int b = (int) Math.round (rgba.blue * 255).clamp (0, 255);

        return r << 16 | g << 8 | b;
    }
}
