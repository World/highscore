// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.AnalogUtils {
    public bool is_pointing_at_direction (
        double angle,
        double diagonal_area,
        InputDirection direction
    ) {
        double d = diagonal_area / 2;

        switch (direction) {
            case UP:
                return angle <=    -Math.PI / 4 + d && angle >= -3 * Math.PI / 4 - d;
            case DOWN:
                return angle >=     Math.PI / 4 - d && angle <=  3 * Math.PI / 4 + d;
            case LEFT:
                return angle >= 3 * Math.PI / 4 - d || angle <= -3 * Math.PI / 4 + d;
            case RIGHT:
                return angle <=     Math.PI / 4 + d && angle >=     -Math.PI / 4 - d;
            default:
                assert_not_reached ();
        }
    }
}
