// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.Filename {
    private Regex filename_ext_regex = null;

    public string get_title (File file) throws Error {
        var file_info = file.query_info (
            FileAttribute.STANDARD_DISPLAY_NAME,
            FileQueryInfoFlags.NONE,
            null
        );
        var name = file_info.get_display_name ();

        try {
            if (filename_ext_regex == null)
                filename_ext_regex = /\.\w+$/;

            name = filename_ext_regex.replace (name, name.length, 0, "");
        } catch (RegexError e) {
            error ("Regex replacement failed: %s", e.message);
        }

        name = name.split ("(")[0];
        name = name.strip ();

        return name;
    }
}
