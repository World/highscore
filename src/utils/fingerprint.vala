// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.Fingerprint {
    private string get_for_file (File file, size_t start, size_t? length) throws Error {
        var istream = file.read ();

        return get_for_file_input_stream (istream, start, length);
    }

    private string get_for_file_input_stream (FileInputStream file_stream, size_t start, size_t? length) throws Error {
        size_t size;
        if (length == null) {
            file_stream.seek (0, SeekType.END);
            size = (size_t) file_stream.tell ();
        } else {
            size = length;
        }

        file_stream.seek (start, SeekType.SET);
        var bytes = file_stream.read_bytes (size);

        return Checksum.compute_for_bytes (ChecksumType.MD5, bytes);
    }

    public string get_uid (File file, string prefix) throws Error {
        var fingerprint = Fingerprint.get_for_file (file, 0, null);

        return @"$prefix-$fingerprint";
    }

    public string get_uid_for_chunk (File file, string prefix, size_t start, size_t length) throws Error {
        var fingerprint = Fingerprint.get_for_file (file, start, length);

        return @"$prefix-$fingerprint";
    }
}
