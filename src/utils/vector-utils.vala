// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.VectorUtils {
    private inline Graphene.Vec3 pow3 (Graphene.Vec3 vec, float power) {
        Graphene.Vec3 ret = {};

        ret.init (
            Math.powf (vec.get_x (), power),
            Math.powf (vec.get_y (), power),
            Math.powf (vec.get_z (), power)
        );

        return ret;
    }

    private inline Graphene.Vec3 clamp3 (Graphene.Vec3 vec, float min, float max) {
        Graphene.Vec3 ret = {};

        ret.init (
            vec.get_x ().clamp (min, max),
            vec.get_y ().clamp (min, max),
            vec.get_z ().clamp (min, max)
        );

        return ret;
    }
}
