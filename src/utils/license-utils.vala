// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.LicenseUtils {
    public Gtk.License spdx_to_license (string spdx) {
        if (spdx == "GPL-2.0-or-later")
            return GPL_2_0;
        if (spdx == "GPL-3.0-or-later")
            return GPL_3_0;
        if (spdx == "LGPL-2.1-or-later")
            return LGPL_2_1;
        if (spdx == "LGPL-3.0-or-later")
            return LGPL_3_0;
        if (spdx == "BSD-2-Clause")
            return BSD;
        if (spdx == "MIT")
            return MIT_X11;
        if (spdx == "Artistic-2.0")
            return ARTISTIC;
        if (spdx == "GPL-2.0-only")
            return GPL_2_0_ONLY;
        if (spdx == "GPL-3.0-only")
            return GPL_3_0_ONLY;
        if (spdx == "LGPL-2.1-only")
            return LGPL_2_1_ONLY;
        if (spdx == "LGPL-3.0-only")
            return LGPL_3_0_ONLY;
        if (spdx == "AGPL-3.0-or-later")
            return AGPL_3_0;
        if (spdx == "AGPL-3.0-only")
            return AGPL_3_0_ONLY;
        if (spdx == "BSD-3-Clause")
            return BSD_3;
        if (spdx == "Apache-2.0")
            return APACHE_2_0;
        if (spdx == "MPL-2.0")
            return MPL_2_0;
        if (spdx == "0BSD")
            return @0BSD;

        warning ("Unknown license: %s", spdx);
        return UNKNOWN;
    }
}
