// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://wiki.osdev.org/ISO_9660

namespace Highscore.ISO9660 {
    public struct PathTableEntry {
        uint8 data[255];

        public uint8 directory_id_length {
            get { return (uint8) data[0]; }
        }

        public uint8 extended_attribute_record_length {
            get { return (uint8) data[1]; }
        }

        public uint32 location_of_extent {
            get { return (uint32?) &data[2]; }
        }

        public uint16 parent_directory {
            get { return (uint16?) &data[6]; }
        }

        public string? directory_id {
            owned get {
                if (directory_id_length == 0)
                    return null;

                return sanitize_string ((char[]) &data[8], directory_id_length);
            }
        }

        public inline uint32 length () {
            var ret = (uint32) (
                sizeof (uint8) +
                sizeof (uint8) +
                sizeof (uint32) +
                sizeof (uint16) +
                directory_id_length
            );

            if (ret % 2 != 0)
                ret++;

            return ret;
        }
    }
}
