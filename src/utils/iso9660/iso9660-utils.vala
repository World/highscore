// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://wiki.osdev.org/ISO_9660

namespace Highscore.ISO9660 {
    public inline uint16 get_uint16_lsb_msb (uint16[] data) {
        if (BYTE_ORDER == LITTLE_ENDIAN)
            return uint16.from_little_endian (data[0]);
        else
            return uint16.from_big_endian (data[1]);
    }

    public inline int16 get_int16_lsb_msb (int16[] data) {
        if (BYTE_ORDER == LITTLE_ENDIAN)
            return int16.from_little_endian (data[0]);
        else
            return int16.from_big_endian (data[1]);
    }

    public inline uint32 get_uint32_lsb_msb (uint32[] data) {
        if (BYTE_ORDER == LITTLE_ENDIAN)
            return uint32.from_little_endian (data[0]);
        else
            return uint32.from_big_endian (data[1]);
    }

    public inline int32 get_int32_lsb_msb (int32[] data) {
        if (BYTE_ORDER == LITTLE_ENDIAN)
            return int32.from_little_endian (data[0]);
        else
            return int32.from_big_endian (data[1]);
    }

    public string sanitize_string (char[] str, int length = -1) {
        if (length < 0)
            length = str.length;

        var builder = new StringBuilder ();
        builder.append_len ((string) str, length);

        var ret = builder.free_and_steal ();
        return ret.chomp ();
    }
}
