// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://wiki.osdev.org/ISO_9660

namespace Highscore.ISO9660 {
    public struct DirectoryDateTime {
        uint8 data[7];

        public uint years_since_1900 {
            get { return data[0]; }
        }

        public uint month {
            get { return data[1]; }
        }

        public uint day {
            get { return data[2]; }
        }

        public uint hour {
            get { return data[3]; }
        }

        public uint minute {
            get { return data[4]; }
        }

        public uint second {
            get { return data[5]; }
        }

        public uint timezone {
            get { return data[6]; }
        }
    }

    [Flags]
    public enum FileFlags {
        HIDDEN,
        DIRECTORY,
        ASSOCIATED,
        HAS_FORMAT,
        HAS_PERMISSIONS,
        RESERVED_1,
        RESERVED_2,
        NOT_FINAL,
    }

    public struct DirectoryRecord {
        uint8 data[255];

        public uint8 length {
            get { return (uint8) data[0]; }
        }

        public uint8 extended_attribute_record_length {
            get { return (uint8) data[1]; }
        }

        public uint32 location_of_extent {
            get { return get_uint32_lsb_msb ((uint32[]) &data[2]); }
        }

        public uint32 data_length {
            get { return get_uint32_lsb_msb ((uint32[]) &data[10]); }
        }

        public DirectoryDateTime? datetime {
            get { return (DirectoryDateTime?) &data[18]; }
        }

        public FileFlags flags {
            get { return data[25]; }
        }

        public uint8 file_unit_size {
            get { return (uint8) data[26]; }
        }

        public uint8 interleave_gap_size {
            get { return (uint8) data[27]; }
        }

        public uint16 volume_sequence_number {
            get { return get_uint16_lsb_msb ((uint16[]) &data[28]); }
        }

        public uint8 file_id_length {
            get { return (uint8) data[32]; }
        }

        public string? file_id {
            owned get {
                return sanitize_string ((char[]) &data[33], file_id_length);
            }
        }

        public DirectoryRecord copy () {
            var d = new uint8[length];

            Posix.memcpy (d, data, length);

            return (DirectoryRecord?) d;
        }
    }
}
