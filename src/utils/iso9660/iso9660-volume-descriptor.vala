// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://wiki.osdev.org/ISO_9660

namespace Highscore.ISO9660 {
    public struct DecDateTime {
        uint8 data[17];
    }

    public struct VolumeDescriptor {
        uint8 data[2048];

        public uint8 type_code {
            get { return data[0]; }
        }

        public string standard_id {
            owned get { return sanitize_string ((char[]) &data[1], 5); }
        }

        public uint8 version {
            get { return data[6]; }
        }
    }

    public struct PrimaryVolumeDescriptor : VolumeDescriptor {
        public string system_id {
            owned get { return sanitize_string ((char[]) &data[8], 32); }
        }

        public string volume_id {
            owned get { return sanitize_string ((char[]) &data[40], 32); }
        }

        public uint32 volume_space_size {
            get { return get_uint32_lsb_msb ((uint32[]) &data[80]); }
        }

        public uint16 volume_set_size {
            get { return get_uint16_lsb_msb ((uint16[]) &data[120]); }
        }

        public uint16 volume_sequence_number {
            get { return get_uint16_lsb_msb ((uint16[]) &data[124]); }
        }

        public uint16 logical_block_size {
            get { return get_uint16_lsb_msb ((uint16[]) &data[128]); }
        }

        public uint32 path_table_size {
            get { return get_uint32_lsb_msb ((uint32[]) &data[132]); }
        }

        public uint32 path_table_location {
            get {
                if (BYTE_ORDER == LITTLE_ENDIAN)
                    return uint16.from_little_endian ((uint16?) &data[140]);
                else
                    return uint16.from_big_endian ((uint16?) &data[148]);
            }
        }

        public uint32 optional_path_table_location {
            get {
                if (BYTE_ORDER == LITTLE_ENDIAN)
                    return uint16.from_little_endian ((uint16?) &data[144]);
                else
                    return uint16.from_big_endian ((uint16?) &data[152]);
            }
        }

        public DirectoryRecord? root_directory {
            get { return (DirectoryRecord?) &data[156]; }
        }

        public string volume_set_id {
            owned get { return sanitize_string ((char[]) &data[190], 128); }
        }

        public string publisher_id {
            owned get { return sanitize_string ((char[]) &data[318], 128); }
        }

        public string data_preparer_id {
            owned get { return sanitize_string ((char[]) &data[446], 128); }
        }

        public string application_id {
            owned get { return sanitize_string ((char[]) &data[574], 128); }
        }

        public string copyright_file_id {
            owned get { return sanitize_string ((char[]) &data[702], 37); }
        }

        public string abstract_file_id {
            owned get { return sanitize_string ((char[]) &data[739], 37); }
        }

        public string bibliographic_file_id {
            owned get { return sanitize_string ((char[]) &data[776], 37); }
        }

        public DecDateTime volume_creation_datetime {
            get { return (DecDateTime?) &data[813]; }
        }

        public DecDateTime volume_modification_datetime {
            get { return (DecDateTime?) &data[830]; }
        }

        public DecDateTime volume_expiration_datetime {
            get { return (DecDateTime?) &data[847]; }
        }

        public DecDateTime volume_effective_datetime {
            get { return (DecDateTime?) &data[864]; }
        }

        public uint8 file_structure_version {
            get { return data[881]; }
        }
    }
}
