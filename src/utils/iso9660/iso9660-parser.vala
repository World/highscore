// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://wiki.osdev.org/ISO_9660

public errordomain Highscore.ISO9660.ParserError {
    CANNOT_READ_SECTOR,
    NOT_ISO9660,
    UNSUPPORTED_VOLUME,
    WRONG_FILE_TYPE,
    FILE_NOT_FOUND,
}

public class Highscore.ISO9660.Parser : Object {
    private const uint32 SYSTEM_AREA_SIZE = 0x10;
    private const string MAGIC_STRING = "CD001";

    public Mirage.Track track { get; construct; }

    public string system_id { get; private set; }
    public string volume_id { get; private set; }

    private uint32 start_sector;
    private uint16 block_size;

    private unowned VolumeDescriptor descriptor;
    private unowned DirectoryRecord root_dir;

    public Parser (Mirage.Track track) {
        Object (track: track);
    }

    construct {
        start_sector = track.get_track_start ();
    }

    private inline Mirage.Sector get_sector (uint32 address) throws Error {
        return track.get_sector ((int) (start_sector + address), false);
    }

    private uint8[] read_sector (uint32 address) throws Error {
        var sector = get_sector (address);

        uint8[] buffer;
        if (!sector.get_data (out buffer)) {
            throw new ParserError.CANNOT_READ_SECTOR (
                "Failed to read sector %u", address
            );
        }

        return buffer;
    }

    private uint8[] read_sectors (uint32 address, uint length) throws Error {
        var buffer = new Array<uint8> (false, false);

        for (uint i = 0; i < length; i++) {
            var sector = read_sector (address + i);

            buffer.append_vals (sector, sector.length);
        }

        return buffer.data;
    }

    private inline uint8[] read_blocks (uint32 address, uint length) throws Error {
        var buffer = new Array<uint8> (false, false);

        uint i = 0;

        while (buffer.length < length * block_size) {
            var sector = read_sector (address + i);

            buffer.append_vals (sector, sector.length);

            i++;
        }

        return buffer.data;
    }

    public void parse () throws Error {
        descriptor = (VolumeDescriptor?) read_sector (SYSTEM_AREA_SIZE);

        if (descriptor.standard_id != MAGIC_STRING)
            throw new ParserError.NOT_ISO9660 ("Not an ISO9660 filesystem");

        if (descriptor.type_code == 1) {
            read_primary_volume_descriptor ((PrimaryVolumeDescriptor?) descriptor);

            return;
        }

        // TODO: handle supplementary volumes too?
        throw new ParserError.UNSUPPORTED_VOLUME ("Not a primary volume");
    }

    private void read_primary_volume_descriptor (PrimaryVolumeDescriptor desc) throws Error {
        system_id = desc.system_id;
        volume_id = desc.volume_id;

        block_size = desc.logical_block_size;
        root_dir = desc.root_directory;
    }

    private DirectoryRecord? find_file_in_dir (DirectoryRecord dir, string name) throws Error {
        uint32 extent_location = dir.location_of_extent;
        uint32 length = dir.data_length;

        uint blocks = (uint) Math.ceil ((float) length / block_size);

        for (int i = 0; i < blocks; i++) {
            var extent = read_blocks (extent_location + i, 1);
            uint32 offset = 0;

            while (offset < block_size && (offset + i * block_size) < length) {
                unowned var child = (DirectoryRecord?) &extent[offset];

                if (child.length == 0)
                    break;

                var file_id = child.file_id;

                if (file_id == name)
                    return child.copy ();

                offset += child.length;
            }
        }

        return null;
    }

    private string[] split_path (string path) {
        var components = path.split ("/");
        string[] ret = {};

        foreach (var c in components) {
            if (c == null || c == "")
                continue;

            ret += c;
        }

        return ret;
    }

    private DirectoryRecord? find_file (string path) throws Error {
        unowned var current_dir = root_dir;
        var components = split_path (path);

        for (int i = 0; i < components.length; i++) {
            var child = find_file_in_dir (current_dir, components[i]);
            if (child == null)
                break;

            if (i == components.length - 1)
                return child;

            if (!(DIRECTORY in child.flags)) {
                var builder = new StringBuilder ();

                for (int j = 0; j < i; j++) {
                    builder.append_c ('/');
                    builder.append (components[j]);
                }

                throw new ParserError.WRONG_FILE_TYPE (
                    "File '%s' is not a directory", builder.free_and_steal ()
                );
            }

            current_dir = child;
        }

        throw new ParserError.FILE_NOT_FOUND (
            "File '%s' not found", path
        );
    }

    public bool file_exists (string path) throws Error {
        return find_file (path) != null;
    }

    public uint8[] read_file (string path) throws Error {
        var file = find_file (path);

        if (DIRECTORY in file.flags) {
            throw new ParserError.WRONG_FILE_TYPE (
                "File '%s' is a directory", path
            );
        }

        uint32 extent_location = file.location_of_extent;
        uint32 length = file.data_length;

        uint blocks = (uint) Math.ceil ((float) length / block_size);
        var extent = read_blocks (extent_location, blocks);

        extent.length = (int) length;

        return extent;
    }

    public uint8[] get_system_area () throws Error {
        return read_sectors (0, SYSTEM_AREA_SIZE);
    }
}
