/*
 *  libMirage: utility functions and helpers
 *  Copyright (C) 2006-2014 Rok Mandeljc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib.h>
#include <mirage/mirage.h>

#define CRC32_LUT(tab, idx) crc32_lut[(tab) * 256 + (idx)]

/* Copied from mirage_helper_calculate_crc32_fast() */
guint32
highscore_crc_utils_calculate_crc32_fast (guint32       crc,
                                          const guint8 *data,
                                          guint         length,
                                          gboolean      reflected)
{
  const guint32 *crc32_lut = crc32_d8018001_lut;

  guint32 *current = (guint32 *)data;
  guint8 *current2 = (guint8 *)data;

  g_assert (data);

  if (!reflected) {
    /* FIXME: Implement non-reflected version of slicing-by-8 algorithm */
    while (length--)
      crc = (crc << 8) ^ crc32_lut[(crc >> 24) ^ *data++];
  } else {
    /* Process any initial un-aligned bytes */
    guint ub = ((gulong) current) % sizeof(guint64);

    if (ub) {
      guint temp = ub = sizeof(guint64) - ub;

      while (temp--)
        crc = (crc >> 8) ^ CRC32_LUT(0, (crc & 0xFF) ^ *current2++);

      current = (guint32*) ((guint8*) current + ub);
      length -= ub;
    }

    /* Make sure we are 64-bit aligned here */
    g_assert((((gulong) current) % sizeof(guint64)) == 0);

    /* Process eight bytes at once */
    while (length >= 8) {
      if (G_BYTE_ORDER == G_LITTLE_ENDIAN) {
        guint32 one = *current++ ^ crc;
        guint32 two = *current++;
        crc = CRC32_LUT(0, (two >> 24)       ) ^
              CRC32_LUT(1, (two >> 16) & 0xFF) ^
              CRC32_LUT(2, (two >> 8 ) & 0xFF) ^
              CRC32_LUT(3, (two      ) & 0xFF) ^
              CRC32_LUT(4, (one >> 24)       ) ^
              CRC32_LUT(5, (one >> 16) & 0xFF) ^
              CRC32_LUT(6, (one >> 8 ) & 0xFF) ^
              CRC32_LUT(7, (one      ) & 0xFF);
      } else if (G_BYTE_ORDER == G_BIG_ENDIAN) {
        guint32 one = *current++ ^ GUINT32_SWAP_LE_BE(crc);
        guint32 two = *current++;
        crc = CRC32_LUT(0, (two      ) & 0xFF) ^
              CRC32_LUT(1, (two >> 8 ) & 0xFF) ^
              CRC32_LUT(2, (two >> 16) & 0xFF) ^
              CRC32_LUT(3, (two >> 24)       ) ^
              CRC32_LUT(4, (one      ) & 0xFF) ^
              CRC32_LUT(5, (one >> 8 ) & 0xFF) ^
              CRC32_LUT(6, (one >> 16) & 0xFF) ^
              CRC32_LUT(7, (one >> 24)       );
      } else {
        g_assert_not_reached();
      }
      length -= 8;
    }

    current2 = (guint8 *)current;

    /* Process remaining 1 to 7 bytes */
    while (length--)
      crc = (crc >> 8) ^ CRC32_LUT(0, (crc & 0xFF) ^ *current2++);
  }

  return crc;
}
