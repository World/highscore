// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.DebugInfo {
    public string get_debug_info () {
        var builder = new StringBuilder ();

        append_version (builder);
        builder.append ("\n");

        append_build_deps (builder);
        builder.append ("\n");

        append_runtime_deps (builder);
        builder.append ("\n");

        append_gtk (builder);
        builder.append ("\n");

        append_system (builder);
        builder.append ("\n");

        if (append_flatpak (builder))
            builder.append ("\n");

        append_env (builder);
        builder.append ("\n");

        append_available_cores (builder);
        builder.append ("\n");

        append_used_cores (builder);
        builder.append ("\n");

        append_firmware (builder);
        builder.append ("\n");

        append_gamepads (builder);
        builder.append ("\n");

        append_filters (builder);

        return builder.free_and_steal ();
    }

    private void append_version (StringBuilder builder) {
        string[] suffix_parts = {};

        if (Config.VCS_TAG != "")
            suffix_parts += Config.VCS_TAG;

        if (Config.PROFILE != "")
            suffix_parts += Config.PROFILE;

        if (suffix_parts.length == 0)
            builder.append (Config.VERSION);

        var suffix = string.joinv (", ", suffix_parts);
        builder.append (@"Highscore: $(Config.VERSION) ($suffix)\n");
    }

    private void append_build_deps (StringBuilder builder) {
        var glib = "%u.%u.%u".printf (
            Version.MAJOR, Version.MINOR, Version.MICRO
        );
        var gtk = "%u.%u.%u".printf (
            Gtk.MAJOR_VERSION, Gtk.MINOR_VERSION, Gtk.MICRO_VERSION
        );
        var adw = "%u.%u.%u".printf (
            Adw.MAJOR_VERSION, Adw.MINOR_VERSION, Adw.MICRO_VERSION
        );
        var manette = "%u.%u.%u".printf (
            Manette.MAJOR_VERSION, Manette.MINOR_VERSION, Manette.MICRO_VERSION
        );

        builder.append ("Compiled against:\n");
        builder.append (@"- GLib: $glib\n");
        builder.append (@"- GTK: $gtk\n");
        builder.append (@"- libadwaita: $adw\n");
        builder.append (@"- libmanette: $manette\n");
    }

    private void append_runtime_deps (StringBuilder builder) {
        var glib = "%u.%u.%u".printf (
            Version.major, Version.minor, Version.micro
        );
        var gtk = "%u.%u.%u".printf (
            Gtk.get_major_version (),
            Gtk.get_minor_version (),
            Gtk.get_micro_version ()
        );
        var adw = "%u.%u.%u".printf (
            Adw.get_major_version (),
            Adw.get_minor_version (),
            Adw.get_micro_version ()
        );
        var manette = "%u.%u.%u".printf (
            Manette.get_major_version (),
            Manette.get_minor_version (),
            Manette.get_micro_version ()
        );

        builder.append ("Running against:\n");
        builder.append (@"- GLib: $glib\n");
        builder.append (@"- GTK: $gtk\n");
        builder.append (@"- libadwaita: $adw\n");
        builder.append (@"- libmanette: $manette\n");
    }

    private void append_gtk (StringBuilder builder) {
        var display = Gdk.Display.get_default ();
        var surface = new Gdk.Surface.toplevel (display);
        var renderer = Gsk.Renderer.for_surface (surface);

        var backend_name = display.get_class ().get_name ();
        var renderer_name = renderer.get_class ().get_name ();

        backend_name = backend_name.replace ("Gdk", "").replace ("Display", "");
        renderer_name = renderer_name.replace ("Gsk", "").replace ("Renderer", "");

        builder.append ("GTK:\n");
        builder.append (@"- GDK backend: $backend_name\n");
        builder.append (@"- GSK renderer: $renderer_name\n");

        renderer.unrealize ();
        surface.destroy ();
    }

    private void append_system (StringBuilder builder) {
        var os_name = Environment.get_os_info (OsInfoKey.NAME);
        var os_version = Environment.get_os_info (OsInfoKey.VERSION);

        builder.append ("System:\n");
        builder.append (@"- Name: $os_name\n");
        builder.append (@"- Version: $os_version\n");
    }

    private bool append_flatpak (StringBuilder builder) {
        var file = File.new_for_path ("/.flatpak-info");
        if (!file.query_exists (null))
            return false;

        try {
            var keyfile = new KeyFile ();
            keyfile.load_from_file ("/.flatpak-info", NONE);

            var runtime = keyfile.get_string ("Application", "runtime");
            var runtime_commit = keyfile.get_string ("Instance", "runtime-commit");
            var arch = keyfile.get_string ("Instance", "arch");
            var flatpak_version = keyfile.get_string ("Instance", "flatpak-version");

            bool devel = false;
            if (keyfile.has_key ("Instance", "devel"))
                devel = keyfile.get_boolean ("Instance", "devel");

            builder.append ("Flatpak:\n");
            builder.append (@"- Runtime: $runtime\n");
            builder.append (@"- Runtime commit: $runtime_commit\n");
            builder.append (@"- Arch: $arch\n");
            builder.append (@"- Flatpak version: $flatpak_version\n");

            if (devel)
                builder.append ("- Devel\n");
        } catch (Error e) {
            critical ("Failed to fetch Flatpak info: %s", e.message);
            return false;
        }

        return true;
    }

    private inline void maybe_append_var (StringBuilder builder, string name) {
        var variable = Environment.get_variable (name);

        if (variable != null)
            builder.append (@"- $name: $variable\n");
    }

    private void append_env (StringBuilder builder) {
        var desktop = Environment.get_variable ("XDG_CURRENT_DESKTOP");
        var session_desktop = Environment.get_variable ("XDG_SESSION_DESKTOP");
        var session_type = Environment.get_variable ("XDG_SESSION_TYPE");
        var lang = Environment.get_variable ("LANG");
        var inside_builder = Environment.get_variable ("INSIDE_GNOME_BUILDER");

        builder.append ("Environment:\n");
        builder.append (@"- Desktop: $desktop\n");
        builder.append (@"- Session: $session_desktop ($session_type)\n");
        builder.append (@"- Language: $lang\n");

        if (inside_builder != null)
            builder.append ("- Running inside Builder\n");

        maybe_append_var (builder, "HIGHSCORE_DEBUG");
        maybe_append_var (builder, "GTK_DEBUG");
        maybe_append_var (builder, "GTK_THEME");
        maybe_append_var (builder, "GTK_USE_PORTAL");
        maybe_append_var (builder, "ADW_DISABLE_PORTAL");
    }

    private void append_available_cores (StringBuilder builder) {
        builder.append ("Available cores:\n");

        var register = CoreRegister.get_register ();
        var cores = register.get_cores ();
        foreach (var core in cores) {
            builder.append (@"- $(core.filename) $(core.version)");

            if (core.commit != null)
                builder.append (@" ($(core.commit))");

            builder.append ("\n");
        }
    }

    private void append_used_cores (StringBuilder builder) {
        builder.append ("Used cores:\n");

        var register = CoreRegister.get_register ();
        var platforms = PlatformRegister.get_register ().get_all_platforms ();
        foreach (var platform in platforms) {
            var used_core = register.get_core_for_platform (platform);

            string name;

            if (used_core != null)
                name = used_core.filename;
            else
                name = "(missing)";

            builder.append (@"- $(platform.id): $name\n");
        }
    }

    private void append_firmware (StringBuilder builder) {
        builder.append ("Firmware:\n");

        var platforms = PlatformRegister.get_register ().get_all_platforms ();
        foreach (var platform in platforms) {
            var list = FirmwareList.create (platform);
            if (list == null)
                continue;

            builder.append (@"- $(platform.id):\n");

            foreach (var firmware in list.list_firmware ()) {
                var id = firmware.id;

                builder.append (@"  - $id: ");

                try {
                    firmware.check ();
                } catch (FirmwareError.MISSING e) {
                    builder.append ("missing\n");
                    continue;
                } catch (FirmwareError.WRONG_MD5 e) {
                    var md5 = firmware.get_current_md5 ();
                    builder.append (@"mismatching md5 ($md5)\n");
                    continue;
                } catch (FirmwareError.WRONG_SHA512 e) {
                    var sha512 = firmware.get_current_sha512 ();
                    builder.append (@"mismatching sha512 ($sha512)\n");
                    continue;
                } catch (Error e) {
                    builder.append (@"error ($(e.message))\n");
                    continue;
                }

                if (firmware.has_multiple_checksums ()) {
                    var version = firmware.get_version ();

                    builder.append (@"installed ($version)\n");
                } else {
                    builder.append ("installed\n");
                }
            }
        }
    }

    private void append_gamepads (StringBuilder builder) {
        var manager = GamepadManager.get_instance ();

        builder.append ("Gamepads:\n");

        uint n = manager.gamepads.get_n_items ();
        for (uint i = 0; i < n; i++) {
            var gamepad = manager.gamepads.get_item (i) as Gamepad;

            var name = gamepad.name;
            var guid = gamepad.guid;
            var gamepad_type = gamepad.gamepad_type.id;
            bool connected = gamepad.device != null;

            builder.append (@"- $guid\n");
            builder.append (@"  - Name: $name\n");
            builder.append (@"  - Type: $gamepad_type\n");

            if (connected) {
                var manette_type = gamepad.device.get_device_type ();

                builder.append ("  - Connected\n");
                builder.append (@"  - Libmanette type: $manette_type\n");

                if (gamepad.supports_mapping) {
                    builder.append ("  - Mapping:\n");

                    GamepadMappingUtils.parse_mapping (gamepad, (id, value) => {
                        builder.append (@"    - $id: $value\n");
                    });
                }
            }

            var missing = gamepad.get_missing_controls ();
            if (missing.length > 0) {
                var missing_str = string.joinv (", ", missing);
                builder.append (@"  - Missing: $missing_str\n");
            }

            if (connected && gamepad.supports_rumble)
                builder.append ("  - Supports rumble\n");
        }
    }

    private void append_filters (StringBuilder builder) {
        var register = VideoFilterRegister.get_instance ();

        builder.append ("Filters:\n");

        foreach (var filter in register.get_internal_filters ()) {
            var name = filter.name;

            builder.append (@"- $name\n");
        }

        foreach (var filter in register.get_user_filters ()) {
            var name = filter.name;

            builder.append (@"- $name\n");
        }
    }
}
