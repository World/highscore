// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public class Highscore.GLFramebuffer : Object {
    public int width { get; private set; }
    public int height { get; private set; }

    private GLuint fbo;
    private GLTexture? texture;

    private bool texture_bound;

    public GLFramebuffer () {
        glGenFramebuffers (1, (GLuint[]) &fbo);
    }

    ~GLFramebuffer () {
        glDeleteFramebuffers (1, (GLuint[]) &fbo);

        texture = null;
    }

    public void bind (GLenum dest = GL_FRAMEBUFFER) {
        glBindFramebuffer (dest, fbo);
    }

    public void unbind (GLenum dest = GL_FRAMEBUFFER) {
        glBindFramebuffer (dest, 0);

        if (texture_bound)
            unbind_texture ();
    }

    public void bind_texture () {
        texture.bind ();
        texture_bound = true;
    }

    public void unbind_texture () {
        texture.unbind ();
        texture_bound = false;
    }

    public uint get_texture_id () {
        return texture.get_id ();
    }

    public void create_texture (
        int width,
        int height,
        GLFilterMode min_filter,
        GLFilterMode mag_filter,
        GLWrapMode wrap,
        GLInternalTextureFormat internal_format
    ) {
        texture = new GLTexture ();
        texture.bind ();

        texture.set_storage (width, height, internal_format, GL_RGBA);
        texture.set_params (min_filter, mag_filter, wrap);

        attach_texture (texture);

        texture.unbind ();
    }

    public void attach_texture (GLTexture texture) {
        width = texture.width;
        height = texture.height;

        texture.bind ();
        glFramebufferTexture2D (
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
            texture.get_id (), 0
        );
        texture.unbind ();

        this.texture = texture;
    }

    public void release_texture () {
        width = 0;
        height = 0;

        texture.bind ();
        glFramebufferTexture2D (
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
            0, 0
        );
        texture.unbind ();

        texture = null;
    }
}
