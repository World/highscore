// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public errordomain Highscore.GLShaderError {
    COMPILE_FAILURE,
    LINKING_FAILURE,
}

public class Highscore.GLShader : Object {
    public string source { get; construct; }

    private GLuint program;

    public GLShader (Gdk.GLContext context, string source) throws GLShaderError {
        Object (source: source);

        bool es = context.get_use_es ();
        bool legacy = context.is_legacy ();

        program = init_shader (es, legacy);
    }

    ~GLShader () {
        glDeleteProgram (program);
    }

    private GLuint create_shader (GLenum type, bool es, bool legacy) throws GLShaderError {
        GLuint shader;
        GLint status = GL_FALSE;
        string[] sources = {};

        if (legacy)
            sources += "#version 130\n";
        else if (!es)
            sources += "#version 330\n";

        if (es)
            sources += "#define GLES\n";
        else if (legacy)
            sources += "#define LEGACY\n";
        else
            sources += "#define CORE\n";

        if (type == GL_VERTEX_SHADER)
            sources += "#define VERTEX\n";
        else
            sources += "#define FRAGMENT\n";

        sources += "\n";

        sources += source;
        sources += "\n";

        shader = glCreateShader (type);
        glShaderSource (shader, sources.length, sources, null);
        glCompileShader (shader);

        glGetShaderiv (shader, GL_COMPILE_STATUS, (GLint[]) &status);
        if (status == GL_FALSE) {
            GLint log_len = 0;

            glGetShaderiv (shader, GL_INFO_LOG_LENGTH, (GLint[]) &log_len);

            var buffer = new GL.GLubyte[log_len + 1];
            glGetShaderInfoLog (shader, log_len, (GLsizei[]) null, buffer);

            throw new GLShaderError.COMPILE_FAILURE (
                "Compile failure in %s shader:\n%sSource:\n%s",
                 type == GL_VERTEX_SHADER ? "vertex" : "fragment",
                 (string) buffer,
                 string.joinv ("", sources)
             );
        }

        return shader;
    }

    private GLuint init_shader (bool es, bool legacy) throws GLShaderError {
        GLuint vertex, fragment;
        GLuint program = 0;
        GLint status = GL_FALSE;

        vertex = create_shader (GL_VERTEX_SHADER, es, legacy);
        if (vertex == 0)
            return 0;

        fragment = create_shader (GL_FRAGMENT_SHADER, es, legacy);
        if (fragment == 0) {
            glDeleteShader (vertex);
            return 0;
        }

        program = glCreateProgram ();
        glAttachShader (program, vertex);
        glAttachShader (program, fragment);

        glLinkProgram (program);

        glGetProgramiv (program, GL_LINK_STATUS, (GLint[]) &status);
        if (status == GL_FALSE) {
            GLint log_len = 0;

            glGetProgramiv (program, GL_INFO_LOG_LENGTH, (GLint[]) &log_len);

            var buffer = new GL.GLubyte[log_len + 1];
            glGetProgramInfoLog (program, log_len, (GLsizei[]) null, buffer);

            throw new GLShaderError.LINKING_FAILURE (
                "Linking failure:\n%s",
                 (string) buffer
             );
        }

        glDetachShader (program, vertex);
        glDetachShader (program, fragment);

        return program;
    }

    public void bind () {
        glUseProgram (program);
    }

    public void unbind () {
        glUseProgram (0);
    }

    public void set_uniform_bool (string name, bool value) {
        set_uniform_int (name, value ? 1 : 0);
    }

    public void set_uniform_int (string name, int value) {
        var location = glGetUniformLocation (program, name);

        if (location < 0)
            return;

        glProgramUniform1i (program, location, value);
    }

    public void set_uniform_int_array (string name, int[] values) {
        var location = glGetUniformLocation (program, name);

        if (location < 0)
            return;

        glProgramUniform1iv (program, location, values.length, values);
    }

    public void set_uniform_float (string name, float value) {
        var location = glGetUniformLocation (program, name);

        if (location < 0)
            return;

        glProgramUniform1f (program, location, value);
    }

    public void set_uniform_vec2 (string name, Graphene.Vec2 value) {
        var location = glGetUniformLocation (program, name);

        if (location < 0)
            return;

        glProgramUniform2f (program, location, value.get_x (), value.get_y ());
    }

    public void set_uniform_vec3 (string name, Graphene.Vec3 value) {
        var location = glGetUniformLocation (program, name);

        if (location < 0)
            return;

        glProgramUniform3f (
            program, location, value.get_x (), value.get_y (), value.get_z ()
        );
    }

    public void set_uniform_vec3_array (string name, Graphene.Vec3[] value) {
        var location = glGetUniformLocation (program, name);

        if (location < 0)
            return;

        float[] values = {};

        foreach (var vec in value) {
            values += vec.get_x ();
            values += vec.get_y ();
            values += vec.get_z ();
        }

        glProgramUniform3fv (
            program, location, values.length, values
        );
    }

    public void set_uniform_vec4 (string name, Graphene.Vec4 value) {
        var location = glGetUniformLocation (program, name);

        if (location < 0)
            return;

        glProgramUniform4f (
            program, location,
            value.get_x (), value.get_y (), value.get_z (), value.get_w ()
        );
    }

    public void set_uniform_matrix (string name, Graphene.Matrix value) {
        var location = glGetUniformLocation (program, name);

        if (location < 0)
            return;

        float values[16];
        value.to_float (ref values);

        glProgramUniformMatrix4fv (
            program, location, 1, (GLboolean) GL_FALSE, (GLfloat[]) values
        );
    }

    public void set_attribute_pointer (
        string name,
        int size,
        GLenum type,
        bool normalized = false,
        ulong stride = 0,
        ulong offset = 0
    ) {
        var location = glGetAttribLocation (program, name);
        var offset_ptr = (void*) offset;

        glEnableVertexAttribArray (location);
        glVertexAttribPointer (
            location, size, type,
            (GLboolean) (normalized ? GL_TRUE : GL_FALSE),
            (int) stride, (GLvoid[]?) offset_ptr
        );
    }
}
