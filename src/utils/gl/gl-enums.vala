// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public enum Highscore.GLWrapMode {
    BORDER,
    EDGE,
    REPEAT;

    public GLenum to_gl_enum () {
        switch (this) {
            case BORDER:
                return GL_CLAMP_TO_BORDER;
            case EDGE:
                return GL_CLAMP_TO_EDGE;
            case REPEAT:
                return GL_REPEAT;
            default:
                assert_not_reached ();
        }
    }

    public string to_string () {
        switch (this) {
            case BORDER:
                return "border";
            case EDGE:
                return "edge";
            case REPEAT:
                return "repeat";
            default:
                assert_not_reached ();
        }
    }

    public static GLWrapMode? from_string (string wrap_str) {
        if (wrap_str == "border")
            return BORDER;

        if (wrap_str == "edge")
            return EDGE;

        if (wrap_str == "repeat")
            return REPEAT;

        warning ("Unknown wrap mode: %s", wrap_str);
        return null;
    }
}

public enum Highscore.GLFilterMode {
    NEAREST,
    LINEAR;

    public GLenum to_gl_enum () {
        switch (this) {
            case NEAREST:
                return GL_NEAREST;
            case LINEAR:
                return GL_LINEAR;
            default:
                assert_not_reached ();
        }
    }

    public string to_string () {
        switch (this) {
            case NEAREST:
                return "nearest";
            case LINEAR:
                return "linear";
            default:
                assert_not_reached ();
        }
    }

    public static GLFilterMode? from_string (string filter_str) {
        if (filter_str == "nearest")
            return NEAREST;

        if (filter_str == "linear")
            return LINEAR;

        warning ("Unknown filter mode: %s", filter_str);
        return null;
    }
}

public enum Highscore.GLInternalTextureFormat {
    RGBA8,
    RGBA16,
    RGBA16F,
    RGBA32F;

    public GLenum to_gl_enum () {
        switch (this) {
            case RGBA8:
                return GL_RGBA8;
            case RGBA16:
                return GL_RGBA16;
            case RGBA16F:
                return GL_RGBA16F;
            case RGBA32F:
                return GL_RGBA32F;
            default:
                assert_not_reached ();
        }
    }

    public GLenum to_textype () {
        switch (this) {
            case RGBA8:
                return GL_UNSIGNED_BYTE;
            case RGBA16:
                return GL_UNSIGNED_SHORT;
            case RGBA16F:
                return GL_HALF_FLOAT;
            case RGBA32F:
                return GL_FLOAT;
            default:
                assert_not_reached ();
        }
    }

    public string to_string () {
        switch (this) {
            case RGBA8:
                return "rgba8";
            case RGBA16:
                return "rgba16";
            case RGBA16F:
                return "rgba16f";
            case RGBA32F:
                return "rgba32f";
            default:
                assert_not_reached ();
        }
    }

    public static GLInternalTextureFormat? from_string (string format_str) {
        if (format_str == "rgba8")
            return RGBA8;

        if (format_str == "rgba16")
            return RGBA16;

        if (format_str == "rgba16f")
            return RGBA16F;

        if (format_str == "rgba32f")
            return RGBA32F;

        warning ("Unknown internal texture format: %s", format_str);
        return null;
    }
}
