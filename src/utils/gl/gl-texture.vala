// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public class Highscore.GLTexture : Object {
    public int width { get; private set; }
    public int height { get; private set; }

    private GLuint tex;
    private bool is_dmabuf;

    public GLTexture () {
        glGenTextures (1, (GLuint[]) &tex);
    }

    public GLTexture.from_dmabuf (Dmabuf dmabuf) {
        tex = dmabuf.import ();
        width = dmabuf.get_width ();
        height = dmabuf.get_height ();

        is_dmabuf = true;
    }

    ~GLTexture () {
        glDeleteTextures (1, (GLuint[]) &tex);
    }

    public void bind () {
        glBindTexture (GL_TEXTURE_2D, tex);
    }

    public void unbind () {
        glBindTexture (GL_TEXTURE_2D, 0);
    }

    public GLuint get_id () {
        return tex;
    }

    public void upload (Gdk.Texture texture) requires (!is_dmabuf) {
        var downloader = new Gdk.TextureDownloader (texture);

        var format = texture.get_format ();

        int pixel_size;
        GLenum gl_format, internal_format;

        switch (format) {
            case R8G8B8:
                pixel_size = 3;
                gl_format = GL_RGB;
                internal_format = GL_RGB;
                break;
            case R8G8B8A8:
            case R8G8B8A8_PREMULTIPLIED:
            case R8G8B8X8:
                pixel_size = 4;
                gl_format = GL_RGBA;
                internal_format = GL_RGBA;
                break;
            case B8G8R8A8:
            case B8G8R8A8_PREMULTIPLIED:
            case B8G8R8X8:
                pixel_size = 4;
                gl_format = GL_BGRA;
                internal_format = GL_RGBA;
                break;
            default:
                error ("Unknown texture format: %d", format);
        }

        downloader.set_format (format);

        size_t stride;
        var bytes = downloader.download_bytes (out stride);

        width = texture.width;
        height = texture.height;

        glPixelStorei (GL_UNPACK_ROW_LENGTH, (GLint) stride / pixel_size);
        glTexImage2D (
            GL_TEXTURE_2D,
            0,
            (GLint) internal_format,
            width,
            height,
            0,
            gl_format,
            GL_UNSIGNED_BYTE,
            (GLvoid[]) bytes.get_data ()
        );
        glPixelStorei (GL_UNPACK_ROW_LENGTH, 0);
    }

    public void set_storage (
        int width,
        int height,
        GLInternalTextureFormat internal_format,
        GLint format
    ) {
        this.width = width;
        this.height = height;

        glTexImage2D (
            GL_TEXTURE_2D,
            0,
            (GLint) internal_format.to_gl_enum (),
            width,
            height,
            0,
            format,
            internal_format.to_textype (),
            null
        );
    }

    public void set_params (GLFilterMode min_filter, GLFilterMode mag_filter, GLWrapMode wrap) {
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLint) wrap.to_gl_enum ());
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLint) wrap.to_gl_enum ());
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint) min_filter.to_gl_enum ());
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint) mag_filter.to_gl_enum ());
    }
}
