// This file is part of Highscore. License: GPL-3.0-or-later

// Partially adapted from MirageFragment
/*
 *  libMirage: fragment
 *  Copyright (C) 2006-2014 Rok Mandeljc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

namespace Highscore.CdUtils {
    public string[] get_disc_extra_paths (string original, Mirage.Disc disc) {
        var filenames = new Gee.TreeSet<string> ((a, b) => strcmp (a, b));

        disc.enumerate_sessions (session => {
            return session.enumerate_tracks (track => {
                return track.enumerate_fragments (fragment => {
                    var main = fragment.main_data_get_filename ();
                    if (main != null && main != original)
                        filenames.add (main);

                    var subchannel = fragment.subchannel_data_get_filename ();
                    if (subchannel != null && main != original)
                        filenames.add (subchannel);

                    return true;
                });
            });
        });

        return filenames.to_array ();
    }

    public bool find_magic_string (Mirage.Track track, string magic) throws Error {
        var start = track.get_track_start ();
        var length = track.layout_get_length ();

        for (int i = start; i < length; i++) {
            var sector = track.get_sector (i, false);

            uint8[] buffer;
            if (!sector.get_data (out buffer))
                return false;

            if (find_magic_in_buffer (buffer, magic))
                return true;
        }

        return false;
    }

    private bool find_magic_in_buffer (uint8[] data, string magic) {
        for (int i = 0; i < data.length - magic.length; i++) {
            if (Posix.memcmp (&data[i], magic, magic.length) == 0)
                return true;
        }

        return false;
    }

    public string get_disc_checksum (Mirage.Disc disc) throws Error {
        var builder = new StringBuilder ();

        disc.enumerate_sessions (session => {
            return session.enumerate_tracks (track => {
                try {
                    uint32 crc = get_track_crc (track);

                    if (crc == 0)
                        return true;

                    builder.append_printf ("%08x", crc);
                } catch (Error e) {
                    critical ("Failed to get track checksum: %s", e.message);
                    return false;
                }

                return true;
            });
        });

        return Checksum.compute_for_string (MD5, builder.free_and_steal ());
    }

    // Adapted from libmirage
    private uint64 fragment_get_main_data_position (Mirage.Fragment fragment, int address) {
        /* Calculate 'full' sector size:
            -> track data + subchannel data, if there's internal subchannel
            -> track data, if there's external or no subchannel
        */

        uint64 size_full = fragment.main_data_get_size ();
        var subchannel_format = fragment.subchannel_data_get_format ();
        if ((subchannel_format & Mirage.SubchannelDataFormat.INTERNAL) > 0)
            size_full += fragment.subchannel_data_get_size ();

        /* We assume address is relative address */
        /* guint64 casts are required so that the product us 64-bit; product of two
           32-bit integers would be 32-bit, which would be truncated at overflow... */
        return fragment.main_data_get_offset () + (uint64) address * size_full;
    }

    private uint64 read_main_data_fast (
        Mirage.Fragment fragment,
        Mirage.Stream stream,
        int address,
        uint8[] buffer,
        uint64 last_pos
    ) throws Error {
        /* Determine position within file */
        uint64 position = fragment_get_main_data_position (fragment, address);

        ssize_t read_len = 0;

        try {
            if (last_pos == 0)
                stream.seek ((int64) position, SET);
            else if (last_pos != position)
                stream.seek ((int64) (position - last_pos), CUR);

            last_pos = position + buffer.length;

            read_len = stream.read (buffer, buffer.length);
        } catch (Error e) {
            /* Note: we ignore all errors here in order to be able to cope with truncated mini images */
        }

        /* Binary audio files may need to be swapped from BE to LE */
        if (fragment.main_data_get_format () == Mirage.MainDataFormat.AUDIO_SWAP) {
            uint16[] buffer_16 = (uint16[]) buffer;

            for (int i = 0; i < read_len; i++) {
                buffer_16[i] = buffer_16[i].to_little_endian ();
            }
        }

        return last_pos;
    }

    private uint32 get_track_crc (Mirage.Track track) throws Error {
        int start = track.get_track_start ();
        int length = track.layout_get_length ();

        uint32 crc = 0;

        int start_sector = track.layout_get_start_sector ();

        var sector = Object.new (typeof (Mirage.Sector), null) as Mirage.Sector;
        var sector_type = track.get_sector_type ();

        Mirage.Fragment fragment = null;
        int fragment_start = 0;
        uint8[] main_buffer = null;
        Mirage.Stream stream = null;

        uint64 last_pos = 0;

        unowned uint8[] sector_data;

        for (int i = start; i < length; i++) {
            int abs_address = i + start_sector;

            var new_fragment = track.get_fragment_by_address (i);

            if (new_fragment != fragment) {
                fragment = new_fragment;
                fragment_start = fragment.get_address ();

                int new_size = fragment.main_data_get_size ();
                if (main_buffer == null || new_size != main_buffer.length)
                    main_buffer = new uint8[new_size];

                var filename = fragment.main_data_get_filename ();
                stream = fragment.create_input_stream (filename);
            }

            last_pos = read_main_data_fast (fragment, stream, i - fragment_start, main_buffer, last_pos);

            mirage_sector_feed_data (
                sector,
                abs_address,
                sector_type,
                main_buffer,
                PW,
                null,
                0
            );

            if (!sector.get_data (out sector_data))
                continue;

            crc = CrcUtils.calculate_crc32_fast (crc, sector_data, true);
        }

        return crc;
    }
}

extern bool mirage_sector_feed_data (
    Mirage.Sector sector,
    int address,
    Mirage.SectorType type,
    [CCode (type = "const guint8 *", array_length_cname = "main_data_length", array_length_type = "guint", array_length_pos = 4.5)]
    uint8[] main_data,
    Mirage.SectorSubchannelFormat subchannel_format,
    [CCode (type = "const guint8 *", array_length_cname = "subchannel_data_length", array_length_type = "guint", array_length_pos = 6.5)]
    uint8[]? subchannel_data,
    int ignore_data_mask
) throws Error;
