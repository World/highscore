// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.CrcUtils {
    public extern uint32 calculate_crc32_fast (uint32 crc, uint8[] data, bool reflected);
}
