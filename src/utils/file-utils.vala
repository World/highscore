// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.FileUtils {
    public File get_directory_safely (File parent, string initial_name) {
        int i = 0;

        while (true) {
            string name;

            if (i == 0)
                name = initial_name;
            else
                name = @"$initial_name ($i)";

            var child = parent.get_child (name);

            if (!child.query_exists () ||
                child.query_file_type (NONE) == DIRECTORY) {
                return child;
            }

            i++;
        }
    }

    public void split_filename (string name, out string basename, out string extension) {
        var pos = name.last_index_of_char ('.');

        if (pos < 0) {
            basename = name;
            extension = "";
            return;
        }

        basename = name.substring (0, pos);
        extension = name.substring (pos + 1, -1);
    }

    public File replace_extension (File file, string new_extension) {
        string basename;

        split_filename (file.get_basename (), out basename, null);

        var parent = file.get_parent ();

        return parent.get_child (@"$basename.$new_extension");
    }

    public File get_file_with_unique_name (File parent, string initial_name) {
        string initial_basename = "", extension = "";
        int i = 0;

        while (true) {
            string name;

            if (i == 0)
                name = initial_name;
            else
                name = @"$initial_basename ($i).$extension";

            var child = parent.get_child (name);

            if (!child.query_exists ())
                return child;

            if (i == 0)
                split_filename (initial_name, out initial_basename, out extension);

            i++;
        }
    }

    public async void copy_recursively (File src, File dest) throws Error {
        var src_type = src.query_file_type (FileQueryInfoFlags.NONE);

        if (src_type == FileType.DIRECTORY) {
            if (!dest.query_exists ())
                dest.make_directory_with_parents ();

            var enumerator = yield src.enumerate_children_async (
                FileAttribute.STANDARD_NAME, FileQueryInfoFlags.NONE
            );

            for (var info = enumerator.next_file (); info != null; info = enumerator.next_file ()) {
                var child_name = info.get_name ();
                var src_child = src.get_child (child_name);
                var dest_child = dest.get_child (child_name);

                yield copy_recursively (src_child, dest_child);
            }
        } else {
            yield src.copy_async (dest, FileCopyFlags.NONE);
        }
    }

    public async void delete_recursively (File file) throws Error {
        var src_type = file.query_file_type (FileQueryInfoFlags.NONE);

        if (src_type == FileType.DIRECTORY) {
            var enumerator = yield file.enumerate_children_async (
                FileAttribute.STANDARD_NAME, FileQueryInfoFlags.NONE
            );

            for (var info = enumerator.next_file (); info != null; info = enumerator.next_file ()) {
                var child_name = info.get_name ();
                var child = file.get_child (child_name);

                yield delete_recursively (child);
            }
        }

        yield file.delete_async ();
    }
}
