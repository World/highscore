// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.PlaceholderModel : Object, ListModel {
    public class Item : Object {
        public Object? object { get; construct; }
        public bool is_placeholder {
            get { return object == null; }
        }

        public Item (Object? object) {
            Object (object: object);
        }
    }

    public ListModel model { get; construct; }
    public bool placeholder_enabled { get; set; default = true; }

    public PlaceholderModel (ListModel model) {
        Object (model: model);
    }

    construct {
        model.items_changed.connect ((position, removed, added) => {
            uint offset = placeholder_enabled ? 1 : 0;

            items_changed (position + offset, removed, added);
        });

        notify["placeholder-enabled"].connect (() => {
            if (placeholder_enabled)
                items_changed (0, 0, 1);
            else
                items_changed (0, 1, 0);
        });
    }

    public Type get_item_type () {
        return typeof (Item);
    }

    public uint get_n_items () {
        uint offset = placeholder_enabled ? 1 : 0;
        return model.get_n_items () + offset;
    }

    public Object? get_item (uint position) {
        if (placeholder_enabled && position == 0)
            return new Item (null);

        uint offset = placeholder_enabled ? 1 : 0;
        var item = model.get_item (position - offset);
        if (item == null)
            return null;

        return new Item (item);
    }
}
