// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.WidgetUtils {
    public async void change_stack_child (Gtk.Stack stack, Gtk.Widget child) {
        if (stack.visible_child == child)
            return;

        Idle.add_once (() => {
            stack.visible_child = child;
        });

        var signal1 = stack.notify["transition-running"].connect (() => {
            if (stack.visible_child != child || stack.transition_running)
                return;

            change_stack_child.callback ();
        });
        var signal2 = stack.notify["visible-child"].connect (() => {
            if (Adw.get_enable_animations (stack) || stack.visible_child != child)
                return;

            change_stack_child.callback ();
        });

        yield;

        stack.disconnect (signal1);
        stack.disconnect (signal2);
    }

    public async void change_stack_child_by_name (Gtk.Stack stack, string name) {
        var child = stack.get_child_by_name (name);

        yield change_stack_child (stack, child);
    }

    public Adw.Dialog? get_visible_dialog (Gtk.Window window) {
        if (window is Adw.ApplicationWindow) {
            var w = window as Adw.ApplicationWindow;

            return w.visible_dialog;
        }

        if (window is Adw.Window) {
            var w = window as Adw.Window;

            return w.visible_dialog;
        }

        return null;
    }
}
