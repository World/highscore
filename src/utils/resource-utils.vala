// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.ResourceUtils {
    public static string load_to_string (string path) {
        try {
            var bytes = resources_lookup_data (path, NONE);

            return (string) bytes.get_data ();
        } catch (Error e) {
            error ("Can't load resource '%s': %s", path, e.message);
        }
    }

    public Menu load_menu (string path, string name = "menu") {
        var builder = new Gtk.Builder.from_resource (path);
        var menu = builder.get_object (name);

        if (menu == null)
            error ("Missing menu in %s", path);

        if (!(menu is Menu)) {
            error (
                "Invalid menu type %s in %s",
                menu.get_class ().get_name (), path
            );
        }

        return menu as Menu;
    }
}
