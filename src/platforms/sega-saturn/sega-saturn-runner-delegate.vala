// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.SegaSaturn.RunnerDelegate : Highscore.RunnerDelegate {
    private uint n_players;

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        n_players = yield proxy.get_players ();

        yield update_controllers ();
    }

    public override async void after_reset (bool hard) throws Error {
        yield update_controllers ();
    }

    public override async void load_state (SnapshotPlatformMetadata? metadata) throws Error {
        yield update_controllers ();
    }

    private async void update_controllers () throws Error {
        var proxy = runner.platform_proxy as Proxy;
        var metadata = runner.game.metadata as Metadata;

        bool supports_3d_pad = metadata.supports_3d_control_pad ();

        for (uint player = 0; player < n_players; player++) {
            if (supports_3d_pad) {
                yield proxy.set_controller (player, @3D_CONTROL_PAD);

                runner.input_buffer.modify_input_state (state => {
                    // TODO: Have a digital<->analog switch somewhere?
                    state->saturn.pad_mode[player] = ANALOG;
                });
            } else {
                yield proxy.set_controller (player, CONTROL_PAD);
            }

            runner.set_controller_type (player, supports_3d_pad ? "sega-saturn-3d-pad" : "sega-saturn");
        }
    }

    public override uint get_n_players () {
        return n_players;
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        var n_discs = runner.game.get_media ().length;
        if (n_discs < 2)
            return null;

        string[] discs = {};

        for (int i = 0; i < n_discs; i++)
            discs += _("Disc %u").printf (i + 1);

        var switcher = new MediaSwitcher (CD, _("Switch Disc"), discs);

        switcher.track_fullscreen (view);

        runner.bind_property (
            "current-media", switcher, "selected-media", SYNC_CREATE | BIDIRECTIONAL
        );

        return switcher;
    }

    public override OverlayMenuAddin? create_overlay_menu () {
        var n_discs = runner.game.get_media ().length;
        if (n_discs < 2)
            return null;

        string[] discs = {};

        for (int i = 0; i < n_discs; i++)
            discs += _("Disc %u").printf (i + 1);

        var switcher = new OverlayMenuMediaSwitcher ("psx", _("Switch Disc"), discs);

        runner.bind_property (
            "current-media", switcher, "selected-media", SYNC_CREATE | BIDIRECTIONAL
        );

        return switcher;
    }
}
