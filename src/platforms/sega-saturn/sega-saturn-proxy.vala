// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.SegaSaturn.Proxy : Object {
    public abstract async uint get_players () throws Error;
    public abstract async void set_controller (uint player, Hs.SegaSaturnController controller) throws Error;
    public abstract async void set_bios_path (Hs.SegaSaturnBios bios, string path) throws Error;
    public abstract async Hs.SegaSaturnBios get_used_bios () throws Error;
}
