// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.SegaSaturn.Metadata : Object, GameMetadata {
    public string product_number { get; set; }
    public string regions { get; set; }
    public string devices { get; set; }
    public string title { get; set; }

    public Metadata (string product_number, string regions, string devices, string title) {
        Object (product_number: product_number, regions: regions, devices: devices, title: title);
    }

    protected VariantType serialize_type () {
        return new VariantType.tuple ({
            VariantType.BYTESTRING,
            VariantType.BYTESTRING,
            VariantType.BYTESTRING,
            VariantType.BYTESTRING
        });
    }

    protected Variant serialize () {
        return new Variant.tuple ({
            new Variant.bytestring (product_number),
            new Variant.bytestring (regions),
            new Variant.bytestring (devices),
            new Variant.bytestring (title),
        });
    }

    protected void deserialize (Variant variant) {
        product_number = variant.get_child_value (0).get_bytestring ();
        regions = variant.get_child_value (1).get_bytestring ();
        devices = variant.get_child_value (2).get_bytestring ();
        title = variant.get_child_value (3).get_bytestring ();
    }

    public bool supports_3d_control_pad () {
        return devices.contains ("E") || devices.contains ("A");
    }
}
