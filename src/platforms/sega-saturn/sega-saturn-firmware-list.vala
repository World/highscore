// This file is part of Highscore. License: GPL-3.0-or-later
// vala-lint=skip-file

public class Highscore.SegaSaturn.FirmwareList : Highscore.FirmwareList {
    construct {
        var jp_firmware = new Firmware ("jp", "BIOS (Japan)");

        jp_firmware.filename = "saturn_bios_jp.bin";

        // Japanese BIOS v1.0
        jp_firmware.add_checksum (
            "af5828fdff51384f99b3c4926be27762",
            "917c44d22dd1e6e85e8ec0427eaf831ed6d36a09e4d6677d8f99e5ee40ac347185b9642b20dfde350e85a2060b4c93381aee78d9ca571c58329d0b85c3ed75d8"
        );
        // Japanese BIOS v1.003
        jp_firmware.add_checksum (
            "74570fed4d44b2682b560c8cd44b8b6a",
            "3000981f95b96b348dd9f0f5a3f56e1c79ea2fa39a138e8aad7045f5d2fb6ca28c9bd2b424c09b6bdb46861caef707fd5b90768b6b54731fa28b64ce4085f54d"
        );
        // Japanese BIOS v1.01
        jp_firmware.add_checksum (
            "85ec9ca47d8f6807718151cbcca8b964",
            "0516cc3deec97a6f2c0d1e08f68eec0331a90ba8697252357ea6b9a38974822a205860d7ff50f1cc345edd4bf7c41aa6ab1067c8cbc5aed3283b55641c1bc446"
        );

        add_firmware (jp_firmware);

        var us_eu_firmware = new Firmware ("us-eu", "BIOS (North America/Europe)");

        us_eu_firmware.filename = "saturn_bios_us_eu.bin";

        // North America / Europe BIOS v1.0
        us_eu_firmware.add_checksum (
            "f273555d7d91e8a5a6bfd9bcf066331c",
            "c478c97ae6276116c517e3768932505ad6a8c8af2064ca9cded439de246d5681252748e02197e4fb179204bb3eaf807076f09c542d70ed9bc02043f79526e5c5"
        );

        // North America / Europe BIOS v1.01 (commonly known as "mpr-17933")
        us_eu_firmware.add_checksum (
            "3240872c70984b6cbfda1586cab68dbe",
            "bb6bef1a0d710494a50776fb8c14ff7b5e87d2e177ac129846d77c8a5dd3395b71a3b24a057fae3014de5ae50acc056f45c3abd4bf1aa549b44b72eee145c25e"
        );

        add_firmware (us_eu_firmware);
    }

    public override async void load (string id, File file, Runner runner) throws Error {
        var proxy = runner.platform_proxy as Proxy;

        if (id == "jp")
            yield proxy.set_bios_path (JP, file.get_path ());
        if (id == "us-eu")
            yield proxy.set_bios_path (US_EU, file.get_path ());
    }

    public override async Firmware[] get_used_firmware (Runner runner) throws Error {
        var proxy = runner.platform_proxy as Proxy;

        switch (yield proxy.get_used_bios ()) {
            case JP:
                return { get_firmware ("jp") };
            case US_EU:
                return { get_firmware ("us-eu") };
            default:
                assert_not_reached ();
        }
    }
}
