// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.SegaSaturn.Controls : PlatformControls {
    construct {
        n_players = Hs.SEGA_SATURN_MAX_PLAYERS;

        var controller = new PlatformController ("sega-saturn", _("Control Pad"));
        controller.add_sections (
            "dpad", "face-buttons", "face-buttons-xyz",
            "shoulder-buttons", "menu", null
        );

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Directional Button"),
            Hs.SegaSaturnButton.UP,
            Hs.SegaSaturnButton.DOWN,
            Hs.SegaSaturnButton.LEFT,
            Hs.SegaSaturnButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("Button A"), Hs.SegaSaturnButton.A,
            "b", _("Button B"), Hs.SegaSaturnButton.B,
            "c", _("Button C"), Hs.SegaSaturnButton.C,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons-xyz",
            "x", _("Button X"), Hs.SegaSaturnButton.X,
            "y", _("Button Y"), Hs.SegaSaturnButton.Y,
            "z", _("Button Z"), Hs.SegaSaturnButton.Z,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "shoulder-buttons",
            "l", _("Button L"), Hs.SegaSaturnButton.L,
            "r", _("Button R"), Hs.SegaSaturnButton.R,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "start", _("Start Button"), Hs.SegaSaturnButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_A,     "a");
        kbd_mapping.map (Linux.Input.KEY_S,     "b");
        kbd_mapping.map (Linux.Input.KEY_D,     "c");
        kbd_mapping.map (Linux.Input.KEY_Q,     "x");
        kbd_mapping.map (Linux.Input.KEY_W,     "y");
        kbd_mapping.map (Linux.Input.KEY_E,     "z");
        kbd_mapping.map (Linux.Input.KEY_Z,     "l");
        kbd_mapping.map (Linux.Input.KEY_X,     "r");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (WEST,   "a");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "c");
        gamepad_mapping.map_button (NORTH,  "z");
        gamepad_mapping.map_button (L,      "x");
        gamepad_mapping.map_button (R,      "y");
        gamepad_mapping.map_button (L2,     "l");
        gamepad_mapping.map_button (R2,     "r");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        controller = new PlatformController (
            "sega-saturn-3d-pad",
            _("3D Control Pad"),
            "platform-sega-saturn-3d-pad-symbolic"
        );

        controller.add_sections (
            "stick", "dpad", "face-buttons", "face-buttons-xyz",
            "triggers", "menu", null
        );

        var stick = new StickControl ("stick", _("3D Directional Pad"));
        stick.moved.connect (stick_moved_cb);
        controller.add_control ("stick", stick);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Directional Pad"),
            Hs.SegaSaturnButton.UP,
            Hs.SegaSaturnButton.DOWN,
            Hs.SegaSaturnButton.LEFT,
            Hs.SegaSaturnButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("Button A"), Hs.SegaSaturnButton.A,
            "b", _("Button B"), Hs.SegaSaturnButton.B,
            "c", _("Button C"), Hs.SegaSaturnButton.C,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons-xyz",
            "x", _("Button X"), Hs.SegaSaturnButton.X,
            "y", _("Button Y"), Hs.SegaSaturnButton.Y,
            "z", _("Button Z"), Hs.SegaSaturnButton.Z,
            null
        );

        var l_trigger = new TriggerControl ("l", _("Left Trigger"));
        l_trigger.moved.connect (l_trigger_moved_cb);
        controller.add_control ("triggers", l_trigger);

        var r_trigger = new TriggerControl ("r", _("Right Trigger"));
        r_trigger.moved.connect (r_trigger_moved_cb);
        controller.add_control ("triggers", r_trigger);

        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "start", _("Start Button"), Hs.SegaSaturnButton.START,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "stick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "stick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "stick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "stick:right");
        kbd_mapping.map (Linux.Input.KEY_I,     "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_K,     "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_J,     "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_L,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_A,     "a");
        kbd_mapping.map (Linux.Input.KEY_S,     "b");
        kbd_mapping.map (Linux.Input.KEY_D,     "c");
        kbd_mapping.map (Linux.Input.KEY_Q,     "x");
        kbd_mapping.map (Linux.Input.KEY_W,     "y");
        kbd_mapping.map (Linux.Input.KEY_E,     "z");
        kbd_mapping.map (Linux.Input.KEY_Z,     "l");
        kbd_mapping.map (Linux.Input.KEY_X,     "r");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "start");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (WEST,   "a");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "c");
        gamepad_mapping.map_button (NORTH,  "z");
        gamepad_mapping.map_button (L,      "x");
        gamepad_mapping.map_button (R,      "y");
        gamepad_mapping.map_button (L2,     "l");
        gamepad_mapping.map_button (R2,     "r");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "stick");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        default_controller = "sega-saturn";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->saturn.pad_buttons[player], button, pressed);
        });
    }

    private static void stick_moved_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->saturn.pad_stick_x[player] = x;
            state->saturn.pad_stick_y[player] = y;
        });
    }

    private static void l_trigger_moved_cb (Runner runner, uint player, double pressure) {
        runner.input_buffer.modify_input_state (state => {
            state->saturn.pad_left_trigger[player] = pressure;
        });
    }

    private static void r_trigger_moved_cb (Runner runner, uint player, double pressure) {
        runner.input_buffer.modify_input_state (state => {
            state->saturn.pad_right_trigger[player] = pressure;
        });
    }
}
