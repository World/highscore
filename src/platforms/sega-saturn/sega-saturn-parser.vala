// This file is part of Highscore. License: GPL-3.0-or-later

// Reference: http://koti.kapsi.fi/~antime/sega/files/ST-040-R4-051795.pdf
public class Highscore.SegaSaturn.Parser : GameParser {
    private static Regex? device_info_regex;

    private string[] data_paths;
    private string checksum;
    private string product_number;
    private string regions;
    private string devices;
    private string title;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    private void extract_media_set_info (string device_info) {
        if (device_info_regex == null) {
            try {
                device_info_regex = new Regex ("CD-([0-9]+)/([0-9]+)");
            } catch (Error e) {
                error (e.message);
            }
        }

        MatchInfo info;
        if (device_info_regex.match (device_info, 0, out info)) {
            string disc_number_str = info.fetch (1);
            string discs_total_str = info.fetch (2);

            int disc_number = int.parse (disc_number_str);
            int discs_total = int.parse (discs_total_str);

            if (disc_number > 0 && discs_total > 0 && disc_number <= discs_total) {
                if (discs_total == 1) {
                    // Single-disc game, we're done
                    return;
                }

                var current_disc = @"$product_number-$disc_number";

                string[] disc_set = {};
                for (int i = 1; i <= discs_total; i++)
                    disc_set += @"$product_number-$i";

                set_incomplete (current_disc, disc_set);

                return;
            }
        }

        warning (
            "Invalid device info for '%s': '%s'",
            file.get_path (),
            device_info
        );
    }

    public override bool parse () throws Error {
        var context = Object.new (typeof (Mirage.Context)) as Mirage.Context;
        var disc = context.load_image ({ file.get_path () });

        if (disc.get_medium_type () != CD) {
            parser_debug ("Not a CD, discarding");
            return false;
        }

        if (disc.get_number_of_tracks () < 1) {
            parser_debug ("Game must have at least 1 track, discarding");
            return false;
        }

        var track = disc.get_track_by_index (0);
        if (track.get_sector_type () != MODE1) {
            parser_debug ("Track 1 is not Mode 1, discarding");
            return false;
        }

        var parser = new ISO9660.Parser (track);
        uint8[] system_area = parser.get_system_area ();

        var hw_id = ISO9660.sanitize_string ((char[]) &system_area[0], 16);
        if (hw_id != "SEGA SEGASATURN") {
            parser_debug ("Wrong magic string, discarding");
            return false;
        }

        product_number = ISO9660.sanitize_string ((char[]) &system_area[0x20], 10);
        var device_info = ISO9660.sanitize_string ((char[]) &system_area[0x38], 8);

        extract_media_set_info (device_info);

        regions = ISO9660.sanitize_string ((char[]) &system_area[0x40], 10);
        devices = ISO9660.sanitize_string ((char[]) &system_area[0x50], 16);
        title = ISO9660.sanitize_string ((char[]) &system_area[0x60], 112);

        data_paths = CdUtils.get_disc_extra_paths (file.get_path (), disc);
        checksum = CdUtils.get_disc_checksum (disc);

        parser_debug (
            "Found metadata: product number '%s', regions '%s', devices '%s', title '%s'",
            product_number, regions, devices, title
        );

        return true;
    }

    public override string get_game_uid (Media[] media) throws Error {
        var prefix = @"$(platform.id)-";
        var builder = new StringBuilder ();

        foreach (var m in media) {
            var checksum = m.uid.replace (prefix, "");

            builder.append (checksum);
        }

        var checksum = Checksum.compute_for_string (
            MD5, builder.free_and_steal ()
        );

        return @"$prefix$checksum";
    }

    public override string get_media_uid () throws Error {
        return @"$(platform.id)-$checksum";
    }

    public override string[]? get_extra_paths () {
        return data_paths;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (product_number, regions, devices, title);
    }
}
