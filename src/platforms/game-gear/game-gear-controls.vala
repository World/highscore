// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameGear.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("game-gear", _("Game Gear"));
        controller.add_sections ("dpad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Directional Button"),
            Hs.GameGearButton.UP,
            Hs.GameGearButton.DOWN,
            Hs.GameGearButton.LEFT,
            Hs.GameGearButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "one", _("Button 1"), Hs.GameGearButton.ONE,
            "two", _("Button 2"), Hs.GameGearButton.TWO,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "start", _("Start Button"), Hs.GameGearButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "one");
        kbd_mapping.map (Linux.Input.KEY_D,     "two");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH, "one");
        gamepad_mapping.map_button (EAST,  "two");
        gamepad_mapping.map_button (START, "start");
        gamepad_mapping.map_stick  (LEFT,  "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "game-gear";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->game_gear.buttons, button, pressed);
        });
    }
}
