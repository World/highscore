// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameGear.Metadata : Object, GameMetadata {
    public uint64 id { get; set; }

    public Metadata (uint64 id) {
        Object (id: id);
    }

    protected VariantType serialize_type () {
        return VariantType.UINT64;
    }

    protected Variant serialize () {
        return id;
    }

    protected void deserialize (Variant variant) {
        id = variant.get_uint64 ();
    }
}
