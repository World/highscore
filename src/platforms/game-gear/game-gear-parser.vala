// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://www.smspower.org/Development/ROMHeader
public class Highscore.GameGear.Parser : GameParser {
    private const size_t OFFSET1 = 0x1FF0;
    private const size_t OFFSET2 = 0x3FF0;
    private const size_t OFFSET3 = 0x7FF0;
    private const size_t HEADER_SIZE = 0x10;
    private const string MAGIC = "TMR SEGA";
    private const size_t ID_LENGTH = 8;

    private uint64 id;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    private bool parse_header (DataInputStream stream, size_t offset) throws Error {
        stream.seek (offset, SET);

        var magic_buffer = new uint8[MAGIC.length];
        stream.read (magic_buffer);

        for (int i = 0; i < MAGIC.length; i++) {
            if (magic_buffer[i] != ((uint8[]) MAGIC)[i])
                return false;
        }

        var id_buffer = new uint8[ID_LENGTH];
        stream.read (id_buffer);

        uint64 id = 0;

        for (var i = 0; i < ID_LENGTH; i++)
            id |= (uint64) id_buffer[i] << ((ID_LENGTH - i - 1) * 8);

        this.id = id;

        parser_debug ("Found ID: 0x%lX", id);

        return true;
    }

    public override bool parse () throws Error {
        var stream = new DataInputStream (file.read ());

        var info = file.query_info (FileAttribute.STANDARD_SIZE, NONE);
        int64 size = info.get_size ();

        if (size >= OFFSET3 + HEADER_SIZE && parse_header (stream, OFFSET3))
            return true;

        if (size >= OFFSET2 + HEADER_SIZE && parse_header (stream, OFFSET2))
            return true;

        if (size >= OFFSET1 + HEADER_SIZE && parse_header (stream, OFFSET1))
            return true;

        id = 0;

        parser_debug ("Missing header");

        return true;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (id);
    }
}
