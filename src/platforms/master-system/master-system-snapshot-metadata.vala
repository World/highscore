// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.MasterSystem.SnapshotMetadata : SnapshotPlatformMetadata {
    public bool enable_fm_audio { get; set; }

    public override void load (KeyFile keyfile) {
        try {
            enable_fm_audio = keyfile.get_boolean ("Master System", "FM Audio");
        } catch (Error e) {
            enable_fm_audio = true;
        }
    }

    public override void save (KeyFile keyfile) {
        keyfile.set_boolean ("Master System", "FM Audio", enable_fm_audio);
    }
}
