// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.MasterSystem.Overrides {
    private const uint64[] LIGHT_PHASER_GAMES = {
        0x000044E90160004F, // Hang-On & Safari Hunt (USA) (Beta)
        0x000057120360004F, // Marksman Shooting & Trap Shooting (USA)
        0x000099760160014F, // [BIOS] Hang-On & Safari Hunt (USA, Europe) (v2.4)
        0x0000B75574700040, // Laser Ghost (Europe)
        0x0000CEEE0160004F, // Hang-On & Safari Hunt (USA)
        0x0000E3328060004F, // Marksman Shooting & Trap Shooting & Safari Hunt (Europe)
        0x19871BC97450004F, // Gangster Town (USA, Europe)
        0x20200D1039700040, // Operation Wolf (Europe)
        0x20208A3A7250004F, // Shooting Gallery (USA, Europe)
        0x2020FBD30651004F, // Rescue Mission (USA, Europe)
        0x414C154A0180004F, // Missile Defense 3-D (USA, Europe)
        0xFFFF125434700040, // Assault City (Europe)
        0xFFFF126D15700040, // Rambo III (USA, Europe)
        0xFFFF90E8FFFFFF4F, // Wanted (USA, Europe)
        0xFFFF9D9910900040, // Space Gun (Europe)
        0xFFFF9F7434700040, // Assault City (Europe) (Light Phaser)
    };

    private inline bool find_light_phaser_override (uint64 id) {
        void *ret = Posix.bsearch (
            &id,
            LIGHT_PHASER_GAMES,
            LIGHT_PHASER_GAMES.length,
            sizeof (uint64),
            (a, b) => {
                var needle = (uint64?) a;
                var override = (uint64?) b;

                if (needle < override)
                    return -1;
                if (needle > override)
                    return 1;
                return 0;
            }
        );

        return (uint64?) ret == id;
    }

    public bool supports_light_phaser (uint64 id) {
        if (id == 0)
            return false;

        return find_light_phaser_override (id);
    }
}
