// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.MasterSystem.RunnerDelegate : Highscore.RunnerDelegate {
    private bool enable_fm_audio;
    private uint n_players;

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        n_players = yield proxy.get_players ();

        yield reset_fm_audio ();

        var metadata = runner.game.metadata as Metadata;

        if (metadata.supports_light_phaser ()) {
            runner.set_controller_type (0, "master-system-light-phaser");

            yield proxy.set_enable_light_phaser (true);
        }
    }

    public override async void before_reset (bool hard) throws Error {
        yield reset_fm_audio ();
    }

    private async void reset_fm_audio () throws Error {
        var settings = new GameSettings (runner.game).get_platform ();

        enable_fm_audio = settings.get_boolean ("fm-audio");

        var proxy = runner.platform_proxy as Proxy;
        yield proxy.set_enable_fm_audio (enable_fm_audio);
    }

    public override async void load_state (SnapshotPlatformMetadata? metadata) throws Error {
        var platform_metadata = metadata as SnapshotMetadata;

        enable_fm_audio = platform_metadata.enable_fm_audio;

        var proxy = runner.platform_proxy as Proxy;
        yield proxy.set_enable_fm_audio (enable_fm_audio);
    }

    public override async void save_state (SnapshotPlatformMetadata metadata) throws Error {
        var platform_metadata = metadata as SnapshotMetadata;

        platform_metadata.enable_fm_audio = enable_fm_audio;
    }

    public override uint get_n_players () {
        return n_players;
    }
}
