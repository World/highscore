// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.MasterSystem.Controls : PlatformControls {
    construct {
        n_players = Hs.MASTER_SYSTEM_MAX_PLAYERS;

        var controller = new PlatformController ("master-system", _("Control Pad"));
        controller.add_sections ("dpad", "face-buttons", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Directional Button"),
            Hs.MasterSystemButton.UP,
            Hs.MasterSystemButton.DOWN,
            Hs.MasterSystemButton.LEFT,
            Hs.MasterSystemButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "one", _("Button 1"), Hs.MasterSystemButton.ONE,
            "two", _("Button 2"), Hs.MasterSystemButton.TWO,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "one");
        kbd_mapping.map (Linux.Input.KEY_D,     "two");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH, "one");
        gamepad_mapping.map_button (EAST,  "two");
        gamepad_mapping.map_stick  (LEFT,  "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // light phaser

        controller = new PlatformController (
            "master-system-light-phaser",
            _("Light Phaser"),
            "platform-master-system-light-phaser-symbolic"
        );
        controller.add_section ("phaser");

        var phaser = new PointerControl ("phaser", _("Light Phaser"));
        phaser.pressed.connect (phaser_fire_cb);
        phaser.moved.connect (phaser_fire_cb);
        phaser.released.connect (phaser_stop_cb);
        controller.add_control ("phaser", phaser);

        controller.set_for_screen (Highscore.ScreenSet.DEFAULT_SCREEN, phaser);

        kbd_mapping = new PlatformKeyboardMapping ();
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // global

        global_controller = new PlatformController ("console", _("Built-in"));
        global_controller.add_section ("system");

        var pause_button = new ButtonControl ("pause", _("Pause Button"));
        pause_button.activate.connect ((runner, player, pressed) => {
            set_pause_button_pressed (runner, pressed);
        });
        global_controller.add_control ("system", pause_button);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_ENTER, "pause");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (START, "pause");
        global_controller.default_gamepad_mapping = gamepad_mapping;

        default_controller = "master-system";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->master_system.pad_buttons[player], button, pressed);
        });
    }

    private static void phaser_fire_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->master_system.light_phaser_fire = true;
            state->master_system.light_phaser_x = x;
            state->master_system.light_phaser_y = y;
        });
    }

    private static void phaser_stop_cb (Runner runner, uint player) {
        runner.input_buffer.modify_input_state (state => {
            state->master_system.light_phaser_fire = false;
        });
    }

    private void set_pause_button_pressed (Runner runner, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->master_system.pause_button = pressed;
        });
    }
}
