// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/master-system/master-system-properties.ui")]
public class Highscore.MasterSystem.Properties : PlatformProperties {
    private Settings settings;

    public bool fm_audio { get; set; }

    construct {
        settings = new GameSettings (game).get_platform ();

        settings.bind ("fm-audio", this, "fm-audio", DEFAULT);
    }
}
