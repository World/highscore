// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.MasterSystem.Proxy : Object {
    public abstract async uint get_players () throws Error;
    public abstract async void set_enable_fm_audio (bool enable_fm_audio) throws Error;
    public abstract async void set_enable_light_phaser (bool enable_light_phaser) throws Error;
}
