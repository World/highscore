// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Atari2600.RunnerDelegate : Highscore.RunnerDelegate {
    private Hs.Atari2600Difficulty difficulty[Hs.ATARI_2600_MAX_PLAYERS];

    private Hs.Atari2600TVType _tv_type;
    public Hs.Atari2600TVType tv_type {
        get { return _tv_type; }
        set {
            if (tv_type == value)
                return;

            _tv_type = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_2600.tv_type = tv_type;
            });
        }
    }

    public Hs.Atari2600Difficulty left_difficulty {
        get { return difficulty[0]; }
        set {
            if (left_difficulty == value)
                return;

            difficulty[0] = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_2600.difficulty[0] = left_difficulty;
            });
        }
    }

    public Hs.Atari2600Difficulty right_difficulty {
        get { return difficulty[1]; }
        set {
            if (right_difficulty == value)
                return;

            difficulty[1] = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_2600.difficulty[1] = right_difficulty;
            });
        }
    }

    private uint n_players;

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        var left_controller = yield proxy.get_controller (0);
        var left_type = get_controller_type (left_controller);

        var right_controller = yield proxy.get_controller (1);
        var right_type = get_controller_type (right_controller);

        // There are two paddles in each port, so we present them as two players

        int port = 0;
        runner.set_controller_type (port++, left_type);

        if (left_controller == PADDLES)
            runner.set_controller_type (port++, left_type);

        runner.set_controller_type (port++, right_type);

        if (right_controller == PADDLES)
            runner.set_controller_type (port++, right_type);

        n_players = (left_controller == PADDLES) ? 2 : 1;

        if (right_controller == PADDLES)
            n_players += 2;
        else if (right_controller != NONE)
            n_players++;

        tv_type = COLOR;
        left_difficulty = yield proxy.get_default_difficulty (0);
        right_difficulty = yield proxy.get_default_difficulty (1);
    }

    private string? get_controller_type (Hs.Atari2600Controller controller) {
        switch (controller) {
            case NONE:
                return null;
            case JOYSTICK:
                return "atari-2600";
            case GENESIS:
                return "atari-2600-2button";
            case THREE_BUTTON:
                return "atari-2600-3button";
            case DRIVING:
                return "atari-2600-driving";
            case PADDLES:
                return "atari-2600-paddle";
            default:
                return null;
        }
    }

    public override async void after_reset (bool hard) throws Error {
        if (hard) {
            var proxy = runner.platform_proxy as Proxy;

            tv_type = COLOR;
            left_difficulty = yield proxy.get_default_difficulty (0);
            right_difficulty = yield proxy.get_default_difficulty (1);
        }
    }

    public override async void load_state (SnapshotPlatformMetadata? metadata) throws Error {
        var platform_metadata = metadata as SnapshotMetadata;

        tv_type = platform_metadata.tv_type;
        left_difficulty = platform_metadata.left_difficulty;
        right_difficulty = platform_metadata.right_difficulty;
    }

    public override async void save_state (SnapshotPlatformMetadata metadata) throws Error {
        var platform_metadata = metadata as SnapshotMetadata;

        platform_metadata.tv_type = tv_type;
        platform_metadata.left_difficulty = left_difficulty;
        platform_metadata.right_difficulty = right_difficulty;
    }

    public override uint get_n_players () {
        return n_players;
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        var menu = new SwitchMenu ();

        menu.track_fullscreen (view);

        bind_property (
            "tv-type", menu, "tv-type", BIDIRECTIONAL | SYNC_CREATE
        );
        bind_property (
            "left-difficulty", menu, "left-difficulty", BIDIRECTIONAL | SYNC_CREATE
        );
        bind_property (
            "right-difficulty", menu, "right-difficulty", BIDIRECTIONAL | SYNC_CREATE
        );
        menu.select.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.flick_select.begin ();
        });
        menu.reset.connect (() => {
            runner.reset.begin (false);
        });

        return menu;
    }
}
