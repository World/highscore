// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Atari2600.SnapshotMetadata : SnapshotPlatformMetadata {
    public Hs.Atari2600TVType tv_type { get; set; }
    public Hs.Atari2600Difficulty left_difficulty { get; set; }
    public Hs.Atari2600Difficulty right_difficulty { get; set; }

    public override void load (KeyFile keyfile) {
        string? tv_type_str;

        try {
            tv_type_str = keyfile.get_string ("Atari 2600", "TV Type");
        } catch (Error e) {
            tv_type_str = "Color";
        }

        if (tv_type_str == "Color") {
            tv_type = COLOR;
        } else if (tv_type_str == "Black & White") {
            tv_type = BLACK_WHITE;
        } else {
            warning ("Unsupported Atari 2600 TV type: %s", tv_type_str);
            tv_type = COLOR;
        }

        string[] names = { "Left", "Right" };
        for (int i = 0; i < Hs.ATARI_2600_MAX_PLAYERS; i++) {
            var name = names[i];
            Hs.Atari2600Difficulty diff;
            string? diff_str;

            try {
                diff_str = keyfile.get_string ("Atari 2600", @"$name Difficulty");
            } catch (Error e) {
                diff_str = "Advanced";
            }

            if (diff_str == "Advanced") {
                diff = ADVANCED;
            } else if (diff_str == "Beginner") {
                diff = BEGINNER;
            } else {
                warning ("Unsupported Atari 2600 difficulty: %s", diff_str);
                diff = ADVANCED;
            }

            if (i == 0)
                left_difficulty = diff;
            else
                right_difficulty = diff;
        }
    }

    public override void save (KeyFile keyfile) {
        var tv_type_str = "";

        switch (tv_type) {
            case COLOR:
                tv_type_str = "Color";
                break;
            case BLACK_WHITE:
                tv_type_str = "Black & White";
                break;
            default:
                assert_not_reached ();
        }

        keyfile.set_string ("Atari 2600", "TV Type", tv_type_str);

        string[] names = { "Left", "Right" };
        Hs.Atari2600Difficulty[] difficulty = {
            left_difficulty, right_difficulty
        };

        for (int i = 0; i < Hs.ATARI_2600_MAX_PLAYERS; i++) {
            var name = names[i];
            var diff_str = "";

            switch (difficulty[i]) {
                case ADVANCED:
                    diff_str = "Advanced";
                    break;
                case BEGINNER:
                    diff_str = "Beginner";
                    break;
                default:
                    assert_not_reached ();
            }

            keyfile.set_string ("Atari 2600", @"$name Difficulty", diff_str);
        }
    }
}
