// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/atari-2600/atari-2600-switch-menu.ui")]
public class Highscore.Atari2600.SwitchMenu : Adw.Bin {
    [GtkChild]
    private unowned Gtk.MenuButton button;
    [GtkChild]
    private unowned SwitchIcon icon;

    public Hs.Atari2600TVType tv_type { get; set; }
    public Hs.Atari2600Difficulty left_difficulty { get; set; }
    public Hs.Atari2600Difficulty right_difficulty { get; set; }

    public signal void select ();
    public signal void reset ();

    construct {
        notify["tv-type"].connect (() => {
            icon.set_switch_state (0, tv_type == BLACK_WHITE);
        });
        notify["left-difficulty"].connect (() => {
            icon.set_switch_state (1, left_difficulty == BEGINNER);
        });
        notify["right-difficulty"].connect (() => {
            icon.set_switch_state (2, right_difficulty == BEGINNER);
        });
    }

    static construct {
        install_property_action ("atari2600.tv-type", "tv-type");
        install_property_action ("atari2600.left-difficulty", "left-difficulty");
        install_property_action ("atari2600.right-difficulty", "right-difficulty");
        install_action ("atari2600.select", null, widget => {
            var self = widget as SwitchMenu;
            self.select ();
        });
        install_action ("atari2600.reset", null, widget => {
            var self = widget as SwitchMenu;
            self.reset ();
        });
    }

    public void track_fullscreen (FullscreenView view) {
        view.track_menu_button (button);
    }
}
