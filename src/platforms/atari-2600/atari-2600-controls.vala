// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Atari2600.Controls : PlatformControls {
    private const double PADDLE_SPEED = 4;

    construct {
        // There are two paddles for each slot, so there are twice as many players
        n_players = Hs.ATARI_2600_MAX_PLAYERS * 2;

        var controller = new PlatformController ("atari-2600", _("Joystick Controller"));
        controller.add_sections ("joystick", "buttons", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joystick", _("Joystick"),
            Hs.Atari2600JoystickButton.UP,
            Hs.Atari2600JoystickButton.DOWN,
            Hs.Atari2600JoystickButton.LEFT,
            Hs.Atari2600JoystickButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "buttons",
            "button", _("Button"), Hs.Atari2600JoystickButton.FIRE,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joystick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joystick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joystick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joystick:right");
        kbd_mapping.map (Linux.Input.KEY_D,     "button");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("joystick");
        gamepad_mapping.map_button (EAST, "button");
        gamepad_mapping.map_stick  (LEFT, "joystick");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "atari-2600";

        // genesis controller (2 buttons)

        controller = new PlatformController (
            "atari-2600-2button",
            _("Sega Genesis Controller"),
            "platform-mega-drive-symbolic"
        );
        controller.add_sections ("joystick", "buttons", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joystick", _("Joystick"),
            Hs.Atari2600JoystickButton.UP,
            Hs.Atari2600JoystickButton.DOWN,
            Hs.Atari2600JoystickButton.LEFT,
            Hs.Atari2600JoystickButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "buttons",
            "one", _("Button 1"), Hs.Atari2600JoystickButton.FIRE,
            "two", _("Button 2"), Hs.Atari2600JoystickButton.FIRE_5,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joystick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joystick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joystick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joystick:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "one");
        kbd_mapping.map (Linux.Input.KEY_D,     "two");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("joystick");
        gamepad_mapping.map_button (SOUTH, "one");
        gamepad_mapping.map_button (EAST,  "two");
        gamepad_mapping.map_stick  (LEFT,  "joystick");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // 3-button joysticks (joy2bplus and boostergrip)

        controller = new PlatformController (
            "atari-2600-3button",
            _("Three-Button Joystick"),
            "platform-atari-2600-3button-symbolic"
        );
        controller.add_sections ("joystick", "buttons", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joystick", _("Joystick"),
            Hs.Atari2600JoystickButton.UP,
            Hs.Atari2600JoystickButton.DOWN,
            Hs.Atari2600JoystickButton.LEFT,
            Hs.Atari2600JoystickButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "buttons",
            "one",   _("Button 1"), Hs.Atari2600JoystickButton.FIRE,
            "two",   _("Button 2"), Hs.Atari2600JoystickButton.FIRE_5,
            "three", _("Button 3"), Hs.Atari2600JoystickButton.FIRE_9,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joystick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joystick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joystick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joystick:right");
        kbd_mapping.map (Linux.Input.KEY_A,     "one");
        kbd_mapping.map (Linux.Input.KEY_S,     "two");
        kbd_mapping.map (Linux.Input.KEY_D,     "three");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("joystick");
        gamepad_mapping.map_button (EAST,  "one");
        gamepad_mapping.map_button (SOUTH, "two");
        gamepad_mapping.map_button (WEST,  "three");
        gamepad_mapping.map_stick  (LEFT,  "joystick");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // paddle

        controller = new PlatformController (
            "atari-2600-paddle",
            _("Paddle Controller"),
            "platform-atari-2600-paddle-symbolic"
        );
        controller.add_sections ("knob", "buttons", null);

        var knob = new RotaryControl ("knob", _("Knob"));
        knob.rotated.connect (paddle_rotated_cb);
        controller.add_control ("knob", knob);

        var button = new ButtonControl ("button", _("Button"));
        button.activate.connect (paddle_button_cb);
        controller.add_control ("buttons", button);

        add_controller (controller);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "knob:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "knob:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "button");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_stick  (LEFT,  "knob");
        gamepad_mapping.map_button (LEFT,  "knob:left");
        gamepad_mapping.map_button (RIGHT, "knob:right");
        gamepad_mapping.map_button (SOUTH, "button");
        controller.default_gamepad_mapping = gamepad_mapping;

        // driving

        controller = new PlatformController (
            "atari-2600-driving",
            _("Driving Controller"),
            "platform-atari-2600-paddle-symbolic"
        );
        controller.add_sections ("knob", "buttons", null);

        var driving = new RotaryControl ("knob", _("Knob"));
        driving.rotated.connect (driving_rotated_cb);
        controller.add_control ("knob", driving);

        button = new ButtonControl ("button", _("Button"));
        button.activate.connect (driving_button_cb);
        controller.add_control ("buttons", button);

        add_controller (controller);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "knob:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "knob:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "button");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_stick  (LEFT,  "knob");
        gamepad_mapping.map_button (LEFT,  "knob:left");
        gamepad_mapping.map_button (RIGHT, "knob:right");
        gamepad_mapping.map_button (SOUTH, "button");
        controller.default_gamepad_mapping = gamepad_mapping;

        // global

        global_controller = new PlatformController ("console", _("Built-in"));
        global_controller.add_section ("system");

        button = new ButtonControl ("tv-type", _("TV Type"));
        button.activate.connect (tv_type_cb);
        global_controller.add_control ("system", button);

        button = new ButtonControl ("left-difficulty", _("Left Difficulty"));
        button.activate.connect (left_difficulty_cb);
        global_controller.add_control ("system", button);

        button = new ButtonControl ("right-difficulty", _("Right Difficulty"));
        button.activate.connect (right_difficulty_cb);
        global_controller.add_control ("system", button);

        button = new ButtonControl ("select", _("Game Select"));
        button.activate.connect (select_cb);
        global_controller.add_control ("system", button);

        button = new ButtonControl ("reset", _("Game Reset"));
        button.activate.connect (reset_cb);
        global_controller.add_control ("system", button);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_1,         "tv-type");
        kbd_mapping.map (Linux.Input.KEY_2,         "left-difficulty");
        kbd_mapping.map (Linux.Input.KEY_3,         "right-difficulty");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "reset");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (L2,     "tv-type");
        gamepad_mapping.map_button (L,      "left-difficulty");
        gamepad_mapping.map_button (R,      "right-difficulty");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "reset");
        global_controller.default_gamepad_mapping = gamepad_mapping;
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        if (player > Hs.ATARI_2600_MAX_PLAYERS)
            return;

       runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->atari_2600.joystick[player], button, pressed);
        });
    }

    private static void tv_type_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.tv_type == COLOR)
            delegate.tv_type = BLACK_WHITE;
        else
            delegate.tv_type = COLOR;
    }

    private static void left_difficulty_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.left_difficulty == ADVANCED)
            delegate.left_difficulty = BEGINNER;
        else
            delegate.left_difficulty = ADVANCED;
    }

    private static void right_difficulty_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.right_difficulty == ADVANCED)
            delegate.right_difficulty = BEGINNER;
        else
            delegate.right_difficulty = ADVANCED;
    }

    private static void select_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.select_switch = pressed;
        });
    }

    private static void reset_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            runner.reset.begin (false);
    }

    private void paddle_rotated_cb (Runner runner, uint player, double delta) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.paddle_speed[player] = delta * PADDLE_SPEED;
            state->atari_2600.paddle_axis[player] = double.NAN;
        });
    }

    private void paddle_button_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.paddle_fire[player] = pressed;
        });
    }

    private void driving_rotated_cb (Runner runner, uint player, double delta) {
        if (player > Hs.ATARI_2600_MAX_PLAYERS)
            return;

        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.driving_axis[player] = delta;
        });
    }

    private void driving_button_cb (Runner runner, uint player, bool pressed) {
        if (player > Hs.ATARI_2600_MAX_PLAYERS)
            return;

        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.driving_fire[player] = pressed;
        });
    }
}
