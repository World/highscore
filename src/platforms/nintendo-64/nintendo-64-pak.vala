// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.Nintendo64.Pak {
    public Hs.Nintendo64Pak from_string (string str) {
        if (str == "none")
            return NONE;
        if (str == "memory-pak")
            return MEMORY_PAK;
        if (str == "rumble-pak")
            return RUMBLE_PAK;

        critical ("Unknown Nintendo 64 pak: %s", str);
        return NONE;
    }

    public string to_string (Hs.Nintendo64Pak model) {
        switch (model) {
            case NONE:
                return "none";
            case MEMORY_PAK:
                return "memory-pak";
            case RUMBLE_PAK:
                return "rumble-pak";
            default:
                assert_not_reached ();
        }
    }

    public string to_display_name (Hs.Nintendo64Pak model) {
        switch (model) {
            case NONE:
                return _("None");
            case MEMORY_PAK:
                return _("Controller Pak");
            case RUMBLE_PAK:
                return _("Rumble Pak");
            default:
                assert_not_reached ();
        }
    }

    public Hs.Nintendo64Pak get_auto_for_game (Game game, uint player) {
        var metadata = game.metadata as Metadata;

        if (player > metadata.players)
            return NONE;

        var supported_paks = metadata.get_paks (player);

        if ((supported_paks & SupportedPaks.MEMORY_PAK) > 0)
            return MEMORY_PAK;

        if ((supported_paks & SupportedPaks.RUMBLE_PAK) > 0)
            return RUMBLE_PAK;

        return NONE;
    }

    public void bind_settings (
        Settings settings,
        string key,
        Object obj,
        string prop,
        SettingsBindFlags flags
    ) {
        settings.bind_with_mapping (
            key, obj, prop, flags,
            (value, variant, user_data) => {
                var str = variant.get_string ();
                var pak = from_string (str);

                value = (Hs.Nintendo64Pak) pak;

                return true;
            },
            (value, variant_type, user_data) => {
                var pak = (Hs.Nintendo64Pak) value.get_enum ();
                return to_string (pak);
            },
            null, null
        );
    }
}
