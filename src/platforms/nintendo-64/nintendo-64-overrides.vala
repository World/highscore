// This file is part of Highscore. License: GPL-3.0-or-later

// This file is generated, see tools/generate-n64-db.py
namespace Highscore.Nintendo64.Overrides {
    public struct GameDataMD5 {
        string md5;
        uint8 players;
        SupportedPaks paks;
    }

    public struct GameDataCRC {
        uint64 crc;
        uint8 players;
        SupportedPaks paks;
    }

    public const GameDataMD5[] GAME_DATA_MD5 = {
        { "000364bac80e41d9060a31a5923874b7", 2, 0 }, // Pokemon Puzzle League (G) [!]
        { "000e40366ece225565d3ec83be7d97a4", 1, MEMORY_PAK }, // Blast Corps (U) (V1.0) [b1]
        { "00327e0b5df6dce6decc31353f33a3d3", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (U) [!]
        { "0035e8205336982e362402aaea37d147", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (E) [!]
        { "00396d6b4e66d323d4ad29833a53df74", 4, MEMORY_PAK | RUMBLE_PAK }, // Asteroids Hyper 64 (U) [o1][t1]
        { "004ea34ee12cbabdc053419714e66dd4", 2, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja 2 Starring Goemon (E) (M3) [t1]
        { "0054b7fc0c2acbed650efe727cdba472", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (E) (M3) [!]
        { "0057a561d7034392d4267a4134da41b0", 4, MEMORY_PAK }, // NBA Hangtime (E) [h1C]
        { "0059b65e1c813bc8bd1a61a1fe009ccd", 1, 0 }, // Bike Race '98 V1.0 by NAN (PD)
        { "006cafec9ba13060d4d2f0be211081bd", 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (J) [h1C]
        { "0072538ef925645db310f8e23a480b89", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (E) [!]
        { "008710f628c29d69bd939066a44ab877", 1, RUMBLE_PAK }, // Bomberman Hero (E) [f1] (NTSC)
        { "008b473841ce4d9ac050d55f99b4b5d4", 2, MEMORY_PAK | RUMBLE_PAK }, // Knockout Kings 2000 (U) [!]
        { "00a3e885f8d899646228a21d946b2102", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) [!]
        { "00ce20766866883063af3a6c3368039f", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f4] (PAL-Z64)
        { "00e2920665f2329b95797a7eaabc2390", 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) [!]
        { "00f846a9aa9e0083ce8c0e566843a7c7", 4, 0 }, // SRAM Upload Tool (PD)
        { "0112ffaada116d172abce136e9043a93", 4, 0 }, // PGA European Tour (E) (M5) [!]
        { "012c9daf61705f109392a85b2621cd42", 1, MEMORY_PAK }, // Tonic Trouble (U) (V1.1) [f1] (PAL)
        { "013e130d0d4f132741aa38d1a397e10a", 1, 0 }, // Heiwa Pachinko World 64 (J) [h1C]
        { "014464f2ac70fbbb94b08b9762e65a72", 1, MEMORY_PAK }, // Jangou Simulation Mahjong Do 64 (J) [b2]
        { "014a2b82fdf9a72fc5a26a7eaf8f8176", 4, 0 }, // Doctor V64 BIOS V2.03 (Red)
        { "015a3f07366cef93b5cb38b386e659f4", 4, 0 }, // Zelda 64 Boot Emu V1 by Crazy Nation (PD)
        { "0170c86e42e15a364cdc6e176a63a500", 4, 0 }, // Z64 BIOS V2.12b3
        { "01933a0cbbde1d4eb4c58cc6c6c27633", 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [b1][t1]
        { "01a1304d98a58e2d5069079a4f4441f9", 1, RUMBLE_PAK }, // Mega Man 64 (U) [t1][f1] (PAL-NTSC)
        { "01bdcc854d0ab798500e7bc31a24d94f", 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T+Eng2010-12-02_Zoinkity]
        { "01cd9938dae5dcdd4b264ae7f26c6d4d", 1, MEMORY_PAK }, // Scooby-Doo! - Classic Creep Capers (U) (V1.1) [!]
        { "01e2c444385597dc4fcf09e45c522b01", 4, 0 }, // Zelda 64 Boot Emu V2 by Crazy Nation (PD)
        { "01e34cd8a451f0b5aef93ccf9adb37af", 1, RUMBLE_PAK }, // Dezaemon 3D (J) [b1]
        { "02192b4b3797983bbe5e452336584208", 1, RUMBLE_PAK }, // Duck Dodgers Starring Daffy Duck (U) (M3) [!]
        { "023175b6c809704f00a317abcd25a8f9", 4, 0 }, // N64 Stars Demo (PD) [b1]
        { "026789d47db5fe202a76f89797b33ac7", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (U) [!]
        { "0283cc125aea29ef72a03bee7d759c5d", 4, MEMORY_PAK }, // Bomberman 64 (E) [h1C]
        { "02aa0c059ec36145927353c8e617aa41", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (E) (M4) [h1C]
        { "02aed169eb579494ace75d22e10d789b", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Backstage Assault (U) [!]
        { "02b16ac23998f78f09af6513f4acb664", 4, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush 2049 (E) (M6) [!]
        { "02c8e8fd7eb19bfeb325e2db0380506a", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (E) [f2] (Z64-Save)
        { "02e344ec824bba4a4cffcac5feadea51", 4, 0 }, // F-ZERO Expansion Kit (N64DD)
        { "02eef6ed11174664a548626337879e8c", 4, MEMORY_PAK | RUMBLE_PAK }, // Destruction Derby 64 (U) [f1] (PAL)
        { "030eab316dc3a2d33aac3de15fdc4473", 4, 0 }, // Doctor V64 BIOS V2.03 (Black)
        { "0321fcdcd35e6dc0d3f4dc1251919997", 4, MEMORY_PAK }, // International Superstar Soccer 64 (U) [h1C]
        { "032adeef1c2d0b4387fac099334f72e3", 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [hI]
        { "03418f1567dc45cbb4b4e9da772caa7f", 4, 0 }, // R.I.P. Jay Demo by Ste (PD)
        { "0343b990f58cd816d96fbe5d5e710c82", 2, MEMORY_PAK }, // Virtual Chess 64 (U) [o1]
        { "035239281994e1e254d7afc79f5570fb", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [f1]
        { "035b02e493a388e950fb42f4b3d891f8", 4, MEMORY_PAK | RUMBLE_PAK }, // Rally '99 (J) [f1] (PAL)
        { "037b58ee5fb4f3bff408ee406e3539c0", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [h6C]
        { "037d2727b90998bed41da0f290bb8441", 4, 0 }, // Pom Part 2 Demo (PD)
        { "03877a5cd0dd1e8c14eeee70660e79da", 4, MEMORY_PAK | RUMBLE_PAK }, // Wayne Gretzky's 3D Hockey (J) [o1]
        { "039875b92c0e4fef9797ec1744877b17", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.2) [!]
        { "03aa4d09fde77eed9b95be68e603d233", 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (U) (M5) [!]
        { "03bd8e5ca2b1b7d74398db4739979282", 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 6 (J) (V1.2) [!]
        { "03d17aa3dc7663502017d3cc5a19aa8b", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (E) (Beta) (2000-05-31)
        { "040538ea3c225d1e7f2497acefde8cd0", 1, RUMBLE_PAK }, // Bakuretsu Muteki Bangai-O (J) [h1C]
        { "040a9ea84fa3c73a8f318358e3250aa4", 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [b1]
        { "04150a63d1574673beab036e4eb5a249", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T+RusPreAlpha_Alex&gottax]
        { "041813684340ce4d988b2eba341ce5c5", 4, 0 }, // Doctor V64 BIOS V1.31 (Blue)
        { "041ce6116c560c239023efb88705da94", 4, 0 }, // Split! 3D Demo by Lem (PD)
        { "0451768553ab8cdd6c9ca37514e6c339", 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [hI][f1] (PAL)
        { "0458bc47cd771d8bc66b0ceae6895724", 4, MEMORY_PAK | RUMBLE_PAK }, // Rally Challenge 2000 (U) [!]
        { "045e0b801f70ade69981fcbb2ede5376", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball '99 (U) [f1] (PAL)
        { "04840612a35ece222afdb2dfbf926409", 4, RUMBLE_PAK }, // Mario Party 2 (U) [!]
        { "049db657f4223d949f56e9dc5b6a9180", 1, MEMORY_PAK }, // F-1 Pole Position 64 (U) (M3) [!]
        { "04a2c1a6efae28e07a4ad77603081f86", 4, MEMORY_PAK }, // Choro Q 64 (J) [b3]
        { "04a3f6d2a30fede6cadc5c2fcbcfbd12", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t1] (Hit Anywhere)
        { "04a6f25cb0f2084e631b3b7fff76befd", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [T+Ita100_Cattivik66]
        { "04c492be7f89fc6f425238bd67629544", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (U) (V1.0) [!]
        { "04dd2a319f4f5c22569b612cfdf9f00c", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter Destiny 2 (U) [!]
        { "04e650b7742a69dae98f125d1b492d78", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (U) (V1.1) [!]
        { "050531139c082e35b101e0929b6e07ea", 0, 0 }, // LaC's MOD Player - The Temple Gates (PD)
        { "05056045447bf1fba8f9878a7f6009f3", 2, MEMORY_PAK }, // AeroGauge (E) (M3) [!]
        { "0513a584bc6b3859555e72e10008b55f", 1, 0 }, // Super Mario 64 (U) [b2]
        { "05194d49c14e52055df72a54d40791e1", 4, RUMBLE_PAK }, // Conker's Bad Fur Day (E) [!]
        { "0529542de0794316320cf99ea80c51b7", 4, 0 }, // Star Soldier - Vanishing Earth Arcade (Aleck64)
        { "052bb2c8c4174633a0d38deb4d31c719", 4, 0 }, // Clay Fighter - Sculptor's Cut (U) [b1]
        { "05318721c273cb65cfbdf78ac2724bf2", 3, 0 }, // Jeopardy! (U) [h1C]
        { "053bb6d2e14d67f0fb0b509f2a6b4485", 1, MEMORY_PAK }, // Doom 64 (J) [b1]
        { "053c515bebe73391b4479a90a8ee93f9", 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [f4] (PAL)
        { "05426547e5ad9dbf88cf0a401032079d", 2, RUMBLE_PAK }, // F-1 World Grand Prix (U) [h2C]
        { "0580d96a71671c9e6972fdcf5897cc26", 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [!]
        { "0583ad9fdd1e3d10076aab40e5b4e7bb", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.0) [b1]
        { "060d0313e23b660180441fcc7d24d7db", 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 6 (J) (V1.0) [!]
        { "0620c2d134a0430f4afa208ffeda67b8", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [!]
        { "062c3ad13478d5c75b4e8db2ffd500dc", 4, MEMORY_PAK }, // VNES64 V0.12 by Jean-Luc Picard (PD)
        { "0630226f63561a05916edcfbc8d96c04", 4, MEMORY_PAK | RUMBLE_PAK }, // Rally '99 (J) [!]
        { "0635fe16018bb216e5cd29c300cadc74", 2, RUMBLE_PAK }, // AeroFighters Assault (U) [h1C]
        { "0658d69eba236fdf6da5d491bdc96574", 4, 0 }, // Z64 BIOS V2.17 by zmod.onestop.net (ORB HDisk ZIP250 2.18zd Hack)
        { "06a43bacf5c0687f596df9b018ca6d7f", 1, RUMBLE_PAK }, // Banjo-Kazooie (E) (M3) [!]
        { "06b58673f7d31c56f8fe8186e86f6bd6", 1, MEMORY_PAK }, // Castlevania (U) (V1.2) [!]
        { "06db24befb76c04d06d4ddc3a50d5319", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [o2][f1]
        { "06f15ef2228c1b1147c41dccaa07d9de", 1, MEMORY_PAK }, // Doom 64 (J) [!]
        { "0722e679f9695d9e5f1d2823dc7d8004", 4, RUMBLE_PAK }, // F-ZERO X (E) [b1]
        { "07660b03456dd0f776f392ef3e05c481", 4, 0 }, // TRON Demo (PD) [a1]
        { "07703e5a6268eedf552fe770f24273a7", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [b7]
        { "07a7d3db5b64a6ccf9d44b321472cc21", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (U) [t1]
        { "07ab7c19dbe85c8cc0961baaf3b5ca79", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (U) [h2C]
        { "07b7a5cfde9bc43f02a622387c5b5c58", 2, RUMBLE_PAK }, // Uchhannanchan no Hono no Challenger - Denryu IraIra Bou (J) [h1C]
        { "07c102e1adf6190e05ef3c897090dfd3", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 99 (U) [b1]
        { "07c468e5050fc619791437930d740b10", 4, 0 }, // Z64 BIOS V2.12b (Nintendo Backup Crew Hack)
        { "07c4944ddaad234f1f6b0b200bea60b2", 1, MEMORY_PAK }, // Donald Duck - Quack Attack (E) (M5) [b1]
        { "07d6006f744727daac7f53692ad950c4", 4, RUMBLE_PAK }, // RR64 - Ridge Racer 64 (U) [t1]
        { "07e123e104ba7b335b53932edb2cfb2f", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (U) (V1.0) [f1] (PAL)
        { "07ea11684d74da87719f05a1bdc4ccb3", 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.0) [f2]
        { "07f0ad841350bd7ca1e6a99fb7e887e6", 4, RUMBLE_PAK }, // WCW-nWo Revenge (E) [h1C]
        { "0802f5bb4004d8948a6c0b51c8857b74", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (G) [f1] (NTSC)
        { "0806e6db8b2bf0501be9f2d78d280dcf", 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (E) [b1]
        { "0846fffda3081821ea0dcbb7d4deaaa3", 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (U) [t1]
        { "084aba16d84c87712a731d485fcdbe3e", 4, MEMORY_PAK }, // FIFA Soccer 64 (E) (M3) [o2]
        { "087b6c2d5fbd778667c12d7fc7c305a7", 4, MEMORY_PAK }, // Wetrix (E) (M6) [t1]
        { "08826e96f3fb022a1c6351774198ba9d", 4, 0 }, // Wetrix (J) [o1]
        { "089d12ee45bfacd4a9fc93185af5c42b", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [b3]
        { "08b8942e977a81caf81dac2237e1aa65", 2, RUMBLE_PAK }, // F-1 World Grand Prix (E) (Prototype) [h1C]
        { "08bea3310e778a6584eb64cd3f15f86e", 4, MEMORY_PAK | RUMBLE_PAK }, // Ms. Pac-Man - Maze Madness (U) [!]
        { "08cbb141dec604e4dad2787f237d57a2", 4, MEMORY_PAK }, // Hexen (G) [!]
        { "08cca8d372ea8c38148f77ab5a03354d", 4, MEMORY_PAK }, // Eikou no Saint Andrews (J) [h1C]
        { "08d79a5c0f393d81296f50a6d556d999", 2, MEMORY_PAK }, // Rakuga Kids (J) [h1C]
        { "08e02f52e0547686a9bfac7cbb03c129", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (E) [!]
        { "08e1152e9d9742e9bbf6c224b6958f2d", 4, RUMBLE_PAK }, // Snowboard Kids 2 (U) [!]
        { "08e3f4eeea56c873a84ea076aa362edd", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [f1] (PAL)
        { "08e491f87445c6e5c168d982fc665d5f", 4, 0 }, // Bomberman 64 - Arcade Edition (J) [b1]
        { "08ebe38d2ea9bcaced94805bbbce7192", 4, 0 }, // Neon64 V1.2 by Halley's Comet Software (PD) [o1]
        { "0905a37dbb3b9cec38b52f6e109a9eaa", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (E) (M4) [h1C]
        { "093058ece14c8cc1a887b2087eb5cfe9", 4, MEMORY_PAK }, // Bomberman 64 (U) [!]
        { "094f639a9ba63b2136d2887c8d72bca0", 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (E) [!]
        { "094fe60c44f35bdc7fbe85339e9c4470", 1, 0 }, // Sporting Clays by Charles Doty (PD) [b1]
        { "0966cb688d6a5f1b22b4fab5a393c0e2", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [o2]
        { "096726498e698b51afe72aab5262a15a", 4, 0 }, // Wayne Gretzky's 3D Hockey '98 (U) [T+Ita_cattivik66]
        { "097189b4c9bf6775e4685951b6e66f24", 4, MEMORY_PAK }, // Frogger 2 (U) (Prototype 1) [!]
        { "097605021951024c3ecb2d502c0c2a9f", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [!]
        { "09ba01c6ec8b178a27fbee3a9dee8eb6", 4, 0 }, // Oerjan Intro by Oerjan (POM '99) (PD)
        { "09bee9d7eb1159d11e49bdfb03a2c4cc", 4, MEMORY_PAK | RUMBLE_PAK }, // RTL World League Soccer 2000 (G) [f1] (NTSC)
        { "09c5b4d19364efe48bb818087734978e", 4, MEMORY_PAK | RUMBLE_PAK }, // Super Bowling (J) [!]
        { "09fd63afa1156405e618752fc583df93", 4, MEMORY_PAK }, // Bakushou Jinsei 64 - Mezase! Resort Ou (J) [!]
        { "0a010dffb20b9a4fb7ba2f962788f54f", 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [f1]
        { "0a04f5f5938218b510a573b86967b199", 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.0) [t1]
        { "0a3955c704210225d95937dcab86c9e3", 1, RUMBLE_PAK }, // Chopper Attack (E) [t1]
        { "0a3a4c761960f7d648afa60b1e565c7c", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (U) (Beta) [h1C]
        { "0a44393e1c97e4f91303029e27f28af4", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (U) [t1]
        { "0a5acfea0c7cf68ae25202040d5ad1eb", 2, MEMORY_PAK | RUMBLE_PAK }, // 40 Winks (E) (M3) (Prototype)
        { "0a6715a80957dd7c58f12e1f33d75f43", 4, RUMBLE_PAK }, // SmashRemix1.0.1
        { "0a9d53ef71a0c6f2ae3a0435e3d58747", 4, MEMORY_PAK }, // NBA Jam 99 (E) [h1C]
        { "0aa8e4b96ebb5b8a5cf91cfb04bc3741", 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T-Eng2007-03-22_Brandon Dixon]
        { "0ab48b2d44a74b3bb2d384f6170c2742", 1, 0 }, // Legend of Zelda, The - Ocarina of Time (Ch) (iQue) [!]
        { "0ac7a16f8afcc03031835c89d095d7b9", 4, MEMORY_PAK | RUMBLE_PAK }, // Getter Love!! (J) [T+Eng1.01_ozidual]
        { "0ad0800d351f77ec31092096d58c25f0", 4, 0 }, // Dragon King by CrowTRobo (PD) [b1]
        { "0ad7111c5db3ca075dcd5ac9838c3b24", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o7][T+Ita_cattivik66]
        { "0aee21a365f9450c258a59556f77edb8", 4, 0 }, // Nintendo Family by CALi (PD)
        { "0b0df8ec747bf99f3a55a3300ce8bc0d", 4, 0 }, // U64 (Chrome) Demo by Horizon64 (PD) [a1]
        { "0b13983a634732fde9d2d638c0a14c51", 4, MEMORY_PAK }, // New Tetris, The (U) [f1] (PAL)
        { "0b281d478b1b4ff9329b45eeb80d53f2", 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [b4]
        { "0b2b4d3c38426763059d68aa53b86fdf", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) (Unl)
        { "0b2b6cc62cfeeb7c4642a3643a3ed3c8", 4, MEMORY_PAK }, // Mario Kart 64 (U) [t4]
        { "0b5a50e8d477b3bc53be997264fe84a5", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [b1]
        { "0b5f909546de5a414f2281ad0c83d9f9", 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (E) [f1] (NTSC)
        { "0b7c2a6d2b3de3d7d0ff7aafab6f8da7", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.10
        { "0b8303e33b3f6e57531fb8eb3fae65e9", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (U) (M7) [o1]
        { "0b86fa9259e2b751111a1701091644b1", 2, RUMBLE_PAK }, // Dance Dance Revolution - Disney Dancing Museum (J) [!]
        { "0bbe17ec2271dc293f1d2f4a7b0d61d4", 4, 0 }, // POMolizer Demo by Renderman (POM '99) (PD)
        { "0bd1f7bb9f4b02520e4e9285c809f099", 1, RUMBLE_PAK }, // Star Wars Episode I - Battle for Naboo (E) [!]
        { "0bd4aeb12e502c39140e4da52f77642e", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [b2]
        { "0bd71426a0240ca4d871a8f54b7761f0", 4, RUMBLE_PAK }, // Cruis'n World (E) [f2]
        { "0be4958b3e8eed916307dc9f5de3206d", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [t3]
        { "0bf64427cf68e49c70e9ec2c9d815209", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mickey's Speedway USA (U) [!]
        { "0bf79f532c938384431da9406b846afa", 4, 0 }, // Rotating Demo USA by Rene (PD)
        { "0c0bfd1038eda4f5c958dc362cdff2d6", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (G) (V1.0) [!]
        { "0c13e0449a28ea5b925cdb8af8d29768", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina - Zelda Collection Version (J) (GC) [!]
        { "0c26665c2f40a786778ee4370c3a43fc", 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [f1] (PAL)
        { "0c2cbafec6f184ad39ef29b2b5e0f44a", 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (E) [!]
        { "0c3448a9d85300acb9c5556f27a84b23", 2, RUMBLE_PAK }, // V-Rally Edition 99 (U) [!]
        { "0c74b44680276ffe808cfa6045329819", 1, RUMBLE_PAK }, // Rockman Dash (J) [!]
        { "0c91c461ed69ec1e93e9e0def9053546", 4, 0 }, // Doctor V64 BIOS V2.01
        { "0c9f7fa54ca36bfc6d8be4187e4abd99", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [o2][f1]
        { "0cba02d6ea55809a1e8d9ba02d199b7d", 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [b2]
        { "0cc1a139f9c3865fe698c64209c45253", 4, 0 }, // Z64 BIOS V2.11PAL
        { "0cff6a7c1b1334e07585f0c5cdb53b40", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.1) [o1]
        { "0d0d6a170871ed3433a52d110b01357e", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [b5]
        { "0d3f2f58310387c60f932f6ec936788d", 2, 0 }, // War Gods (U) [o1][h1C]
        { "0d6331c2fa062f20272cc1a7f80f7271", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.14]
        { "0d68a405bcb04318a294dbeabf1de063", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (J) [f1] (Z64)
        { "0d708dde7ec6b67e705f1d865d70afda", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t5] (2x Aggressor)
        { "0d77918e6c989261eef81fe244e48eef", 4, 0 }, // Action Replay Pro 64 V3.0 (Unl) [b1]
        { "0d97158c3408f8e3e6d38a17223ac881", 1, RUMBLE_PAK }, // Mission Impossible (U) [b2]
        { "0dc80adcd95af4fc8fe2c7e648ca64f0", 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (E) (M3) [h1C]
        { "0dcf29478a6db0a3113b9edd2ae428e1", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [f1]
        { "0dd2887af64c5786c1c66877afcc0b97", 2, 0 }, // Dark Rift (E) [b1]
        { "0e0e920ab13ef13508f5a98cc4cd2ff8", 1, MEMORY_PAK }, // Disney's Donald Duck - Goin' Quackers (U) [!]
        { "0e2516c65b94214c891b2d07c3d92aec", 4, 0 }, // MAME 64 Demo (PD) [b2]
        { "0e274a162cad0b1b50f9b8d64d9f82fc", 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (U) [t1]
        { "0e2cab03b712e7d4e1c6501978e2e637", 1, MEMORY_PAK }, // Quest 64 (U) [b2]
        { "0e38732bc5f564d74d57ef0a0cba8d56", 4, 0 }, // Cliffi's Little Intro by Cliffi (POM '99) (PD) [b1]
        { "0e85a098d0f0e8a7eb572a69612a6873", 4, TRANSFER_PAK }, // Pokemon Stadium (F) [!]
        { "0edbcb64159b7e32dda165a7a363ab35", 4, 0 }, // Clay Fighter 63 1-3 (U) [b2]
        { "0ef2448c243f86c4c9f194f49cfd8352", 1, RUMBLE_PAK }, // Body Harvest (E) (M3) [f1] (NTSC)
        { "0f0b7b78b345fbf2581d834cb4a81245", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.0) [!]
        { "0f205ba6b663c3c31579894873cafa66", 1, MEMORY_PAK }, // Saikyou Habu Shougi (J) [h2C]
        { "0f412c4cde69ae41b89d390731d34f71", 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (E) [b1]
        { "0f6d3edb5797a775757336549a8a9b94", 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.0) [b1][o1]
        { "0f7c52ad7c7c88103960f44fcd0b119a", 4, MEMORY_PAK }, // Eikou no Saint Andrews (J) [b2]
        { "0f83626497b7acc4aab3eb39cde893d3", 4, 0 }, // Doctor V64 BIOS V1.92
        { "0f85543f27896856eb82c52e3c7ccab5", 4, 0 }, // GBlator for PAL Dr V64 (PD)
        { "0f9b89c903befedcfd107e5b5ade687f", 2, 0 }, // Mortal Kombat Trilogy (E) [h1C]
        { "0fa23cd30b68eb86548630fe635bb90e", 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.0) [o1]
        { "0fdc4acab1b531cbb4dfb142cd630b7a", 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (U) [t1]
        { "0fe31a113ef389f8f95460d340c7d922", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (U) [h2C]
        { "0ff1f8628d8fe69582db54572d2bea79", 1, MEMORY_PAK }, // Wonder Project J2 - Koruro no Mori no Jozet (J) [!]
        { "103592e65c65464c141a2184dbfd0947", 4, 0 }, // SRAM Upload Tool V1 by LaC (PD)
        { "103e16bf7fcfb4d713d2ebe19dcb5311", 2, RUMBLE_PAK }, // Tsumi to Batsu - Hoshi no Keishousha (J) [T+Spa1.0b_TheFireRed&IlDucci] (Fandub)
        { "1051e1402ee110f3c5e372c9e1c5b338", 4, MEMORY_PAK }, // Derby Stallion 64 (J) (Beta)
        { "10747662a55241b9234cd114c940504f", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (J) [!]
        { "10774bfbc9d815194ad904f898cbe96f", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [o1][h2C]
        { "108a605e3d57a5e5c52398b675861ef4", 4, MEMORY_PAK }, // Berney Must Die! by Nop_ (POM '99) (PD) [t1]
        { "108b1a72e6f93e6935e1ad8d619797db", 1, RUMBLE_PAK }, // Dezaemon 3D (J) [t1]
        { "108f6609cb131830ec4c67f4a8a71d30", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (E) [h1C]
        { "10a28df32d06b7d3d9b14091ac9d2e14", 4, 0 }, // Pip's RPGs Beta 2 by Mr. Pips (PD)
        { "10a2e08708e25195e063d2e05231ab66", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Spa01b_toruzz]
        { "10e62952244834e0b52af32898de8d9f", 4, 0 }, // Pip's RPGs Beta 6 by Mr. Pips (PD)
        { "11169a32d449ec3a8903ca8a9d69a6aa", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (E) (M3) (Fre-Ger-Dut) [!]
        { "113044b16b75f98792bf9c20c6b6282b", 4, MEMORY_PAK }, // Shin Nihon Pro Wrestling - Toukon Road 2 - The Next Generation (J) [!]
        { "1143b49d788ba477dae98c05d6623344", 1, RUMBLE_PAK }, // Bomberman Hero (U) [b2]
        { "114af722029d6386c3bfea4cc8fa603c", 4, 0 }, // Mario Artist Communication Kit (J)
        { "115118dd5e0f02d82ba1bf070a7b78f1", 4, RUMBLE_PAK }, // Custom Robo V2 (J) [!]
        { "115521bac11608febce874f243acc66e", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (U) [f1] (PAL)
        { "1170033980adc1981505cf958f35f1eb", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) (Beta) [!]
        { "117ef2639e0485210c7ab70cbf3e0c00", 4, RUMBLE_PAK }, // WCW-nWo Revenge (U) [b2]
        { "118e9ce360b97a6f8dab2c9f30660bf5", 4, RUMBLE_PAK }, // Donkey Kong 64 (E) (M4) [!]
        { "11b20cfe8e3eccc680aa8157a7906d6d", 4, MEMORY_PAK }, // 64 de Hakken!! Tamagotchi Minna de Tamagotchi World (J) [b1]
        { "11b891b7551bfbdb79732bfb94b45603", 4, MEMORY_PAK }, // 64 de Hakken!! Tamagotchi Minna de Tamagotchi World (J) [o1]
        { "11be82186c64ef407b28403d49b199db", 2, MEMORY_PAK }, // AeroGauge (U) [h1C]
        { "11d6fff288de1bd61ccbd7cca0c4a97b", 4, RUMBLE_PAK }, // Top Gear Overdrive (E) [h1C]
        { "11da2a2d0e91074bb8ff1fcb029535fd", 4, RUMBLE_PAK }, // Donkey Kong 64 - Tag Anywhere (V5) (U)
        { "11eee2f34bf8da05a1b8f4fb9fe9f74c", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (J) [!]
        { "11fed930d4c4dd0ca7cb03b5f1bf0a12", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [b1][f1] (PAL)
        { "1211c556d77b169d81a666a9661e1777", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (U) [!]
        { "121a19e2a547c554155433788986033c", 1, 0 }, // Super Mario 64 (E) (M3) [t1]
        { "1221c67c46c77487d35f03d3d13a3022", 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (U) (M10) [b1][t1]
        { "123417811cdc49118eb01ecf76baf760", 4, 0 }, // SRAM Manager V1.0 Beta (32Mbit) (PD) [a1]
        { "1236fb3fc8464b559bdedfa966d6679f", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [f1][h1C]
        { "1247b093e0fd6ccfd50d15de59301076", 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [!]
        { "12534dab32dbfc6ca4f66d05729102e6", 4, RUMBLE_PAK }, // Monster Truck Madness 64 (E) (M5) [f1] (NTSC)
        { "12562bff127e52a6ee9b4fef92050af9", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [f1] (PAL)
        { "12569b4fa5239a3ad53de5d2e0243488", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [t1]
        { "1280c78f286fc1c437a4905ee42c47f1", 1, RUMBLE_PAK }, // Hey You, Pikachu! (U) [!]
        { "12827e2ef6eb1ff0f8aa494e671c3094", 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [f1] (PAL)
        { "12bd3fafb4e064b6b0c07b7ee156243b", 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (U) (Beta) [!]
        { "12ce267f5d538c2959bdeba5e87bde79", 4, RUMBLE_PAK }, // Cruis'n World (E) [f3] (NTSC)
        { "12f0671c66d25622c4c9cd71c678a1f6", 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [t1] (Boost)
        { "131ccc5c9e9bd830147562dfd06444bf", 1, MEMORY_PAK }, // Quest 64 (U) [b5]
        { "133b6615cd0936ac705dfc81e4389cbb", 1, 0 }, // Bike Race '98 V1.2 by NAN (PD) [b2]
        { "13532519cbc43bfa638fd5a7754ee281", 1, RUMBLE_PAK }, // Body Harvest (U) [b2]
        { "136245692e02003ed1a972f424cd9323", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [h2C]
        { "137471d67dc8847815e327a73ea30068", 1, MEMORY_PAK }, // Sim City 2000 (J) [o1]
        { "13893db9e919c4e7cf0c0b0064ccb554", 1, RUMBLE_PAK }, // Itoi Shigesato no Bass Tsuri No. 1 Kettei Ban! (J) [!]
        { "13962910efd82c2a2333cf38ee1fcd96", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (U) [T+Ita_Cattivik66]
        { "13b5ae646cc09876d64366bb07796c78", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball '99 (U) [o1]
        { "13ccaa36d309b78d20c9c0c89a8ce650", 2, MEMORY_PAK }, // AeroGauge (J) (V1.0) (Kiosk Demo) [b3]
        { "13cd4c03fca1bc23ae8b57885e76bca6", 4, 0 }, // Doctor V64 BIOS V1.73
        { "13d7834291a311077b84b9a2816af6fc", 4, 0 }, // Birthday Demo for Steve by Nep (PD) [b1]
        { "13d9514a4d208dc6c7b0c833f68114d2", 4, MEMORY_PAK }, // Shin Nihon Pro Wrestling - Toukon Road - Brave Spirits (J) [!]
        { "13ecbaeef7111d5343d73a80e03e353a", 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) (Demo) (V_ECTS_Decrypted)
        { "13faa58604597e4edc608070f8e0ae24", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (E) (V1.0) [!]
        { "13fab67e603b002ceaf0eea84130e973", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [!]
        { "1408fcf68b60d845f090107ef355a7e5", 1, RUMBLE_PAK }, // Looney Tunes - Duck Dodgers (E) (M6) [!]
        { "1412dddcfd2dc960da7d282d99fc2356", 4, 0 }, // Z64 BIOS V1.10b
        { "141430d034c16fc4b4ba20d53eab61c1", 1, RUMBLE_PAK }, // Bomberman Hero (E) [b2]
        { "144b10a484a22367fd2679529dbd2fed", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (E) (M4) [!]
        { "144b8906dc40534cfbef6d7b994a982b", 1, 0 }, // Pokemon Snap (G) [!]
        { "1480e755e96b392490f606a75bc306c6", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.0) [b5]
        { "148de3073727b9fd156f10afadd46864", 4, MEMORY_PAK }, // FIFA 99 (U) [!]
        { "1492806f12d33c3ea0edb6848d43b1cc", 3, MEMORY_PAK | RUMBLE_PAK }, // Rampage 2 - Universal Tour (E) [!]
        { "14a21928be46c18ba04161305e89f5de", 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (J) [!]
        { "14abd7c0fa3b5256fc62824039f06a20", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.1) [T+Ita1.0_Rulesless]
        { "14acc20454b0c2789c9f7ef7a556231a", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [b1]
        { "14aeb927f3620efe48af7fc748c1931f", 1, RUMBLE_PAK }, // Chopper Attack (U) [b1]
        { "14d070077d70c94cbb1413c7641e7272", 4, MEMORY_PAK }, // Brunswick Circuit Pro Bowling (U) [b1]
        { "14ddf19382385774f253049abeaf01d2", 4, 0 }, // Summer64 Demo by Lem (PD) [b1]
        { "14e8e1761b8994c54b304418cea49773", 2, 0 }, // 77a Special Edition by Count0 (PD)
        { "14f1f43a0314c3e36d9d248e1f03ec2e", 4, 0 }, // Ronaldinho's Soccer 64 (B) (Unl) (Pirate)
        { "151e92f5e3ee294baff5d4c3e4e95d77", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (J) [f1] (PAL)
        { "152b9939a5f50734d5401980028856b4", 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (E) [!]
        { "1538a4235fded9d65b98c25edf6f9c0e", 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (E) [b2]
        { "1546877fd85c00a83515005727e5fda5", 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (E) (M6) [!]
        { "155fa36dd3927a73534461b50417325f", 4, 0 }, // Famista 64 (J) [b6]
        { "1561c75d11cedf356a8ddb1a4a5f9d5d", 4, TRANSFER_PAK }, // Pokemon Stadium 2 (U) [!]
        { "1587b17344a43532b791dceb237d2bfc", 4, BIO_SENSOR }, // Tetris 64 (J) [T+Ita]
        { "15a87a6d01dba1a7c4375ffbc1214bb8", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (E) (M3) [!]
        { "15b57cdeca506b34ff6246ed06aabd50", 4, 0 }, // GoldenEye 007 (U) (No Power Bar Hack)
        { "15c2c37d4ffa1e2c5bc4d1029bc225ac", 4, 0 }, // LaC's Universal Bootemu V1.2 (PD)
        { "15d1a2217cad61c39cfecbffa0703e25", 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.0) [!]
        { "15db716942c36a0cb392226001536a2a", 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (E) (M3) [b1]
        { "15df97a59b2354b130dec3fb86bba513", 1, 0 }, // Elmo's Letter Adventure (U) [!]
        { "15e4c1b4f3f459d4caa7f7e2cf0c95da", 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (F) [!]
        { "15f425829d54dd290451d2f3ebaf953f", 4, MEMORY_PAK }, // Wetrix (E) (M6) [f1] (NTSC)
        { "1614e1c0bebfaa0855da9990ef859a68", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 99 (U) [b2]
        { "1618403427e4344a57833043db5ce3c3", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (E) (GC) [!]
        { "1630a446857f28d3068907f302b02114", 4, 0 }, // Z64 BIOS V2.17
        { "164050084561ecb691a51b6d1e5bf4ae", 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (E) (M3) [h1C]
        { "1659f9f8c8a5c24f775b245bda574462", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [b1]
        { "166221365db70d446c4206083d422dd1", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (V1.1) [!]
        { "167a3502f06cf0eef56758533f3d0e52", 2, MEMORY_PAK }, // Rakuga Kids (E) [!]
        { "168872484f1e933ff351d1c3dc1fc601", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.23 (Odd Bytes)
        { "1698508f521280d0a80e078ec981d4ac", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (E) [!]
        { "16a3143dc0e5fe6639f49db7a51d24d4", 1, RUMBLE_PAK }, // Mission Impossible (S) (V1.1) [!]
        { "16b82d53d7f038a8fe67a78027720516", 1, MEMORY_PAK }, // Blast Dozer (J) [!]
        { "16cb66dd44f7a46eeac5ffcb38fbe26b", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [b3]
        { "16cc6db10a56331b56f374b4fb254d5e", 1, MEMORY_PAK }, // Scooby-Doo! - Classic Creep Capers (U) (V1.0) [!]
        { "16d7676c2394e422076f7cb855de901b", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [b1]
        { "16e77c21e1ea47fdd8c5635cfff94bfd", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.21
        { "16ec788c8a4e7eee268fdf9072a4f0d4", 1, RUMBLE_PAK }, // Itoi Shigesato no Bass Tsuri No. 1 Kettei Ban! (J) [b1]
        { "16f2644156ac9dd15ca8cc01deae5e7e", 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (U) [b3]
        { "171e55bf4383676c0b8f3d38b0d6a739", 4, 0 }, // Fogworld USA Demo by Horizon64 (PD) [h1C][o1]
        { "1730119b0455ef89c4e495dec8e950a5", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (U) [!]
        { "1746d1cc38c42f08f0cd12d170c046b8", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.09]
        { "175edab7f1a3dfbb5d23c4aa1b9d647e", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.0) [f2] (zpc1)
        { "176d518ad6e1d89bc0fef0672a4c1882", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.09
        { "1793b45c4797d9418685946a002b5d15", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [b2]
        { "17951444830696aafc07f311ab128099", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o1]
        { "179537a49daa33bf031f09a9589eaa28", 1, 0 }, // Super Mario 64 (U) [h2C]
        { "17b839b8e72a6287a5bb3cefcbc035e9", 4, 0 }, // N64 Scene Gallery by CALi (PD)
        { "17d6d6da5d03c9d295d72a212a719eb2", 2, RUMBLE_PAK }, // Tom and Jerry in Fists of Furry (U) [t1]
        { "1830ede2557e8685e6f76d05cc65076a", 4, MEMORY_PAK | RUMBLE_PAK }, // ECW Hardcore Revolution (E) [!]
        { "1880da358f875c0740d4a6731e110109", 4, RUMBLE_PAK }, // GoldenEye 007 (J) [!]
        { "18c00b5c3d9df09ab9e8a737e61c3b47", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [t1] (Rapid Fire)
        { "18e09c0650fa3f83a90089dadd96a213", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (E) [h1C]
        { "18e1ca4be7d8e56ac940852ec5e3ed22", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [t2] (Hit Anywhere)
        { "18e741aac72eca5a9fefefe6f7a3e57a", 2, MEMORY_PAK | RUMBLE_PAK }, // King Hill 64 - Extreme Snowboarding (J) [b2]
        { "190584d9c0dc2875183fb84c5072b8f1", 3, 0 }, // Jeopardy! (U) [o1]
        { "190906a8a5f1c1d4909f6374a784eca4", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) [T+Ita0.40]
        { "190ccca6526dc4a1f611e3b40f5131c0", 1, MEMORY_PAK }, // Doom 64 (E) [!]
        { "192b715e8bc5972a4986df21dc8bf357", 1, RUMBLE_PAK }, // Taz Express (E) (M6) [!]
        { "1934618c99a70dd8e74305c6db06caf3", 4, 0 }, // Spice Girls Rotator Demo by RedboX (PD)
        { "193913bb063f3d945019e8f765f48f1c", 4, 0 }, // Hard Pom '99 Demo by TS_Garp (POM '99) (PD)
        { "1942833ac1a71be8bae74bbdfd6de278", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2000 (U) [!]
        { "194ce13b2b6b1f584587a68cc2f398dc", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (U) [b3]
        { "1960a3879fadf2c5eff5beb47e0e1441", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (J) [!]
        { "1977d63b511a900628db2a3a104160ac", 4, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush 2049 (U) [t1]
        { "197e24c1ab6503998798d7f56eae7add", 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (U) [t1]
        { "1987e0f12ddd619f440cd9637df7cc28", 1, MEMORY_PAK }, // Tonic Trouble (U) (V1.1) [t1]
        { "19a646e0148ea6695e797c1990919b8a", 1, RUMBLE_PAK }, // Command & Conquer (G) [f1] (Z64)
        { "19ba19834f948eee3828640246405b5d", 4, 0 }, // V64Jr Flash Save Util by CrowTRobo (PD)
        { "19ba66fee088181bbb0aca6894bb8971", 2, 0 }, // Space Dynamites (J) [b1]
        { "19bfc2c0124c009b51fe1724b07fac81", 1, MEMORY_PAK | RUMBLE_PAK }, // Ganbare Goemon - Neo Momoyama Bakufu no Odori (J) [b2]
        { "19f4694b8557bc095ee07b5c8fbd23b5", 4, 0 }, // Heavy 64 Demo by Destop (PD)
        { "1a232a07b0d2a102ca528e75cb7bba22", 4, MEMORY_PAK }, // VNES64 + Galaga (PD)
        { "1a56fe2b2fe339c7dd4ff8d37ec8654b", 4, 0 }, // GameBooster 64 V1.1 (PAL) (Unl)
        { "1a5d6fbf59d1639af71b61410553ab04", 2, TRANSFER_PAK }, // Pocket Monsters Stadium (J) [f1] (Boot-PAL)
        { "1a7936367413e5d6874abda6d623ad32", 4, 0 }, // Dr. Mario 64 (U) [!]
        { "1a9195662c89dcbea88bcfa99b096cde", 1, RUMBLE_PAK }, // Command & Conquer (G) [!]
        { "1a9eba4d8cb647fd03aaeb022d04344f", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [b2]
        { "1aa79b5defe1f5852d5add799383cccd", 2, MEMORY_PAK }, // Robotron 64 (U) [b4]
        { "1ac234649d28f09e82c0d11abb17f03b", 4, MEMORY_PAK | RUMBLE_PAK }, // Xena Warrior Princess - The Talisman of Fate (U) [!]
        { "1add2c0217662b307cdfd876b35fbf7a", 1, RUMBLE_PAK }, // Resident Evil 2 (U) (V1.1) [!]
        { "1af93922a67085ab2c777b1f08af365b", 1, RUMBLE_PAK }, // In-Fisherman Bass Hunter 64 (U) [f1] (PAL)
        { "1b1378bb9ee819f740550f566745af73", 1, MEMORY_PAK }, // Doom 64 (U) (V1.1) [!]
        { "1b1dc8b1dd950e70c0f532dd81c9a982", 4, 0 }, // Summer64 Demo by Lem (PD)
        { "1b28c4ca21648d318bc6dd3ef27bb1fa", 4, 0 }, // Die Hard 64 (U) (Prototype) (Level 3)
        { "1b41a885cbaa693714575383db4c05be", 2, MEMORY_PAK | RUMBLE_PAK }, // Mike Piazza's Strike Zone (U) [h2C]
        { "1b94d98f509f467f9bc541102f6ebdfc", 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack (J) (V1.0) [b1]
        { "1b991cf41c70ff2c92ffbefacabe8d03", 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (E) [!]
        { "1bced1ba1bf5a0e2ff5768c714ee4627", 1, RUMBLE_PAK }, // Bomberman Hero (U) [b3]
        { "1bdc2bfdd0b6ca9d8fd354aeaf1a3cf6", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [f1] (NTSC100%)
        { "1bf5f42b98c3e97948f01155f12e2d88", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina (J) (V1.1) [!]
        { "1c0fcfc7b304698f77069a5f31d27de1", 4, 0 }, // Doctor V64 BIOS V1.09
        { "1c3b29cc7f362ca63ac1c26456737bb3", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [t2]
        { "1c494719032ff99382b167c43fb11762", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (U) [!]
        { "1c6fe6a40aac4bd515373b6ed8d25dbf", 2, 0 }, // Mortal Kombat Trilogy (E) [b1]
        { "1c7fa31271e5588c968675e932c08991", 1, RUMBLE_PAK }, // Batman Beyond - Return of the Joker (U) [o1][t1]
        { "1c897ec059c6c34c8ed57e1965410f2e", 2, 0 }, // Mace - The Dark Age (U) [b9]
        { "1cc5cf3b4d29d8c3ade957648b529dc1", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [!]
        { "1ccd7db5b3f57944caf7da86ef1763e3", 1, 0 }, // Bike Race '98 V1.2 by NAN (PD)
        { "1cd90b13b7fd6afdcb838f801d807826", 4, RUMBLE_PAK }, // Chameleon Twist (E) [!]
        { "1cf31e7f6e0deb2c18c39ddd4eed9e51", 1, MEMORY_PAK }, // Bokujou Monogatari 2 (J) (V1.0) [!]
        { "1cf379aca2397222af9f3dc5d8e3c444", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (G) [!]
        { "1d178b74a163711ca9c06327f3b709cf", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (U) [h1C]
        { "1d26e8b65cd2f1bf1eabaffdbbfc1f26", 1, RUMBLE_PAK }, // Command & Conquer (E) (M2) [b3]
        { "1d2cb88bbcb67e12289b74f5bd88ae4c", 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 64 (J) [h1C]
        { "1d4843f0c69e273a1099a4267bb9f526", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) [b3]
        { "1d4bd885db0c0c5365698526077e10f4", 2, 0 }, // Mace - The Dark Age (U) [b8]
        { "1d71761771e3bffc091e38b9e8b5e590", 4, MEMORY_PAK }, // FIFA Soccer 64 (E) (M3) [b1]
        { "1d7cfc7cdb5a0e1b69b1dc2e58a4f192", 4, 0 }, // Planet Console Intro (PD)
        { "1d86ae43ff88e4583c8161815577f1cd", 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (U) [t1][f1] (PAL-NTSC)
        { "1dde4d26cedb2e8565aacb2c8098df8d", 4, 0 }, // Neon64 GS V1.05 (Gameshark Version) (PD)
        { "1de07fca04fb0810d95b8ae4da173a2e", 4, 0 }, // UltraMSX2 V1.0 by Jos Kwanten (PD) [f1] (V64)
        { "1df4f3503feffba6214092e0abba33b2", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.21 (Odd Bytes)
        { "1e03884f7017230e96f741085e5817f8", 4, RUMBLE_PAK }, // Lylat Wars (A) (M3) [t1] (Boost)
        { "1e6573f2a7454c133eb05a14c15909a9", 4, MEMORY_PAK | RUMBLE_PAK }, // Getter Love!! (J) [b2]
        { "1e672743a9b6b4e9dfb40d4004735f38", 2, MEMORY_PAK | RUMBLE_PAK }, // Flying Dragon (U) [b2]
        { "1e9fead701fe5aaaa248d4713891775d", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [!]
        { "1eac55826a435fd1c0e0feefe89fac15", 4, RUMBLE_PAK }, // Mario Party (E) (M3) [T+Spa1.0_PacoChan]
        { "1ed9c697bd2f2aebe6b3b8bf0d24a8ca", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f3][t1]
        { "1ee8800a4003e7f9688c5a35f929d01b", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [!]
        { "1f324e92af23cdc5499bbc35f3840205", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [b1]
        { "1f455908263c2745e4100c0319b0f4c0", 2, MEMORY_PAK }, // Namco Museum 64 (U) [o1]
        { "1f57194ea272cf5db500d228e9c94d75", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (E) [f1] (NTSC)
        { "1f72e506250b16726e2c5a629cae8e0f", 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [hI][t1]
        { "1f83663b2c84512fc3706a6cacc1a9f3", 4, 0 }, // Wave Race 64 (Ch) (iQue) [!]
        { "1f997b26062b7d59f23193f448157ee3", 1, RUMBLE_PAK }, // Bass Rush - ECOGEAR PowerWorm Championship (J) [b2]
        { "1fa409fcac007ddeccc4cf439a0d8dae", 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (U) [!]
        { "1fb40ee386b58feab6cf29ddb33bcccc", 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.0) [!]
        { "1fc540d2168650e135bcc7d3b542e03a", 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (E) (M4) (V1.0) [b1]
        { "1fe59964ec1a5899f3c7d97590f676eb", 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [b2]
        { "1fec5a43f756c4aae958cd286f6451c5", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [t1]
        { "1ffbdab727c9d634d4f4ddbd9f358445", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (E) [t1]
        { "2002438491f94c468cb487032ac4aa33", 1, MEMORY_PAK }, // Pachinko 365 Nichi (J) [b1]
        { "203c3bbfdd10c5a0b7c5d0cdb085d853", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW vs. nWo - World Tour (U) (V1.0) [!]
        { "2048a640c12d1cf2052ba1629937d2ff", 2, MEMORY_PAK }, // Wave Race 64 (U) (V1.1) [!]
        { "207efe58c7c221dbdfff285ab80126c1", 2, MEMORY_PAK }, // Rugrats in Paris - The Movie (U) [!]
        { "2080262a251d270f9ce819887f2104a7", 4, MEMORY_PAK }, // Hexen (E) [!]
        { "2084974c9098b2d01c1f58b0e56ee058", 4, 0 }, // Clay Fighter 63 1-3 (E) [h1C]
        { "2085fbd994d3597a8a520f695e336382", 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [b4]
        { "209db8333eeb526ae9a91209175348ce", 4, MEMORY_PAK }, // J.League Live 64 (J) [!]
        { "20ae0ba68601eb0a49d61bc7a00feadd", 4, MEMORY_PAK }, // Hyper Olympics in Nagano 64 (J) [o1]
        { "20b854b239203baf6c961b850a4a51a2", 1, 0 }, // Super Mario 64 (U) [!]
        { "20c68eafdfcb1ba98b495e5beed5e4b6", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.0) [f1] (zpfc)
        { "20c7abf5a4db8dc1c70725fd213e29a2", 2, RUMBLE_PAK }, // Puyo Puyo Sun 64 (J) [b1]
        { "20d02eecfa887dc5a31948b18b4bb18d", 2, RUMBLE_PAK }, // Magical Tetris Challenge (E) [b1]
        { "20da03c2098f83a790e929ac9a4cdb2e", 4, 0 }, // Ghemor - CD64 Xfer & Save Util (Parallel) by CrowTRobo (PD)
        { "20da62ece553ede84d02283174becc8f", 4, MEMORY_PAK }, // Penny Racers (E) [!]
        { "20db5e4ddb0cd5b04c4bf09cdc95592f", 4, RUMBLE_PAK }, // Cruis'n World (E) (V1.1) [!]
        { "20e51b27e8098a9d101b44689014c281", 2, RUMBLE_PAK }, // Magical Tetris Challenge (E) [!]
        { "20ed9559d47535ea491d3548fb9e15e2", 1, RUMBLE_PAK }, // Yoshi Story (J) [f4]
        { "20f5eeee849db44146aafb1b97a857f6", 4, RUMBLE_PAK }, // F-ZERO X (U) [T+Por0.99_byftr]
        { "20fbba5fccb7454367e071a744d6bb2f", 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [b3]
        { "211256abbc0844d9152d6a7ffa9e48bd", 4, RUMBLE_PAK }, // Top Gear Overdrive (U) [t1]
        { "2131a0ae317d243618a1e90e2153215c", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (U) [b2]
        { "2138ce5aec363bac2f18ce65a78eca25", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [o1]
        { "2151ab8629dacd8936439fc90f0afd10", 4, RUMBLE_PAK }, // Chameleon Twist (E) [b3]
        { "217b0e4723dbacda40b70b26e610f5f9", 1, RUMBLE_PAK }, // Mission Impossible (S) [f1] (NTSC)
        { "217d93b21fec3a5506db014bf445b581", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (U) (M7) [b3]
        { "218de5b3709555da3870ce859d1975e5", 1, 0 }, // Harvest Moon 64 (U) [f1] (PAL)
        { "21954e4e404d9e87dbdb87dd309f3e94", 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (V1.1) [!]
        { "2202b39b9017bb061822552af83fd331", 4, 0 }, // MeeTING Demo by Renderman (PD) [b1]
        { "22082b8d892a2560032fd4da5ab975a1", 4, MEMORY_PAK }, // FIFA 99 (U) [b1]
        { "220b85771e64c554514163120c2f82f4", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx (U) [t1]
        { "22134e81100d2854323b064edd98aac4", 4, 0 }, // Doctor V64 BIOS V1.41
        { "222f1e75f70a654b642382989e024749", 1, 0 }, // Star Wars - Teikoku no Kage (J) [o2]
        { "2249d931b5d2d4544eeb75e5b4136709", 2, 0 }, // Mace - The Dark Age (E) [b2]
        { "225223abf4bc1f3e38a45873ba94cebc", 1, 0 }, // Super Mario 64 (E) (M3) [b2]
        { "2257945eabc008e4d6464196b288d5bf", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (E) (M5) [b4]
        { "2258052847bdd056c8406a9ef6427f13", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina (J) (V1.2) [!]
        { "226127fdba21fc9019397ed2c5e5b19f", 4, 0 }, // CZN Module Player (PD)
        { "226c19c8759314ac740420ddc3a34eb4", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 99 (E) (M5) [!]
        { "229089089661f517c863242d6bb77746", 2, MEMORY_PAK }, // Rugrats in Paris - The Movie (E) [!]
        { "22a689c97faedf39a8f115db8f71830c", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (U) [b3]
        { "22aad544c062fb8a2dc6b2deb32de188", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [b1]
        { "22b86ab3f320a607899a0516c90a24d0", 4, RUMBLE_PAK | TRANSFER_PAK }, // Puyo Puyo 4 - Puyo Puyo Party (J) [!]
        { "22c7fcdbc74de237e0fc7778d8a6e671", 4, RUMBLE_PAK }, // Snowboard Kids 2 (U) [f1] (PAL)
        { "22e183bd8af57a79deb6679ca919b61e", 4, MEMORY_PAK }, // Mario Kart 64 (U) [t2] (Course Cheat)
        { "22faced432b0a9a4ec69a79452d16049", 4, MEMORY_PAK }, // Mario Kart 64 (U) [T+Ita1.0_Rulesless]
        { "22ff6c18eb028792ef350554b1e1fa68", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [o1]
        { "23033085561cd331cc81f0026fcb2ce2", 1, RUMBLE_PAK }, // Super Mario 64 - Shindou Edition (J) [b1]
        { "231bac1afb3de138072c2d697783059b", 4, MEMORY_PAK }, // Golden Nugget 64 (U) [!]
        { "231f0134d2899c08930f6e34682d2c0b", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.1) [b1]
        { "231f61e0d66a70b173bb22672ee67bd6", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (E) (M3) (V1.0) [t1]
        { "23215642f5faaad0761166ea018963f0", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (J) [o2]
        { "2322da2b9e7dc74678cd683b7a246b49", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF - War Zone (U) [!]
        { "232a5dc2e98c6cab1d9f73c1d1c1c73a", 0, 0 }, // Alienstyle Intro by Renderman (PD)
        { "23409668a6e6c4ece7b5fb0b7d0e8f2c", 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 2000 (J) (V1.0) [!]
        { "235511bbdb21af5a767bdb7502a80f06", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (F) [!]
        { "23a4ed8d79882594206173b1d476f0e9", 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (U) (M2) [!]
        { "23e5af30267036a69a8328f005df07b1", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [t1]
        { "23ee24faba0edfb04b5b0407e174496b", 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 6 (J) (V1.1) [!]
        { "241cf94bed487fff62ffb7b846da46ab", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (E) (M3) (Eng-Fre-Ita) [!]
        { "243f8c4067d50d994c1e883968de8c61", 2, 0 }, // Mace - The Dark Age (U) [b4]
        { "24467b76a192dfb67069c5a87b8606d0", 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [o1][t1]
        { "2449bb712a64e3363a6cbd56f5adeda5", 4, TRANSFER_PAK }, // Pocket Monsters Stadium 2 (J) [!]
        { "244bea64ea209990e9c69a830b507135", 1, MEMORY_PAK }, // Sim City 2000 (J) [!]
        { "245fd6d774b74d4703822511ca174a54", 1, 0 }, // Harvest Moon 64 (U) [b1]
        { "24658c0d8ac2c4fd58a74f13bca1d16a", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [b2]
        { "24a0d8c8cabc22116e469476ff6c691d", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey '98 (E) (M4) [!]
        { "24b023696975d27eae8319ef40001348", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (E) (M3) [b1]
        { "24b5bfa896802253d2b715fc760d1b3e", 2, MEMORY_PAK }, // Power Rangers - Lightspeed Rescue (E) [f1] (NTSC)
        { "24be2ccb0dea0755908c02bf67e22fe5", 4, TRANSFER_PAK }, // Pokemon Stadium (G) [!]
        { "24ca46d2c8e2d68978aeae46c3862f1e", 2, RUMBLE_PAK }, // AeroFighters Assault (U) [f1] (PAL)
        { "24e39d47a0d1afa138b1f39aa2daa648", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [t1][f1] (PAL)
        { "24e3ee6a54278db65c463804f2bb6223", 1, MEMORY_PAK }, // Bokujou Monogatari 2 (J) (V1.2) [!]
        { "24e6e064454de99c88eba9f8c3574af3", 1, RUMBLE_PAK }, // Star Wars Episode I - Battle for Naboo (U) [t1]
        { "25099eeb13e4fb47e390087ef6a20cbc", 1, 0 }, // Analogue Test Utility by WT_Riker (POM '99) (PD)
        { "2524e5fb8ed4bb8c831c5ac057e8f344", 1, RUMBLE_PAK }, // Yoshi's Story (E) (M3) [!]
        { "25258460f98f567497b24844abe3a05b", 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [!]
        { "2525e3734b921e662903e94bab119c32", 4, RUMBLE_PAK }, // Jet Force Gemini (U) (Kiosk Demo) [b1][f1] (Save)
        { "25620a9c9f372023a8ba862330e6373a", 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (U) [f1] (PAL)
        { "256564daf29311497917baaa14ed3f6d", 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) [T+Fre1.3_Corrigo]
        { "256a1cb23f9e1a2762a7a171417b5d68", 1, RUMBLE_PAK }, // Akumajou Dracula Mokushiroku - Real Action Adventure (J) [!]
        { "257057553d60c1dd1b7fdc5814cb21d9", 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (U) [f1]
        { "25b297143e9e5ccbb4b80a7fb6af399b", 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy's Reckin' Balls (E) [!]
        { "25bafc84ba4d87854dc44df3ef8764ea", 1, MEMORY_PAK }, // Premier Manager 64 (E) [!]
        { "25d19487cb80db724db9d735f2c28e8b", 2, MEMORY_PAK }, // AeroGauge (E) (M3) [h2C]
        { "25d9cdcd8c23af6559228c4435ff0cb3", 4, 0 }, // Doctor V64 BIOS V1.41b
        { "25e03763fc8a388a33871c58dfeb7c0e", 4, 0 }, // Tristar and Lightforce Quake Intro by Ayatollah & Mike (PD)
        { "25e39a8b45fddfa1989ce6662f87ef23", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (E) (M3) [b2]
        { "26087e0750fc6da0f3de2a431e34bddf", 4, 0 }, // Randnet Disk (J) [Rev. 00]
        { "2608a4d7a695d0b1a1bbc47695eace0e", 4, RUMBLE_PAK }, // Mario Party (E) (M3) [b1]
        { "26129419799db01bfe79f279ec43b334", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.1) [!]
        { "2616b2901f794eadfd26d5fa2bf5564d", 4, 0 }, // Congratulations Demo for SPLiT by Widget and Immortal (PD)
        { "2628818af2fee3c649aaf8627981a63d", 4, 0 }, // U64 (Chrome) Demo by Horizon64 (older) (PD)
        { "264b82f0fc2431d6eefde9c9f3ed7596", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (E) [!]
        { "266c0989ed0929df499389954779ea97", 4, MEMORY_PAK | RUMBLE_PAK }, // Battlezone - Rise of the Black Dogs (U) [!]
        { "26a1616667eec82288534c47b6927d45", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (U) (Debug Version) [f1]
        { "26b18d35984528ae2f90adbb2f2642f7", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (E) (M7) [!]
        { "26b5eaa13dc5b5e35307fe8c0cf5b6ba", 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (U) (M10) [!]
        { "26c3654d20b8718a75b5fe8da5b3284a", 4, 0 }, // Monopoly (U) [f1] (PAL)
        { "26c99a528862342791193ca6aaacc9dd", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (E) (M3) [b1]
        { "26cec7a56abb81f3468cfa26d8f16b67", 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy's Reckin' Balls (E) [o1]
        { "26f033c7716a10755a3d22c62acf6851", 4, MEMORY_PAK }, // Penny Racers (U) [hI]
        { "26f4ca20f7b9c88199ac046c57e282b4", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [!]
        { "26f7d8f4640ebdfa823f84e5f89d62bf", 4, MEMORY_PAK | RUMBLE_PAK }, // HSV Adventure Racing (A) [!]
        { "272b359d8f8ac48acbf053c262f422e4", 2, MEMORY_PAK | RUMBLE_PAK }, // Flying Dragon (U) [!]
        { "27369bf665ad2fe9f23c6c6d202aa5e8", 4, 0 }, // Doctor V64 BIOS V1.52
        { "2740f1b89ce7d78a5d31cf60c97a0c11", 1, 0 }, // Pilotwings 64 (J) [b1]
        { "2755152e6168516f2224adc6737b96f0", 2, MEMORY_PAK | TRANSFER_PAK }, // PD Ultraman Battle Collection 64 (J) [f1] (PAL)
        { "277d852df9c71be8dcba01ba8c862482", 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.0) [b3]
        { "278ef4b79b3544de327cf7dbcdd60081", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [f1] (PAL)
        { "279ec83bd60a3cce69a1db22b0a5c318", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (E) [!]
        { "27a7a2a19fc67346bb25e3c5bb3a91ce", 4, MEMORY_PAK | RUMBLE_PAK }, // Super Speed Race 64 (J) [o1]
        { "27c6f780f9032a531e9a322969cd9159", 4, 0 }, // Robotech - Crystal Dreams (Beta)
        { "27cb290fab6c32faa75eb0e53be73418", 0, 0 }, // Ultrafox 64 by Megahawks (PD)
        { "27d3efbd35b1f580330961480b988fb5", 1, MEMORY_PAK }, // Holy Magic Century (F) [o1]
        { "27e4c31540e18e3e8cea97bff744cc11", 2, MEMORY_PAK }, // Robotron 64 (U) [o1]
        { "27f61a55e27ea7f1ede4ca11966acc7c", 4, MEMORY_PAK }, // Frogger 2 (U) (Alpha) [o1]
        { "27f761b7dd73e12bb311772780d6b9f4", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t1] (Hit Anywhere)
        { "281e754123ec0cc688c4aa7c853de303", 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [t2] (No Damage-Unbreakable Wings)
        { "2838a9018ad2bcb8b7f6161c746a1b71", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.2) [!]
        { "2859090d78581e0925a3af8045e81e4b", 4, TRANSFER_PAK }, // Pokemon Stadium (E) (V1.0) [!]
        { "285d3277d28ca3b9a5a2be8971ccd767", 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (V1.0) [f2] (Save)
        { "28803dc0d1cd120e5c0285f7291626b8", 4, 0 }, // LCARS Demo by WT Riker (PD)
        { "288a514e98972bf9d329167aa29e66b6", 4, RUMBLE_PAK }, // Mickey no Racing Challenge USA (J) [!]
        { "28c2108a375f7731e719333a09439d2f", 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (E) [!]
        { "28c2373f6d831eec81f6146a809e701b", 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (U) [!]
        { "28f8f7d8423b291e3056047c62e2937a", 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [t1]
        { "28fe768ced3c87659b343a7811cfac0a", 2, MEMORY_PAK }, // Rakuga Kids (E) [b1]
        { "29047fba820695bd14c5bd7aa1aa4400", 1, 0 }, // Super Mario 64 (Ch) (iQue) [!]
        { "290be44b47110a011523a343786a15a0", 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 64 (J) [f1]
        { "2953091d648ed1b60a67200b17a9410d", 4, 0 }, // VNES64 + Test Cart (PD)
        { "295faa9b7cc7127bc82aea90adb211e5", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [h1C]
        { "2991bb68eca54813d6b834adbbbacc4c", 2, 0 }, // Powerpuff Girls, The - Chemical X-Traction (U) [!]
        { "29974692808c112b306fbd259273dc96", 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater 2 (U) [!]
        { "29b79bf5812e5f9e5ecef073d59f8915", 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (E) (Prototype)
        { "29bc5c1a24d3979d376ad421000ac9cb", 2, MEMORY_PAK | RUMBLE_PAK }, // Goemon's Great Adventure (U) [!]
        { "29e0ab31b3fc6a6b1efc11fbdb82cb97", 4, MEMORY_PAK | RUMBLE_PAK }, // Jeremy McGrath Supercross 2000 (U) [f1]
        { "29e85b9d9072ac232ceb87efe826269a", 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [t2]
        { "29e875c7ffa44ca629f2576cb47a1020", 4, 0 }, // SNES 9X Alpha by Loom-Crazy Nation (PD)
        { "2a0a8acb61538235bc1094d297fb6556", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [!]
        { "2a0bf5a9a136d57af01d199b16899634", 2, MEMORY_PAK | RUMBLE_PAK }, // City-Tour GP - Zennihon GT Senshuken (J) [!]
        { "2a64632c71ffb90df3ae83836dedd7a6", 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.1) [T+Ita_Cattivik66][b1]
        { "2a8086945375826fa6495f1ef2f1dfb6", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.0) [b1]
        { "2abcd1ad41b3356fbc1018ecb121283e", 2, MEMORY_PAK }, // Robotron 64 (E) [!]
        { "2abe36754e866b9b6c4bdcffc1d11abf", 4, RUMBLE_PAK }, // Wheel of Fortune (U) [!]
        { "2ae35bdf163613024d876a09f25063f3", 1, 0 }, // Yuke Yuke!! Trouble Makers (J) [!]
        { "2aee59cc8dda4382969bb76dc97d4228", 4, 0 }, // Vivid Dolls (Aleck64)
        { "2b1d9f38389522a372d950e1bc6fa13f", 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (U) (M5) [t1]
        { "2b2186a38b0b3541c3227144ea3c71a3", 4, 0 }, // SRAM Upload Tool V1.1 by Lac (PD)
        { "2b417ad282db321c9c67eda7131a7f06", 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (E) (M3) [f1] (NTSC)
        { "2b60ef777f759b4f0e19ddf3cb84d9ab", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.0) [b1]
        { "2b7370e6880f83c808a2cb9e7f33dc46", 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (U) [t1]
        { "2b86775ea4d848202e4f4a39c33571ca", 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [!]
        { "2bb149a583fdefea96805f628fe42fd9", 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.1) [!]
        { "2bbd484bfa0f16c09c7f13fbdc2637c6", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.0) [b4]
        { "2bc48b3e6f61896b9bc7bef5205cc49c", 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (G) [!]
        { "2bd614a2a1a317ecbeeb4193954c5fa5", 2, MEMORY_PAK | RUMBLE_PAK }, // City-Tour GP - Zennihon GT Senshuken (J) [b2]
        { "2bde49f2855030de342976c9a95b81b3", 4, MEMORY_PAK }, // Ganbare Goemon - Mononoke Sugoroku (J) [!]
        { "2c27b4e000e85fd78dbca551f1b1c965", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (GC) [!]
        { "2c618f6c69c3b4803f08762a03835139", 1, RUMBLE_PAK }, // Bass Rush - ECOGEAR PowerWorm Championship (J) [!]
        { "2c68a2d12c1ebfa2ad8f0be1a09aeee7", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [b7]
        { "2c6a75570997ba7a1244950ac8fe6cbf", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [ba]
        { "2c6d146a8473511cfcfbbe8518f49d71", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (U) (M7) [!]
        { "2c6fdcf4a6a6614d09224efaa55fb531", 4, RUMBLE_PAK }, // 64 Trump Collection - Alice no Wakuwaku Trump World (J) [h1C]
        { "2c944319017a21652566cc4196c714cc", 4, MEMORY_PAK }, // Choro Q 64 (J) [b1]
        { "2c94a246e701d667ba807dab6c9771e2", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (G) [!]
        { "2ca82f82284d2b3fe381999889350855", 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (U) [t1]
        { "2cf9568a149177ac0b86378fbc8dcb71", 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (U) [f1] (PAL)
        { "2cf9edb51ada9de2ae7ad9fd5acc5580", 4, MEMORY_PAK | RUMBLE_PAK }, // 64 Oozumou (J) [!]
        { "2d30fbe45c2858957f9efdb5e9000033", 4, MEMORY_PAK }, // Brunswick Circuit Pro Bowling (U) [o1]
        { "2d6abf559a9aae65bbeb0e60b3f82d1b", 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [f1] (PAL)
        { "2d727c3278aa232d94f2fb45aec4d303", 1, RUMBLE_PAK }, // Super Mario 64 - Shindou Edition (J) [!]
        { "2d79c395765a601f967cd3ef7cbf439e", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.2) [b1]
        { "2db6ed9a6a244cb209ffdf52d9006e4d", 4, MEMORY_PAK }, // Golden Nugget 64 (U) [b3][f1] (PAL)
        { "2dce0f6556c00b44367586fd1eeb40e5", 1, 0 }, // Alleycat 64 by Dosin (POM '99) (PD) [b1]
        { "2de4d0f0788871cc4bb6d30b60f72184", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (E) (GC) [h1C]
        { "2df942636fb2bf924844a8f4129913be", 4, 0 }, // Doctor V64 BIOS V1.31
        { "2e2c990d8bbaee7e0681fdc89c29c0f3", 4, 0 }, // Ultra Demo by Locke^ (PD)
        { "2e458d7cc355d7918493b0e0362c9a20", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.1) [!]
        { "2e50bc853f6efc1e0d5e4679739f43c4", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [t2]
        { "2e5fd9303138e8f558bf67bb9e799960", 4, MEMORY_PAK }, // Jikkyou World Soccer - World Cup France '98 (J) (V1.2) [!]
        { "2e8367ed95724816646131bad480bdd2", 4, 0 }, // SRAM Manager V2.0 (PD) [h2C]
        { "2e95e5833894a6e1e3d109e3b3d809f1", 4, RUMBLE_PAK }, // F-ZERO X (E) [b3]
        { "2ee917366f64a06472d7622a2a05990e", 1, 0 }, // Mischief Makers (U) (V1.1) [!]
        { "2eea8d20bef26f88a5e82fdd39f87e75", 4, 0 }, // Sydney 2000 (U) (Prototype)
        { "2ef9fa16de2a09ea15b6289447f40490", 2, 0 }, // Pokemon Puzzle League (E) [!]
        { "2f02d478fc89e46fa143b128e29b434c", 0, 0 }, // Absolute Crap Intro #1 by Kid Stardust (PD) [b1]
        { "2f2163c53db135792331df00398b3f87", 1, 0 }, // Glover (E) (M3) [!]
        { "2f4c29dd26b6d5311f3d46063a393687", 4, 0 }, // Memory Manager V1.0b by R. Bubba Magillicutty (PD)
        { "2f4e88c48823714825f35148f5b5738d", 4, 0 }, // Doctor V64 BIOS V2.03 (Blue)
        { "2f6b0acb7270f78c972f5e6d4c75b02b", 4, 0 }, // PGA European Tour (E) (M5) [f1] (NTSC)
        { "2f8109e5c9bcc45b9987f31ea00eca66", 4, 0 }, // Sample Demo by Florian (PD)
        { "2fa1ac0a989d5f081d6a8733f2330923", 4, 0 }, // SRAM Upload Tool + Star Fox 64 SRAM (PD)
        { "2facdf2f3e52ee8a603de79348e441e6", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [f2] (Z64-Save)
        { "30194320b54ff67cb0cda5ddb061d933", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [b6]
        { "30413678ce47e1a2eb8da80af0d768f5", 4, 0 }, // Famista 64 (J) [b7]
        { "30486a26c7b87cdbdb1a421493cc745a", 2, RUMBLE_PAK }, // StarCraft 64 (Beta) [f2] (PAL)
        { "304f496ce0858959700c7f71e55c55b7", 1, MEMORY_PAK }, // Jangou Simulation Mahjong Do 64 (J) [b3]
        { "308384eeeb6a88b5b498179e7484944f", 4, RUMBLE_PAK }, // F-ZERO X (E) [b4]
        { "30aae8a534c72ffc89343e8ef0f1c5a1", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [t2]
        { "30b2e1f7f3afdc5d8e6a42c8885aad93", 0, 0 }, // Mandelbrot Zoomer by RedBox (PD)
        { "30bdf5405a8c615dc924db9480e996b7", 1, 0 }, // Star Wars - Shadows of the Empire (E) [o1]
        { "30c6676ec1d62122f4e7607ef3abbd41", 4, RUMBLE_PAK }, // WCW-nWo Revenge (E) [!]
        { "30d7d730e8c131c77ec6f6390b9eb8b3", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.11
        { "30e23d3de446e37e5e7fbef6794a6fc9", 4, MEMORY_PAK }, // J.League Eleven Beat 1997 (J) [!]
        { "30e25793acc95529f8c69a55c6679e73", 4, 0 }, // Doctor V64 BIOS V1.04
        { "30e7e083b978408d5b7760d0ce4dc61d", 4, 0 }, // Clay Fighter - Sculptor's Cut (U) [!]
        { "30ec0eea5c487e5609d9f9f356d21f27", 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater (U) (V1.0) [t1]
        { "30edcca84850e3b4129936350a9b6b8b", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (E) [o1]
        { "310659115e9939f219a783abdd456ce9", 2, MEMORY_PAK }, // Wave Race 64 (E) (M2) [!]
        { "313f1541748acf01c4355ba471043868", 4, RUMBLE_PAK }, // Super Smash Bros. (U) [b2]
        { "3141a1ee8dcde0f772e915e629823f13", 4, 0 }, // Donchan Puzzle Hanabi de Doon! (Aleck64)
        { "31448b07cbc932018870f71d1254e317", 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (V1.0) [o1]
        { "3152b82c03fc7b928d3d5c43f95aef51", 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (E) (M3) [h1C]
        { "3166be5787bc30526e71277eca3091df", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.2) [o1]
        { "316c59dae45c20250a0419a937e7d65b", 2, MEMORY_PAK | RUMBLE_PAK }, // G.A.S.P!! Fighter's NEXTream (E) [!]
        { "319377a6d811ef8ad7b3e16b537e6346", 4, 0 }, // Z64 BIOS V2.00b (Ravemax Hack)
        { "3194c65208047c938ef443325e1e73c1", 4, 0 }, // Sitero Demo by Renderman (PD)
        { "319816aaa30e512827be7b7f81f80d86", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (E) (M4) [!]
        { "31b4a8ed52b48e756b344c9f22736e50", 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (E) (M6) [!]
        { "31bb57c1fad0d47dc2353c1950b11886", 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (U) [!]
        { "31e739e6420da88abf4980ad0c76439a", 2, MEMORY_PAK }, // AeroGauge (U) [h2C]
        { "31ee2de8e65e30f5934c450dbaa924f0", 4, TRANSFER_PAK }, // Pokemon Stadium (E) (V1.1) [!]
        { "31f08f1d2f789c5d698b7e05f287e7ae", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (E) [o1]
        { "31fb88048076ace4bd4205c5f40414ab", 4, MEMORY_PAK }, // Centre Court Tennis (E) [!]
        { "3207bf22e305c488109b09a03706f36f", 4, 0 }, // Clay Fighter 63 1-3 (U) [!]
        { "32257bfffd9b2d680f582e148e9b0611", 4, MEMORY_PAK | RUMBLE_PAK }, // Kira to Kaiketsu! 64 Tanteidan (J) [!]
        { "325b9dcd5d23f269017c16e2ba62dea3", 4, 0 }, // NBC-LFC Kings of Porn Vol 01 (PD) [a1]
        { "326bf85c5ef158205364710454ee1af1", 4, MEMORY_PAK }, // 64 de Hakken!! Tamagotchi Minna de Tamagotchi World (J) [h2C]
        { "326dc227fccaa4975583e63624ddfda1", 4, RUMBLE_PAK }, // Super Smash Bros. (U) [f1]
        { "329c0a564d3b4477e2887097389ab7d6", 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [b1]
        { "334d4e8443a67e964649b28428c8f1ae", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.0) [b1][f1] (PAL)
        { "336ae63bd1ebacc64d9d92370b555537", 1, TRANSFER_PAK }, // Mario Artist: Talent Studio (J) [CART HACK] [T+Eng0.1_LuigiBlood]
        { "338faa6e250f72f1b1d7b873bfaee918", 1, 0 }, // SM64 Splitscreen Multiplayer Beta
        { "33b9dddd6d7039ce1c9e6773056a65c8", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [b4]
        { "33cbc59786063285dc3c090e0ed312e3", 4, 0 }, // Randnet Disk (J) [Rev. 01]
        { "33cf09d5eab2fb1242108a172e1844c2", 4, 0 }, // Textlight Demo by Horizon64 (PD)
        { "33efc323aa05f522cef2324cc1d3f5dd", 4, 0 }, // U64 (Chrome) Demo by Horizon64 (PD)
        { "33f2bc6847985f96de8177147dcd3b85", 2, 0 }, // Custom Robo (Ch) (iQue) [!]
        { "33fb7852c180b18ea0b9620b630f413f", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina GC (J) (GC) [!]
        { "33fdab9712d9fea793a3ae44293999c3", 1, 0 }, // Pocket Monsters Snap (J) (VC) [!]
        { "3406a505c22bac2f40d9bfc6ff08cf86", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx (U) [!]
        { "3419ec10ee470f1888811d4a9b5faace", 4, 0 }, // Ghemor - CD64 Xfer & Save Util (CommsLink) by CrowTRobo (PD)
        { "342476d20d7a7bf4c5484d2defcc79e7", 4, MEMORY_PAK | RUMBLE_PAK }, // Bassmasters 2000 (U) [f1] (PAL)
        { "34489365b550f32c97337d86d52d8c84", 4, MEMORY_PAK }, // International Superstar Soccer '98 (E) [!]
        { "34779af3681f58e4f642f03eaef198f9", 2, TRANSFER_PAK }, // Robot Ponkotsu 64 - 7tsu no Umi no Caramel (J) [f1] (PAL)
        { "34957b173cd33c999b954e7034fc01ea", 1, RUMBLE_PAK }, // Chopper Attack (U) [b3]
        { "349f61f9747f2d2098f305924c97a1bf", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Blades of Steel '99 (U) [!]
        { "349ff1b4ae28e31a2b2dc93d76a5219e", 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (U) [b3]
        { "34ab1dea3111a233a8b5c5679de22e83", 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (E) (M3) [!]
        { "34af9f96de01a7df6b296598908bb398", 4, 0 }, // RPA Site Intro by Lem (PD)
        { "34e6b35260f03ca72593fa373e38bbea", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) (Kiosk Demo) [b2]
        { "34f5f402064f76b375771ebf80dbe734", 1, RUMBLE_PAK }, // Neon Genesis Evangelion (J) [b1]
        { "34f623903c3e2dbf8fb1e47f688d1859", 1, MEMORY_PAK }, // Saikyou Habu Shougi (J) [h4C]
        { "3509a4e21e43cedbd704f8a1192c31df", 4, 0 }, // Doctor V64 BIOS V1.02
        { "354e8a1bc64683f01fe598f51d5ab4be", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.30
        { "3559be56b70bafb77c900516b909bfef", 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ita) [f2] (NTSC)
        { "35662cfd07fd6af4bab90ca23f7c98e6", 4, MEMORY_PAK | RUMBLE_PAK }, // International Track & Field 2000 (U) [!]
        { "356a5d3d59e0adef6efae6096fc20f77", 4, 0 }, // StarCraft 64 (G) (Prototype)
        { "358941ca393f6d7415e99b28d49ccf68", 1, RUMBLE_PAK }, // Dezaemon 3D (J) [b2]
        { "35a01aeb01f3184dd20904af28fa71c6", 2, MEMORY_PAK | RUMBLE_PAK }, // Polaris SnoCross (U) [t1]
        { "35ba407ea9e4ef7c0ace8b4f58beec41", 4, 0 }, // Action Replay Pro 64 V3.0 (Unl)
        { "35bbaf4869d0b0d2147d35f1ec3cf2ca", 1, 0 }, // Super Mario 64 (U) [T-Ita1.0final_beta1_Rulesless]
        { "35bf5c390af1ad730e00eb618aed7b72", 4, 0 }, // Virtual Springfield Site Intro by Presten (PD)
        { "35cf26d4d33717ba730d7978e2f2107d", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.1) [f1] (Z64)
        { "35e039f8e79843917d02be06d00c457b", 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.3) [!]
        { "35e9ece11306ef7d8f5f09f65761d365", 4, MEMORY_PAK }, // Hexen (G) [h1C]
        { "35f3928f12da627db79edfc96b93f8d3", 4, 0 }, // Famista 64 (J) [b4]
        { "35f6c42d5a9284688284c24250f4d6be", 2, RUMBLE_PAK }, // F-1 World Grand Prix (F) [!]
        { "35fab2cc64e0e808eb629a25542c7326", 4, 0 }, // Pip's RPGs Beta 7 by Mr. Pips (PD)
        { "35fda68c8481f7c3419ccd91edbb6a9e", 4, 0 }, // Worms - Armageddon (E) (M6) [f1] (NTSC)
        { "360044d5675a328e62191250f43b6671", 2, MEMORY_PAK }, // Robotron 64 (E) [h1C]
        { "360dc6be6d06dca12e53c077ac0d2571", 2, MEMORY_PAK }, // Fox Sports College Hoops '99 (U) [!]
        { "3611779f29997267622cbc80e2d087cc", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 - World Cup heno Michi (J) [!]
        { "361f65d37f41536cde98f83cad456217", 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (U) [o1]
        { "3620674acb51e436d5150738ac1c0969", 1, RUMBLE_PAK }, // Mega Man 64 (U) [!]
        { "36295982651a9974eb7342b1dd5245e1", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.0) [o1]
        { "362d10c7d270ae48d83d1d7bb78bf527", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (E) (M3) [b1]
        { "36759c85694a4cf5a1f089a4136dae2b", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [T-Ita0.9M_Vampire]
        { "3682aeba1da191a1cb2c513b1f834f70", 4, RUMBLE_PAK }, // Chameleon Twist (E) [f1] (NTSC)
        { "36b3d908fb50e5521defbeb2c39f47b0", 4, 0 }, // Dr. Mario 64 (U) [T+Jap1.0_Zoinkity]
        { "36bedfd15cb70478916baa469491d80d", 2, RUMBLE_PAK }, // Magical Tetris Challenge (U) [hI]
        { "36c97c12ddabb02d7d2d3f6b33b3d83f", 1, MEMORY_PAK }, // Blast Corps (E) (M2) [b2]
        { "37149c4064a1d120b9f4f5482a915262", 4, 0 }, // Shuffle Puck 64 (PD)
        { "371620871660767900fc00f72681e1fd", 4, MEMORY_PAK | RUMBLE_PAK }, // Wayne Gretzky's 3D Hockey (J) [b1]
        { "37260287d59fe4ec6049c1d22b5614e6", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.1) [!]
        { "3728be19fdcfc3ecebd8c7504c13d1f5", 4, RUMBLE_PAK }, // Chameleon Twist (E) [o2]
        { "372f76c0dbe0be41f5a0a47252869e8e", 4, 0 }, // Friendship Demo by Renderman (PD) [b1]
        { "373fbbf4b909e7c8a3aab3dd80debd11", 1, 0 }, // Pokemon Snap (U) [f2] (Save-PAL)
        { "374fd81c3e8e858eeb2226501659b333", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [b2]
        { "376803f28ca8b2133671783419933ca2", 4, MEMORY_PAK }, // International Superstar Soccer 64 (E) [!]
        { "376ffec460443409bc29bb815abfb42a", 4, MEMORY_PAK }, // FIFA Soccer 64 (E) (M3) [o3]
        { "377b9c40ebe4f4cf845e577a8dc9accb", 2, MEMORY_PAK }, // Robotron 64 (U) [f1] (PAL)
        { "377daf6edd0c996422d2a9c8c5dec033", 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (U) [f1] (Country Check)
        { "378c6ce13df3363f77d76b64c8dab287", 2, MEMORY_PAK }, // Rakuga Kids (J) [b1]
        { "37954ba9067840faff851092043f0435", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 98 (E) [o1]
        { "37962cb6d58c2d6b044eaecdcf70ec09", 4, 0 }, // All Star Tennis '99 (E) (M5) [f2] (NTSC)
        { "3796829f54958ce103dcf5e3e8eb80b4", 4, MEMORY_PAK }, // Onegai Monsters (J) [!]
        { "37a4f634cadb89dc3e43d389ed050925", 4, MEMORY_PAK }, // International Superstar Soccer 64 (U) [h2C]
        { "37aa3a354fbe508f5449e0c515083128", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.0) [b1]
        { "37b24ee746a1efd54730b5e24551e8cb", 4, MEMORY_PAK }, // FIFA Soccer 64 (U) (M3) [o1]
        { "37b430ee16167831c6c6292994f93277", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (G) [!]
        { "37c0c97dfce99f9968778448f5c9f0b6", 4, 0 }, // Wet Dreams Main Demo by Immortal (POM '99) (PD)
        { "37c36e4286d36892a9fc70eafe4104be", 4, 0 }, // 64DD IPL (USA)
        { "37eb3d60a2e85be5077f73a5df651f05", 4, RUMBLE_PAK }, // Mario Party (E) (M3) [h1C]
        { "37f7f2f2cfb488e48a2706fc8c4b2dd5", 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [f1][h1C]
        { "37feb7178712980dd0953d195187c35d", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t3] (All Attacks Hurt P2)
        { "3803f70982907c7c24713a7f9742b6cd", 4, 0 }, // MAME 64 Demo (PD) [b1]
        { "382eca33c5dd52b8c03a56d20f2d3f21", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (U) [f1] (PAL)
        { "388440013641887d85b791cf01729fa8", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (G) (V1.1) [!]
        { "38a82a56ae61a4d354c6a26e64d25e1c", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (E) [!]
        { "38b0db4916328672ed592759ffb1f09c", 2, RUMBLE_PAK }, // F-1 World Grand Prix (G) [h1C]
        { "38d04a1c192d591940437b6baf628119", 4, 0 }, // Neon64 V1.2a by Halley's Comet Software (PD) [o1]
        { "38dcf67cba39a84108adf48ee53544b0", 4, 0 }, // Display List Ate My Mind Demo by Kid Stardust (PD)
        { "38f004f68d5f1abbc4391d156debcaef", 1, RUMBLE_PAK }, // Yoshi Story (J) [f2]
        { "390fc46621581fa62f7f4cc3537f255d", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.0) [o2]
        { "3965f14d450f019beb8591aa10afc2bf", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.30 (Odd Bytes)
        { "397a69aa3829ea5fecb4cdf399084e31", 1, MEMORY_PAK | RUMBLE_PAK }, // Harukanaru Augusta Masters 98 (J) [b1]
        { "397be52d4fb7df1e26c6275e05425571", 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.0) [!]
        { "39a2bca1c17cd4cf1a9f3ae2b725b5c6", 2, 0 }, // Mace - The Dark Age (U) [!]
        { "3a1488c2a8a8f2ab9368d7c056eb02a2", 2, RUMBLE_PAK }, // Dance Dance Revolution - Disney Dancing Museum (J) [o1]
        { "3a314ce9388e4178fd6fc9c74d5e7137", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (U) [b2]
        { "3a5edf7256ea5e7af4b451e07cc54003", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (E) [b1]
        { "3a67d9986f54eb282924fca4cd5f6dff", 4, MEMORY_PAK }, // Mario Kart 64 (U) [!]
        { "3a6a249843ddeafffd334ab3c5931f8d", 4, 0 }, // Spacer by Memir (POM '99) (PD) [t1]
        { "3aa263aca66f3a07bb081b575d66deeb", 4, MEMORY_PAK }, // World Cup 98 (E) (M8) [!]
        { "3aa7d5f55e3e5e6c7419cc1625af69bc", 4, 0 }, // GoldenEye 007 (U) (Frozen Enemies Hack)
        { "3aa8c8c023f6ab8384cbdd412decf977", 4, MEMORY_PAK }, // Mario Kart 64 (U) [t1]
        { "3aaf74d7e6d560c9cb1331209ad00f22", 4, 0 }, // Pamela Demo - Resized (PD)
        { "3abc2d34ad3ce623f4ed8f126d30cc80", 4, 0 }, // Super Smash Bros. (Ch) (iQue) (Manual) [!]
        { "3ae32658b839ffa3189e3ca84e0b884a", 4, 0 }, // F-ZERO X (U) [f1] (Sex V1.0 Hack)
        { "3b21996f8c6496903c01dcbd77ddd6ee", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (U) (M2) [f1] (PAL)
        { "3b2615d754a61e45b1034d555d830a78", 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (J) [!]
        { "3b2ca0da0810c95b31df8c1fb8811be2", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (E) (M5) [!]
        { "3b41bad41abf4512fbf62727410eac71", 1, RUMBLE_PAK }, // Yoshi Story (J) [t1]
        { "3b4330445d9aae1ef9c56b1393e281e3", 4, 0 }, // BB SRAM Manager (PD)
        { "3b5691b895b415056e4ea69695dc26b4", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f1] (DS-1)
        { "3b6994b373bec6e917f0b834aa6d1a46", 1, 0 }, // Heiwa Pachinko World 64 (J) [b3]
        { "3b6dd7b60437234895500beff28df6d6", 1, MEMORY_PAK }, // Scooby-Doo! - Classic Creep Capers (E) [!]
        { "3b6ff1829cbbeae87704f53950791d64", 4, MEMORY_PAK }, // Hyper Olympics in Nagano 64 (J) [b1]
        { "3b8585ed03e8ddb89d7de456317545e7", 1, RUMBLE_PAK }, // Body Harvest (U) [!]
        { "3b86a6c5b478a0d33a6547d3ea276afe", 0, 0 }, // Fire Demo by Lac (PD)
        { "3baff04c5dd367f362248dc627d848c1", 1, MEMORY_PAK }, // Donald Duck - Quack Attack (E) (M5) [f1] (NTSC)
        { "3bc50511de8598a49e79f4cf2e4cf8e0", 4, 0 }, // GoldenEye 007 (U) (God Mode Hack)
        { "3bd16f3ddb0dd890412bfe1e59ca5463", 1, MEMORY_PAK }, // Mahjong Master (J) [o1]
        { "3bd42f6aec477c056e1afebb3515495c", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (Kiosk Demo) [!]
        { "3bd4ee76d4cbb14d69ba628533bd7cbc", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.2) [b2]
        { "3be170e7e94a03bd4e36732eb137ee39", 4, MEMORY_PAK | RUMBLE_PAK }, // Getter Love!! (J) [b1]
        { "3c02f56dd7b1a06be83a7a288755612f", 1, MEMORY_PAK }, // Wonder Project J2 - Koruro no Mori no Jozet (J) [T+Eng1.0_Ryu]
        { "3c28c951b1ea46690065dc32862272e4", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [b1]
        { "3c9f329d5e0c7fe57355b8dc68f79331", 4, RUMBLE_PAK }, // Monster Truck Madness 64 (U) [t1]
        { "3ca46ad43f9b62df409759d56468f18e", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [f3] (NTSC)
        { "3cb88b934572e7520f35e5458798775b", 1, RUMBLE_PAK }, // Star Wars Episode I - Battle for Naboo (U) [!]
        { "3cbb8012f1256a37d5284a85b859cc8b", 4, 0 }, // Tower&Shaft (Aleck64)
        { "3cdd87026efec9a03648d225f97858a5", 1, 0 }, // Harvest Moon 64 (U) [T+Pol001]
        { "3cee1895a80005940887c9830b09dde6", 2, RUMBLE_PAK }, // F-1 World Grand Prix (G) [o1]
        { "3d1c87bcf3f8ae8e55fc87a25be4173c", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [t1]
        { "3d1e03b097f2124f8f713013d8219291", 4, 0 }, // Die Hard 64 (U) (Prototype) (Level 2)
        { "3d22d5bd7997293612ecdd3046beba13", 2, RUMBLE_PAK | TRANSFER_PAK }, // Transformers - Beast Wars Metals 64 (J) [!]
        { "3d2cca1b2e3bde267769812219edb864", 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.0) [b1]
        { "3d3573a855835a98de29d598c35590e0", 1, MEMORY_PAK }, // Tonic Trouble (E) (M5) [!]
        { "3d3855a86fd5a1b4d30beb0f5a4a85af", 1, RUMBLE_PAK }, // Banjo to Kazooie no Daibouken (J) [!]
        { "3d38bcd5b57ceecb0985609cce940ffa", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [t2] (Boost)
        { "3d4c7b11076bafa4620bcc154c0eeef3", 1, MEMORY_PAK }, // Hamster Monogatari 64 (J) [!]
        { "3d5f017d75679cefce2127cacc46c9f2", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (U) [o1]
        { "3d6c792ed4fa3ce5c92013f15bccd327", 2, MEMORY_PAK }, // AeroGauge (U) [b1]
        { "3d78b907657918c8c58ea002feee1d0f", 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [b3]
        { "3d8f438057cb98f3c9e1472abff61610", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (U) (M2) [f2] (PAL)
        { "3da2c5fa29d11d0481b60f6c6b14393a", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [t2] (Health and Eggs)
        { "3dc541c0b97db9c95a6aa8c2ded27f4a", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) [t2]
        { "3dcb15043063bd656a0223c519218cfb", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [!]
        { "3dcd5e15aa35a73c68db4f56e2670fa2", 4, 0 }, // Star Fox 64 (Ch) (V4) (iQue) (Manual) [!]
        { "3de02cf363e4d081530fef53fdab5780", 4, 0 }, // Game Boy 64 (POM '98) (PD) (Illegal Mode Enabled)
        { "3df1646cc28c04133b3cb5eb143ae016", 4, 0 }, // Pip's Porn Pack 2 by Mr. Pips (POM '99) (PD)
        { "3df7f2af3e7b1f2fd55c2c02125f0130", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [f1]
        { "3e003a77f044f0c97748be6861c55038", 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [f4] (Nosave-Z64)
        { "3e217e9c70608e56da20feac9a458efc", 4, 0 }, // NUTS - Nintendo Ultra64 Test Suite by MooglyGuy (PD)
        { "3e2ad600bd89b21531f59a84839fe732", 4, 0 }, // Kuru Kuru Fever (Aleck64)
        { "3e60ca693babff1f62cf8c0aec338556", 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.0) [b1]
        { "3e660d3f991c0529e90bfec0244db31a", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (U) [!]
        { "3e6ff36ae59f9261ed275feabbf76ced", 4, MEMORY_PAK }, // Simon for N64 V0.1a by Jean-Luc Picard (POM '99) (PD)
        { "3e972e3340bed6e47d3c515dd10eaeda", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [b4]
        { "3e973b5bbef8b24a4c4d7c326955e71f", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (U) [h1C]
        { "3ea21256ddc4157c3231ae5cc9c4652a", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [!]
        { "3eb732a8d004263ad8eb0da59a29582a", 2, RUMBLE_PAK }, // StarCraft 64 (Beta)
        { "3ec0471e2cbee17471ddbf80c56606d5", 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.2) [!]
        { "3ec3f83eab22702e146c467eb1db45fa", 2, RUMBLE_PAK }, // Super Robot Spirits (J) [!]
        { "3ee5ef09471ed2adf02e6e1ba8d17963", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [b3]
        { "3f1a1b941e58ff977ef47b65ce359a26", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) (Kiosk Demo) [f1] (PAL)
        { "3f1cf2d6c1580fdccd662f9fcf8c4214", 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (U) [t1][f2] (PAL-NTSC)
        { "3f2802f12058b4511a0778a891050297", 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (U) [b3]
        { "3f39c01455b585caeb2df8a09765d0b3", 4, TRANSFER_PAK }, // Pocket Monsters Stadium 2 (J) [f2]
        { "3f3ebfad624d623fafef767b99609e15", 1, 0 }, // Super Mario 64 (U) [t1] (Invincible)
        { "3f40f37b0464dd065067523fb21016dd", 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Captain Buzz Lightyear auf Rettungsmission! (G) (V1.0) [!]
        { "3f4b73963abc91cee59c416063efd4ae", 1, MEMORY_PAK | TRANSFER_PAK }, // Super Robot Taisen 64 (J) [!]
        { "3f50712011be1cb59b45684d7d61f11e", 2, RUMBLE_PAK }, // Ganbare Goemon - Derodero Douchuu Obake Tenkomori (J) [t2]
        { "3f556cc3b3a996cd2f471fa0d992d529", 4, RUMBLE_PAK }, // Mario Party (J) [!]
        { "3f5ca81183ea0e69f35417f6bbd7c5ac", 1, RUMBLE_PAK }, // Lode Runner 3-D (U) [f1] (PAL)
        { "3f64b4f72e61225ef3ae93976c9bfc7c", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [!]
        { "3f88078e2d9dbf6c9372f6373cf9ae09", 2, 0 }, // Tetrisphere (U) [!]
        { "3fc4d3187435443455f8355b2d3f8934", 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (U) [!]
        { "3fcd4969f9a080bd2bcb913ec5d7a3bd", 1, 0 }, // Pilotwings 64 (E) (M3) [!]
        { "3fdb2e5f9982fda2c2344a883b8ab6ef", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (E) [!]
        { "3fe076d5992c53de6e095e53e53d9bc5", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Rus099wip]
        { "3fe2b38e18152162a34e3002dea071f7", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.2) [b1]
        { "400a14f132d993f5544f8b008ec136fa", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (E) (V1.1) [!]
        { "401c0dbdf49a64294daa2bc8607245dd", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (V1.0) [t3]
        { "4022b2a0f22218870764cdae73580762", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [b8]
        { "4024477aaed7dd5ff5ea60bf568123b7", 4, 0 }, // F-ZERO X (Ch) (iQue) [!]
        { "406b08987ab92d73d72b597ec6b11bd9", 2, MEMORY_PAK | RUMBLE_PAK }, // Razor Freestyle Scooter (U) [!]
        { "4076973cfda277fc876e9f066cc73deb", 4, 0 }, // Star Wars - Shadows of the Empire (U) (Prototype)
        { "407a1a18bd7dbe0485329296c3f84eb8", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (U) (M2) [!]
        { "4086114ffb03b156dbcaa0364eb0f81b", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [b2]
        { "40866bbe1629e9b170de552aca83d967", 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (U) [t1]
        { "40aa8ec52c5e025d6c6ad94cebc2218f", 4, 0 }, // Dezaemon 3D Expansion Disk (J) (Proto)
        { "40cbc5a23b7d8c1fdd47d78caca29248", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [b2]
        { "40cc2085a5c12456bef830b047068326", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (E) (M3) [!]
        { "40e03eda831c01e0a12294287fd240e2", 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.1) [!]
        { "40e74da10255dcf00a45f02bed1afcad", 4, 0 }, // Z64 BIOS V1.11
        { "40e98faa24ac3ebe1d25cb5e5ddf49e4", 4, RUMBLE_PAK }, // Banjo-Tooie (U) [!]
        { "40f4a3a5fe703527e03c5747bc0a2fd6", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Spa1.0_eduardo]
        { "41010b4bfe1f8db801237b02a9fa265f", 4, 0 }, // Doctor V64 BIOS V2.03
        { "410146ea14cf34d134bc97c78522d322", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (E) [h1C]
        { "4111482c92ee806484aaa2c210893a52", 1, RUMBLE_PAK }, // Mission Impossible (G) [!]
        { "4116cf435db315a2481af8d1e9352feb", 1, MEMORY_PAK | RUMBLE_PAK }, // Nightmare Creatures (U) [!]
        { "4116e492168aafff1bd3100c7b0aa28f", 1, MEMORY_PAK | RUMBLE_PAK }, // O.D.T. (U) (M3) [!]
        { "4121dd79cebf67ebca1aa2b3a7aed858", 4, 0 }, // Doctor V64 BIOS V1.83
        { "41417fce2b37eaae787c5a845a0015c4", 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [!]
        { "4151ed80568efcca34f8831d957eb7a6", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (J) [o1]
        { "4166d03402397a56043ac2cd45f4cb87", 2, MEMORY_PAK }, // Virtual Chess 64 (E) (M6) [b1]
        { "4168c145279ea29912ba0e35dca35289", 4, 0 }, // Super Bomberman 2 by Rider (POM '99) (PD)
        { "417afe49bf73f3663c5f5f0af897fc79", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 99 (E) [b1]
        { "41965b533f3dd95663361d9df68b0c1f", 4, 0 }, // Clay Fighter 63 1-3 (E) [!]
        { "41be85236052a2e8eed144989b0a3e0a", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [h5C]
        { "41ca21bd737e16ba81168982b74276f1", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) (VC) [!]
        { "41e419f4392cfef2c354006169362286", 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [b2]
        { "41f67b5c8bb8daeffd989123846fc063", 4, MEMORY_PAK | RUMBLE_PAK }, // Roadsters Trophy (E) (M6) [!]
        { "41fe06c68915666e5f8fef81b8fff59f", 4, 0 }, // MAME 64 Beta 3 (PD)
        { "420c9fdbae15767c5e584070209ff253", 2, MEMORY_PAK | RUMBLE_PAK }, // Twisted Edge Extreme Snowboarding (E) [!]
        { "422fd8833f865c4a6c17fcac4450a69e", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [o2]
        { "4242fde5b74f22aaf5746459e126121f", 2, 0 }, // Dark Rift (E) [!]
        { "4244cc48674c26bd848718c05688f821", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2000 (E) [!]
        { "4250ff0265ad72fc798e6aba61cfcd0e", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (Kiosk Demo) (Gore On Hack)
        { "426725bfe1a505d4c3b12862188a2526", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [b2]
        { "42672ba5e98cd21d7f3e3745e69038dd", 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (E) [!]
        { "427c5457d4a29a222811f0caa9cca7b9", 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (E) (M3) [!]
        { "428067dc7db42dfc977a775f0a6e55b1", 4, RUMBLE_PAK }, // Donkey Kong 64 (E) (M4) [b1]
        { "42ac30399688f98e58be6ad1d2217e56", 4, 0 }, // MAME 64 Demo (PD)
        { "42af1992978229bbb5f560571708e25e", 1, 0 }, // Starshot - Space Circus Fever (U) (M3) [!]
        { "42cc0fe1442b0a498dfd8f743179c51a", 1, 0 }, // Pocket Monsters Snap (J) [b1]
        { "42da4c7d040f9e7cd046a42ec3e68027", 1, RUMBLE_PAK }, // Command & Conquer (E) (M2) [!]
        { "42e282a3f208c4bde50a4a4301181b16", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (U) [h1C]
        { "42f3a177f54c86f2c8ee6afdb892a5a9", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [b3]
        { "43115145f2dc0f3d0c6091d05cb9fa52", 2, RUMBLE_PAK }, // F-1 World Grand Prix (G) [b1][o1]
        { "4311a1aef1898678331f7e3486055307", 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (U) [!]
        { "431de8f6611a8131b536f0ede1f330d9", 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (E) (M5) [!]
        { "432df5eac0ba573c6587a5bb4e51834a", 2, RUMBLE_PAK }, // StarCraft 64 (Beta) [f1]
        { "4347174bb415ca970f2d50df2973f656", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (J) [!]
        { "43475aa0d793806c23d8645f49f6656d", 4, 0 }, // Neon64 V1.2a by Halley's Comet Software (PD) [b1]
        { "434bb8de49011573ac38e893224c5623", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (E) [!]
        { "435d75d07878d305293ec4d591b86303", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // GoldenEye X
        { "436ba873e9466aab237d9429348a5f70", 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (J) [!]
        { "4374a8342e612951148136194053967c", 1, RUMBLE_PAK }, // Neon Genesis Evangelion (J) [b2]
        { "4379c26bf067f7161bc6629445e22188", 4, MEMORY_PAK | RUMBLE_PAK }, // Snobow Kids (J) [h2C]
        { "437efd7fd7f84f4c0f802d3bf1f8464e", 4, 0 }, // GameShark Pro V2.0 (Unl)
        { "4392f33c4282b68c9def5111ccf733ca", 4, MEMORY_PAK | RUMBLE_PAK }, // RTL World League Soccer 2000 (G) [f1][o1]
        { "43b02af2789990a14f77ce020e6f135c", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (E) [!]
        { "43b326b27b89288f2d17d22ea49f86bd", 4, MEMORY_PAK }, // International Superstar Soccer '98 (U) [o1]
        { "43b9a7a5ccb6ea3f4860f9f80d73669d", 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (E) (M2) (Fre-Ger) [!]
        { "43c23d76e4b6b1cbfce446dfadfb0396", 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (G) [f1] (NTSC)
        { "43c3375bbd6496b6c51d08ec193bc8ed", 4, 0 }, // Glover (Prototype)
        { "43d788e7caea1146228b763c13e8e740", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (E) [f1] (NTSC)
        { "441fa65faa5c12339f89a0bb7db43c8f", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 2001 (U) [!]
        { "4421f891f0ba3cc7aa0fd566a162f2c8", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (U) (M7) [b4]
        { "4443b11c457348b0b6b64650909f0143", 4, 0 }, // Birthday Demo for Steve by Nep (PD)
        { "4446e99e8d43e1b7557412acea918f68", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (U) [b3]
        { "44494cd5d6e3332f79465de51c2ec636", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [T-Ita0.9Z3_Vampire]
        { "444f70a655ac89ca900f6fafaf926b16", 2, TRANSFER_PAK }, // Robot Ponkottsu 64 - 7tsu no Umi no Caramel (J) [!]
        { "4454122ec735252e40ec22ff5de021c6", 1, MEMORY_PAK | RUMBLE_PAK }, // Harukanaru Augusta Masters 98 (J) [h1C]
        { "446d5215c4d34eb8ab0f355f324b8d0e", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [!]
        { "4472b1f6b9d594b6361dfa396f8cfb2c", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [f3]
        { "44b0be1c4b48f6119d3ac9424903d0eb", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (F) [!]
        { "44bc029b5219e1a16f3b99125056f98b", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (U) [b2]
        { "44c4566572dc0662d4299ab5b19043ae", 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (E) [!]
        { "44d5f87127053a21db120bd108b7ac0c", 2, MEMORY_PAK | RUMBLE_PAK }, // F1 Racing Championship (B) (M2) [!]
        { "44d7f3fc23428913d10e68017acb097f", 4, 0 }, // Spice Girls Rotator Demo by RedboX (PD) [a1]
        { "44de1f9ed1fa35b72d31a1f809285b01", 4, RUMBLE_PAK }, // SmashRemix1.2.0
        { "44e6921a257e913977088b9b6ba9d25b", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Spa2.0_eduardo_a2j]
        { "44fc2a7f028f0b6f71b255f672c8b495", 4, 0 }, // Worms - Armageddon (E) (M6) [!]
        { "44fe06ba3686c02a7988f27600a533da", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G XG2 (U) [!]
        { "4561840840760ffa7b59f03a5f416a5c", 4, RUMBLE_PAK }, // RR64 - Ridge Racer 64 (E) [!]
        { "45676429ef6b90e65b517129b700308e", 1, 0 }, // Super Mario 64 (E) (M3) [!]
        { "45b507aaaf0ccdb6efe3cd717f0ddb95", 2, 0 }, // Pokemon Puzzle League (G) (VC) [!]
        { "45c610fc443d0df07ed9f07ac8b7c54a", 2, 0 }, // 77a Special Edition by Count0 (PD) [b1]
        { "45cdcbcb7159ebae1ed4d7aae70749e0", 4, 0 }, // Pip's Tic Tak Toe by Mark Pips (PD)
        { "45d1d039ab7926adc748de640afd986a", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (E) [!]
        { "45ef2794a1ac6ad737288a6577d55194", 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (E) (M3) [o1]
        { "45f6b7eaf40e8445205904934ee365a6", 4, 0 }, // JPEG Slideshow Viewer by Garth Elgar (PD)
        { "45fc88e2ba6711f25f0de988e719df29", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.1) [!]
        { "45feb0fbbec6cb48ff21deae176e9b6b", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 2000 (U) [!]
        { "460a42ed5612ebbf92f386689067384e", 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.0) [f1]
        { "462a2af0e3b72da4a0e9266078ee5717", 1, MEMORY_PAK | TRANSFER_PAK }, // Super Robot Taisen 64 (J) [b1]
        { "462b9c4f38758c2e558312ac60df2b91", 4, MEMORY_PAK }, // Morita Shougi 64 (J) [!]
        { "464211abb602ee1005974d2d835a3bcf", 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy's Reckin' Balls (U) [!]
        { "4645672a0cf00ada9b5e37cfde8b024e", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [!]
        { "46476a5626cd99b3749ac1ee1e234fac", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (E) [!]
        { "4648c4f656bd4a74647df6a7a2985f37", 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (U) [f1] (PAL)
        { "465dfd27ddab4f5488f4dadc09b7f938", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (E) (M4) [!]
        { "4688c66f8c13eda13dd5678176a7fd8a", 4, TRANSFER_PAK }, // Pokemon Stadium (F) [f1]
        { "468f7eaa4b94b831aeed8b83c062b5cb", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [b1]
        { "4695a944b019f3829f2616591659a7ce", 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (U) [b1]
        { "46b95ea3eb82654439a6750443bcde53", 4, 0 }, // Clay Fighter 63 1-3 (E) [b1][h1C]
        { "46be5d00682fcc1f7fc0fba507e8e5c1", 2, RUMBLE_PAK }, // Tom and Jerry in Fists of Furry (E) (M6) [!]
        { "46c45620f26e4ed77b918f862fa597cd", 4, 0 }, // XtraLife Dextrose Demo by RedboX (PD)
        { "46c9461b0b6f232c5a50ca795395d706", 4, 0 }, // DS1 Manager V1.1 by R. Bubba Magillicutty (PD) [T+Ita]
        { "46e9a352ad6afb83b0557ec659957b2e", 2, MEMORY_PAK | RUMBLE_PAK }, // Flying Dragon (U) [b1]
        { "46f584dbfa1e292f0eca74ca1b12009c", 4, MEMORY_PAK }, // J.League Live 64 (J) [b1]
        { "470d3e391a2b4cebf0a9e15ec49cd324", 2, MEMORY_PAK }, // Wave Race 64 (E) (M2) [o1]
        { "470e1635a9693102b426a840dbd691e5", 4, MEMORY_PAK }, // Choro Q 64 (J) [b2]
        { "4723e4599e37db86b9bbd2fd18bfdb8f", 4, 0 }, // Neon64 First Public Beta Release V2 by Halley's Comet Software (PD)
        { "473fddbb121255a171a40c14130d0165", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (E) (V1.0) [b1]
        { "474647aedfa95aef73229a2784897edd", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (U) [b1]
        { "4748d96916ae2bcc5fc1630515ee2561", 4, TRANSFER_PAK }, // Pokemon Stadium 2 (F) [!]
        { "476203b64e2204306b591d5d3afc7183", 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.0) [f2] (PAL)
        { "47661ef1964524b6319b759913f08b62", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 - 2nd Offence (E) [!]
        { "476fdc0554bd3b4746fc871fdd02f4b2", 4, MEMORY_PAK }, // J.League Eleven Beat 1997 (J) [h1C]
        { "4782ca6f893aa03f701d6eda7552711b", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b6]
        { "479554129eb27a3546bac96c80af4f88", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (E) (M7) [o1]
        { "47b0348020c4200608b7ebd512028788", 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [f2] (PAL)
        { "47b5e3955d54f969941533f26691ab38", 4, RUMBLE_PAK }, // Snowboard Kids 2 (E) [!]
        { "47c7df82b3adeae19c8aface40feb1ac", 4, 0 }, // Game Boy 64 + Super Mario 3 (PD)
        { "47cac4e2a6309458342f21a9018ffbf0", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.0) [!]
        { "47ea239717c4d225c9d0e9fd37b9fcb3", 1, RUMBLE_PAK }, // Akumajou Dracula Mokushiroku Gaiden - Legend of Cornell (J) [!]
        { "47ea6f037b093381ca88a41fbb6c4199", 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [b1]
        { "47f9d900c97ece154bb40a9c6dccd3fd", 1, MEMORY_PAK }, // Gex 64 - Enter the Gecko (U) [!]
        { "480855e2348d6b85721c620ae9fef138", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [b3]
        { "4808e8129b22ac8b3185b37243113005", 2, MEMORY_PAK | RUMBLE_PAK }, // City-Tour GP - Zennihon GT Senshuken (J) [b1]
        { "482bdd39ad2574b943db780b12a9bdfb", 4, MEMORY_PAK }, // Jikkyou G1 Stable (J) (V1.1) [!]
        { "483bd87237bb38884e7e22f4981acfdb", 4, 0 }, // Doctor V64 BIOS V1.94
        { "484f3e696c94980acf3d7068f9acc98f", 4, TRANSFER_PAK }, // Pokemon Stadium 2 (G) [!]
        { "48522ccf3d059ddf2dcf5d6aee2d1fd7", 2, RUMBLE_PAK }, // AeroFighters Assault (U) [b3]
        { "48782bc5728f38099b21e8b281adc1ca", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (U) [T+Ita_HRG]
        { "4890576c70e8486d2a90424c57c53752", 1, MEMORY_PAK }, // Quest 64 (U) [b3]
        { "48ca89956120e0522d4b6b7e2810d04c", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Pol1.3]
        { "48d1fa279fea4c99cfeec90e9ad1668f", 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (V1.0) [f1]
        { "48d6c194ae106018ddfc3486a8b347f7", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (E) [b1]
        { "48da6cdcab838153caa2ecc3dd592a65", 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack - Covert Operations (U) [!]
        { "48e6a629101b200a70873589b5d2f524", 4, MEMORY_PAK | RUMBLE_PAK }, // Asteroids Hyper 64 (U) [o1][f1] (PAL)
        { "4903c39007aeacc113fe0e9e4bbe4711", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.0) [t1]
        { "49198056ddebb41b1a820a09289a2e3f", 1, MEMORY_PAK | RUMBLE_PAK }, // Nightmare Creatures (U) [t2]
        { "491d4db6718302489bf05fb962c73651", 4, 0 }, // Worms - Armageddon (U) (M3) [!]
        { "49551e729a39f9640058ab5771a49311", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [b1]
        { "4955dea9356d1506d6de8bd8da10e938", 4, MEMORY_PAK }, // NBA Hangtime (U) [b2]
        { "495a9bffd6620be43225db7133373fc5", 1, 0 }, // Mischief Makers (U) (V1.0) [!]
        { "495e0b30318fc67978d68bd034165981", 4, MEMORY_PAK }, // VNES64 + Mario (PD)
        { "496929ea74183198d121035ea808c480", 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (V1.0) [b1]
        { "497d1e0677395a921b6006bbf41eed04", 4, 0 }, // Nintro64 Demo by Lem (POM '98) (PD)
        { "499013da36ad50c94b2ce6b794cc9983", 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [f2] (NTSC)
        { "49b49e6dd528e896de9bce3dccf3442e", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [b2]
        { "49e9c7aacf839e46ea2d255a99fb2613", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [b4]
        { "4a3263eab67fab2d1171a23765d73ac6", 4, 0 }, // Chaos 89 Demo (PD)
        { "4a5c509a20db7a429dc1dd4e219ad4a2", 4, MEMORY_PAK }, // AI Shougi 3 (J) [!]
        { "4a5e85a3e7118a742e3678f8f7e82b0e", 4, MEMORY_PAK }, // Mario Kart 64 (U) [b1]
        { "4a8f8ebb93af51878c8fb9057ad5f43c", 1, RUMBLE_PAK }, // Space Station Silicon Valley (U) [f1] (PAL)
        { "4ab8d6535028805f5794d3f01003cfd5", 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T+Eng2007-08-10_Brandon Dixon]
        { "4ac1589c067ac573fd63912a5c6c47a5", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.0) [f1]
        { "4afa73e31031c0f582c11cda47cc8729", 4, 0 }, // Doctor V64 BIOS V1.11
        { "4b2dbc0d448b7ad8569291e4e44821f0", 4, 0 }, // N64probe by MooglyGuy (PD)
        { "4b4c85d9dd2d460adafabae8db48b4fa", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (F) [!]
        { "4b6914d5552e4ff348fb590d52a6df8a", 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (U) [b2]
        { "4b86c373533d015860467c5dc1f1c662", 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [b1]
        { "4ba4b1a2abaf72ec83a2db4cb6aeb7d6", 1, RUMBLE_PAK }, // Yoshi Story (J) [f1]
        { "4ba95aa97ecfee36051ebe0a9024eee8", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 99 (E) [!]
        { "4bbaf03594c6dcecb135ac8c57cb4cd7", 4, MEMORY_PAK }, // Jikkyou G1 Stable (J) (V1.0) [b1]
        { "4bc1d3074fa3a3dcaf1f16888b82a966", 4, 0 }, // T-Shirt Demo by Neptune and Steve (POM '98) (PD) [b1]
        { "4bcde237f4a390a564369696d0579760", 4, RUMBLE_PAK }, // Cruis'n World (U) [f1]
        { "4bef5e9aa9e71205dac1a7060e778235", 4, MEMORY_PAK }, // World Cup 98 (U) (M8) [!]
        { "4bfb72fc9a787543323adefc8ec2a042", 1, MEMORY_PAK }, // Blast Corps (U) (V1.0) [b2]
        { "4c15c85280dfb003c3be6506b217b775", 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (U) [f1] (PAL)
        { "4c383e57bc1062466328d0b59fb7aa67", 4, 0 }, // GameBooster 64 V1.1 (PAL) (Unl) [f1] (Sound)
        { "4c3f1b48e7252391b337d9bae404cf83", 4, 0 }, // Neon64 GS V1.2 (Gameshark Version) (PD)
        { "4c58608f3879627886051b7c035e3c23", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [o1]
        { "4c5be1bfc1cccff501eba2a685226962", 4, MEMORY_PAK | RUMBLE_PAK }, // Jeremy McGrath Supercross 2000 (E) [!]
        { "4c5f3c38743ebb2d12154a83c026da46", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [o2]
        { "4c7092585cf64bbe7f26eca720ae8941", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [b2]
        { "4c7a2f4881eaca75dc2fc36673ae2a20", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (J) [!]
        { "4c9b2848a2eed723c331973a926488ab", 4, 0 }, // Famista 64 (J) [b3]
        { "4c9b419dc583c0df4ab908adf83bfc65", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.1) [!]
        { "4caae8ca91f58d746b6a00bcbc122b87", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (U) (M7) [b2]
        { "4ccff861ad2cfd65cc660f496c1d1664", 1, RUMBLE_PAK }, // Lt. Duck Dodgers (Prototype)
        { "4cef5cdd83360127b0445e7a9ea0bc8f", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [h1C]
        { "4cf0249345388bb098f79ddcd7a00798", 4, 0 }, // NEO Myth N64 Menu Demo V0.1 (PD)
        { "4cfaed7dad4ee592572964ba03d8c07e", 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [f1]
        { "4d010ae1af4b04d6b70b799c56f05993", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (E) [b1]
        { "4d02f5a719bd455f67721c64b16ae0cb", 4, 0 }, // Mario Artist Paint Studio (J) (1999-02-11 Proto)
        { "4d1243d71f5536a416f3664a02fdf8e2", 4, 0 }, // Mind Present Demo Readme by Widget and Immortal (POM '98) (PD)
        { "4d1821886352eaa2f663cff63ed17dd8", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (U) [t2]
        { "4d280855ea4ffe98054bdf633ea5a7f7", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [t2]
        { "4d28ddb5b93ea302ee9f27805de1b46f", 4, 0 }, // Pip's World Game 1 by Mr. Pips (PD)
        { "4d37726fdfec039cb36e2aae65b9727d", 4, RUMBLE_PAK }, // Super Smash Bros. (E) (M3) [b1]
        { "4d4483c0ca3b86997e41a7032f6ad25f", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) [f1] (Save)
        { "4d5c42b239dfd67e40d4e9476a208a1e", 4, MEMORY_PAK }, // Bomberman 64 (E) [b2]
        { "4d6e12e0ab62d4045b4934b6d7bb8bdc", 4, 0 }, // TRON Demo (PD)
        { "4d972ae7faa1aa3ed26026f6f46c8a6a", 1, RUMBLE_PAK }, // Command & Conquer (E) (M2) [b1]
        { "4dbad6b0e2f30a63f974a85513b668e0", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [h2C]
        { "4dc614b1022252f41e4e182249fa7baa", 4, 0 }, // All Star Tennis '99 (E) (M5) [f1] (NTSC)
        { "4dc885150dfec67acdd807f9ffe9596e", 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [t1]
        { "4dd6a2cf6091a92c43ce1b4a1b6f1556", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 99 (U) (M5) [b2]
        { "4ded9381cd0d396ae6c9634d575dd93c", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t4] (Hyper Mode)
        { "4e054610a872e1313228637c815e514c", 1, 0 }, // Manic Miner - Hidden Levels by RedboX (PD)
        { "4e08d29b094c13c030ac73a1da2f8cd2", 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [b1]
        { "4e12b97690897465a27bdf0dda722826", 1, 0 }, // Star Wars - Shadows of the Empire (U) (Unl)
        { "4e15d92cca23e1a01bb65246431b5c5a", 4, 0 }, // Glover 2 - Unreleased Beta Version (New Build) (Prototype)
        { "4e275b05babca9f2cd77749026bea5ed", 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (U) (M7) [b1]
        { "4e39474136c94ce25061fb6c0b473e29", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [t2]
        { "4e450dd2899fe7434110e30e19dd10a2", 2, RUMBLE_PAK }, // F-1 World Grand Prix (U) [h1C]
        { "4e730d045ac33a64ba97d5672589c6c7", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o5]
        { "4ea0f7563f2eaeb3d46e860c12fabfcb", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (Kiosk Demo) [h1C]
        { "4ea4119d7d07fd965bb09a974341e0f3", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.08
        { "4ea8627701c3d6af50ddaa607aa3b7f9", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move '99 (U) [f1] (PAL)
        { "4eab63be36dcc7fa6d612be780bf79c1", 2, MEMORY_PAK | RUMBLE_PAK }, // Supercross 2000 (U) [f1] (PAL)
        { "4eae45340300293065cef5b598c0ac6e", 4, 0 }, // Doctor V64 BIOS V1.22
        { "4ee1dc953b1ff209811cf2808d78f064", 2, MEMORY_PAK | RUMBLE_PAK }, // Space Invaders (U) [t2]
        { "4f0e07f0eeac7e5d7ce3a75461888d03", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [!]
        { "4f0e2af205beeb49270154810660ff37", 2, MEMORY_PAK | RUMBLE_PAK }, // Twisted Edge Extreme Snowboarding (E) [b1]
        { "4f256146bac4a3dde5ad0d5f9c909251", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (U) [!]
        { "4f557c3f71c0d04a54776450913e027f", 4, 0 }, // Z64 BIOS V2.16
        { "4f8e5341b89e6c2069a319ef2fb58b16", 1, MEMORY_PAK }, // Zool - Majou Tsukai Densetsu (J) [f1] (PAL)
        { "4f94d90294d6e5c1fc116e073dd3f542", 4, 0 }, // DS1 Manager V1.1 by R. Bubba Magillicutty (PD)
        { "4fa4dc582c23eee81feb39b7eebd15d6", 4, 0 }, // Psychodelic Demo by Ste (POM '98) (PD) [b1]
        { "4fca132d6c96130371f4186a3df85077", 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [b1][f2] (NTSC)
        { "4fcd85c487e1fbedfd85e27f991d1fa6", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (U) [o1]
        { "4fd424df60f80ee7e362a9572049ba11", 2, MEMORY_PAK | TRANSFER_PAK }, // PD Ultraman Battle Collection 64 (J) [b1]
        { "4fdf9e702d4a6a75124623d9434bf99f", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (E) (M3) [h1C]
        { "4ff9589a3224aaa46e9877d6b25e68e3", 2, MEMORY_PAK | RUMBLE_PAK }, // Monaco Grand Prix (U) [!]
        { "50091668743d2de7caa253420d32c2d8", 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (U) [f1] (PAL)
        { "500afe6d6732e0888d108410d135b78e", 4, 0 }, // Liner V1.00 by Colin Phillipps of Memir (PD) [b2]
        { "50195216c8a37f9bd5b2105a40ee8d8f", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (As) [!]
        { "50272f8ea99e4e06add8ccab5f5a4f41", 4, MEMORY_PAK | RUMBLE_PAK }, // Violence Killer - Turok New Generation (J) [b1]
        { "503c548f7b38a3721b93fd0abf979019", 4, 0 }, // TRSI Intro by Ayatollah (POM '99) (PD)
        { "504c92a5978b7eae7a3fd30645e06d39", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (F) [f1] (NTSC)
        { "5065d7a833fc73cb75ded8d639aaada5", 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (U) [b2]
        { "507ceab72ef2a1bf145bf190f5ce1c80", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (U) [!]
        { "508b2d76887e3fe169b60b6cf70e6dba", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b5]
        { "508be860974b75470851a2d25c0fcb36", 4, RUMBLE_PAK }, // Super Smash Bros. (U) [b1]
        { "50a8b47e5f8fe5bf955afa70fe3893ac", 1, 0 }, // Mahjong Hourouki Classic (J) [b1]
        { "50c10082d0c077fdb5658ef5a6e3f54f", 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [!]
        { "50f8e73ca0160eb1a339ac2137a7b559", 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (U) [f1] (PAL)
        { "51074cfe7a319270cf088a7c65387368", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (U) (V1.0) [t1]
        { "511833a46be4e00d1b3655c88e278422", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [o1]
        { "51313e88d86dff531e386529bbe45dd5", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.30
        { "514d61d3b3d5e6326869783eb2e84a00", 4, RUMBLE_PAK }, // Monster Truck Madness 64 (U) [!]
        { "515c86480a7fc1238bfe252bd2c53b36", 1, 0 }, // Sporting Clays by Charles Doty (PD)
        { "51778e50ce70466796f8d51c686224c4", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Spa1.0]
        { "518b14054a667a3b9e0b72d3bf784e41", 4, MEMORY_PAK }, // Penny Racers (U) [!]
        { "51a1eee441240229beb2e6cd8fab285c", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (E) [f1] (NTSC)
        { "52037ed117d09f46f82c3f72044366e7", 4, 0 }, // DS1 SRAM Manager V1.1 by _Sage_ (PD)
        { "5224ccbe260a4b7713d662d9bc269063", 4, MEMORY_PAK }, // Chou Kuukan Night Pro Yakyuu King (J) [b1]
        { "523883a766c662e8377cd256755b27b4", 2, 0 }, // Mace - The Dark Age (E) [!]
        { "5259f6331389f6af55b867f60aa373c8", 4, MEMORY_PAK }, // Baku Bomberman (J) [h1C]
        { "5270d98f9e67dc7ef354ece109c2a18f", 4, MEMORY_PAK | RUMBLE_PAK }, // Getter Love!! (J) [!]
        { "527f05f166398599f0a1b10f8e1f9585", 1, MEMORY_PAK }, // Premier Manager 64 (E) [f1] (NTSC)
        { "528445fb76d0f060df5cbe71aeeda20f", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (E) [t1] (Endless Ice)
        { "52f4f3320607f3e8dd1a16a2f1bfbdb0", 2, MEMORY_PAK | TRANSFER_PAK }, // PD Ultraman Battle Collection 64 (J) [!]
        { "52fd60268a69dc7b532ad58edd2133c0", 1, 0 }, // SLiDeS (PD)
        { "530b796ea3b1748841f9d057f3f9efa8", 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [t1]
        { "5320608c729c9f5b32581f81604d75e6", 1, RUMBLE_PAK }, // Mission Impossible (U) [t1]
        { "5340dd96cf1cd7ac6d72000d40b915ce", 4, MEMORY_PAK }, // Golden Nugget 64 (U) [b2]
        { "5349abd4ee13c8f029a60032735b1ae3", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (U) (V1.0) [o1]
        { "535995506105d0eaba796e371660cbf2", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (U) [b2]
        { "5382ad48844a3473d4cba87c85f2bac2", 4, MEMORY_PAK }, // NBA Jam 99 (U) [f1] (PAL)
        { "538b54c2aaea73faa3a021d42a3225be", 4, MEMORY_PAK }, // Jikkyou World Soccer - World Cup France '98 (J) (V1.1) [!]
        { "538d2b75945eae069b29c46193e74790", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (J) [!]
        { "53922802f7744cc38bdd75852214057f", 2, MEMORY_PAK | RUMBLE_PAK }, // Supercross 2000 (U) [b1]
        { "53a5d74fa55e378ada84af9b0ed2b728", 1, 0 }, // Pokemon Snap (U) [f3]
        { "53b325ccdf2069ab20caf1c9b401cd20", 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (E) (M5) [f2] (NTSC)
        { "53dfa593019bda070dd3cd5fc5b58436", 1, RUMBLE_PAK }, // Taz Express (E) (M6) [f1] (NTSC)
        { "53e09e6b72f114440b389fa8549e8f97", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [T+Spa01b_toruzz]
        { "53e2872612760133ab7b2cc2e22b847c", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (E) (M3) [!]
        { "53ee0172e29cdcbbfa5957a442863e5a", 4, MEMORY_PAK }, // Centre Court Tennis (E) [h1C]
        { "5426eaed0d350367393f385025af164c", 4, 0 }, // Pom Part 3 Demo (PD)
        { "542fed7864218156ea90050013cd048e", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.0) [o1]
        { "5443bfb72d91d0dfd72fc1c6fa03a113", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [b3]
        { "5468a2ac2e93ad3f49e9b271c8341cac", 1, 0 }, // Ogre Battle 64 - Person of Lordly Caliber (J) (V1.1) [b1][o1]
        { "547cf20308f2ce2ce5b5310a5e555113", 4, 0 }, // Pip's Porn Pack 3 by Mr. Pips (PD)
        { "548c444fb604ca179ead4115ffb070fc", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t3] (All Attacks Hurt P2)
        { "548fc0e6035b65bc2108255039859934", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (E) (V1.2) [!]
        { "54ad823812d2c2e8b1688c4ace2e654d", 1, 0 }, // Harvest Moon 64 (U) [t1][f1] (PAL-NTSC)
        { "54be265e7b2c28ab92bf1a4130acb5a2", 1, RUMBLE_PAK }, // Dezaemon 3D (J) [!]
        { "54c610270d3f5eea39bc6a8f73b3b909", 2, MEMORY_PAK }, // Dual Heroes (J) [o2]
        { "54d0a39123c15f74aabb1ecc24d4d6a0", 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (E) [!]
        { "54f43e6b68782e98caabea5e7976b2be", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [!]
        { "54fd2659fcce58fcc6ee544f95f8f7a3", 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant in NBA Courtside (E) [f1]
        { "550f4c177942fc0df00b646c42eb4a90", 1, RUMBLE_PAK }, // Mission Impossible (U) [b1]
        { "551324f5fd0a2b0bc2b1bfbb0fe29c08", 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (E) [T+Spa1.0.1_IlDucci]
        { "55185016031cdc73c0fd471527c35706", 4, RUMBLE_PAK }, // Rugrats - Treasure Hunt (E) [!]
        { "55224f1148f873a689604f2dc6432682", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 2000 (U) [b1]
        { "553037c41f9029f544353b8446be77d9", 4, 0 }, // Attax64 by Pookae (POM '99) (PD) [b1]
        { "553d8d5347969c66e5d91c3fe35208b9", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW vs. nWo - World Tour (E) [!]
        { "553f6e6766b85cb0247e5ca62f515c75", 4, 0 }, // Doctor V64 BIOS V2.00
        { "554bfcc857be49b2aacd077e6059b281", 1, 0 }, // Star Wars - Shadows of the Empire (E) [b4]
        { "554ee4bf15388dde586de90ac72ef5fa", 1, 0 }, // Earthworm Jim 3D (U) [T+Rus]
        { "55634ff90ee997790781f79a5b0097ee", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (E) [!]
        { "55685d6324efde5bc9d26c98706b0b8a", 4, RUMBLE_PAK }, // Les Razmoket - La Chasse Aux Tresors (F) [!]
        { "557f89a263b2028936753956f7902299", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [t1]
        { "5582cf3bb6e6b11d6d97f6fdd1ee9a3b", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (E) [f1] (NTSC)
        { "5583527ed4aa49cf713fae400e77f9a2", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [o1]
        { "55868ecccc14f43bf61032de21d9f026", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (U) [t1]
        { "559f71b861f639b6376d891e3023414b", 2, RUMBLE_PAK }, // StarCraft 64 (U) [!]
        { "55a789c553827114306e29d71e26e5dc", 4, MEMORY_PAK | RUMBLE_PAK }, // Quake II (E) [h1C]
        { "55cd360f12be8063c9e7b6f59f268170 ", 1, 0 }, // Ogre Battle 64 - Person of Lordly Caliber (J) (V1.1) (VC) [!]
        { "55d13dc1512b1a3656db8130e59e31d2", 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (E) [h1C]
        { "55f99a3f3402491e9517980be636e3fe", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (U) [t1][h2C]
        { "561b438f6e8240bef1daeb36aae72675", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (E) [b1]
        { "5642e51d746f603dfe5a71c5cbdb5c73", 1, RUMBLE_PAK }, // Bomberman Hero (U) [b1]
        { "5653b6ddae328c870138e71ec30122a1", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [f1]
        { "565da8d53422f16207ecf11a81d2e649", 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (J) [b1]
        { "567b8914e0642721408d46627e36003b", 4, 0 }, // Bomberman 64 - Arcade Edition (J) [f1] (PAL)
        { "568d51e1d4602cd74ac3a2d9719a390a", 4, 0 }, // Z64 BIOS V2.17 by zmod.onestop.net (ORB HDisk ZIP250 Hack)
        { "5690d74157c6623e2928a6f0353ef4af", 1, 0 }, // Mischief Makers (E) [b1]
        { "5698757883a6f46fe5b4c9b6e780b480", 2, MEMORY_PAK }, // Robotron 64 (U) [b1]
        { "57044e8f223d117f55b22e7fd2edf71e", 1, RUMBLE_PAK }, // Command & Conquer (U) [b1]
        { "570b4f55a3005b709e6ed5d625981b90", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.1) [!]
        { "57146b6cd8ee7d96b01a811f98a1ac61", 1, MEMORY_PAK }, // Castlevania (E) (M3) [!]
        { "57464801a058b97e056766ddfcf93a69", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [b5]
        { "574704affbd28c7a2728da85cc1da0f3", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [b2]
        { "5783373634b11f81c86908c3d81ca988", 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (E) [!]
        { "5798e844953662880c5eb4134f886909", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [T+Ita1.0Beta_Vampire]
        { "57a9719ad547c516342e1a15d5c28c3d", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.2) [!]
        { "57beb4b0afdd4afade9db6a13b72d17d", 4, 0 }, // SRAM Manager V1.0 PAL Beta (PD)
        { "57c6b3e6309ac2f4bb64b5161e4ed8ef", 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [f3] (Nosave)
        { "57cb2d2f6c87f520016aacf7e2785222", 2, 0 }, // Mace - The Dark Age (U) [b2]
        { "57cbadb75429aafc0d7040de69dc8d70", 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [b2]
        { "57d31ea7121dd5a05b547225efa5cfd7", 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (J) [!]
        { "57d8e020643146b325a11612fefacf74", 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (U) [o1]
        { "5809670a42ca34d39a39598efa82f5f3", 0, 0 }, // Dynamix Intro by Widget and Immortal (PD)
        { "58153ac5c4030d1bfd3c15cf57fb02e7", 4, MEMORY_PAK }, // Jikkyou J.League Perfect Striker (J) [!]
        { "585aa96cbb44df3bbb3b294538b0605a", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [T-Ita0.9e_Vampire]
        { "586a092e22604840973b82dfaceac77a", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [!]
        { "5875fc73069077c93e214233b60f0bdc", 1, MEMORY_PAK }, // Blast Corps (U) (V1.1) [!]
        { "589dd744c7b07ec4da91568cfbac09ef", 4, 0 }, // Mind Present Demo 0 by Widget and Immortal (POM '98) (PD)
        { "58adc17903754febf77dc9cd3db8b7d8", 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [f2]
        { "58b2716389ba3c5575b5a287e59f49af", 1, 0 }, // Bike Race '98 V1.2 by NAN (PD) [b1]
        { "58b27ae9b41b755e4f9c503656b93a97", 4, 0 }, // Analogue Test Utility by WT_Riker (POM '99) (PD) [b1]
        { "58d200d43620007314304f4e6c9e6528", 4, RUMBLE_PAK }, // F-ZERO X (J) [!]
        { "58efaf3d7d0985bc426245efa5418cc2", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (E) [h1C]
        { "5919371bca053c750d1cb357c58953a5", 1, RUMBLE_PAK }, // Space Station Silicon Valley (E) (M7) [b1]
        { "591cf8e672c9cc0fe9c871cc56dcc854", 1, 0 }, // Star Wars - Shadows of the Empire (E) [!]
        { "592292df29823a0fd81ed32666878c15", 4, 0 }, // Psychodelic Demo by Ste (POM '98) (PD)
        { "592ce7718efdd1ff2f077c9b2b5275fb", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (E) [!]
        { "5932d07d6aaa84b7d74f5251d8942c91", 1, RUMBLE_PAK }, // F-Zero X Expansion Kit (U) [CART HACK]
        { "597204ee766b93c1ae33b7fc0739e170", 1, 0 }, // Super Mario 64 (U) [T+Rus]
        { "597adb9702440e72d5954d50671d9c57", 4, 0 }, // SRAM Manager V2.0 (PD)
        { "59985249f824f74c335c6c47a6e29c4a", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [b1]
        { "599b5d40b51f53c2c9a909e0139702fc", 1, RUMBLE_PAK }, // Mission Impossible (E) [!]
        { "59ba99120be9d438c53eaed6091b9b7d", 4, MEMORY_PAK }, // Hexen (U) [t2]
        { "59c28e02baf4065051c63ea51736892a", 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (E) (M2) [h1C]
        { "59eb5646fa079bcbd7a340d7a10196dd", 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ita) [!]
        { "59f0839cae935f8b06baec434cb29318", 4, 0 }, // Mario Artist: Communication Kit (J) [CART HACK]
        { "59fa8c6d533d36c0ffc2aafab7166e6f", 4, RUMBLE_PAK }, // Charlie Blast's Territory (U) [b1]
        { "5a04d7f1ab9c6b080176567aa7168d3a", 4, TRANSFER_PAK }, // Pokemon Stadium 2 (I) [!]
        { "5a19d395b6a8b1911deb334e392f4526", 4, RUMBLE_PAK }, // Super Smash Bros. (U) [f3]
        { "5a28008b05923016ea411bace06ef32a", 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [b2]
        { "5aaa98d1fdf963fb88b3d25476924a04", 4, 0 }, // Doctor V64 BIOS V2.02b
        { "5aac6e652c5cf1e37a466ac0073e88ca", 4, RUMBLE_PAK }, // SmashRemix0.9.4
        { "5ab39f2d7a144e1ba243df059560e878", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (E) (M6) [!]
        { "5ab4773bb270be3a72bb79ef68dea414", 2, MEMORY_PAK }, // AeroGauge (J) (V1.0) (Kiosk Demo) [b2]
        { "5ac5cedbd22ceea8dd50cf6084193357", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (G) [b1]
        { "5ad80a8ef44dee1fdc456d66104165b4", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (U) (V1.1) [!]
        { "5ae3bb3604539abdeba639e5892bd60e", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (U) [b2]
        { "5b127e6b090a0b3f68a114d4d89323d4", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.1) [!]
        { "5b4c268422469f50b94779e655f2b798", 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (U) [!]
        { "5b677f4bf93d6578252fcad2c8ceb637", 4, RUMBLE_PAK }, // Donkey Kong 64 (J) [!]
        { "5b6b6b0c8c9a40286dcf61706b6a05cb", 1, 0 }, // Star Wars - Teikoku no Kage (J) [!]
        { "5ba3aa2953c47c8b2e615b21e60f2f17", 2, 0 }, // Tsumi to Batsu - Hoshi no Keishousha (Ch) (iQue) [!]
        { "5ba3dc37860c08a209f24286b8dfec8c", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mickey's Speedway USA (E) (M5) [!]
        { "5baa85768f2410c61fbe2dfb62c3d836", 4, 0 }, // V64Jr Backup Tool by WT_Riker (PD)
        { "5bba457e286d250101ce274e0e58080d", 1, MEMORY_PAK }, // Gex 64 - Enter the Gecko (E) [!]
        { "5bbe9ade7171f2e1daaa7c48fad38728", 4, RUMBLE_PAK }, // Jet Force Gemini (U) (Kiosk Demo) [!]
        { "5bd1fe107bf8106b2ab6650abecd54d6", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [!]
        { "5bde53960ad95f4c25c76c0f70c35244", 2, RUMBLE_PAK }, // Magical Tetris Challenge (E) [f1] (NTSC)
        { "5bf0f2351aee577a8345d6e13d197e06", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (E) [f1] (NTSC)
        { "5c3d2699c01ebd30d42c6f52491ea7f0", 4, RUMBLE_PAK }, // Mario Party 3 (U) [f3]
        { "5c4496c6d937364de0022d1c07be38a1", 4, 0 }, // Fractal Zoomer Demo by RedboX (PD)
        { "5c98466c2b2aaf7f3b51d4406eee39fd", 2, 0 }, // War Gods (U) [o3]
        { "5c9c715430acbaef3a5fbdb3e6661acf", 4, MEMORY_PAK | RUMBLE_PAK }, // Roadsters Trophy (U) (M3) [t1]
        { "5ca2a7f712b6c5737d0b46e67c8dda44", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [b1]
        { "5ca3a45c21e16edafbafe1bd0491dc8d", 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [f2] (NTSC)
        { "5cb521b928fb94e84f4e0e49b6e25cd4", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (E) [h1C]
        { "5cbf54627693f800524038127bbf46bf", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (E) [b1]
        { "5cce8ad5f86e8a373a7525dc4c7e6705", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [!]
        { "5cdc052c88a5cadcc3c73b165163e8c7", 1, RUMBLE_PAK }, // Mission Impossible (E) [b1]
        { "5cdee5503a57d14533c66b35a5848899", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (U) [!]
        { "5d144d323785818a9bd5c667a5567bbd", 4, 0 }, // Pip's RPGs Beta 12 by Mr. Pips (PD)
        { "5d1e41aa28169e71b4919fa08f1acd9b", 4, MEMORY_PAK | RUMBLE_PAK }, // ECW Hardcore Revolution (G) [!]
        { "5d29c01b96a738c3eb68254ef40552f3", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (U) [f1] (PAL)
        { "5d3c7935193a3b7327021b5e4740deb6", 1, RUMBLE_PAK }, // Yoshi's Story (E) (M3) [b2]
        { "5d5dd380115abf43958a710c7b597771", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T-Rus0.1_Alex]
        { "5d72155e00bf2cab41b0f4a2f2e09c61", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark Plus
        { "5d7368d95b89f451e68f0832ed2ba8ff", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Mayhem (U) [f1] (PAL)
        { "5d82e903f65341487ddc11af80ad607a", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (F) [!]
        { "5da2c22480a3f6fe9bef9c6ad4852d37", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (U) [t1]
        { "5dbbfd5ace8222fa8fe51be113453c13", 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (E) (M5) [!]
        { "5ddce4475a98f7ba9cce2f255723eb31", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [b2]
        { "5df35d0c05e58fce938baec6cb2a230e", 4, 0 }, // R.I.P. Jay Demo by Ste (PD) [b1]
        { "5df3d21223f0dafd0417998e9c1e7065", 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [hI]
        { "5df98ae5eac2797555587d3391222fe4", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Rus.06]
        { "5dfb0200b6698cef2ba2a1775ba252a8", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (U) [o1][h1C]
        { "5e1940ca1236a76e8f2d15de0414ae55", 1, RUMBLE_PAK }, // Neon Genesis Evangelion (J) [!]
        { "5e2a5c56ca1a0ccaea3ae5f852e0c58d", 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T-Eng2007-02-09_Brandon Dixon]
        { "5e30d0208fc4cb810a464e5deffb29a3", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 - 2nd Offense (U) [f1] (PAL)
        { "5e3b1fac7c16d30499b3faa36caf98fd", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [t2] (No Damage-Unbreakable Wings)
        { "5e4dabadb17439a1f2122f5e2c141fd1", 4, 0 }, // Neon64 V1.0 by Halley's Comet Software (PD)
        { "5e54c6c563b09c107f86fb33e914ef81", 4, RUMBLE_PAK }, // Super Smash Bros. (E) (M3) [!]
        { "5e6202200af40a8f026780edfe1e15d0", 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 64 (J) [!]
        { "5e8539e037eea88c5a2746f60e431c8d", 1, 0 }, // Heiwa Pachinko World 64 (J) [!]
        { "5ea0ed74cf1ddaaa964d728a129e7cf9", 4, 0 }, // Mario Kart 64 (Ch) (V5) (iQue) [!]
        { "5eb2b1892b68767eb1d310ff7507d34e", 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (E) [h1C]
        { "5ed7e392198a5fa56ee37ea9e93a8d50", 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater (U) (V1.0) [!]
        { "5f1906df4eb30537c2ac2fcbd005907d", 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (E) (M4) (V1.0) [!]
        { "5f2c9e5e39ab09311d96e6c751184b6b", 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (E) [!]
        { "5f3d42d5f96191f3ce50d70e0e42127a", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (Beta) (1998-08-05) [!]
        { "5f46fc3780a50d5bb8fcb0f1c95686b0", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 99 (U) (M5) [b3]
        { "5f4d852bd3b707c1b4fe29f2fe19c687", 4, MEMORY_PAK }, // Choro Q 64 (J) [b4]
        { "5f73634c622abedf6204cb801872c79c", 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [b1]
        { "5fa39ef0ebe18da55c260a3a89f935b6", 1, RUMBLE_PAK }, // Chopper Attack (U) [b5]
        { "5fa70a63e352ea804607999674381749", 1, 0 }, // Pokemon Snap (F) [b1]
        { "5fb4de8b475c4427f0f30a28c1266cf9", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (U) [o1]
        { "5fba92d908b9962f26d389c85f6e88cf", 1, RUMBLE_PAK }, // Lode Runner 3-D (U) [b1][f1] (PAL)
        { "5fc8116dafdec73de9c5ccc2d8e14b4f", 1, RUMBLE_PAK }, // Banjo-Kazooie (E) (M3) [f1]
        { "5fd0f1ee3f6e0c1e0ba9a91ed797a8b9", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [T-Ita0.9f_Vampire]
        { "5fe7486a5a458d538179da898b067ad3", 4, 0 }, // Yoshi's Story BootEmu (PD)
        { "5ffd43089b7334072b2b74421618d973", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (J) [!]
        { "601e5335e43f83676f9f341b57d03611", 4, 0 }, // Doctor V64 BIOS V1.10r (RBubba Hack)
        { "60347200a1a7cabc0d849ee69ec51df7", 2, MEMORY_PAK | RUMBLE_PAK }, // Supercross 2000 (U) [!]
        { "604e2fb5ac9a26a81883a4c19037862a", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 2000 (U) [b2]
        { "604feb17258044a3e6c3aa9d2c5b62f9", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Jam 2000 (E) [!]
        { "60535265bae43ddfcbdb0d71594b1693", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.1) [!]
        { "6073174a8da1305cf91b59369faefddf", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (J) [b4]
        { "609b47b79da21f3df9b31d06c95c09a1", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (VC) [!]
        { "60a2cff1515d4c7902dde32a6e01d411", 1, MEMORY_PAK | RUMBLE_PAK }, // Nightmare Creatures (U) [b1]
        { "60a39ba7f3dcc592112fdf8220335f3d", 4, 0 }, // Z64 BIOS V1.09
        { "60a5f3e1e94f696bf5384022247b4059", 1, RUMBLE_PAK }, // Dezaemon 3D (J) [b1][t1]
        { "60bc7fe282c6be6b90ea0b5fc63bc944", 4, 0 }, // Z64 BIOS V1.08 (Ravemax Hack V1.00b)
        { "60cdf7445fad2aba05c958f46691501b", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 - 2nd Offense (U) [!]
        { "60d0264b38e22ef0d6b9549e4c81c29f", 4, 0 }, // GameBooster 64 V1.1 (NTSC) (Unl)
        { "60f0cbe10dce67fdd6527a78b51dccbc", 1, 0 }, // Earthworm Jim 3D (U) [t1]
        { "6160d5de99be421acb620d2d203c2fec", 2, MEMORY_PAK | RUMBLE_PAK }, // Racing Simulation 2 (G) [h1C]
        { "617197ef2b6b51e58488c9f6b75f112e", 4, 0 }, // Hard Coded Demo by Silo and Fractal (PD) [a1]
        { "617ceca1d2beffce943ef832326898bf", 4, 0 }, // PGA European Tour (U) [!]
        { "619b1f196b70260dce89b21e66d10934", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (E) [t1] (Hit Anywhere)
        { "61b5c5c3e5e1a81e5d37072c01b39b76", 4, RUMBLE_PAK }, // Banjo-Tooie (A) [!]
        { "61e637b542d5df178040454075c28e19", 4, MEMORY_PAK | RUMBLE_PAK }, // Wayne Gretzky's 3D Hockey (J) [!]
        { "62365463743857cfc823978e0e590d84", 4, MEMORY_PAK }, // NBA Hangtime (E) [!]
        { "626478cc0790b1452045a3d853b46c18", 4, RUMBLE_PAK }, // Cruis'n World (E) [b1]
        { "628aa3cd492559b705488f634797e045", 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (E) (M3) [!]
        { "628c10a5d2acc462c5527e6302467440", 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [f2] (PAL)
        { "62a0ce9a58273811e34cc73be8e6f8c4", 2, 0 }, // Mace - The Dark Age (U) [b6]
        { "62b005d0e9dae96526443498d6420ec3", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [b1][t1]
        { "62cd83fc8fc8e8cf8899320bf25474ce", 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (E) [b1]
        { "62dfa8a5906e973a2c923a165ebe00a1", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [h2C]
        { "62e182d019dce0c0eeaa3be2023a327a", 4, MEMORY_PAK }, // NBA Hangtime (U) [o1][f1]
        { "62e92102d6fd1701a6e904da6ab58ae8", 4, MEMORY_PAK }, // Brunswick Circuit Pro Bowling (U) [!]
        { "630400d4e2dced3dde56dfccf39a512a", 2, MEMORY_PAK | RUMBLE_PAK }, // Knockout Kings 2000 (U) [f1] (PAL)
        { "630776e478e00cda1d09bab55656aca7", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (G) [b2]
        { "630a177a8d5b603f8c86d8a089e3471b", 4, 0 }, // Sample Demo by Florian (PD) [b1]
        { "630e4122b0743a29c246da2c257f92da", 4, 0 }, // Spacer by Memir (POM '99) (PD)
        { "6310c7173385ed2b06020f3b90158e9e", 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (E) (M10) [!]
        { "632c98cf281cda776e66685b278a4fa6", 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [!]
        { "635c94434922dc73c58965004e3f699d", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) [b1]
        { "63609225da40e2baec425f4aeb365330", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b2]
        { "63657276e7256dca562f024fc71f4a47", 2, RUMBLE_PAK }, // AeroFighters Assault (U) [h3C]
        { "637a7ea2a39f20c5b20834187230d89d", 2, MEMORY_PAK | RUMBLE_PAK }, // SD Hiryuu no Ken Densetsu (J) [!]
        { "637b9a40bed777fc96eb1bd07ea74783", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (E) [h1C]
        { "63c2a327f7195ff43229738d315cf51c", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (U) [b1]
        { "63cbabef9069fe3375a7726c89e9e25d", 4, 0 }, // Pip's Porn Pack 1 by Mr. Pips (PD)
        { "63ce29bba130aacd05402efc06c0dc89", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [t3] (No Damage-Unbreakable Wings)
        { "63d7ab29ba3dfc5d5b12c1d9c5832355", 1, RUMBLE_PAK }, // Indiana Jones and the Infernal Machine (E) (Unreleased)
        { "63da3150ac7b603187de3951a3270f6e", 4, RUMBLE_PAK }, // Cruis'n World (U) [b3]
        { "63f850d7cc829b287ff1dc61ca3f7cca", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [t1]
        { "63fa04c30c4a0fbe162bcdec3cb42888", 2, MEMORY_PAK | RUMBLE_PAK }, // Space Invaders (U) [f1] (PAL)
        { "64011dd56ff84cf9eb46153e7aeccd26", 4, 0 }, // Dragon Sword 64 (U) (Prototype) (1999-08-25)
        { "6402498e891455ffb86c95ad8abb87f6", 4, 0 }, // Pip's RPGs Beta 3 by Mr. Pips (PD)
        { "640b928ad387b271094f64b9b93abbfc", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Dut]
        { "641a6513e4fe77b88085478f99f1857d", 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (U) [t1]
        { "6432c622c05ea9cd3217e280ac2ce37c", 4, MEMORY_PAK }, // FIFA 99 (E) (M8) [!]
        { "643cce1ab06f97e9590241d27e5c2363", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (U) [!]
        { "6472bd444d2025b1d883ffae35d94fd4", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [b1][f1] (PAL)
        { "64b4fb2d4c49ae6902f13149c81cf2a1", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.1) [o1]
        { "64c75e19890acde25842a815a21d55c9", 1, RUMBLE_PAK }, // Chopper Attack (U) [b6]
        { "64d508c5e0f1050048be429e20ef4b07", 2, 0 }, // Mace - The Dark Age (U) [o1]
        { "64e59ba2f7ba4daffdd5934f818dc7da", 1, 0 }, // Star Wars - Shadows of the Empire (E) [b3]
        { "6504fc6f3adb596f7d68060eb39a75b4", 4, MEMORY_PAK }, // Power League 64 (J) [o1]
        { "654557c316f901a2ca6f7f4b43343147", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx - Global Assault (U) [!]
        { "6560ffc9208a31f0518036f7937b53da", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.0) [h1] (Language Select)
        { "656b1442015cf95d4d1e01713309f71d", 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (U) [b1]
        { "657836f55dcb952c56bc5c645d82d6c2", 4, 0 }, // SRAM Manager V2.0 (PD) [h1C]
        { "6591a5fcfff8879382c966906c3186e0", 4, 0 }, // MAME 64 V1.0 (PD)
        { "65ac7755cb41e937a1dc5c58902a4222", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.35]
        { "65b71f404dc3cf9a135d3229ffea3c15", 4, 0 }, // MMR by Count0 (PD)
        { "65b9cac3900435ed6893b9d489f1655f", 4, MEMORY_PAK }, // International Superstar Soccer 64 (E) [h1C]
        { "65cbd8dcec6b7ce61e1cb233efa4a317", 4, MEMORY_PAK }, // Mario Kart 64 (U) [T+Por1.0_Dr_X]
        { "65e67d43e0a9b146d7881cbc803ec5c3", 4, 0 }, // TR64 Demo by FIres and Icepir8 (PD)
        { "65f1d583b5392d3467bd187224fbbd89", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (E) (M4) [b1]
        { "65fc73d7abffabafff3c35ff3cb60de2", 1, MEMORY_PAK }, // Sim City 2000 (J) [h1C]
        { "6614298a943234fd30885bd0b1383cbb", 1, 0 }, // Super Mario 64 (J) [T+Kor1.0_Minio]
        { "66335f4dc6ab27034398bc26f263b897", 1, 0 }, // 64 Hanafuda - Tenshi no Yakusoku (J) [!]
        { "668908c495432ad099c5439e38809053", 1, MEMORY_PAK }, // Holy Magic Century (F) [h1C]
        { "669584eeb9f85c7a8a96b716a2b43739", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [b3]
        { "66b8ec24557a50514a814f15429bd559", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (U) (V1.1) [!]
        { "66c31f31a386320f61aaf8f5f7936053", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (J) [b2]
        { "66c7eb8148e0714b5a71f5717dff8642", 1, RUMBLE_PAK }, // Mission Impossible (I) [!]
        { "66db457b130d31a286a23d6e4dd9726e", 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [!]
        { "6711b9d80d4d22ad0dc4af20c9088763", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.23
        { "672152cf4dcb5d0a19662c11eff71452", 4, MEMORY_PAK }, // Hexen (J) [!]
        { "673d4ba4f41a0fe23650f06af53eec50", 4, MEMORY_PAK | RUMBLE_PAK }, // Quake II (E) [!]
        { "6746f4cc43acebf3f04b1677b16c1935", 4, 0 }, // Clay Fighter 63 1-3 (U) [o2]
        { "674d416c2bd1ef0192bee34aa260b21a", 4, 0 }, // Tetris Beta Demo by FusionMan (POM '98) (PD)
        { "6770ddec84eb21a5e0d0f55dfd52a01a", 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (U) [!]
        { "677356691e55a9061772585cddfd7f76", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (U) [o1]
        { "67836fe63901319874e0754bc918af7d", 4, 0 }, // N64 Stars Demo (PD)
        { "67afa5df80a5cfc91fce1dc918ea0a4f", 4, 0 }, // Action Replay Pro 64 V3.3 (Unl)
        { "67c96076459eb5f71733f39d7fcc76a3", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (E) [!]
        { "67f2e18ed18a5f0ea3fe2d913e3f1786", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // GoldenEye X
        { "67f75c4dd30922a001c8c32aeb9333ac", 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (U) (V1.1) [!]
        { "681df5a32e857e77194106b35304d6b5", 2, MEMORY_PAK | RUMBLE_PAK }, // Rush 2 - Extreme Racing USA (E) (M6) [!]
        { "68230d510015ff6817ef898c0b8b636c", 4, RUMBLE_PAK }, // Jinsei Game 64 (J) [!]
        { "68246cf0ab9de7b6f84751fcc86a959a", 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (E) (M5) [b1]
        { "685b2f0decb5be33d25145e55f24a6d3", 1, RUMBLE_PAK }, // F-Zero X Expansion Kit (J) [CART HACK]
        { "685eb7d3d1f2c82932edf7b476a6e21b", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [b2]
        { "687af471a5a16d665e16ac1e9069d16b", 4, MEMORY_PAK | RUMBLE_PAK }, // Xena Warrior Princess - The Talisman of Fate (U) [f1] (PAL)
        { "68828bdcaae1c222dabb679b74b8acd0", 1, MEMORY_PAK }, // Gex 64 - Enter the Gecko (U) [t1]
        { "68a27fbab060857c267a639931d2c3d6", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.2) [!]
        { "68b478c274812450369dbf153cad185d", 1, MEMORY_PAK | RUMBLE_PAK }, // Nightmare Creatures (U) [b2]
        { "68c7fc1924ab4400cb65e40c32b7d3a6", 2, 0 }, // Pong by Oman (PD) [a1]
        { "68eb1713156605d5b05cfb838c214ef2", 4, 0 }, // Kid Stardust Intro with Sound by Kid Stardust (PD) [a1]
        { "68ebc8c849459fe30e7162d48ebac0bd", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t6] (P1 Invincible)
        { "69144ceed3f4d7b208d81a3dc3f57ad0", 4, RUMBLE_PAK }, // Jet Force Gemini (U) (Kiosk Demo) [b2]
        { "6917deeebc2fa9e185582215e7460271", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (U) [f1] (PAL)
        { "6922a2d0db719ad7972c0dc146ff0d8c", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [o2][b1]
        { "692a33b0d7456fc733a81ab83c20382b", 1, MEMORY_PAK }, // Zool - Majou Tsukai Densetsu (J) [!]
        { "69431c0e9152a9c5b62131c034250a25", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [b6]
        { "694dea68dbf1c3f06ff0476acf2169e6", 4, RUMBLE_PAK }, // Super Smash Bros. (A) [!]
        { "6969e979395a0af64caf2df9c1054c94", 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (E) (M3) [f1] (NTSC)
        { "698930c7ccd844673d77ffeccb3dd66e", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (E) [!]
        { "69895c5c78442260f6eafb2506dc482a", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina GC URA (J) (GC) [!]
        { "699a6ba5281ea0c83190e9d45c708c26", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [b4]
        { "69a7d3a2f2805bc2fc84db917f8af592", 4, 0 }, // Doctor V64 BIOS V1.50
        { "69cd5ba6bc9310b9e37ccb1bc6bd16ad", 2, MEMORY_PAK }, // Cruis'n USA (E) [!]
        { "69ce88c46a7c829c6f54004de93efcef", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (E) (Beta) (2000-06-06)
        { "69ed326664e48baa9ce743e6e9c450da", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (U) [b1]
        { "69f3d2580cb4c91c91c2e238c72b4403", 4, 0 }, // Kid Stardust Intro with Sound by Kid Stardust (PD)
        { "6a11f7eb319cf603ab30a281e0639126", 4, 0 }, // UltraMSX2 V1.0 by Jos Kwanten (PD)
        { "6a2b060606cfc20e0ee0a26759fe001b", 4, 0 }, // Mario Artist: Polygon Studio (J) [CART HACK]
        { "6a345402ae1db5ce1041365e36126bce", 4, MEMORY_PAK }, // International Superstar Soccer 64 (U) [!]
        { "6a52188e8a7d008f1921aa8a6d40a74c", 2, MEMORY_PAK }, // AeroGauge (J) (V1.0) (Kiosk Demo) [b1]
        { "6a56589ec8fadd1139ec4b6eed9d19ae", 2, RUMBLE_PAK }, // AeroFighters Assault (U) [h2C]
        { "6a78da715440c0c6872b5d78d31bfd3b", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.08
        { "6a814e33bb6d449be23fbbcac7606c0b", 4, MEMORY_PAK | RUMBLE_PAK }, // Michael Owens WLS 2000 (E) [b1]
        { "6a925ab624ee3b343231d799733ba898", 2, MEMORY_PAK | RUMBLE_PAK }, // Rush 2 - Extreme Racing USA (U) [!]
        { "6a954e08533c13dbd7599068d03a3adb", 1, 0 }, // Super Mario 64 (E) (M3) [h1C]
        { "6ac12c2d6c0ccdf27c0d5179a500bb2d", 4, RUMBLE_PAK }, // F-ZERO X (J) [b3]
        { "6ae97ec4933a13c153a281dc8b68a0d9", 4, MEMORY_PAK }, // Penny Racers (E) [h2C]
        { "6af573eb055648a8542aa82d9524fb2f", 4, 0 }, // South Park - Chef's Luv Shack (U) [!]
        { "6afa476787b762dc08f5b9eeb237c4ee", 2, RUMBLE_PAK }, // Uchhannanchan no Hono no Challenger - Denryu IraIra Bou (J) [h2C]
        { "6b1e294a9199e6f22dbc6abc59bd7568", 1, RUMBLE_PAK }, // Lode Runner 3-D (E) (M5) [b1]
        { "6b2bafe540e0af052a78e85b992be999", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.1) [!]
        { "6b3db6aaa348f5721afaf892e9b274de", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Ita100]
        { "6b4398ce56a9c4786f2087f681fffaf3", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (E) (M3) [t1]
        { "6b4772aa1743feac1919618b3fc6b3e1", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) [b1]
        { "6b53b94b64dd8acae1b43bda2caf23fc", 4, 0 }, // Kyojin no Doshin 1 (J) [CART HACK]
        { "6b5d93b3566e96147009d1ac4fb15c97", 4, MEMORY_PAK | RUMBLE_PAK }, // Super Speed Race 64 (J) [!]
        { "6b626c55c99cd12e88a3db3b52372104", 4, 0 }, // Doctor V64 BIOS V1.93
        { "6b870ebedcbd81b9ff369008f904c1cd", 1, 0 }, // Earthworm Jim 3D (U) [hI]
        { "6b8ba063c40420c164aa81acb6089cb6", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [o2]
        { "6b947f654775cf5dacd1e5d53d577da7", 4, RUMBLE_PAK }, // 64 Trump Collection - Alice no Wakuwaku Trump World (J) [!]
        { "6bab5f2a62a4babaf456d5da2976871d", 1, 0 }, // Mario no Photopie (J) [a1]
        { "6bbd8c42f6ef8f5b9541d6f4db657dd7", 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (U) [!]
        { "6bd5c85e25bf3146a42f962a379b4f54", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW vs. nWo - World Tour (U) (V1.0) [b1]
        { "6be030475c4db52f273ef8a02b4dafa8", 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater 2 (E) [!]
        { "6c25c8d7ce393c0214917a211a199f0c", 2, RUMBLE_PAK }, // AeroFighters Assault (E) (M3) [o1]
        { "6c36e6a39abbab3e9139e9ecbc67e569", 4, 0 }, // Game Boy 64 (POM '98) (PD)
        { "6c5aee2be768e581038f3b211d7f6066", 1, 0 }, // Super Mario 64 (U) (Silver Mario Hack)
        { "6c634d0ce7cd99cccbb4ce8eb837edb4", 4, MEMORY_PAK }, // Mario Kart 64 (U) [o2]
        { "6c65a252f227aef18df2dd3ce04cc821", 4, RUMBLE_PAK }, // Top Gear Overdrive (E) [!]
        { "6c73558de5a1eee6597d7264f9a11b0c", 2, MEMORY_PAK | RUMBLE_PAK }, // G.A.S.P!! Fighter's NEXTream (J) [!]
        { "6cb35999d2e3820391913f0e4b9bc072", 4, MEMORY_PAK | RUMBLE_PAK }, // Xena Warrior Princess - The Talisman of Fate (U) [t1]
        { "6cbac6e5b13a4a92347488de1c806bc0", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [T-Ita0.9P1+G_Vampire]
        { "6cbf4014c053e16852a3db80aeb4c853", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '99 (U) [!]
        { "6ce5d16e977b1d4e0e71e209b2932099", 1, MEMORY_PAK }, // F-1 Pole Position 64 (U) (M3) [b1]
        { "6ceacf73f03ba3de740bdb00d7cc18a1", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f2] (PAL)
        { "6d1d465ebaacd3747b1362037abed9ef", 4, MEMORY_PAK }, // Jikkyou J.League Perfect Striker (J) [b3]
        { "6d38909faa2840fc409afa221489de49", 2, RUMBLE_PAK }, // Transformers - Beast Wars Transmetal (U) [!]
        { "6d3db67319da339df4b68ad0084904d5", 2, MEMORY_PAK }, // Virtual Pool 64 (U) [!]
        { "6d4cba7fb2e910ca1a87e63a4c1924c2", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (U) [b1]
        { "6d5147a9cd7ddd0f1fa8b8bda439a734", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (E) (V1.0) [b1]
        { "6d58a01ef7a7a7c779d2a66315992c5f", 2, MEMORY_PAK | RUMBLE_PAK }, // Supercross 2000 (E) (M3) [!]
        { "6d75719bfa6244c4591070d3f7356072", 1, 0 }, // Dexanoid R1 by Protest Design (PD)
        { "6d84657c9a145e67b2a8a61cd31a7218", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.0) [b2]
        { "6d954294e254e239b3ea694c2d9adff9", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (U) [h1C]
        { "6da848a70d83ece130d274124760928e", 1, 0 }, // Harvest Moon 64 (U) [!]
        { "6dc6820cef755fc1253d06df45c9bd2a", 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.2) [!]
        { "6dc88503df78fe4faa16bcf7aec4d1e7", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.1) [T+Ita1.0Beta_Vampire]
        { "6dcfde0423d1db58ea9e6a8f1bffd849", 4, 0 }, // Explode Demo by NaN (PD)
        { "6df0d6259261d0096c90bbc6aa037d8e", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (U) (V1.0) [!]
        { "6df573ee08c4577bbbf117b813d93b19", 4, 0 }, // Pause Demo by RedboX (PD)
        { "6e01b8d425ae74ef5a0f875c700a3b18", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2001 (U) [!]
        { "6e0af13dcefee6a11c4d7262206d6d2d", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (J) [!]
        { "6e0fcff93600a28c379ef52940c54e68", 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (U) [f1] (PAL)
        { "6e207c47faad2a4f64ae721a3a8f6161", 2, MEMORY_PAK }, // Bottom of the 9th (U) [b1]
        { "6e23e6bad28f8ac5c07344dcb0c266b8", 4, RUMBLE_PAK }, // GoldenEye 007 (E) [T+Spa2.0_Sogun&IlDucci]
        { "6e3fbaf95c79a3668b6a9211f1372216", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b4]
        { "6e51b140a9b0c30d6f55ca7942a969fc", 2, RUMBLE_PAK }, // Tsumi to Batsu - Hoshi no Keishousha (J) [T+Eng1.0_Zoinkity]
        { "6e5858a47cb42f9b1cb26042ba229ef6", 2, RUMBLE_PAK }, // F-1 World Grand Prix (G) [o1][h1C]
        { "6e5e919d0fa292b97aad08e9200e8c9c", 1, RUMBLE_PAK }, // Yoshi's Story (E) (M3) [b1]
        { "6e6e7a703c131adaddf4175e9037a2eb", 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (U) [!]
        { "6e7c82aff9f73a3dbf5b18d924bdf103", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (U) [b1]
        { "6e81d3056e409208e4af2d39a2ff0f03", 4, MEMORY_PAK }, // Wetrix (U) (M6) [!]
        { "6eacfe4f39265f24fb8145fded8272f6", 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [b1]
        { "6eea5c4a6256092ed8f9ba8861c689c6", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes 2 (U) [!]
        { "6ef19bf8d8d6196390745f1b858ac16a", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.1) [!]
        { "6ef9fed309f28bd59b605f128869aa00", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (E) (M3) [!]
        { "6f14b999499e90761d8444f784564ab3", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [o1]
        { "6f2c37a20e6eccb657fbfc4ba36a34bb", 2, MEMORY_PAK | RUMBLE_PAK }, // Triple Play 2000 (U) [!]
        { "6f32b4de3b3fc0c719bf9ea27efc18be", 1, MEMORY_PAK }, // Quest 64 (U) [b1]
        { "6f378ffb873c217675f4cdd00a0318d4", 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (U) [b2]
        { "6f7030284b6bc84a49e07da864526b52", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (U) [!]
        { "6f868b1652183376665f67f8183eeb5c", 4, MEMORY_PAK }, // Rat Attack (E) (M6) [h1C]
        { "6f936ece2ba8ea8c74bdbba9bda1407a", 4, 0 }, // Cube Demo (PD) [b1]
        { "6f9e56c8a2b6ba5f6dbe5214e352590d", 1, 0 }, // Yuke Yuke!! Trouble Makers (J) [a1]
        { "6fec88e88ed25ae7e823a69444d6098a", 1, MEMORY_PAK }, // Pro Mahjong Kiwame 64 (J) (V1.0) [o1]
        { "7037df7e0050a6d8b9b338c774a1ceb0", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Rus1.0beta2_Sergey Anton]
        { "70415d917d5b7beefedafdb65cd64d43", 4, MEMORY_PAK }, // Baku Bomberman (J) [b1]
        { "7044e135e702b32dfcf13e688461967f", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 99 (U) (M5) [b1]
        { "7052f9c1be69b103d6be3de61a94f861", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [h1C]
        { "70632ee119e54f76a344a7b04340a6f9", 4, MEMORY_PAK }, // Baku Bomberman (J) [t1]
        { "706db19d4aa4eaf902c8b48544884cef", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.11 (Odd Bytes)
        { "709f966c30ce6df1833e95740a5a2ab2", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 98 (U) [!]
        { "70a61772b2123879367011a5e2900569", 4, RUMBLE_PAK }, // SmashRemix1.1.1
        { "70c3e7412ff8196e16cca076b3b622f7", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [b4]
        { "70c525880240c1e838b8b1be35666c3b", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [!]
        { "70d480a73db5751eed9e826108005834", 4, MEMORY_PAK }, // International Superstar Soccer 64 (E) [h2C]
        { "70de1eab508596b6bbefd168b5d07194", 1, RUMBLE_PAK }, // Indiana Jones and the Infernal Machine (U) [!]
        { "70e9eb9bf2f7bc76ca38ce450ba01c2e", 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) (Debug Version) [f1] (Decrypted)
        { "70f73d01feaf768758f31b23de2c2620", 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (E) (M3) [t1] (P1 Untouchable)
        { "70fd44b722b55956be86b660f153713d", 4, 0 }, // PC-Engine 64 (POM '99) (PD)
        { "710ba49ebd5c9a2b26653fae93bd667a", 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (E) (M2) [!]
        { "7151ae130ba8dc97a205b62ff0ee0fb6", 4, MEMORY_PAK }, // Choro Q 64 (J) [h1C]
        { "715a8816f30fa24b8d174dc5cb6f25a9", 4, RUMBLE_PAK }, // Banjo to Kazooie no Daibouken 2 (J) [!]
        { "715d537c6d78440c085ddd22e827c260", 1, RUMBLE_PAK }, // Command & Conquer (U) [f1] (Z64)
        { "7173250dc66ca58024c1c4d27b0f8720", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [t1]
        { "71be7a48778737fddd3cde7849502c5b", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [f1] (PAL)
        { "71c2e66cce1fec929b5485ac41a61d95", 4, 0 }, // SRAM Upload Tool V1.1 by Lac (PD) [b1]
        { "71dcdbe9f4b37a251e2877900cdbf406", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [b1][f1] (PAL)
        { "71ec90e34421ceeaa8b71acaf76efd33", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [b2]
        { "71fbae5d2b27926ea54e92ce2fc91622", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.1) (Debug Version)
        { "7200d1c1cf489fafff767729f215e6e6", 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (V1.0) [!]
        { "7211951fdf7da809d5f3d51ca06cd465", 2, 0 }, // Tetrisphere (U) [b1]
        { "721fdcc6f5f34be55c43a807f2a16af4", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [!]
        { "722c3a74a90305d6079a37994cebf5b2", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Cup (J) [!]
        { "722d55c70368323d79ab1b63696a45e4", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx (U) [b1][t1]
        { "7258f4ab367b025c95a4f476c461e717", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (U) [!]
        { "7260da1cecd0d8844c5e29aa63476def", 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (E) (Beta) [!]
        { "72775ab85b5106a97decb0520dfd9180", 4, 0 }, // Z64 BIOS V2.10NTSC
        { "72a6aa28608ee93a1cb6feb0a5f4c28c", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Legenden des Verlorenen Landes (G) [!]
        { "72b60fac5ee257fa387b43c57632d50c", 2, RUMBLE_PAK }, // StarCraft 64 (E) [!]
        { "72ba2c693efa5ff3b80374cdbd0c957b", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (V1.0) [t2]
        { "72c7ffcea6c1430616867616f5e9d51a", 2, MEMORY_PAK }, // AeroGauge (U) [!]
        { "72f324b7e115618771c506af3f604f8f", 4, 0 }, // MSFTUG Intro #1 by LaC (PD)
        { "730209ad52c178551c29074f49ae9b39", 4, 0 }, // Z64 BIOS V1.07
        { "73084495f3209c54900525436bbbc531", 2, MEMORY_PAK | RUMBLE_PAK }, // Hiryuu no Ken Twin (J) [!]
        { "73113ae7aaf0911ba6a1df1353980d14", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (E) [T+Spa1.0.2_IlDucci]
        { "73165cbe4e2421475c5fe9a227dcafec", 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (E) [T+Spa2.0_IlDucci]
        { "732167d19c627caf7384c780c64ab80a", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [o1]
        { "7344483130995ca211b5b9ff9891a9de", 4, MEMORY_PAK }, // NBA Jam 99 (U) [b1]
        { "7344708b7e0fc0683bba86d902b9603d", 4, 0 }, // Zelda 64 Boot Emu V1 by Crazy Nation (PD) [a1]
        { "73971508a3b4ca92f7f4cf59c9170372", 4, 0 }, // Rotating Demo USA by Rene (PD) [a1]
        { "73bb54ffd3c0fc71f941d9a8cc57e2a1", 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [!]
        { "73c6d87dbe50f73f3b44e0f237a546d7", 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (U) [!]
        { "73ffacdb7f649154984a5957ed451257", 4, 0 }, // Kyojin no Doshin 1 (J) (Kiosk Demo) [CART HACK]
        { "740ad4db03952bbe997db09947a41e62", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [!]
        { "7412417c50a42427637575a3cd2e652e", 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [b1]
        { "741a94eee093c4c8684e66b89f8685e8", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.1) [!]
        { "74341014043cc42d8804dadbf252b344", 4, RUMBLE_PAK }, // Chou Snobow Kids (J) [f1] (PAL)
        { "745fcd72a921ed178af6c319b09df1f3", 4, RUMBLE_PAK }, // Jet Force Gemini (U) [b2]
        { "74650f7154e0b2dd7c364f511b0d6a77", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (G) [!]
        { "747e76d50dc3c06fd35e146129706a60", 4, RUMBLE_PAK }, // Charlie Blast's Territory (U) [!]
        { "74904f302a25620d7f63c0cb5333d464", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (E) (M3) [o1]
        { "7496f1814afbd1fc85fe76d6ebe9ea03", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [t1]
        { "7497adadada89230827c06e4bffbbbd8", 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [o1]
        { "74eb415e16c333b252847a8e09432fd9", 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (U) [!]
        { "75239d11aee906c9abea752ba1e2aa00", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (U) [b2]
        { "7527b90543e3d6042a72619818b47145", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [o1]
        { "752ef6d2c2a6dbbb8cbaefb596412c1d", 1, MEMORY_PAK }, // Eltale Monsters (J) [b1]
        { "753437d0d8ada1d12f3f9cf0f0a5171f", 4, RUMBLE_PAK }, // F-ZERO X (U) [!]
        { "7558e3da7225712936d3ba3dce210c36", 2, RUMBLE_PAK }, // AeroFighters Assault (E) (M3) [!]
        { "755df7f57edf87706d4c80ff15883312", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Air Combat (U) [!]
        { "756325378480e1252cb089110a689563", 4, 0 }, // The Corporation 2nd Intro by TS_Garp (PD)
        { "7579ab0e79b1011479b88f2bf39d48e0", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (J) [!]
        { "759358fad1ed5ae31dcb2001a07f2fe5", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis (U) [!]
        { "759b8749e55113526e8c1c468e244ca8", 4, MEMORY_PAK }, // Jikkyou J.League Perfect Striker (J) [b1]
        { "759df0adf7f78d603ad344c89f360e32", 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (U) [h1C]
        { "75a8f25b679a0d148f0d8e1143104845", 4, 0 }, // Eurasia First N64 Intro by Sispeo (PD)
        { "75ad771425cbe2b1e8c7b4d94e67b1ca", 2, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja 2 Starring Goemon (E) (M3) [hI]
        { "75aea2b252309552e434b30edfe72711", 4, RUMBLE_PAK }, // F-ZERO X (J) [t1]
        { "75c0121d84915bf5c1fbd389cf9f9c17", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 2000 (E) [!]
        { "75f197def1d084534857493582d84e73", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (E) [o1]
        { "75f4a7e5932fb4ae791875e770e51c72", 4, 0 }, // Fireworks Demo by CrowTRobo (PD)
        { "75f91a5d3012080ee9bf90c017b5128f", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [b4]
        { "760110bc5733a4ac31ee680a06f4f2a1", 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (U) [hI][t1]
        { "760d5f0a578d65e68f5b4e8f64271c88", 4, MEMORY_PAK }, // NBA Showtime - NBA on NBC (U) [f1] (PAL)
        { "76176b237d4f60e59f98d247283d9365", 4, 0 }, // Mario Kart 64 (Ch) (V6) (iQue) (Manual) [!]
        { "761dc0de0913d697ed1f9899948552bb", 4, 0 }, // Eurasia Intro by Ste (PD) [b1]
        { "762543cc6409c199fd50a3bcf545730d", 2, RUMBLE_PAK }, // StarCraft 64 (E) [f1] (NTSC)
        { "763e5eaca4606e15a1e4d67dd6d987c0", 4, 0 }, // XtraLife Dextrose Demo by RedboX (PD) [h1C]
        { "764dcdf5163d3fce81d3e7189fa97e05", 4, 0 }, // WideBoy BIOS V980910
        { "764f22ad3d0f59667a7f083d2f789b31", 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (U) [!]
        { "7657c4bb92e3f6f126d94f5942fdb445", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b4]
        { "7658075d52c403783640dfac2f8b6a9e", 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (U) [f2] (PAL)
        { "765a330d5ce2dbe7120c6c8e18a1487d", 2, 0 }, // Tetrisphere (E) [!]
        { "765dc057507aa32daaec194e32ff26b1", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f2][b1]
        { "76996a6cc868ac0e2bb2506a3c4db606", 4, RUMBLE_PAK }, // Jet Force Gemini (E) (M4) [f1]
        { "76a1480be9985d30fc580e4216974d3e", 4, 0 }, // Y2K Demo by WT_Riker (PD)
        { "76a8bbc81bc2060ec99c9645867237cc", 4, RUMBLE_PAK }, // Mario Party 3 (U) [!]
        { "76ae2b6f38e611683aca3b87d1b3d5ce", 1, 0 }, // Pocket Monsters Snap (J) [f1]
        { "76c8efdd11f6d2e553055b801a022653", 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (U) [b1]
        { "76db89759121710c416ecfb1736b5e39", 4, MEMORY_PAK }, // NBA Showtime - NBA on NBC (U) [f1] (Country Check)
        { "770d0d2bb08235613569667a284bca2c", 4, 0 }, // Dragon Sword 64 (E) (Prototype)
        { "771fc17cdc840ed36db8a699757c37f8", 2, 0 }, // War Gods (U) [t1] (God Mode)
        { "772cc6eab2620d2d3cdc17bbc26c4f68", 4, RUMBLE_PAK }, // Jet Force Gemini (U) [!]
        { "772fa166e5db51effc77fb8d832ac4d2", 1, RUMBLE_PAK }, // Densha de Go! 64 (J) [!]
        { "77314fe3c3edd472d0554c12fedb38f4", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (GC) [f1]
        { "773fd446da7f4e392907505053bf2a42", 4, RUMBLE_PAK }, // Top Gear Overdrive (U) [o1]
        { "77554810629d4ef4d93519745fa6066b", 1, RUMBLE_PAK }, // Body Harvest (U) [b1]
        { "775e6d9305bf219c0696063c809f2b48", 4, 0 }, // Neon64 GS V1.1 (Gameshark Version) (PD)
        { "77bf20d3eca19ebcfc5fed35f5cf65dc", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Por1.0]
        { "77ce01c32e172c7dd812c4f1179fba2e", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [b2]
        { "77dc38cce0ffab004fd54f54ee25a593", 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (U) [f2] (PAL)
        { "7818696426c0a429fbfccc4efe8d5570", 4, RUMBLE_PAK }, // Top Gear Overdrive (U) [!]
        { "781ae174beda8641e3f0e1efad761050", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.30 (Even Bytes)
        { "7853f02dc66a35bc8c2bc33d03b8f0ca", 4, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (E) [!]
        { "78551d23f230b58b9f449cdb4a285761", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball '99 (U) [!]
        { "7863ed3ee54f72bd59cf970c5c402fa2", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T-Rus0.2_Alex]
        { "78771beb349d481e69baa9225b36d63a", 4, 0 }, // Mario Kart 64 (Ch) (V4) (iQue) [!]
        { "78838c202c4ff5a460586451ee9182aa", 4, MEMORY_PAK }, // Chou Kuukan Night Pro Yakyuu King (J) [!]
        { "789482b7931419fce7fc2472026e1c65", 1, RUMBLE_PAK }, // Lode Runner 3-D (U) [t1]
        { "78bb5b9a279156bd61be132817a9b2c8", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (E) [o1][h1C]
        { "78cabb696ca4847bfad09b01257c6fda", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [h2C]
        { "78d5f8a98a5ed21d0817856bcd2ad750", 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (E) (M3) [!]
        { "78e2ed1db5f90e2ea9c04771b27685bc", 1, 0 }, // Heiwa Pachinko World 64 (J) [T+Eng1.0_Zoinkity]
        { "78f83525e0cc98641aeace37e3ff7f16", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [T+Spa2.0_Sogun&IlDucci]
        { "793dbf504e20c92f6b73b4e8a25a220c", 4, 0 }, // J.League Tactics Soccer (J) (V1.1) [!]
        { "795f7c1d3d98125c3aea0fffe54410de", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (U) [t1]
        { "7966f17f36c43d3807c0949aad45168a", 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (U) [f1] (PAL)
        { "79743743904eea727cb82725be765ec6", 4, MEMORY_PAK }, // Mario Kart 64 (U) [T+Spa1.1_Blade133bo]
        { "79748e4e030b047dbae8252c21305c75", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.0) [f1]
        { "7995d76f4ce236b0f0289f47ae523d32", 1, MEMORY_PAK }, // Pro Mahjong Kiwame 64 (J) (V1.0) [!]
        { "79a8a007092d1e5b00d0ed94f83620eb", 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) [T+Spa1.1_Blade133bo]
        { "79bd05a97e50ac63a638e4e90ab9588b", 2, RUMBLE_PAK }, // Tsumi to Batsu - Hoshi no Keishousha (J) [T+Spa1.0b_TheFireRed&IlDucci]
        { "79bdd4467e102a1cbb4cc74dfc96bf7d", 3, 0 }, // Jeopardy! (U) [o1][h1C]
        { "79be4eeeb2dd94e542516dfc189b099e", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.33]
        { "79fb6e2452af077c5ef1dde5fc810f04", 2, RUMBLE_PAK }, // AeroFighters Assault (U) [!]
        { "79fcc98002d1f4c79deaf55784222df8", 2, RUMBLE_PAK }, // Magical Tetris Challenge (U) [!]
        { "7a28179b00734c9aa0f0609fafaafd5f", 4, MEMORY_PAK }, // New Tetris, The (U) [!]
        { "7a4acab8997d180c045d9286c917ed1b", 4, 0 }, // Clay Fighter 63 1-3 (U) [b1]
        { "7a558bbad8ce8828414a9cf3b044a87d", 2, 0 }, // Mortal Kombat Trilogy (E) [!]
        { "7a5d0d77a462b5a7c372fb19efde1a5f", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [b3]
        { "7a638431ce966dda97b282ff04b1d9d7", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [b1]
        { "7a94f94485bd28f0d6d67257050b26d4", 4, 0 }, // Taz Express (U) (Prototype)
        { "7a99628edf0a6602d0c408f31b701435", 4, RUMBLE_PAK }, // Lylat Wars (A) (M3) [!]
        { "7ab29c6ad2d8f4007d8213eb3411e0bd", 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (E) (M3) [!]
        { "7ac3b9a3aa846c439575912811b3e6e0", 4, RUMBLE_PAK }, // F-ZERO X (U) [f1]
        { "7ad954add3779fb3be5904d1b935ebfb", 4, 0 }, // GameBooster 64 V1.1 (NTSC) (Unl) [b1]
        { "7ae0ba601fd72514c984bc80ee6bab8e", 1, RUMBLE_PAK }, // Space Station Silicon Valley (U) [f2] (PAL)
        { "7af70073e7cfd8945e087a7ffd6d8348", 1, 0 }, // Ogre Battle 64 - Person of Lordly Caliber (J) (V1.1) [b2]
        { "7b261247150c431de55ab371e8b46ea8", 1, MEMORY_PAK }, // Tokisora Senshi Turok (J) [!]
        { "7b38c3f7da8d9778b41b0047a133590a", 4, MEMORY_PAK | RUMBLE_PAK }, // HSV Adventure Racing (A) [b1]
        { "7b3ad8b6fe2d8f0602ed1006fd673970", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.37]
        { "7b41f28a920112f17401d633b368ba0c", 1, 0 }, // Tigger's Honey Hunt (U) [t1]
        { "7b593077efc15b44c8d8ff639d6fb434", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [o2][t1]
        { "7b5988138a8adca592de616260b2700d", 2, RUMBLE_PAK }, // Last Legion UX (J) [b1]
        { "7b5afa5d60f53fa640a5f2f24f5b2741", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter Destiny 2 (U) [f1] (PAL-NTSC)
        { "7b7b2916a857b9fc513c5f9619b36c98", 0, 0 }, // Wet Dreams Readme by Immortal (POM '99) (PD)
        { "7b815846ec91e6c4a8b8baa0ce4078f0", 4, 0 }, // Super Smash Bros. (Ch) (iQue) [!]
        { "7ba990008966bda71d8cb78fb7ba414e", 1, RUMBLE_PAK }, // Super Mario 64 - Shindou Edition (J) [h2C]
        { "7bb3974754a54ba27a93f96691dc4695", 4, 0 }, // Dezaemon 3D Expansion Disk (J) (Proto)
        { "7bbd5a1594526de1ce6a16f4faf82e7d", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) [h1C]
        { "7bc08eaf55a645de9226617c3f83b19f", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.1) [t1]
        { "7bd3f10d47db6d062280d8ae750a1acb", 1, MEMORY_PAK }, // Human Grand Prix - New Generation (J) [o1]
        { "7bd97d75d09e38996f82c6190780d447", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (U) [b1]
        { "7be2aab259c437e8ba91ff8e5903837c", 4, RUMBLE_PAK }, // Donkey Kong 64 (E) (M4) [f1] (Save)
        { "7c567a5bb1ad4cbe414fb6bbff66e336", 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [!]
        { "7c58db7ee170912a2df84e67a489058a", 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T-Eng2007-02-08_Brandon Dixon]
        { "7c5e794e856311403fef548dae98a183", 4, MEMORY_PAK }, // Mario Kart 64 (U) [o1]
        { "7c63bf480d1f84fc2c0d4a959154fa27", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (E) (M3) [h1C]
        { "7c74d08a04b6b1b670d90cdb153f083c", 4, RUMBLE_PAK }, // Chameleon Twist (E) [b2]
        { "7c87831b28d5e49f4ec2bdae9d01f3b9", 4, 0 }, // Freekworld BBS Intro by Rene (PD) [a1]
        { "7c8efcf4fba28f9f5b5ea10a71283bf3", 4, BIO_SENSOR }, // Tetris 64 (J) [!]
        { "7c9c000b61bf0bcf81afd4e583ec9b2a", 1, MEMORY_PAK }, // Blast Corps (E) (M2) [b1]
        { "7cbb43473597f48aa9a8e0b0d38ce685", 4, RUMBLE_PAK }, // Charlie Blast's Territory (U) [hI]
        { "7cc16a6090cac3749dd6b71ca7a58605", 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [b1]
        { "7cc328ffaa5b77cd6247124dd0af8899", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.0) [b1]
        { "7ccb9b385d1405603e7f645352ea62aa", 4, 0 }, // Freekworld New Intro by Ste (PD)
        { "7ccc138882a64267ad58449f309fc1d1", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [f1]
        { "7cd386c8d2e69dbbf55f02a564bd6a9a", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [b1]
        { "7ce28adab6fe43207b4bb19564c38aef", 4, MEMORY_PAK }, // Golden Nugget 64 (U) [b1]
        { "7ce4559fed0ea7d82670e9a4ecc370cd", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [t1]
        { "7d32a665027abb585bd4fb7194752b34", 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (U) [t1]
        { "7d3e935156844de0002db875e1076a5c", 1, MEMORY_PAK }, // Tonic Trouble (U) (V1.1) [!]
        { "7d47911b5c3d91a303ef19e764f3c02b", 2, RUMBLE_PAK }, // Sonic Wings Assault (J) [!]
        { "7d54d888964a3b0d058eb80886c44c88", 4, MEMORY_PAK }, // Mario Kart 64 (U) [h1C]
        { "7d5afcd953bb15ec204b175761ca28c2", 4, 0 }, // J.League Tactics Soccer (J) (V1.0) [f1] (PAL)
        { "7d61bb9d25baf9a608139592dc4e28dc", 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (E) (M5) [b1][f1] (NTSC)
        { "7d6ac6295ef6a0abb3d4594af407b7a6", 2, 0 }, // 77a by Count0 (POM '98) (PD)
        { "7dba0d5d4279274af07b0ef3ec3a84ad", 4, 0 }, // PGA European Tour (E) (M5) [f2] (NTSC)
        { "7dcc05b98e2fa690b478808ebbad5d1a", 4, MEMORY_PAK }, // International Superstar Soccer '98 (U) [!]
        { "7dceaff24bb1bb3d9b8fe9b78ccc7048", 2, RUMBLE_PAK }, // V-Rally Edition 99 (E) (M3) [f1] (NTSC)
        { "7dd2f0e19e35511c3e1c81d206cdc472", 4, 0 }, // GoldenEye 007 (U) (G5 Multi (for backups) Hack)
        { "7ddbca3f642cb4caa780e81733c94037", 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T+Eng90%]
        { "7ddf1cc63401cf43836bd14f199b6c70", 1, 0 }, // Super Wario 64 V1.0 by Rulesless (Super Mario 64 Hack) [T+Ita]
        { "7dfe40e387971269f38fe3cbe803567a", 2, RUMBLE_PAK }, // F-1 World Grand Prix (J) [h1C]
        { "7e0ac3917227edf51132fbebb867ff9b", 4, 0 }, // V64Jr 512M Backup Program by HKPhooey (PD)
        { "7e0da017551101da1c4fb6a0c49a2328", 4, 0 }, // Doraemon - Nobita to 3tsu no Seireiseki (J) [b2]
        { "7e3e9bc4188b37a4fc283e22bedb5d49", 2, RUMBLE_PAK }, // AeroFighters Assault (U) [b2]
        { "7e4bca92da50145c9906b2a72f9351b5", 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [f1] (PAL)
        { "7e6456e104503b51a97735c8ff349e4c", 4, 0 }, // Ultra Demo by Locke^ (PD) [a1]
        { "7e856fa8f743900feba5a4e28c234af7", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz - Special Edition (U) [!]
        { "7e8f028411746cf816848384b788b624", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (U) [b3]
        { "7eb07675e2e9cb547a3b238aa71f57cc", 2, RUMBLE_PAK }, // Ganbare Goemon - Derodero Douchuu Obake Tenkomori (J) [t1]
        { "7eb0c27f162b74032c3823e721eda00b", 4, 0 }, // Eleven Beat - World Tournament (Aleck64)
        { "7ec0484c2ba6aa9f0a45d7ac1f4117da", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 99 (E) [!]
        { "7ed3f10bc32cf76f172d8c31d15a2799", 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (E) (M3) [!]
        { "7f0900dd9726a7e56a398e910d014b61", 2, MEMORY_PAK }, // Rakuga Kids (E) [f1] (NTSC)
        { "7f18a06bb16a507e23f9dd636b7046a6", 4, RUMBLE_PAK }, // Super Smash Bros. (A) [f1]
        { "7f1991b8861e7e532ec21ecf2af82191", 1, MEMORY_PAK | RUMBLE_PAK }, // Spider-Man (U) [!]
        { "7f244aff8d729417e32b6a5b299afda5", 4, MEMORY_PAK | RUMBLE_PAK }, // Telefoot Soccer 2000 (F) [!]
        { "7f34eb3e1363580fea95b23a9cb1c3af", 4, 0 }, // Doctor V64 BIOS V2.03 (Green)
        { "7f4171b0c8d17815be37913f535e4e93", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (U) (V1.0) [!]
        { "7f4e8f2ecf04be27e660ac2efdac0c68", 1, MEMORY_PAK | RUMBLE_PAK }, // Ganbare Goemon - Neo Momoyama Bakufu no Odori (J) [b1]
        { "7f4ed2aaf94a2197b0ad63c6ece9dea9", 1, 0 }, // Mario no Photopie (J) [!]
        { "7f57463856540104b21a5289312b626f", 4, MEMORY_PAK }, // Derby Stallion 64 (J) [!]
        { "7f6c5e71711dec81e77ccf71060f67ca", 2, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz (U) [!]
        { "7f919d2e35cbe561e139ae8fe93aca86", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (E) (M3) (V1.0) [!]
        { "7f9cdbbb1aaaaf0983c64988ef9c58be", 2, 0 }, // Space Dynamites (J) [!]
        { "7fb25633fdb226f35a222b4ea17bd2dc", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis (U) [f1] (Save)
        { "7fb6422e5bbf60fee39651eff3f79bab", 4, 0 }, // All Star Tennis '99 (E) (M5) [f4] (NTSC)
        { "7fc933a64884a382aa07605ea7204ff5", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu - Basic Ban 2001 (J) (V1.0) [!]
        { "7fcc22f4bf1fc82f886c455981d2a420", 4, MEMORY_PAK }, // International Superstar Soccer 64 (E) [b1]
        { "7fccb47498eec06e96ae9372247d1e90", 4, MEMORY_PAK | RUMBLE_PAK }, // Destruction Derby 64 (U) [!]
        { "7fcf424b960f4e429fd683a4b19b3356", 4, 0 }, // Super Fighter Demo Halley's Comet Software (PD)
        { "7fd3ba9b095c244367e84a6e4493349b", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Por1.5BetaFinal]
        { "7fd6bffb80f920e01ef869829d485ea3", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [!]
        { "7fdafd3252c0f59ffba59cd42729d959", 1, RUMBLE_PAK }, // Banjo to Kazooie no Daibouken (J) [f1]
        { "7fdc16c96b517d4e433cf7353b22fba6", 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 64 (J) [b1]
        { "7fe135d08035782a8aae0befe9a9780b", 1, RUMBLE_PAK }, // Wild Choppers (J) [o1]
        { "7fe719dde2cf1c9227bb9650afa21a4b", 1, RUMBLE_PAK }, // Neon Genesis Evangelion (J) [f1] (PAL)
        { "7fec099d1a989d5222d3f9e1a7770404", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 2000 (E) (M4) [!]
        { "7ffd84fc7d112eab4eef15cb10189a38", 4, RUMBLE_PAK }, // Charlie Blast's Territory (U) [hIR]
        { "800acc7d609ecdb3e09e68cbd05f5fa0", 4, 0 }, // J.League Tactics Soccer (J) (V1.0) [!]
        { "801ecc49daf04fba099c6f75a06f0454", 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (U) [b1]
        { "8043d829fcd4f8f72dd81e5c6dde916f", 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (U) [!]
        { "8046a4b8abd4353b2ab9696106ccf8d2", 4, MEMORY_PAK | RUMBLE_PAK }, // Jeremy McGrath Supercross 2000 (U) [!]
        { "805f74aef610ba2c1f6fcef723cd1970", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o3]
        { "8095b5926a9fa7a0c9da7b46731513a1", 4, 0 }, // UltraSMS V1.0 by Jos Kwanten (PD)
        { "80b4f92970cbb13344f57e827c60959b", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.0) [b2]
        { "80cc112f62e9a8581a1bb6a1d1e1488b", 1, MEMORY_PAK }, // Holy Magic Century (E) [!]
        { "80e9d39b21e537ff53fd362b7272f255", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [o1][f1]
        { "80f3b1abd9fb9ae73997489db185a74d", 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (U) [!]
        { "80f4cd56317218264e35894ba22cad35", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (U) [t1]
        { "80f918d90df42c9841ae998a89223c1e", 4, MEMORY_PAK }, // NBA Hangtime (U) [o1]
        { "8107825ac2a522057422463ed81e276b", 1, RUMBLE_PAK }, // Bakuretsu Muteki Bangai-O (J) [!]
        { "810f8bc2c8c66bda3b206c7dd4b6d42f", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey '98 (U) [!]
        { "8113d0ea2008402d4631f241f625d16b", 3, MEMORY_PAK | RUMBLE_PAK }, // Rampage 2 - Universal Tour (U) [!]
        { "813ad5c00bad7c4d41f8558cecedae51", 2, MEMORY_PAK }, // Rakuga Kids (J) [!]
        { "8183688a4b7d0a390496b5655bcd252e", 4, MEMORY_PAK }, // Baku Bomberman (J) [!]
        { "818737338b3f1d87b7e1b7fb55057bb5", 1, 0 }, // Star Wars - Shadows of the Empire (E) [b1]
        { "8191f6945dab90a17e752aa3c94b0402", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.21 (Even Bytes)
        { "81a2c7e1ec426d3aee1c4b823a2bc7bf", 4, 0 }, // Neon64 GS V1.2 (Pro Action Replay Version) (PD)
        { "81b1122ee15f7b50a341ae62e9c5716b", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (E) (M3) [!]
        { "81efb80db5e014d865ec2ec88c393e4d", 4, MEMORY_PAK | RUMBLE_PAK }, // Wayne Gretzky's 3D Hockey (J) [o2]
        { "81fd532b1c7c2ccd687e56696e073c83", 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [b2]
        { "81fe5ad03175b6f2d23dbf5a549b0cda", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [t2]
        { "820929ebbe6fd332ac1720f94b745a8b", 4, 0 }, // Die Hard 64 (U) (Prototype) (Level 1)
        { "821a739790f1d390dc7d4168c5e032de", 4, 0 }, // HSD Quick Intro (PD)
        { "821e7f74880bbd04aa9501d3bb25138e", 4, RUMBLE_PAK }, // Jet Force Gemini (E) (M4) [T+Ita0.9beta_Rulesless]
        { "822154ee2b96bf78bce2e46af6578131", 2, 0 }, // Tetrisphere (E) [b2]
        { "8222d1129394ab4f382476d8231ef9f6", 4, 0 }, // BMP View by Count0 (PD)
        { "82388a4b44ef74f9c250c8152ab93669", 1, MEMORY_PAK }, // Jangou Simulation Mahjong Do 64 (J) [b1]
        { "825ea802bc3cba523667b64dd696c597", 4, MEMORY_PAK | RUMBLE_PAK }, // Destruction Derby 64 (U) [t1]
        { "826ae85b4e74354ee378e8dc2ad33271", 2, MEMORY_PAK | TRANSFER_PAK }, // PD Ultraman Battle Collection 64 (J) [f2] (PAL)
        { "827106236756dc5b585c4226184835fa", 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy-kun no Bura Bura Poyon (J) [o1]
        { "827cffd3648116511ed2d5da76c83fbb", 4, 0 }, // Cube Demo by Msftug (PD)
        { "8286ded701dfa22436d311bd5b93bd29", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (E) (M4) [!]
        { "8287a908e36e79b2d3af0bd22c43ecd9", 4, MEMORY_PAK }, // Choro Q 64 (J) [!]
        { "828e0c399c0aa7e3b047859f4fad3550", 1, RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 - Shiokaze ni Notte (J) [b1]
        { "829e4f5ddeced5db6c64e346eda47e5c", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [t1]
        { "82c6213ef921bf2805b3ca7ca4f08bfb", 4, RUMBLE_PAK }, // Cruis'n World (U) [b2]
        { "82fe3916997da14a02c2301ac2b5e321", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [h1C]
        { "831dc9eeb9114912ababc15b6b14da4b", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [o3][f1]
        { "835b149a0a312262a69daa010728a825", 4, RUMBLE_PAK }, // Snowboard Kids 2 (U) [f2] (PAL)
        { "835e98c0b8a16401bdc6653e8855f0d6", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o7]
        { "8363f102fe496b8d06494aa22e183a3e", 4, 0 }, // F-ZERO X (U) [f1] (Sex V1.1 Hack)
        { "83673d0a0ef4705c624043788ebf11ee", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (E) (GC) [T+Pol1.0]
        { "83807a5437a2ea21e2133dba1e410750", 1, MEMORY_PAK }, // Wonder Project J2 - Koruro no Mori no Jozet (J) [T-Eng0.05]
        { "83b122b18b98b96a63c6f824833116bc", 4, 0 }, // Doctor V64 BIOS V1.75
        { "83b75800690af27800df362697e5205e", 4, 0 }, // Plasma Demo (PD) [a1]
        { "83c51e3cd82a0d86cace6d852990814f", 2, MEMORY_PAK | RUMBLE_PAK }, // Mike Piazza's Strike Zone (U) [h3C]
        { "83cb0ce82555fe733a121c052bdf3662", 4, 0 }, // Neon64 V1.1 by Halley's Comet Software (PD) [o2]
        { "8400c0875e16f599c1b7fc433e339d58", 4, 0 }, // F-ZERO X (Ch) (V2) (iQue) (Manual) [!]
        { "843db032839bf1aa74759f2e39a2abdc", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) (Kiosk Demo) [b1]
        { "84706e10026bd2ee9654da5e9821598d", 4, 0 }, // Dezaemon 3D Expansion Disk (J) (Proto)
        { "847a00f2c5da6696fc49702431df3150", 4, RUMBLE_PAK }, // WCW-nWo Revenge (U) [f2] (PAL)
        { "8485643e5830cd67ed4c0a5fd49e2491", 4, 0 }, // Mario Artist Paint Studio (J)
        { "849da5f1790a386b1cd9118ca1398e9d", 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [b1]
        { "84b1421f658d5185812d861e23437520", 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.0) [b2]
        { "84dd20f80db23cb49bfe98639be6cd71", 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (U) [b1]
        { "84f11cacb9608e2e595fa5fbb8c91ce9", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.1) [b1][T+Ita_Cattivik66]
        { "8524d92a742bc9d0d6f072f9f80e887c", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [f1] (PAL)
        { "852e8651d6c610ce593c35cea678c9f1", 4, 0 }, // Bomberman 64 - Arcade Edition (J) [f2] (PAL-CRC)
        { "8541d7a8737be09c52d4ec1274de2a91", 2, RUMBLE_PAK }, // V-Rally Edition 99 (J) [!]
        { "8555dcf12fd35206451fb415f3fd2eef", 2, MEMORY_PAK }, // Robotron 64 (U) [b2]
        { "8567382d3cd5bc0406b7b4c780f621dc", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move '99 (U) [!]
        { "8595ba0a0d2ea73944228bded4a969ad", 1, RUMBLE_PAK }, // Paper Mario (U) [T+Chi]
        { "85a90c61b65b56334e00950210c6cfc4", 4, 0 }, // Yoshi's Story (Ch) (V6) (iQue) (Manual) [!]
        { "85acebc851a407630d7464ccb63408be", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [b2]
        { "85b99f3d09c4070eea7669a09d20243f", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [t2] (Endless Ice)
        { "85d61f5525af708c9f1e84dce6dc10e9", 1, 0 }, // Super Mario 64 (J) [!]
        { "85f616e54160ffe14e4e817065ea61ce", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) (Analog Movement Controls_1.01 - No Invert)
        { "8603b180e70b2a72ef77d46c2bec2234", 1, RUMBLE_PAK }, // Star Wars - Shutsugeki! Rogue Chuutai (J) [!]
        { "8657cba95c409b74c1f5632cbc36643f", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (E) [!]
        { "866d401c51cc05a3188c9a2d4e7bfee5", 2, 0 }, // Pokemon Puzzle League (F) (VC) [!]
        { "868b37d1b66d1d994e2bad4e218bf129", 1, RUMBLE_PAK }, // Space Station Silicon Valley (U) [!]
        { "86a9550ad8cddaca13813765045a00ee", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Spa2.2_eduardo_a2j]
        { "86c6b996a55f90fc2410b16ae4ba5ee0", 4, 0 }, // GoldenEye 007 (U) (No Music Hack)
        { "86e98aaca0d90bf744da090539ba4ad8", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (E) [!]
        { "873dc63304f632a71b309988c39c48f7", 1, 0 }, // Glover (U) [b2]
        { "8743bdde07a2e9e0e5a0792d7d8678cb", 1, RUMBLE_PAK }, // Batman of the Future - Return of the Joker (E) (M3) [o1]
        { "874c7b7b365d2c20aaa1a0c90c93f9b8", 4, MEMORY_PAK | RUMBLE_PAK }, // Asteroids Hyper 64 (U) [!]
        { "875fa77726e7cd99bd410e3fc86c7777", 1, 0 }, // Dexanoid R1 by Protest Design (PD) [b1]
        { "87615822a0e00dd4fc7fdf636d4b8139", 1, 0 }, // Glover (U) [t1]
        { "876f87c91a4b6339daa8fc1f41eb7acd", 4, 0 }, // Excitebike 64 (Ch) (iQue) (Manual) [!]
        { "878d8a26fd02fdb08200464cb6f566ef", 4, MEMORY_PAK }, // Jikkyou G1 Stable (J) (V1.0) [!]
        { "879a085b07b7e6d52dfe2ce636d7a239", 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (U) [t1]
        { "87aa5740dff79291ee97832da1f86205", 1, 0 }, // Glover (U) [!]
        { "87bd533084acb4dc6c3e01b32f26357c", 2, 0 }, // Mace - The Dark Age (E) [o1]
        { "87bf3784709cd09693cd7bcc08460c63", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Mayhem (U) [!]
        { "87d7eebd0e6b403a2022bbd84bc12868", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o2]
        { "87f35bc0c6e2ead981b49d691095b118", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [b1]
        { "88072f30d4f9cf384b2b3a0300649218", 4, MEMORY_PAK | RUMBLE_PAK }, // CyberTiger (U) [!]
        { "88095c9bd22232383af82235f03b2658", 4, 0 }, // NBC-LFC Kings of Porn Vol 01 (PD)
        { "880bb39f8b31c087d66f7193d3528efe", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b3]
        { "881e98b47f32093c330a8b0dad6bb65d", 4, MEMORY_PAK }, // NBA Showtime - NBA on NBC (U) [!]
        { "88228e990b58a94e9b3460beff632304", 4, 0 }, // Mario Artist Talent Studio (J)
        { "882b30625b722b5c29d0d94e70d9302e", 4, 0 }, // N64 Seminar Demo - CPU by ZoRAXE (PD)
        { "8847bd6403ba49fd62899115d661b623", 4, 0 }, // Pom Part 1 Demo (PD)
        { "884ccca35cbeedb8ed288326f9662100", 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [!]
        { "885a29b9a52183b96e5d636f2b765231", 2, RUMBLE_PAK }, // Dance Dance Revolution - Disney Dancing Museum (J) [o2]
        { "88763c3fd50e036bbb9cf504f77c0741", 4, MEMORY_PAK | RUMBLE_PAK }, // 64 Oozumou (J) [b1]
        { "8881fbeae9b84485cf66876b60cd45df", 1, 0 }, // Glover (E) (M3) [f1] (NTSC)
        { "8897a39e34aee4d3f807af255c6617d6", 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (U) [!]
        { "889d4d337ad11ce94357511c725eab6a", 1, MEMORY_PAK }, // Blast Corps (E) (M2) [!]
        { "88b638825a16e46837fb521eda328c5a", 2, 0 }, // Mace - The Dark Age (U) [b5]
        { "88d89282b2346f98c7f30850b061d486", 2, MEMORY_PAK }, // Virtual Chess 64 (E) (M6) [o1]
        { "891e4a11b6f80e31a2f247fea3730744", 4, 0 }, // SRAM Manager V1.0 Beta (32Mbit) (PD)
        { "892222cc4baf9958405d20bc492175bf", 4, MEMORY_PAK | RUMBLE_PAK }, // Michael Owens WLS 2000 (E) [!]
        { "8976913efbe98efed3b0b32cbcaa8b49", 2, MEMORY_PAK | RUMBLE_PAK }, // Hiryuu no Ken Twin (J) [h1C]
        { "898362d1f6e207818e8cdf88225017b6", 4, 0 }, // NuFan Demo by Kid Stardust (PD) [b1]
        { "89bbbb739e54721f77f1a6441c00c093", 1, RUMBLE_PAK }, // Banjo to Kazooie no Daibouken (J) [f2]
        { "8a0479714259954bae41d5275ac926f4", 1, RUMBLE_PAK }, // Bomberman Hero (U) [t1]
        { "8a47091f443117bfa629b0b29d4450c6", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 98 (U) [o1]
        { "8a59477c29b4353feab85303c58b98f0", 2, MEMORY_PAK | RUMBLE_PAK }, // Rush 2 - Extreme Racing USA (E) (M6) [h1I]
        { "8a5f9b3308107c253ec5dca198f55c83", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (U) [t1]
        { "8a7b5a6ab0e815dbdffd1fc99939ce9f", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t2] (All Attacks Hurt P1)
        { "8a8e73d70cbc902cd99c7a4e2b982778", 1, RUMBLE_PAK }, // Bomberman Hero (E) [b3]
        { "8a964671c5a4f4fc62787f1f25edd70d", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (J) [!]
        { "8ad8d24ea38b68dfab154c2585fb550c", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (U) (Debug Version) [f2]
        { "8ada90fbd3313d5080007905d183cbd4", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [t4]
        { "8ae2e8f0c356fee638c8d908dcbb3381", 1, MEMORY_PAK }, // Mahjong 64 (J) [!]
        { "8af9d04dba1313d1f5f4ba51d9592fbb", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.0) [f2] (PAL)
        { "8b2e56f18421a67bca861427453a1e19", 4, RUMBLE_PAK }, // Banjo-Tooie (E) (M4) [!]
        { "8b346182730ceaffe5e2ccf6d223c5ef", 1, 0 }, // Pilotwings 64 (U) [!]
        { "8b7816e975db27b4bf591bfc73c43944", 4, 0 }, // N64 Seminar Demo - RSP by ZoRAXE (PD)
        { "8b7e6ff3453f660dcfc7b968e622e7c9", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [t3]
        { "8b95abb5f6e71454fe2c415a0787a384", 4, 0 }, // Doctor V64 BIOS V1.74
        { "8bc2712139fbf0c56c8ea835802c52dc", 4, RUMBLE_PAK }, // Mario Party (U) [!]
        { "8bec4edfa1446859da896e4ebae8f925", 0, 0 }, // 3DS Model Conversion by Snake (PD)
        { "8bf0eafb7014b7b5ce8e7d778b6b4b99", 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy's Reckin' Balls (U) [o1]
        { "8c0f46fef9a6034fcf0b7d6952ffec53", 4, RUMBLE_PAK }, // Top Gear Overdrive (J) [b1]
        { "8c1348872d9ac31ff9d22c07d7186ccb", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [f1]
        { "8c361e9283d740e2f266782d9c1ce914", 1, RUMBLE_PAK }, // Super Mario 64 - Shindou Edition (J) [b2]
        { "8c3e4fe5f6b2d37c66a71097e8b1c72a", 2, MEMORY_PAK | RUMBLE_PAK }, // 40 Winks (U) (Prototype)
        { "8c432235a57d34bc4a9b8b290e21e01e", 4, RUMBLE_PAK }, // Rugrats - Scavenger Hunt (U) [!]
        { "8c4a4cd472d610cda5459b3a92f21d30", 4, MEMORY_PAK | RUMBLE_PAK }, // CyberTiger (E) [!]
        { "8c5d52229da5a223ea2da093a5b9d31b", 1, MEMORY_PAK | RUMBLE_PAK }, // Spider-Man (U) [t1]
        { "8c7af37a3cb7306ebccac4029eb5c57d", 4, MEMORY_PAK | RUMBLE_PAK }, // HSV Adventure Racing (A) [f1] (NTSC)
        { "8c87830ff258893c453d70a90f62019f", 2, MEMORY_PAK | RUMBLE_PAK }, // Space Invaders (U) [t1]
        { "8c9f139bf5fc0b7a0497f28241c6f819", 4, 0 }, // Tetris Beta Demo by FusionMan (POM '98) (PD) [b1]
        { "8ca71e87de4ce5e9f6ec916202a623e9", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (U) (Debug Version)
        { "8cc73c373016070647030dde492fdc8c", 4, MEMORY_PAK }, // Power League 64 (J) [!]
        { "8cc7e31925fbfa13a584f529e8912207", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (E) (M3) [f1] (Save)
        { "8cd8fdf74a47ca163cd23ae1d01b7e95", 4, 0 }, // Doctor V64 BIOS V1.81
        { "8ce0a41cb29c14300b482e0e9897ebbf", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [h1C]
        { "8ce55da21d0fa7b41646a1f3f45f57a1", 4, 0 }, // NBCG's Kings of Porn Demo (PD)
        { "8cf63267ba38ab73763354c79c4adb74", 4, RUMBLE_PAK }, // RR64 - Ridge Racer 64 (U) [f1] (PAL)
        { "8cf8043bdfa89a2b0c4699d0e695e844", 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (U) [t1][f1] (PAL-NTSC)
        { "8d21dfcde6534127cbb444b7361b0d99", 1, RUMBLE_PAK }, // Body Harvest (U) [T+Spa0.98_jackic]
        { "8d296a614a47ac99b80d1ede669e62b3", 4, TRANSFER_PAK }, // Pokemon Stadium (S) [f1]
        { "8d3d9f294b6e174bc7b1d2fd1c727530", 4, 0 }, // 64DD IPL (JPN)
        { "8d4daa86bf542d8a55460f56c52f7d02", 4, 0 }, // Cube Demo (PD)
        { "8d8f3a8393f3f5489b3b144369565594", 4, RUMBLE_PAK }, // Ganbare Nippon! Olympics 2000 (J) [!]
        { "8dc201a0a9cfd1d2c85ee2b42d8ac49a", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (V1.0) [t1]
        { "8e18064a2c4b3ec15a20c3d676644b3a", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (J) [!]
        { "8e1968191dd27655c03be812cf041a95", 1, 0 }, // Pokemon Snap (I) [!]
        { "8e2b951678785db1f54a31f778bfa491", 2, 0 }, // Mace - The Dark Age (E) [o2]
        { "8e2f186fc1b86abc1cc04ffe1845c6ac", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o6]
        { "8e33ad20c31feb61d7230fad28846c5c", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [!]
        { "8e3d79fd6c089f88ed7a2cc1cdbdd535", 4, 0 }, // UltraSMS V1.0 by Jos Kwanten (PD) [f1] (V64)
        { "8e4d5c6d95efd1c94bb13644f7672909", 1, 0 }, // Dexanoid R1 by Protest Design (PD) [t1]
        { "8e55fd6990472a08568324309c2fd31f", 2, RUMBLE_PAK }, // Last Legion UX (J) [a1]
        { "8e62ec6fbe3cc9ff6284191c9c88e68f", 4, RUMBLE_PAK }, // Mario Party 3 (E) (M4) [!]
        { "8e632f2aff9aef3f5c1ae2febd482d9d", 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.0) [b2]
        { "8e79ec6d9f8db1c50100860bf4437462", 4, 0 }, // POMbaer Demo by Kid Stardust (POM '99) (PD)
        { "8e870036e7a40ba42803c333a412efe7", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b1]
        { "8eacc93671249508bf12cf6ec101056d", 2, 0 }, // Pong by Oman (PD)
        { "8eb1c2443d0b2e6eda52a4eea66d6c35", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis 64 (J) [!]
        { "8ec29958e0e3eb8c20ab7c9b3acb07d8", 4, RUMBLE_PAK }, // SmashRemix1.0.0
        { "8ee01de7da2e9ad08d7ed913a5ee8632", 1, MEMORY_PAK }, // Jangou Simulation Mahjong Do 64 (J) [!]
        { "8eebb16b7a4d39ae8bc8cccbc41f0a01", 4, MEMORY_PAK | RUMBLE_PAK }, // ECW Hardcore Revolution (U) [!]
        { "8eee06c88613b1265721b26517b64f2c", 4, 0 }, // Z64 BIOS V2.16b
        { "8eef2bb9543b78197cedda6da860da97", 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (U) [b4]
        { "8f1c1feaaf0d053e230361e3f81b0269", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (E) [h1C]
        { "8f2544f095ef7d9a5096e3d52472d699", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [b1]
        { "8f281800fba5ddcb1d2b377731fc0215", 4, 0 }, // Legend of Zelda, The - Majora's Mask - Preview Demo (U) [!]
        { "8f33fe36837198b3f21cc1f37fc67cc8", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (J) [b3]
        { "8f47f6a80734d6c8501aded549ff65c5", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [b5]
        { "8f6bed633be214cf039dbdac356231ce", 1, RUMBLE_PAK }, // Chopper Attack (E) [!]
        { "8f8f50ab00c4089ae32c6b9fefd69543", 4, 0 }, // Paper Mario (Ch) (iQue) [!]
        { "8f94c0a43d697d8deae9de660865a966", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [t3]
        { "8f96ff06d9b3c3219ceba5cdb7cf19ed", 4, MEMORY_PAK | RUMBLE_PAK }, // Telefoot Soccer 2000 (F) [b1]
        { "8f9810383b86e86ec447f65d47ca74a7", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (U) [T+Ita_cattivik66]
        { "8fa253fd69b73df9a831ef2f731491f2", 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (E) [!]
        { "8fa5e55e5597e2615f81cd236dca4721", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [b2]
        { "8faa825507e6cd3f388cc33343fae547", 4, TRANSFER_PAK }, // Pokemon Stadium (G) [f1]
        { "8fad1e4fa7baf1443b7f21ad1947b429", 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.0) [!]
        { "8fd3c06ed13cd27bb3d664c92a455aa8", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.0) [T+Ita_Cattivik66][b1]
        { "8ff8b861a0d095b910dd97eb1ecc02ff", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o1][T+Ita_cattivik66]
        { "90002501777e3237739f5ed9b0e349e2", 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 2 - Oudou Keishou (J) [!]
        { "9036bb44a51a62cb0a74c288ebb850c7", 4, BIO_SENSOR }, // Tetris 64 (J) [b2]
        { "903b912ce88626900221731224e9dbe8", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [!]
        { "903e6929666531d72d05d1e4c522e305", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [!]
        { "90448c4175ee9d0247674474dcabdfed", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (E) [!]
        { "9058970ddf26431a4daf353c2eea86ab", 4, MEMORY_PAK | RUMBLE_PAK }, // Bomberman 64 - The Second Attack! (U) [t1][f1] (PAL-NTSC)
        { "906d5a77188a0dc90ebf294ef8a3915e", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [b1]
        { "907403b12d2ad59a0f186a1632ae39b9", 4, TRANSFER_PAK }, // Pocket Monsters Stadium 2 (J) [f1]
        { "9081370141079031ebbdbca56fc8c7d8", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Choro Q 64 II - Hacha Mecha Grand Prix Race (J) [!]
        { "90ca5d19b7f085474ebd2186d70c59af", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.0) [o3]
        { "90faf5461e5f5cd5b67ce7891b9b055b", 4, 0 }, // Neon64 GS V1.2a (Gameshark Version) (PD)
        { "912628118e67fea670ed2c63f7a7b003", 4, 0 }, // Legend of Zelda, The - Ocarina of Time (Ch) (V4) (iQue) (Manual) [!]
        { "9137129a586e1bcab6ae81bac6b01275", 4, 0 }, // Xplorer64 BIOS V1.067 (G)
        { "91545c2642db6349b8d18bf0bee29162", 4, MEMORY_PAK | RUMBLE_PAK }, // ECW Hardcore Revolution (E) [b1]
        { "915e7762364c2449aaebc4ca5278d1e8", 2, RUMBLE_PAK }, // AeroFighters Assault (E) (M3) [f1] (NTSC)
        { "919a27c2abbea83ba996cee553227929", 0, 0 }, // SPLiT's Nacho64 by SPLiT (PD)
        { "919e5d60a7f9a79d89ac179123d47eee", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (G) [f1] (NTSC)
        { "91cf1982f309bd73822165087dad4371", 4, MEMORY_PAK | RUMBLE_PAK }, // Operation WinBack (E) (M5) [!]
        { "91d74621ddef6d37fb845b3bc7059a38", 2, MEMORY_PAK }, // Power Rangers - Lightspeed Rescue (U) [!]
        { "91fc4c63ca613530df22c8bb810cb2c3", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.1) (VC) [!]
        { "920faacce1f8a80022433acfd5cd2bc3", 4, MEMORY_PAK | RUMBLE_PAK }, // Quake II (U) [f1] (PAL)
        { "9213243118acce5664d1a35ccdccd87e", 4, 0 }, // Famista 64 (J) [b5]
        { "923d65e69f51858c697e0e5759cd038b", 2, MEMORY_PAK }, // Dual Heroes (U) [!]
        { "924e6553aede5ea66e2e14d29e48d929", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.0) [b2]
        { "926b1495565435da0af29bb660532da3", 4, 0 }, // Wave Race 64 (Ch) (V4) (iQue) (Manual) [!]
        { "928869d1ce001b813ba908dfe18d7f94", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 99 (U) [!]
        { "92a6ffa1c8d537c7a97c5c613cae05c6", 1, 0 }, // Ide Yousuke no Mahjong Juku (J) [!]
        { "92aed3d84a1aa0082b128e2cfd84c4aa", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Rus.01]
        { "92c842fc8cf706ff290044d40a64d346", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [T+Ita100]
        { "92d2bd868b17b3ca8a19116addb963b0", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [o1]
        { "92e6117285ec6a2d836b1c6839ddfd9b", 4, RUMBLE_PAK }, // Jet Force Gemini (U) [b1]
        { "9301923ae938320ff0186b7279afe6d8", 4, MEMORY_PAK }, // Hexen (U) [t1]
        { "930c7f6e5863471dde1816d28a10eb88", 4, MEMORY_PAK | RUMBLE_PAK }, // Bassmasters 2000 (U) [!]
        { "933c474fc970a0e7d659f32334db6475", 1, 0 }, // Super Mario 64 (U) [b1]
        { "93582a59f442502cadaa39bd6cc1b2ce", 1, 0 }, // Super Wario 64 V1.0 by Rulesless (Super Mario 64 Hack)
        { "9358aa90887fc31c45bc2b849c37d6a0", 4, 0 }, // Pip's World Game 2 by Mr. Pips (PD)
        { "935dd85ad198bbde92161cdce47cbfb3", 4, MEMORY_PAK }, // Eikou no Saint Andrews (J) [!]
        { "9362c5ac8ca78c12ff553cb7f12a7ff1", 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [f5]
        { "936db74ae766bb1219e8a712f0d06e4a", 4, MEMORY_PAK }, // Eikou no Saint Andrews (J) [b1]
        { "93b98d93275f803ef4c925bae5fb711f", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (U) [h1C]
        { "940ffb6c94f84bffd972a69ed86dd2fd", 4, MEMORY_PAK }, // FIFA 99 (E) (M8) [hI]
        { "941ba29d2945fa5c903fb0fc6491ac62", 4, 0 }, // Doctor V64 BIOS V1.53
        { "942a09966ec752a5ee81d042af11d4bb", 4, RUMBLE_PAK }, // Cruis'n World (E) [b2]
        { "943337c945da689af869877498c7aa2c", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [f2] (PAL)
        { "944eced50e57d46779576f732cb27dc0", 4, 0 }, // Dragon King by CrowTRobo (PD)
        { "9466618f4497da6c9091e05cc9666786", 4, RUMBLE_PAK }, // Chou Snobow Kids (J) [!]
        { "94751fe93510eba2d42d82269b224cf8", 4, MEMORY_PAK | RUMBLE_PAK }, // Asteroids Hyper 64 (U) [o1]
        { "94a7605a28861c43f636ae0ff7f25f2e", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx (U) [b1]
        { "94b284286a090b5980b7f18a4fa77853", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (J) [b1]
        { "94d81d14a2d768dd0eda3a845afb25cd", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [T+Bra_EmuBrazil]
        { "94e3f2ace3eb74150f036b217ff50370", 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [hI][f1] (PAL)
        { "950161325c7e11a057cc85c95685db05", 4, RUMBLE_PAK }, // Rugrats - Treasure Hunt (E) [h1C]
        { "950b88955933f42514a3f4284f5a285c", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.0) [o1]
        { "9516794650a56e60353135bb9a63c872", 4, 0 }, // Doctor V64 BIOS V1.51
        { "953b00882a99d650e1f2817674595f0b", 4, 0 }, // All Star Tennis '99 (U) [h1C]
        { "953bb3ad87b3c1b7ac85f12b135faa85", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [t1]
        { "955d19e26b4ba7cc941f86a54a0fc13d", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 2000 (U) [!]
        { "956aec111b74720d55496f81ada0fefd", 1, 0 }, // Harvest Moon 64 (U) [t1]
        { "956fcd46e850e9d10e623c6749d5524e", 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [t1]
        { "9581fc6ceac86dc8a2aee4053043e422", 2, MEMORY_PAK | RUMBLE_PAK }, // F1 Racing Championship (E) (M5) [!]
        { "959e64f082449bdbd0e5164c32491470", 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.0) [b2]
        { "95b784d6dd3470903baf6bbb533e088b", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [t3] (All Guns Zoom Mode)
        { "95c345ee264f3a91f1381e76cad82f11", 1, MEMORY_PAK }, // Saikyou Habu Shougi (J) [h3C]
        { "9602768ea88f4f5b53237e1aa20de68a", 4, 0 }, // LaC's Universal Bootemu V1.0 (PD)
        { "9656c4a5251f054704e35134006cffc7", 4, 0 }, // Liner V1.00 by Colin Phillipps of Memir (PD)
        { "965ad2fa317f0644e49a89a3219719cb", 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (U) [!]
        { "96c8da6d1567f43570ccb362811114de", 1, MEMORY_PAK }, // Castlevania (E) (M3) [t1]
        { "96ca36d474e82c270a129d775c63167a", 2, RUMBLE_PAK }, // Ganbare Goemon - Derodero Douchuu Obake Tenkomori (J) [!]
        { "96d5910a6ffd3ee53638251ace86e0cb", 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (U) [f1] (PAL-NTSC)
        { "96f8b3e6f6b91923462b02554025c403", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (U) [h1C]
        { "970488fb7d9c5c25bd924e6f898b84a0", 4, MEMORY_PAK | RUMBLE_PAK }, // International Track & Field Summer Games (E) (M3) [!]
        { "97275f6585547462a73910e9a80e4067", 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (J) [o1]
        { "9729a68f40f197d4d040fa641ff099e7", 4, 0 }, // Legend of Zelda, The - Ocarina of Time (Ch) (V2) (iQue) (Manual) [!]
        { "9729fc397e8d178ea974869e07df0502", 4, 0 }, // F-ZERO X (Ch) (V4) (iQue) (Manual) [!]
        { "972d0468ceb1221214aded7181da7853", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (E) (M5) [h1C]
        { "973de87cfc38353259f3b1f6678feb76", 1, MEMORY_PAK }, // Sim City 2000 (J) [o1][h1C]
        { "976825dab07e6f5d69bfd5b46337de85", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [o1]
        { "9768a9874d8329569f8fb27ec0ab723b", 2, RUMBLE_PAK }, // V-Rally Edition 99 (U) [f1] (PAL)
        { "9773150709bd804b8e57e35f1d6b0eed", 4, RUMBLE_PAK }, // Mario Party (E) (M3) [!]
        { "9789a48e1d9e42c2f69c59964371089f", 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack (J) (V1.1) [!]
        { "97c4cae584f427ec44266e9b98fbf7b6", 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (U) (M10) [b1]
        { "97e706ed9cc6f30708ffdc187c85d59f", 1, MEMORY_PAK }, // Human Grand Prix - New Generation (J) [!]
        { "97ea079ecf105ea16654a855644d870a", 1, 0 }, // Earthworm Jim 3D (E) (M6) [b1]
        { "97eab4dc83da0ad2890de2aaaa5d109a", 4, MEMORY_PAK | RUMBLE_PAK }, // Chou Kuukan Night Pro Yakyuu King 2 (J) [!]
        { "97fe61e8719cf0a4747301e97c759cbf", 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (U) [b1]
        { "9801aaa87d650c42b021f94d5c34c7a4", 4, 0 }, // DKONG Demo (PD)
        { "9803f144ab702a7c3e16092aa929ab45", 2, RUMBLE_PAK }, // Puyo Puyo Sun 64 (J) [T+Kor1.0_Zoinkity&UltimatePenguin]
        { "980dfb38ad9a883119de135f45b7db36", 1, 0 }, // Earthworm Jim 3D (U) [!]
        { "983f8a1d9a8d6aa1a6500af413ce31ff", 4, RUMBLE_PAK }, // SmashRemix0.9.5b
        { "987682cc63777221b938cc465607bd8c", 1, 0 }, // Pilotwings 64 (E) (M3) [b2]
        { "987a07daf0f06567bc8e794291d98695", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [h2C]
        { "988b490c2aebf7de3c274b7eaeef0999", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (E) [o1]
        { "988f5abd96259196343659e913666820", 1, MEMORY_PAK }, // Holy Magic Century (F)
        { "989e7b15d24649b784813e1a07df6887", 4, 0 }, // Doraemon - Nobita to 3tsu no Seireiseki (J) [b1]
        { "98a3122a6d7c9b02381fc5b0cfb8c14b", 4, RUMBLE_PAK }, // Super Smash Bros. (U) [f4] (GameShark)
        { "98a5836e3a5da7bd0b5819e7498acea2", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) (Kiosk Demo) [!]
        { "98a5dba896685d86a07ceea283fd36d3", 4, 0 }, // SRAM Manager V1.0 Beta (PD)
        { "98a802d44fe4169fd73d7b481c08613e", 2, MEMORY_PAK }, // Rakuga Kids (E) [h1C]
        { "98c0a62fe977a8202f558e5f2e2a5a60", 2, MEMORY_PAK }, // Namco Museum 64 (U) [o1][f1] (PAL)
        { "98c20a229170cf393bbf442d44ffc5b1", 4, 0 }, // Wet Dreams Can Beta Demo by Immortal (POM '99) (PD)
        { "990f97d56456fc23e52bd263e709e21e", 4, RUMBLE_PAK }, // RR64 - Ridge Racer 64 (U) [!]
        { "99138cb0d1325f7bb7c2edab5d7f6c54", 1, 0 }, // Super Wario 64 (Super Mario 64 Hack)
        { "991e5f527ec9330320e65233c293d4c5", 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (U) [f1] (PAL)
        { "992ba72f4a1e9c51934ff345cdd0d90c", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (E) (V1.1) [!]
        { "99380a8ed84c89e54de53e0b1fb423c9", 4, 0 }, // Liner V1.02 by Colin Phillipps of Memir (PD)
        { "993a67ecd02212b4be5b2e14652f835b", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [b2]
        { "993ccc0961cfd9f1342ad99a1ba2e282", 1, 0 }, // Sporting Clays by Charles Doty (PD) [a1]
        { "9946eff915fc947a226407ac1f7b35c4", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (J) [!]
        { "994d171a0154abe747338890af179252", 1, 0 }, // Super Mario 64 (E) (M3) [t2]
        { "99605763ad74d40d5b2d24cf3f0a3cd5", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [b3]
        { "9976833301116d0cb9e278e857921bf6", 4, MEMORY_PAK }, // Bomberman 64 (E) [t1]
        { "997fd8f79cd6f3cd1c1c1fd21e358717", 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (U) [!]
        { "99b35d7833daebce199cdcc240e29fdf", 4, MEMORY_PAK }, // New Tetris, The (U) [f2] (PAL)
        { "99bd941850aa68440b886ea05539f7dc", 1, 0 }, // Super Mario 64 (U) [T+SpaFinal_Mistergame][a1]
        { "99c1ab76126b7badfa9023d50dca1430", 4, RUMBLE_PAK }, // Super Smash Bros. (E) (M3) [f1]
        { "99d711ab6dfd36121e5c23c1dba78c01", 4, 0 }, // Pip's World Game 1 by Mr. Pips (PD) [b1]
        { "99dc3ff78356ce77cb240ac7782ee6e5", 4, RUMBLE_PAK }, // F-ZERO X (E) [b2]
        { "99f4652feff7373576ed440e1d33db24", 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [t1]
        { "99f95ad4a3b0c78b6f58a0fc3ad22db6", 4, 0 }, // Tommy Thunder (U) (Prototype)
        { "99fd0e1cba30c18c869f2de98678cd16", 1, 0 }, // Pilotwings 64 (E) (M3) [h1C]
        { "9a008af4e90d7c55f21039f4e82170ef", 4, 0 }, // Dynamix Intro (Hidden Song) by Widget and Immortal (PD)
        { "9a203506db213f97fe28210aaf56f820", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Pro 99 (E) [f1] (NTSC)
        { "9a27d420b43a908ecf4f28f859e1c541", 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [f3]
        { "9a2b0f3226fb8d129beb7509c169476a", 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (E) (M5) [h1C]
        { "9a3014ed85f796ccb836e85e78d2618a", 4, 0 }, // Cube Demo (PD) [b2]
        { "9a7006212947ee7648ee1d13162e55e0", 4, MEMORY_PAK }, // FIFA Soccer 64 (E) (M3) [!]
        { "9a70d30114f440b5240c10260ab2c8ed", 2, MEMORY_PAK | RUMBLE_PAK }, // Kakutou Denshou - F-Cup Maniax (J) [f1] (PAL)
        { "9a737609d5a7565589da44381c476fd3", 4, MEMORY_PAK | RUMBLE_PAK }, // 64 Oozumou (J) [b2]
        { "9a8465e302263d635557a14aa197fe3c", 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (E) (M5) [!]
        { "9ab9fb9782c7bb42a48c1465cd8f4ef6", 4, 0 }, // GoldenEye 007 (E) (Citadel Hack)
        { "9abb0f6f79e01fc9a942d6027719c3e4", 4, 0 }, // Tsumi to Batsu - Hoshi no Keishousha (Ch) (iQue) (Manual) [!]
        { "9ae941a12974b7b56cfa8b5ea1e670d7", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [b2]
        { "9af2919c27fcf6972986993c564947cd", 1, MEMORY_PAK | TRANSFER_PAK }, // Super Robot Taisen 64 (J) [f1] (PAL)
        { "9b241103ce0f13cad5fe522df67ed88b", 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 64 (J) [f2]
        { "9b2ffe72080b03a5f92eb87ea849cac4", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (U) (V1.1) [!]
        { "9b456acb96291fc8b55232a08ae03346", 1, MEMORY_PAK }, // Eltale Monsters (J) [!]
        { "9b463a87da73b8c923b05c3bd46edb09", 4, MEMORY_PAK | RUMBLE_PAK }, // Puzzle Bobble 64 (J) [f1] (PAL)
        { "9b4efc228c894e1ba80d7d11fda4e037", 4, 0 }, // U64 (Chrome) Demo by Horizon64 (PD) [a1][b1]
        { "9b5a92f32b9c807b2a9c01e42ef8f461", 4, 0 }, // GT Demo (PD) [a1]
        { "9b69d303affa2cd9aba96c21a2c21476", 4, 0 }, // Doctor V64 BIOS V1.05
        { "9b7f29aab911d6753f2011c48da752bf", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [!]
        { "9b8a7541d0234f4d97004ecbc216d9c2", 4, 0 }, // HIPTHRUST by MooglyGuy (PD)
        { "9b929e0bf8d63e62820f2fa6257cc5cf", 1, MEMORY_PAK | RUMBLE_PAK }, // Ganbare Goemon - Neo Momoyama Bakufu no Odori (J) [!]
        { "9bbc3945f526c8329710824c29ffc040", 2, TRANSFER_PAK }, // Pocket Monsters Stadium (J) [f2] (Boot)
        { "9bc0e3361385713bdf50f0eebf59ffe0", 1, 0 }, // Alleycat 64 by Dosin (POM '99) (PD)
        { "9be63892a2aa2b20b75b3ac4e7b7dcdf", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [b1]
        { "9be795563dba802dd566df75d6106cfc", 4, 0 }, // Z64 BIOS V2.18
        { "9bf4d7fbe8157c9d6866a90269dca7cb", 2, MEMORY_PAK | RUMBLE_PAK }, // Monaco Grand Prix (U) [f1] (PAL)
        { "9c0e454cffe65835aed12b39a8d9d6ec", 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [f2]
        { "9c1b750a2f9dfc53361f72f4bfb647a7", 4, 0 }, // Pipendo by Mr. Pips (PD)
        { "9c1e3d607b1cc5fb90ad62d3f02a50ed", 4, TRANSFER_PAK }, // Mario Artist: Paint Studio (J) [CART HACK]
        { "9c39391d103e6db39cec6c774edb082a", 4, MEMORY_PAK }, // Chou Kuukan Night Pro Yakyuu King (J) [h1C]
        { "9c595e5a5f027385a340addf609852dd", 4, MEMORY_PAK }, // NBA Hangtime (U) [b1]
        { "9c6cf9d3cb5852439de4ef4a399253b9", 4, 0 }, // Mini Racers (U) (Prototype)
        { "9c7d1b51ecbc9ac333d00273f96758b1", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T+Pol1.0]
        { "9c99c6d9ea98a960056c531cb78eb35b", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (U) [!]
        { "9cb5f5cd6ab141454d645c92fd9bf67c", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (E) [h1C]
        { "9cb963e8b71f18568f78ec1af120362e", 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (U) [!]
        { "9cc60d046372d2c9a529ed225e3cdb08", 2, 0 }, // Mortal Kombat Trilogy (E) [t3] (All Attacks Hurt P2)
        { "9cc940f5f463416f0b2c15f07a088f33", 4, TRANSFER_PAK }, // Pokemon Stadium (E) (V1.0) [f1]
        { "9cf16783687686c8fe2abe0b71316c3d", 2, 0 }, // Mace - The Dark Age (E) [b1]
        { "9cf33b5f0d677c7bce85cb877b0942cd", 4, 0 }, // Mempack Manager for Jr 0.9c by deas (PD)
        { "9d0d85e3a0c94b98d6404e8fc28e7b01", 1, 0 }, // Heiwa Pachinko World 64 (J) [b1]
        { "9d258df027ed369886a0249bfca936bd", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) [o1]
        { "9d401fa1a1dddf5c8995278e09e2210d", 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [t2][b1]
        { "9d4891bf26881c4541171b0235015fd4", 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater 3 (U)
        { "9d58996a8aa91263b5cd45c385f45fe4", 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (U) [!]
        { "9d5a1b779f8b43e63e8ce8427675a7ef", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (E) (M3) [h1C]
        { "9d7dad00eaac504d459608ab30cb0c1e", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [f1] (PAL)
        { "9da1e143ff580834fe485d08c21b6d01", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [b3]
        { "9df6c2c97fa34a978ef3cb77631536fe", 2, MEMORY_PAK | RUMBLE_PAK }, // Twisted Edge Extreme Snowboarding (U) [!]
        { "9e35b55606bdf4c77a1e817ce503a951", 4, 0 }, // Pip's RPGs Beta x (PD) [a1]
        { "9e4f62bd672e3601f1bbf9cdab791f9f", 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (U) (V1.0) [f1] (PAL)
        { "9e59882163ea99a45629b9b8b491d86b", 0, 0 }, // Absolute Crap #2 by Lem (PD) [b1]
        { "9e684fdcac7b5999cc863084deca463d", 1, MEMORY_PAK }, // F-1 Pole Position 64 (U) (M3) [h1C]
        { "9e6d80933b63b183ccbd8b5f6543dcad", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (E) (M5) [b1]
        { "9e943752bcf4fba9ca3028e596f4eb4a", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Mayhem (E) [!]
        { "9ea99ed5e27e35976e8266facf5064cd", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (E) [o1][h1C]
        { "9eaa60f295dba9a9687e3238daba14ee", 2, RUMBLE_PAK }, // Tom and Jerry in Fists of Furry (E) (M6) [f1] (NTSC)
        { "9eafca831487923e0be79717a678894e", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (U) [t1] (Never Drop Weapons)
        { "9eb9a0a45c2dd35dcb5a4a8a7fc4b151", 4, 0 }, // Nintro64 Demo by Lem (POM '98) (PD) [b1]
        { "9ec41abf2519fc386cadd0731f6e868c", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) [!]
        { "9ed82c5e9b6bf204276b434c8b0f6892", 4, 0 }, // NuFan Demo by Kid Stardust (PD)
        { "9ee432f07f1e4e9a13cd0090ccc5a94b", 1, 0 }, // Super Mario 64 (E) (M3) [b1]
        { "9f0492a34d7a2d7c4e9f29dc1848a04a", 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (E) (M3) (Eng-Spa-Ita) [!]
        { "9f04c8e68534b870f707c247fa4b50fc", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina (J) (V1.0) [!]
        { "9f101b6f6bef4f267deb5c6c37a24b97", 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (E) (M2) (Eng-Ger) [!]
        { "9f42209e413a1a92c788bf7bc26bfb7b", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [b1]
        { "9f556d184d945369ddd11b5f815814a8", 4, 0 }, // GameShark Pro V3.3 (Mar 2000) (Unl) [!]
        { "9f6db67262220c62536cebe0af3c8e73", 2, RUMBLE_PAK }, // Magical Tetris Challenge Featuring Mickey (J) [b1]
        { "9f7527ee8a80dae339db769eae707dcc", 2, 0 }, // Tetrisphere (U) [t1]
        { "9f797a9c704b5ebd04d6a2b036309af2", 4, 0 }, // Nihon Pro Golf Tour 64 (J)
        { "9f85f64c6dd30ca67eb355ef0332f102", 4, MEMORY_PAK }, // Mario Kart 64 (U) [t3] (Star Cheat)
        { "9faf514cb3dbb742c129deb395dca342", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (U) [!]
        { "9fbe3363da6c146ef2e29605bca834ff", 2, RUMBLE_PAK }, // F-1 World Grand Prix (E) (Prototype) [!]
        { "9ff2ba3c8408de9f0edb6d764a97c197", 2, 0 }, // War Gods (U) [!]
        { "9ff8aff664781384a5fc01449dc5ad8d", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Blades of Steel '99 (U) [f1] (PAL)
        { "9ffa642b02c27d8e01eab17b0f0f4251", 4, 0 }, // Neon64 V1.2a by Halley's Comet Software (PD)
        { "a0087a475a29f38bd4ce5df0a41a89cb", 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.1) [b1][f1]
        { "a00d0d095592113a279a27acd785ce50", 1, RUMBLE_PAK }, // Super Mario 64 - Shindou Edition (J) [T+Kor1.0_Minio]
        { "a02a4fb4b93e9847348440652cef8d4d", 1, MEMORY_PAK | RUMBLE_PAK }, // Harukanaru Augusta Masters 98 (J) [!]
        { "a04ebd753276c52e95e25af7683da32d", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina (J) (V1.0) [T+Chi.02_madcell]
        { "a059e27de410bda05d8603c22776979f", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (E) (M5) [b2]
        { "a05d4ff483af3f1c55bedc455e7edbbe", 4, 0 }, // NBC First Intro by CALi (PD)
        { "a05e7db2409deecca36e48e9d931cacb", 4, MEMORY_PAK }, // Jikkyou World Soccer - World Cup France '98 (J) (V1.0) [!]
        { "a0657bc99e169153fd46aeccfde748f3", 2, RUMBLE_PAK }, // Tsumi to Batsu - Hoshi no Keishousha (J) [!]
        { "a06d2e83cf2628915e8847f609474661", 2, 0 }, // Custom Robo (J) [!]
        { "a06e757cf1930b29fa4c0b5c9f31335f", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (E) (M2) (Ita-Spa) [!]
        { "a08676124b326b1035b202c83a97468f", 1, RUMBLE_PAK }, // Batman Beyond - Return of the Joker (U) [!]
        { "a09663b596f348d28af846a51375eb81", 1, 0 }, // Tigger's Honey Hunt (E) (M7) [!]
        { "a0b2e39fd3fe3802c369a6937416b38e", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b9]
        { "a0e6fb493b57c2faee8e517e0330c23e", 4, 0 }, // Sim City 64 (J) [CART HACK]
        { "a0ee7484500178bab44fde0af6d5748f", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (E) [h1C]
        { "a1025e61229d5d855d232210b39f5e66", 1, 0 }, // Bike Race '98 V1.0 by NAN (PD) [b1]
        { "a1082a6676455c040843fd75e92de1a3", 4, MEMORY_PAK | RUMBLE_PAK }, // RTL World League Soccer 2000 (G) [!]
        { "a12b361f9d38fdfe3183c2b4c1f2f217", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [o1]
        { "a133f08f88f6d49e1d23d051524ad094", 4, 0 }, // Doctor V64 BIOS V1.10
        { "a17aadcc962393d476edc321e59c504b", 4, TRANSFER_PAK }, // Pocket Monsters Stadium Kin Gin (J) [!]
        { "a18ca5dbc85668667aa467add6a62b39", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 98 (E) [!]
        { "a1c34717476027dfba72eea56f94d650", 1, 0 }, // Glover (U) [b1]
        { "a1cde773e4fb8b691098f2b3d5f2dbba", 4, 0 }, // Z64 BIOS V2.00
        { "a1d23d8c7a3194dcd0ac0ca1af79136e", 4, 0 }, // TheMuscularDemo by megahawks (PD)
        { "a1d47db4c6b93c78fe55cbfc1bb28d28", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [f2]
        { "a1f18de7d5570a502c7c93b7bf27e868", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Rus.82]
        { "a215bc1ce06960e0c469db9520e907c6", 4, 0 }, // Super Irishley Drunk Giant WaLuigi 64 (Super Mario 64 Hack)
        { "a2604cb559d289837f718db02afea85e", 4, 0 }, // Doctor V64 BIOS V1.74 (Revive Version)
        { "a267d8c94031678dd395fe4667c4a76a", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [b3]
        { "a29129b8c044d8a825f7f3780153fcc7", 4, MEMORY_PAK | RUMBLE_PAK }, // Snobow Kids (J) [h1C]
        { "a2957c46e62cec7b10ede4547f70425f", 4, RUMBLE_PAK }, // Lylat Wars (A) (M3) [t2] (No Damage-Unbreakable Wings)
        { "a29e203ddb6293b7d105bf4a2eeedd1e", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (G) [h1C]
        { "a2a4a0318dab366a595336b6f80ff3ab", 2, MEMORY_PAK | RUMBLE_PAK }, // Deadly Arts (U) [!]
        { "a2b81c0b7e3989152ca90901d05e6352", 1, MEMORY_PAK | RUMBLE_PAK }, // Ganbare Goemon - Neo Momoyama Bakufu no Odori (J) [h1C]
        { "a2e4be02876cb0f0d1e925ff95090c96", 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (E) (M5) [!]
        { "a2fa12a1defcb152dfd04e82968c7d3a", 4, 0 }, // All Star Tennis '99 (U) [f1] (PAL)
        { "a30cfa60ccce25f1324d4ad98c26af5d", 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.1) [o1]
        { "a3134dae771addb83191422615df8551", 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (U) [f1] (PAL)
        { "a31ad7e7e6177bed7345635fda563fca", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (E) (M3) [h1C]
        { "a33677b04870de18013b760b4f53f6bf", 1, MEMORY_PAK }, // Gex 64 - Enter the Gecko (U) [f1] (PAL)
        { "a34093ac2b038a92ccf525cab4ce346e", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (E) (GC) [f1] (NTSC)
        { "a3551578b7298a2e4719a9108e0aa37b", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [f1]
        { "a366bcbf8ff44d23a9450ecd8c71223c", 1, RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 - Shiokaze ni Notte (J) [b2]
        { "a3ba044dfc00bb766b4b2bfb9d4b5be9", 2, 0 }, // Pokemon Puzzle League (F) [!]
        { "a3d30fbdfd84b5e7298568bbd5ced7fd", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b2]
        { "a3d49aac6bc8eea0d707042a68d2d48c", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (U) [t1]
        { "a3f64d34614a0b01b6a49ae4031acbdf", 1, 0 }, // Ogre Battle 64 - Person of Lordly Caliber (J) (V1.1) [b1]
        { "a4039368e0472c68e3072c02c7a80f94", 4, MEMORY_PAK | RUMBLE_PAK }, // Mia Hamm Soccer 64 (U) (M2) [!]
        { "a4111a6cddbde0a45489106f0df0ca2b", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Pro 98 (E) [!]
        { "a42f6f14f7ea10abeb3b55ffd42eb572", 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (U) [!]
        { "a43a350385fa814ec9793b7acd9e18f4", 4, 0 }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) (Room121 Hack)
        { "a44b7a612964a6d6139d0426e569d9c9", 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (E) [!]
        { "a4599ef88f36e62c1692bcec416c5477", 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [t1]
        { "a45c39767d33ac21956a3d4e6c03cfa1", 1, 0 }, // Ogre Battle 64 - Person of Lordly Caliber (J) (V1.1) [!]
        { "a45d7115be5a06fd1567f9f913c3bdf8", 1, 0 }, // Pokemon Snap (S) [!]
        { "a45f7200537c0d928a88cbba2dfeb680", 3, 0 }, // Jeopardy! (U) [!]
        { "a475e9f8615513666a265c464708ae8f", 1, 0 }, // Legend of Zelda, The - Ocarina of Time (Ch) (Traditional) (iQue) [!]
        { "a485a6e9e30b7d55d23d8dd043770c64", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (E) (M3) [!]
        { "a4a1d490ba67831775fc381b846e2168", 1, RUMBLE_PAK }, // Doraemon 3 - Nobita no Machi SOS! (J) [!]
        { "a4a2b825797e2059b5df60d733461f34", 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Captain Buzz Lightyear auf Rettungsmission! (G) (V1.1) [!]
        { "a4b138caedce038d93c5b1fedd897e7d", 4, 0 }, // Z64 BIOS V2.15
        { "a4db62babe8583e5f5674ffcff8b8dfc", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [b3]
        { "a4f7c57c180297b2e7ba5a5feb44fe0b", 1, MEMORY_PAK }, // Doubutsu no Mori (J) [!]
        { "a50182b02e3f0d28be1ac341ddc92dca", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (U) [t3]
        { "a550debdf72b753b52882a1a573560b5", 4, 0 }, // Doctor V64 BIOS V2.03 (Purple)
        { "a55af433e65e3690e2dc2b16018e9a0f", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [b5]
        { "a56b15faf24389dd375e7afb03a7d3c1", 4, RUMBLE_PAK }, // 007 - Goldfinger
        { "a56bef4c7e6899a96f1f33424b434790", 4, 0 }, // U64 (Chrome) Demo by Horizon64 (older) (PD) [b1]
        { "a5921c00111200257284ce0aba0904ca", 4, MEMORY_PAK }, // Hexen (F) [!]
        { "a5a8a0249e14a052227b3ed71a98f0b4", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Rus099bfix]
        { "a5b6e5d7b689216ba5c8f6360bf89b67", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [b1][t1]
        { "a5c1f8af5d9aa7d2c771da1c84841017", 4, 0 }, // SRAM to DS1 Tool by WT_Riker (PD)
        { "a5db496c328c36682eb770cefe6c2064", 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ger) [f1] (NTSC)
        { "a5ee8a6c34863e3d0eb8c06ae8668b30", 1, RUMBLE_PAK }, // Batman of the Future - Return of the Joker (E) (M3) [!]
        { "a5f7c9f5b711eca7c2a2923eea6cf762", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [t1]
        { "a61854cf27e536c8513174faef08dfcb", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Pro 99 (E) [!]
        { "a62fa044bcb5507d358424abda6419db", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL 99 (U) [!]
        { "a63921b3190c45940721ba3ac64d1114", 0, 0 }, // SPLiT's Nacho64 by SPLiT (PD) [f1] (PAL)
        { "a63a9af85be8bb47c1741b8a37115354", 2, RUMBLE_PAK }, // Tom and Jerry in Fists of Furry (U) [!]
        { "a63f43cc53d9e57a045e1e20a5b12592", 2, MEMORY_PAK }, // Killer Instinct Gold (E) [o1]
        { "a66fee7af89d58b24a25c346a0e90172", 4, 0 }, // Wave Race 64 (Ch) (V2) (iQue) (Manual) [!]
        { "a6836524b4e71e5ad14dd8c3651665ad", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [t1]
        { "a68b65e6b4bc976ad9deab335de3bf70", 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (U) [t1]
        { "a698b3419600e7e283aa1313fb2522cf", 1, MEMORY_PAK }, // Sim City 2000 (J) [b2]
        { "a6a0109023837c4ed5cac62f4a66aba6", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [f2] (PAL)
        { "a6ac9d140112bd7a3aa8a5aba79ebd88", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (E) [b1]
        { "a6bd93ea576cdf8569f68171452f7e8a", 1, MEMORY_PAK }, // F-1 Pole Position 64 (E) (M3) [!]
        { "a6c51954ee5df373e159e0e55deae239", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [f3] (PAL)
        { "a6c5625ff127d9e741f595ebf3b3abb9", 1, MEMORY_PAK }, // Tonic Trouble (U) (V1.1) [b1]
        { "a6df7d8c1e2dab58496ef633ac09366e", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [t2]
        { "a6e03fa194a2932d36e0a67f9f64d92e", 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (U) [t1]
        { "a7087a96a709e4c662a459b4b4159094", 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.1) [b1]
        { "a722f8161ff489943191330bf8416496", 1, RUMBLE_PAK }, // Paper Mario (U) [!]
        { "a74429738648c96fab23944ccb06aa6c", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [b3]
        { "a74738d03aca4e51690408d203e86d49", 4, 0 }, // Twintris by Twinsen (POM '98) (PD)
        { "a74dbaa3bf59de13b6a191a6675c85d7", 4, 0 }, // Money Creates Taste Demo by Count0 (POM '99) (PD)
        { "a75637b1d22f59f2c32bda359a9ee79e", 1, 0 }, // Super Mario Magic Plant Adventure 64 (Super Mario 64 Hack)
        { "a76b619ec832a7e2abcfbdfeb5375e39", 4, 0 }, // Mario Artist Paint Studio (J) (1999-02-11 Proto)
        { "a7781d441af55c4ff8afc68ab3a59313", 2, RUMBLE_PAK }, // Indy Racing 2000 (U) [!]
        { "a78517540876ae94c2027fe260633ca8", 1, RUMBLE_PAK }, // Bakuretsu Muteki Bangai-O (J) [f1] (PAL)
        { "a7bd2f54d9646d43a2a4ad388450223b", 4, 0 }, // Doctor V64 BIOS V1.03
        { "a7c15e0c1e7d898c1b20276df7a62660", 4, RUMBLE_PAK }, // Charlie Blast's Territory (E) [o1]
        { "a7c82bb5098a7526d5417ee9a8981f48", 1, MEMORY_PAK | RUMBLE_PAK }, // Ganbare Goemon - Neo Momoyama Bakufu no Odori (J) [h1C][t1]
        { "a7cd65e5a4a8838d1cd452ba66e74df6", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (J) [!]
        { "a7d3e42effd6e7935da7c9a6b6899cac", 2, MEMORY_PAK }, // Dual Heroes (J) [o1]
        { "a81321759af38beb30a40fdaca2ea22a", 4, TRANSFER_PAK }, // Pokemon Stadium (I) [!]
        { "a81b1de864df3f4bb0903760be673f21", 2, RUMBLE_PAK }, // F-1 World Grand Prix (U) [!]
        { "a8266778591994efa3a3b9e6e806f8c8", 2, RUMBLE_PAK }, // Cruis'n Exotica (U) [t1]
        { "a8426180c65a4c9dd1df9fd033ddcbb6", 2, 0 }, // 77a by Count0 (POM '98) (PD) [b1]
        { "a8567ddabd3672fff18bc5df933cf8c7", 4, 0 }, // Glover 2 (Prototype)
        { "a87455244919bda6e44fa32c7e72bfba", 2, 0 }, // Tetrisphere (E) [b1]
        { "a88fcf3ff00f21d3cec44d2e0faaaad8", 1, 0 }, // Pokemon Snap (A) [f1] (GameShark)
        { "a89761018fa98e5c919f4ca0bb3e6001", 4, 0 }, // Doctor V64 BIOS V2.00b
        { "a8c78454c70bd73375aaf69c4078d5da", 2, RUMBLE_PAK }, // F-1 World Grand Prix (G) [!]
        { "a8cdd4c61604b9c279861289ed7c4542", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (U) [o2]
        { "a8d31d9715bb5645efc3245a6497f542", 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (U) [t1][f1] (PAL-NTSC)
        { "a8d5b5f1902afb1346d97a9d0d52b292", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) [b5]
        { "a8dfdff49144627492da9b0b65b91845", 1, MEMORY_PAK }, // Blast Corps (U) (V1.0) [!]
        { "a8e8ad9b1a05205085dcb4eadbe38020", 4, 0 }, // Spacer by Memir (POM '99) (PD) [t2]
        { "a90fe01fdaffb70dd123c0ba04589faa", 4, 0 }, // All Star Tennis '99 (U) [T+Bra1.0_Guto]
        { "a911f13e7d4cf157c79330e908bae9a7", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.21
        { "a92f9fcd93d20778ed8ff15546bb30f5", 4, 0 }, // Doctor V64 BIOS V1.01
        { "a94135d163e6091c960adc918c1fb8a7", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (E) (M3) [!]
        { "a952e495077d13c8d9587b7b8bbde2db", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [t3]
        { "a95442ce25eeb3bd31cef8525b9936fd", 4, 0 }, // View N64 Test Program (PD) [b1]
        { "a954e2bc5d774dc6e1cebce529f1cded", 4, 0 }, // Y2K Demo by WT_Riker (PD) [b1]
        { "a9683b99e82271dbf84ddb59806e9423", 2, RUMBLE_PAK }, // StarCraft 64 (Beta) [f3] (Country Code)
        { "a992e010bc3740c7c266d4f6e90226a1", 2, MEMORY_PAK }, // Wave Race 64 (E) (M2) [h1C]
        { "a99d3f4334f2f59b247fd5cb3dc85a77", 4, 0 }, // CD64 Memory Test (PD)
        { "a99e2cb6acef7a004961de5f6dfeeff0", 4, MEMORY_PAK }, // Wetrix (E) (M6) [!]
        { "a9ba7523591c975ac00309ed516fd248", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T+Ita2.0_Rulesless]
        { "a9be6a493a680642d840f859a65816ca", 1, RUMBLE_PAK }, // Paper Mario (E) (M4) [!]
        { "a9c272687dabd59c5144774b53bcc35a", 1, 0 }, // Pokemon Snap Station (U) [!]
        { "a9c393aa232b32798adf378f4318f99f", 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [!]
        { "a9dd498e6a28f55311ce4ef057e164b8", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (E) (M3) (V1.1) [!]
        { "aa0727f8f2b2c233f7bfd202194d41bc", 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant's NBA Courtside (U) [h1C]
        { "aa11e9e44b401794c37290896c9412ac", 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [f2] (NTSC)
        { "aa15fa62a776917e594a17b49bbc6980", 2, RUMBLE_PAK }, // F-1 World Grand Prix (G) [b1]
        { "aa57d69867f53456f351a289eba08c3d", 2, MEMORY_PAK | RUMBLE_PAK }, // Racing Simulation 2 (G) [!]
        { "aa93f4df16fceada399a749f5ad2f273", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (U) (Debug) (2000-03-22) [!]
        { "aa951294528ca2cf55f810f9fe433f8d", 2, MEMORY_PAK | RUMBLE_PAK }, // F1 Racing Championship (E) (M5) [f1] (NTSC)
        { "aaaa0551db73728d79ebc1d1edd127f6", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.23 (Even Bytes)
        { "aac7b04083fb1b9ac5b478ad89d8d3ba", 4, 0 }, // Doctor V64 BIOS V1.21
        { "aad3d42a9e800780087009b16afb1327", 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (E) [o1]
        { "aad6807d240fc3ff4fedab57135ad458", 4, 0 }, // Doctor V64 BIOS V1.82
        { "aada358ce97f06c4df5bf55987268844", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (U) [!]
        { "aada4cbd938e58a447b399a1d46f03e6", 4, RUMBLE_PAK }, // Cruis'n World (U) [!]
        { "aaeb5aca09291e8f5fedfa1f576d9f0a", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Pol1.2]
        { "ab06e6d777ae710f9b5a4dc4e7a295c8", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [t1]
        { "ab299b3943161b9c2abf03f5a9824e9f", 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.0) [b1]
        { "ab34fecd9a7a1030ec0892328f9cef8a", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [b3]
        { "ab36ed30033f4101d3f660951a568413", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [b7]
        { "ab4382e583ae139eedbafce5fa87e4c8", 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (E) [!]
        { "ab50026daea629f554408e5ba3efa182", 2, MEMORY_PAK }, // Robotron 64 (U) [b3]
        { "ab676c3e9d26a77450ddb4aacd1a3861", 1, MEMORY_PAK }, // Holy Magic Century (G) [!]
        { "ab68fb43f012c1a45af1dbcc8e8c109c", 2, MEMORY_PAK }, // Virtual Pool 64 (E) [!]
        { "ab72cb8971fa37176d3219b5f49af442", 2, 0 }, // Mortal Kombat Trilogy (E) [t1] (Hit Anywhere)
        { "ab7d5366fedcf8ec2d414b6001beb399", 4, RUMBLE_PAK }, // Chameleon Twist (E) [b1]
        { "abdb8d8c53e1f938853ec80742c8af77", 4, MEMORY_PAK }, // Mario Kart 64 (U) [b2]
        { "abfdaa35c995c839d0773d3601d59e67", 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (U) [b1][t1]
        { "ac004667ab4578baa9a1c0a7771e8b17", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (U) [b1]
        { "ac0751dbc23ab2ec0c3144203aca0003", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) (GC)
        { "ac129ab2688c02419fc190a87f2a2e93", 1, RUMBLE_PAK }, // Chopper Attack (E) [b1]
        { "ac1339207a3e442b61ec0e074673c93b", 1, RUMBLE_PAK }, // Wild Choppers (J) [b2]
        { "ac1c00167b95929f8ee40dfacb49bb57", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (E) [b1]
        { "ac52781c224e9eb0b8f408a782fac22a", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (U) [t1]
        { "ac5828ba750a4dcdc840050de66305e2", 2, MEMORY_PAK }, // AeroGauge (J) (V1.0) (Kiosk Demo) [b4]
        { "ac88a7d81ff0b712ea3a302419f3a927", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [b1]
        { "acbb37b24238f82b2333b710cbee388a", 1, MEMORY_PAK }, // F-1 Pole Position 64 (E) (M3) [b1]
        { "acc6fd4e26d360d1bed54c316d7e33b9", 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack - Covert Operations (U) [b1]
        { "acd0118ac4709db3943b3d35112c2001", 4, MEMORY_PAK | RUMBLE_PAK }, // TG Rally 2 (E) [!]
        { "ace58f325d5eecbe033b21f2c7bfe870", 4, RUMBLE_PAK }, // F-ZERO X (E) [h1C]
        { "acf18822457a451b09bd88e15a70505c", 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (U) (M2) [f1] (PAL)
        { "acf823a6ef4e77fe5b940d716dced6e9", 1, MEMORY_PAK }, // Holy Magic Century (G) [b1]
        { "ad0f2ec565d7575fb37512bc8df8a092", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 2002 (U) [!]
        { "ad1ad9fe15c4565ff88b9b48af6f300c", 1, MEMORY_PAK }, // Mahjong Master (J) [b1]
        { "ad2de210a3455ba5ec541f0c78d91444", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (E) (Debug) (2000-04-26) [!]
        { "ad3d099c84096dbacad7f1cf982631b2", 4, 0 }, // GoldenEye 007 Intro by SonCrap (PD)
        { "ad47a9521988cedd8d43097cfd9be042", 1, RUMBLE_PAK }, // Taz Express (E) (M6) [f2] (NTSC)
        { "ad4a781024e29f4a735cb4b87f2d59db", 4, RUMBLE_PAK }, // Cruis'n World (U) [o1]
        { "ad7c7b994994cf56481e4450e958049f", 4, MEMORY_PAK }, // Berney Must Die! by Nop_ (POM '99) (PD)
        { "ad921b1336eda406d124940bed174465", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.0) [o1]
        { "ad922dae446a301e1aafe1dfbad75a2e", 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (E) [!]
        { "adad4ff18e09d2f5b29aa6b5f159ef4e", 4, 0 }, // Famista 64 (J) [o1]
        { "adbe5ca10f659af2be712038e8522704", 4, MEMORY_PAK }, // NBA Jam 99 (U) [!]
        { "adc95ae01855fa305b13f8b22427e597", 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (E) (M3) [!]
        { "add115aad0ab7d33bfd243936d809178", 2, MEMORY_PAK | RUMBLE_PAK }, // Flying Dragon (E) [!]
        { "ade88a1544d469d77d4568364ef25319", 4, 0 }, // Doctor V64 BIOS V1.08
        { "adee5835a112677ccf74d5f4a1503294", 1, RUMBLE_PAK }, // Yoshi's Story (E) (M3) [t1] (Health and Eggs)
        { "ae0693c8d73e5a1407a2edf41f1164a9", 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (E) [b1]
        { "ae13e575386775e766f0e00ad2a40c9e", 1, 0 }, // Dexanoid R1 by Protest Design (PD) [f2] (PAL)
        { "ae36dd0bcc2f515917b0cf52aee48ac1", 4, 0 }, // Doctor V64 BIOS V2.01 (Red)
        { "ae3bdf729214964620417cc3163f0bb6", 2, MEMORY_PAK }, // Power Rangers - Lightspeed Rescue (E) [!]
        { "ae480013f39d4aec86eea1b4995600d1", 2, MEMORY_PAK }, // Wave Race 64 (U) (V1.0) [!]
        { "ae5107efdd3c210e1edd4acd9b3cac31", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) [!]
        { "ae705f25c57acabbd0e8bae5ec6e237f", 4, MEMORY_PAK | RUMBLE_PAK }, // Roadsters Trophy (U) (M3) [f1] (PAL)
        { "aec1fdb0f1caad86c9f457989a4ce482", 4, MEMORY_PAK | RUMBLE_PAK }, // Bomberman 64 - The Second Attack! (U) [!]
        { "aed29a731056f34b79e6b996bdbafb63", 1, 0 }, // Super Mario 64 (U) [o1]
        { "aee3fa928b2c1b73d841b47074d91c71", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f5] (SRAM)
        { "aee5016e6d60d12ad768e9f6d10adde8", 4, MEMORY_PAK }, // Let's Smash Tennis (J) [!]
        { "aee981977d8f069003574cd10a268d47", 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (J) [!]
        { "af149336b3ddb899598e7be8740d7dc6", 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (V1.0) [!]
        { "af1d07679014760b88923a4827658caf", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 99 (U) [!]
        { "af284ab7ff9363016782e99d00574e70", 4, 0 }, // The Corporation XMAS Demo '99 by TS_Garp (PD)
        { "af40ef12ce923ff1c26e76cc9d9b9ed9", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (G) [!]
        { "af5be0adff51a8e9c6d771282c295810", 4, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush 2049 (U) [!]
        { "af6521e33fde918b9a2298fa0bd3aa90", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2000 (E) [h1C]
        { "af701ee852ecd13f5f9f3284ebe60561", 4, 0 }, // Liner V1.00 by Colin Phillipps of Memir (PD) [b1]
        { "af7083fc0abcfd5a2c6a5e971453d831", 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (U) [!]
        { "af740b224e5dd0bd09f7254811559adf", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (E) [h1C]
        { "af83e0cf36298e62e9eb2eb8c89aa710", 4, 0 }, // Animal Crossing (Ch) (iQue) [!]
        { "af8a5a437f286fa230d99507109c79d9", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [t1] (Boost)
        { "af950a1b6c460d7fc3e78375d35047ef", 4, RUMBLE_PAK }, // Cruis'n World (E) [!]
        { "afbc694a3ba5ae83d3ceef906bf01839", 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [b1]
        { "afc3dc9bb737d81903f6ce4875b63ae9", 4, 0 }, // All Star Tennis '99 (U) [!]
        { "afc5eedd3fceef85458e9c8fa162b303", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (J) [b1]
        { "afe059ae1210751b6c849cfb482c99dd", 4, 0 }, // Kyojin no Doshin 1 (J) (Store Demo)
        { "afecc9a2df7b1a66a6b7ab3aa8b4bd2e", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Jam 2000 (U) [!]
        { "aff424a1883dc7bb92c7b2ebe9342f85", 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater (U) (V1.1) [!]
        { "b01d031be1789565e2b8a44cd76b96c6", 2, MEMORY_PAK }, // Dual Heroes (E) [b1]
        { "b025f0e20fb41a0aae280847329cb919", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mickey's Speedway USA (U) [t1]
        { "b03e6fafea6a314285d0181d5d2abbab", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.20
        { "b042c238c4093db533ec4e849c40de4f", 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (U) [t1]
        { "b04957d052ef850c5edece69db7377b3", 4, 0 }, // Nintama Rantarou 64 Game Gallery (J) [!]
        { "b04f298721223a22e1150cebc712ee6a", 1, RUMBLE_PAK }, // Resident Evil 2 (E) (M2) [!]
        { "b05dec10cdefacf4153e345940410477", 2, 0 }, // Mortal Kombat Trilogy (E) [o1]
        { "b05e10857a76ab70a83017adde8a8acc", 4, 0 }, // 2 Blokes & An Armchair - Nintendo 64 Remix Remix by Tesko (PD)
        { "b06625703db3a03bae3d02fd0f904541", 4, MEMORY_PAK }, // Hexen (U) [b1]
        { "b0679c685a1fb4d2ba96e4bb941262de", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) [b4]
        { "b07ff4f972b269f2bdc69713b4b701a7", 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [o1]
        { "b0bb6cab395c93c966f1ca485fc1bfbc", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Rus101b2]
        { "b0f9c3ec65c1c88a43c694ebf429e008", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G XG2 (U) [t1]
        { "b0fe1343a2ed21d2f083041b1fe1b7a9", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [h1C]
        { "b10d781ec625ca45713fd34e5096c24a", 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (J) [!]
        { "b11b30536d445ab6c7dc22c85c68a218", 4, 0 }, // Textlight Demo by Horizon64 (PD) [b1]
        { "b11f476d4bc8e039355241e871dc08cf", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.1) [!]
        { "b1271db50d6ef8f6b53cc640c3743e4f", 4, TRANSFER_PAK }, // Pokemon Stadium 2 (E) [!]
        { "b141c46c0fb2918d59933107ea8c1b7a", 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.0) [f1] (PAL)
        { "b15d755b8c6a4b5933802e5f227a183c", 4, 0 }, // Z64 BIOS V2.11NTSC
        { "b182bbbc8ce22bd3f44bb6ed91f6acd4", 4, RUMBLE_PAK }, // Lylat Wars (A) (M3) [f1]
        { "b18bc60ea6b9c72048fd84a2b8f182d4", 1, 0 }, // Manic Miner by RedboX (PD)
        { "b19805b3dfecf85fea7e04614362dc28", 4, MEMORY_PAK }, // Onegai Monsters (J) [b1]
        { "b1a67aebc2be89a800e5eb60c0dfa968", 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.0) [!]
        { "b1ab1d5b3b92b0aca380d0291da0689e", 1, RUMBLE_PAK }, // Bass Rush - ECOGEAR PowerWorm Championship (J) [b1]
        { "b206f917c1b4fd41cdadc4064c48b07b", 4, 0 }, // Congratulations Demo for SPLiT by Widget and Immortal (PD) [b1]
        { "b2242070800bf82e78ce33dc92a1db84", 4, 0 }, // Star Fox 64 (Ch) (V5) (iQue) [!]
        { "b22a6e184cb0657882cb4d6a81534254", 4, RUMBLE_PAK }, // Cruis'n World (E) [f1]
        { "b230d307428f4b69b3a80109ac905a44", 4, RUMBLE_PAK }, // WCW-nWo Revenge (U) [b1]
        { "b23bfc7dc874ddaa2005f9aff59a47ff", 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [f1] (NTSC100%)
        { "b26aafd452c9816e1b7aa0954e75825f", 4, TRANSFER_PAK }, // Pokemon Stadium 2 (S) [!]
        { "b27336a278658ac6d62ab6c4c3fb1db6", 1, 0 }, // Earthworm Jim 3D (U) [f1] (PAL)
        { "b278ddd3fb63f70b8b6efef445d81595", 4, 0 }, // Mortal Kombat SRAM Loader (PD)
        { "b27fa5e9ad0cb47bb3a74ffac7bc8edf", 1, RUMBLE_PAK }, // Body Harvest (E) (M3) [!]
        { "b29240bc2ffa6c46b83023f2226e3cde", 4, 0 }, // Z64 BIOS V2.17 [h1]
        { "b292b14bbc7ffcc2a741fe87a80b8d4d", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move '99 (U) [b1]
        { "b293e449967b246451564b25a4a36bec", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [T+Ita_cattivik66][b4]
        { "b29599651a13f681c9923d69354bf4a3", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [!]
        { "b2de25613755b234a376747422d83bbc", 1, RUMBLE_PAK }, // Wild Choppers (J) [t1]
        { "b2df29627e0219a9c14605f46803c94c", 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [b1]
        { "b31f8cca50f31acc9b999ed5b779d6ed", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.1) [!]
        { "b32e555bc1a375256e8a4021a25339be", 2, MEMORY_PAK }, // Wave Race 64 (J) [!]
        { "b34cbca3eee5c010ff3e504af2064069", 1, RUMBLE_PAK }, // Chopper Attack (U) [b4]
        { "b39592c6c9a3121258bdb62485956880", 4, 0 }, // Robotech - Crystal Dreams (Beta) [a1]
        { "b39d12910d12498605a9f793c29866be", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 98 (U) [b1]
        { "b3a172fe81f7bb47519d8e19e5f08154", 1, 0 }, // Mischief Makers (U) (V1.0) [o1]
        { "b3c1d4b9ec7dcd2922e681dbbc393915", 4, MEMORY_PAK | TRANSFER_PAK }, // Super B-Daman - Battle Phoenix 64 (J) [!]
        { "b3e1d1b92626ba86e5bdf0f18409a25b", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [T-Spa1.0_eduardo][b1]
        { "b406ad1694d65725fd31a054f6ece213", 2, MEMORY_PAK }, // Dual Heroes (E) [f1] (NTSC)
        { "b42c11b1dcbfc0ae42551021cd69ab22", 4, 0 }, // The Corporation 1st Intro by i_savant (PD)
        { "b42f62483f7ca2aac5af911175463db8", 1, MEMORY_PAK }, // Pro Mahjong Kiwame 64 (J) (V1.1) [!]
        { "b436f4717ac585b0d847756468fd6393", 1, RUMBLE_PAK }, // Command & Conquer (U) [!]
        { "b43b3c7a3d2149feedc8ba3b8198be5d", 2, TRANSFER_PAK }, // Pocket Monsters Stadium (J) [b1]
        { "b444cc20aefad0de4a552d8dd0486e82", 4, 0 }, // Doctor V64 BIOS V1.76
        { "b44e9c2d9d2f2de3af4793b824ccf936", 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (U) [!]
        { "b454490eb44f0978f009fa41de8c478e", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.1) [!]
        { "b457298b87b85bbf950f24867daa9475", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (U) [!]
        { "b4724120c269a1dc86991d34b1561f3d", 1, RUMBLE_PAK }, // Star Wars Episode I - Battle for Naboo (U) [b1]
        { "b478d4af60d43c38ba81de9faea6e057", 4, MEMORY_PAK | RUMBLE_PAK }, // Puzzle Bobble 64 (J) [!]
        { "b482f51b616f4337f1b3bf5efdd650d4", 1, MEMORY_PAK }, // Doom 64 (E) [b1]
        { "b48a8f8e2d9c7403210c2aa5262d94c7", 1, 0 }, // Super Mario 64 (U) [t2] (Speed)
        { "b49ec888940fab43651e48f760bf7bbd", 1, RUMBLE_PAK }, // Banjo to Kazooie no Daibouken (J) [f3]
        { "b4a2127033cb671eefe535b08f1afb38", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [b2]
        { "b4bd31b13e474df29922150e1ef3f328", 4, 0 }, // Super Mario 64 (Ch) (V6) (iQue) (Manual) [!]
        { "b4e3489f9b655439f9902f7a466cf973", 4, RUMBLE_PAK }, // Jet Force Gemini (U) (Kiosk Demo) [b1][f2]
        { "b4e658dff037706ffb2eb64f1dfdbb3f", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [f3]
        { "b524d10a8a26764eb2198acec3f09948", 4, 0 }, // Dr. Mario 64 (Ch) (V4) (iQue) (Manual) [!]
        { "b5691794a851d8b603f0c741d44aa244", 4, RUMBLE_PAK }, // Top Gear Overdrive (J) [!]
        { "b5772a2827befc81e107ae756b36bb2a", 4, 0 }, // Boot Emu by Jovis (PD)
        { "b5a5a71483679dccacbda770647a9dbf", 1, RUMBLE_PAK }, // Duck Dodgers Starring Daffy Duck (U) (M3) [t1]
        { "b5a98ced4b9a10682988ccb219a5c9a7", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.0) [b1]
        { "b5ac31d584bb2ef93c2a229e19b6d9a1", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [h1C]
        { "b5d68ff8e5a982400556c530409263db", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.1) [b1][f1] (PAL)
        { "b5d92ff099b3b74c6d0f0d75590eac36", 4, 0 }, // Doctor V64 BIOS V1.80
        { "b5fbf033d76fde89375d84b8918a5b61", 4, MEMORY_PAK }, // FIFA Soccer 64 (E) (M3) [o1]
        { "b5fdfdf26e1f27eaf0bd849caa4cc3b8", 1, 0 }, // Star Wars - Teikoku no Kage (J) [o1]
        { "b60d26c2c2242bff61f76469fc272d2a", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (Beta)
        { "b6154ecd50053ca96953176404b07824", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [f1] (PAL)
        { "b61f53695517d380eb7240331a2342c2", 4, 0 }, // Vector Demo by Destop (POM '99) (PD)
        { "b62076fa1421b8e7fdec2b4f8a910ea3", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (U) [!]
        { "b63346465fe70da3b1e7493ce5a15a31", 4, MEMORY_PAK }, // Mario Kart 64 (U) (Super W00ting Hack)
        { "b653c963ed8d3a749676810f07cfe4e5", 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 2000 (J) (V1.1) [!]
        { "b67748b64a2cc7efd2f3ad4504561e0e", 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [!]
        { "b6894826f5386019b19f0a775d11b042", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [h3C]
        { "b68f49aa8f6f7499184ac6b7b8570f2b", 4, MEMORY_PAK }, // Bomberman 64 (E) [!]
        { "b69b84083d1b4ec99a465b3067f74417", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (F) [b1]
        { "b6a7b85c123df4e0b9cc118578dbc9a2", 2, 0 }, // Mortal Kombat Trilogy (U) (Beta) (1996-05-13) [!]
        { "b6bc123e51ae5c3190bca41cfc1d637f", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b7]
        { "b6d6eda244c308e63d36eec607942605", 4, 0 }, // Z64 BIOS V2.13
        { "b6df5a75ced54dfee0e29bce9abd0aaf", 4, MEMORY_PAK }, // Penny Racers (E) [h1C]
        { "b6fd2a048d1f4f324cebc97ba09872bb", 4, 0 }, // Toon Panic (J) (Prototype)
        { "b708601a36054fca51d849d6d26cec5c", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [b4]
        { "b7240cada85a35b6c872a865f6a018a1", 4, 0 }, // Soncrap Intro by RedboX (PD)
        { "b74a059d227830c606ba79134278f55e", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (E) (M5) [b3]
        { "b74f70654b2fe3fc573d3bc913a62268", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [o3][f1]
        { "b75149f87cc5f3a508643ac377f2fcc9", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (E) [!]
        { "b75945407d7dea00a2a29ad24056a416", 2, RUMBLE_PAK }, // StarCraft 64 (E) [b1]
        { "b760d4c35303c74b893f1168415d743d", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (U) [t1]
        { "b775e4ff0205c405e3aa0a8df50c54de", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (G) [o1]
        { "b7a1d8b4bd19c710e4b9daac5590e301", 4, 0 }, // Neon64 First Public Beta Release V3 by Halley's Comet Software (PD)
        { "b7a220b59303d47f3beae233ca868cfd", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW vs. nWo - World Tour (U) (V1.1) [!]
        { "b7b236051e4347428288f8d840bfd2e4", 1, RUMBLE_PAK }, // Yoshi Story (J) [b1]
        { "b7db0d2cdcab307d744b6370f1bba9c1", 2, MEMORY_PAK }, // AeroGauge (U) [f1] (PAL)
        { "b7e41d34d209b6cfa92e3d622f911c4e", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL 99 (E) [!]
        { "b7f2eb7989c9c00096655d087d72ec51", 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (E) [!]
        { "b8047551f6e44e367862e9bc9c2a4ddf", 4, 0 }, // Clay Fighter - Sculptor's Cut (U) [h1C]
        { "b8085c2edb1c6d23e52ed8c06d92b4f8", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [!]
        { "b8267c225583a02acc3b74b3178b87e9", 2, RUMBLE_PAK }, // Puyo Puyo Sun 64 (J) [T+Eng1.1_Zoinkity]
        { "b85fcec60c7bea9106bd9b9c14479ee7", 4, 0 }, // Doctor V64 BIOS V1.61
        { "b867880c96c9e8d3130dde5526c95439", 4, 0 }, // Wildwaters (U) (Prototype)
        { "b86ec8d19d7d1a50ea900ea10355a735", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (G) [!]
        { "b882eda9cbf832beea2641e442a79edd", 4, 0 }, // Ultra Demo Bootcode by Locke^ (PD)
        { "b88c172ed14f4acb43c4a352d71cc640", 2, MEMORY_PAK | RUMBLE_PAK }, // 40 Winks (U) (Unl)
        { "b88f0ec125c54685fa516d233b564842", 1, 0 }, // Pokemon Snap Station (U) [f1]
        { "b8b760613caa702e33f271741170632a", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [t1]
        { "b8ba26f0c27253b80e5c7d96b0fb66ab", 2, MEMORY_PAK }, // AeroGauge (U) [T+Ita0.01_Cattivik66]
        { "b8bea2194d7d75a1711c024f5069d18e", 1, RUMBLE_PAK }, // Chopper Attack (U) [b2]
        { "b8d4b92e66a312708626b3216de07a3a", 4, MEMORY_PAK | RUMBLE_PAK }, // Snobow Kids (J) [!]
        { "b8dd77f17a8b638b1d38f57c81eaa89a", 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (U) [t2]
        { "b907c051f1d7d0d15de65ea87cf3f922", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [b1]
        { "b90ab8f7605d971cc7a6d9ba5e67d1af", 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (U) [!]
        { "b90b728e0643abc582793b451444733e", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T+Rus0.85_Alex]
        { "b932116c967795076b5c112841ab4427", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (G) [!]
        { "b935b87f3dcca8aeeb6a9365124846dc", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (E) [!]
        { "b96abfbd746bc25243d607613297d769", 2, 0 }, // Mace - The Dark Age (U) [b7]
        { "b978dd8ba5f4dac6850b7d1297eb1f11", 4, 0 }, // Doctor V64 BIOS V1.32
        { "b97c963235635672cbeab35154dfdfa2", 4, RUMBLE_PAK }, // Cruis'n World (U) [b1]
        { "b97fc8b1d00b9d673c229ca7fae453d4", 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.0) [T+Ita_Cattivik66]
        { "b9a06a029a842207a9e17aa40a702f7b", 4, 0 }, // Neon64 GS V1.2a (Pro Action Replay Version) (PD)
        { "b9a7befadfc4c46d61883b012fd2e69f", 4, 0 }, // Fogworld USA Demo by Horizon64 (PD)
        { "b9e6a38b28108e4d6e6a0a47bf981747", 4, MEMORY_PAK }, // Bomberman 64 (E) [b1]
        { "b9ef940408e1dfd63f867527e4212b9d", 4, 0 }, // Tom Demo (PD)
        { "ba145d51b681ef4432db59c1da703047", 4, 0 }, // Christmas Flame Demo by Halley's Comet Software (PD)
        { "ba2816f0b775099e42f124e5c43fe30c", 4, 0 }, // Doctor V64 BIOS V2.01b
        { "ba49514441023722f02d41c62612f6c3", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2000 (U) (V1.0) [!]
        { "ba538cbf43471b3522c4d08bad0797a7", 1, RUMBLE_PAK }, // Wild Choppers (J) [b1]
        { "ba863554222e503bac7f01fa4888be8a", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (U) [o1]
        { "ba976164c2a900c03f445acd51518fb0", 4, 0 }, // Pong V0.01 by Omsk (PD)
        { "ba99c445adc6994c97fe6463f3e0ec10", 1, RUMBLE_PAK }, // Densha de Go! 64 (J) [f1] (PAL)
        { "baaf237e71aa7526c9b2f01c08b68a53", 4, RUMBLE_PAK }, // Jet Force Gemini (E) (M4) [!]
        { "bac8634f10f286e6a0db889fcd12b25e", 4, MEMORY_PAK }, // Hyper Olympics in Nagano 64 (J) [b3]
        { "bad7c9731029ad4880ff4b92e5569065", 4, 0 }, // Mempack to N64 Uploader by Destop V1.0 (PD)
        { "badcedf5b6becdbcca7267bd2d791eb1", 1, 0 }, // Yuke Yuke!! Trouble Makers (J) [o1]
        { "bae518e0d40f846b17c510e4ea9e8c52", 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [f3] (NTSC)
        { "bb1623bd4751240138de55e18d3aca7e", 4, TRANSFER_PAK }, // Pokemon Stadium 2 (E) [T+Ita0.05]
        { "bb2472b3f8a41fbf3aec3ccef7ea8c78", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (E) (Beta)
        { "bb4c5769eed69362f1a90a2be19998d6", 4, MEMORY_PAK }, // Bomberman 64 (U) [b1]
        { "bb5c21bfea9aa7e9c90fe76c132bec49", 1, MEMORY_PAK | TRANSFER_PAK }, // Super Robot Taisen 64 (J) [b2]
        { "bb7f98e657fb4b5fcc7dc04bd72e2d2b", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G XG2 (E) (M5) [!]
        { "bb82adc51562f8cddcdf4f6fad2479ba", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (J) [b5]
        { "bb8a58282c5d4e32b34556407acd920e", 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (E) (M3) [b1]
        { "bb9f2e48015d9dfb347b4be4c2dfb334", 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (U) [a1][!]
        { "bbb48be198089a26050c84fe5b7b8bd5", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [!]
        { "bbcce848679f3d0722cba622505fb206", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Spa097b2]
        { "bbcde4966bee5602a80d9b8c1011dfa6", 1, MEMORY_PAK }, // Fushigi no Dungeon - Fuurai no Shiren 2 - Oni Shuurai! Shiren Jou! (J) [!]
        { "bbdc4c4f1c474298189312008a1768c4", 2, MEMORY_PAK | RUMBLE_PAK }, // Polaris SnoCross (U) [!]
        { "bbf5060e64bd0201954e65bd281ec486", 4, 0 }, // Pip's World Game 2 by Mr. Pips (PD) [b1]
        { "bc0fd2468ac7769a37c3c58cd5699585", 2, RUMBLE_PAK }, // F-1 World Grand Prix II (E) (M4) [!]
        { "bc2ecbe96b04edc56b08347df7c7fecd", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (E) (GC) [T+Ita70%_Rulesless]
        { "bc4bcd45fa712ecdd8a85918a9ca5f4b", 2, MEMORY_PAK | RUMBLE_PAK }, // Xeno Crisis (A)
        { "bc66239c12ae8696926e50c2b6ed9c49", 4, MEMORY_PAK }, // Rat Attack (E) (M6) [!]
        { "bc8ba67065fdbaa731defc866d25b8c2", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [b1]
        { "bcac0108f0d521ff5478ba6c50b23774", 4, 0 }, // Doctor V64 BIOS V1.71
        { "bcb210a6a3dca9a653f836e2957bd6dc", 1, 0 }, // Pokemon Snap (U) [f3] (GameShark)
        { "bcd2a45c39128b7d7e29e351788e860e", 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ita) [f1] (NTSC)
        { "bcee33dc37c872a188a72eeaf41369f4", 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Pro 99 (E) [o1]
        { "bd1de2fc1cf31096423563a40ecbf933", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (E) [!]
        { "bd22a79568534b42eaa3d8db9a7c4c91", 1, 0 }, // Starshot - Space Circus Fever (U) (M3) [b1]
        { "bd26bc90750d6388972c7e29441039df", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T-Rus0.50_Alex]
        { "bd46dd6275077d5a701482571f5dcdcc", 4, 0 }, // Fish Demo by NaN (PD)
        { "bd4e03620567c6291f11b9217ac67c28", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (U) [b4]
        { "bd5bb4ce1face911f9d8c777e2594998", 2, RUMBLE_PAK }, // Magical Tetris Challenge (U) [b1][hI]
        { "bd5d214bed53c4682e9ff8b8e9919cd4", 4, MEMORY_PAK | RUMBLE_PAK }, // Michael Owens WLS 2000 (E) [f1] (NTSC)
        { "bda717ecc8434f12f313342485828b58", 4, MEMORY_PAK | RUMBLE_PAK }, // Destruction Derby 64 (E) (M3) [!]
        { "bdd79f498f37d01b8958f56ec6ffa097", 4, RUMBLE_PAK }, // Mario Party 3 (U) [f1]
        { "be0000d3415787f9920e178942c297c5", 4, 0 }, // Doraemon - Nobita to 3tsu no Seireiseki (J) [b3]
        { "be22c6b95553b9cd5e0934b66bc36c84", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (U) [b1]
        { "be28ef71e1ee0092eec4aa395acfeeff", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [f1] (PAL)
        { "be2ab7cd9dbdda8ddae1ea288c663f15", 4, 0 }, // Cliffi's Little Intro by Cliffi (POM '99) (PD)
        { "be41ba94878999987dc8d2fd3a5551d9", 4, 0 }, // Clay Fighter 63 1-3 (Beta) [b1]
        { "be72be370bc0a76d403ff2b9ed2a9173", 4, MEMORY_PAK }, // NBA Jam 99 (E) [!]
        { "be734b15770d28c0a26d2db98df6cccc", 1, 0 }, // Yuke Yuke!! Trouble Makers (J) [b1]
        { "be804a0317f138775454405155c1d9a5", 4, MEMORY_PAK }, // VNES64 V0.1 by Jean-Luc Picard (POM '98) (PD)
        { "be823d7958151b97f812fbf01e7621d2", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [h1C]
        { "be83566e7c7b9249fac73dd2f1121424", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (U) [h1C]
        { "be9e0c9d509aab0c72f7788d6be3b443", 4, 0 }, // Pip's Pong by Mr. Pips (PD) [b1]
        { "beccfded43a2f159d03555027462a950", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.1) [!]
        { "bef0f36513f6fa8b01638319252b2b99", 4, MEMORY_PAK }, // J.League Eleven Beat 1997 (J) [b2][h1C]
        { "befa319919474bae05dfcb570cc0cf31", 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [t1]
        { "bf0622c4c479e8ff5983db3786f84293", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina (J) (V1.0) [f1]
        { "bf07de5816ffec94af30cb9b9b35909f", 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [f1]
        { "bf15a61aff71c93bf5e05243f57bca1d", 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (J) [!]
        { "bf167523e079c57dcca880a6f38889f2", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (U) (V1.0) [f1] (PAL)
        { "bf2df4b86faa2181a7acfe2643fa2293", 1, 0 }, // Earthworm Jim 3D (E) (M6) [!]
        { "bf33c371bf396a5f6e72627a34aedaf5", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [h1C]
        { "bf3e84cdd01cac05987fd8da5191534b", 1, RUMBLE_PAK }, // Bass Hunter 64 (E) [!]
        { "bf6780e2982c16d4a4fdb553be8f9226", 2, MEMORY_PAK | RUMBLE_PAK }, // Big Mountain 2000 (U) [!]
        { "bf710f7d1d51ec89ac3f58022b2b5d7f", 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [T+Bra1.0_Doom64BR]
        { "bf72e10eb79e00e095829112576f01fe", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (E) (M5) [b1]
        { "bf964ceca78a13a82055ebda80b95cca", 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.0) [!]
        { "c01a079e0005dcf5ad9ab0d58cd9515e", 1, MEMORY_PAK }, // Quest 64 (U) [t1]
        { "c02c32351bd69cb2d3fac355be074dc0", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [h1C]
        { "c049fcd4cb185a553d8122cfe6c30139", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (U) [o1]
        { "c0513ae9494462433cdc83870320da4e", 4, 0 }, // Mind Present Demo 0 by Widget and Immortal (POM '98) (PD) [b1]
        { "c0612aa12888c10e9565a57eb2c3605d", 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (E) [h1C]
        { "c0969e1c141be6e144e651cab1aea3d8", 4, MEMORY_PAK }, // International Superstar Soccer 64 (U) [b1]
        { "c0abe398cbc45ec90d03c5adc26c1065", 4, 0 }, // NBCG's Tag Gallery 01 by CALi (PD)
        { "c0b245e95a0ce736e1401a2183c3bbd1", 4, 0 }, // Unix SRAM-Upload Utility 1.0 by Madman (PD)
        { "c0b6deef900b63a5e4624804b14a1ba6", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [b5]
        { "c0cc62ac4b06416c9db5b765ade5f626", 1, RUMBLE_PAK }, // Command & Conquer (E) (M2) [b2]
        { "c0eb519122d63a944a122437ec1b98ee", 4, RUMBLE_PAK }, // Chameleon Twist (J) [!]
        { "c108a9b1eb024f0d2317c05976ba49f2", 4, 0 }, // Dezaemon 3D Expansion Disk (J) (Proto) [a1]
        { "c10b51eeac9f520419f23f95228c719a", 4, 0 }, // Nintendo On My Mind Demo by Kid Stardust (PD)
        { "c129ee8da7fe1b976d71932733385438", 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [f1] (PAL)
        { "c1384f3637d7a381b29341fed3ef3ceb", 4, RUMBLE_PAK }, // WCW-nWo Revenge (U) [!]
        { "c14090cd5cf313a471b5cd3d55ec0b53", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.0) [f1] (PAL)
        { "c14ae14c6de0dca126c423c4adbee315", 4, RUMBLE_PAK }, // F-ZERO X (J) [b1]
        { "c17f78a103d99b21533f0c1566378ef6", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (U) [!]
        { "c187bd285ba0e2d0ef5cffee44b367ee", 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [b3]
        { "c18f1c91f6f15971db18167eb72e6cef", 4, 0 }, // Friendship Demo by Renderman (PD)
        { "c19e5a3fcdf52c7292097f215ddc428a", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [b1]
        { "c1cdb77bea29a4b6f9a0829f8ba8a3f3", 4, RUMBLE_PAK }, // F-ZERO X (U) (DXP Track Pack Hack)
        { "c2166455e94e89e9e3ab612b4377c443", 4, 0 }, // Doraemon - Nobita to 3tsu no Seireiseki (J) [!]
        { "c246bd299aa756459193f406a261d1d1", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [t2]
        { "c252fec96171abd07a6c5b99b74924d4", 2, MEMORY_PAK }, // Virtual Chess 64 (U) (M3) [b1][o1]
        { "c2907eb2f9a350793317ece878a3b8e3", 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (U) [!]
        { "c2909d5799099819e9a7ece3cd4737fc", 4, 0 }, // Doraemon - Nobita to 3tsu no Seireiseki (J) [t1]
        { "c293d2adf01e0528554cdbadf0f86554", 1, 0 }, // Super Mario 64 (U) [t3]
        { "c2bedcb364162d6dac86548d7721f827", 1, 0 }, // Heiwa Pachinko World 64 (J) [b2]
        { "c2c3115821c738a4834e977e8325f89e", 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (U) [t1][f1][b1]
        { "c2d094778da53c7a84466130ec5c65b0", 4, MEMORY_PAK }, // NBA Hangtime (U) [f1] (PAL)
        { "c2dbae0c18c6666336f4cfa896d8ed18", 4, RUMBLE_PAK }, // Super Smash Bros. (U) [f2] (PAL)
        { "c3005d76af42e929e5c67421a19f8235", 4, MEMORY_PAK | RUMBLE_PAK }, // Violence Killer - Turok New Generation (J) [!]
        { "c306c80a07743f75b0c69af2b484e957", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (J) [b1]
        { "c31a8bb6705bd2112a5abf4f825fd3c0", 4, 0 }, // Quake 64 Intro (PD)
        { "c33cd926e1e71f39f7238af7b9e0dc5c", 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (Beta)
        { "c33fa02791077a71b0afe1cfed47c180", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (E) [!]
        { "c3565b1c983b83c6c965bb88e24135b2", 4, MEMORY_PAK }, // Mario Kart 64 (U) [b4]
        { "c36ac032e36c61723698344c33dc1f7e", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b7]
        { "c37e8afb4f3ecc86d01ce7388ca59347", 1, RUMBLE_PAK }, // Chopper Attack (U) [!]
        { "c38a7f6f6b61862ea383a75cdf888279", 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.1) [!]
        { "c38acbae773cc3845ea354421e171998", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (E) (Beta) (2000-07-10)
        { "c3a4ebeea4d22d8fc245b346d5c1a81e", 1, 0 }, // Super Mario 64 (U) [h1C]
        { "c3a55d506e5bd011ec20f601ea2b33bb", 4, MEMORY_PAK }, // Brunswick Circuit Pro Bowling (U) [o1][f1] (PAL)
        { "c3ed4cd05978decfd761fa2503dcb39e", 4, RUMBLE_PAK }, // Rugrats - Scavenger Hunt (U) [f1] (PAL)
        { "c4179723dc6e5d319c97f2f66ab05162", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [f1]
        { "c41f0d1edd2c8259eb27a8c97caa3d71", 4, 0 }, // GT Demo (PD)
        { "c429fe5c86f14764923f08d44884c823", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [b2]
        { "c43e3e28d84f337ff1bc88fa77996d33", 1, RUMBLE_PAK }, // Yoshi Story (J) [f3]
        { "c44d99d0437142211daca4826f8ecb88", 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (U) [t1]
        { "c4543f27b4e481d0818b51036b67dfef", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [t1]
        { "c46e087d966a35095df96799b0b4ffae", 2, TRANSFER_PAK }, // Pocket Monsters Stadium (J) [!]
        { "c47e95bb32ab132c41d67bd243f9e02a", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (U) [!]
        { "c48c46362602766ea758faaeed88af50", 4, RUMBLE_PAK }, // Donkey Kong 64 (E) (M4) [f1] (Boot&Save)
        { "c4a9bbf989ddfaf5f389e4adc6195dbc", 2, MEMORY_PAK }, // Robotron 64 (U) [!]
        { "c4c1b52f9c4469c6c747942891de3cfd", 1, 0 }, // Dinosaur Planet (Dec 2000 Beta)
        { "c4cbcb54b010a5a71fe5caa391e5c25f", 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [!]
        { "c4d652425aa4f2fc1d12564df01f8a04", 4, 0 }, // Paper Mario (Ch) (V2) (iQue) (Manual) [!]
        { "c4e47228706bc724d7fbd811231d20c9", 1, MEMORY_PAK }, // Saikyou Habu Shougi (J) [!]
        { "c4ecfec66eceea23f39632ab6753300f", 4, MEMORY_PAK }, // New Tetris, The (E) [!]
        { "c4f4ca4ffa94a870d0a3e18b74fe9fde", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (J) [b1]
        { "c4fd61c707977f1bf48dc15e31bd2fb1", 2, MEMORY_PAK }, // Virtual Pool 64 (E) [o1]
        { "c5594defbcfe16d56284c66fde49e540", 2, MEMORY_PAK }, // Virtual Chess 64 (E) (M6) [b2]
        { "c56ad8abd0fb5efef1fa0229f9a2eff0", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (U) [f1] (PAL)
        { "c57c4dd678748c6ed7cbf498445de247", 1, RUMBLE_PAK }, // Kyojin no Doshin: Kaihou Sensen Chibikko Chikko Daishuugou (J) [CART HACK]
        { "c5ebbdd41eaea8bd02cf520640ccccdf", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Pro 99 (E) [!]
        { "c605f40bf669e00a5e51baf0d00621ea", 1, RUMBLE_PAK }, // In-Fisherman Bass Hunter 64 (U) [!]
        { "c60f99dcf6b05faa2c33970462f93865", 4, 0 }, // GameBooster 64 V1.1 (NTSC) (Unl) [f1]
        { "c61fbf0be56fbcd8fd373edfb5d3e538", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (U) [b1]
        { "c644e318b33c4ebd94695c0c3e1e80ff", 2, RUMBLE_PAK }, // Defi au Tetris Magique (F) [!]
        { "c66d2037a654378c5cd365b135e36d87", 4, 0 }, // Z64 BIOS V1.05
        { "c6855df00bd5be8a41dce809093ee40b", 2, MEMORY_PAK }, // Virtual Pool 64 (U) [o1]
        { "c6867566c9e173beab459c5945b69c23", 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (U) [t1]
        { "c68cb260a7fd8d177640fa487dccacf6", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [b2]
        { "c69fa82e2ca2460000c629e9d46d4dc2", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [t1]
        { "c6b01c020fdfd2e5c037c5a330b161ad", 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant in NBA Courtside (E) [!]
        { "c6faac42045be1af769ec52cea6ded52", 4, 0 }, // Pip's RPGs Beta 15 by Mr. Pips (PD)
        { "c70578054aa00d7f95ae1d64e3aa9995", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f3][t2] (All Levels)
        { "c70af6197e67b310f316c34cced64c19", 4, 0 }, // Mario Kart 64 (Ch) (V2) (iQue) (Manual) [!]
        { "c70b0b680807f2b8c2c3d5dc495fa8c2", 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy-kun no Bura Bura Poyon (J) [!]
        { "c70b91430866300ce38b49098019ef9d", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [!]
        { "c714806b4cc144a3eabd234c07412109", 4, 0 }, // Bomberman 64 - Arcade Edition (J) [T+Eng1.3_Zoinkity]
        { "c7152167915e8eb928c0cbbf9af9886e", 4, MEMORY_PAK }, // Jikkyou J.League Perfect Striker (J) [f1] (PAL)
        { "c71750c4b84081f3331d957d3fe932ad", 4, RUMBLE_PAK }, // Chameleon Twist (E) [o1]
        { "c72417e0f8f043f9f11851633c4b1a57", 2, MEMORY_PAK | RUMBLE_PAK }, // Space Invaders (U) [!]
        { "c76828840668b6f3d4670cf4ad89c4aa", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (U) [t1]
        { "c77ac9bc65eeea761fc8b081af728cdb", 4, 0 }, // Mempack Manager for Jr 0.9 by deas (PD)
        { "c7921f3d4b91a42472dadd4dba2cdc54", 4, 0 }, // Pom Part 4 Demo (PD)
        { "c7a22dc5d95ebe1f8edb0d68c13027c1", 4, RUMBLE_PAK }, // GoldenEye 007 (U) [h1C]
        { "c7ac7460af3897bb38d6745bc4557c8e", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b5]
        { "c7b40352aad8d863d88d51672f9a0087", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.2) [!]
        { "c7b9024594f932ba5e3a1fb70fe9bd6b", 4, 0 }, // Rape Kombat Trilogy Beta1 (Mortal Kombat Hack)
        { "c7f1a43764a26da2e43f2a36a5f76e4c", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [!]
        { "c80d0a8193d530530ae875f8d9398e82", 1, RUMBLE_PAK }, // Yoshi's Story (E) (M3) [b3]
        { "c870967473a8bd2942d00eaa3d7ccc9e", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.0) [b2]
        { "c88637dcc7a00fed6297b61e79cf75a9", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [b1]
        { "c8c14d748679ae6f8ff1a62c4ad3b61e", 1, MEMORY_PAK }, // F-1 Pole Position 64 (U) (M3) [b2]
        { "c8d6a414505595c13c7da4d07a172343", 4, MEMORY_PAK }, // NBA Hangtime (U) [b3]
        { "c902bb7203c6c77dda16abcdf8995e32", 1, MEMORY_PAK }, // Bokujou Monogatari 2 (J) (V1.0) [b1]
        { "c9214988b08511d2f44fac2fad2ee67a", 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.0) [f1]
        { "c9319d97dabe9e842401433a27e00dac", 4, 0 }, // MAME 64 V1.0 (PD) [b1]
        { "c93a17d130b96fba27a0e959cab2a450", 2, MEMORY_PAK | RUMBLE_PAK }, // Monaco Grand Prix - Racing Simulation 2 (E) (M4) [!]
        { "c93d92f10a1a97d2ba87386be7d178fd", 2, MEMORY_PAK }, // Killer Instinct Gold (E) [!]
        { "c949e4929a001b339dae000a3da9e0a8", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [T+Por100%]
        { "c95353c14c4ae3dc95d1d91d6566ef92", 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (U) [f1] (Country Check)
        { "c980d2c07b98452110a3a8e52db5da88", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [b6]
        { "c9a6446918da5d65612c521d432b7da1", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (E) (V1.0) [b2]
        { "c9e9c4a18b1540c6b4111331d7c663b8", 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater (E) [!]
        { "c9f607002f051f087f4fd68b8b77b4d3", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (U) [o1]
        { "c9f95f9480b5867b03cfc0533f319253", 4, 0 }, // Wayne Gretzky's 3D Hockey '98 (U) [h1C]
        { "ca02c44e41d6a7cd51785db0981c54c7", 4, 0 }, // Neon64 V1.1 by Halley's Comet Software (PD)
        { "ca21467bde6b355e7a15b8f1ada7b24d", 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ger) [!]
        { "ca28a3645fc7ad969ebd75c5d6506e7a", 4, RUMBLE_PAK }, // Star Twins (J) [!]
        { "ca56f2de80839ec88750c15a21aa7c53", 4, MEMORY_PAK }, // FIFA Soccer 64 (U) (M3) [b1]
        { "ca60967822cccefe22299691b453d893", 1, RUMBLE_PAK }, // Mission Impossible (I) [f1] (NTSC)
        { "ca7161f298df38bc8182b21bb754db4b", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [h1C]
        { "ca825227e9e203e8429600189825ad74", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (U) (V1.0) [f2] (PAL)
        { "ca956015b6820dcff1c814f3532e18b1", 4, MEMORY_PAK | RUMBLE_PAK }, // Baku Bomberman 2 (J) [!]
        { "caa7caa9adfb4b8e4b2dbed88f963d07", 4, 0 }, // TopGun Demo by Horizon64 (PD)
        { "cad993128a11b5616e5dde5cf50cb998", 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [h1C]
        { "caf9a78db13ee00002ff63a3c0c5eabb", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [!]
        { "caff2dac142f169ee76965ca00518b26", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.0) [b1]
        { "cb174fa89f352dd8451a43fef2b8d1aa", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (U) [b1]
        { "cb2b7214be1084f2bfaee556b0b4fd2c", 2, MEMORY_PAK }, // Virtual Chess 64 (E) (M6) [o1][h1C]
        { "cb2c9c6104c3ef69a1cf979525f2f73d", 4, MEMORY_PAK }, // Frogger 2 (U) (Prototype 2) [!]
        { "cb2fb00c3921245ae04bb38ba01abe92", 4, 0 }, // F-ZERO X Expansion Kit (J)
        { "cb7c26c00ab065f372dda9950260bcba", 4, 0 }, // Namp64 - N64 MP3-Player by Obsidian (PD)
        { "cb97350d5cab48eab8b954d91c0e5dac", 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing - Round 2 (U) [t1]
        { "cba2cccd3c3772b96cda7128090c1fd7", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (J) [b2]
        { "cbbeaff5a9074d1a5507cf46cd683d36", 4, 0 }, // Clay Fighter 63 1-3 (Beta) [!]
        { "cbea298bfe9b59bbdbc379abfd2cd208", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.11
        { "cbef54768670f4b5602ccbc90150007a", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (G) [!]
        { "cbfead02c508dadfb44a00e7f7ed61b8", 2, 0 }, // Dark Rift (U) [t1]
        { "cc2433dfb7738d5cb71a75b054e52dd5", 4, 0 }, // Z64 BIOS V2.12PAL
        { "cc2be97a16744860fae8a94611479c4c", 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (E) (M2) (Fre-Ita) [!]
        { "cc6eae9ed582044cc27945684fd396cd", 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (Beta) (2000-05-09)
        { "cc75d0771dbc825ac9f499400443a6a0", 4, RUMBLE_PAK }, // Wheel of Fortune (U) [b1]
        { "cc7c58a032aaba19e72ccbfa6b3eeff6", 4, MEMORY_PAK }, // FIFA Soccer 64 (U) (M3) [!]
        { "cc8460b117aafbd484cf43c1eb3d8e15", 4, RUMBLE_PAK }, // Wheel of Fortune (U) [o1][b1]
        { "cc8b0a9f15b92d68963668a3495a6bf6", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (U) [f2] (PAL)
        { "cc8c56b4e37bc5a501087e7ce8c12cc5", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [b1]
        { "cc93c30c633ff461c29b54ceabefd701", 4, MEMORY_PAK | RUMBLE_PAK }, // Quake II (U) [!]
        { "cc9f8a2181c6c9c5ed49b77ff632395a", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [f1] (PAL)
        { "cca4e87ec206b5b65aeab9531c0f275b", 2, MEMORY_PAK | RUMBLE_PAK }, // King Hill 64 - Extreme Snowboarding (J) [!]
        { "cca6f7593302d46ada66c991c976da2d", 1, 0 }, // Pokemon Snap (U) [T+Spa]
        { "cca8d3874996a78c3b5828e7a66ac916", 1, 0 }, // Pocket Monsters Snap (J) [f2] (GameShark)
        { "ccd0ba9c220d1ee91b39747a6e5065ee", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t2] (All Attacks Hurt P1)
        { "ccd5a3b82976d65b3b42e8e2b2b43b48", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [b1]
        { "ccd869c8d45c3998addcbf43791d6c56", 4, 0 }, // Clay Fighter - Sculptor's Cut (U) [h2C]
        { "cceceaa389a7847de3433888c66a76e5", 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (U) [t1] (P1 Untouchable)
        { "ccee2fcf38dc2200128d75d15db53283", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [!]
        { "ccf012df82022d4797ce4cc5405e084f", 1, 0 }, // Mischief Makers (U) (V1.0) [b1]
        { "cd00c317003b03cd0bbeec4570d2b55d", 4, 0 }, // Clay Fighter - Sculptor's Cut (U) [t1]
        { "cd04abc5979f22ef5d6fdc4dcfaa4d50", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2001 (U) [f1] (PAL)
        { "cd0824e9405185af434837ba1c8c0cd5", 4, 0 }, // Mega Man 64 (U) (Beta) [!]
        { "cd09029edcfb7c097ac01986a0f83d3f", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (GC) [!]
        { "cd17388e175bcf0f621ee9313d5a1c8d", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (U) [f1] (PAL)
        { "cd19788ae04dad7af27fd0c591983cce", 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [b2]
        { "cd24763becfa1d0053e5438b0ef5e75e", 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (Beta) (2000-02-10)
        { "cd2c0dfdac5572988aab98d125eeeb53", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.2) [o1]
        { "cd40d9dd238e23ad79dc4484f77a7c4a", 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (J) [t1]
        { "cd4d5c6c96c8c528a0fce2958a4a7ac9", 2, 0 }, // Pong by Oman (PD) [t1]
        { "cd4f6c55a7567a25954908df613b236f", 4, MEMORY_PAK | RUMBLE_PAK }, // 64 Oozumou (J) [b3]
        { "cd61a7fdbd7297733b246204e8360d83", 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (U) (V1.1) [!]
        { "cd75ccd32eb435713ea54dff313fa50c", 4, RUMBLE_PAK }, // Mario Party 2 (U) [f2] (PAL)
        { "cd87ba2998d63c13b4366eb2c54e1eb6", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.0) [!]
        { "cdacc6fe2cc7851a1110a60ce76fbdfb", 1, 0 }, // Super Mario 64 (U) [T+Ita2.0final_beta2_Rulesless]
        { "cdcb7101471dd1129df63d6076834337", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [b6]
        { "ce71d1ce0a2b6d597f72cb4fc08f5844", 1, MEMORY_PAK }, // Castlevania (U) (V1.1) [!]
        { "ce72237707f481cfe97fde330c2afcd6", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (E) (Kiosk Demo) [!]
        { "ce86ce535a5b55ae4a0e6cc942701082", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [b2]
        { "ce915d72fea86aa99d9c015661a3d768", 4, 0 }, // NBCG Special Edition (PD)
        { "ce9ae0aa6dbbf965b1f72bc3aa6a7cef", 2, MEMORY_PAK | RUMBLE_PAK }, // King Hill 64 - Extreme Snowboarding (J) [b1]
        { "cec824b5da28b974b1562338eb728805", 4, MEMORY_PAK }, // Eikou no Saint Andrews (J) [h2C]
        { "cecab8df02c02f38c9cf1bdd57b1da00", 4, 0 }, // Tamiya Racing 64 (U) (Prototype)
        { "ced71061b6a8581dc0b95970d522848a", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) [f2]
        { "ceedab23299fa003f32975a4c1e407e3", 4, 0 }, // Ultra Demo Bootcode by Locke^ (PD) [h1C]
        { "cf0d228e8efdf823a227979bb352dd5b", 1, MEMORY_PAK }, // Mahjong Master (J) [!]
        { "cf17af84662e1457e61ad54575d50d20", 4, 0 }, // View N64 Test Program (PD)
        { "cf1e4e42b93ecbb080c05b73bb4af808", 4, 0 }, // CD64 BIOS Direct-Upgrade V1.13
        { "cf2276afd84e06ac780dba2e0ea4806d", 1, 0 }, // Dexanoid R1 by Protest Design (PD) [f1] (PAL)
        { "cf3802a99d8fe412359f1eb018fd991f", 1, RUMBLE_PAK }, // Yoshi Story (J) [t2] (Health and Eggs)
        { "cf55223f9e3bd118969a34950bedfd1f", 4, MEMORY_PAK }, // FIFA Soccer 64 (E) (M3) [o4]
        { "cf5b28578fd62fa1ff8690079f5d68f5", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (U) (V1.0) [!]
        { "cf5ff92eed5fd3c714bc3c4d15c47ae3", 2, RUMBLE_PAK }, // AeroFighters Assault (U) [b1]
        { "cf63ab6af5fbd5dbfe500dd9a658e069", 4, 0 }, // Attax64 by Pookae (POM '99) (PD)
        { "cf6bd6e20b40635b0e6ded5bb5114875", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (E) [f1] (Z64-Save)
        { "cf8b2c134c3fcd5a5d74402d75ca0543", 4, 0 }, // Doctor V64 BIOS V1.91
        { "cf97c336479ddbf1217e4dde89d9d2d3", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [!]
        { "cfa21e43dac50dff3122abf3af3511f8", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (J) [b1]
        { "cfbdc1c5e419ff162df02a0065d9bc1d", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (G) [b1]
        { "cfc196103e355b12a50605ab268992fa", 2, 0 }, // Mortal Kombat Trilogy (E) [t2] (All Attacks Hurt P1)
        { "cfcb20eacfceaa43fddbe05fb0d170d1", 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (U) [b3]
        { "cfd1b123bbabf1658ca6d52f34434a1e", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.0) [b3]
        { "cfe3e75b8b4c75f231cbbd9c99804ec3", 2, MEMORY_PAK | RUMBLE_PAK }, // Big Mountain 2000 (U) [t1]
        { "cff69b70a8ad674a0efe5558765855c9", 4, RUMBLE_PAK }, // GoldenEye 007 (E) [!]
        { "d038813541589f0b3f1f900f4fd22c9b", 1, RUMBLE_PAK }, // Lode Runner 3-D (U) [!]
        { "d0410d2c399cbf216bb756f35e5ac4ec", 1, RUMBLE_PAK }, // Banjo-Kazooie (E) [T+Spa1.1_PacoChan]
        { "d0453459095f69be36d675d8f743069b", 1, 0 }, // Pokemon Snap (I) [b1]
        { "d0472173524d6ec4e78cbca30efd98fc", 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [T+Eng1.1_Zoinkity]
        { "d05c6f3caf9059b306cc13535e2a8ba6", 2, MEMORY_PAK }, // Wave Race 64 (J) (V1.1) [!]
        { "d0698135c4a74cdea8374da0bd15dfdb", 2, TRANSFER_PAK }, // Pocket Monsters Stadium (J) [b2]
        { "d072ddbcc5961ae85e6fa9bf50241370", 4, RUMBLE_PAK }, // Mario Party (U) [f1] (PAL)
        { "d08d61e3c244a081c166c6a8d539a181", 2, 0 }, // War Gods (U) [o1]
        { "d0a050e86e27126fd7324b5674a30764", 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (U) [f1] (PAL)
        { "d0aa9d20a4b85fe514d2a3150d0133ea", 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (U) [!]
        { "d0ae6c07ac0481eba5ff9ce798a69574", 1, RUMBLE_PAK }, // Pikachu Genki Dechu (J) [b1]
        { "d0dd14f100b3825536db8f32c332457f", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [o1]
        { "d11bc38f26ea2835fbf017fe9bd404fe", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [b1]
        { "d1368c0e3869b85947a55c3da6ffca10", 4, RUMBLE_PAK }, // F-ZERO X (E) [f1]
        { "d13a38b9639cf1944fe7024e4c58739d", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2001 (U) [f1] (PAL-NTSC)
        { "d13d7e9aa29d972cb93c39a15f304faa", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [b3]
        { "d14a499bc4e324974eae3e42dec58625", 4, TRANSFER_PAK }, // Pokemon Stadium (S) [!]
        { "d1624977a3c25192671772623fa76438", 1, MEMORY_PAK }, // Pro Mahjong Kiwame 64 (J) (V1.0) [b1]
        { "d1855b19ba0d65c103fe9318f14f5d7d", 1, 0 }, // Mahjong Hourouki Classic (J) [b3]
        { "d1958141566ecc4942c71d1e97171639", 4, MEMORY_PAK }, // Mario Kart 64 (U) [T+Ita0.01_Cattivik66]
        { "d1b6d277f9fae2029a7532d8e1f4d6b2", 4, MEMORY_PAK | RUMBLE_PAK }, // Telefoot Soccer 2000 (F) [f1] (NTSC)
        { "d1ba3b1899576a4b67908abb6544d75a", 1, RUMBLE_PAK }, // Mission Impossible (S) [!]
        { "d1ce0fa3d46ca27f1c15e782acec49da", 2, 0 }, // Mace - The Dark Age (U) [b3]
        { "d1f6bc22557bf5607aa2e3c61be44d59", 1, 0 }, // Super Mario 64 (E) (M3) [o1]
        { "d20f2c6704d5b93372816ba0559e4e0f", 4, RUMBLE_PAK }, // Wheel of Fortune (U) [o1]
        { "d2442969e039254cf1b9b059dbb6311c", 1, RUMBLE_PAK }, // Mission Impossible (F) [b2]
        { "d24435c65c48e8de0831fc8f466baa51", 2, 0 }, // War Gods (U) (Power Bar Hack)
        { "d25dd15903bdcb7724a2e8a02561987f", 2, 0 }, // War Gods (E) [!]
        { "d266b288829193bd8205dddcdfa5cf30", 1, RUMBLE_PAK }, // Body Harvest (U) [b1][t1]
        { "d2860d4fbd0ec4b2711a6ef8d78f9866", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (F) [!]
        { "d2af6993e4d657d00520c2cce741480e", 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.1) [!]
        { "d2bd19a0873a6900aef07ecce103710c", 4, 0 }, // UltraMSX2 V1.0 w-Salamander by Jos Kwanten (PD)
        { "d2bd8dd8c3be1e8f0b8ae49206dbd7e5", 1, RUMBLE_PAK }, // Lode Runner 3-D (J) [!]
        { "d2cb04b88e565170d820e3cf78b54863", 1, 0 }, // Mahjong Hourouki Classic (J) [b2]
        { "d2da27872f13c0bbb2d686c495f13499", 4, 0 }, // Doctor V64 BIOS V1.72
        { "d2f7b3ace75a2ce7a06beac929711d94", 4, MEMORY_PAK }, // Hyper Olympics in Nagano 64 (J) [!]
        { "d30a4723e266d57d86bdc52f932d0df1", 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [t1]
        { "d313af5f8af4d19f732a1a2c4d4d66bb", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (B) [!]
        { "d31466b4b14b03387fe52fbbbcd25a5c", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [b1]
        { "d31a94a5685a21a932cc886d64cc9b21", 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (E) [!]
        { "d33e4254336383a17ff4728360562ada", 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [!]
        { "d341a40ea9fbe95b6f0f99dfc5901fb7", 4, 0 }, // All Star Tennis '99 (U) [f2] (PAL)
        { "d3644b398c090528e0ed9eb3c140366e", 4, MEMORY_PAK | RUMBLE_PAK }, // Roadsters Trophy (U) (M3) [!]
        { "d37ab17d3e27ed333704c44dcfaa28e7", 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) [b2]
        { "d37c79e4e4eabcb5dc6a07bd76688223", 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant's NBA Courtside (U) [!]
        { "d3929aadf7640f8c5b4ce8321ad4393a", 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (GC) [!]
        { "d3b28e1e154c5b58a40e3d71d8599ce2", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t4] (Hyper Mode)
        { "d3bdaa50d6c0efff9d696c521b704376", 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (U) [t1]
        { "d3ea654d4679e0bf40bb671ca7d4a7a3", 4, 0 }, // Doctor V64 BIOS V1.90
        { "d41ee755c8ddf4bbd5e368670c672488", 4, 0 }, // Clay Fighter 63 1-3 (U) [o1]
        { "d42cc78a2bf0d34518b8e0d0439e432e", 1, MEMORY_PAK }, // Eltale Monsters (J) [b2]
        { "d42e4496bea3c61e353643c8438cd9c7", 2, RUMBLE_PAK }, // Uchhannanchan no Hono no Challenger - Denryu IraIra Bou (J) [o1]
        { "d43c2e1938534363e56a22413b91d051", 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (G) [!]
        { "d43d1e41dba17fff92829fd2495b7619", 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [o1][f1]
        { "d441bb80efd86b8fd65ce855fec21631", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [f1]
        { "d4718e7ea75847646025d07626a1df69", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.26]
        { "d47ab8ca78d9686a7021ebbba00b0aa0", 4, 0 }, // Clay Fighter - Sculptor's Cut (U) [b2]
        { "d47ec4844c8c3946a3222fb22e103f04", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.09
        { "d4806cd9d9dfc7e52d6f825b5d5cc7c2", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [b5]
        { "d48cfe3fbf77d5335731509bfde10d47", 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina (J) (V1.0) [T+Chi]
        { "d4bc7e3227225e303a7f6491c1dbb4cb", 4, 0 }, // Doctor V64 BIOS V1.70
        { "d4cd4f6a5029048b44c8b420982e664e", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [b2]
        { "d4d046f74be40e067875701751647178", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (E) [o1]
        { "d51506edb0a941a00eb45850703b32cb", 4, 0 }, // Monopoly (U) [!]
        { "d5271db3cbe6b24099dcc6878c137ff7", 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ita) [b1]
        { "d52e871b562ad000ba3728d9b45ec9cb", 4, 0 }, // The Corporation XMAS Demo '99 by TS_Garp (PD) [b1]
        { "d54fb5a78a2a9189d01acf20a6043f4b", 4, 0 }, // Neon64 V1.2 by Halley's Comet Software (PD)
        { "d54fd7067bd774e32b57f9c2c0496899", 4, 0 }, // Bomberman 64 - Arcade Edition (J) [!]
        { "d5696cb5a4f2185bd8469df018b7856b", 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (E) [f1]
        { "d599490909faacb26d134d152cb5f1ed", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [o1][t1]
        { "d5ce3a477104213499ffea3a0bcb1555", 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (E) [h1C]
        { "d5edc7b884dfba7cf4ac4cdfa8ac64a4", 2, 0 }, // Pokemon Puzzle League (U) [t2]
        { "d5f73bb1fc46328440c3bd9812a49284", 0, 0 }, // Absolute Crap #2 by Lem (PD) [b2]
        { "d60046c23400bfebd5b051f89e7f2f07", 1, MEMORY_PAK }, // Pachinko 365 Nichi (J) [!]
        { "d616adf6441acbbd0e6bef023a8f6031", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [!]
        { "d61d97a7658c419a25a9bac96b0a3df8", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (U) [!]
        { "d62566c4a47396bfc97173d8cf468a5d", 4, 0 }, // Doctor V64 BIOS V1.40b
        { "d635824b5fc256da0f2e457663cff286", 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [b3]
        { "d63faf99e7876c39a9aca2659596342f", 4, 0 }, // MMR by Count0 (PD) [b1]
        { "d652769f8ee9bbe5f0bd5df9b955d47e", 4, 0 }, // Wet Dreams Madeiragames Demo by Immortal (POM '99) (PD)
        { "d653e6b050f63b5e1c1e9856319287e1", 2, MEMORY_PAK }, // Wave Race 64 (U) (V1.0) [t1]
        { "d65b99356296ca8f6b3273540203e0aa", 4, 0 }, // Z64 BIOS V2.12b4
        { "d6778d9d560dec35d8c05af4738e27aa", 4, 0 }, // Yoshi's Story (Ch) (V4) (iQue) (Manual) [!]
        { "d68fc4d8568b7d02efd47f32b9c091d7", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t6] (P1 Invincible)
        { "d6b0135be06df3545a8931957fe805fa", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [o2]
        { "d6cd1af1597cf0d279a39f75ee07c43a", 4, RUMBLE_PAK }, // WCW-nWo Revenge (U) [f1] (PAL)
        { "d6d30577e6e8341495b7d8066035c21d", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx - Global Assault (U) [f2] (PAL)
        { "d6e56b92ec7dbfb8b14dd5d8cff6492e", 1, MEMORY_PAK }, // Wonder Project J2 - Koruro no Mori no Jozet (J) [b1]
        { "d6e667fe10afe8f7116888efde98ae0e", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx - Global Assault (E) (M3) [!]
        { "d6eed7ba15e4cf88b4d235a016b1c880", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [t1]
        { "d6f58b98115e78d841089074401ae524", 2, 0 }, // Pokemon Puzzle League (U) (VC) [!]
        { "d714580dd74c2c033f5e1b6dc0aeac77", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.1) [!]
        { "d78e10c6b3e98f3b32fe0f23ed72db42", 1, MEMORY_PAK }, // Parlor! Pro 64 - Pachinko Jikki Simulation Game (J) [!]
        { "d7c983f603ee7ae8156c6eaeef7195c4", 2, MEMORY_PAK | RUMBLE_PAK }, // G.A.S.P!! Fighter's NEXTream (J) [o1]
        { "d7cb11a82edaee3f519346c7eae201e9", 4, 0 }, // ObjectVIEWER V1.1 by Kid Stardust (PD)
        { "d81ffd06311ab9334858ccc28efe992a", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [f2] (PAL)
        { "d83bde156d6fc400c28308a447b8fca0", 4, RUMBLE_PAK }, // GoldenEye 007 (E) [h1C]
        { "d85c29fd57fe481a34afbcdb9774395a", 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [t1]
        { "d861b752f59774a5597e66e8c58e52d9", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [b3]
        { "d8903586b5e479e6c4937879650d0d2d", 4, MEMORY_PAK | RUMBLE_PAK }, // Bassmasters 2000 (U) [b1]
        { "d8a88acfcd89df7a59d9a1b050fda740", 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.1) [!]
        { "d8ab932afd1ced97acd33fa984b309ed", 4, RUMBLE_PAK }, // SmashRemix1.1.0
        { "d8bd69aee8246a3127a58e202d447ba1", 1, RUMBLE_PAK }, // Body Harvest (U) [t1]
        { "d8d02e6f920e95047071e04420bb7d8f", 4, RUMBLE_PAK }, // SmashRemix0.9.7
        { "d8f6d887b94ba913ef60a0b03f781ae6", 4, MEMORY_PAK }, // 64 de Hakken!! Tamagotchi Minna de Tamagotchi World (J) [h1C]
        { "d9030ca30e4d1af805acce1bfed988cc", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (U) [!]
        { "d9160e48ce1f06b9b749b43a38367a28", 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [b2]
        { "d92eeba87794311c8f4443c29cc177c5", 1, MEMORY_PAK }, // Saikyou Habu Shougi (J) [h1C]
        { "d94a8f78178473d4ba4bed62fa8e2e66", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (E) (V1.0) [!]
        { "d9597922207298a87fa46878c017e6da", 4, 0 }, // Paper Mario (Ch) (V4) (iQue) (Manual) [!]
        { "d99b1a3f6d72defd76f3620959b94944", 4, MEMORY_PAK }, // Famista 64 (J) [!]
        { "d9b59e463a73fa880e6682a08d1c3071", 2, RUMBLE_PAK }, // F-1 World Grand Prix II (E) (M4) [f1] (NTSC)
        { "d9b5cd305d228424891ce38e71bc9213", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (E) (M5) [!]
        { "d9ba9fc609bfcdac0081b7c568311be1", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.0) [b3]
        { "d9ea905727b44f2774926cb03c2300a7", 4, 0 }, // Super Mario 64 Disk Version (J) (Spaceworld 1996 Demo)
        { "d9f8b84fd6fd21f0b1d750062ac86efc", 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.1) [!]
        { "d9fe93eba610e60f8f5d9880e52db056", 0, 0 }, // Absolute Crap #2 by Lem (PD)
        { "da027db6dcb643b0bfce71720a1f7500", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.23
        { "da23ee561578b7dad77ed72728b46d30", 4, 0 }, // Mario Artist Polygon Studio (J)
        { "da35577fe54579f6a266931cc75f512d", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (U) (GC) [!]
        { "da77c8ab76e6209550d56fed1817afbd", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [b2]
        { "da861c4d9202f661575466450a27c412", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (E) (M3) [!]
        { "da934ff1a6ce9eec70c235f879b23afe", 4, 0 }, // The Corporation 1st Intro by i_savant (PD) [b1]
        { "dac77d638ff03e77f569119116d4b668", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [h2C]
        { "dafe880cef56af0231eba2390259a2ac", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b8]
        { "dafee0c1fc99882a2c6a340bf0e58a08", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (E) [h1C]
        { "db02d36555e2473ea3036b8e0868254e", 4, 0 }, // GameBooster 64 V1.1 (PAL) (Unl) [b1]
        { "db24a5dba68f21d4513a37f7a7b0cf60", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [o3]
        { "db2a3682451f1a6ceafbf44792d4a5a7", 1, RUMBLE_PAK }, // Neon Genesis Evangelion (J) [b3]
        { "db2d737de0f117d419b3a48442b6db22", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [f2]
        { "db3a918fa61e1fb3110cfe6e9da33e97", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 2000 (U) [f1] (PAL)
        { "db502d4577ee908160441d38531ebb8d", 1, 0 }, // Super WaLuigi 64 (Super Mario 64 Hack)
        { "db644859c4dce5418010a2427e3904c0", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [o1]
        { "db67b8d7251314163e4f713724f55c66", 4, 0 }, // Z64 BIOS V2.10PAL
        { "db7a03b77d44db81b8a3fcdfc4b72d8c", 2, RUMBLE_PAK }, // Cruis'n Exotica (U) [!]
        { "dba166a42710f40dc78dc52eb37b0be6", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (E) [!]
        { "dba237ed8415383afcaba035d817272f", 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [t1]
        { "dbd2bc43fcdadacd234aefc8130a5413", 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (E) [b1]
        { "dbe3f0869f817dcd52a5cb9d43bb4fd1", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [b2]
        { "dbe79ae6531b491b8f8ee8b2b814d665", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 99 (U) (M5) [!]
        { "dbe9af0db46256e42b5c67902b696549", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask - Collector's Edition (E) (M4) (GC) [!]
        { "dbf04773ec2b8adf8a94db7e3e461138", 0, 0 }, // Mupen64Plus Demo by Marshallh (GPL)
        { "dbf7f4d881e1df604c41367b04233e74", 1, 0 }, // Super Mario 64 (J) [h1C]
        { "dc09e2685eb95471311e57b3605f4894", 2, 0 }, // Pokemon Puzzle League (U) [t1]
        { "dc15fcbeae0f1fef7bee141d77bb25a0", 4, MEMORY_PAK }, // NBA Hangtime (U) [!]
        { "dc1a78608b19dbe2ae3dc0cad9b79472", 4, MEMORY_PAK }, // Shin Nihon Pro Wrestling - Toukon Road - Brave Spirits (J) [b1]
        { "dc34f5aa26488352b4d025edbf47fe39", 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (U) [b1]
        { "dc577d70e9308f94e5f3ce03f3508539", 1, 0 }, // Ide Yousuke no Mahjong Juku (J) [b1]
        { "dc5aae1cadf43df43184d2935c32c26c", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx - Global Assault (U) [f1] (Country Check)
        { "dc5f1a814c8423b4b43f71c229d65a84", 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [!]
        { "dc8f45a200fed15c3b0bd97a2f52aca5", 4, TRANSFER_PAK }, // Pocket Monsters Stadium 2 (J) [f3]
        { "dcac12eb5832d4a489188330eb9ec387", 2, RUMBLE_PAK }, // V-Rally Edition 99 (E) (M3) [!]
        { "dcc316effc4928f5b0ae8d273d8024bf", 4, 0 }, // HiRes CFB Demo (PD)
        { "dcfb92a4b106fe61e54579b1d3b7336e", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (E) [f1] (NTSC)
        { "dd0a82fcc10397afb37f12bb7f94e67a", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.2) [!]
        { "dd21150cbc21c05420304599ec57411c", 1, RUMBLE_PAK }, // Resident Evil 2 (U) (V1.0) [!]
        { "dd291b9c65420fd892107f6c665b7a45", 4, 0 }, // Dr. Mario 64 (Ch) (iQue) [!]
        { "dd44e1d799fb9000983c5291d9d34306", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [b4]
        { "dd4a580e27d82bcca6a229ba17c69368", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis 64 (J) [f1] (Country Check)
        { "dd53e1f83e8789d23df6af942ffef236", 4, RUMBLE_PAK }, // Charlie Blast's Territory (E) [!]
        { "dd65e5f4b192a12966bbdd6718b694e6", 4, 0 }, // MeeTING Demo by Renderman (PD)
        { "dd8154d507c88694afd69c7af16a8cd6", 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (U) (V1.0) [!]
        { "dd824a8e1a6778fa5229433109afaeff", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [b2]
        { "dd8a0e5472f13ea87b176f0155fa0c66", 1, RUMBLE_PAK }, // Yoshi Story (J) [!]
        { "dd96bb80e652cd1cf23ca4b8294ba7b5", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2000 (U) (V1.1) [!]
        { "dda7aa9c5d1e617da183750f82b55780", 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (U) [t2]
        { "ddbe4248ea34e11c091a975776042670", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2000 (U) (V1.0) [f1] (PAL)
        { "ddd1958df0330b79288b51e65e1e36a0", 4, 0 }, // Doctor V64 BIOS V2.02
        { "de30f7c3d077778380e2b3d507efe2d3", 1, MEMORY_PAK }, // Castlevania (E) (M3) [b1]
        { "de4631d6419b7361de9c4a5472964d21", 4, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (E) [o1]
        { "de4ca7c5f9dd9216771451a5aff9f634", 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [f2] (PAL)
        { "de7384a7da97eeeaef80cbefbd17961a", 4, 0 }, // Freekworld BBS Intro by Rene (PD)
        { "de9498be76134bd066aa714ce2c71a16", 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (U) (V1.1) (M2) [!]
        { "dec4598a39728c28cd0ceba45a173ce1", 4, RUMBLE_PAK }, // Rugrats - Die grosse Schatzsuche (G) [!]
        { "deec4faec416f4e02d934c2e42c0caad", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [!]
        { "df00c698e2b18a6ca94c09ccd82caceb", 2, 0 }, // Mortal Kombat Trilogy (E) [t6] (P1 Invincible)
        { "df011e19f41b1b19c21f1e77e13780b7", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (E) [!]
        { "df2031338c9b0ab8317661afa64e5f9e", 1, 0 }, // Super Mario 64 (E) (M3) [o2]
        { "df2a6a2446e8e01375ade872a8f93366", 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [t1]
        { "df342b032ed1ed15c24448ec6be9f34e", 2, 0 }, // Mace - The Dark Age (U) [b1]
        { "df3cdd959e8c63b45f557fc197ce0e63", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (G) [!]
        { "df4446a2b55c4d8d67e9c0c19e0fd9fb", 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing - Round 2 (U) [!]
        { "df4fc3c46307527707f9106e75074829", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [o1][f1]
        { "df54f17fb84fb5b5bcf6aa9af65b0942", 1, RUMBLE_PAK }, // Mario Story (J) [!]
        { "df607e79b1539f7d6f9ad34c31f42296", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [o2]
        { "df6bc38c3d0c722dadd4eedb5dfce305", 0, 0 }, // 3DS Model Conversion by Snake (PD) [h1C]
        { "df8164da753c9ef0b7c2611f584e81a9", 4, 0 }, // Yoshi's Story (Ch) (V2) (iQue) (Manual) [!]
        { "dfb437bfb32864a17e903fede9ec3a27", 4, 0 }, // U64 (Chrome) Demo by Horizon64 (PD) [b1]
        { "dfb55c3f9b2d2a188616e9c123e0c816", 1, RUMBLE_PAK }, // Wild Choppers (J) [b3]
        { "dfebf3a9570b95d95ca6be2ce8311d1d", 2, MEMORY_PAK }, // Dual Heroes (E) [b2]
        { "dff0efe2b35fcde506d21b0c0bd373a5", 2, MEMORY_PAK | RUMBLE_PAK }, // Kakutou Denshou - F-Cup Maniax (J) [!]
        { "e0018c33346714b63a55c0e040f23dea", 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (Kiosk Demo) [!]
        { "e001b32d05d36edb7e8a99b7c7e4c789", 2, MEMORY_PAK }, // Namco Museum 64 (U) [t1]
        { "e0071b3ce2e29597dc4c73d83402723c", 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [h1C]
        { "e0256e4270887b99da7e91a620ffeff3", 2, MEMORY_PAK }, // AeroGauge (E) (M3) [h1C]
        { "e02aef75ac059fc2446bffdd06ee64a9", 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (U) [h1C]
        { "e034c969bcdc2e8dbe2c8a73bed57b53", 4, 0 }, // All Star Tennis '99 (E) (M5) [f3] (NTSC)
        { "e03b088b6ac9e0080440efed07c1e40f", 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (U) (V1.1) [!]
        { "e040de91a74b61e3201db0e2323f768a", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.0) [!]
        { "e048c2fe226634f039882c35a2a4eea9", 4, MEMORY_PAK }, // Golden Nugget 64 (U) [b3]
        { "e0530f182ea3bb780a6264b1d06737a1", 4, 0 }, // Z64 BIOS V2.20cf
        { "e062dd1e3c8b7a266fa018c6fbc17455", 4, 0 }, // Neon64 First Public Beta Release by Halley's Comet Software (PD)
        { "e06a7190b2df9cccca30c1724ddfe95b", 0, 0 }, // Absolute Crap Intro #1 by Kid Stardust (PD) [h1C]
        { "e06b804b5a8809c42e99b229106cc813", 0, 0 }, // Dynamix Readme by Widget and Immortal (PD)
        { "e0992a90191be4f1b2ba02258599334e", 2, RUMBLE_PAK }, // Magical Tetris Challenge (G) [!]
        { "e0bb65c30c1185fd9997020a1994b07e", 4, MEMORY_PAK }, // Rat Attack (E) (M6) [f1] (NTSC)
        { "e0bcb2758edf0ac6ab7db36d98e1e57c", 1, RUMBLE_PAK }, // Pikachu Genki Dechu (J) [!]
        { "e0c39e55de1d588371d5f99056f3578c", 2, MEMORY_PAK | RUMBLE_PAK }, // Hiryuu no Ken Twin (J) [h2C]
        { "e0ced30bf10f05ebaf1f1a27563e3e5d", 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.0) [f1]
        { "e0d1170d521b49e3a5edaca3a6f579ab", 1, MEMORY_PAK }, // Mahjong Master (J) [b2]
        { "e126b84fa242916289d04d68c0e20bfe", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (J) [b1]
        { "e1376f9b151ee3da3e8ed52d970480ef", 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (E) (M3) (V1.0) (Language Select Hack)
        { "e151cce259636c5ae15ca1c8b380ee76", 2, MEMORY_PAK }, // Robotron 64 (U) [t1]
        { "e16cfe9268e472a7814c58783ae7d3a2", 4, MEMORY_PAK }, // Bakushou Jinsei 64 - Mezase! Resort Ou (J) [h1C]
        { "e19f762182f4b4adf5d8f6d998033059", 1, RUMBLE_PAK }, // Command & Conquer (E) (M2) [f1] (Z64)
        { "e1a9cbdb3c066e4bc0a4fb71ada70cb2", 4, 0 }, // Mempack Manager for Jr 0.9b by deas (PD)
        { "e1bd016961edeb895f12676ad4b77fb5", 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (U) [b2]
        { "e226801c0b57fd2f2d2e303986db5e5c", 2, MEMORY_PAK | RUMBLE_PAK }, // City-Tour GP - Zennihon GT Senshuken (J) [b3]
        { "e2304df5ff6b40e74fd5bf62655d3e57", 4, MEMORY_PAK }, // New Tetris, The (U) [f3] (PAL)
        { "e24942948f7140ee4260268db763d0fd", 1, RUMBLE_PAK }, // Yakouchuu II - Satsujin Kouru (J) [!]
        { "e25f3a1136c9c81702e0827040c5a115", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Jam 2000 (U) [f1] (PAL)
        { "e26f4ffd2aa19e6ee9f9216a15189f14", 1, MEMORY_PAK }, // Holy Magic Century (G) [b2]
        { "e27c00c2114a56d18dd27b426193b79c", 4, 0 }, // Plasma Demo (PD)
        { "e28f8f19e56cc6c7a0f3a3286aeb60c1", 1, 0 }, // Pilotwings 64 (U) [h1C]
        { "e292551471766d7f51907f2bc65079aa", 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.0) [b1]
        { "e2b73feb0843874ea831a2e0076fcb72", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (F) [!]
        { "e2e79c7167bdb26e176d220904739c91", 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (F)
        { "e2f6ef5d4181558656dde8b0162d39d3", 1, RUMBLE_PAK }, // Yoshi's Story (E) (M3) [b2][f1]
        { "e2fb4f16a039a0e302d28aca94d5d928", 1, MEMORY_PAK | RUMBLE_PAK }, // O.D.T. (E) (M5) [!]
        { "e2fcf7782b622495fabe12fc7244b35b", 1, 0 }, // Super Mario 64 (U) [T+Spa3.9_Blade133bo]
        { "e3060d3fada2a502764e765f8140fc42", 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [t1]
        { "e32385ece93df2228fbefe3113220a20", 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (E) [f1]
        { "e3609fd12369c464e832c6d2a4d20790", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (I) [!]
        { "e3706865241d2bca4f397b4567cf2c2e", 4, 0 }, // Japan Pro Golf Tour 64 (J) [CART HACK]
        { "e38c4d24254bb3cb6c9ec75cd29a6566", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [b4]
        { "e38f1dae2a2023a529b47a26e4666759", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (U) [b1]
        { "e3b408997d7db91f8219f168c6d57d26", 4, RUMBLE_PAK }, // Monster Truck Madness 64 (E) (M5) [!]
        { "e3bfaf1ad4a58a3ab7fd7310c1f45d81", 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (E) [b1]
        { "e3ce4adb8a7c0cf39e11a2846e00178a", 4, MEMORY_PAK }, // J.League Eleven Beat 1997 (J) [b1][h1C]
        { "e3cfb5f1828caf1f00b23ada863460ad", 2, 0 }, // Pong by Oman (PD) [h1C][o1]
        { "e3d4f75c24da0521488e704812259a46", 1, 0 }, // Pokemon Snap (U) [f1] (Save)
        { "e3db1863e138b3ad5685a16029d0a44c", 4, MEMORY_PAK }, // World Cup 98 (E) (M8) [f1] (NTSC)
        { "e3ef8d7acca9ccb551c26b0f879e6b25", 2, MEMORY_PAK }, // Dual Heroes (U) [b1]
        { "e3f4c868917a12bd5e84d94d1c260c7d", 4, 0 }, // All Star Tennis '99 (E) (M5) [!]
        { "e3fdab4e3c417e623a02875d6a6e7467", 4, 0 }, // GBlator for NTSC Dr V64 (PD)
        { "e4076e07dbda9ceab5edcef4d4648eab", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (E) [f1] (NTSC)
        { "e419774a7113f1b82f6621ea512f10cf", 4, 0 }, // Zelda 64 Boot Emu V2 by Crazy Nation (PD) [a1]
        { "e423a21c971b8a91c3e8980e2605b840", 4, RUMBLE_PAK }, // Jet Force Gemini (U) (Kiosk Demo) [b1]
        { "e428181d573e25dcc0dc7f9f3bf4d1e1", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b1]
        { "e4353b43cb4302b7a308a42d6bb04435", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [f1] (Save)
        { "e4498d8fe11a6e0cc0737d89361de097", 1, RUMBLE_PAK }, // Batman Beyond - Return of the Joker (U) [o1]
        { "e458f74e694b2916eba9818103f7cf13", 1, 0 }, // Super Mario 64 (U) (Enable Hidden Scroller Hack)
        { "e48559e990de24458f8a3acdf3aee901", 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (E) (M5) [t1]
        { "e4d314c536f7bf78f60197ec238514ae", 4, 0 }, // T-Shirt Demo by Neptune and Steve (POM '98) (PD)
        { "e4edd73cb036c6b143cee9aeeed60341", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2001 (U) [!]
        { "e51a6ff9b2dbbd216158cacff35eb215", 1, 0 }, // Super Mario 64 (U) [T+SpaFinal_Mistergame]
        { "e5544b156833120e7b97a05aab743f98", 1, 0 }, // Super Mario 64 (U) [T+Ita4.2_Rulesless]
        { "e566d58641bedd25c450e443608eb46e", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [f1][o1]
        { "e56ef244d61b15dbaef474f9c0c0b23a", 4, RUMBLE_PAK }, // Donkey Kong 64 (U) [f3]
        { "e5a041b1b7c8e3b4c2e8178e5a138e2d", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.0) [b3]
        { "e5a0ca3dc54b38ea7fcd927e3cffad3b", 1, 0 }, // Pokemon Snap (A) [!]
        { "e5a39521fa954eb97b96ac2154a5fd7a", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (E) [!]
        { "e5b207e7c8dbd8baf9b42c96ef3c42e8", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) [b1]
        { "e5d2c7ce59282c0d716dd67ca787cf7b", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (U) [o1]
        { "e5d7b56b5edbd8575eceb9e5eccf4a00", 4, TRANSFER_PAK }, // Pocket Monsters Stadium 2 (J) [f4]
        { "e60ce3d4354066cfd80129cb6c2466d5", 4, 0 }, // Doctor V64 BIOS V1.40
        { "e61251d2819e3bf3a9c0b95329f60f70", 2, MEMORY_PAK }, // Namco Museum 64 (U) [!]
        { "e6129460e23170ffd4ec4b09d5e0cc56", 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 6 (J) (V1.0) [b1]
        { "e62751efde5312ed12dec6c9fdcbf062", 2, MEMORY_PAK }, // Virtual Chess 64 (U) (M3) [f1][o1]
        { "e627b898a7692c08b595a8d2178e34a0", 1, MEMORY_PAK }, // Bokujou Monogatari 2 (J) (V1.1) [!]
        { "e62f4fdcc82c244ba9709e40756d9b62", 1, RUMBLE_PAK }, // Lode Runner 3-D (E) (M5) [!]
        { "e66ed1cc4ab95d0872bb2ebc49b206c4", 1, RUMBLE_PAK }, // Space Station Silicon Valley (J) [!]
        { "e67651e58a4b864765a752e3beb5f4e4", 1, MEMORY_PAK }, // Mahjong 64 (J) [o1]
        { "e684c672c2bf64717a10f1d104108998", 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T-Rus0.36Alex]
        { "e6a5b9a18bb8d0508fbb703eb90703b7", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.30]
        { "e6b5c17b7bbbb7c432b3506c085d16c4", 2, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja 2 Starring Goemon (E) (M3) [!]
        { "e6ec9d86f94ceb2e311f9c23bbd3781f", 4, RUMBLE_PAK }, // F-ZERO X (J) [b2]
        { "e6f18336406ef3bfc990483bbf184acd", 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (E) (M3) [f1] (NTSC)
        { "e7008d17fd71d9c2bda1362c885388b2", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (U) [!]
        { "e722576a15182cfed6782379ce4bc8be", 2, 0 }, // Pokemon Puzzle League (U) [!]
        { "e730eda716ae40bdbc404f7601491d77", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.11 (Even Bytes)
        { "e7432c73fe47991b52eb4a44156a8d9d", 4, 0 }, // LCARS Demo by WT Riker (PD) [b1]
        { "e74404ed5a7076855cde3832474751b6", 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (U) (M10) [b1][f1] (PAL)
        { "e7652ed5ceceb5b1bc14495c58546d1c", 2, MEMORY_PAK }, // Dual Heroes (J) [!]
        { "e76ea30a71f9dc19c6502591fa7cb8e2", 4, 0 }, // 2 Blokes & An Armchair - Nintendo 64 Remix Remix by Tesko (PD) [f1]
        { "e76f0536cdad4c6f89802e15a98ae985", 4, 0 }, // Z64 BIOS V2.12NTSC
        { "e784353b07dafcb66690193b03cc3e1f", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.28]
        { "e790be1a5b883beba44bc0d2666c65f5", 2, MEMORY_PAK }, // Virtual Chess 64 (E) (M6) [!]
        { "e7916845b46c3482cf4843968370557e", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) [t1]
        { "e7bf80861a0ab2a788959463d953b5d5", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (E) [!]
        { "e7c2944774e1610ee6df7358717b5da3", 4, 0 }, // Doctor V64 BIOS V1.60
        { "e7c8d1090fa90e9b8c29b48734cf3a78", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.10
        { "e7d3a4a73d373da534a025e99b4d0eef", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [b1]
        { "e80ad38b3139cba5a367cb9b0b21aafa", 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [b5]
        { "e81062b36b08078ce4cff5a362f44764", 4, 0 }, // Z64 BIOS V1.12
        { "e811554e6c6a61be750a1ed315909840", 4, TRANSFER_PAK }, // Mario Artist: Paint Studio (J) [CART HACK] [T+Eng0.2_LuigiBlood]
        { "e821b540a91b7bf088e52abc02a5cc9a", 4, 0 }, // Pip's Pong by Mr. Pips (PD)
        { "e824b289a850e7e161af761d876f1cae", 1, RUMBLE_PAK }, // Super Mario 64 - Shindou Edition (J) [h1C]
        { "e83325ec6e8520b0cbdb49b52418fe4e", 1, 0 }, // Star Wars - Shadows of the Empire (E) [b2]
        { "e86a022815d0b7e65fd5557f0b3451d4", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [T+Ita_cattivik66][b3]
        { "e86a8bf46985b7ce8459afbe5fc3dcc3", 4, MEMORY_PAK }, // Hexen (E) [h1C]
        { "e887bee5b1de4e4d92ada183a245150e", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (E) (V1.0) [h1C]
        { "e8891f8f498a615a6cbaf75b7ddc9fa6", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (E) [!]
        { "e8955c3b743fddfe403e52e769e9853f", 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (E) [!]
        { "e8997bf5662540b184fbf8277d260984", 4, 0 }, // Wetrix (J) [!]
        { "e8b403a0f0e212fa5c1220af10d9c379", 1, MEMORY_PAK }, // Donald Duck - Quack Attack (E) (M5) [!]
        { "e8b666a429fedb2a1a1228cd450cd4fc", 4, MEMORY_PAK | RUMBLE_PAK }, // Stunt Racer 64 (U) [!]
        { "e8cbaadedb6d19acc142be3105e94c4f", 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (U) [b1]
        { "e8cddf0c52b72453d52da385322dfe15", 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (V1.1) [!]
        { "e8d70e021c40078f8a82318ae8165a73", 4, 0 }, // Spacer by Memir (POM '99) (PD) [t2][b1]
        { "e8e6ec0692009009f5dca6827b21f59a", 1, 0 }, // Pilotwings 64 (J) [!]
        { "e8eb810d996e12cd6c47445f87a94c72", 4, 0 }, // Kyojin no Doshin 1 (J)
        { "e8ee7d44858c1ab90c1f48a649dc98b6", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.1) [b1]
        { "e9028f9ccc307806695dd81742d05d5d", 1, 0 }, // Pokemon Snap (F) [!]
        { "e92fc6ae193092f6b597e05946b558eb", 2, RUMBLE_PAK }, // Super Robot Spirits (J) [b1]
        { "e942a3eeb1eb572badd6f705eb12a22c", 1, 0 }, // Mahjong Hourouki Classic (J) [!]
        { "e946855eef7f1bbd47aeeca33a2737bb", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [o1]
        { "e94a9142b1d981a6556747ba147e1a19", 2, RUMBLE_PAK }, // F-1 World Grand Prix II (E) (M4) [h1C]
        { "e95d73ff55fbb63e79aa9eab14608584", 2, MEMORY_PAK | RUMBLE_PAK }, // Knockout Kings 2000 (E) [!]
        { "e96fecba52905db14addad7cfd61091f", 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (E) [!]
        { "e970af3de25bb5ae1154010e26af776f", 2, MEMORY_PAK }, // AeroGauge (J) (V1.0) (Kiosk Demo) [!]
        { "e979bf2e2b43ccb5e2e0a2798ad7770c", 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [o1][h1C]
        { "e9c2b20ea27e6cab89842af4d768724e", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (U) [t1]
        { "e9e3680c47ce093a3f0765539f31c6ae", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (U) [b2]
        { "e9efcc508a7054bb94e5b3243018f1ac", 1, MEMORY_PAK }, // Quest 64 (U) [b4]
        { "e9f65ea602a442966be4ddce799a8e20", 2, MEMORY_PAK | RUMBLE_PAK }, // Rush 2 - Extreme Racing USA (E) (M6) [h2I]
        { "e9f989e09e3f1519aefe619889a4f710", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.0) [!]
        { "ea0e3e6aefa58738a12906298373218b", 4, RUMBLE_PAK }, // GoldenEye 007 (UE) (Switch Online) [!]
        { "ea168957b644afe852da1b827afc3f9f", 4, RUMBLE_PAK }, // F-ZERO X (U) [f2] (GameShark)
        { "ea335a87c87f302c6a10d0d3d3e09b72", 4, MEMORY_PAK | RUMBLE_PAK }, // Puzzle Bobble 64 (J) [f2] (PAL)
        { "ea383e48637b44fff9294d23b076548b", 3, MEMORY_PAK | RUMBLE_PAK }, // Rampage 2 - Universal Tour (U) [t2]
        { "ea4fa94072849f88cac96e60c1edd41d", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [T-Spa1.0_eduardo]
        { "ea552e33973468233a0712c251abdb6b", 1, MEMORY_PAK }, // Quest 64 (U) [!]
        { "ea5a22a5a2da96d88939171cea41a1f1", 4, MEMORY_PAK }, // Hexen (U) [h1C]
        { "ea6a92de5a221a00814f7448bf6f1b31", 4, MEMORY_PAK }, // 64 de Hakken!! Tamagotchi Minna de Tamagotchi World (J) [!]
        { "ea6d0ebd673a66c395569a2a230aea6f", 1, RUMBLE_PAK }, // Bomberman Hero (E) [b1]
        { "ea9b6ea32cceb8f7efbb2fea211b8e89", 2, MEMORY_PAK | RUMBLE_PAK }, // Hiryuu no Ken Twin (J) [h3C]
        { "ea9d71af1508636e88d95ac1d525ef93", 4, 0 }, // The Corporation 2nd Intro by TS_Garp (PD) [b1]
        { "eace2271ae7f32a71625c9074115d640", 4, 0 }, // Famista 64 (J) [b1]
        { "ead417ff24eab32e2bf45cea9200d425", 2, MEMORY_PAK }, // Namco Museum 64 (U) [f1] (PAL)
        { "eae7e0ee5328ed9f13b9cf9990189928", 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (U) [!]
        { "eb11fc0797ae1107201c4601fee5471a", 2, RUMBLE_PAK }, // Last Legion UX (J) [!]
        { "eb1908e51c8d10af8b9caf77797bfe00", 2, MEMORY_PAK | RUMBLE_PAK }, // Mike Piazza's Strike Zone (U) [!]
        { "eb31f4f9c1fe26a3a663f74e9790516e", 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (U) [!]
        { "eb357505efc9d17a08adba5590896c4d", 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [t2]
        { "eb3b078a74d4dc827e1e79791004dfbb", 1, 0 }, // Mischief Makers (E) [!]
        { "eb712f2e8a6a893141b82607b11da8a2", 4, 0 }, // LaC's Universal Bootemu V1.1 (PD)
        { "eb98f1b8c6898af7417f6882946da9b3", 4, MEMORY_PAK }, // Hexen (U) [!]
        { "ebafb5c3bcc84543a8e7fef1993e573f", 3, MEMORY_PAK | RUMBLE_PAK }, // Rampage 2 - Universal Tour (U) [t1]
        { "ebb4b4d2808df427aaa3085a41b8a954", 1, MEMORY_PAK }, // Ogre Battle 64 - Person of Lordly Caliber (U) (V1.1) [!]
        { "ebb9e3d3782347743bffb8b6d178fc8a", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (E) [h1C]
        { "ebba03f20096fc2bc178fc3a1f4ec2b6", 4, 0 }, // Sim City 64 (J)
        { "ebc0cf55fc58845c6ee86cf8b2d87303", 4, MEMORY_PAK | RUMBLE_PAK }, // WWF - War Zone (E) [!]
        { "ebdc155d7462f8c73db4730c6bf4010d", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 99 (E) [b1]
        { "ebdc9053485aaee05378a9d95d991104", 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [t2]
        { "ebf2ac190ebb2fe6d75ad0acaa2a82cd", 2, 0 }, // Mortal Kombat Trilogy (E) [t4] (Hyper Mode)
        { "ec097eb33bad93a8ac6e15f7839762ab", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [b3]
        { "ec0fae8002ac6356e0470ce21bfefa4f", 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.0) (Super W00ting Hack)
        { "ec18e3131040842e32ebab66c7496ebd", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 2000 (U) [!]
        { "ec2bcb1b7fc7d068be1f39e79e49a842", 4, MEMORY_PAK | RUMBLE_PAK }, // Xena Warrior Princess - The Talisman of Fate (E) [!]
        { "ec39579f066a9714ff030d07dec3c9d3", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [!]
        { "ec4458443fba0f09b8593ac5ed114c26", 4, 0 }, // 1964 Demo by Steb (PD)
        { "ec700909d09c081da0e4bae454c657f0", 4, MEMORY_PAK | RUMBLE_PAK }, // Michael Owens WLS 2000 (E) [b2]
        { "ec9b393b3f52b76c02e3bf2aa20145a5", 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o4]
        { "ec9db32624b6c16376429d0660f0710b", 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [f1]
        { "ecab0839d3ed0e3aa8516ef1c077cf0d", 4, 0 }, // GoldenEye 007 (U) (Citadel Hack)
        { "ecb170ebbfda0e932c07524040bcc36c", 2, 0 }, // Dark Rift (U) [!]
        { "ecb9184142d288b40bcd198fb191a275", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [f1][h1C]
        { "ecb92e2663d5b3eea08a149fc31e3701", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (U) [t2]
        { "ecc06617156122882c4fa4bbcd241a4f", 4, BIO_SENSOR }, // Tetris 64 (J) [b1]
        { "ecc1692d12fdcb0c3f605e44dd54ce8c", 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [T+Eng1.0_Zoinkity]
        { "ecd127a57e25ce8514307bb47ecc1e95", 0, 0 }, // Alienstyle Intro by Renderman (PD) [a1]
        { "ecd4f078dc77f4b2d12c1c0f5caba323", 4, RUMBLE_PAK }, // GoldenEye 007 (E) [b1]
        { "ece232d98b095604d1c15f9571de45f8", 4, 0 }, // GBlator for CD64 (PD)
        { "ed1378bc12115f71209a77844965ba50", 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.0) [!]
        { "ed1cbfc5d3756a1520cd2d57f6998bdf", 1, TRANSFER_PAK }, // Mario Artist: Talent Studio (J) [CART HACK]
        { "ed3e962653a1cd56aab175deee6ee52a", 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (E) [!]
        { "ed4414b512cf22da64b080d34b5e2f4f", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [b4]
        { "ed46a867a648a101d239da0650b49783", 4, 0 }, // SRAM Manager V2.0 (PD) [a1]
        { "ed4fe5eaa6631bb142def7f317315dfa", 1, MEMORY_PAK }, // Premier Manager 64 (E) [f2] (NTSC100%)
        { "ed5f1e12da36dbec8a0a24ed98d4aed5", 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball '99 (E) [!]
        { "ed7773988bcc03d06e2330a8e77bec6f", 2, 0 }, // Mortal Kombat Trilogy (E) [t5] (2x Aggressor)
        { "ed8641a704910bb4a0fac9ad08f9c1d2", 4, RUMBLE_PAK }, // Super Smash Bros. (U) [hI]
        { "ed99f330ce7a2638ab13351012eeb86b", 4, RUMBLE_PAK }, // Mario Party 3 (J) [!]
        { "ed9d2c04a7a0934372216ed8321ea6e3", 4, 0 }, // Doctor V64 BIOS V1.30
        { "edbdb55ad58554c48b25a47d7e178513", 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (U) (V1.0) [b1]
        { "edd7187c79cebdb0d1fabc040502e940", 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx (U) [f1] (PAL)
        { "ee045a2e9f924cd8fd00018b50e46650", 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (U) [!]
        { "ee052c69ffa67db3544872ab0e745ddf", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [o1][h1C]
        { "ee0c756f832827e3acdf6dbd410adca4", 2, MEMORY_PAK }, // Fox Sports College Hoops '99 (U) [f1] (PAL)
        { "ee273763c7391458865ff26c7ea0c3f1", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [!]
        { "ee38a903556c24520cd162fd046748a2", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [b1]
        { "ee3d3550acc463ca57408bf14e541f68", 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack (J) (V1.0) [!]
        { "ee43f010f2e87bc68535d333fe64c516", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [b1]
        { "ee4dcaed6759ce013bc7b5e8e815b343", 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [f1] (PAL)
        { "ee79a8fe287b5dcaea584439363342fc", 4, RUMBLE_PAK }, // F-ZERO X (E) [!]
        { "eeb69597e42e2f5d2914070acf161b4f", 1, RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 - Shiokaze ni Notte (J) [!]
        { "eebdfbd7cb57202d70cfffcaaf55e93e", 1, RUMBLE_PAK }, // Mission Impossible (U) [!]
        { "eec0fab75af59e9c23e6de2132de89ff", 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [!]
        { "eeead69d62eb43f093d9d46c38a82ac5", 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (U) [t1]
        { "eefd695b00354c5199df348ecf404a0f", 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [b2]
        { "ef0988efdcea31b88a3920ceec17b4ff", 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (U) [t1]
        { "ef0f425689586850a6f5796124b0c85b", 4, MEMORY_PAK }, // Jikkyou World Soccer 3 (J) [!]
        { "ef2001f581a80d760c51d130725a9930", 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (E) (M5) [f1] (NTSC)
        { "ef2453bff7ad0c4bfa9ab0bd6324ebf3", 1, RUMBLE_PAK }, // Bomberman Hero (U) [!]
        { "ef34da35ef8a0734843cb182c19feb26", 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (E) (M3) [!]
        { "ef3d1e9059723766941ff4e0e913c941", 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (U) [b3]
        { "ef5cb24ce3fe4e0f850901205437b574", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (E) [!]
        { "ef878dfacf5cd5c00ba3297b0dc888d2", 4, 0 }, // Light Force First N64 Demo by Fractal (PD)
        { "ef9a84fbf0b4c31e581648f256e8ed81", 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant's NBA Courtside (U) [f1]
        { "efbcb657a3b05b5cb080fd628b852bd0", 4, 0 }, // Pip's RPGs Beta 14 by Mr. Pips (PD)
        { "efe2cebf5bb21bf766ff72c055df36be", 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [b3]
        { "effb7b42c6d7c29540c93006e6142f99", 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [t1]
        { "f012dc3c0caa38618a1fad27c73fcbd5", 4, MEMORY_PAK }, // Jikkyou J.League Perfect Striker (J) [b2]
        { "f015ae0f3ad80975fb21b43ef7f14422", 4, 0 }, // Doctor V64 BIOS V1.33
        { "f015fc28e1d62a36b4ebf4c79ca8f285", 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.0) [!]
        { "f023f3189722a96acdcf0bdd9154ef11", 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (G) [h1C]
        { "f0595a36eab6109c5ff2420eb452b6e2", 1, 0 }, // SM64 Splitscreen Multiplayer
        { "f05bd2dad01f3e354713380aaffb63e9", 2, MEMORY_PAK }, // Virtual Chess 64 (U) (M3) [b1]
        { "f0a1cc3d1d99a60f35d37ddccf43cabb", 2, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz (U) [f1] (PAL)
        { "f0ad16aa47258818d320a5217f2e6b1e", 1, MEMORY_PAK }, // Sim City 2000 (J) [b1][o1]
        { "f0cd3b2db0f20ffdd64bf081176eb421", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.1) [t1] (Energy)
        { "f0d62c8fd773f55a34eaac20d3b95f17", 4, 0 }, // DS1 Manager V1.0 by R. Bubba Magillicutty (PD)
        { "f0f687b449a9f4b0bff08104c35ea08c", 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (G) (V1.2) [!]
        { "f0fe973338c7170d54cb41602b9d48a3", 2, MEMORY_PAK | RUMBLE_PAK }, // Snow Speeder (J) [b1]
        { "f0ff28b0d26cdefc11f22fe73148a6dc", 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [f1]
        { "f10d601d64786c52cac1fd0ab5511c82", 1, RUMBLE_PAK }, // Chopper Attack (U) [t1]
        { "f1116faebba932a0159b882af9946be6", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [f1] (PAL)
        { "f120fadb52b414eb4fb7d13092ac3cdb", 2, MEMORY_PAK }, // Dual Heroes (E) [!]
        { "f13d0803885b73b4a6b35eddd40b9253", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu - Basic Ban 2001 (J) (V1.1) [!]
        { "f152c4a90b33d0d2bfaf782f56b202df", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (E) [o2]
        { "f17884a2c16fb6fd11a74d65b1388b4a", 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G XG2 (J) [!]
        { "f1a2e4dd22adf4f90da4bddca37d5f18", 1, MEMORY_PAK }, // Pro Mahjong Tsuwamono 64 - Jansou Battle ni Chousen (J) [!]
        { "f1ae48b778c8431a50c37eb1ed96b120", 4, 0 }, // South Park - Chef's Luv Shack (E) [!]
        { "f1af35375519e97bb7c0e37e2f68416e", 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Mayhem (E) [h1C]
        { "f1d0406a74ad74be5891620b7fe6640d", 4, 0 }, // Evek - V64jr Save Manager by WT_Riker (PD)
        { "f1f1c5e2b895db63348bc738c0cdc645", 1, RUMBLE_PAK }, // Space Station Silicon Valley (U) (V1.1) [!]
        { "f1ff1f364c459701f42beb8989675d44", 2, RUMBLE_PAK }, // Magical Tetris Challenge Featuring Mickey (J) [!]
        { "f21c920f0a7bf42f80c38c39cd61bd60", 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [t4]
        { "f21e2a3a05acf49586f32dce5c449fe4", 1, 0 }, // Pilotwings 64 (E) (M3) [b1]
        { "f23e4cd437465f3e725262253cf3ea59", 4, RUMBLE_PAK }, // Mario Party 2 (J) [!]
        { "f248f0aae609111ba9dff9fd7afbc485", 2, RUMBLE_PAK }, // F-1 World Grand Prix (J) [!]
        { "f253c5a0fb69ab56a1548d435af84d0f", 4, RUMBLE_PAK }, // Mario Party 2 (U) [f1] (PAL)
        { "f26102590aa6982ea78701e6a418bed0", 4, 0 }, // SRAM Uploader-Editor by BlackBag (PD)
        { "f27300e1883ea849a69567d3aedc5f56", 4, 0 }, // NBCrew 2 Demo (PD)
        { "f275a4216744d8f04b4a0cd74de53111", 4, 0 }, // GameShark Pro V3.3 (Apr 2000) (Unl) [!]
        { "f27cbdb653aaf1d4856a53d5951f36dd", 2, MEMORY_PAK | RUMBLE_PAK }, // Polaris SnoCross (U) [o1][t1]
        { "f29a0a02c41815ee3f5437e409ec327b", 2, MEMORY_PAK | RUMBLE_PAK }, // SD Hiryuu no Ken Densetsu (J) [f1] (PAL)
        { "f2a8106403d2bf9350bfeab08689d54a", 1, 0 }, // Pokemon Snap (E) [!]
        { "f2df4577d3749345f93ba0606beca8dd", 4, 0 }, // PC-Engine 64 (POM '99) (PD) [t1]
        { "f2fa48329ff6ddbb7bd4eea838fd800b", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [o2]
        { "f30dfecb19dbd82a2fd334a8a37b8d38", 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (E) [b1]
        { "f323e3807a6aededeac9e0739490d13c", 4, 0 }, // SNES 9X Alpha by Loom-Crazy Nation (PD) [f1] (V64BIOS1.91)
        { "f32e61e30c5bc232a01e52f8bc2eef07", 0, 0 }, // Dynamix Intro by Widget and Immortal (PD) [h1C]
        { "f3365b47e430011d89162fb0d7dd8de5", 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack - Covert Operations (U) [f1] (PAL)
        { "f3515a45ec01d2c9feafbab2b85d72c4", 2, MEMORY_PAK }, // Rugrats in Paris - The Movie (U) [T+Spa0.10]
        { "f388ee4148a7a4a2c8a815823d23149c", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [b5]
        { "f38eca581bf6944ea35e3a816ad472df", 1, MEMORY_PAK }, // Pachinko 365 Nichi (J) [b2]
        { "f39476591e147906291ba48145f304b0", 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [b3]
        { "f3a1b04e1f3436e183932b54212b75b5", 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [b2]
        { "f3e25d0a7b98e8e25b7570a6e056d33c", 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (E) (M3) [f2] (NTSC)
        { "f403fceb3fcd41bb5830142989fc9486", 2, MEMORY_PAK | RUMBLE_PAK }, // Deadly Arts (U) [b1]
        { "f40d3de4745bacc69458bab25d6067f9", 1, MEMORY_PAK }, // Holy Magic Century (G) [b3]
        { "f41fca7f185b3ec45ed90491bc8c0983", 2, MEMORY_PAK }, // Virtual Chess 64 (U) (M3) [f1] (PAL)
        { "f427033d9184aca164b971e559c0b420", 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (U) [t1][f1] (PAL)
        { "f4401bcc9dd25254503e32b482eb3faa", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2000 (U) [f1] (PAL)
        { "f47575c35c7352e685b848cdaff96b80", 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [b3]
        { "f4ab4cd6a8af21e8a51bf48578cc7f24", 4, 0 }, // GoldenEye 007 (U) (G5 Multi Hack)
        { "f4b41863440137c6a3ba22942f3e0da2", 4, 0 }, // Excitebike 64 (Ch) (iQue) [!]
        { "f4b5863f550bc83c7924e423b01e1c5a", 4, 0 }, // N64probe (Button Test) by MooglyGuy (PD)
        { "f4d2be1c47486ab3a6fcccd2e7532c67", 4, MEMORY_PAK }, // Hyper Olympics in Nagano 64 (J) [b2]
        { "f52661ef93b9fd4df16f1f113082aeaa", 4, MEMORY_PAK }, // Centre Court Tennis (E) [f1] (NTSC)
        { "f5441580b27ecfb94a8c6b7989b1d101", 4, 0 }, // Z64 BIOS V1.08
        { "f5509b1ea83a49d01e5e4ef475b87278", 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t5] (2x Aggressor)
        { "f551ada6c6fc1877fca0515df4f515df", 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (U) (V1.0) [t1]
        { "f558165311c9cdca73fb089e298f2056", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [b1]
        { "f5723c4ccfb9a1ce28426962d59cc85f", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (E) (M3) [b3]
        { "f5977968b621e74773e80cc4a33f7d2e", 1, MEMORY_PAK }, // Wonder Project J2 - Koruro no Mori no Jozet (J) [h1C]
        { "f5978a54bb22e3bc4d46c0c3e9597b12", 2, MEMORY_PAK | RUMBLE_PAK }, // Mike Piazza's Strike Zone (U) [h1C]
        { "f5b33dc7dc76bb308801cb518b1515f3", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [T+Por1.0]
        { "f5c9b6e4db938258ea277670b6d37f16", 1, RUMBLE_PAK }, // Batman of the Future - Return of the Joker (E) (M3) [b1]
        { "f604701c59c7d2bba2492d0073e928f2", 4, 0 }, // J.League Tactics Soccer (J) (V1.0) [b1]
        { "f61757be8ecf0d7068c23cfce23d2f10", 4, 0 }, // GBlator for CD64 (PD) [f1] (V64-PAL)
        { "f64b263f67aca5de1957299fd92fb35a", 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (U) [b2]
        { "f661889fdf65ddcd212e9fb53b2c8f50", 4, MEMORY_PAK }, // Rat Attack (U) (M6) [!]
        { "f666e020218392e52662fddfa1ea4f21", 1, MEMORY_PAK }, // Ogre Battle 64 - Person of Lordly Caliber (U) (V1.0) [!]
        { "f67c06af4f6613804b3f700b1169b58d", 4, 0 }, // Pom Part 5 Demo (PD)
        { "f68f02a69829469f208d538d7cf4a215", 4, MEMORY_PAK }, // Golden Nugget 64 (U) [b3][h1C]
        { "f69de158bc76416b4c6c4704b15b6eb0", 1, 0 }, // Pilotwings 64 (U) [t1]
        { "f6ae85b8cc2b643f37de95d7f5682262", 4, 0 }, // The Corporation 2nd Intro by TS_Garp (PD) [a1]
        { "f6b0e57512fddd06345dd41b7c73b228", 4, 0 }, // Shag'a'Delic Demo by Steve and NEP (PD)
        { "f70112b652b0ee4856af83f4e8005c31", 4, RUMBLE_PAK }, // Mario Party 2 (E) (M5) [!]
        { "f70a06305d5c545980d9d2d16ef122b2", 4, 0 }, // CD64 BIOS EEPROM-Burner V1.08 [a1]
        { "f733453ed26afa0aca8d3eb3b5b6d8ea", 1, 0 }, // Elmo's Number Journey (U) [!]
        { "f734667ef21da97092f54d2547e11e51", 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [f2]
        { "f73ea6171b04cd9ff05b76274e824194", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [b1]
        { "f747b178ffab97ee8545f7e6efaef7ca", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [f2] (PAL)
        { "f7544b9df78d3cdb1c1b31006372c531", 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [h2C]
        { "f77d70959222276491222f31ebff3bf1", 1, RUMBLE_PAK }, // Biohazard 2 (J) [!]
        { "f79ef0813157880ffbad6199e07579be", 1, RUMBLE_PAK }, // Bomberman Hero (E) [!]
        { "f79f17353dc5683ab60257462be9d2fd", 4, 0 }, // Nintendo WideBoy 64 by SonCrap (PD)
        { "f7a0efcf8db90fbd6fd9bfc570a4804a", 4, RUMBLE_PAK }, // GoldenEye 007 (E) [t1] (Rapid Fire)
        { "f7aa4f819f41cb4236792a8145684627", 4, 0 }, // Yoshi's Story (Ch) (iQue) [!]
        { "f7c1b1ee1ce37ce09aa48c7e0a115efa", 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (E) (M4) (V1.1) [!]
        { "f7c52568a31aadf26e14dc2b6416b2ed", 4, RUMBLE_PAK }, // Super Smash Bros. (U) [!]
        { "f7c796371e77e0a6fbd02eb866341952", 4, MEMORY_PAK | RUMBLE_PAK }, // 64 Oozumou 2 (J) [!]
        { "f7ca97be225630b7690a862fca97e442", 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [f2] (NTSC-Z64)
        { "f7e5bdf8e4076c27961baf9a42c73524", 4, 0 }, // Wayne Gretzky's 3D Hockey '98 (U) [h2C]
        { "f7e66da23c8bb8e59f641a636a9cae82", 2, MEMORY_PAK | RUMBLE_PAK }, // Snow Speeder (J) [!]
        { "f7e6c9cff1bc16b0deda0ecefc5c3dfb", 4, 0 }, // Eurasia Intro by Ste (PD)
        { "f7f44996dd1140469c6c866da0affad7", 0, 0 }, // Sinus (PD)
        { "f81580c9c480ddf1b8f7e6a56d1b9cd5", 4, 0 }, // Money Creates Taste Demo by Count0 (POM '99) (PD) [f1]
        { "f818fb61331122d455692c00edb2a2ff", 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes 2 (U) [t1]
        { "f81c314b04cee9dc92fd4d50811f6fbb", 1, RUMBLE_PAK }, // Banjo-Kazooie (E) (M3) [f2]
        { "f848e34dc2918c708d2d0c02e082b06b", 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [h3C]
        { "f85f2a2b6ca64898f0add2a78ccdccf3", 1, RUMBLE_PAK }, // Wild Choppers (J) [!]
        { "f85fc5ce4451c1e9f3ee00b814489a51", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.42]
        { "f8636514b5b0edebf376c3111d24417a", 1, 0 }, // Tigger's Honey Hunt (U) [!]
        { "f871b07c700f5bb373e648fc64d99d2e", 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [b1][t1]
        { "f87937c9de3703898c480fc84c692523", 2, 0 }, // War Gods (U) [o2]
        { "f8906b10bae363acb170fa841a88e96c", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [t2]
        { "f8a35270279b277586d7210fd15134ff", 2, MEMORY_PAK }, // Virtual Chess 64 (U) (M3) [!]
        { "f8d6c5aba62fc302c2848c53104e2683", 4, 0 }, // Pip's RPGs Beta x (PD)
        { "f8d705ad5eff0db9e3e17a7ecb6d2123", 2, RUMBLE_PAK }, // Last Legion UX (J) [b2]
        { "f8f87aeb2c537c9cb2e9913050bfc928", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2 (J) [!]
        { "f8f9116279fbafd511748542994598b2", 4, RUMBLE_PAK }, // Mario Party 3 (U) [f2] (PAL)
        { "f9027df3909c849cbaddab1c1468bacc", 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [h4C]
        { "f905ca22cc030bbed516f3d1b6c2153a", 4, 0 }, // My Angel Demo (PD)
        { "f9190dbaf547d6d3f5f3569accf26061", 4, 0 }, // iQue Club (Ch) (V3) (iQue) [!]
        { "f931e1df9524bacef6b731d50d7dc10f", 1, MEMORY_PAK }, // Mahjong 64 (J) [o2]
        { "f972b634f638cc54ad6dcbfdc661cc02", 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [h1C]
        { "f9a6ffd35d478a8e2b369460bfab2cc8", 1, MEMORY_PAK }, // Human Grand Prix - New Generation (J) [b1]
        { "f9d9666de2f946c9a7d6173dfadf9a81", 4, 0 }, // Doctor V64 BIOS V1.33b
        { "f9da7eb74e391c5e6813212f7f55959a", 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [o1]
        { "f9e8fef0005e26720eb0d0fdfab7a859", 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [f1]
        { "fa0f12c15b3655f9f56888c3249b1ced", 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz L'eclair A La Rescousse! (F) [!]
        { "fa27089c425dbab99f19245c5c997613", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [!]
        { "fa31743c3fc0236d6cec0bd514dfb053", 1, RUMBLE_PAK }, // Mission Impossible (U) [f1] (PAL)
        { "fa34eaebaa8a4e48d6bbd93df62cd995", 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack - Covert Operations (U) [t1]
        { "fa3a043997a3acdf17337385b126bc04", 4, MEMORY_PAK | RUMBLE_PAK }, // Super Bowling (U) [!]
        { "fa3a5597012b710c844682da586a62c9", 1, MEMORY_PAK }, // Blast Dozer (J) [b1]
        { "fa4347ac05bc0c0bbc8cabe3ed90b8a4", 4, MEMORY_PAK }, // Bomberman 64 (U) [o1]
        { "fa635e837275d28fd5a24d5675ba42c8", 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.1) [!]
        { "fa6a2e00bbff236dac91f08544affa40", 1, MEMORY_PAK }, // Holy Magic Century (E) [b1]
        { "fa6b3e3a04de8d96b2c26f974f59a34d", 1, MEMORY_PAK | RUMBLE_PAK }, // Harukanaru Augusta Masters 98 (J) [b2]
        { "fa6ce44ecf86dec0afa321d82cd3ed6e", 4, MEMORY_PAK }, // Frogger 2 (U) (Alpha) [o2]
        { "fa88969ecfa72358ef1c045035442f5c", 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (U) [b1]
        { "fa94645f1a15aba440ad0136ed92e9d1", 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [f4]
        { "faa64abb0d222fcc0c6e2515d3805d9f", 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (E) (M4) [!]
        { "faaa2094b04dca4c287af9334d22529d", 2, RUMBLE_PAK }, // Puyo Puyo Sun 64 (J) [!]
        { "fabcb3078ede955c04a00479ee11c43f", 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [b5]
        { "fac450eaff8fa46a1414db02e6eeab9f", 2, RUMBLE_PAK }, // F-1 World Grand Prix (E) [!]
        { "fad4da8e17ce12f68cdf29180cdd4a90", 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (V1.0) [!]
        { "fb19afd5e8c49978e6e6ae3622e0498a", 2, MEMORY_PAK }, // Bottom of the 9th (U) [!]
        { "fb1f9f093f6081e5695f688b9efd4095", 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (U) [b1]
        { "fb7565fe1eaaed1cacfae3b011fa6cb6", 1, RUMBLE_PAK }, // Doraemon 3 - Nobita no Machi SOS! (J) [f1] (PAL-NTSC)
        { "fb8974c6130a6b8585ea98d6661553bc", 4, 0 }, // Hard Coded Demo by Silo and Fractal (PD)
        { "fb916f914d9635f8fcbce37a6466079d", 4, RUMBLE_PAK }, // GoldenEye 007 (J) [t1] (Rapid Fire)
        { "fbb0f6af0a931ee4feaee4f3cc7dbe03", 4, RUMBLE_PAK }, // Jinsei Game 64 (J) [f1] (PAL)
        { "fbccfed51a43b0012ce927cc73d6aada", 4, 0 }, // Diddy Kong Racing SRAM by Group5 (PD)
        { "fbdd421fc2105cf4b9663df97fd7041d", 1, 0 }, // Puzzle Master 64 by Michael Searl (PD)
        { "fbdd74ed68e6a0cd734562d56ccb752d", 1, 0 }, // Pocket Monsters Snap (J) [!]
        { "fbf566693bca3145d86df34d18dcdd43", 2, 0 }, // Pokemon Puzzle League (E) (VC) [!]
        { "fc2d2f7ef02484ea0478a5eafd0cbff0", 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (E) [b1]
        { "fc32007ba03ff2510020e979c7bdad4f", 4, 0 }, // Sydney 2000 (E) (Prototype)
        { "fc3c9329b7cdd67cf7650abf63b9a580", 1, 0 }, // Pokemon Snap (U) [!]
        { "fc47f85cc501c8c5bd9d0ca4db48258f", 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 2000 (U) (M4) [!]
        { "fc61d60f2c6fe4610f70ce4949a7a062", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (U) [!]
        { "fca7afcadcf5e5545a62919ba94dad18", 1, RUMBLE_PAK }, // Space Station Silicon Valley (E) (M7) [!]
        { "fca7be078d6dc68ac6f2e1b7b4369720", 4, 0 }, // UltraMSX2 V1.0 w-F1 Spirit by Jos Kwanten (PD)
        { "fcb5472f929c8da0daed4c0984f8053b", 4, 0 }, // Doctor V64 BIOS V1.52 (CH2 Hack)
        { "fcca9af8c1174c40b478ea0e02673b16", 4, 0 }, // Kyojin no Doshin - Kaihou Sensen Chibikkochikko Dai Shuugou (J)
        { "fcd0bff90306b13eb8e5007e5b4c2cb7", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [h2C]
        { "fcfbfd04a56400de8b84297cf37cee3d", 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (U) [hI]
        { "fd0102d535bc3bbb22dd823a636dc757", 4, MEMORY_PAK }, // International Superstar Soccer 64 (E) [b2]
        { "fd0c0e8c523437f9b6b630e369fdfc69", 1, RUMBLE_PAK }, // Mission Impossible (F) [!]
        { "fd2cbdea0992452b52e2803224232d12", 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 2001 (U) [!]
        { "fd2d06e59bf17a1bd1da98454f83ab0d", 1, MEMORY_PAK }, // Sim City 2000 (J) [b1]
        { "fd35d8a9de0f5ab500a588a6ab5f6fb9", 4, 0 }, // GoldenEye 007 (U) (Tetris Hack)
        { "fd523af9832b5a17f0eb96ed2a77e77b", 4, 0 }, // V64Jr Backup Tool V0.2b_Beta by RedboX (PD)
        { "fd85e6b20a9099f670c3cfbd13aee719", 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (U) [o1][b1]
        { "fd8681cbf1a599ff2ea55dbf40ea7d27", 4, 0 }, // Action Replay Pro 64 V3.0 (Unl) [f1]
        { "fd979768ac0ec6cbbca8c5339d62bb05", 2, MEMORY_PAK }, // AeroGauge (E) (M3) [f1] (NTSC)
        { "fda57f65eb159278223eb9d03267c27f", 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.0) [!]
        { "fdc76a53b1056d3e50ea6a3e295fe4d1", 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (G) [!]
        { "fde5f910974a8b9c561a27b07e68f244", 2, MEMORY_PAK | RUMBLE_PAK }, // Twisted Edge Extreme Snowboarding (E) [h1C]
        { "fdf797c5ffb531fcf04e0a1928691ea3", 1, 0 }, // Heiwa Pachinko World 64 (J) [o1]
        { "fdffa662ae3357bf5970338ee7589f6c", 1, RUMBLE_PAK }, // Densha de Go! 64 (J) (Localization Patch v1.01)
        { "fe15a6e7af4e37487252369d6f0ab35b", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b3]
        { "fe1b92f6a7e15a1af367fad957375b20", 4, 0 }, // Famista 64 (J) [b2]
        { "fe2605193736a128ad65db1c9835a130", 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (B) [!]
        { "fe2aa269fd3ec81c358845bba6ca0167", 4, 0 }, // South Park - Chef's Luv Shack (U) [f1] (Country Check)
        { "fe320327cf6edc64d06bfa56d8e7a085", 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [b6]
        { "fe38bb1eaa1c74baf01847bde64e7f9d", 4, 0 }, // Pamela Demo (PD)
        { "fe49420725f9c1cf0f15ba5682d27709", 4, 0 }, // Dr. Mario 64 (Ch) (V2) (iQue) (Manual) [!]
        { "fe4ac551b284bd0924f2aaa387249b4b", 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (E) [t1]
        { "fe63f970bb7f1d6798878d5835e1026c", 1, MEMORY_PAK }, // Doom 64: Complete Edition (Doom 64 Hack)
        { "fe75abb3934f88429e138019146e8393", 4, MEMORY_PAK }, // Mario Kart 64 (U) [b3]
        { "fe81aa381719fada693d803bae7d5eb9", 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (U) [!]
        { "fea06f46f61deb721cf905c785299994", 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (E) (M3) [h1C]
        { "ff02fe6a6fe7f6c31bca6962e53a63ca", 1, MEMORY_PAK | RUMBLE_PAK }, // Nightmare Creatures (U) [t1]
        { "ff4d4feff0af8a0f9b9ae509da7d9071", 0, 0 }, // Absolute Crap Intro #1 by Kid Stardust (PD)
        { "ff5a05ffde29707c15d89e807ed66b81", 1, 0 }, // Super Mario 64 (U) (No Cap Hack)
        { "ff67df97476c210d158779ae6142f239", 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [!]
        { "ff7c6d9909d36d3a11d4c9052eafb2e8", 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T-Por.22]
        { "ff9cc9e03993dd15f1436af3874f94cf", 1, RUMBLE_PAK }, // Mission Impossible (F) [b1]
        { "ff9f85c50982dbdba9853a8915321d31", 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (F) [!]
        { "ffa562936db863ea02204f9f514b6067", 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.1) [t2] (Boost)
        { "ffaaafb32a2d87c6b621b62ffc85a2d6", 4, 0 }, // Z64 BIOS V2.00 (Barebones)
        { "ffc584040d0d052fbab4cb6c19245449", 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (U) [!]
        { "ffdb4456f799722bcfe430632c3986ae", 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.1) [!]
        { "ffe3839fd3974a0858a62cf363d0f8db", 4, 0 }, // RADWAR 2K Party Inv. Intro by Ayatolloh (PD)
        { "ffeb5c1a85babbbe60f2feba2b35c893", 2, RUMBLE_PAK }, // Uchhannanchan no Hono no Challenger - Denryu IraIra Bou (J) [!]
        { "fff4e1455c03f71272063c309d9f9102", 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [h1C]
        { "fff632a867cdadb5ab2098cb8e2cfadc", 4, 0 }, // WideBoy BIOS V980914
        { "fff9b3e22abb9b60215dafb13ad5a4de", 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis (E) [!]
    };

    public const GameDataCRC[] GAME_DATA_CRC = {
        { 0x0000000000000000, 4, 0 }, // 64DD IPL (JPN)
        { 0x0000000000005053, 4, 0 }, // Z64 BIOS V2.20cf
        { 0x001A3BD0AFB3DE1A, 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (F) [!]
        { 0x00A46EE3554158C6, 1, RUMBLE_PAK }, // Body Harvest (U) [b2]
        { 0x00BC1D7800709D53, 1, RUMBLE_PAK }, // Taz Express (E) (M6) [f2] (NTSC)
        { 0x00EDCC3475D2DEA0, 2, MEMORY_PAK | RUMBLE_PAK }, // Space Invaders (U) [f1] (PAL)
        { 0x011F98B21E1C8263, 4, 0 }, // NBCG Special Edition (PD)
        { 0x015BB81EF105AC82, 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (U) (M2) [f1] (PAL)
        { 0x018CF7335409AD45, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (U) [t1]
        { 0x0198A651FC219D84, 4, MEMORY_PAK }, // FIFA 99 (E) (M8) [!]
        { 0x01D66F37365F18B3, 4, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush 2049 (U) [t1]
        { 0x022E645675BBB3C7, 2, 0 }, // Mace - The Dark Age (U) [b4]
        { 0x0290AEB967A3B6C1, 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T+Eng2010-12-02_Zoinkity]
        { 0x029CAE052B8F9DF1, 4, 0 }, // SRAM Manager V1.0 Beta (PD)
        { 0x02B1538FC94B88D0, 1, 0 }, // Elmo's Number Journey (U) [!]
        { 0x02B46F5561778D0B, 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (E) (M2) (Ita-Spa) [!]
        { 0x02D8366A6CABEF9C, 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (E) [h1C]
        { 0x0304C48EAC4001B8, 4, MEMORY_PAK }, // Rat Attack (U) (M6) [!]
        { 0x033F4C13319EE7A7, 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (U) [!]
        { 0x03529CD9186BDDCD, 1, 0 }, // Pokemon Snap (U) [f3] (GameShark)
        { 0x03571182892FD06D, 4, TRANSFER_PAK }, // Pokemon Stadium 2 (U) [!]
        { 0x036897CEE0D4FA54, 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (U) (M2) [!]
        { 0x036A20D8A99C4D3D, 2, RUMBLE_PAK }, // StarCraft 64 (Beta) [f2] (PAL)
        { 0x036CBFE59E626365, 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [t2]
        { 0x0375CF6756A93FAA, 1, 0 }, // Ogre Battle 64 - Person of Lordly Caliber (J) (V1.1) [b2]
        { 0x03D7F0218FDE0000, 4, 0 }, // Neon64 GS V1.05 (Gameshark Version) (PD)
        { 0x0414CA612E57B8AA, 4, RUMBLE_PAK }, // GoldenEye 007 (E) [h1C]
        { 0x045C08C44AFD798B, 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 64 (J) [h1C]
        { 0x0481591487141455, 4, 0 }, // 2 Blokes & An Armchair - Nintendo 64 Remix Remix by Tesko (PD)
        { 0x04842D981A9FF551, 1, 0 }, // Earthworm Jim 3D (U) [hI]
        { 0x0484CD981AFE0B59, 1, 0 }, // Earthworm Jim 3D (U) [t1]
        { 0x04853364948ACF82, 1, 0 }, // Earthworm Jim 3D (U) [f1] (PAL)
        { 0x04DAF07F0D18E688, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (U) [b4]
        { 0x053C89A7A5064302, 4, RUMBLE_PAK }, // Donkey Kong 64 (J) [!]
        { 0x0549765A93B9D042, 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [o2]
        { 0x0553AE9DEAD8E0C1, 4, MEMORY_PAK | RUMBLE_PAK }, // Xena Warrior Princess - The Talisman of Fate (U) [!]
        { 0x056EAB63C215FCD5, 2, MEMORY_PAK }, // Dual Heroes (J) [o2]
        { 0x0578F24F9175BF17, 4, RUMBLE_PAK }, // Top Gear Overdrive (J) [b1]
        { 0x058381EF6A36FEC6, 4, TRANSFER_PAK }, // Pocket Monsters Stadium 2 (J) [f3]
        { 0x05BCD54E97E2A8C9, 1, RUMBLE_PAK }, // Yoshi Story (J) [f3]
        { 0x05EDE011CE166686, 1, 0 }, // Super Mario 64 (U) [T+Ita4.2_Rulesless]
        { 0x0684FBFB5D3EA8A5, 2, RUMBLE_PAK }, // StarCraft 64 (U) [!]
        { 0x06CB44B73163DB94, 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.2) [b1]
        { 0x06F6B18097F0AD11, 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [f2] (PAL)
        { 0x076C5C15BDA9D062, 4, RUMBLE_PAK }, // F-ZERO X (J) [t1]
        { 0x076F8FB3509A2054, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (Beta) (1998-08-05) [!]
        { 0x07861842A12EBC9F, 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (V1.0) [b1]
        { 0x079501B9AB0232AB, 4, RUMBLE_PAK }, // Custom Robo V2 (J) [!]
        { 0x07A69D019A7D41A1, 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (E) [b1]
        { 0x07C1866E5775CCDE, 2, RUMBLE_PAK }, // F-1 World Grand Prix II (E) (M4) [h1C]
        { 0x07F3B276EC8F3D39, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (U) [!]
        { 0x07FCBF1D24546EB4, 2, RUMBLE_PAK }, // Puyo Puyo Sun 64 (J) [T+Kor1.0_Zoinkity&UltimatePenguin]
        { 0x081235950510F1DE, 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (U) [h1C]
        { 0x0837F87AD1436FF8, 4, 0 }, // Kyojin no Doshin 1 (J) [CART HACK]
        { 0x085055B65346ED7B, 4, MEMORY_PAK }, // Mario Kart 64 (U) [t2] (Course Cheat)
        { 0x0866BEDDAE31C2EE, 1, RUMBLE_PAK }, // Bakuretsu Muteki Bangai-O (J) [f1] (PAL)
        { 0x086E91C989F47C51, 0, 0 }, // Dynamix Intro by Widget and Immortal (PD) [h1C]
        { 0x0894909CDAD4D82D, 2, MEMORY_PAK | RUMBLE_PAK }, // Knockout Kings 2000 (U) [!]
        { 0x08D4926203763D39, 4, 0 }, // Die Hard 64 (U) (Prototype) (Level 2)
        { 0x08FFA4B701F453B6, 2, MEMORY_PAK | RUMBLE_PAK }, // Big Mountain 2000 (U) [!]
        { 0x09035DC5870C1041, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (E) [f1] (NTSC)
        { 0x093F916E4408B698, 1, MEMORY_PAK }, // Tonic Trouble (E) (M5) [!]
        { 0x09465AC3F8CB501B, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (GC) [!]
        { 0x096816CCD272EA8C, 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (U) (V1.1) (M2) [!]
        { 0x096A40EA8ABE0A10, 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (U) (M10) [b1]
        { 0x098D46A2D1A4CF3B, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 - 2nd Offense (U) [f1] (PAL)
        { 0x09A0A6E58396630D, 2, MEMORY_PAK | RUMBLE_PAK }, // Rush 2 - Extreme Racing USA (E) (M6) [h2I]
        { 0x09AE57B1182A5637, 1, MEMORY_PAK | RUMBLE_PAK }, // Harukanaru Augusta Masters 98 (J) [h1C]
        { 0x09CC4801E42EE491, 1, 0 }, // Pilotwings 64 (J) [b1]
        { 0x09D4D757F2775DBD, 1, RUMBLE_PAK }, // Mission Impossible (U) [f1] (PAL)
        { 0x09D53E163AB268B9, 2, MEMORY_PAK | RUMBLE_PAK }, // Mike Piazza's Strike Zone (U) [h3C]
        { 0x0A1667C7293346A6, 4, MEMORY_PAK | RUMBLE_PAK }, // Xena Warrior Princess - The Talisman of Fate (E) [!]
        { 0x0A5D8F8398C5371A, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.1) [T+Ita1.0Beta_Vampire]
        { 0x0AA0055B7637DF65, 1, RUMBLE_PAK }, // Looney Tunes - Duck Dodgers (E) (M6) [!]
        { 0x0AC244D11F0EC605, 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 2000 (J) (V1.0) [!]
        { 0x0AC61D39ABFA03A6, 2, MEMORY_PAK }, // Rugrats in Paris - The Movie (E) [!]
        { 0x0ADAECA7B17F9795, 1, MEMORY_PAK }, // Ogre Battle 64 - Person of Lordly Caliber (U) (V1.1) [!]
        { 0x0AE2B37CFBC174F0, 1, MEMORY_PAK }, // Pro Mahjong Kiwame 64 (J) (V1.1) [!]
        { 0x0B0AB4CD7B158937, 4, RUMBLE_PAK }, // Mario Party 3 (J) [!]
        { 0x0B28EBA35E062616, 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [t4]
        { 0x0B46D03D5FFAE173, 4, RUMBLE_PAK }, // F-ZERO X (U) [f2] (GameShark)
        { 0x0B58B8CDB7B291D2, 1, RUMBLE_PAK }, // Body Harvest (E) (M3) [!]
        { 0x0B623F24F8D4F434, 4, MEMORY_PAK }, // Mario Kart 64 (U) [t3] (Star Cheat)
        { 0x0B6B4DDB9671E682, 4, MEMORY_PAK | RUMBLE_PAK }, // Roadsters Trophy (U) (M3) [!]
        { 0x0B93051B603D81F9, 1, 0 }, // Mischief Makers (U) (V1.0) [o1]
        { 0x0BBCDFCF2DD952CC, 2, RUMBLE_PAK }, // AeroFighters Assault (E) (M3) [f1] (NTSC)
        { 0x0BD8008B26051E1D, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (E) [f1] (NTSC)
        { 0x0BFC12D91B377DA7, 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (U) [b2]
        { 0x0C02B3C59E2511B8, 4, RUMBLE_PAK }, // Rugrats - Scavenger Hunt (U) [!]
        { 0x0C41F9C201717A0D, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (F) [!]
        { 0x0C5057AD046E126E, 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (E) (M4) (V1.1) [!]
        { 0x0C581C7A3D6E20E4, 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.2) [!]
        { 0x0C5EE085A167DD3E, 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (U) [b1]
        { 0x0C814EC458FE5CA8, 1, MEMORY_PAK }, // Scooby-Doo! - Classic Creep Capers (U) (V1.0) [!]
        { 0x0CAD17E671A5B797, 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx - Global Assault (E) (M3) [!]
        { 0x0CB816865FD85A81, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 2000 (U) [!]
        { 0x0CE3D2E66C6E443D, 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [f1] (PAL)
        { 0x0CEBC4C70C9CE932, 1, RUMBLE_PAK }, // Wild Choppers (J) [o1]
        { 0x0D93BA11683868A6, 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (E) [b1]
        { 0x0D99E89943E0FCD1, 4, 0 }, // Ghemor - CD64 Xfer & Save Util (Parallel) by CrowTRobo (PD)
        { 0x0DCBC50D19FD1AA3, 4, RUMBLE_PAK }, // GoldenEye 007 (UE) (Switch Online) [!]
        { 0x0DD4ABABB5A2A91E, 4, RUMBLE_PAK }, // Donkey Kong 64 (U) (Kiosk Demo) [b2]
        { 0x0DE2CE36D41D29E6, 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (J) [b1]
        { 0x0DED05681502515E, 4, MEMORY_PAK }, // Eikou no Saint Andrews (J) [h2C]
        { 0x0E31EDF0C37249D5, 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (E) (M7) [o1]
        { 0x0E3ED77B8E1C26FD, 4, 0 }, // Cube Demo (PD) [b2]
        { 0x0E450FFC9FFC8375, 1, 0 }, // Glover (U) [b1]
        { 0x0E4B8C927F47A9B8, 4, MEMORY_PAK }, // VNES64 V0.12 by Jean-Luc Picard (PD)
        { 0x0E596247753D4B8B, 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (J) [!]
        { 0x0EC158F5FB3E6896, 1, RUMBLE_PAK }, // Mega Man 64 (U) [!]
        { 0x0EC580B5EBEE9C03, 1, 0 }, // Dexanoid R1 by Protest Design (PD) [t1]
        { 0x0F1FA987BFC1AFA6, 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (U) [!]
        { 0x0F5FD10BAAEDEBEB, 4, RUMBLE_PAK }, // Top Gear Overdrive (U) [t1]
        { 0x0F743195D8A6DB95, 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (E) (M3) [h1C]
        { 0x0FC42C708754F1CD, 1, MEMORY_PAK }, // Mahjong Master (J) [o1]
        { 0x0FC70E2728FFE7AA, 1, RUMBLE_PAK }, // Space Station Silicon Valley (U) (V1.1) [!]
        { 0x0FE684A98BB77AC4, 2, 0 }, // Tetrisphere (E) [b2]
        { 0x1001F10C3D51D8C1, 4, MEMORY_PAK | RUMBLE_PAK }, // Mia Hamm Soccer 64 (U) (M2) [!]
        { 0x102888BF434888CA, 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (U) [!]
        { 0x1063755703E93693, 4, 0 }, // Cliffi's Little Intro by Cliffi (POM '99) (PD) [b1]
        { 0x1077590AB537FDA2, 4, 0 }, // Planet Console Intro (PD)
        { 0x112051D268BEF8AC, 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (V1.1) [!]
        { 0x1145443D11610EDB, 2, 0 }, // Mace - The Dark Age (E) [o2]
        { 0x1180A247EBE234B3, 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [t2] (No Damage-Unbreakable Wings)
        { 0x1185EC854B5A7731, 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G XG2 (E) (M5) [!]
        { 0x11936D8C6F2C4B43, 4, RUMBLE_PAK }, // Donkey Kong 64 (E) (M4) [b1]
        { 0x1194FFD2808C6FB1, 4, 0 }, // DS1 Manager V1.0 by R. Bubba Magillicutty (PD)
        { 0x11C646E74D86C048, 4, 0 }, // Fogworld USA Demo by Horizon64 (PD)
        { 0x11FBD007E7029A81, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter Destiny 2 (U) [f1] (PAL-NTSC)
        { 0x123D0F44CF60A446, 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (U) (M2) [f2] (PAL)
        { 0x126788304E5AE439, 4, 0 }, // GameBooster 64 V1.1 (PAL) (Unl)
        { 0x12737DA523969159, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (J) [b1]
        { 0x132D2732C70E9118, 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (U) [o1]
        { 0x136EBEE335B7229E, 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (E) (Beta) [!]
        { 0x13836389265B3C76, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (U) [o3]
        { 0x138D7E5672BCC9D9, 2, MEMORY_PAK | RUMBLE_PAK }, // Space Invaders (U) [t2]
        { 0x139A06BC416B0055, 4, 0 }, // Mind Present Demo Readme by Widget and Immortal (POM '98) (PD)
        { 0x13CD08303CCF65E9, 1, 0 }, // SM64 Splitscreen Multiplayer
        { 0x13D0EE29D57A41AD, 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [t2]
        { 0x13E959A00E93CAB0, 4, 0 }, // Worms - Armageddon (U) (M3) [!]
        { 0x1416FEBB93472028, 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (U) (V1.0) [t1]
        { 0x142A17AA13028D96, 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (E) (M4) [!]
        { 0x146C436672A6DEB3, 4, MEMORY_PAK }, // Jikkyou J.League Perfect Striker (J) [b3]
        { 0x147E0EDB36C5B12C, 1, RUMBLE_PAK }, // Neon Genesis Evangelion (J) [b3]
        { 0x14DE709EF882947D, 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (U) [f1] (PAL)
        { 0x150DDE31969D3994, 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (U) [t1]
        { 0x151F79F48EEDC8E5, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (E) [h1C]
        { 0x155B7CDFF0DA7325, 4, RUMBLE_PAK }, // Banjo-Tooie (A) [!]
        { 0x158A155DC1538CDB, 4, 0 }, // Action Replay Pro 64 V3.0 (Unl) [b1]
        { 0x15A0096934E5A285, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2000 (U) (V1.0) [!]
        { 0x15AA9AF2FF33D333, 4, 0 }, // NBCG's Tag Gallery 01 by CALi (PD)
        { 0x15DB95D477BC52D8, 4, 0 }, // NBC First Intro by CALi (PD)
        { 0x161545FEA3C44C28, 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (U) (V1.0) [f1] (PAL)
        { 0x1649D810F73AD6D2, 1, MEMORY_PAK | TRANSFER_PAK }, // Super Robot Taisen 64 (J) [b2]
        { 0x16510823E2EEE387, 1, 0 }, // Super Mario 64 (E) (M3) [t2]
        { 0x165CBDA205A62246, 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.0) [t1]
        { 0x166312CBC30220D2, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (E) (M3) [f1] (Save)
        { 0x1671E7BCB7D756B9, 1, MEMORY_PAK }, // Tonic Trouble (U) (V1.1) [f1] (PAL)
        { 0x1671F4A05C0E7928, 1, MEMORY_PAK }, // Tonic Trouble (U) (V1.1) [t1]
        { 0x16931D7465DC6D34, 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (E) [o1]
        { 0x16EDC447B291F183, 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [t1]
        { 0x16FB52A47AED1FB3, 4, 0 }, // GameShark Pro V2.0 (Unl)
        { 0x170D49738FA23F51, 2, MEMORY_PAK | TRANSFER_PAK }, // PD Ultraman Battle Collection 64 (J) [f2] (PAL)
        { 0x1739EFBAD0B43A68, 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant in NBA Courtside (E) [!]
        { 0x17679054E235F4A7, 1, RUMBLE_PAK }, // Taz Express (E) (M6) [f1] (NTSC)
        { 0x17C54A614A83F2E7, 1, RUMBLE_PAK }, // Densha de Go! 64 (J) [!]
        { 0x1843910560424C9E, 1, RUMBLE_PAK }, // Banjo to Kazooie no Daibouken (J) [f3]
        { 0x18531B7D074AF73E, 4, MEMORY_PAK }, // Simon for N64 V0.1a by Jean-Luc Picard (POM '99) (PD)
        { 0x186CC1A9A0DE4C8D, 0, 0 }, // Dynamix Readme by Widget and Immortal (PD)
        { 0x1899FF33885180E6, 0, 0 }, // SPLiT's Nacho64 by SPLiT (PD) [f1] (PAL)
        { 0x1917566C8BE6CD0F, 2, MEMORY_PAK | RUMBLE_PAK }, // Supercross 2000 (U) [f1] (PAL)
        { 0x192C14225B8CFF48, 1, RUMBLE_PAK }, // Banjo-Kazooie (E) (M3) [f1]
        { 0x192C15DA74BD6300, 1, RUMBLE_PAK }, // Banjo-Kazooie (E) (M3) [f2]
        { 0x1938525C586E9656, 4, MEMORY_PAK | RUMBLE_PAK }, // Ms. Pac-Man - Maze Madness (U) [!]
        { 0x19AB29AFC71BCD28, 1, RUMBLE_PAK }, // Paper Mario (E) (M4) [!]
        { 0x19C553A7A70F4B52, 2, 0 }, // Pokemon Puzzle League (U) (VC) [!]
        { 0x19E0E54CCB721FD2, 4, 0 }, // Dragon King by CrowTRobo (PD)
        { 0x19F55D4673A27B34, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.1) [!]
        { 0x1A122D43C17DAF0F, 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.1) [b1]
        { 0x1A4D3AD859AF7FA9, 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (As) [!]
        { 0x1A5C1CAD7BAF97F2, 4, 0 }, // GameBooster 64 V1.1 (NTSC) (Unl) [b1]
        { 0x1A7F70B500B7B9FD, 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater 3 (U)
        { 0x1AA05AD546F52D80, 1, 0 }, // Pilotwings 64 (E) (M3) [h1C]
        { 0x1AA7151951360D55, 4, 0 }, // T-Shirt Demo by Neptune and Steve (POM '98) (PD)
        { 0x1AD61BB9F1E2BE1A, 4, 0 }, // Super Fighter Demo Halley's Comet Software (PD)
        { 0x1B598BF1ECA29B45, 2, RUMBLE_PAK }, // AeroFighters Assault (U) [h3C]
        { 0x1B5AAD82368B88C1, 4, RUMBLE_PAK }, // SmashRemix0.9.4
        { 0x1B6FAB5999199038, 4, 0 }, // Spacer by Memir (POM '99) (PD) [t2][b1]
        { 0x1B80E202CB5AE74F, 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [t2] (Boost)
        { 0x1BC0CA2C0F516B86, 4, 0 }, // Pom Part 1 Demo (PD)
        { 0x1BDCB30FA132D876, 1, MEMORY_PAK }, // Pro Mahjong Tsuwamono 64 - Jansou Battle ni Chousen (J) [!]
        { 0x1C010CCD22D3B7FA, 1, MEMORY_PAK }, // Zool - Majou Tsukai Densetsu (J) [!]
        { 0x1C348AFE5B96ECFA, 2, RUMBLE_PAK }, // Puyo Puyo Sun 64 (J) [T+Eng1.1_Zoinkity]
        { 0x1C37BF54AC368304, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [t2]
        { 0x1C5EE088D32A963D, 1, 0 }, // Star Wars - Shadows of the Empire (U) (Unl)
        { 0x1C6F7718CBAC1E7E, 4, RUMBLE_PAK }, // GoldenEye 007 (U) [t3] (All Guns Zoom Mode)
        { 0x1C8CDF74F761051F, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.1) [!]
        { 0x1C952873FDA4238B, 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (U) [f1] (PAL)
        { 0x1CC0633887388926, 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [b1]
        { 0x1CD50DF2345593AF, 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.0) [t1]
        { 0x1CEACBA042BC06D6, 4, 0 }, // Vector Demo by Destop (POM '99) (PD)
        { 0x1D4136F3AF63EEA9, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (E) (GC) [h1C]
        { 0x1D82BE217EF19C9A, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (E) [f2] (Z64-Save)
        { 0x1E0E96E84E28826B, 4, RUMBLE_PAK }, // Charlie Blast's Territory (U) [hIR]
        { 0x1E12883DD3B92718, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (F)
        { 0x1E17A3779D8C5E86, 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [f1] (PAL)
        { 0x1E22CF2E42AAC813, 4, MEMORY_PAK }, // Brunswick Circuit Pro Bowling (U) [o1]
        { 0x1E4C4743FEA2C4D1, 2, RUMBLE_PAK }, // Cruis'n Exotica (U) [t1]
        { 0x1EA26214E790900F, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (E) [!]
        { 0x1EC6C03CB0954ADA, 4, 0 }, // Mempack Manager for Jr 0.9b by deas (PD)
        { 0x1EDA4DE022BF698D, 4, 0 }, // XtraLife Dextrose Demo by RedboX (PD)
        { 0x1F95CAAA047FC22A, 4, RUMBLE_PAK }, // Donkey Kong 64 (E) (M4) [f1] (Save)
        { 0x1FA056E0A4B9946A, 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack (J) (V1.0) [b1]
        { 0x1FB5D9323BA9481B, 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (U) [!]
        { 0x1FBAF1612C1C54F1, 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f2][b1]
        { 0x1FBD27A96CC3EB42, 0, 0 }, // SPLiT's Nacho64 by SPLiT (PD)
        { 0x1FC215320B6466D4, 2, MEMORY_PAK }, // Rugrats in Paris - The Movie (U) [!]
        { 0x1FDFCBDCFCD77D11, 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (U) [t1]
        { 0x1FE04890D6696958, 4, 0 }, // Pom Part 3 Demo (PD)
        { 0x20073BC75E3B0111, 4, MEMORY_PAK | RUMBLE_PAK }, // International Track & Field 2000 (U) [!]
        { 0x20095B34343D9E87, 1, RUMBLE_PAK }, // Mission Impossible (F) [b2]
        { 0x201DA461EC0C992B, 4, 0 }, // DS1 SRAM Manager V1.1 by _Sage_ (PD)
        { 0x202A8EE483F88B89, 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (E) [!]
        { 0x204489C11286CF2B, 1, RUMBLE_PAK }, // Batman Beyond - Return of the Joker (U) [o1]
        { 0x204EC022B119D185, 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater (U) (V1.0) [!]
        { 0x204F4E4C59000000, 4, 0 }, // Doctor V64 BIOS V1.52
        { 0x2070044BE7D82D16, 4, 0 }, // TR64 Demo by FIres and Icepir8 (PD)
        { 0x208A0427DF465260, 4, MEMORY_PAK }, // New Tetris, The (U) [f1] (PAL)
        { 0x208A04370E53FF5F, 4, MEMORY_PAK }, // New Tetris, The (U) [f2] (PAL)
        { 0x20B536627B61899F, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (E) (M3) [h1C]
        { 0x20FD0BF1F5CF1D87, 4, MEMORY_PAK }, // Rat Attack (E) (M6) [h1C]
        { 0x21250314E6A82CB9, 4, MEMORY_PAK }, // FIFA 99 (U) [b1]
        { 0x21260D9406AE1DFE, 4, 0 }, // Tommy Thunder (U) (Prototype)
        { 0x2129FFF8AD000000, 4, 0 }, // F-ZERO Expansion Kit (N64DD)
        { 0x2138A076408AD8B2, 4, MEMORY_PAK | RUMBLE_PAK }, // ECW Hardcore Revolution (G) [!]
        { 0x214CAD94BE1A3B24, 1, RUMBLE_PAK }, // Chopper Attack (U) [b6]
        { 0x2153143F992D6351, 4, MEMORY_PAK }, // New Tetris, The (U) [!]
        { 0x21548CA99059F32C, 4, 0 }, // Mini Racers (U) (Prototype)
        { 0x219109730F2E70D2, 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G XG2 (U) [t1]
        { 0x219191C133183C61, 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (E) (M3) (V1.1) [!]
        { 0x21CD632F8BCA2AD4, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) (Unl)
        { 0x21F7ABFB6A8AA7E8, 4, MEMORY_PAK | RUMBLE_PAK }, // Jeremy McGrath Supercross 2000 (E) [!]
        { 0x21FF039109F141A1, 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (U) [t2]
        { 0x21FFFD0AFA0D1D98, 4, RUMBLE_PAK }, // Ganbare Nippon! Olympics 2000 (J) [!]
        { 0x2209094B2C9559AF, 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (E) (M4) [h1C]
        { 0x2209095FECD28311, 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (E) (M4) [b1]
        { 0x222123514046594B, 2, RUMBLE_PAK }, // Sonic Wings Assault (J) [!]
        { 0x2256ECDA71AB1B9C, 1, RUMBLE_PAK }, // Mission Impossible (E) [b1]
        { 0x22AD2E8E73CC712C, 1, RUMBLE_PAK }, // Command & Conquer (U) [f1] (Z64)
        { 0x22CA2C52CA62736F, 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.0) [f2] (PAL)
        { 0x22E9623FB60E52AD, 2, MEMORY_PAK | RUMBLE_PAK }, // Flying Dragon (E) [!]
        { 0x22F301B6D0A03C3C, 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [b2]
        { 0x2337D8E86B8E7CEC, 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [!]
        { 0x2374957880DC58FD, 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [o1]
        { 0x237E73B4D63B6B37, 4, MEMORY_PAK | RUMBLE_PAK }, // Bomberman 64 - The Second Attack! (U) [!]
        { 0x2388984CDA7B3CC5, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (U) (V1.1) [!]
        { 0x23E3989673AB48DC, 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [b3]
        { 0x24081000AD880000, 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (E) [b1]
        { 0x2483F22B136E025E, 4, RUMBLE_PAK }, // Lylat Wars (A) (M3) [f1]
        { 0x24F7EB5774B3F9F4, 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [hI]
        { 0x2500267E2A7EC3CE, 4, RUMBLE_PAK }, // RR64 - Ridge Racer 64 (U) [!]
        { 0x254CC7366F5C721B, 4, 0 }, // Glover (Prototype)
        { 0x254CF663DC8131EE, 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [t1]
        { 0x255018DF57D6AE3A, 1, RUMBLE_PAK }, // Lode Runner 3-D (U) [!]
        { 0x2575EF19D13D2A2C, 4, 0 }, // GT Demo (PD)
        { 0x2577C7D4D18FAAAE, 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.1) [o1]
        { 0x259F7F847C9EED26, 1, RUMBLE_PAK }, // Batman of the Future - Return of the Joker (E) (M3) [o1]
        { 0x26035CF8802B9135, 1, RUMBLE_PAK }, // Mission Impossible (U) [b1]
        { 0x2621129797DB4DFC, 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [hI][f1] (PAL)
        { 0x264D7E5C18874622, 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [o1]
        { 0x2655BB70667D9925, 1, MEMORY_PAK | RUMBLE_PAK }, // O.D.T. (U) (M3) [!]
        { 0x26647A178D3980C3, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [t1]
        { 0x267FBAD57593FADC, 1, 0 }, // Super Mario 64 (U) [T+Rus]
        { 0x26809B2039A516A4, 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [f1] (PAL)
        { 0x26AD85C5F908A36B, 4, 0 }, // Pamela Demo - Resized (PD)
        { 0x26CD0F5453EBEFE0, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Choro Q 64 II - Hacha Mecha Grand Prix Race (J) [!]
        { 0x26E43C40E11F283C, 4, 0 }, // Attax64 by Pookae (POM '99) (PD) [b1]
        { 0x272B690FAD0A7A77, 2, TRANSFER_PAK }, // Robot Ponkottsu 64 - 7tsu no Umi no Caramel (J) [!]
        { 0x273433C002B1F61B, 4, 0 }, // BMP View by Count0 (PD)
        { 0x275A4533ACD94796, 1, RUMBLE_PAK }, // Lode Runner 3-D (U) [f1] (PAL)
        { 0x277B129DDD3879FF, 1, MEMORY_PAK }, // Holy Magic Century (E) [b1]
        { 0x27A3831DB505A533, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (E) (GC) [f1] (NTSC)
        { 0x27C425D08C2D99C1, 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (E) [h1C]
        { 0x27C985A8ED7CE5C6, 4, 0 }, // Mega Man 64 (U) (Beta) [!]
        { 0x2803B8CE4A1EE409, 4, 0 }, // Pom Part 5 Demo (PD)
        { 0x2816D957A59FF70A, 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [f2]
        { 0x2829657EA0621877, 4, RUMBLE_PAK }, // Mario Party (U) [!]
        { 0x282A426258B47E76, 4, 0 }, // Money Creates Taste Demo by Count0 (POM '99) (PD)
        { 0x2857674DCC4337DA, 1, MEMORY_PAK | RUMBLE_PAK }, // Nightmare Creatures (U) [b2]
        { 0x28705FA5B509690E, 2, MEMORY_PAK | RUMBLE_PAK }, // Monaco Grand Prix - Racing Simulation 2 (E) (M4) [!]
        { 0x28768D6DB379976C, 2, MEMORY_PAK | RUMBLE_PAK }, // Monaco Grand Prix (U) [!]
        { 0x2877AC2DC3DC139A, 2, MEMORY_PAK | RUMBLE_PAK }, // Racing Simulation 2 (G) [h1C]
        { 0x28784622FFB22985, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 2001 (U) [!]
        { 0x287AAA278C2C02D8, 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [f1] (NTSC100%)
        { 0x287D601EABF4F8AE, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL 99 (E) [!]
        { 0x28D5562DE4D5AE50, 2, RUMBLE_PAK }, // Uchhannanchan no Hono no Challenger - Denryu IraIra Bou (J) [o1]
        { 0x2952369CB6E4C3A8, 4, TRANSFER_PAK }, // Pokemon Stadium 2 (E) [T+Ita0.05]
        { 0x2954A98FC2236510, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [f4]
        { 0x29A045CEABA9060E, 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (F) [!]
        { 0x29A9E7F61B90CFCE, 4, RUMBLE_PAK }, // Monster Truck Madness 64 (U) [t1]
        { 0x29CE769271C58579, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (E) [o1][h1C]
        { 0x2A49018DD0034A02, 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (E) (M5) [b1]
        { 0x2A6B18206ABCF466, 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.0) [o3]
        { 0x2A721F064097F7B1, 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [f1] (PAL)
        { 0x2A9804007D805B1C, 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [f2] (PAL)
        { 0x2AD072307A0719F7, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Mayhem (U) [f1] (PAL)
        { 0x2AF9B65C85E2A2D7, 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (U) [o1]
        { 0x2B38AEC06350B810, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (F) [!]
        { 0x2B4F4EFB43C511FE, 2, RUMBLE_PAK }, // Tom and Jerry in Fists of Furry (E) (M6) [!]
        { 0x2B696CB47B93DCD8, 4, RUMBLE_PAK }, // Les Razmoket - La Chasse Aux Tresors (F) [!]
        { 0x2B6FA7C009A71225, 4, 0 }, // Clay Fighter 63 1-3 (U) [o1]
        { 0x2BC1FCF27B9A0DF4, 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (E) (M3) (Fre-Ger-Dut) [!]
        { 0x2BCCF9C4403D9F6F, 4, MEMORY_PAK }, // Choro Q 64 (J) [h1C]
        { 0x2C3E19BD5113EE5E, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW vs. nWo - World Tour (U) (V1.0) [b1]
        { 0x2C739EAC9EF77726, 1, MEMORY_PAK }, // Doom 64 (E) [b1]
        { 0x2CBB127F09C2BFD8, 2, MEMORY_PAK | RUMBLE_PAK }, // Supercross 2000 (E) (M3) [!]
        { 0x2CE75AEFE16825BC, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (U) (Debug) (2000-03-22) [!]
        { 0x2CEEE75431B69DBE, 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (G) [f1] (NTSC)
        { 0x2D03FD5BD5C096D5, 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (U) [b2]
        { 0x2D15DC8CD3BBDB52, 4, 0 }, // Boot Emu by Jovis (PD)
        { 0x2D21C57B8FE4C58C, 4, 0 }, // Worms - Armageddon (E) (M6) [!]
        { 0x2D56F77BCBE813FC, 4, 0 }, // All Star Tennis '99 (U) [f1] (PAL)
        { 0x2D5EFCC598CF79D2, 4, 0 }, // Mario Artist: Polygon Studio (J) [CART HACK]
        { 0x2DC4FFCCC8FF5A21, 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (E) [!]
        { 0x2DCFCA608354B147, 1, RUMBLE_PAK }, // Yoshi Story (J) [!]
        { 0x2DD07E2024D40CD6, 4, 0 }, // TRSI Intro by Ayatollah (POM '99) (PD)
        { 0x2E0E7749B8B49D59, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (E) (M4) [!]
        { 0x2E341C3D5AC8DEA3, 4, 0 }, // All Star Tennis '99 (E) (M5) [f3] (NTSC)
        { 0x2E3593393FA5EDA6, 1, RUMBLE_PAK }, // Chopper Attack (E) [b1]
        { 0x2E7E893C4E68B642, 0, 0 }, // Absolute Crap #2 by Lem (PD) [b2]
        { 0x2E955ECDF3000884, 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (U) [o1][b1]
        { 0x2EF4D519C64A0C5E, 2, MEMORY_PAK | RUMBLE_PAK }, // Snow Speeder (J) [b1]
        { 0x2F493DD02E64DFD9, 1, RUMBLE_PAK }, // Resident Evil 2 (U) (V1.0) [!]
        { 0x2F57C9F7F1E29CA6, 4, 0 }, // Vivid Dolls (Aleck64)
        { 0x2F67DC5974AAD9F1, 4, 0 }, // Ghemor - CD64 Xfer & Save Util (CommsLink) by CrowTRobo (PD)
        { 0x2F7009DDFC3BAC53, 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (E) (V1.0) [h1C]
        { 0x2F700DCD176CC5C9, 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.2) [!]
        { 0x2F70F10D5C4187FF, 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) [o1]
        { 0x2FC5C34C7A05CC9D, 4, MEMORY_PAK }, // Hyper Olympics in Nagano 64 (J) [o1]
        { 0x2FDAA221A588A7CE, 2, MEMORY_PAK }, // Virtual Chess 64 (E) (M6) [o1][h1C]
        { 0x2FE62885F6853DAF, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [f1] (PAL)
        { 0x2FEB152B9C205EA5, 4, RUMBLE_PAK }, // Chou Snobow Kids (J) [f1] (PAL)
        { 0x3015A8AD8CEAF5F8, 4, 0 }, // All Star Tennis '99 (E) (M5) [f1] (NTSC)
        { 0x305AB328F72F632A, 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [t1] (Boost)
        { 0x306B337505F4E698, 1, 0 }, // Alleycat 64 by Dosin (POM '99) (PD) [b1]
        { 0x308BC746A229B775, 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [f1]
        { 0x308DFEC8CE2EB5F6, 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [b1]
        { 0x30B6A6AB319EFD09, 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (U) (M2) [f1] (PAL)
        { 0x30C7AC507704072D, 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) [T+Fre1.3_Corrigo]
        { 0x30E6FE793EEA5386, 0, 0 }, // Mandelbrot Zoomer by RedBox (PD)
        { 0x30EAD54F31620BF6, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz - Special Edition (U) [!]
        { 0x31100ADC86E97D63, 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (J) [t1]
        { 0x312E2FC5CB76AB4D, 4, RUMBLE_PAK }, // Cruis'n World (E) [b1]
        { 0x315C74663A453265, 4, 0 }, // Star Soldier - Vanishing Earth Arcade (Aleck64)
        { 0x31739C69A99E3CB2, 4, RUMBLE_PAK }, // Donkey Kong 64 - Tag Anywhere (V5) (U)
        { 0x319093EC0FC209EF, 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (U) [b2]
        { 0x31C41A8B544057AE, 4, 0 }, // UltraMSX2 V1.0 by Jos Kwanten (PD) [f1] (V64)
        { 0x31E33A962744267B, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (G) [f1] (NTSC)
        { 0x322EF54038D316EB, 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (U) [b3]
        { 0x3261D42970AF148E, 2, MEMORY_PAK }, // Fox Sports College Hoops '99 (U) [f1] (PAL)
        { 0x3261D479ED0DBC25, 2, MEMORY_PAK }, // Fox Sports College Hoops '99 (U) [!]
        { 0x32A92961DB34982C, 4, 0 }, // R.I.P. Jay Demo by Ste (PD)
        { 0x32CA974BB2C29C50, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (F) [!]
        { 0x32EFC7CBC3EA3F20, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (U) [T+Ita_Cattivik66]
        { 0x33487563AC0AE62A, 1, RUMBLE_PAK }, // Dezaemon 3D (J) [t1]
        { 0x336364A006C8D5BF, 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (E) (M2) (Eng-Ger) [!]
        { 0x337F24EE73A807D9, 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack - Covert Operations (U) [f1] (PAL)
        { 0x33828616933456E0, 2, MEMORY_PAK | RUMBLE_PAK }, // Space Invaders (U) [t1]
        { 0x33A275A4B8504459, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF - War Zone (E) [!]
        { 0x33BE8CD6EC186912, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Mayhem (U) [!]
        { 0x33E6115397A2E2C5, 4, RUMBLE_PAK }, // Cruis'n World (E) (V1.1) [!]
        { 0x3409CEB6CEC8517A, 4, RUMBLE_PAK }, // Super Smash Bros. (U) [f3]
        { 0x34495BAD502E9D26, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.0) [o1]
        { 0x34AEB154A4253B86, 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) [T+Spa1.1_Blade133bo]
        { 0x34B493C907654185, 4, 0 }, // View N64 Test Program (PD)
        { 0x34DBB6CFEA06789D, 1, 0 }, // Earthworm Jim 3D (U) [T+Rus]
        { 0x34E42466F50868AC, 4, 0 }, // ObjectVIEWER V1.1 by Kid Stardust (PD)
        { 0x356130AD29367E56, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Pro 99 (E) [f1] (NTSC)
        { 0x35D13E0DE8652252, 4, 0 }, // All Star Tennis '99 (U) [f2] (PAL)
        { 0x35D9BA0CDF485586, 4, MEMORY_PAK | RUMBLE_PAK }, // Rally '99 (J) [!]
        { 0x35E811F399792724, 2, MEMORY_PAK | RUMBLE_PAK }, // Susume! Taisen Puzzle Dama Toukon! Marumata Chou (J) [o2][b1]
        { 0x35F51C8E39EF72B8, 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack - Covert Operations (U) [t1]
        { 0x35F533830C8D0AE4, 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [h1C]
        { 0x35FF8F1A6E79E3BE, 2, MEMORY_PAK | RUMBLE_PAK }, // Hiryuu no Ken Twin (J) [h3C]
        { 0x361E12887A26BE5F, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (U) (V1.0) [t1]
        { 0x36281F23009756CF, 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [b2]
        { 0x364C4ADCD3050993, 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [f1] (NTSC100%)
        { 0x369B314BE7780D04, 1, RUMBLE_PAK }, // Yoshi Story (J) [t1]
        { 0x36ACBA9BF28D4D94, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [b3]
        { 0x36F1C74BF2029939, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (E) [!]
        { 0x36F22FBF318912F2, 1, 0 }, // 64 Hanafuda - Tenshi no Yakusoku (J) [!]
        { 0x36FA35EBE85E2E36, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2001 (U) [!]
        { 0x373F58899A6CA80A, 4, RUMBLE_PAK }, // Conker's Bad Fur Day (E) [!]
        { 0x37463412EAC5388D, 2, 0 }, // Space Dynamites (J) [b1]
        { 0x37955E65C6F2B7B3, 4, 0 }, // Tamiya Racing 64 (U) (Prototype)
        { 0x37B521133A77A655, 2, MEMORY_PAK | TRANSFER_PAK }, // PD Ultraman Battle Collection 64 (J) [f1] (PAL)
        { 0x37B8F920A58BB3EF, 4, 0 }, // Twintris by Twinsen (POM '98) (PD)
        { 0x37BDF22892CF779C, 4, 0 }, // Cube Demo by Msftug (PD)
        { 0x37FA8F165F824D37, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (E) (Beta) (2000-06-06)
        { 0x38112EC7E91410C3, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mickey's Speedway USA (U) [t1]
        { 0x38186DAF232AE3D8, 1, 0 }, // Pokemon Snap (U) [f2] (Save-PAL)
        { 0x3844263466B3F060, 2, RUMBLE_PAK }, // F-1 World Grand Prix (G) [o1][h1C]
        { 0x38D637C15F05D28F, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.0) [f2] (PAL)
        { 0x3911987207722E9F, 1, 0 }, // Pokemon Snap Station (U) [!]
        { 0x3918834A15B50C29, 2, MEMORY_PAK | RUMBLE_PAK }, // Razor Freestyle Scooter (U) [!]
        { 0x3925D6258C83C75E, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (E) [h1C]
        { 0x392A0C42B790E77D, 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (U) [!]
        { 0x395E91DF441D95C5, 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [b4]
        { 0x396F5ADD6693ECA7, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Backstage Assault (U) [!]
        { 0x3983D1A92004158C, 4, RUMBLE_PAK }, // F-ZERO X (U) (DXP Track Pack Hack)
        { 0x399B9B81D533AD11, 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G XG2 (J) [!]
        { 0x39CE39C104271200, 4, MEMORY_PAK | RUMBLE_PAK }, // Asteroids Hyper 64 (U) [o1][f1] (PAL)
        { 0x39F60C11AB2EBA7D, 2, MEMORY_PAK }, // Power Rangers - Lightspeed Rescue (E) [!]
        { 0x3A03E50FE78D1CA7, 4, RUMBLE_PAK }, // GoldenEye 007 (U) [T+Spa2.0_Sogun&IlDucci]
        { 0x3A089BBC54AB2C06, 4, MEMORY_PAK }, // Berney Must Die! by Nop_ (POM '99) (PD)
        { 0x3A180FF45C8E8AF7, 4, MEMORY_PAK | RUMBLE_PAK }, // Chou Kuukan Night Pro Yakyuu King 2 (J) [!]
        { 0x3A3EB7FB0DDD515C, 4, TRANSFER_PAK }, // Mario Artist: Paint Studio (J) [CART HACK]
        { 0x3A3FA5199595CD82, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis 64 (J) [f1] (Country Check)
        { 0x3A4760B52D74D410, 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (U) [b2]
        { 0x3A4B5938A26F989D, 1, MEMORY_PAK }, // Wonder Project J2 - Koruro no Mori no Jozet (J) [T-Eng0.05]
        { 0x3A595B0EF8788AA6, 4, MEMORY_PAK }, // Baku Bomberman (J) [t1]
        { 0x3A6C42B51ACADA1B, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis 64 (J) [!]
        { 0x3A6F8C6B2897BAEB, 1, RUMBLE_PAK }, // Indiana Jones and the Infernal Machine (E) (Unreleased)
        { 0x3AE679E21A3C42DB, 1, 0 }, // Heiwa Pachinko World 64 (J) [T+Eng1.0_Zoinkity]
        { 0x3B18F4F782BFB2B3, 4, 0 }, // Neon64 First Public Beta Release by Halley's Comet Software (PD)
        { 0x3B18F6F98C4BE567, 4, 0 }, // Neon64 First Public Beta Release V2 by Halley's Comet Software (PD)
        { 0x3B8E7E016AE876A8, 4, 0 }, // SRAM Upload Tool V1 by LaC (PD)
        { 0x3B941695F90A5EEB, 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (E) (M3) [!]
        { 0x3BA7CDDC464E52A0, 1, RUMBLE_PAK }, // Mario Story (J) [!]
        { 0x3BCCA16E0AF6D30F, 4, MEMORY_PAK }, // Jikkyou J.League Perfect Striker (J) [f1] (PAL)
        { 0x3BCCD2DB953FA1F5, 4, 0 }, // GameBooster 64 V1.1 (PAL) (Unl) [f1] (Sound)
        { 0x3C059038C8BF2182, 2, RUMBLE_PAK }, // V-Rally Edition 99 (U) [!]
        { 0x3C084040081B060C, 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 6 (J) (V1.1) [!]
        { 0x3C0A802D354A3458, 4, 0 }, // Neon64 GS V1.2 (Pro Action Replay Version) (PD)
        { 0x3C0A802D354A3470, 4, 0 }, // Neon64 GS V1.2a (Pro Action Replay Version) (PD)
        { 0x3C0A8030354A4E78, 4, 0 }, // Neon64 GS V1.1 (Gameshark Version) (PD)
        { 0x3C0A8030354A5198, 4, 0 }, // Neon64 GS V1.2 (Gameshark Version) (PD)
        { 0x3C0A8030354A51B0, 4, 0 }, // Neon64 GS V1.2a (Gameshark Version) (PD)
        { 0x3C1FDABE02A4E0BA, 2, 0 }, // Tetrisphere (U) [b1]
        { 0x3C2AC7FA72BC3A5F, 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [f1] (PAL)
        { 0x3C2B07FC8C6AC97B, 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [t1]
        { 0x3C524346E4ABE776, 4, 0 }, // SRAM Upload Tool V1.1 by Lac (PD)
        { 0x3C7AEE0556099CB6, 1, 0 }, // Dexanoid R1 by Protest Design (PD) [f2] (PAL)
        { 0x3CB8AAB805C8E573, 4, 0 }, // Pip's RPGs Beta 12 by Mr. Pips (PD)
        { 0x3CC7715021CDB987, 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (E) [!]
        { 0x3CECBCB86126BF07, 2, MEMORY_PAK | RUMBLE_PAK }, // F1 Racing Championship (E) (M5) [!]
        { 0x3D02989BD4A381E2, 1, RUMBLE_PAK }, // Star Wars Episode I - Battle for Naboo (U) [b1]
        { 0x3D6044AB46768D77, 2, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz (U) [f1] (PAL)
        { 0x3D615CF56984930A, 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx (U) [t1]
        { 0x3D67C62B31D03150, 4, MEMORY_PAK }, // Let's Smash Tennis (J) [!]
        { 0x3D81FB3EBD843E34, 1, 0 }, // Legend of Zelda, The - Ocarina of Time (Ch) (Traditional) (iQue) [!]
        { 0x3DBD5E85EA9A328A, 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.0) [f1] (PAL)
        { 0x3DF17480193DED5A, 1, MEMORY_PAK }, // Donald Duck - Quack Attack (E) (M5) [!]
        { 0x3E198D9EF2E1267E, 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [!]
        { 0x3E5055B62E92DA52, 4, MEMORY_PAK }, // Mario Kart 64 (U) [o2]
        { 0x3E5D67559AE4BD3B, 4, 0 }, // Explode Demo by NaN (PD)
        { 0x3E6250B212F5155C, 1, MEMORY_PAK | RUMBLE_PAK }, // Spider-Man (U) [t1]
        { 0x3E70E8664438BAE8, 1, 0 }, // Heiwa Pachinko World 64 (J) [o1]
        { 0x3E93A24FAC2D58E1, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.10
        { 0x3EB2E6F3062F9EFE, 2, 0 }, // Pokemon Puzzle League (F) (VC) [!]
        { 0x3EDC7E12E26C1CC9, 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (U) [!]
        { 0x3F16568F37D93603, 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [f1] (PAL)
        { 0x3F245305FC0B74AA, 1, RUMBLE_PAK }, // Pikachu Genki Dechu (J) [b1]
        { 0x3F462D4293AE9238, 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [t2][b1]
        { 0x3F47EEA05DDF7A3C, 2, MEMORY_PAK }, // AeroGauge (J) (V1.0) (Kiosk Demo) [b4]
        { 0x3F4AFB7B2D24C8F8, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [t2]
        { 0x3F66A9D99BCB5B00, 2, RUMBLE_PAK }, // Defi au Tetris Magique (F) [!]
        { 0x3F84EB47141964BF, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [t3]
        { 0x3FC42B3ECED602E6, 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.1) [b1][f1]
        { 0x3FEA56207456DB40, 4, MEMORY_PAK }, // Jikkyou World Soccer - World Cup France '98 (J) (V1.0) [!]
        { 0x3FF65EDD0F8158B0, 1, 0 }, // Pokemon Snap (U) [f3]
        { 0x3FFE80F4A7C15F7E, 4, MEMORY_PAK }, // NBA Showtime - NBA on NBC (U) [!]
        { 0x400668839375D98F, 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (Beta) (2000-02-10)
        { 0x4072572DA23DB72C, 4, 0 }, // HiRes CFB Demo (PD)
        { 0x40CA1EC98CC292D4, 4, MEMORY_PAK }, // Mario Kart 64 (U) [T+Spa1.1_Blade133bo]
        { 0x40D5D2DE78489F0F, 2, MEMORY_PAK }, // Rugrats in Paris - The Movie (U) [T+Spa0.10]
        { 0x40DFEDA03AA09CB2, 4, 0 }, // Dragon Sword 64 (E) (Prototype)
        { 0x412D7090CEDABAC3, 1, 0 }, // Pocket Monsters Snap (J) [f2] (GameShark)
        { 0x412E02B851A57E8E, 4, MEMORY_PAK | RUMBLE_PAK }, // Puzzle Bobble 64 (J) [f2] (PAL)
        { 0x41380792A167E045, 2, MEMORY_PAK | RUMBLE_PAK }, // Polaris SnoCross (U) [!]
        { 0x4147B09163251060, 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.1) [o1]
        { 0x417DD4F41B482FE2, 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [h1C]
        { 0x417DD7F4C95B6E9E, 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [t2] (Hit Anywhere)
        { 0x418BDA98248A0F58, 1, 0 }, // Mischief Makers (E) [b1]
        { 0x4191BA8041C5E21A, 4, RUMBLE_PAK }, // Chameleon Twist (E) [f1] (NTSC)
        { 0x41B1BF58A1EB9BB7, 4, 0 }, // Textlight Demo by Horizon64 (PD)
        { 0x41B25DC41B726786, 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o7][T+Ita_cattivik66]
        { 0x41D440095E94483F, 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (U) [f2] (PAL)
        { 0x41F2B98FB458B466, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (U) (V1.1) [!]
        { 0x42011E1BE3552DB5, 4, TRANSFER_PAK }, // Pokemon Stadium (G) [!]
        { 0x4222D89FAFE0B637, 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move '99 (U) [!]
        { 0x422D29E1C1FB33F3, 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (U) [f1] (PAL)
        { 0x423E96F4CE88F05B, 1, MEMORY_PAK }, // Doom 64 (U) (V1.1) [!]
        { 0x4248BA8799BE605D, 4, 0 }, // Kuru Kuru Fever (Aleck64)
        { 0x4252A5ADAE6FBF4E, 2, MEMORY_PAK | RUMBLE_PAK }, // Goemon's Great Adventure (U) [!]
        { 0x4264DF23BE28BDF7, 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 2000 (J) (V1.1) [!]
        { 0x42CF5EA39A1334DF, 2, RUMBLE_PAK }, // StarCraft 64 (E) [b1]
        { 0x42EDB4BBDEA42760, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (U) [f1] (PAL)
        { 0x4336202020202020, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.30 (Even Bytes)
        { 0x438CEBDC44C1837C, 4, MEMORY_PAK }, // Golden Nugget 64 (U) [b3][f1] (PAL)
        { 0x438E60263BA24E07, 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 6 (J) (V1.2) [!]
        { 0x439B7E7EC1A1495D, 4, TRANSFER_PAK }, // Pokemon Stadium 2 (G) [!]
        { 0x43D67345A60D5D3D, 4, MEMORY_PAK }, // New Tetris, The (U) [f3] (PAL)
        { 0x43EFB5BBD8C62E2B, 0, 0 }, // Alienstyle Intro by Renderman (PD) [a1]
        { 0x440355A4869CE5F9, 2, MEMORY_PAK }, // AeroGauge (E) (M3) [f1] (NTSC)
        { 0x44127A316D341229, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (U) [f1] (PAL)
        { 0x441768D07D73F24F, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 99 (U) [b1]
        { 0x4434202020202056, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.30 (Odd Bytes)
        { 0x444105330C61FA96, 4, 0 }, // N64probe (Button Test) by MooglyGuy (PD)
        { 0x4445612BF8CC9F8B, 1, MEMORY_PAK }, // Gex 64 - Enter the Gecko (U) [f1] (PAL)
        { 0x4446FDD6E3788208, 1, RUMBLE_PAK }, // Bomberman Hero (U) [b1]
        { 0x44705CED6FDFDE02, 4, 0 }, // The Corporation 1st Intro by i_savant (PD)
        { 0x44853212B1F75C3E, 4, 0 }, // GoldenEye 007 (E) (Citadel Hack)
        { 0x4499548420A5FC5E, 2, MEMORY_PAK }, // Wave Race 64 (J) (V1.1) [!]
        { 0x44C54A0B22C0B63A, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [f2] (PAL)
        { 0x451ACA0F7863BC8A, 4, RUMBLE_PAK }, // Rugrats - Die grosse Schatzsuche (G) [!]
        { 0x4537BDFFD1ECB425, 4, MEMORY_PAK }, // VNES64 V0.1 by Jean-Luc Picard (POM '98) (PD)
        { 0x45627CED28005F9C, 4, 0 }, // Pipendo by Mr. Pips (PD)
        { 0x4578A0553F31815D, 4, MEMORY_PAK }, // Mario Kart 64 (U) [T+Ita1.0_Rulesless]
        { 0x457B9CD909C55352, 2, RUMBLE_PAK }, // Ganbare Goemon - Derodero Douchuu Obake Tenkomori (J) [!]
        { 0x45C59BC3D4C62468, 4, RUMBLE_PAK }, // Mario Party (E) (M3) [T+Spa1.0_PacoChan]
        { 0x46039FB40337822C, 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [!]
        { 0x465035CAC6DE1A09, 4, MEMORY_PAK }, // Mario Kart 64 (U) [t4]
        { 0x4690FB1C4CD56D44, 4, MEMORY_PAK }, // Golden Nugget 64 (U) [b3][h1C]
        { 0x46A3F7AF0F7591D0, 2, RUMBLE_PAK }, // Cruis'n Exotica (U) [!]
        { 0x46C6F185E13841C8, 4, MEMORY_PAK | RUMBLE_PAK }, // Jeremy McGrath Supercross 2000 (U) [f1]
        { 0x46F3248B534F2F61, 2, MEMORY_PAK }, // Virtual Chess 64 (U) (M3) [f1][o1]
        { 0x470112E953C66CB4, 2, MEMORY_PAK | RUMBLE_PAK }, // Xeno Crisis (A)
        { 0x4794B85F3EBD5B68, 4, 0 }, // Summer64 Demo by Lem (PD)
        { 0x4795217F63D42538, 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [f1] (PAL)
        { 0x47F0985329CCE24F, 1, RUMBLE_PAK }, // Space Station Silicon Valley (U) [f1] (PAL)
        { 0x48176863E1829A22, 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (U) [t1]
        { 0x485256FFCF6DF912, 4, RUMBLE_PAK }, // F-ZERO X (U) [T+Por0.99_byftr]
        { 0x486BF335034DCC81, 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (F) [!]
        { 0x4875AF3D9A66D3A2, 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (E) [o1]
        { 0x489C84E64C6E49F9, 4, MEMORY_PAK | RUMBLE_PAK }, // Getter Love!! (J) [b2]
        { 0x49020C8EA000B949, 2, MEMORY_PAK }, // Rakuga Kids (E) [f1] (NTSC)
        { 0x49088A116494957E, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (V1.0) [!]
        { 0x490A59F222BC515E, 1, 0 }, // Super Mario 64 (U) [T+Ita2.0final_beta2_Rulesless]
        { 0x492B9DE8C6CCC81C, 1, 0 }, // Earthworm Jim 3D (E) (M6) [b1]
        { 0x492F4B6104E5146A, 2, MEMORY_PAK }, // Wave Race 64 (U) (V1.1) [!]
        { 0x4998DDBBF7B7AEBC, 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (U) [b1]
        { 0x49D83BB920CC4064, 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [b5]
        { 0x49E46C2D7B1A110C, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Cup (J) [!]
        { 0x4A0CD2F4F1F22FE3, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.11
        { 0x4A1CD153D830AEF8, 2, 0 }, // Pokemon Puzzle League (E) (VC) [!]
        { 0x4A997C74E2087F99, 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (E) [!]
        { 0x4AAAF6ED376428AD, 4, RUMBLE_PAK }, // Jinsei Game 64 (J) [!]
        { 0x4B0313E265657446, 4, 0 }, // Cliffi's Little Intro by Cliffi (POM '99) (PD)
        { 0x4B34DF0B5899C0FE, 1, 0 }, // Pokemon Snap Station (U) [f1]
        { 0x4B4672B92DBE5DE7, 4, RUMBLE_PAK | TRANSFER_PAK }, // Puyo Puyo 4 - Puyo Puyo Party (J) [!]
        { 0x4B548E10BB510803, 4, 0 }, // PC-Engine 64 (POM '99) (PD) [t1]
        { 0x4B629EF499B21D9B, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 98 (E) [o1]
        { 0x4BBBA2E28BF3BBB2, 1, 0 }, // Puzzle Master 64 by Michael Searl (PD)
        { 0x4BCDFF47AAA3AF8F, 1, MEMORY_PAK }, // Castlevania (U) (V1.2) [!]
        { 0x4BD245D44202B322, 4, 0 }, // V64Jr Backup Tool by WT_Riker (PD)
        { 0x4C1150089F124960, 4, TRANSFER_PAK }, // Pokemon Stadium (S) [f1]
        { 0x4C2613234F295E1A, 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (G) [h1C]
        { 0x4C2B343C7249BAFF, 4, MEMORY_PAK | RUMBLE_PAK }, // Rally '99 (J) [f1] (PAL)
        { 0x4C3048192CBE7573, 0, 0 }, // 3DS Model Conversion by Snake (PD) [h1C]
        { 0x4C52BBB2CEAB0F6B, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Air Combat (U) [!]
        { 0x4CBC3B56FDB69B1C, 1, TRANSFER_PAK }, // Mario Artist: Talent Studio (J) [CART HACK]
        { 0x4D0224A51BEB5794, 2, RUMBLE_PAK }, // V-Rally Edition 99 (J) [!]
        { 0x4D2B531847DB5316, 1, 0 }, // Glover (E) (M3) [f1] (NTSC)
        { 0x4D3ADFDA7598FCAE, 4, RUMBLE_PAK }, // Rugrats - Treasure Hunt (E) [h1C]
        { 0x4D3E622E9B828B4E, 4, RUMBLE_PAK }, // F-ZERO X (J) [!]
        { 0x4D486681AB7D9245, 1, 0 }, // Star Wars - Shadows of the Empire (E) [o1]
        { 0x4D79D316E8501B33, 2, RUMBLE_PAK }, // Transformers - Beast Wars Transmetal (U) [!]
        { 0x4D9C55B685E58D87, 4, MEMORY_PAK }, // Mario Kart 64 (U) (Super W00ting Hack)
        { 0x4DB05DC428DBF801, 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [o3]
        { 0x4DD7ED5474F9287D, 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.2) [o1]
        { 0x4DEC9986A8904450, 4, 0 }, // SRAM Manager V1.0 PAL Beta (PD)
        { 0x4E01B4A6C884D085, 4, 0 }, // Nintro64 Demo by Lem (POM '98) (PD)
        { 0x4E17004A259E946C, 4, RUMBLE_PAK }, // RR64 - Ridge Racer 64 (U) [f1] (PAL)
        { 0x4E30BD3AB77D6DFB, 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [f2]
        { 0x4E4A7643A37439D7, 2, MEMORY_PAK }, // Virtual Pool 64 (U) [o1]
        { 0x4E4B06401B49BCFB, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (U) (V1.0) [!]
        { 0x4E5507F2E7D3B413, 4, 0 }, // SRAM Manager V2.0 (PD)
        { 0x4E69B487FE18E290, 4, MEMORY_PAK }, // NBA Hangtime (U) [o1][f1]
        { 0x4E7DE4EF0DEC3712, 4, 0 }, // UltraMSX2 V1.0 by Jos Kwanten (PD)
        { 0x4EAA3D0E74757C24, 1, 0 }, // Super Mario 64 (J) [h1C]
        { 0x4EBFDD33664C9D84, 1, 0 }, // Tigger's Honey Hunt (U) [!]
        { 0x4ECEB1DE552B6E88, 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [t1]
        { 0x4EF87A50975A7CC7, 2, 0 }, // Tetrisphere (U) [t1]
        { 0x4F1E88F74A5A3F96, 1, MEMORY_PAK }, // Wonder Project J2 - Koruro no Mori no Jozet (J) [T+Eng1.0_Ryu]
        { 0x4F29474F30CB707A, 2, MEMORY_PAK | RUMBLE_PAK }, // Kakutou Denshou - F-Cup Maniax (J) [!]
        { 0x4F354F88A75E40E0, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.21
        { 0x4F55D05C0CD66C91, 4, 0 }, // Virtual Springfield Site Intro by Presten (PD)
        { 0x4F7075832D6326AA, 4, 0 }, // Neon64 First Public Beta Release V3 by Halley's Comet Software (PD)
        { 0x4F8AA5EC1E6E72DE, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2000 (U) [f1] (PAL)
        { 0x4F8AFC3AF7912DF2, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (E) [!]
        { 0x4F8B1784763A4E04, 4, MEMORY_PAK | RUMBLE_PAK }, // Quake II (U) [f1] (PAL)
        { 0x4FAFC64BB7DBE63A, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [f1] (PAL)
        { 0x4FBFA4296920BB15, 4, MEMORY_PAK }, // J.League Eleven Beat 1997 (J) [h1C]
        { 0x4FF5976FACF559D8, 1, 0 }, // Pokemon Snap (E) [!]
        { 0x5001CF4FF30CB3BD, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis (U) [!]
        { 0x503EA760E1300E96, 2, MEMORY_PAK }, // Cruis'n USA (E) [!]
        { 0x504ABA0A269AA7F4, 4, 0 }, // UltraMSX2 V1.0 w-F1 Spirit by Jos Kwanten (PD)
        { 0x5129B6DA9DEF3C8C, 2, MEMORY_PAK }, // Namco Museum 64 (U) [o1]
        { 0x514B6900B4B19881, 4, RUMBLE_PAK }, // Banjo to Kazooie no Daibouken 2 (J) [!]
        { 0x5168D520CA5FCD0D, 1, RUMBLE_PAK }, // Banjo to Kazooie no Daibouken (J) [!]
        { 0x51938192BD4A98A1, 1, 0 }, // Glover (U) [t1]
        { 0x519EA4E1EB7584E8, 2, MEMORY_PAK | RUMBLE_PAK }, // King Hill 64 - Extreme Snowboarding (J) [b2]
        { 0x51D29418D5B46AE3, 4, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush 2049 (E) (M6) [!]
        { 0x522409D39340EBFF, 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [t2]
        { 0x5242DFBC2FDE6D42, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T+Ita2.0_Rulesless]
        { 0x5243B915BE7743B1, 4, 0 }, // PGA European Tour (E) (M5) [f1] (NTSC)
        { 0x529AA985A85509A3, 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (U) [f1] (Country Check)
        { 0x52A3CF474EC13BFC, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 99 (E) [b1]
        { 0x52BA5D2A9BE3AB78, 4, 0 }, // SRAM to DS1 Tool by WT_Riker (PD)
        { 0x52F22511B9D85F75, 4, 0 }, // Eurasia Intro by Ste (PD) [b1]
        { 0x52F788058B8FCAB7, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (U) [o1]
        { 0x5306CF45CBC49250, 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.1) [!]
        { 0x5318786C994249C6, 1, MEMORY_PAK }, // Doom 64: Complete Edition (Doom 64 Hack)
        { 0x5326696FFE9A99C3, 1, RUMBLE_PAK }, // Body Harvest (U) [b1]
        { 0x5354631C03A2DEF0, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [T-Rus0.50_Alex]
        { 0x53575A7554573214, 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (U) [f2] (PAL)
        { 0x535DF3E2609789F1, 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [o1]
        { 0x53CCAD28AEA6EDA2, 2, MEMORY_PAK | RUMBLE_PAK }, // F1 Racing Championship (B) (M2) [!]
        { 0x53D440E77519B011, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [o2]
        { 0x53D93EA2B88836C6, 4, MEMORY_PAK | RUMBLE_PAK }, // Puzzle Bobble 64 (J) [f1] (PAL)
        { 0x53ED2DC406258002, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (E) (M3) [!]
        { 0x5402C27E60021F86, 1, 0 }, // Sporting Clays by Charles Doty (PD) [a1]
        { 0x540F359DFF327745, 4, RUMBLE_PAK }, // GoldenEye 007 (E) [t1] (Rapid Fire)
        { 0x54310E7D6B5430D8, 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (E) [!]
        { 0x5446C6EFE18E47BB, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2001 (U) [!]
        { 0x544E3629875BDD4A, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (V1.0) [t3]
        { 0x54554A42E4985FFB, 4, MEMORY_PAK }, // J.League Live 64 (J) [b1]
        { 0x549EA1B0F666F794, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.08
        { 0x551C7F439149831C, 4, MEMORY_PAK }, // Shin Nihon Pro Wrestling - Toukon Road 2 - The Next Generation (J) [!]
        { 0x551E06B402CD16A8, 4, 0 }, // Z64 BIOS V1.07
        { 0x5535972EBD8E3295, 1, MEMORY_PAK }, // Human Grand Prix - New Generation (J) [o1]
        { 0x5573C64D905B49FA, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [t1]
        { 0x559A0FD3897F777C, 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [t1]
        { 0x55C0515C901DA4A7, 4, 0 }, // Doraemon - Nobita to 3tsu no Seireiseki (J) [t1]
        { 0x55D4C4CE7753C78A, 4, MEMORY_PAK | RUMBLE_PAK }, // Battlezone - Rise of the Black Dogs (U) [!]
        { 0x5638C7155EA6181D, 4, MEMORY_PAK | RUMBLE_PAK }, // Asteroids Hyper 64 (U) [o1][t1]
        { 0x56563C89C803F77B, 4, 0 }, // Pip's RPGs Beta 14 by Mr. Pips (PD)
        { 0x568E02F5E212A80C, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.1) [T+Ita1.0_Rulesless]
        { 0x569433ADF7E13561, 1, MEMORY_PAK }, // Scooby-Doo! - Classic Creep Capers (U) (V1.1) [!]
        { 0x56E0692210F665F6, 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (E) [T+Spa2.0_IlDucci]
        { 0x5753720D2A8A884D, 1, 0 }, // Pokemon Snap (G) [!]
        { 0x575CCC2EA8B5ACE0, 2, MEMORY_PAK }, // Robotron 64 (U) [t1]
        { 0x575F340B9F1398B2, 4, MEMORY_PAK }, // Jikkyou G1 Stable (J) (V1.1) [!]
        { 0x57AF893E884A377C, 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [T+Eng1.0_Zoinkity]
        { 0x57AF893E8EEF209A, 2, MEMORY_PAK | RUMBLE_PAK }, // Wave Race 64 - Shindou Edition (J) (V1.2) [T+Eng1.1_Zoinkity]
        { 0x57BFF74DDE747743, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (E) [o1]
        { 0x57CD5D6D046282B1, 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.0) [f2]
        { 0x57D1CEA5E2CCC89F, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.08
        { 0x57F81C9B1133FA35, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 99 (U) (M5) [b3]
        { 0x580162ECE3108BF1, 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ger) [!]
        { 0x581900E58E662FC8, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (E) [f1] (NTSC)
        { 0x58886DD0AD93BCB1, 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [f4] (PAL)
        { 0x588F980C63FF7803, 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [t2]
        { 0x58B5EA6227177133, 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [t2] (No Damage-Unbreakable Wings)
        { 0x58FBD28FFEB595F0, 2, TRANSFER_PAK }, // Pocket Monsters Stadium (J) [f1] (Boot-PAL)
        { 0x58FD3F25D92EAA8D, 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [b1][f2] (NTSC)
        { 0x591A806EA5E6921D, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL 99 (U) [!]
        { 0x593BD58BC330786C, 4, RUMBLE_PAK }, // Super Smash Bros. (E) (M3) [f1]
        { 0x59445C711F1FE699, 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (E) (M3) [t1]
        { 0x596E145BF7D9879F, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.1) [!]
        { 0x5972E751228381B3, 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [t1]
        { 0x59BEB6F9E6DC1630, 2, 0 }, // War Gods (U) [t1] (God Mode)
        { 0x59F345580C3130E4, 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (E) [f1]
        { 0x5A160336BC7B37B0, 4, MEMORY_PAK }, // Bomberman 64 (E) [h1C]
        { 0x5A98A468AB27C89D, 4, RUMBLE_PAK }, // RR64 - Ridge Racer 64 (U) [t1]
        { 0x5A9D385997AAE710, 4, 0 }, // Wayne Gretzky's 3D Hockey '98 (U) [h2C]
        { 0x5AAD4E366625D1D2, 4, 0 }, // Neon64 V1.2a by Halley's Comet Software (PD) [o1]
        { 0x5AC383E1D712E387, 4, 0 }, // Monopoly (U) [!]
        { 0x5AEB33A323063A25, 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [t1]
        { 0x5B5DA9FB11EEE5E5, 1, 0 }, // Super Mario 64 (U) (No Cap Hack)
        { 0x5B652BFF8FA82E35, 1, 0 }, // Super Mario 64 (U) [t2] (Speed)
        { 0x5B6AC01B8D1A562A, 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (U) [t1][f1] (PAL-NTSC)
        { 0x5B7558426CA39C7A, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2000 (U) (V1.1) [!]
        { 0x5B904BAE4CA06CC9, 4, RUMBLE_PAK }, // Mario Party (U) [f1] (PAL)
        { 0x5B9A96184B86828E, 1, RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 - Shiokaze ni Notte (J) [b1]
        { 0x5B9B16181B43C649, 1, RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 - Shiokaze ni Notte (J) [!]
        { 0x5B9D65DFA18AB4AE, 4, 0 }, // CZN Module Player (PD)
        { 0x5BBE6E34088B6D0E, 1, 0 }, // SLiDeS (PD)
        { 0x5BC51145D3A29FE7, 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant's NBA Courtside (U) [f1]
        { 0x5BF45B7B596BEEE8, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (E) [!]
        { 0x5C1AAD1CAF7BF297, 4, 0 }, // GameBooster 64 V1.1 (NTSC) (Unl) [f1]
        { 0x5C1B5FBD7E961634, 4, MEMORY_PAK }, // Hexen (F) [!]
        { 0x5C277FBAE8A351C9, 1, 0 }, // SM64 Splitscreen Multiplayer Beta
        { 0x5C4503804B5CB6E6, 4, 0 }, // 1964 Demo by Steb (PD)
        { 0x5C4EB1C912A49603, 2, RUMBLE_PAK }, // AeroFighters Assault (U) [f1] (PAL)
        { 0x5C8BD40763F71E2A, 1, 0 }, // Super Mario 64 (U) [t1] (Invincible)
        { 0x5C9191D6B30AC306, 2, MEMORY_PAK }, // Wave Race 64 (J) [!]
        { 0x5CABD8916229F6CE, 4, 0 }, // Fish Demo by NaN (PD)
        { 0x5CD4150B470CC2F1, 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G XG2 (U) [!]
        { 0x5D0283F52CDEEA76, 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ita) [f2] (NTSC)
        { 0x5D0F8DD2990BE538, 4, 0 }, // GBlator for PAL Dr V64 (PD)
        { 0x5D1795D2C0D72338, 4, 0 }, // GBlator for CD64 (PD) [f1] (V64-PAL)
        { 0x5D391A4084D7A637, 4, 0 }, // Heavy 64 Demo by Destop (PD)
        { 0x5D6AA6EA657352B1, 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (U) [f1] (PAL)
        { 0x5D8B922647605A2A, 4, 0 }, // GBlator for CD64 (PD)
        { 0x5DDB495CCE6E5573, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [b2]
        { 0x5DFC424999529C07, 3, MEMORY_PAK | RUMBLE_PAK }, // Rampage 2 - Universal Tour (E) [!]
        { 0x5E0C3DF78BA027D9, 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (U) [t3]
        { 0x5E23985BE3CF7CDA, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [t3]
        { 0x5E3E60E84AB5D495, 1, MEMORY_PAK }, // Saikyou Habu Shougi (J) [h4C]
        { 0x5E547A4D90E60795, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (U) [h2C]
        { 0x5E6508BBF321C996, 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (E) (M3) (V1.0) [t1]
        { 0x5ECE09AE8230C82D, 4, 0 }, // Tom Demo (PD)
        { 0x5EF4464244D05A25, 2, MEMORY_PAK | RUMBLE_PAK }, // 40 Winks (U) (Unl)
        { 0x5F25B0EE6227C1DB, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 2000 (U) (M4) [!]
        { 0x5F2763C462412AE5, 4, MEMORY_PAK }, // International Superstar Soccer 64 (U) [h2C]
        { 0x5F3F49C60DC714B0, 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (E) [b1]
        { 0x5F44492A18552A0A, 4, 0 }, // Unix SRAM-Upload Utility 1.0 by Madman (PD)
        { 0x5F48A227B97D9B7F, 1, MEMORY_PAK }, // Quest 64 (U) [t1]
        { 0x5F6A04E2D4FA070D, 1, RUMBLE_PAK }, // Mission Impossible (S) [!]
        { 0x5F6DDEA24DD9E759, 1, RUMBLE_PAK }, // Mission Impossible (S) (V1.1) [!]
        { 0x5FD7CDA0D9BB51AD, 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (E) [h1C]
        { 0x60005D1565727EFF, 4, 0 }, // Zelda 64 Boot Emu V2 by Crazy Nation (PD)
        { 0x60006C982605A381, 4, MEMORY_PAK | RUMBLE_PAK }, // Violence Killer - Turok New Generation (J) [b1]
        { 0x60460680305F0E72, 1, RUMBLE_PAK }, // Lode Runner 3-D (E) (M5) [!]
        { 0x60B8BCC92EC4DFB6, 4, 0 }, // Split! 3D Demo by Lem (PD)
        { 0x60C437E5A2251EE3, 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (E) (M3) [!]
        { 0x60CC9644C62AE877, 1, 0 }, // Harvest Moon 64 (U) [t1][f1] (PAL-NTSC)
        { 0x60D0A702432236C6, 4, 0 }, // Game Boy 64 + Super Mario 3 (PD)
        { 0x60D5E10B8BEDED46, 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (E) (M5) [!]
        { 0x60E528A69500D4D3, 4, 0 }, // Nintendo Family by CALi (PD)
        { 0x6107987632DB765E, 4, MEMORY_PAK }, // NBA Jam 99 (U) [f1] (PAL)
        { 0x616B84948A509210, 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant's NBA Courtside (U) [h1C]
        { 0x6192E59D538A78CF, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (U) [t1]
        { 0x61A5FCEEB59FD8D3, 4, 0 }, // NBCG's Kings of Porn Demo (PD)
        { 0x61D116B0FA24D60C, 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (E) (M3) [h1C]
        { 0x61F5B152046122AB, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (J) [b1]
        { 0x62269B3DFE11B1E8, 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (U) [o1]
        { 0x625E08501F1B2327, 4, RUMBLE_PAK }, // GoldenEye 007 (E) [T+Spa2.0_Sogun&IlDucci]
        { 0x626C7AA3C1480B86, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.08 [a1]
        { 0x62E957D07FC15A5D, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (E) [o1][h1C]
        { 0x62F6BE95F102D6D6, 2, RUMBLE_PAK }, // AeroFighters Assault (E) (M3) [o1]
        { 0x6309FC171D4F5EF3, 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.1) [!]
        { 0x630AA37D896BD7DB, 4, MEMORY_PAK | RUMBLE_PAK }, // Destruction Derby 64 (E) (M3) [!]
        { 0x63112A53A29FA88F, 4, MEMORY_PAK | RUMBLE_PAK }, // Jikkyou J.League 1999 - Perfect Striker 2 (J) (V1.0) [b2]
        { 0x635A2BFF8B022326, 1, 0 }, // Super Mario 64 (U) [o1]
        { 0x635A42C5BDC58EDC, 1, 0 }, // Super Wario 64 V1.0 by Rulesless (Super Mario 64 Hack)
        { 0x635CB5C6E9192BFF, 1, RUMBLE_PAK }, // Mission Impossible (S) [f1] (NTSC)
        { 0x636E6B19E57DDC5F, 2, RUMBLE_PAK }, // V-Rally Edition 99 (E) (M3) [!]
        { 0x637758865FB80E7B, 4, TRANSFER_PAK }, // Pocket Monsters Stadium 2 (J) [!]
        { 0x63CCD041F12A8BC0, 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [b1][f1] (PAL)
        { 0x63E7391CE6CCEA33, 2, RUMBLE_PAK }, // Tom and Jerry in Fists of Furry (U) [!]
        { 0x641D3A7F86820466, 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (E) (M3) [b1]
        { 0x6420535A50028062, 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.0) [b3]
        { 0x6440D7CEF6597A03, 4, MEMORY_PAK | RUMBLE_PAK }, // Telefoot Soccer 2000 (F) [f1] (NTSC)
        { 0x6459533B7E22B56C, 4, 0 }, // Pip's Tic Tak Toe by Mark Pips (PD)
        { 0x64709212699BCF3B, 4, 0 }, // Kid Stardust Intro with Sound by Kid Stardust (PD) [a1]
        { 0x64B62D1643A2A592, 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [t1]
        { 0x64BF47C4F4BD22BA, 2, RUMBLE_PAK }, // F-1 World Grand Prix (J) [h1C]
        { 0x64F1084A763C7E91, 4, 0 }, // WideBoy BIOS V980914
        { 0x64F1B7CA71A23755, 1, MEMORY_PAK }, // Castlevania (E) (M3) [b1]
        { 0x650EFA9630DDF9A7, 2, MEMORY_PAK }, // Wave Race 64 (E) (M2) [o1]
        { 0x651F2792E40CC56B, 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 2000 (U) [f1] (PAL)
        { 0x65234451EBD3346F, 1, MEMORY_PAK }, // Blast Dozer (J) [b1]
        { 0x652642177FB64F7F, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (U) (V1.0) [f1] (PAL)
        { 0x6539E5291FE8CE01, 4, 0 }, // NEO Myth N64 Menu Demo V0.1 (PD)
        { 0x655BE9319FB2E4FB, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (U) [t1]
        { 0x656C656173652031, 4, 0 }, // Xplorer64 BIOS V1.067 (G)
        { 0x658E2804443386FA, 1, 0 }, // Super Mario 64 (U) (Silver Mario Hack)
        { 0x658F8F371813D28D, 4, MEMORY_PAK | RUMBLE_PAK }, // RTL World League Soccer 2000 (G) [!]
        { 0x65AEDEEF3857C728, 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.1) (VC) [!]
        { 0x65DB63E364357A65, 4, RUMBLE_PAK }, // Mario Party 3 (U) [f1]
        { 0x65EEE53AED7D733C, 1, RUMBLE_PAK }, // Paper Mario (U) [!]
        { 0x661B45F39ED6266D, 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey '98 (E) (M4) [!]
        { 0x664BA3D4678A80B7, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [b3]
        { 0x6657208028E348E1, 2, RUMBLE_PAK }, // Super Robot Spirits (J) [b1]
        { 0x665E8259D098BD1D, 2, TRANSFER_PAK }, // Pocket Monsters Stadium (J) [b1]
        { 0x665F09DDFC3BAC53, 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (E) (V1.0) [b1]
        { 0x665FC7936934A73B, 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (G) (V1.2) [!]
        { 0x665FD963B5CC6612, 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (G) (V1.0) [!]
        { 0x66751A5754A29D6E, 4, MEMORY_PAK }, // Hexen (J) [!]
        { 0x66807E77EBEA2D76, 4, MEMORY_PAK }, // VNES64 + Mario (PD)
        { 0x668FA3362C67F3AC, 4, 0 }, // Pip's RPGs Beta x (PD) [a1]
        { 0x66A24A1222AF6AC2, 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.0) [h1] (Language Select)
        { 0x66A24BEC2EADD94F, 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.0) [b1]
        { 0x66CF0FFEAD697F9C, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (E) [!]
        { 0x66D8DE56C2AA25B2, 0, 0 }, // Ultrafox 64 by Megahawks (PD)
        { 0x66E4FA0FDE88C7D0, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Legenden des Verlorenen Landes (G) [!]
        { 0x6712C7793B72781D, 4, MEMORY_PAK | RUMBLE_PAK }, // International Track & Field Summer Games (E) (M3) [!]
        { 0x673D099BA4C808DE, 3, MEMORY_PAK | RUMBLE_PAK }, // Rampage 2 - Universal Tour (U) [!]
        { 0x67C3866094038DC6, 2, RUMBLE_PAK }, // F-1 World Grand Prix II (E) (M4) [f1] (NTSC)
        { 0x67D20729F696774C, 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [b3]
        { 0x67D21868C5424061, 2, MEMORY_PAK }, // Rakuga Kids (E) [h1C]
        { 0x67D239A0960F3A1A, 2, MEMORY_PAK | RUMBLE_PAK }, // F1 Racing Championship (E) (M5) [f1] (NTSC)
        { 0x67FF12CC76BF0212, 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [o1]
        { 0x68CE63CC2FFE262C, 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (E) [t1]
        { 0x68D128AE67D60F21, 1, RUMBLE_PAK }, // Densha de Go! 64 (J) (Localization Patch v1.01)
        { 0x68D7A1DE0079834A, 4, RUMBLE_PAK }, // Jet Force Gemini (E) (M4) [T+Ita0.9beta_Rulesless]
        { 0x68E8A8750CE7A486, 4, RUMBLE_PAK }, // WCW-nWo Revenge (E) [h1C]
        { 0x68FCF72649658CBC, 2, MEMORY_PAK | RUMBLE_PAK }, // G.A.S.P!! Fighter's NEXTream (E) [!]
        { 0x69256460B9A3F586, 3, 0 }, // Jeopardy! (U) [o1][h1C]
        { 0x693BA2AEB7F14E9F, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.2) [!]
        { 0x69458B9EFC95F936, 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy-kun no Bura Bura Poyon (J) [o1]
        { 0x69458FFEC9D007AB, 4, 0 }, // Freekworld New Intro by Ste (PD)
        { 0x696F2896764F6D3F, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) [f1]
        { 0x6976748D1BF61F62, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighting Force 64 (U) [t1][f1] (PAL)
        { 0x698FE5E81D57B278, 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (U) [t1]
        { 0x69AE04382C63F3F3, 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.1) [!]
        { 0x6A097D8BF999048C, 4, 0 }, // Mempack to N64 Uploader by Destop V1.0 (PD)
        { 0x6A121930665CC274, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '98 (U) [o1]
        { 0x6A162FF22093704C, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (E) [!]
        { 0x6AA4DDE7E3E2F4E7, 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx (U) [b1]
        { 0x6AECEC4FF0924814, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask - Collector's Edition (E) (M4) (GC) [!]
        { 0x6B2AFCC7EEB27A34, 4, 0 }, // Nintendo On My Mind Demo by Kid Stardust (PD)
        { 0x6B45223FF00E5C56, 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (U) (V1.0) [o1]
        { 0x6B70075029D621FE, 2, 0 }, // Mace - The Dark Age (U) [o1]
        { 0x6BB116452C6F637E, 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [b1]
        { 0x6BCF21E8CAB5AF4F, 1, 0 }, // Harvest Moon 64 (U) [f1] (PAL)
        { 0x6BE878A83DC16D91, 2, 0 }, // Pong by Oman (PD) [t1]
        { 0x6BF11774D330D7ED, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.30
        { 0x6BFF4758E5FF5D5E, 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.0) [o1]
        { 0x6C2C6C499BE5CA66, 4, 0 }, // Taz Express (U) (Prototype)
        { 0x6C45B60CDCE50E30, 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [h6C]
        { 0x6C48FE118E1A27EC, 4, 0 }, // Clay Fighter - Sculptor's Cut (U) [t1]
        { 0x6C80F13B427EDEAA, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (U) (V1.1) [!]
        { 0x6CED60ACC74292DC, 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing - Round 2 (U) [t1]
        { 0x6D03673A1D63C191, 4, 0 }, // N64 Seminar Demo - RSP by ZoRAXE (PD)
        { 0x6D2C07F1C884F0D0, 4, 0 }, // N64 Scene Gallery by CALi (PD)
        { 0x6D452016713C09EE, 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (E) [o1]
        { 0x6D8DF08ED008C3CF, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (E) (V1.0) [b2]
        { 0x6D9D1FE484D10BEA, 4, 0 }, // Eleven Beat - World Tournament (Aleck64)
        { 0x6DF2CB254D4821F8, 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (E) [t1] (Endless Ice)
        { 0x6DFDCDC34DE701C8, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (U) [o1][h1C]
        { 0x6DFF4C37B1B763FD, 4, 0 }, // Famista 64 (J) [o1]
        { 0x6EDA5178D396FEC1, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (G) [!]
        { 0x6EDD4766A93E9BA8, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu - Basic Ban 2001 (J) (V1.0) [!]
        { 0x6F46DA42D971A312, 4, 0 }, // Ronaldinho's Soccer 64 (B) (Unl) (Pirate)
        { 0x6F48FF84754B4549, 2, RUMBLE_PAK }, // V-Rally Edition 99 (U) [f1] (PAL)
        { 0x6F66B92D80B9E520, 1, RUMBLE_PAK }, // Body Harvest (E) (M3) [f1] (NTSC)
        { 0x6FA4B82129561690, 4, 0 }, // Rotating Demo USA by Rene (PD)
        { 0x6FC4EEBC125BF459, 4, 0 }, // V64Jr Flash Save Util by CrowTRobo (PD)
        { 0x6FD0035D371B785D, 4, 0 }, // All Star Tennis '99 (E) (M5) [f2] (NTSC)
        { 0x700694EA636C43D5, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (U) (V1.0) [f1] (PAL)
        { 0x700F6DD9E2EB805F, 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (E) (M5) [b1][f1] (NTSC)
        { 0x7040B0821BE98D03, 4, 0 }, // Neon64 V1.2 by Halley's Comet Software (PD) [o1]
        { 0x70478A35F9897402, 1, 0 }, // Pilotwings 64 (U) [t1]
        { 0x7055FD0AE0A45808, 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (E) (M5) [f2] (NTSC)
        { 0x705AC38AA8D714E8, 1, RUMBLE_PAK }, // Command & Conquer (G) [f1] (Z64)
        { 0x70666F63FEC28A44, 1, RUMBLE_PAK }, // Yoshi Story (J) [f2]
        { 0x70B0260E6716D04C, 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (J) [!]
        { 0x70BF6C4C6F365E21, 2, RUMBLE_PAK }, // Magical Tetris Challenge (E) [f1] (NTSC)
        { 0x71255651C6AE0EA6, 4, 0 }, // N64 Stars Demo (PD)
        { 0x713FDDD372D6A0EF, 1, 0 }, // Bike Race '98 V1.0 by NAN (PD) [b1]
        { 0x714001E32EB04B67, 4, 0 }, // Freekworld BBS Intro by Rene (PD)
        { 0x7188F44584410A68, 2, RUMBLE_PAK }, // Dance Dance Revolution - Disney Dancing Museum (J) [o2]
        { 0x7194B65B9DE67E7D, 4, 0 }, // Pip's RPGs Beta 6 by Mr. Pips (PD)
        { 0x71BE60B91DDBFB3C, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW vs. nWo - World Tour (U) (V1.1) [!]
        { 0x71ED5D1476A110D0, 1, MEMORY_PAK }, // Gex 64 - Enter the Gecko (U) [t1]
        { 0x720278EC594B7975, 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (G) [f1] (NTSC)
        { 0x72611D7D9919BDD2, 4, MEMORY_PAK | RUMBLE_PAK }, // HSV Adventure Racing (A) [b1]
        { 0x726189060EDC784B, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (U) (V1.0) [f2] (PAL)
        { 0x72836ECB75D15FAE, 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (E) [t1] (Hit Anywhere)
        { 0x729B5E32B728D980, 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (U) [!]
        { 0x72F703986556A98B, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [!]
        { 0x73036F3BCE0D69E9, 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (E) [h1C]
        { 0x730D25F44A37A36E, 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [f3] (Nosave)
        { 0x733FCCB1444892F9, 1, RUMBLE_PAK }, // Banjo-Kazooie (E) (M3) [!]
        { 0x734F816BC6A6EB67, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.1) [!]
        { 0x7365D8F89ED9326F, 1, MEMORY_PAK }, // Bokujou Monogatari 2 (J) (V1.1) [!]
        { 0x736657F63C88A702, 1, MEMORY_PAK }, // Bokujou Monogatari 2 (J) (V1.2) [!]
        { 0x736AE6AF4117E9C7, 4, RUMBLE_PAK }, // Mickey no Racing Challenge USA (J) [!]
        { 0x73A88E3D3AC5C571, 4, MEMORY_PAK | RUMBLE_PAK }, // Rally Challenge 2000 (U) [!]
        { 0x73ABB1FB9CCA6093, 4, MEMORY_PAK }, // Penny Racers (U) [!]
        { 0x73C0403DE821D951, 1, TRANSFER_PAK }, // Mario Artist: Talent Studio (J) [CART HACK] [T+Eng0.1_LuigiBlood]
        { 0x7433D9D72C4322D0, 4, MEMORY_PAK | RUMBLE_PAK }, // Quake II (E) [h1C]
        { 0x7435C9BB39763CF4, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (J) [!]
        { 0x74554B3BF4AEBCB5, 1, MEMORY_PAK }, // Pachinko 365 Nichi (J) [b2]
        { 0x74BCA5D34A945F7E, 4, MEMORY_PAK | RUMBLE_PAK }, // Roadsters Trophy (U) (M3) [t1]
        { 0x74E87A706293AED4, 4, MEMORY_PAK | RUMBLE_PAK }, // Roadsters Trophy (E) (M6) [!]
        { 0x752E1BF8E81DF66D, 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (U) [t1]
        { 0x75A0B8934CA321B5, 4, 0 }, // Robotech - Crystal Dreams (Beta) [a1]
        { 0x75A15B072E391683, 1, 0 }, // Super Wario 64 V1.0 by Rulesless (Super Mario 64 Hack) [T+Ita]
        { 0x75A4E2476008963D, 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx - Global Assault (U) [!]
        { 0x75B616477ADABF78, 2, RUMBLE_PAK }, // Magical Tetris Challenge (U) [!]
        { 0x75BC6AD678552BC9, 4, MEMORY_PAK | RUMBLE_PAK }, // Kira to Kaiketsu! 64 Tanteidan (J) [!]
        { 0x75CAA9909FFDC9BD, 4, RUMBLE_PAK }, // Jet Force Gemini (U) [b1]
        { 0x75CF4CCDF47E10F6, 2, RUMBLE_PAK }, // V-Rally Edition 99 (E) (M3) [f1] (NTSC)
        { 0x75FA0E14C9B3D105, 1, MEMORY_PAK }, // Holy Magic Century (G) [b3]
        { 0x75FBDE20A3189B31, 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (Beta)
        { 0x760EF304AD6D6A7C, 4, 0 }, // V64Jr 512M Backup Program by HKPhooey (PD)
        { 0x7613A6303ED696F3, 4, MEMORY_PAK }, // FIFA 99 (U) [!]
        { 0x762C75D1AA09C759, 4, 0 }, // Pip's World Game 1 by Mr. Pips (PD)
        { 0x76515D1142BF7875, 4, 0 }, // PGA European Tour (E) (M5) [f2] (NTSC)
        { 0x7671215935666812, 1, 0 }, // Dexanoid R1 by Protest Design (PD)
        { 0x768AD2AAC3C79B68, 4, RUMBLE_PAK }, // WCW-nWo Revenge (U) [f2] (PAL)
        { 0x769147F32033C10E, 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (U) [!]
        { 0x769D4D13DA233FFE, 4, 0 }, // Dr. Mario 64 (U) [!]
        { 0x76A8C9F43391893D, 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [f5]
        { 0x76AF28D864A07F55, 2, TRANSFER_PAK }, // Pocket Monsters Stadium (J) [f2] (Boot)
        { 0x7711DBA74856F7E0, 1, MEMORY_PAK }, // Zool - Majou Tsukai Densetsu (J) [f1] (PAL)
        { 0x7739A454A2F52A66, 4, 0 }, // U64 (Chrome) Demo by Horizon64 (PD)
        { 0x773E3E8E1E8A0CBE, 4, MEMORY_PAK }, // Wetrix (E) (M6) [t1]
        { 0x775AFA9C0EB52EF6, 4, 0 }, // Hard Coded Demo by Silo and Fractal (PD)
        { 0x776646F606B9AC2B, 4, RUMBLE_PAK }, // F-ZERO X (E) [h1C]
        { 0x7773E2BB71D0A925, 1, 0 }, // Glover (U) [b2]
        { 0x77C44B0F5E8948A3, 1, 0 }, // Super Mario 64 (U) [b2]
        { 0x77DA3B8D162B0D7C, 1, 0 }, // Ide Yousuke no Mahjong Juku (J) [b1]
        { 0x782A9075E552631D, 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Captain Buzz Lightyear auf Rettungsmission! (G) (V1.0) [!]
        { 0x787AECF1CC8147BA, 4, MEMORY_PAK | RUMBLE_PAK }, // Roadsters Trophy (U) (M3) [f1] (PAL)
        { 0x792C3A8E0F2200F7, 4, MEMORY_PAK }, // Wetrix (E) (M6) [f1] (NTSC)
        { 0x79E318E186861726, 4, 0 }, // RPA Site Intro by Lem (PD)
        { 0x7A4747AC44EEEC23, 2, 0 }, // Pokemon Puzzle League (G) (VC) [!]
        { 0x7A6081FCFF8F7A78, 4, RUMBLE_PAK }, // 64 Trump Collection - Alice no Wakuwaku Trump World (J) [h1C]
        { 0x7AA65B36FDCEE5AD, 1, MEMORY_PAK }, // Doom 64 (J) [b1]
        { 0x7AADA8DD94DAB468, 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (U) [t2]
        { 0x7B016055F8AC6092, 4, RUMBLE_PAK }, // Jinsei Game 64 (J) [f1] (PAL)
        { 0x7BB18D4083138559, 1, 0 }, // Pokemon Snap (A) [!]
        { 0x7BB5698FD99BF02B, 1, MEMORY_PAK | RUMBLE_PAK }, // Ganbare Goemon - Neo Momoyama Bakufu no Odori (J) [h1C][t1]
        { 0x7C3829D96E8247CE, 4, RUMBLE_PAK }, // Mario Party 3 (U) [!]
        { 0x7C647C25D9D901E6, 1, MEMORY_PAK }, // Blast Corps (U) (V1.0) [b2]
        { 0x7C647E651948D305, 1, MEMORY_PAK }, // Blast Corps (U) (V1.1) [!]
        { 0x7C64E6DB55B924DB, 1, MEMORY_PAK }, // Blast Corps (E) (M2) [b2]
        { 0x7CD08B121153FF89, 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (U) [!]
        { 0x7D1727F16C6B83EB, 4, 0 }, // Plasma Demo (PD) [a1]
        { 0x7D292963D5277C1F, 4, 0 }, // Light Force First N64 Demo by Fractal (PD)
        { 0x7D5772F83A6785EF, 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (J) [t1]
        { 0x7DE11F5374872F9D, 2, MEMORY_PAK }, // Wave Race 64 (U) (V1.0) [!]
        { 0x7E39984903DAC1CD, 4, 0 }, // F-ZERO X (U) [f1] (Sex V1.0 Hack)
        { 0x7E3D8157D9BF4D91, 4, TRANSFER_PAK }, // Pokemon Stadium (F) [f1]
        { 0x7EAE24889D40A35A, 1, RUMBLE_PAK }, // Biohazard 2 (J) [!]
        { 0x7EC22587EF1AE323, 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (U) [h1C]
        { 0x7ECBE9393C331795, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (U) [b1]
        { 0x7ED67CD4B4415E6D, 2, 0 }, // Dark Rift (E) [b1]
        { 0x7EE0E8BB49E411AA, 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (E) (M3) (V1.0) [!]
        { 0x7EE0E9457D8FEE20, 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (E) (M3) (V1.0) (Language Select Hack)
        { 0x7F0FDA096061CE0B, 4, MEMORY_PAK }, // International Superstar Soccer '98 (U) [o1]
        { 0x7F2D025D2B7DA4AD, 4, 0 }, // Oerjan Intro by Oerjan (POM '99) (PD)
        { 0x7F30409952CF5276, 2, RUMBLE_PAK }, // Last Legion UX (J) [b2]
        { 0x7F3CEB778981030A, 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (U) [o1]
        { 0x7F43E701536328D1, 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally (E) [h1C]
        { 0x7F564549ED60C5E6, 2, TRANSFER_PAK }, // Robot Ponkotsu 64 - 7tsu no Umi no Caramel (J) [f1] (PAL)
        { 0x7F9345D3841ECADE, 2, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja 2 Starring Goemon (E) (M3) [!]
        { 0x7FD0761E2190A9ED, 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (U) [f2] (PAL)
        { 0x80203B9457B7F424, 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (E) [f1] (NTSC)
        { 0x8066D58AC3DECAC1, 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (U) (V1.0) [!]
        { 0x80A2BBB5828DFD3E, 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [b1]
        { 0x80A780805F9F8833, 4, 0 }, // Sydney 2000 (U) (Prototype)
        { 0x80C1C05CEA065EF4, 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (U) [b3]
        { 0x80C8564A929C65AB, 2, RUMBLE_PAK }, // Magical Tetris Challenge Featuring Mickey (J) [!]
        { 0x80F41131384645F6, 2, MEMORY_PAK }, // AeroGauge (J) (V1.1) [b7]
        { 0x810729F6E03FCFC1, 4, MEMORY_PAK }, // NBA Jam 99 (U) [b1]
        { 0x810CED2DD7B3F710, 1, RUMBLE_PAK }, // Body Harvest (U) [T+Spa0.98_jackic]
        { 0x812289D0C2E53296, 2, MEMORY_PAK | RUMBLE_PAK }, // Off Road Challenge (E) [b2]
        { 0x813615322AEB643F, 4, 0 }, // Tetris Beta Demo by FusionMan (POM '98) (PD)
        { 0x817D286AEF417416, 1, 0 }, // Pokemon Snap (S) [!]
        { 0x819D5B33A5096295, 1, 0 }, // Super Mario 64 (U) [T-Ita1.0final_beta1_Rulesless]
        { 0x81A3F478B6965E3E, 4, 0 }, // Mempack Manager for Jr 0.9 by deas (PD)
        { 0x822E09150338FD50, 4, RUMBLE_PAK }, // Mario Party 3 (U) [f2] (PAL)
        { 0x82342320BC65A550, 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (U) [t1]
        { 0x82380387DFC744D9, 4, RUMBLE_PAK }, // Mario Party 2 (E) (M5) [!]
        { 0x8264014A3C9FBFC1, 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.1) [t2] (Boost)
        { 0x827E4890958468DC, 1, RUMBLE_PAK }, // Star Wars - Shutsugeki! Rogue Chuutai (J) [!]
        { 0x82B3248BE73E244D, 2, MEMORY_PAK }, // Virtual Chess 64 (U) [o1]
        { 0x82DC04FDCF2D82F4, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (U) [b1]
        { 0x82EFDC30806A2461, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Blades of Steel '99 (U) [f1] (PAL)
        { 0x832C168B56A2CDAE, 1, MEMORY_PAK | RUMBLE_PAK }, // Ganbare Goemon - Neo Momoyama Bakufu no Odori (J) [h1C]
        { 0x833D1192D3C58762, 2, 0 }, // Mortal Kombat Trilogy (E) [t3] (All Attacks Hurt P2)
        { 0x833D11B61848E72B, 2, 0 }, // Mortal Kombat Trilogy (E) [t2] (All Attacks Hurt P1)
        { 0x834304AE5845CD3A, 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [h2C]
        { 0x834C19FF8A93F7D4, 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [t1] (Boost)
        { 0x835B97A2FF6C0060, 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (E) [t1]
        { 0x839F3AD5406D15FA, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis (E) [!]
        { 0x83CB0B877E325457, 2, 0 }, // Custom Robo (J) [!]
        { 0x83D8A30C8552C117, 4, 0 }, // Yoshi's Story BootEmu (PD)
        { 0x83E7E0D2B5B7E503, 1, RUMBLE_PAK }, // Duck Dodgers Starring Daffy Duck (U) (M3) [t1]
        { 0x83F33AA92649847A, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t1] (Hit Anywhere)
        { 0x83F33AA9A901D40D, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t6] (P1 Invincible)
        { 0x83F3931ECB72223D, 4, RUMBLE_PAK }, // Cruis'n World (E) [!]
        { 0x83F9F2CBE7BC4744, 4, 0 }, // Y2K Demo by WT_Riker (PD)
        { 0x83FB2B697A5D2980, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t5] (2x Aggressor)
        { 0x84067BAC87FBA623, 4, 0 }, // NBCrew 2 Demo (PD)
        { 0x8407727557315B9C, 4, TRANSFER_PAK }, // Pokemon Stadium (E) (V1.0) [!]
        { 0x845B026957DE9502, 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (J) [!]
        { 0x8473D0C123120666, 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (GC) [!]
        { 0x84CDFB729CA8D544, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [f1] (PAL)
        { 0x84D4444867CA19B0, 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (E) [h1C]
        { 0x84D5FD75BBFD3CDF, 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (G) [b2]
        { 0x84EAB557C88A190F, 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater 2 (E) [!]
        { 0x84EBE70F352C5E3F, 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.0) [f1]
        { 0x84F5C897A870AC11, 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (U) [t1]
        { 0x84F5CF6308EE7EDC, 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (U) [hI]
        { 0x84FC04FFB1253CE9, 4, MEMORY_PAK | RUMBLE_PAK }, // Snobow Kids (J) [h2C]
        { 0x8544332577D72F56, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.0) [f1]
        { 0x8547FED3C4945FAF, 4, RUMBLE_PAK }, // Jet Force Gemini (U) (Kiosk Demo) [b1][f2]
        { 0x85670AB43834BE8A, 4, MEMORY_PAK }, // Rat Attack (E) (M6) [f1] (NTSC)
        { 0x85AE781AC756F05D, 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [!]
        { 0x85C18B16DF9622AF, 4, MEMORY_PAK | RUMBLE_PAK }, // 64 Oozumou 2 (J) [!]
        { 0x85C52CA4017EE9A6, 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater (U) (V1.0) [t1]
        { 0x85DB494443ADE940, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [f1] (PAL)
        { 0x85E42D70132BD5C2, 1, MEMORY_PAK }, // Donald Duck - Quack Attack (E) (M5) [b1]
        { 0x85F5A08AEAC7053E, 1, RUMBLE_PAK }, // Paper Mario (U) [T+Chi]
        { 0x861C3519F6091CE5, 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (J) [!]
        { 0x861F9BDCC3353C85, 4, MEMORY_PAK }, // Berney Must Die! by Nop_ (POM '99) (PD) [t1]
        { 0x862C06578DFD896D, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [!]
        { 0x8632071E637E1042, 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [t1]
        { 0x8637BEAA9B0EB296, 4, MEMORY_PAK | RUMBLE_PAK }, // Destruction Derby 64 (U) [t1]
        { 0x8637C692CAD23999, 4, MEMORY_PAK | RUMBLE_PAK }, // Destruction Derby 64 (U) [f1] (PAL)
        { 0x8638A3348EBF1508, 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (E) (M3) [t1] (P1 Untouchable)
        { 0x868214B8AB2BE6B7, 4, 0 }, // DS1 Manager V1.1 by R. Bubba Magillicutty (PD) [T+Ita]
        { 0x86EDC3C1E983A7BF, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [f1] (Save)
        { 0x870611BAD8B1226C, 4, 0 }, // Die Hard 64 (U) (Prototype) (Level 1)
        { 0x874621CB0031C127, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 99 (E) [b1]
        { 0x874733A4A823745A, 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (E) (M2) (Fre-Ger) [!]
        { 0x8776674791C27165, 4, MEMORY_PAK }, // J.League Dynamite Soccer 64 (J) [h3C]
        { 0x88047F1D3B837D28, 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [t1]
        { 0x887C368DD1663AA2, 4, 0 }, // Display List Ate My Mind Demo by Kid Stardust (PD)
        { 0x888EEC7A42361983, 0, 0 }, // Alienstyle Intro by Renderman (PD)
        { 0x88A12FB38A583CBD, 4, 0 }, // HIPTHRUST by MooglyGuy (PD)
        { 0x88BD5A9EE81FDFBF, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 2000 (E) [!]
        { 0x88C7938185786A83, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [t3]
        { 0x88CF980A8ED52EB5, 4, MEMORY_PAK }, // Bakushou Jinsei 64 - Mezase! Resort Ou (J) [h1C]
        { 0x88EC367559CE4583, 1, 0 }, // Super Mario 64 (U) [T+Spa3.9_Blade133bo]
        { 0x88FFAB7D298294A2, 4, RUMBLE_PAK }, // Nintendo All-Star! Dairantou Smash Brothers (J) [f3]
        { 0x89529B120853ED3B, 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [hI]
        { 0x8952E61EAA89984C, 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [f1] (PAL)
        { 0x8954584DEFE71E95, 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (U) [f1] (PAL)
        { 0x8954A6553B38F292, 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (U) [t1]
        { 0x8979169CF189F6A0, 1, RUMBLE_PAK }, // Dezaemon 3D (J) [b2]
        { 0x89A498AEDE3CD49A, 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (E) [f1] (NTSC)
        { 0x89A579F1667E97EF, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (U) [!]
        { 0x89FED774CAAFE21B, 1, MEMORY_PAK }, // Gex 64 - Enter the Gecko (U) [!]
        { 0x8A155D1553C1DB8C, 4, 0 }, // Action Replay Pro 64 V3.0 (Unl) [f1]
        { 0x8A2E80DC00F61BE4, 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx - Global Assault (U) [f1] (Country Check)
        { 0x8A5B90180A661D8F, 1, MEMORY_PAK }, // Donald Duck - Quack Attack (E) (M5) [f1] (NTSC)
        { 0x8A6009B694ACE150, 4, RUMBLE_PAK }, // Jet Force Gemini (U) [!]
        { 0x8A86F073CD45E54B, 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (U) [b4]
        { 0x8A8E474B6458BF2B, 4, 0 }, // Liner V1.00 by Colin Phillipps of Memir (PD) [b2]
        { 0x8A97A197272DF6C1, 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (E) (M2) [h1C]
        { 0x8ACE66833FBA426E, 4, MEMORY_PAK }, // Chou Kuukan Night Pro Yakyuu King (J) [h1C]
        { 0x8AFB2D9A9F186C02, 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move '99 (U) [b1]
        { 0x8B2FCAD3361B412C, 1, RUMBLE_PAK }, // Banjo-Kazooie (E) [T+Spa1.1_PacoChan]
        { 0x8B31F4ADF366CBFA, 4, RUMBLE_PAK }, // Lylat Wars (A) (M3) [t2] (No Damage-Unbreakable Wings)
        { 0x8B79DBFD0BB32AC0, 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (U) [f1] (PAL)
        { 0x8BB6E3EB2402CC87, 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (U) (M5) [t1]
        { 0x8BC3A47A74221294, 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) (Debug Version) [f1] (Decrypted)
        { 0x8BD4A3341E138B05, 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (E) (M3) [!]
        { 0x8BDBAF68345B4B36, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW vs. nWo - World Tour (E) [!]
        { 0x8BDDD635EC0E9198, 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (U) [f2] (PAL)
        { 0x8BEE2831D09BC906, 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (U) [t1]
        { 0x8C138BE095700E46, 1, RUMBLE_PAK }, // In-Fisherman Bass Hunter 64 (U) [!]
        { 0x8C3511AE57115A86, 2, 0 }, // Mortal Kombat Trilogy (E) [t6] (P1 Invincible)
        { 0x8C352E506B93F13B, 2, 0 }, // Mortal Kombat Trilogy (E) [t5] (2x Aggressor)
        { 0x8C38E5DBB37C27D7, 4, MEMORY_PAK | RUMBLE_PAK }, // ECW Hardcore Revolution (E) [b1]
        { 0x8C3D1192B5096A18, 2, 0 }, // Mortal Kombat Trilogy (E) [t1] (Hit Anywhere)
        { 0x8C3D1192BEF172E1, 2, 0 }, // Mortal Kombat Trilogy (E) [o1]
        { 0x8C47CE8B06019FEF, 1, RUMBLE_PAK }, // Kyojin no Doshin: Kaihou Sensen Chibikko Chikko Daishuugou (J) [CART HACK]
        { 0x8CC182A6C2D0CAB0, 4, MEMORY_PAK }, // AI Shougi 3 (J) [!]
        { 0x8CD0637962319493, 2, MEMORY_PAK }, // Power Rangers - Lightspeed Rescue (E) [f1] (NTSC)
        { 0x8CDB94C2CB46C6F0, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF No Mercy (E) (V1.1) [!]
        { 0x8CF1CECBC8902FF4, 4, RUMBLE_PAK }, // Super Smash Bros. (U) [f2] (PAL)
        { 0x8D1780B657B3B976, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Pro 99 (E) [!]
        { 0x8D2BAE98D73725BF, 4, MEMORY_PAK }, // Nagano Winter Olympics '98 (U) [o2]
        { 0x8D412933588F64DB, 2, MEMORY_PAK | RUMBLE_PAK }, // Tom Clancy's Rainbow Six (G) [!]
        { 0x8D6F5219D43B163C, 2, MEMORY_PAK | RUMBLE_PAK }, // Knockout Kings 2000 (U) [f1] (PAL)
        { 0x8DF95B18ECDA497B, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2000 (U) [!]
        { 0x8E1B33039D4F5035, 4, RUMBLE_PAK }, // GoldenEye 007 (U) [t2]
        { 0x8E2486492E1CDE52, 0, 0 }, // Fire Demo by Lac (PD)
        { 0x8E3D8D2E9850573F, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.09
        { 0x8E6E01FFCCB4F948, 1, 0 }, // Glover (U) [!]
        { 0x8E835437CD5748B4, 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (U) (M2) [!]
        { 0x8E9692B34264BB2A, 4, 0 }, // Clay Fighter 63 1-3 (E) [h1C]
        { 0x8E97A4A6D1F31B33, 0, 0 }, // LaC's MOD Player - The Temple Gates (PD)
        { 0x8E9D834E1E8B29A9, 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (E) (M5) [h1C]
        { 0x8E9F21D2DC19C5AB, 4, 0 }, // Glover 2 - Unreleased Beta Version (New Build) (Prototype)
        { 0x8ECC02F07F8BDE81, 2, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Hyper Bike (U) [!]
        { 0x8F00E51E030C92C8, 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (U) [hI][f1] (PAL)
        { 0x8F0CC36DC738259E, 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T+Eng90%]
        { 0x8F12C09645DC17E1, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (E) [!]
        { 0x8F3151C84F3AF545, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (G) [b1]
        { 0x8F4C4B326D881FCA, 2, MEMORY_PAK }, // Robotron 64 (U) [f1] (PAL)
        { 0x8F50B845D729D22F, 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (G) [!]
        { 0x8F5179C4803526DC, 4, 0 }, // Spice Girls Rotator Demo by RedboX (PD)
        { 0x8F89ABA0C34C2610, 4, 0 }, // GameShark Pro V3.3 (Mar 2000) (Unl) [!]
        { 0x90111D7EE4B49428, 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx - Global Assault (U) [f2] (PAL)
        { 0x906C3F77CE495EA1, 1, 0 }, // Dinosaur Planet (Dec 2000 Beta)
        { 0x909535F8118FEF8F, 4, 0 }, // Sim City 64 (J) [CART HACK]
        { 0x90A5900331089864, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (U) [t1] (Never Drop Weapons)
        { 0x90AF8D2CE1AC1B37, 4, 0 }, // Tower&Shaft (Aleck64)
        { 0x90F430375C5370F5, 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (J) [!]
        { 0x90F5D9B39D0EDCF0, 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.0) [!]
        { 0x9118F1B8987DAC8C, 4, 0 }, // PC-Engine 64 (POM '99) (PD)
        { 0x91644B9F8CDD0DA5, 4, MEMORY_PAK }, // Brunswick Circuit Pro Bowling (U) [o1][f1] (PAL)
        { 0x916852D873DBEAEF, 4, RUMBLE_PAK }, // NBA Courtside 2 - Featuring Kobe Bryant (U) [!]
        { 0x91691C3DF4AC5B4D, 2, RUMBLE_PAK | TRANSFER_PAK }, // Transformers - Beast Wars Metals 64 (J) [!]
        { 0x916AE6B88817AB22, 1, MEMORY_PAK }, // Tokisora Senshi Turok (J) [!]
        { 0x916B8B5B780B85A4, 4, RUMBLE_PAK }, // Super Smash Bros. (U) [hI]
        { 0x916EB31BF47097A5, 4, 0 }, // Animal Crossing (Ch) (iQue) [!]
        { 0x917D18F669BC5453, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (U) (Debug Version)
        { 0x917D1B16831F9BE1, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (U) (Debug Version) [f1]
        { 0x918E2D60F865683E, 4, MEMORY_PAK | RUMBLE_PAK }, // S.C.A.R.S. (E) (M3) [h1C]
        { 0x91B66D4216AC4E46, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (G) [!]
        { 0x91C9E05DAD3AAFB9, 4, TRANSFER_PAK }, // Pokemon Stadium (E) (V1.1) [!]
        { 0x92072A84275952F8, 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [h2C]
        { 0x9207357AE85D4001, 2, RUMBLE_PAK }, // 1080 Snowboarding (JU) (M2) [f2] (PAL)
        { 0x926C2E3B9829A216, 2, MEMORY_PAK }, // Dual Heroes (E) [f1] (NTSC)
        { 0x9303DD170813B398, 4, 0 }, // GoldenEye 007 Intro by SonCrap (PD)
        { 0x93053075261E0F43, 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (E) (M4) (V1.0) [b1]
        { 0x930C29EA939245BF, 4, RUMBLE_PAK }, // Snowboard Kids 2 (U) [!]
        { 0x931AEF3FEF196B90, 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (J) [!]
        { 0x938CA40143339F1B, 4, MEMORY_PAK }, // Bomberman 64 (E) [t1]
        { 0x93945F485C0F2E30, 4, RUMBLE_PAK }, // Super Smash Bros. (E) (M3) [!]
        { 0x93A292F1ADF98E2E, 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (E) (M3) [f2] (NTSC)
        { 0x93A625B92D6022E6, 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (B) [!]
        { 0x93A943335A613C39, 4, 0 }, // Eurasia Intro by Ste (PD)
        { 0x93C5ED78F67E8528, 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [f2] (NTSC)
        { 0x93DA8551D231E8AB, 4, 0 }, // The Corporation XMAS Demo '99 by TS_Garp (PD)
        { 0x93EB3F7E81675E44, 1, RUMBLE_PAK }, // Mission Impossible (G) [!]
        { 0x93ED11922B2C6244, 2, 0 }, // Mortal Kombat Trilogy (E) [t4] (Hyper Mode)
        { 0x94123517300EEA90, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [f1] (PAL)
        { 0x944FAFC4B288266A, 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) (Beta) [!]
        { 0x94593D97846A278C, 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (E) [f1]
        { 0x947A4B4790BFECA6, 4, 0 }, // Mortal Kombat SRAM Loader (PD)
        { 0x94807E6B60CC62E4, 2, RUMBLE_PAK }, // Puyo Puyo Sun 64 (J) [b1]
        { 0x94D3D5CBE8808606, 4, 0 }, // MSFTUG Intro #1 by LaC (PD)
        { 0x94EDA5B88673E903, 1, 0 }, // Starshot - Space Circus Fever (U) (M3) [b1]
        { 0x94FBA5FDBC8FE181, 4, 0 }, // Worms - Armageddon (E) (M6) [f1] (NTSC)
        { 0x94FEDD3F95719E88, 1, RUMBLE_PAK }, // Yoshi's Story (E) (M3) [t1] (Health and Eggs)
        { 0x95013CCC73F7C072, 4, 0 }, // U64 (Chrome) Demo by Horizon64 (older) (PD)
        { 0x95081A8B49DFE4FA, 4, 0 }, // CD64 Memory Test (PD)
        { 0x9510D8D735100DD2, 4, MEMORY_PAK | RUMBLE_PAK }, // Stunt Racer 64 (U) [!]
        { 0x95286EB4B76AD58F, 1, RUMBLE_PAK }, // Command & Conquer (U) [!]
        { 0x9542E183B1D245E6, 1, MEMORY_PAK }, // Turok - Dinosaur Hunter (U) (V1.0) [t2]
        { 0x95854656C8A24DC5, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.0) [f1]
        { 0x95A80114E0B72A7F, 1, MEMORY_PAK }, // Hamster Monogatari 64 (J) [!]
        { 0x95B2B30B2B6415C1, 4, MEMORY_PAK }, // Hexen (E) [h1C]
        { 0x960621E37D4D5BB0, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.13
        { 0x9624C4F93FB02E03, 4, RUMBLE_PAK }, // Super Smash Bros. (A) [f1]
        { 0x963ADBA6F7D5C89B, 4, BIO_SENSOR }, // Tetris 64 (J) [b2]
        { 0x964ADD0BB29213DB, 1, RUMBLE_PAK }, // Lode Runner 3-D (J) [!]
        { 0x96747EB4104BB243, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (J) [!]
        { 0x96961E83B5DE0894, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.0) [f2] (zpc1)
        { 0x96BA4EFBC9988E4E, 4, MEMORY_PAK }, // Derby Stallion 64 (J) (Beta)
        { 0x975B7845A2505C18, 2, 0 }, // 77a Special Edition by Count0 (PD) [b1]
        { 0x9780C9FB67CF6B4A, 4, 0 }, // Bomberman 64 - Arcade Edition (J) [T+Eng1.3_Zoinkity]
        { 0x979B263EF8470004, 2, MEMORY_PAK }, // Killer Instinct Gold (E) [o1]
        { 0x97E57686F271C12F, 4, 0 }, // Money Creates Taste Demo by Count0 (POM '99) (PD) [f1]
        { 0x97FC21674616872B, 4, 0 }, // Game Boy 64 (POM '98) (PD)
        { 0x9856E53DAF483207, 4, 0 }, // Hard Pom '99 Demo by TS_Garp (POM '99) (PD)
        { 0x9865799F006F908C, 4, 0 }, // My Angel Demo (PD)
        { 0x988C4CC3C9F310C5, 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (U) [f1] (PAL)
        { 0x98A2BB11EE4D4A86, 4, 0 }, // SRAM Upload Tool V1.1 by Lac (PD) [b1]
        { 0x98DF9DFC6606C189, 1, 0 }, // Harvest Moon 64 (U) [b1]
        { 0x98F9F2D003D9F09C, 2, MEMORY_PAK }, // Virtual Pool 64 (E) [o1]
        { 0x99150E181266E6A5, 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater 2 (U) [!]
        { 0x991793592FE7EBC3, 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (E) (M3) (Eng-Spa-Ita) [!]
        { 0x993B7D7A2E54F04D, 4, 0 }, // Wet Dreams Madeiragames Demo by Immortal (POM '99) (PD)
        { 0x993CC742EE65D7A4, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2001 (U) [f1] (PAL)
        { 0x9A2949C104E76074, 4, 0 }, // Rape Kombat Trilogy Beta1 (Mortal Kombat Hack)
        { 0x9A490D9D8F013ADC, 4, MEMORY_PAK | RUMBLE_PAK }, // Milo's Astro Lanes (E) [o2]
        { 0x9A6CF2F5D5F365EE, 4, 0 }, // Sitero Demo by Renderman (PD)
        { 0x9A746EBF2802EA99, 4, 0 }, // Toon Panic (J) (Prototype)
        { 0x9A75C9C2A4488353, 4, MEMORY_PAK | RUMBLE_PAK }, // BattleTanx (U) [f1] (PAL)
        { 0x9A7A9F1405137CC7, 4, 0 }, // Zelda 64 Boot Emu V1 by Crazy Nation (PD)
        { 0x9A9890ACF0C313DF, 1, 0 }, // Mario no Photopie (J) [a1]
        { 0x9AB3B50ABC666105, 4, MEMORY_PAK }, // Hexen (G) [h1C]
        { 0x9ABC592A2AA5DEE2, 4, MEMORY_PAK | RUMBLE_PAK }, // Michael Owens WLS 2000 (E) [f1] (NTSC)
        { 0x9ADD5DE5451F0A65, 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [b2]
        { 0x9B500E8EE90550B3, 1, RUMBLE_PAK }, // Resident Evil 2 (E) (M2) [!]
        { 0x9BA10C4E0408ABD3, 1, MEMORY_PAK }, // Pro Mahjong Kiwame 64 (J) (V1.0) [o1]
        { 0x9BA3E5CCEB222304, 4, MEMORY_PAK | RUMBLE_PAK }, // Bomberman 64 - The Second Attack! (U) [t1][f1] (PAL-NTSC)
        { 0x9BF2A817EE20252A, 2, RUMBLE_PAK }, // Tom and Jerry in Fists of Furry (U) [t1]
        { 0x9C013AA8EB3267A7, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t4] (Hyper Mode)
        { 0x9C044945D31E0B0C, 0, 0 }, // Wet Dreams Readme by Immortal (POM '99) (PD)
        { 0x9C660FA0C06A610D, 4, RUMBLE_PAK }, // Jet Force Gemini (U) (Kiosk Demo) [b1][f1] (Save)
        { 0x9C66306980F24A80, 4, RUMBLE_PAK }, // Mario Party (E) (M3) [h1C]
        { 0x9C7318D224AE0DC1, 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (J) [b1]
        { 0x9C8FB2FA9B84A09B, 4, TRANSFER_PAK }, // Pokemon Stadium (U) (V1.2) [!]
        { 0x9C961069F5EA488D, 4, MEMORY_PAK | RUMBLE_PAK }, // 64 Oozumou (J) [b3]
        { 0x9CAB6AEA87C61C00, 4, MEMORY_PAK }, // Hexen (U) [h1C]
        { 0x9CB1BBB0BC3439C7, 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [b1]
        { 0x9CCE5B1D6351E283, 4, 0 }, // MAME 64 V1.0 (PD)
        { 0x9CE02E22206EF1B0, 4, MEMORY_PAK | RUMBLE_PAK }, // Super Speed Race 64 (J) [o1]
        { 0x9CEED5BA62A146D5, 4, MEMORY_PAK }, // NBA Showtime - NBA on NBC (U) [f1] (PAL)
        { 0x9CF33AA93F05381F, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t3] (All Attacks Hurt P2)
        { 0x9D2B9C7F6D90A8EF, 4, RUMBLE_PAK }, // SmashRemix0.9.7
        { 0x9D63C653C26B460F, 4, 0 }, // POMbaer Demo by Kid Stardust (POM '99) (PD)
        { 0x9D7E3C4BE60F4A6C, 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (J) [!]
        { 0x9D9C362D5BE66B08, 4, 0 }, // Shag'a'Delic Demo by Steve and NEP (PD)
        { 0x9DFE20E918894032, 4, TRANSFER_PAK }, // Pokemon Stadium (E) (V1.0) [f1]
        { 0x9E330C018C0314BA, 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (U) [b1]
        { 0x9E6581AB57CC8CED, 4, 0 }, // MMR by Count0 (PD)
        { 0x9E8FCDFA49F5652B, 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.1) [o1]
        { 0x9E8FE2BA8B270770, 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.0) [o2]
        { 0x9EA95858AF72B618, 4, RUMBLE_PAK }, // Mario Party 2 (U) [!]
        { 0x9F1ECAF0EEC48A0E, 2, MEMORY_PAK }, // Rakuga Kids (J) [h1C]
        { 0x9F35059D48B7F411, 4, 0 }, // Diddy Kong Racing SRAM by Group5 (PD)
        { 0x9F5BF79CD2FE08A0, 2, MEMORY_PAK | RUMBLE_PAK }, // Quake 64 (U) [o2]
        { 0x9F8926A50587B409, 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater (E) [!]
        { 0x9F8B96C3A01194DC, 1, RUMBLE_PAK }, // Yakouchuu II - Satsujin Kouru (J) [!]
        { 0x9FC385E53ECC05C7, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.1) (Debug Version)
        { 0x9FD0987D7EAE10D8, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [f3]
        { 0x9FD375F845F32DC8, 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (E) (M3) [!]
        { 0x9FD881BC0A62D002, 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (U) [t1]
        { 0x9FE6162DE97E4037, 1, 0 }, // Yuke Yuke!! Trouble Makers (J) [o1]
        { 0x9FF69D4F195F0059, 2, MEMORY_PAK }, // Robotron 64 (E) [h1C]
        { 0xA03CF036BCC1C5D2, 1, 0 }, // Super Mario 64 (E) (M3) [o2]
        { 0xA04237B968F62C72, 4, 0 }, // Wildwaters (U) (Prototype)
        { 0xA08D0F776F82E38C, 4, RUMBLE_PAK }, // Conker's Bad Fur Day (U) (Demo) (V_ECTS_Decrypted)
        { 0xA0976BE9FF41562B, 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (U) [t1]
        { 0xA150743ECF2522CD, 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (U) [!]
        { 0xA1815C2EC6806731, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t6] (P1 Invincible)
        { 0xA197CB527520DE0E, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden Football 64 (E) [b1]
        { 0xA19F808977884B51, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball 2000 (E) [!]
        { 0xA19FA3D0E06B2D11, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [f2] (Z64-Save)
        { 0xA1B64A61D014940B, 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (E) (M3) [h1C]
        { 0xA1BFF9C98FB192A2, 4, RUMBLE_PAK }, // SmashRemix1.2.0
        { 0xA1CC026FE09D1766, 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ita) [f1] (NTSC)
        { 0xA1E78A63022A67C9, 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (U) [t1]
        { 0xA1F51C2516D61291, 4, 0 }, // Die Hard 64 (U) (Prototype) (Level 3)
        { 0xA2015C11C317ECAB, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t4] (Hyper Mode)
        { 0xA23553A342BF2D39, 4, TRANSFER_PAK }, // Pokemon Stadium (F) [!]
        { 0xA24F4CF1A82327BA, 4, RUMBLE_PAK }, // GoldenEye 007 (J) [!]
        { 0xA273AB56DA33DB9A, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (U) [o1]
        { 0xA292524F3D6C2A49, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone '99 (U) [!]
        { 0xA2C54BE76719CBB2, 1, MEMORY_PAK }, // Castlevania - Legacy of Darkness (E) (M3) [o1]
        { 0xA2E8F35BC9DC87D9, 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [b2]
        { 0xA3271D8305B533A5, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (GC) [f1]
        { 0xA38F7AA603A48A45, 4, 0 }, // View N64 Test Program (PD) [b1]
        { 0xA39C0306F1DD4CC6, 2, RUMBLE_PAK }, // Ganbare Goemon - Derodero Douchuu Obake Tenkomori (J) [t2]
        { 0xA3A044B56DB1BF5E, 4, 0 }, // Spacer by Memir (POM '99) (PD)
        { 0xA3A95A579FE6C27D, 4, 0 }, // Wet Dreams Main Demo by Immortal (POM '99) (PD)
        { 0xA47D4AD48BFA81F9, 1, 0 }, // Manic Miner by RedboX (PD)
        { 0xA47D4AD4F5B0C6CB, 1, 0 }, // Manic Miner - Hidden Levels by RedboX (PD)
        { 0xA4A52B5823759841, 2, 0 }, // Dark Rift (U) [!]
        { 0xA4BF9306BF0CDFD1, 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b7]
        { 0xA4C8852AE512045C, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (U) [f1] (PAL)
        { 0xA4F2F521F0EB168E, 4, RUMBLE_PAK }, // Chameleon Twist (J) [!]
        { 0xA53FA82DDAE2C15D, 4, TRANSFER_PAK }, // Pokemon Stadium (I) [!]
        { 0xA5533106B9F25E5B, 1, RUMBLE_PAK }, // Akumajou Dracula Mokushiroku Gaiden - Legend of Cornell (J) [!]
        { 0xA5F667E1DA1FBD1F, 4, MEMORY_PAK }, // Derby Stallion 64 (J) [!]
        { 0xA60ED1713D85D06E, 1, MEMORY_PAK | RUMBLE_PAK }, // Spider-Man (U) [!]
        { 0xA62230C3F0834488, 2, MEMORY_PAK }, // Dual Heroes (U) [b1]
        { 0xA6421764ED1A13E9, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball '99 (U) [f1] (PAL)
        { 0xA6A9949152B4E765, 4, MEMORY_PAK | RUMBLE_PAK }, // Wipeout 64 (U) [t2]
        { 0xA6B6B41315D113CC, 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (J) [b5]
        { 0xA6FD08086E6B3ECA, 4, RUMBLE_PAK }, // Charlie Blast's Territory (U) [hI]
        { 0xA75078550D454C66, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [t1]
        { 0xA794152861F1199D, 4, RUMBLE_PAK }, // Chou Snobow Kids (J) [!]
        { 0xA7D015F82289AA43, 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [o3][f1]
        { 0xA806749B1F521F45, 4, 0 }, // MeeTING Demo by Renderman (PD) [b1]
        { 0xA8275140B9B056E8, 1, RUMBLE_PAK }, // Doraemon 3 - Nobita no Machi SOS! (J) [!]
        { 0xA83E101AE937B69D, 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [o1]
        { 0xA84D23BAA1974DBD, 4, 0 }, // GoldenEye 007 (U) (Tetris Hack)
        { 0xA864A92199082CD0, 2, MEMORY_PAK }, // Namco Museum 64 (U) [t1]
        { 0xA896AB1D27E73688, 2, MEMORY_PAK }, // Namco Museum 64 (U) [f1] (PAL)
        { 0xA8D1DAF231206B53, 4, 0 }, // Action Replay Pro 64 V3.3 (Unl)
        { 0xA92D52E51D26B655, 2, MEMORY_PAK | RUMBLE_PAK }, // Flying Dragon (U) [b2]
        { 0xA92E0966341C3912, 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (U) (Beta) [!]
        { 0xA957851C535A5667, 4, 0 }, // JPEG Slideshow Viewer by Garth Elgar (PD)
        { 0xA986ECC150AC924B, 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (U) [t1]
        { 0xA9895CD97020016C, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Pro 99 (E) [o1]
        { 0xA9A17EF3AB1F8703, 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (U) [f1] (PAL-NTSC)
        { 0xA9F7BB48D63C5F56, 4, 0 }, // Neon64 V1.2a by Halley's Comet Software (PD) [b1]
        { 0xAA18B1A507DB6AEB, 1, RUMBLE_PAK }, // Resident Evil 2 (U) (V1.1) [!]
        { 0xAA1D215A91CBBE9A, 4, MEMORY_PAK | RUMBLE_PAK }, // Super Bowling (U) [!]
        { 0xAA7B06589C96937B, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Mayhem (E) [h1C]
        { 0xAA89A18D0FC2AFE6, 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (U) [f1] (PAL)
        { 0xAA94493C3736CC22, 2, MEMORY_PAK | RUMBLE_PAK }, // 40 Winks (U) (Prototype)
        { 0xAAA6622998CA5CAA, 4, 0 }, // Soncrap Intro by RedboX (PD)
        { 0xAAD95AFCBB3A44AC, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (Kiosk Demo) (Gore On Hack)
        { 0xAAD9FC15B2C9DA6F, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.23
        { 0xAAE11F012625A045, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2 (J) [!]
        { 0xAAE125A9C9274BC9, 2, 0 }, // Pokemon Puzzle League (U) [t2]
        { 0xAB5E61C8A2FA7596, 4, MEMORY_PAK }, // Penny Racers (U) [hI]
        { 0xAB7C101DEC58C8B0, 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (E) [b1]
        { 0xAB9EB27D5F05605F, 4, 0 }, // Mario Artist: Communication Kit (J) [CART HACK]
        { 0xAB9F8D9795EAA766, 4, 0 }, // NBC-LFC Kings of Porn Vol 01 (PD)
        { 0xABA51D09C668BAD9, 2, MEMORY_PAK | RUMBLE_PAK }, // 40 Winks (E) (M3) (Prototype)
        { 0xABB57137960F561D, 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (U) [t1][f1] (PAL-NTSC)
        { 0xAC000A2B38E3A55C, 1, RUMBLE_PAK }, // Neon Genesis Evangelion (J) [f1] (PAL)
        { 0xAC062778DFADFCB8, 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (E) (M5) [h1C]
        { 0xAC16400ECF5D071A, 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (U) [!]
        { 0xAC16460E46F60594, 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (U) [f1] (Country Check)
        { 0xAC1730775A14C012, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.2) [!]
        { 0xAC5AA5C7A9B0CDC3, 4, TRANSFER_PAK }, // Pokemon Stadium 2 (F) [!]
        { 0xAC8E4B32E7B47326, 2, MEMORY_PAK }, // Robotron 64 (U) [o1]
        { 0xAC976B38C3A9C97A, 1, MEMORY_PAK | RUMBLE_PAK }, // Paperboy (E) [!]
        { 0xAC9F7DA7A8C029D8, 2, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush - Extreme Racing (U) (M3) (V1.1) [!]
        { 0xACBF6069EABEE162, 1, RUMBLE_PAK }, // Chopper Attack (U) [b1]
        { 0xACD51083EEC8DBED, 4, 0 }, // Pom Part 2 Demo (PD)
        { 0xACDCA0C60F4C66F9, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [f2] (PAL)
        { 0xACDE962FB2CBF87F, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Pro 98 (E) [!]
        { 0xAD19717104DB7E9E, 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [t1]
        { 0xAD1999711075F279, 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [f2] (NTSC)
        { 0xAD19A1729004666A, 4, MEMORY_PAK }, // Frogger 2 (U) (Prototype 1) [!]
        { 0xADA815BE6028622F, 4, RUMBLE_PAK }, // Mario Party (J) [!]
        { 0xADB9498BDAF28F55, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (U) (V1.0) [!]
        { 0xADCDEC69D0179F38, 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (E) (M5) [t1]
        { 0xADD78971A09C18CF, 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [f3] (NTSC)
        { 0xADDD6241AA79B2F2, 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [t2] (Endless Ice)
        { 0xAE21569D5A5BC572, 2, MEMORY_PAK }, // Namco Museum 64 (U) [o1][f1] (PAL)
        { 0xAE2D3A3524F0D41A, 4, MEMORY_PAK }, // Olympic Hockey Nagano '98 (E) (M4) [h1C]
        { 0xAE4992C99253B253, 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (E) (M3) [h1C]
        { 0xAE5B9465C54D6576, 1, RUMBLE_PAK }, // Command & Conquer (E) (M2) [b3]
        { 0xAE5E467CD7194A71, 4, RUMBLE_PAK }, // Monster Truck Madness 64 (E) (M5) [f1] (NTSC)
        { 0xAE6B1E11B7CBD69E, 4, 0 }, // Dragon Sword 64 (U) (Prototype) (1999-08-25)
        { 0xAE809DD00A99B360, 4, 0 }, // Neon64 V1.1 by Halley's Comet Software (PD) [o2]
        { 0xAE82687A9A3F388D, 1, MEMORY_PAK }, // F-1 Pole Position 64 (U) (M3) [h1C]
        { 0xAE90DBEB79B89123, 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (E) (M6) [!]
        { 0xAEBCDD5415FF834A, 1, RUMBLE_PAK }, // Taz Express (E) (M6) [!]
        { 0xAEBE463ECC71464B, 2, MEMORY_PAK }, // AeroGauge (U) [h2C]
        { 0xAEEF2F45F97E30F1, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter Destiny 2 (U) [!]
        { 0xAF13E2C22D502822, 4, RUMBLE_PAK }, // Lylat Wars (A) (M3) [t1] (Boost)
        { 0xAF19D9F5B70223CC, 4, MEMORY_PAK }, // Jikkyou G1 Stable (J) (V1.0) [b1]
        { 0xAF3E4E2D9C63E651, 4, RUMBLE_PAK }, // Cruis'n World (E) [f3] (NTSC)
        { 0xAF5F3773B29B2C9A, 1, RUMBLE_PAK }, // Yoshi Story (J) [t2] (Health and Eggs)
        { 0xAF61F38629F1112C, 2, RUMBLE_PAK }, // 1080 Snowboarding (E) (M4) [f1]
        { 0xAF754F7B1DD17381, 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (Kiosk Demo) [!]
        { 0xAF8679B65E1011BF, 2, MEMORY_PAK | RUMBLE_PAK }, // G.A.S.P!! Fighter's NEXTream (J) [o1]
        { 0xAF981AB7AA1A0A7A, 1, MEMORY_PAK | TRANSFER_PAK }, // Super Robot Taisen 64 (J) [f1] (PAL)
        { 0xAF9DCC151A723D88, 1, RUMBLE_PAK }, // Indiana Jones and the Infernal Machine (U) [!]
        { 0xAFBBB9D58EE82954, 4, 0 }, // Pip's RPGs Beta 15 by Mr. Pips (PD)
        { 0xAFBEB6F6B0E715DC, 2, 0 }, // War Gods (U) (Power Bar Hack)
        { 0xAFDCF8505CCD80DE, 1, MEMORY_PAK | RUMBLE_PAK }, // Nightmare Creatures (U) [t2]
        { 0xAFF44E2D2ECB9679, 4, RUMBLE_PAK }, // Cruis'n World (E) [f2]
        { 0xB00903C93916C146, 2, MEMORY_PAK }, // AeroGauge (J) (V1.0) (Kiosk Demo) [b3]
        { 0xB00E382929F232D1, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu - Basic Ban 2001 (J) (V1.1) [!]
        { 0xB0105C856E5094B9, 1, RUMBLE_PAK }, // Mission Impossible (U) [t1]
        { 0xB0119B37B9774FD5, 2, MEMORY_PAK | RUMBLE_PAK }, // Big Mountain 2000 (U) [t1]
        { 0xB01A1DAB7F7827E3, 1, MEMORY_PAK }, // Doom 64 (U) (V1.0) [T+Bra1.0_Doom64BR]
        { 0xB038EEEB00BA6901, 4, 0 }, // Z64 BIOS V2.00 (Barebones)
        { 0xB044B569373C1985, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.0) [b1]
        { 0xB0565CCBBDA2C237, 4, 0 }, // TheMuscularDemo by megahawks (PD)
        { 0xB0667DEDBB39A4B8, 0, 0 }, // Absolute Crap Intro #1 by Kid Stardust (PD)
        { 0xB088FBB4441E4B1D, 1, RUMBLE_PAK }, // Bass Hunter 64 (E) [!]
        { 0xB0AC43213F4DA1AF, 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (U) (M10) [b1][t1]
        { 0xB0B4EDFFB2A20628, 4, RUMBLE_PAK }, // F-ZERO X (U) [f1]
        { 0xB121ED86883423F1, 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (E) [b1]
        { 0xB19AD9997E585118, 4, RUMBLE_PAK }, // Monster Truck Madness 64 (U) [!]
        { 0xB1C7794BD93DC5F3, 2, RUMBLE_PAK }, // Tsumi to Batsu - Hoshi no Keishousha (J) [T+Spa1.0b_TheFireRed&IlDucci] (Fandub)
        { 0xB1CF11145E59B81D, 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (U) [f1]
        { 0xB1D5280C4BA7BC2A, 1, MEMORY_PAK }, // Sim City 2000 (J) [o1][h1C]
        { 0xB1E1E07B051269DD, 1, 0 }, // Legend of Zelda, The - Ocarina of Time (Ch) (iQue) [!]
        { 0xB2055FBD0BAB4E0C, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.1) [!]
        { 0xB20CC247B879579A, 4, MEMORY_PAK | RUMBLE_PAK }, // HSV Adventure Racing (A) [f1] (NTSC)
        { 0xB20F73B62975FC34, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes 2 (U) [!]
        { 0xB210DF1998B58D1A, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (E) (M3) [!]
        { 0xB216B9F1AB27D881, 4, 0 }, // N64probe by MooglyGuy (PD)
        { 0xB2242748FFBD61DA, 4, RUMBLE_PAK }, // 007 - Goldfinger
        { 0xB24303F49F7D2445, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes 2 (U) [t1]
        { 0xB244B30C72A648D8, 4, RUMBLE_PAK }, // GoldenEye 007 (J) [t1] (Rapid Fire)
        { 0xB2C6D27F2DA48CFD, 4, MEMORY_PAK }, // Ganbare Goemon - Mononoke Sugoroku (J) [!]
        { 0xB3054F9F96B69EB5, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA In the Zone 2000 (E) [h1C]
        { 0xB30ED9783003C9F9, 4, RUMBLE_PAK }, // F-ZERO X (U) [!]
        { 0xB31A9FBB26D03050, 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (U) [t1]
        { 0xB323E37CBBC35EC4, 4, 0 }, // DKONG Demo (PD)
        { 0xB34025547340C004, 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.2) [!]
        { 0xB35FEBB07427B204, 1, MEMORY_PAK }, // Holy Magic Century (F) [o1]
        { 0xB36CF0BEA35F5C7B, 1, 0 }, // Super Mario 64 (J) [T+Kor1.0_Minio]
        { 0xB3CD11F660725EB0, 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 64 (J) [f2]
        { 0xB3D451C6E1CB58E2, 1, MEMORY_PAK }, // Bokujou Monogatari 2 (J) (V1.0) [b1]
        { 0xB443EB084DB31193, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (U) (GC)
        { 0xB44CAB7407029A29, 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (E) (M6) [!]
        { 0xB484EB31D44B1928, 4, 0 }, // Christmas Flame Demo by Halley's Comet Software (PD)
        { 0xB5025BADD32675FD, 1, RUMBLE_PAK }, // Command & Conquer (G) [!]
        { 0xB54CE881BCCB6126, 4, 0 }, // PGA European Tour (U) [!]
        { 0xB58988E9B1FC4BE8, 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (E) [!]
        { 0xB6223A7B2139CEA6, 2, MEMORY_PAK | RUMBLE_PAK }, // Bio F.R.E.A.K.S. (U) [t1]
        { 0xB6306E99B63ED2B2, 1, RUMBLE_PAK }, // Doraemon 2 - Nobita to Hikari no Shinden (J) [b1]
        { 0xB63200D4E36CF5AA, 1, RUMBLE_PAK }, // Mega Man 64 (U) [t1][f1] (PAL-NTSC)
        { 0xB6524461ED6D04B1, 2, MEMORY_PAK }, // Dual Heroes (E) [b2]
        { 0xB66E0F7C2709C22F, 4, RUMBLE_PAK }, // Jet Force Gemini (E) (M4) [f1]
        { 0xB6835E5404A4A0D0, 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (E) (M3) [f1] (NTSC)
        { 0xB6951A9463C849AF, 1, RUMBLE_PAK }, // Akumajou Dracula Mokushiroku - Real Action Adventure (J) [!]
        { 0xB6A90FB4E2C235BA, 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (E) (M3) [f1] (NTSC)
        { 0xB6BC0FB0E3812198, 2, RUMBLE_PAK }, // Tsumi to Batsu - Hoshi no Keishousha (J) [!]
        { 0xB6BE20A5FACAF66D, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok - Rage Wars (E) (M3) (Eng-Fre-Ita) [!]
        { 0xB6C4F40092A39D47, 4, MEMORY_PAK }, // Centre Court Tennis (E) [f1] (NTSC)
        { 0xB6D0CAA0E3F493C8, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Jam 2000 (E) [!]
        { 0xB6DB2595BC45273B, 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [b1][t1]
        { 0xB6E549CEDC8134C0, 4, TRANSFER_PAK }, // Pokemon Stadium (S) [!]
        { 0xB6F68615F4E26039, 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.1) [b1][f1] (PAL)
        { 0xB703EB2328AAE53A, 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (J) [o1]
        { 0xB70BAEE53A5005A8, 2, RUMBLE_PAK }, // F-1 World Grand Prix (F) [!]
        { 0xB722133E5EECEC4D, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [t4]
        { 0xB73AB6F6296267DD, 2, RUMBLE_PAK }, // Tsumi to Batsu - Hoshi no Keishousha (J) [T+Eng1.0_Zoinkity]
        { 0xB74388D77DBD6DFB, 4, 0 }, // Dr. Mario 64 (U) [T+Jap1.0_Zoinkity]
        { 0xB754A584D3530516, 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (U) [t1]
        { 0xB75E20B7B3FEFDFD, 4, MEMORY_PAK | TRANSFER_PAK }, // Jikkyou Powerful Pro Yakyuu 6 (J) (V1.0) [b1]
        { 0xB771E0A97B4A96E6, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.0) [f1] (PAL)
        { 0xB7CF2136FA0AA715, 2, MEMORY_PAK | RUMBLE_PAK }, // Rush 2 - Extreme Racing USA (E) (M6) [!]
        { 0xB7F40BCF553556A5, 4, 0 }, // Glover 2 (Prototype)
        { 0xB8B732631492FC49, 2, MEMORY_PAK | RUMBLE_PAK }, // Polaris SnoCross (U) [t1]
        { 0xB8C303FA544BB5FA, 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (E) (M5) [f1] (NTSC)
        { 0xB8E73356040E42EC, 4, 0 }, // Dynamix Intro (Hidden Song) by Widget and Immortal (PD)
        { 0xB8F0BD034479189E, 2, MEMORY_PAK | RUMBLE_PAK }, // MRC - Multi Racing Championship (E) (M3) [o1]
        { 0xB9233DC79CCF2D4C, 1, RUMBLE_PAK }, // Rocket - Robot on Wheels (U) [f1] (PAL)
        { 0xB98BA4565B2B76AF, 4, MEMORY_PAK }, // 64 de Hakken!! Tamagotchi Minna de Tamagotchi World (J) [o1]
        { 0xB9A9ECA217AAE48E, 4, MEMORY_PAK | RUMBLE_PAK }, // San Francisco Rush 2049 (U) [!]
        { 0xB9AF8CC6DEC9F19F, 4, RUMBLE_PAK }, // Chameleon Twist (E) [o2]
        { 0xB9CDC5C30D2F4668, 4, RUMBLE_PAK }, // SmashRemix0.9.5b
        { 0xB9D36858559A8241, 4, 0 }, // Monopoly (U) [f1] (PAL)
        { 0xBA6C293A9FAFA338, 1, 0 }, // Pokemon Snap (F) [b1]
        { 0xBA780BA00F21DB34, 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.1) [!]
        { 0xBAE8E87135FF944E, 4, MEMORY_PAK | RUMBLE_PAK }, // International Superstar Soccer 2000 (E) (M2) (Fre-Ita) [!]
        { 0xBB214F798B88B16B, 4, 0 }, // Super Bomberman 2 by Rider (POM '99) (PD)
        { 0xBB2D10F8B8ED4D23, 1, 0 }, // Pocket Monsters Snap (J) [f1]
        { 0xBB30B1A5FCF712CE, 4, MEMORY_PAK | RUMBLE_PAK }, // Jeremy McGrath Supercross 2000 (U) [!]
        { 0xBB53C809269594C6, 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (U) (M10) [b1][f1] (PAL)
        { 0xBB752DA730D8BEF4, 4, 0 }, // LaC's Universal Bootemu V1.1 (PD)
        { 0xBB787C1378C1605D, 4, 0 }, // Pip's RPGs Beta x (PD)
        { 0xBBC99D32117DAA80, 2, MEMORY_PAK | RUMBLE_PAK }, // Twisted Edge Extreme Snowboarding (U) [!]
        { 0xBBFDEC37D93B9EC0, 1, RUMBLE_PAK }, // F-Zero X Expansion Kit (J) [CART HACK]
        { 0xBC3C9F6C93B3CB13, 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [t1]
        { 0xBC47A8BF5529DE0F, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Tennis (U) [f1] (Save)
        { 0xBC4F2AB8AA99E32E, 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Captain Buzz Lightyear auf Rettungsmission! (G) (V1.1) [!]
        { 0xBC875B608C993DF1, 4, MEMORY_PAK | RUMBLE_PAK }, // Xena Warrior Princess - The Talisman of Fate (U) [f1] (PAL)
        { 0xBC9B2CC34ED04DA5, 2, RUMBLE_PAK }, // StarCraft 64 (Beta) [f3] (Country Code)
        { 0xBCB1F89F060752A2, 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.3) [!]
        { 0xBCC831456B7F5DA6, 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ger) [f1] (NTSC)
        { 0xBCEAF9B7F84FF876, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (U) [t1]
        { 0xBCF33AADBF3C37EC, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.2) [t2] (All Attacks Hurt P1)
        { 0xBCFACCAAB814D8EF, 4, MEMORY_PAK | RUMBLE_PAK }, // Bassmasters 2000 (U) [b1]
        { 0xBD1263E5388C9BE7, 4, 0 }, // HSD Quick Intro (PD)
        { 0xBD13F63602079573, 1, RUMBLE_PAK }, // Lode Runner 3-D (U) [t1]
        { 0xBD58346E67E74459, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (F) [f1] (NTSC)
        { 0xBD636D6A5D1F54BA, 4, MEMORY_PAK }, // World Cup 98 (U) (M8) [!]
        { 0xBD8E030433E3998A, 2, RUMBLE_PAK }, // Ganbare Goemon - Derodero Douchuu Obake Tenkomori (J) [t1]
        { 0xBD8E206D98C35E1C, 1, MEMORY_PAK }, // Doubutsu no Mori (J) [T-Eng2007-03-22_Brandon Dixon]
        { 0xBDA75F12BDDC1661, 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.0) [t2]
        { 0xBDA8F143B1AF2D62, 4, MEMORY_PAK | RUMBLE_PAK }, // Quake II (U) [!]
        { 0xBDF9766DBD068D70, 4, MEMORY_PAK | RUMBLE_PAK }, // ECW Hardcore Revolution (U) [!]
        { 0xBE37CDC8A88CB420, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park Rally (U) [t2]
        { 0xBE4AF7A014E3D32C, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.10
        { 0xBE5973E089B0EDB8, 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (U) [!]
        { 0xBE76EDFF20452D09, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 99 (U) [!]
        { 0xBEADC61C8622D9F6, 4, MEMORY_PAK | RUMBLE_PAK }, // Xena Warrior Princess - The Talisman of Fate (U) [t1]
        { 0xBEBAB67751B0B5E4, 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (E) [!]
        { 0xBF4317E22DF987E2, 3, MEMORY_PAK | RUMBLE_PAK }, // Rampage 2 - Universal Tour (U) [t1]
        { 0xBF5D6D653F4A9C03, 1, RUMBLE_PAK }, // Chopper Attack (U) [t1]
        { 0xBF79934539FF7A02, 4, 0 }, // Legend of Zelda, The - Majora's Mask - Preview Demo (U) [!]
        { 0xBF9D0FB0981C22D1, 4, 0 }, // Pip's Porn Pack 2 by Mr. Pips (POM '99) (PD)
        { 0xBFA526B40691E430, 1, 0 }, // Mischief Makers (U) (V1.1) [!]
        { 0xBFE23884EF48EAAF, 1, RUMBLE_PAK }, // Space Station Silicon Valley (U) [f2] (PAL)
        { 0xBFF7B1C2AEBF148E, 4, 0 }, // Doraemon - Nobita to 3tsu no Seireiseki (J) [b3]
        { 0xC006E3C0A67B4BBB, 2, RUMBLE_PAK }, // F-1 World Grand Prix (E) [!]
        { 0xC00CA9488E60D34B, 4, 0 }, // South Park - Chef's Luv Shack (U) [!]
        { 0xC0C8504661051B05, 1, 0 }, // Pokemon Snap (I) [b1]
        { 0xC0DE0747A2DF72D3, 4, MEMORY_PAK | RUMBLE_PAK }, // Puzzle Bobble 64 (J) [!]
        { 0xC0F6DB1780E0D532, 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (G) [!]
        { 0xC1373DC8ED88D197, 1, RUMBLE_PAK }, // Super Mario 64 - Shindou Edition (J) [T+Kor1.0_Minio]
        { 0xC14525535D7B24D9, 2, MEMORY_PAK | RUMBLE_PAK }, // Supercross 2000 (U) [b1]
        { 0xC151AD61280FFF22, 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (U) (V1.1) [!]
        { 0xC16C421BA21580F7, 1, MEMORY_PAK }, // Disney's Donald Duck - Goin' Quackers (U) [!]
        { 0xC19EA928226B2610, 4, 0 }, // South Park - Chef's Luv Shack (U) [f1] (Country Check)
        { 0xC1D702BD6D416547, 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.0) [!]
        { 0xC252F9B1AD70338C, 4, 0 }, // Pong V0.01 by Omsk (PD)
        { 0xC2751D1AF8C19BFF, 4, RUMBLE_PAK }, // Snowboard Kids 2 (E) [!]
        { 0xC27F7CEC85E0806F, 2, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja 2 Starring Goemon (E) (M3) [t1]
        { 0xC29FF9E4264BFE7D, 2, MEMORY_PAK | RUMBLE_PAK }, // Rampage - World Tour (U) [h1C]
        { 0xC2B35C2F5CD995A2, 4, 0 }, // Birthday Demo for Steve by Nep (PD)
        { 0xC2E9AA9A475D70AA, 4, RUMBLE_PAK }, // Banjo-Tooie (U) [!]
        { 0xC3193C1653A19708, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (U) [t1]
        { 0xC34304AC2D79C021, 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (U) [o1]
        { 0xC3AB938DD48143B2, 4, 0 }, // The Corporation 2nd Intro by TS_Garp (PD)
        { 0xC3B6DE9D65D2DE76, 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.0) [b2]
        { 0xC3CA0BA325397DC8, 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.1) [t1] (Energy)
        { 0xC3CD76FF9B9DCBDE, 4, MEMORY_PAK | RUMBLE_PAK }, // Forsaken 64 (G) [o1]
        { 0xC3E7E29E5D7251CC, 4, MEMORY_PAK | RUMBLE_PAK }, // Re-Volt (E) (M4) [!]
        { 0xC3F1915965D2BC5A, 4, MEMORY_PAK }, // FIFA Soccer 64 (U) (M3) [o1]
        { 0xC43E23A740B1681A, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball '99 (U) [o1]
        { 0xC4855DA283BBA182, 4, 0 }, // Namp64 - N64 MP3-Player by Obsidian (PD)
        { 0xC49ADCA2F1501B62, 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (U) [b1]
        { 0xC52E0BC656BC6556, 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack (J) (V1.1) [!]
        { 0xC535091FD60CCF6C, 1, RUMBLE_PAK }, // Body Harvest (U) [t1]
        { 0xC53EDC41ECE19359, 1, MEMORY_PAK }, // Mahjong 64 (J) [o2]
        { 0xC541EAB47397CB5F, 4, 0 }, // Sample Demo by Florian (PD)
        { 0xC56741600F5F453C, 4, RUMBLE_PAK }, // Mario Party 3 (E) (M4) [!]
        { 0xC5C4F0D54EEF2573, 4, 0 }, // Psychodelic Demo by Ste (POM '98) (PD)
        { 0xC5F020F327B2CA73, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 98 (E) [f1] (NTSC)
        { 0xC5F1DE795D4BEB6E, 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.1) [!]
        { 0xC5F4865F03340B83, 2, MEMORY_PAK }, // Wave Race 64 (U) (V1.0) [t1]
        { 0xC603175EACADF5EC, 0, 0 }, // Sinus (PD)
        { 0xC63B1ADA9CEAEB08, 4, RUMBLE_PAK }, // Mario Party 2 (U) [f2] (PAL)
        { 0xC6C71B886422C633, 4, RUMBLE_PAK }, // Mario Party 2 (U) [f1] (PAL)
        { 0xC6CE0AAAD117F019, 4, 0 }, // J.League Tactics Soccer (J) (V1.1) [!]
        { 0xC6E39C0AD2726676, 1, RUMBLE_PAK }, // F-Zero X Expansion Kit (U) [CART HACK]
        { 0xC71353BEAA09A6EE, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF WrestleMania 2000 (E) [h1C]
        { 0xC73AD01648C5537D, 1, MEMORY_PAK }, // Jangou Simulation Mahjong Do 64 (J) [b3]
        { 0xC750196806533B11, 2, RUMBLE_PAK }, // StarCraft 64 (E) [f1] (NTSC)
        { 0xC76B18F6AAB1BDF3, 1, RUMBLE_PAK }, // Mission Impossible (I) [f1] (NTSC)
        { 0xC788DCAEBD03000A, 4, MEMORY_PAK }, // NBA Hangtime (E) [h1C]
        { 0xC7C3086D93826E6E, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Dut]
        { 0xC7C98F8E42145DDE, 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (U) [!]
        { 0xC7F15051EE3DEE75, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.30
        { 0xC7F30CFAECB0FA36, 1, RUMBLE_PAK }, // Star Wars - Rogue Squadron (U) (M3) (V1.1) [!]
        { 0xC811CBB18FB7617C, 4, 0 }, // Mind Present Demo 0 by Widget and Immortal (POM '98) (PD)
        { 0xC830954A29E257FC, 4, 0 }, // Pip's RPGs Beta 3 by Mr. Pips (PD)
        { 0xC83CEB83FDC56219, 4, MEMORY_PAK }, // Penny Racers (E) [h2C]
        { 0xC851961C78FCAAFA, 1, 0 }, // Pilotwings 64 (U) [h1C]
        { 0xC8BB4DD9CC5F430B, 1, MEMORY_PAK }, // Quest 64 (U) [b5]
        { 0xC8DC65EB3D8C8904, 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (U) [b3]
        { 0xC8FEF05DDAA3407D, 2, MEMORY_PAK | RUMBLE_PAK }, // World Driver Championship (U) [t1]
        { 0xC9011D05EF078B8C, 4, 0 }, // RADWAR 2K Party Inv. Intro by Ayatolloh (PD)
        { 0xC9176D39EA4779D1, 4, RUMBLE_PAK }, // Banjo-Tooie (E) (M4) [!]
        { 0xC954B38C6F62BEB3, 4, MEMORY_PAK }, // Jikkyou World Soccer - World Cup France '98 (J) (V1.1) [!]
        { 0xC98C4580A8C84DAD, 1, MEMORY_PAK }, // Premier Manager 64 (E) [f2] (NTSC100%)
        { 0xC99936D123D1D65D, 4, 0 }, // Japan Pro Golf Tour 64 (J) [CART HACK]
        { 0xC9C3A9875810344C, 4, MEMORY_PAK }, // Mario Kart 64 (J) (V1.1) [b1][T+Ita_Cattivik66]
        { 0xCA12B54771FA4EE4, 1, 0 }, // Pokemon Snap (U) [T+Spa]
        { 0xCA1BB86F41CCA5C5, 4, RUMBLE_PAK }, // Hoshi no Kirby 64 (J) (V1.1) [!]
        { 0xCA2A744471DAB71C, 2, MEMORY_PAK | RUMBLE_PAK }, // California Speed (E) (Prototype)
        { 0xCA3BC0959649860A, 4, 0 }, // Wet Dreams Can Beta Demo by Immortal (POM '99) (PD)
        { 0xCA69ECE513A88244, 4, 0 }, // SNES 9X Alpha by Loom-Crazy Nation (PD)
        { 0xCADEA28346EAF060, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.09
        { 0xCB1ACDDECF291DF2, 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 (U) (M7) [o1]
        { 0xCB3FF5548773CD0B, 4, 0 }, // TRON Demo (PD)
        { 0xCB93DB977F5C63D5, 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz L'eclair A La Rescousse! (F) [!]
        { 0xCBD838286FDEBEDC, 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [t1]
        { 0xCBF72BBD5D1587B2, 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [t1]
        { 0xCBFE69C7F2C0AB2A, 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (U) [!]
        { 0xCC3CC8B30EC405A4, 2, RUMBLE_PAK }, // F-1 World Grand Prix (U) [h2C]
        { 0xCCCC821E96E88A83, 1, 0 }, // Mahjong Hourouki Classic (J) [b3]
        { 0xCCDEAF028AFC4B28, 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (E) [f1] (NTSC)
        { 0xCCE336FE8F3975BC, 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [b3]
        { 0xCCEB385826952D97, 1, MEMORY_PAK | RUMBLE_PAK }, // Toy Story 2 - Buzz Lightyear to the Rescue! (E) [!]
        { 0xCCEDB696D3883DB4, 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (J) [!]
        { 0xCD09423588074B62, 4, MEMORY_PAK | RUMBLE_PAK }, // Virtual Pro Wrestling 2 - Oudou Keishou (J) [!]
        { 0xCD29E6E54533F032, 4, 0 }, // UltraSMS V1.0 by Jos Kwanten (PD) [f1] (V64)
        { 0xCD3C3CDF317793FA, 4, 0 }, // Nintama Rantarou 64 Game Gallery (J) [!]
        { 0xCD538CE4618AFCF9, 1, MEMORY_PAK | RUMBLE_PAK }, // Chameleon Twist 2 (U) [!]
        { 0xCD5BEC0F86FD1008, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF - War Zone (U) [!]
        { 0xCD7559ACB26CF5AE, 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.1) [!]
        { 0xCDB8B4D08832352D, 4, RUMBLE_PAK }, // Jet Force Gemini (U) [b2]
        { 0xCDB998BE1024A5C8, 2, MEMORY_PAK | RUMBLE_PAK }, // Major League Baseball Featuring Ken Griffey Jr. (E) [!]
        { 0xCDCCFF871D76A49E, 2, RUMBLE_PAK }, // Magical Tetris Challenge (U) [hI]
        { 0xCDDB4BDF84E5EFAC, 4, MEMORY_PAK | RUMBLE_PAK }, // Getter Love!! (J) [T+Eng1.01_ozidual]
        { 0xCE84793D27ECC1AD, 4, RUMBLE_PAK }, // Donkey Kong 64 (U) [f2]
        { 0xCEA8B54F7F21D503, 4, MEMORY_PAK }, // Wetrix (U) (M6) [!]
        { 0xCED986FD3344AC38, 4, RUMBLE_PAK }, // Donkey Kong 64 (U) [f3]
        { 0xCEDCDE1E513A0502, 2, MEMORY_PAK }, // Bust-A-Move 2 - Arcade Edition (E) [!]
        { 0xCEE5C8CDD3D85466, 4, 0 }, // Friendship Demo by Renderman (PD)
        { 0xCF1388792A6144CB, 1, RUMBLE_PAK }, // Densha de Go! 64 (J) [f1] (PAL)
        { 0xCF84F45F00E4F6EB, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 99 (E) (M5) [!]
        { 0xCF8957AD96D57EA9, 2, MEMORY_PAK }, // Power Rangers - Lightspeed Rescue (U) [!]
        { 0xCF8EC9017EC2C48D, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [b2]
        { 0xCFBEE39B76F0A14A, 4, 0 }, // Pom Part 4 Demo (PD)
        { 0xCFE2CB314D6B1E1D, 1, MEMORY_PAK }, // Parlor! Pro 64 - Pachinko Jikki Simulation Game (J) [!]
        { 0xCFEAACE91D532607, 1, 0 }, // Dexanoid R1 by Protest Design (PD) [f1] (PAL)
        { 0xCFEF2CD6C9E973E6, 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (J) [!]
        { 0xD0151AB0FE5CA14B, 4, MEMORY_PAK | RUMBLE_PAK }, // John Romero's Daikatana (U) [!]
        { 0xD03F0CCB11F19CDD, 4, MEMORY_PAK | RUMBLE_PAK }, // Kobe Bryant in NBA Courtside (E) [f1]
        { 0xD06002A72243C636, 4, 0 }, // GameBooster 64 V1.1 (PAL) (Unl) [b1]
        { 0xD072CFE7CE134788, 4, 0 }, // Pip's Pong by Mr. Pips (PD)
        { 0xD094B170D7C4B5CC, 2, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz (U) [!]
        { 0xD09BA5381C1A5489, 4, RUMBLE_PAK }, // Top Gear Overdrive (E) [h1C]
        { 0xD0A1FC5B2FB8074B, 4, TRANSFER_PAK }, // Pokemon Stadium 2 (S) [!]
        { 0xD124DBB77C9FF5E5, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (E) (Beta) (2000-05-31)
        { 0xD137A2CA62B65053, 1, RUMBLE_PAK }, // Itoi Shigesato no Bass Tsuri No. 1 Kettei Ban! (J) [b1]
        { 0xD1A78A0752A3DD3E, 4, MEMORY_PAK | RUMBLE_PAK }, // CyberTiger (E) [!]
        { 0xD1C6C55DF010EF52, 4, 0 }, // WideBoy BIOS V980910
        { 0xD1F7D8AB293B0446, 4, MEMORY_PAK | RUMBLE_PAK }, // Asteroids Hyper 64 (U) [o1]
        { 0xD2015E56A9FE0CE6, 4, 0 }, // Mempack Manager for Jr 0.9c by deas (PD)
        { 0xD21AF769DE1A0E3D, 4, MEMORY_PAK | RUMBLE_PAK }, // South Park (B) [!]
        { 0xD22943DAAC2B77C0, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 5 (J) (V1.0) [b1]
        { 0xD250103E136A0F0C, 4, MEMORY_PAK | RUMBLE_PAK }, // RTL World League Soccer 2000 (G) [f1][o1]
        { 0xD25C121113EEBF67, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 3 - Shadow of Oblivion (U) (Beta) [h1C]
        { 0xD2B908C8E0E73A1D, 4, 0 }, // Congratulations Demo for SPLiT by Widget and Immortal (PD)
        { 0xD2BE2F1438453788, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (U) [b2]
        { 0xD2F75C12E89EB415, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t3] (All Attacks Hurt P2)
        { 0xD32264A1989987EA, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.20
        { 0xD3D806FCB43AA2A8, 4, RUMBLE_PAK }, // Monster Truck Madness 64 (E) (M5) [!]
        { 0xD3DF2FEE8090B699, 4, RUMBLE_PAK }, // Snowboard Kids 2 (U) [f2] (PAL)
        { 0xD3E792A505D39C97, 1, 0 }, // Star Wars - Shadows of the Empire (U) (V1.2) [b2]
        { 0xD3F10E5D052EA579, 1, RUMBLE_PAK }, // Hey You, Pikachu! (U) [!]
        { 0xD3F52BA51258EDFE, 4, RUMBLE_PAK }, // Kirby 64 - The Crystal Shards (U) [f1]
        { 0xD3F97D496924135B, 1, RUMBLE_PAK }, // Yoshi's Story (E) (M3) [b3]
        { 0xD43DA81F021E1E19, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.1) [b1]
        { 0xD48944D1B0D93A0E, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf 64 (J) (V1.0) [b3]
        { 0xD49DFF908DB53A8C, 4, 0 }, // Evek - V64jr Save Manager by WT_Riker (PD)
        { 0xD4C45A1AF425B25E, 4, MEMORY_PAK | RUMBLE_PAK }, // WCW Nitro (U) [b1]
        { 0xD4DD9982FDBA7B67, 1, RUMBLE_PAK }, // Yoshi Story (J) [f4]
        { 0xD51CF20D5534AF93, 4, MEMORY_PAK | RUMBLE_PAK }, // Army Men - Sarge's Heroes (U) [b1]
        { 0xD52956729CB57015, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [o1][f1]
        { 0xD52FE29D8EA6A759, 4, 0 }, // Donchan Puzzle Hanabi de Doon! (Aleck64)
        { 0xD543BCD62BA5E256, 4, MEMORY_PAK | RUMBLE_PAK }, // Gauntlet Legends (E) [b1]
        { 0xD571C883822D3FCF, 2, MEMORY_PAK | RUMBLE_PAK }, // Blues Brothers 2000 (E) (M6) [!]
        { 0xD5898CAF6007B65B, 4, MEMORY_PAK | RUMBLE_PAK }, // Operation WinBack (E) (M5) [!]
        { 0xD5B2339CCABCCAED, 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (E) (M5) [!]
        { 0xD5BDCD1D393AFE43, 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [f3] (NTSC)
        { 0xD5CA46C2F8555155, 4, 0 }, // MAME 64 Beta 3 (PD)
        { 0xD614E5BFA76DBCC1, 1, MEMORY_PAK | RUMBLE_PAK }, // Disney's Tarzan (E) [h1C]
        { 0xD666593BD7A25C07, 1, RUMBLE_PAK }, // Rockman Dash (J) [!]
        { 0xD692CC5EEC58D072, 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy's Reckin' Balls (E) [o1]
        { 0xD6D29529D4EADEE4, 2, 0 }, // 77a by Count0 (POM '98) (PD) [b1]
        { 0xD6FBA4A86326AA2C, 1, RUMBLE_PAK }, // Super Mario 64 - Shindou Edition (J) [h2C]
        { 0xD7134F8DC11A00B5, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 2002 (U) [!]
        { 0xD715CC70271CF5D6, 2, 0 }, // War Gods (E) [!]
        { 0xD72FD14D1FED32C4, 2, MEMORY_PAK }, // Bottom of the 9th (U) [b1]
        { 0xD741CD80ACA9B912, 4, RUMBLE_PAK }, // Top Gear Overdrive (U) [o1]
        { 0xD7484C2A56CFF26D, 4, 0 }, // DS1 Manager V1.1 by R. Bubba Magillicutty (PD)
        { 0xD76333AC0CB6219D, 1, RUMBLE_PAK }, // Bass Rush - ECOGEAR PowerWorm Championship (J) [b2]
        { 0xD767D118A1B07602, 4, 0 }, // F-ZERO X (U) [f1] (Sex V1.1 Hack)
        { 0xD7891F1CC3E43788, 4, MEMORY_PAK }, // Jikkyou Powerful Pro Yakyuu 4 (J) (V1.1) [!]
        { 0xD7A6DCFACCFEB6B7, 4, MEMORY_PAK }, // Power League 64 (J) [o1]
        { 0xD7C762B6F83D9642, 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (J) [!]
        { 0xD7F735531347659F, 4, TRANSFER_PAK }, // Pokemon Stadium (G) [f1]
        { 0xD805980492739B11, 1, RUMBLE_PAK }, // Doraemon 3 - Nobita no Machi SOS! (J) [f1] (PAL-NTSC)
        { 0xD81963C74271A3AA, 4, RUMBLE_PAK }, // Chameleon Twist (U) (V1.1) [!]
        { 0xD83045C8F29D3A36, 2, MEMORY_PAK }, // AeroGauge (E) (M3) [h2C]
        { 0xD83BB920CC406416, 1, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Nushi Tsuri 64 (J) (V1.0) [b4]
        { 0xD84C17CA13F8F651, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.1) [f1] (Z64)
        { 0xD84EEA8445B2F1B4, 1, MEMORY_PAK }, // Shadowgate 64 - Trials Of The Four Towers (E) [!]
        { 0xD851B920F3D6C0CE, 4, 0 }, // Pip's Porn Pack 1 by Mr. Pips (PD)
        { 0xD852E2380E52A960, 1, RUMBLE_PAK }, // Chopper Attack (E) [t1]
        { 0xD85C4E2988E276AF, 1, RUMBLE_PAK }, // Bomberman Hero (E) [f1] (NTSC)
        { 0xD89261A9B1A2C9AF, 4, MEMORY_PAK | RUMBLE_PAK }, // WWF Attitude (U) [f1] (PAL)
        { 0xD8928E46965CE432, 1, RUMBLE_PAK }, // Batman Beyond - Return of the Joker (U) [o1][t1]
        { 0xD89BE2F899C97ADF, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 98 (U) [o1]
        { 0xD89E0E55B17AA99A, 1, 0 }, // Starshot - Space Circus Fever (E) (M3) [!]
        { 0xD8C7404929CDD8C4, 4, MEMORY_PAK | RUMBLE_PAK }, // 007 - The World is Not Enough (E) [T+Spa1.0.1_IlDucci]
        { 0xD9042FBBFCFF997C, 4, MEMORY_PAK | RUMBLE_PAK }, // Telefoot Soccer 2000 (F) [!]
        { 0xD9EDD54D6BB8E274, 4, MEMORY_PAK | RUMBLE_PAK }, // All-Star Baseball '99 (E) [!]
        { 0xD9F75C12A8859B59, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [o1][h2C]
        { 0xD9FECC50D3AA8423, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t5] (2x Aggressor)
        { 0xDA0E5024969ED261, 1, MEMORY_PAK }, // Castlevania (E) (M3) [t1]
        { 0xDA233397FF46DF64, 2, MEMORY_PAK | RUMBLE_PAK }, // Monaco Grand Prix (U) [f1] (PAL)
        { 0xDA573DB99442D273, 1, RUMBLE_PAK }, // Bomberman Hero (U) [t1]
        { 0xDAA66890ED4CCD04, 2, 0 }, // Dark Rift (U) [t1]
        { 0xDAA6CA7D56AA4394, 4, RUMBLE_PAK }, // F-ZERO X (E) [f1]
        { 0xDAA76993D8ACF77C, 2, 0 }, // Pong by Oman (PD) [h1C][o1]
        { 0xDACDE68E7381B3C9, 1, 0 }, // Super Mario 64 (U) (Enable Hidden Scroller Hack)
        { 0xDAD7F7518B6322F0, 4, 0 }, // Pip's RPGs Beta 2 by Mr. Pips (PD)
        { 0xDB363DDA50C1C2A5, 4, 0 }, // Pip's RPGs Beta 7 by Mr. Pips (PD)
        { 0xDB6E353B205B0C53, 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [t1][f1] (PAL)
        { 0xDB7184C9F53AC14A, 1, MEMORY_PAK | RUMBLE_PAK }, // Hercules - The Legendary Journeys (U) [t1][f2] (PAL-NTSC)
        { 0xDB833E3463548286, 5, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (U) [t1]
        { 0xDBF4EA9D333E82C0, 4, MEMORY_PAK | RUMBLE_PAK }, // Snowboard Kids (U) [h1C]
        { 0xDC36626A3F3770CB, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem - ZER0 H0UR (E) [!]
        { 0xDC3BAA590ABB456A, 4, MEMORY_PAK }, // Wayne Gretzky's 3D Hockey (U) (V1.1) [!]
        { 0xDC41E81EAC36CAD1, 4, MEMORY_PAK | RUMBLE_PAK }, // Bassmasters 2000 (U) [f1] (PAL)
        { 0xDC649466572FF0D9, 4, MEMORY_PAK }, // Onegai Monsters (J) [b1]
        { 0xDCB6EAFAC6BBCFA3, 4, 0 }, // Wetrix (J) [o1]
        { 0xDCBC50D109FD1AA3, 4, RUMBLE_PAK }, // GoldenEye 007 (U) [o2]
        { 0xDCBCACD1A67DDFB4, 4, 0 }, // GoldenEye 007 (U) (No Music Hack)
        { 0xDCBCACD1D72FAA33, 4, 0 }, // GoldenEye 007 (U) (Frozen Enemies Hack)
        { 0xDCBCACD1E1051EA6, 4, 0 }, // GoldenEye 007 (U) (God Mode Hack)
        { 0xDCBCACD1F294D541, 4, 0 }, // GoldenEye 007 (U) (No Power Bar Hack)
        { 0xDCBE12CDFCCB5E58, 4, MEMORY_PAK }, // VNES64 + Galaga (PD)
        { 0xDCCEDB8FBA79BEDE, 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (E) (M3) [f1] (NTSC)
        { 0xDCCF21349DD63578, 4, MEMORY_PAK }, // Centre Court Tennis (E) [h1C]
        { 0xDD10BC7EF900B351, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 - 2nd Offence (E) [!]
        { 0xDD16F47CA8B748C7, 4, TRANSFER_PAK }, // Mario Artist: Paint Studio (J) [CART HACK] [T+Eng0.2_LuigiBlood]
        { 0xDD26FDA1CB4A6BE3, 4, RUMBLE_PAK }, // Super Smash Bros. (A) [!]
        { 0xDD318CE2B73798BA, 4, RUMBLE_PAK }, // Waialae Country Club - True Golf Classics (U) (V1.1) [!]
        { 0xDD95F49D2A9B8893, 4, 0 }, // NBC-LFC Kings of Porn Vol 01 (PD) [a1]
        { 0xDDBA4DE5B107004A, 0, 0 }, // Mupen64Plus Demo by Marshallh (GPL)
        { 0xDDD93C85DAE381E8, 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (U) [!]
        { 0xDDD982EF3B0D6FEC, 1, RUMBLE_PAK }, // Star Soldier - Vanishing Earth (U) [t1]
        { 0xDDEF1333E7209D07, 1, RUMBLE_PAK }, // Bomberman Hero - Mirian Oujo wo Sukue! (J) [t1]
        { 0xDDF460CC3CA634C0, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (U) (V1.0) [!]
        { 0xDE4E5E2EDCD0615D, 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (J) [f1] (PAL)
        { 0xDEB78BBA52F6BD9D, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (U) [o1]
        { 0xDED0DD9AE78225A7, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mickey's Speedway USA (E) (M5) [!]
        { 0xDEE584A20F161187, 4, MEMORY_PAK | RUMBLE_PAK }, // Destruction Derby 64 (U) [!]
        { 0xDEE596ABAF3B7AE7, 4, RUMBLE_PAK }, // WCW-nWo Revenge (U) [b2]
        { 0xDF09D6251791C87D, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 99 (U) (V1.1) [!]
        { 0xDF0C666A1F52B368, 4, RUMBLE_PAK }, // Mario Party 3 (U) [f3]
        { 0xDF331A185FD4E044, 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 2000 (U) [!]
        { 0xDF5741919EB5123D, 1, 0 }, // Earthworm Jim 3D (U) [!]
        { 0xDF6FF0F429D14238, 4, 0 }, // Bomberman 64 - Arcade Edition (J) [b1]
        { 0xDF98B95D58840978, 1, RUMBLE_PAK }, // Bakuretsu Muteki Bangai-O (J) [h1C]
        { 0xDFD784ADAE426603, 4, 0 }, // All Star Tennis '99 (E) (M5) [!]
        { 0xDFD8AB473CDBEB89, 4, RUMBLE_PAK }, // Jet Force Gemini (U) (Kiosk Demo) [b2]
        { 0xDFE61153D76118E6, 4, RUMBLE_PAK }, // Cruis'n World (U) [o1]
        { 0xDFF227D90D4D8169, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (G) [!]
        { 0xE0044E9ECD659D0D, 1, 0 }, // Pocket Monsters Snap (J) (VC) [!]
        { 0xE0144180650B78C9, 2, MEMORY_PAK | RUMBLE_PAK }, // Tony Hawk's Pro Skater (U) (V1.1) [!]
        { 0xE09DAF6C8A2B86F4, 4, RUMBLE_PAK }, // GoldenEye 007 (U) [t1] (Rapid Fire)
        { 0xE0A79F8C32CC97FA, 4, MEMORY_PAK }, // Jikkyou World Soccer 3 (J) [!]
        { 0xE0B92B9480E87CBD, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (E) [b1]
        { 0xE0B92B94B9A7E025, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (V1.1) [!]
        { 0xE0C4F72F769E1506, 1, 0 }, // Tigger's Honey Hunt (E) (M7) [!]
        { 0xE13AE2DC4FB65CE8, 1, MEMORY_PAK }, // Eltale Monsters (J) [b2]
        { 0xE185E2914E50766D, 4, 0 }, // All Star Tennis '99 (U) [h1C]
        { 0xE1AAA92527C9C94B, 2, 0 }, // Pokemon Puzzle League (U) [t1]
        { 0xE1C7ABD64E707F28, 4, MEMORY_PAK }, // Jikkyou World Soccer - World Cup France '98 (J) (V1.2) [!]
        { 0xE1EF93F714908B0B, 2, RUMBLE_PAK }, // Magical Tetris Challenge (G) [!]
        { 0xE2A3204946E8F8E2, 4, RUMBLE_PAK }, // Cruis'n World (U) [f1]
        { 0xE2BC82A2591CD694, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (F) [b1]
        { 0xE2D37CF0F57E4EAE, 4, MEMORY_PAK }, // International Superstar Soccer 64 (E) [h2C]
        { 0xE328B4FA004A28E1, 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move 3 DX (E) [b3]
        { 0xE340A49C74318D41, 4, MEMORY_PAK }, // Baku Bomberman (J) [h1C]
        { 0xE36166C28613A2E5, 4, MEMORY_PAK | RUMBLE_PAK }, // Michael Owens WLS 2000 (E) [b1]
        { 0xE378B28A0CA18BCC, 1, RUMBLE_PAK }, // Lode Runner 3-D (E) (M5) [b1]
        { 0xE3AB4ED083040DD2, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Quarterback Club 2000 (U) [b2]
        { 0xE3BD221D3C0834D3, 1, MEMORY_PAK }, // Scooby-Doo! - Classic Creep Capers (E) [!]
        { 0xE3CCFE36398FBC75, 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [f1]
        { 0xE3D6A7952A1C5D3C, 2, MEMORY_PAK | RUMBLE_PAK }, // Knockout Kings 2000 (E) [!]
        { 0xE402430DD2FCFC9D, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.1) [!]
        { 0xE436467A82DE8F9B, 2, RUMBLE_PAK }, // Indy Racing 2000 (U) [!]
        { 0xE43C976505B1C1BE, 1, MEMORY_PAK }, // Wonder Project J2 - Koruro no Mori no Jozet (J) [h1C]
        { 0xE470469DE105C984, 4, MEMORY_PAK }, // Mario Kart 64 (E) (V1.0) (Super W00ting Hack)
        { 0xE48E01F5E6E51F9B, 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (E) (M4) (Eng-Spa-Fre-Ita) [b1]
        { 0xE49066799F243F05, 2, RUMBLE_PAK }, // Magical Tetris Challenge (E) [b1]
        { 0xE4B08007A602FF33, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (E) (M5) [b1]
        { 0xE4B35E4C1AC45CC9, 2, MEMORY_PAK }, // Midway's Greatest Arcade Hits Volume 1 (U) [o1]
        { 0xE4C44FDA98532F4A, 1, 0 }, // Analogue Test Utility by WT_Riker (POM '99) (PD)
        { 0xE584FE349D91B1E2, 1, 0 }, // Sporting Clays by Charles Doty (PD) [b1]
        { 0xE5B7F4052C4462BF, 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (V1.0) [f2] (Save)
        { 0xE600831E59F422A8, 4, MEMORY_PAK }, // NBA Jam 99 (E) [h1C]
        { 0xE606EE268783EC43, 1, RUMBLE_PAK }, // Yoshi's Story (U) (M2) [t2] (Health and Eggs)
        { 0xE616B5BCC9658B88, 2, MEMORY_PAK | RUMBLE_PAK }, // Iggy's Reckin' Balls (U) [o1]
        { 0xE61CFF0ACE1C0D71, 4, MEMORY_PAK }, // New Tetris, The (E) [!]
        { 0xE6419BC569011DE3, 1, MEMORY_PAK }, // Ogre Battle 64 - Person of Lordly Caliber (U) (V1.0) [!]
        { 0xE655B42DF22370DD, 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (U) [t1] (P1 Untouchable)
        { 0xE688A5B8B14B3F18, 2, MEMORY_PAK | RUMBLE_PAK }, // Twisted Edge Extreme Snowboarding (E) [h1C]
        { 0xE68960186013AD31, 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (U) [t1]
        { 0xE68A000E639166DD, 1, MEMORY_PAK }, // Gex 64 - Enter the Gecko (E) [!]
        { 0xE68A9F6E24FB3315, 1, MEMORY_PAK | RUMBLE_PAK }, // Gex 3 - Deep Cover Gecko (U) [f1] (PAL)
        { 0xE6A95A4FBAD2EA23, 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (V1.0) [o1]
        { 0xE6B339F622AE6901, 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (U) [f1] (PAL)
        { 0xE6BA0A068A3D2C2F, 4, MEMORY_PAK }, // Hexen (U) [t2]
        { 0xE6FCB468CFAC7528, 2, RUMBLE_PAK }, // Magical Tetris Challenge Featuring Mickey (J) [b1]
        { 0xE73C7C4FAF93B838, 4, MEMORY_PAK | RUMBLE_PAK }, // Baku Bomberman 2 (J) [!]
        { 0xE742871897751A86, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [f1]
        { 0xE74607D1751139B4, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.21
        { 0xE78F66E6754ABDF5, 4, MEMORY_PAK }, // NBA Showtime - NBA on NBC (U) [f1] (Country Check)
        { 0xE7BDA0BEADA09CAC, 4, 0 }, // Kyojin no Doshin 1 (J) (Kiosk Demo) [CART HACK]
        { 0xE7D20193C1158E93, 2, MEMORY_PAK | RUMBLE_PAK }, // Hot Wheels Turbo Racing (E) (M3) [b1]
        { 0xE820670AA99ED0EF, 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (U) [t1]
        { 0xE8522F0443FA513A, 2, MEMORY_PAK | RUMBLE_PAK }, // Buck Bumble (U) [b2]
        { 0xE857ED956028F9AF, 2, RUMBLE_PAK }, // Star Wars Episode I - Racer (E) [T+Spa1.0.2_IlDucci]
        { 0xE86415A698395B53, 1, MEMORY_PAK | RUMBLE_PAK }, // O.D.T. (E) (M5) [!]
        { 0xE876170226AD5DBE, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mario Golf (U) [b1]
        { 0xE87D098B4C70341C, 4, 0 }, // Neon64 V1.0 by Halley's Comet Software (PD)
        { 0xE87F1ACE809F204B, 4, 0 }, // Bomberman 64 - Arcade Edition (J) [f2] (PAL-CRC)
        { 0xE896092BDC244D4E, 4, RUMBLE_PAK }, // Wheel of Fortune (U) [o1][b1]
        { 0xE8B3F63DE5A997B1, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // GoldenEye X
        { 0xE8BF8416F2D9DA43, 4, 0 }, // TopGun Demo by Horizon64 (PD)
        { 0xE8C95AFC35D121DA, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (U) (Kiosk Demo) [h1C]
        { 0xE8D29DA015E61D94, 4, 0 }, // J.League Tactics Soccer (J) (V1.0) [b1]
        { 0xE8D2A5005AA8DBCA, 4, 0 }, // J.League Tactics Soccer (J) (V1.0) [f1] (PAL)
        { 0xE8DBC96ADAC9E2EE, 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (U) [t1]
        { 0xE8E5B17944AA30E8, 4, MEMORY_PAK }, // Frogger 2 (U) (Alpha) [o2]
        { 0xE8E8DD70415DD198, 4, MEMORY_PAK }, // Morita Shougi 64 (J) [!]
        { 0xE8FC8EA19F738391, 4, MEMORY_PAK | RUMBLE_PAK }, // CyberTiger (U) [!]
        { 0xE921953313FBAFBD, 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing - Round 2 (U) [!]
        { 0xE972B3857668D414, 1, 0 }, // Tigger's Honey Hunt (U) [t1]
        { 0xE97955C6BC338D38, 1, RUMBLE_PAK }, // Legend of Zelda, The - Majora's Mask (E) (M4) (V1.0) [T-Ita0.9f_Vampire]
        { 0xE9C8E262A1D5FACA, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (U) (M2) (V1.0) [f3]
        { 0xE9F523366BEFAA5F, 4, 0 }, // Plasma Demo (PD)
        { 0xEA06F8C307C2DEED, 1, MEMORY_PAK | RUMBLE_PAK }, // Shadow Man (F) [!]
        { 0xEA090572192F9673, 4, MEMORY_PAK | RUMBLE_PAK }, // Armorines - Project S.W.A.R.M. (U) [f1] (PAL)
        { 0xEA2A6A7552B2C00F, 4, 0 }, // Pip's Porn Pack 3 by Mr. Pips (PD)
        { 0xEA630EC72B2F37F3, 1, RUMBLE_PAK }, // Command & Conquer (E) (M2) [f1] (Z64)
        { 0xEA6D4C3F13837E54, 4, RUMBLE_PAK }, // Super Smash Bros. (U) [f4] (GameShark)
        { 0xEA6D5BF8E2B4696C, 4, 0 }, // GameShark Pro V3.3 (Apr 2000) (Unl) [!]
        { 0xEA71056AE4214847, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 (U) [b2]
        { 0xEAB7B429BAC92C57, 2, MEMORY_PAK | RUMBLE_PAK }, // Ready 2 Rumble Boxing (U) [!]
        { 0xEAC074190F9F72A0, 2, RUMBLE_PAK }, // Ken Griffey Jr.'s Slugfest (U) [f4] (Nosave-Z64)
        { 0xEAE6ACE2020B4384, 1, RUMBLE_PAK }, // Star Wars Episode I - Battle for Naboo (E) [!]
        { 0xEB38F792190EA246, 4, MEMORY_PAK | RUMBLE_PAK }, // Madden NFL 2001 (U) [!]
        { 0xEB499C8FCD4567B6, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Live 2000 (E) (M4) [!]
        { 0xEB7709E227A91031, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2001 (U) [f1] (PAL-NTSC)
        { 0xEBA949DC39BAECBD, 1, RUMBLE_PAK }, // Mission Impossible (I) [!]
        { 0xEBE8AA17B1EF65E6, 4, 0 }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) (Room121 Hack)
        { 0xEBEEA8DBF2ECB23C, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Jam 2000 (U) [!]
        { 0xEBF5F6B7C956D739, 2, MEMORY_PAK | RUMBLE_PAK }, // SD Hiryuu no Ken Densetsu (J) [f1] (PAL)
        { 0xEBFE2397FF74DA34, 2, MEMORY_PAK | RUMBLE_PAK }, // Space Invaders (U) [!]
        { 0xEC0F690D32A7438C, 1, 0 }, // Pocket Monsters Snap (J) [!]
        { 0xEC417312EB31DE5F, 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.0) [o1]
        { 0xEC58EABFAD7C7169, 4, RUMBLE_PAK }, // Donkey Kong 64 (U) [b1]
        { 0xEC6F66AC618EF742, 2, RUMBLE_PAK }, // Tom and Jerry in Fists of Furry (E) (M6) [f1] (NTSC)
        { 0xEC7011B77616D72B, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [b1]
        { 0xEC986EA9EB463E4A, 4, RUMBLE_PAK }, // F-ZERO X (E) [b3]
        { 0xEC9BECFFCAB83632, 4, 0 }, // SRAM Uploader-Editor by BlackBag (PD)
        { 0xECAEC238EE351DDA, 4, 0 }, // Fireworks Demo by CrowTRobo (PD)
        { 0xECBD95DD1FAB637D, 4, 0 }, // Sydney 2000 (E) (Prototype)
        { 0xED42A2D47A71CD91, 4, 0 }, // Ultra Demo by Locke^ (PD)
        { 0xED567D0F38B08915, 4, RUMBLE_PAK }, // Mario Party 2 (J) [!]
        { 0xED98957E8242DCAC, 4, MEMORY_PAK | RUMBLE_PAK }, // WinBack - Covert Operations (U) [b1]
        { 0xEDA1A0C758EE0464, 4, 0 }, // Chaos 89 Demo (PD)
        { 0xEDAFD3C039EF3599, 4, 0 }, // POMolizer Demo by Renderman (POM '99) (PD)
        { 0xEDD6E03168136013, 2, MEMORY_PAK | RUMBLE_PAK }, // Rush 2 - Extreme Racing USA (U) [!]
        { 0xEDF419A8BF1904CC, 4, MEMORY_PAK | RUMBLE_PAK }, // Beetle Adventure Racing! (U) (M3) [b2]
        { 0xEDFB6B018893F62C, 4, MEMORY_PAK | RUMBLE_PAK }, // Hydro Thunder (E) [f1] (NTSC)
        { 0xEE08C6026BC2D5A6, 4, 0 }, // PGA European Tour (E) (M5) [!]
        { 0xEE4A0E338FD588C9, 2, MEMORY_PAK | RUMBLE_PAK }, // GT 64 - Championship Edition (E) (M3) [!]
        { 0xEE4FD7C29CF1D938, 4, TRANSFER_PAK }, // Pocket Monsters Stadium Kin Gin (J) [!]
        { 0xEE7AF9ECA750B2D3, 1, RUMBLE_PAK }, // Zelda no Densetsu - Mujura no Kamen (J) (V1.0) [f1]
        { 0xEE802DC4690BD57D, 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (J) [!]
        { 0xEEE48FAF6A92BAFF, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (E) (M3) (V1.0) [f1] (zpfc)
        { 0xEEF7DE2357E4FEA9, 4, MEMORY_PAK | RUMBLE_PAK }, // Superman (U) (M3) [f1] (PAL)
        { 0xEF623F50ECBB509B, 4, 0 }, // LaC's Universal Bootemu V1.0 (PD)
        { 0xEF62A34311E41E37, 4, 0 }, // Dr. Mario 64 (Ch) (iQue) [!]
        { 0xEF703CA44D4A9AC9, 4, MEMORY_PAK }, // Shin Nihon Pro Wrestling - Toukon Road - Brave Spirits (J) [b1]
        { 0xEF9E9714C03B2C7D, 1, MEMORY_PAK }, // Tonic Trouble (U) (V1.1) [b1]
        { 0xEFCEAF0022094848, 4, TRANSFER_PAK }, // Pokemon Stadium 2 (I) [!]
        { 0xEFDAFEA4E38D6A80, 4, 0 }, // NUTS - Nintendo Ultra64 Test Suite by MooglyGuy (PD)
        { 0xEFDF9140A4168D6B, 4, MEMORY_PAK | RUMBLE_PAK }, // Top Gear Rally 2 (Beta)
        { 0xF00F2D4E340FAAF4, 2, MEMORY_PAK | RUMBLE_PAK }, // Carmageddon 64 (U) [!]
        { 0xF034001AAE47ED06, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time - Master Quest (U) (GC) [!]
        { 0xF03C24CAC5237BCC, 4, 0 }, // Clay Fighter 63 1-3 (U) [o2]
        { 0xF04A729C19971680, 4, RUMBLE_PAK }, // Rugrats - Scavenger Hunt (U) [f1] (PAL)
        { 0xF050746C247B820B, 4, MEMORY_PAK | RUMBLE_PAK }, // Road Rash 64 (U) [!]
        { 0xF050D644B5A5CED1, 1, RUMBLE_PAK }, // In-Fisherman Bass Hunter 64 (U) [f1] (PAL)
        { 0xF0C5F320B22773CA, 4, MEMORY_PAK | RUMBLE_PAK }, // NHL Breakaway 99 (U) [b2]
        { 0xF11B663A698824C0, 4, 0 }, // V64Jr Backup Tool V0.2b_Beta by RedboX (PD)
        { 0xF1297BC942CDAE9D, 4, 0 }, // Action Replay Pro 64 V3.0 (Unl)
        { 0xF1301043FD80541A, 4, MEMORY_PAK | RUMBLE_PAK }, // Wayne Gretzky's 3D Hockey (J) [o2]
        { 0xF163A242F2449B3B, 4, RUMBLE_PAK }, // Star Twins (J) [!]
        { 0xF1850C35ACE07912, 4, MEMORY_PAK | RUMBLE_PAK }, // Micro Machines 64 Turbo (U) [b1]
        { 0xF23CA406EC2ACE78, 4, MEMORY_PAK | RUMBLE_PAK }, // Bust-A-Move '99 (U) [f1] (PAL)
        { 0xF255D6F1B65D6728, 2, MEMORY_PAK | RUMBLE_PAK }, // Airboarder 64 (J) [t1]
        { 0xF291B959A24CBCAF, 4, 0 }, // UltraSMS V1.0 by Jos Kwanten (PD)
        { 0xF2A653CB60633B3B, 1, 0 }, // Elmo's Letter Adventure (U) [!]
        { 0xF2C3A524A1C4DB13, 4, 0 }, // GoldenEye 007 (U) (G5 Multi (for backups) Hack)
        { 0xF2F75C3636637257, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t2] (All Attacks Hurt P1)
        { 0xF35D5335B7667CB7, 1, MEMORY_PAK }, // Castlevania (U) (V1.1) [!]
        { 0xF35D5F958AFE3D69, 1, MEMORY_PAK }, // Castlevania (U) (V1.0) [b4]
        { 0xF3700A534795C23D, 4, 0 }, // All Star Tennis '99 (E) (M5) [f4] (NTSC)
        { 0xF389A35A17785562, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (J) [f1] (Z64)
        { 0xF3996B673A3030FA, 4, MEMORY_PAK | RUMBLE_PAK }, // NBA Jam 2000 (U) [f1] (PAL)
        { 0xF3C5BF9B160F33E2, 1, MEMORY_PAK }, // Rayman 2 - The Great Escape (U) (M5) [!]
        { 0xF3D27F54C111ACF9, 1, MEMORY_PAK }, // Premier Manager 64 (E) [!]
        { 0xF3DD35BA4152E075, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (GC) [!]
        { 0xF3F2F3856E490C7F, 4, MEMORY_PAK | RUMBLE_PAK }, // Super Bowling (J) [!]
        { 0xF41B6343C10661E6, 4, MEMORY_PAK }, // International Superstar Soccer '98 (E) [!]
        { 0xF43B45BA2F0E9B6F, 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina GC URA (J) (GC) [!]
        { 0xF4520439753A6281, 1, RUMBLE_PAK }, // Wild Choppers (J) [t1]
        { 0xF45A6784F587C72F, 1, MEMORY_PAK }, // Premier Manager 64 (E) [f1] (NTSC)
        { 0xF4646B69C5751095, 4, MEMORY_PAK | TRANSFER_PAK }, // Super B-Daman - Battle Phoenix 64 (J) [!]
        { 0xF468118CE32EE44E, 2, MEMORY_PAK | TRANSFER_PAK }, // PD Ultraman Battle Collection 64 (J) [b1]
        { 0xF478D8B39716DD6D, 2, MEMORY_PAK | RUMBLE_PAK }, // LEGO Racers (E) (M10) [!]
        { 0xF4B6415946FC16CF, 1, 0 }, // Bike Race '98 V1.2 by NAN (PD) [b2]
        { 0xF4CBE92CB392ED12, 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [f1][h1C]
        { 0xF4D89C083F34930D, 4, 0 }, // Tristar and Lightforce Quake Intro by Ayatollah & Mike (PD)
        { 0xF523730199E3EE93, 1, 0 }, // Glover (E) (M3) [!]
        { 0xF5360FBE2BF1691D, 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (E) [h1C]
        { 0xF5363349DBF9D21B, 2, MEMORY_PAK | RUMBLE_PAK }, // Deadly Arts (U) [b1]
        { 0xF568D51E7E49BA1E, 4, MEMORY_PAK }, // Bomberman 64 (U) [o1]
        { 0xF5733C6717A3973A, 4, MEMORY_PAK }, // FIFA - Road to World Cup 98 - World Cup heno Michi (J) [!]
        { 0xF5C214038FCA0710, 4, RUMBLE_PAK }, // Donkey Kong 64 (U) (Kiosk Demo) [f1] (PAL)
        { 0xF5C5866D052713D9, 4, MEMORY_PAK | RUMBLE_PAK }, // Vigilante 8 - 2nd Offense (U) [!]
        { 0xF611F4BAC584135C, 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina GC (J) (GC) [!]
        { 0xF63B89CE4582D57D, 1, MEMORY_PAK | RUMBLE_PAK }, // Bug's Life, A (I) [!]
        { 0xF6621BFB186D11A6, 4, RUMBLE_PAK }, // Star Fox 64 (U) (V1.0) [t3] (No Damage-Unbreakable Wings)
        { 0xF667DC0486510A81, 4, RUMBLE_PAK }, // Lylat Wars (E) (M3) [f2] (NTSC)
        { 0xF6E3494AE4EC8933, 2, MEMORY_PAK | RUMBLE_PAK }, // Kakutou Denshou - F-Cup Maniax (J) [f1] (PAL)
        { 0xF774EAEEF0D8B13E, 1, MEMORY_PAK }, // Fushigi no Dungeon - Fuurai no Shiren 2 - Oni Shuurai! Shiren Jou! (J) [!]
        { 0xF795ACE18DEDE03C, 1, 0 }, // Pokemon Snap (A) [f1] (GameShark)
        { 0xF7B1C8E870536D3E, 4, 0 }, // Liner V1.02 by Colin Phillipps of Memir (PD)
        { 0xF7DF7D0DED52018F, 4, 0 }, // Shuffle Puck 64 (PD)
        { 0xF7F52DB82195E636, 1, RUMBLE_PAK }, // Zelda no Densetsu - Toki no Ocarina - Zelda Collection Version (J) (GC) [!]
        { 0xF7FE28F6C3F2ACC3, 2, 0 }, // War Gods (U) [o3]
        { 0xF8009DB06B291823, 2, MEMORY_PAK | RUMBLE_PAK }, // City-Tour GP - Zennihon GT Senshuken (J) [b3]
        { 0xF828DF21C5E83F66, 4, 0 }, // LCARS Demo by WT Riker (PD) [b1]
        { 0xF82C85BEDEB717E2, 4, MEMORY_PAK }, // FIFA 99 (E) (M8) [hI]
        { 0xF82DD3778C3FB347, 4, MEMORY_PAK | RUMBLE_PAK }, // TG Rally 2 (E) [!]
        { 0xF8B1C7C71EFCA431, 1, RUMBLE_PAK }, // Legend of Zelda, The - Ocarina of Time (U) (V1.0) [T+Rus1.0beta2_Sergey Anton]
        { 0xF908CA4C36464327, 2, MEMORY_PAK }, // Killer Instinct Gold (U) (V1.2) [o1]
        { 0xF921961EE09D8B11, 1, RUMBLE_PAK }, // Command & Conquer (U) [b1]
        { 0xF96E28F7421A4285, 2, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat 4 (U) [b1]
        { 0xF9864452890A5EA3, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark (E) (Debug) (2000-04-26) [!]
        { 0xF9D411E37CB29BC0, 4, MEMORY_PAK | RUMBLE_PAK }, // Excitebike 64 (U) (V1.1) [!]
        { 0xF9F75B92AFEC042B, 2, 0 }, // Mortal Kombat Trilogy (U) (V1.0) [t1] (Hit Anywhere)
        { 0xF9FC3090FF014EC2, 4, MEMORY_PAK }, // World Cup 98 (E) (M8) [f1] (NTSC)
        { 0xFA5A3DFFB4C9CDB9, 4, 0 }, // Clay Fighter - Sculptor's Cut (U) [h2C]
        { 0xFA7D393597AC54FC, 4, 0 }, // NuFan Demo by Kid Stardust (PD)
        { 0xFA8C4571BBE7F9C0, 4, RUMBLE_PAK | TRANSFER_PAK }, // Mickey's Speedway USA (U) [!]
        { 0xFAB972E41CB0882B, 4, 0 }, // CD64 BIOS Direct-Upgrade V1.11
        { 0xFB315F957786CBFB, 4, 0 }, // Star Wars - Shadows of the Empire (U) (Prototype)
        { 0xFB3C48D08D28F69F, 4, RUMBLE_PAK }, // Charlie Blast's Territory (E) [o1]
        { 0xFB453D58AD565422, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // Perfect Dark Plus
        { 0xFB610592898A9705, 2, MEMORY_PAK | RUMBLE_PAK }, // Hybrid Heaven (U) [f1] (PAL)
        { 0xFB8169896F442541, 4, RUMBLE_PAK }, // SmashRemix1.1.0
        { 0xFB816D353FC67AFF, 4, RUMBLE_PAK }, // SmashRemix1.1.1
        { 0xFBB9F1FA6BF88689, 1, RUMBLE_PAK }, // Lt. Duck Dodgers (Prototype)
        { 0xFC051819A46A48F6, 4, 0 }, // Pip's World Game 2 by Mr. Pips (PD)
        { 0xFC70E27208FFE7AA, 1, RUMBLE_PAK }, // Space Station Silicon Valley (E) (M7) [b1]
        { 0xFC74D4759A0278AB, 2, 0 }, // Powerpuff Girls, The - Chemical X-Traction (U) [!]
        { 0xFC7797BF4A95E83C, 4, MEMORY_PAK | RUMBLE_PAK }, // Automobili Lamborghini (E) [o1]
        { 0xFC8BED44D640D904, 1, MEMORY_PAK }, // Aidyn Chronicles - The First Mage (U) (Beta) (2000-05-09)
        { 0xFCB2955BB302626D, 2, MEMORY_PAK | RUMBLE_PAK }, // NASCAR 99 (U) [f1] (PAL)
        { 0xFCBCCB2172903C6B, 1, MEMORY_PAK | RUMBLE_PAK }, // Mystical Ninja Starring Goemon (U) [h1C]
        { 0xFCE0D79965316C54, 4, RUMBLE_PAK }, // Knife Edge - Nose Gunner (U) [!]
        { 0xFD0E716EFB029C8E, 4, RUMBLE_PAK }, // SmashRemix1.0.0
        { 0xFD2FC8753BD10D89, 3, MEMORY_PAK | RUMBLE_PAK }, // Rampage 2 - Universal Tour (U) [t2]
        { 0xFD61BB45FBB51EB2, 4, 0 }, // N64 Seminar Demo - CPU by ZoRAXE (PD)
        { 0xFD6907F083CBC160, 4, 0 }, // StarCraft 64 (G) (Prototype)
        { 0xFD73F7759724755A, 4, MEMORY_PAK | RUMBLE_PAK }, // Diddy Kong Racing (E) (M3) (V1.0) [o1]
        { 0xFD8F687542608FDC, 1, RUMBLE_PAK }, // Star Wars Episode I - Battle for Naboo (U) [t1]
        { 0xFDA245D2A74A3D47, 4, MEMORY_PAK | RUMBLE_PAK }, // Extreme-G (U) [o1]
        { 0xFDAA963CFDCCD971, 2, MEMORY_PAK }, // AeroGauge (U) [f1] (PAL)
        { 0xFDD248B2569A020E, 1, MEMORY_PAK }, // F-1 Pole Position 64 (E) (M3) [b1]
        { 0xFDF95D15618546CA, 4, MEMORY_PAK | RUMBLE_PAK | TRANSFER_PAK }, // GoldenEye X
        { 0xFDFC3328A0B2BA23, 4, RUMBLE_PAK }, // SmashRemix1.0.1
        { 0xFE05840B9393320C, 4, MEMORY_PAK | RUMBLE_PAK }, // Turok 2 - Seeds of Evil (G) [!]
        { 0xFE16991946F54C2D, 4, 0 }, // CD64 BIOS EEPROM-Burner V1.23
        { 0xFE24AC631B41AA17, 1, 0 }, // Star Wars - Teikoku no Kage (J) [o2]
        { 0xFE4B6B43081D29A7, 2, MEMORY_PAK | RUMBLE_PAK }, // Triple Play 2000 (U) [!]
        { 0xFE5DC5D4E1815BE5, 1, MEMORY_PAK | RUMBLE_PAK }, // Nuclear Strike 64 (U) [t1]
        { 0xFE667A120D2195AD, 4, TRANSFER_PAK }, // Pocket Monsters Stadium 2 (J) [f4]
        { 0xFE94E570E4873A9C, 2, MEMORY_PAK | RUMBLE_PAK }, // Fighter's Destiny (G) [!]
        { 0xFEB72E1D5BD2C925, 4, MEMORY_PAK | RUMBLE_PAK }, // NFL Blitz 2000 (U) (V1.0) [f1] (PAL)
        { 0xFECE2B85331406D1, 1, 0 }, // Super Mario 64 (U) [t3]
        { 0xFEE970104E94A9A0, 4, RUMBLE_PAK }, // RR64 - Ridge Racer 64 (E) [!]
        { 0xFEF9D364FBD6D06E, 1, RUMBLE_PAK }, // Banjo-Kazooie (U) (V1.0) [b6]
        { 0xFF0AC362F4EC09B3, 4, 0 }, // LaC's Universal Bootemu V1.2 (PD)
        { 0xFF14C1DA167FDE92, 4, MEMORY_PAK | RUMBLE_PAK }, // Duke Nukem 64 (E) (Beta)
        { 0xFF2F2FB4D161149A, 2, MEMORY_PAK }, // Cruis'n USA (U) (V1.0) (VC) [!]
        { 0xFF44EDC41AAE9213, 1, MEMORY_PAK | RUMBLE_PAK }, // Mortal Kombat Mythologies - Sub-Zero (E) [h1C]
        { 0xFF922478570B5673, 4, 0 }, // UltraMSX2 V1.0 w-Salamander by Jos Kwanten (PD)
        { 0xFFCAA7C168858537, 4, RUMBLE_PAK }, // Star Fox 64 (J) (V1.0) [o3][f1]
    };
}
