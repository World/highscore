// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Nintendo64.RunnerDelegate : Highscore.RunnerDelegate {
    private Settings settings;
    private uint n_players;

    private Hs.Nintendo64Pak paks[4];
    public Hs.Nintendo64Pak pak1 {
        get { return paks[0]; }
        set {
            paks[0] = value;
            controller_changed_cb (0);
        }
    }
    public Hs.Nintendo64Pak pak2 {
        get { return paks[1]; }
        set {
            paks[1] = value;
            controller_changed_cb (1);
        }
    }
    public Hs.Nintendo64Pak pak3 {
        get { return paks[2]; }
        set {
            paks[2] = value;
            controller_changed_cb (2);
        }
    }
    public Hs.Nintendo64Pak pak4 {
        get { return paks[3]; }
        set {
            paks[3] = value;
            controller_changed_cb (3);
        }
    }

    construct {
        var manager = ControllerManager.get_instance ();

        manager.controller_changed.connect (controller_changed_cb);

        settings = new GameSettings (runner.game).get_platform ();

        var metadata = runner.game.metadata as Metadata;

        n_players = metadata.players;

        for (uint p = 0; p < n_players; p++) {
            var name = @"pak$(p + 1)";
            if (settings.get_user_value (name) == null) {
                var pak = Pak.get_auto_for_game (runner.game, p);
                settings.set_string (name, Pak.to_string (pak));
            }

            Pak.bind_settings (settings, name, this, name, GET);
        }
    }

    ~RunnerDelegate () {
        var manager = ControllerManager.get_instance ();

        manager.controller_changed.disconnect (controller_changed_cb);
    }

    private void controller_changed_cb (uint player) {
        if (player >= n_players || !runner.running)
            return;

        var manager = ControllerManager.get_instance ();
        var proxy = runner.platform_proxy as Proxy;

        var type = manager.get_controller_type (player);

        if (type == NONE) {
            proxy.set_controller.begin (player, false, NONE);
            runner.set_rumble_enabled (player, false);
        } else {
            var pak = paks[player];
            proxy.set_controller.begin (player, true, pak);
            runner.set_rumble_enabled (player, pak == RUMBLE_PAK);
        }
    }

    public override async void after_load () throws Error {
        var manager = ControllerManager.get_instance ();
        var proxy = runner.platform_proxy as Proxy;

        for (uint p = 0; p < Hs.NINTENDO_64_MAX_PLAYERS; p++) {
            ControllerManager.ControllerType type;

            if (p < n_players)
                type = manager.get_controller_type (p);
            else
                type = NONE;

            if (type == NONE) {
                proxy.set_controller.begin (p, false, NONE);
                runner.set_rumble_enabled (p, false);
            } else {
                var pak = paks[p];
                proxy.set_controller.begin (p, true, pak);
                runner.set_rumble_enabled (p, pak == RUMBLE_PAK);
            }
        }
    }

    public override uint get_n_players () {
        return n_players;
    }
}
