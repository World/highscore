// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://n64brew.dev/wiki/ROM_Header
public class Highscore.Nintendo64.Parser : GameParser {
    private const size_t CRC_OFFSET = 0x10;
    private const size_t CRC_LENGTH = 0x8;

    private const size_t HOMEBREW_ID_OFFSET = 0x3C;
    private const size_t HOMEBREW_ID_LENGTH = 0x2;

    private const size_t HOMEBREW_CONTROLLER_OFFSET = 0x34;
    private const size_t HOMEBREW_CONTROLLER_LENGTH = 0x4;

    private uint8 players;
    private SupportedPaks paks[4];

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    private bool parse_homebrew_header (DataInputStream stream) throws Error {
        var homebrew_id = new uint8[HOMEBREW_ID_LENGTH];
        stream.seek (HOMEBREW_ID_OFFSET, SET);
        stream.read (homebrew_id);

        if (homebrew_id[0] != 'E' || homebrew_id[1] != 'D') {
            parser_debug ("Missing homebrew header");
            return false;
        }

        // Homebrew header is present, read controller data

        var homebrew_controllers = new uint8[HOMEBREW_CONTROLLER_LENGTH];
        stream.seek (HOMEBREW_CONTROLLER_OFFSET, SET);
        stream.read (homebrew_controllers);

        players = 0;

        for (int i = 0; i < 4; i++) {
            uint8 c = homebrew_controllers[i];

            switch (c) {
                case 0:
                    paks[i] = DEFAULT_PAKS;
                    break;
                case 2:
                    paks[i] = MEMORY_PAK;
                    break;
                case 3:
                    paks[i] = TRANSFER_PAK;
                    break;
                case 0xFF:
                    paks[i] = 0;
                    break;
                case 0x80: // N64 Mouse
                case 0x81: // VRU
                case 0x82: // GameCube
                case 0x83: // Randnet keyboard
                case 0x84: // GameCube keyboard
                    // Unsupported
                    break;
                default:
                    return false;
            }

            if (c != 0xFF)
                players = (uint8) i;
        }

        if (PARSERS in Debug.get_flags ()) {
            string pak_str[4];
            for (int i = 0; i < 4; i++)
                pak_str[i] = paks[i].to_string ();

            parser_debug (
                "Found metadata in the header: %u players, paks: %s",
                players, string.joinv (", ", pak_str)
            );
        }

        return true;
    }

    private string get_md5 (DataInputStream stream) throws Error {
        size_t size;

        stream.seek (0, SeekType.END);
        size = (size_t) stream.tell ();

        stream.seek (0, SeekType.SET);
        var bytes = stream.read_bytes (size);

        return Checksum.compute_for_bytes (ChecksumType.MD5, bytes);
    }

    public override bool parse () throws Error {
        var stream = new DataInputStream (file.read ());

        if (parse_homebrew_header (stream))
            return true;

        var crc_buffer = new uint8[CRC_LENGTH];
        stream.seek (CRC_OFFSET, SET);
        stream.read (crc_buffer);

        uint64 crc = 0;
        for (var i = 0; i < CRC_LENGTH; i++)
            crc |= (uint64) crc_buffer[i] << ((CRC_LENGTH - i - 1) * 8);

        var md5 = get_md5 (stream);

        var data = find_game_data (md5, crc);

        players = data.players;
        for (int i = 0; i < 4; i++)
            paks[i] = data.paks;

        if (PARSERS in Debug.get_flags ()) {
            string pak_str[4];
            for (int i = 0; i < 4; i++)
                pak_str[i] = paks[i].to_string ();

            parser_debug (
                "Found metadata in the database: %u players, paks: %s",
                players, string.joinv (", ", pak_str)
            );
        }

        return true;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (players, paks);
    }
}
