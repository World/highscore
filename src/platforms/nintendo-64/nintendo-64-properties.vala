// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/nintendo-64/nintendo-64-properties.ui")]
public class Highscore.Nintendo64.Properties : PlatformProperties {
    public Hs.Nintendo64Pak pak1 { get; set; }
    public Hs.Nintendo64Pak pak2 { get; set; }
    public Hs.Nintendo64Pak pak3 { get; set; }
    public Hs.Nintendo64Pak pak4 { get; set; }

    [GtkChild]
    private unowned Adw.PreferencesGroup paks_group;

    private Settings settings;

    construct {
        settings = new GameSettings (game).get_platform ();

        var metadata = game.metadata as Metadata;

        for (uint p = 0; p < metadata.players; p++) {
            var name = @"pak$(p + 1)";

            if (settings.get_user_value (name) == null) {
                var pak = Pak.get_auto_for_game (game, p);
                settings.set_string (name, Pak.to_string (pak));
            }

            Pak.bind_settings (settings, name, this, name, DEFAULT);

            var supported = metadata.get_paks (p);

            // We don't support other paks atm
            supported &= (MEMORY_PAK | RUMBLE_PAK);

            if (supported == 0 || supported == MEMORY_PAK)
                continue;

            bool has_memory = (supported & SupportedPaks.MEMORY_PAK) > 0;
            bool has_rumble = (supported & SupportedPaks.RUMBLE_PAK) > 0;

            Hs.Nintendo64Pak[] paks = {};
            paks += Hs.Nintendo64Pak.NONE;
            if ((supported & SupportedPaks.MEMORY_PAK) > 0)
                paks += Hs.Nintendo64Pak.MEMORY_PAK;
            if ((supported & SupportedPaks.RUMBLE_PAK) > 0)
                paks += Hs.Nintendo64Pak.RUMBLE_PAK;

            var paks_model = new Gtk.StringList (null);
            foreach (var pak in paks)
                paks_model.append (Pak.to_display_name (pak));

            var row = new Adw.ComboRow () {
                title = _("Player %u").printf (p + 1),
                model = paks_model,
            };

            bind_property (
                name, row, "selected", SYNC_CREATE | BIDIRECTIONAL,
                (binding, from_value, ref to_value) => {
                    var pak = (Hs.Nintendo64Pak) from_value.get_enum ();

                    int index = -1;

                    for (int i = 0; i < paks.length; i++) {
                        if (paks[i] == pak)
                            index = i;
                    }

                    to_value = index < 0 ? Gtk.INVALID_LIST_POSITION : (uint) index;
                    return true;
                },
                (binding, from_value, ref to_value) => {
                    uint selected = from_value.get_uint ();

                    if (selected == Gtk.INVALID_LIST_POSITION)
                        return false;

                    var pak = paks[selected];

                    to_value = pak;
                    return true;
                }
            );

            paks_group.add (row);
            visible = true;
        }
    }
}
