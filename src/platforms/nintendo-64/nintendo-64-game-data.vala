// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.Nintendo64 {
    public const uint8 DEFAULT_PLAYERS = 4;
    public const SupportedPaks DEFAULT_PAKS = MEMORY_PAK | RUMBLE_PAK;

    [Flags]
    public enum SupportedPaks {
        MEMORY_PAK,
        RUMBLE_PAK,
        TRANSFER_PAK,
        BIO_SENSOR;

        public string to_string () {
            string[] strs = {};

            if (MEMORY_PAK in this)
                strs += "memory-pak";
            if (RUMBLE_PAK in this)
                strs += "rumble-pak";
            if (TRANSFER_PAK in this)
                strs += "transfer-pak";
            if (BIO_SENSOR in this)
                strs += "bio-sensor";

            return "[%s]".printf (string.joinv (" | ", strs));
        }
    }

    public struct GameData {
        uint8 players;
        SupportedPaks paks;
    }

    private inline Overrides.GameDataMD5? find_override_by_md5 (string md5) {
        void *ret = Posix.bsearch (
            md5,
            Overrides.GAME_DATA_MD5,
            Overrides.GAME_DATA_MD5.length,
            sizeof (Overrides.GameDataMD5),
            (a, b) => {
                var needle = (string) a;
                var override = (Overrides.GameDataMD5 *) b;
                return strcmp (needle, override.md5);
            }
        );

        if (ret == null)
            return null;

        return * ((Overrides.GameDataMD5 *) ret);
    }

    private inline Overrides.GameDataCRC? find_override_by_crc (uint64 crc) {
        void *ret = Posix.bsearch (
            (void *) crc,
            Overrides.GAME_DATA_CRC,
            Overrides.GAME_DATA_CRC.length,
            sizeof (Overrides.GameDataCRC),
            (a, b) => {
                uint64 needle = (uint64) a;
                var override = (Overrides.GameDataCRC *) b;

                if (needle < override.crc)
                    return -1;

                if (needle > override.crc)
                    return 1;

                return 0;
            }
        );

        if (ret == null)
            return null;

        return * ((Overrides.GameDataCRC *) ret);
    }

    public GameData find_game_data (string md5, uint64 crc) {
        var md5_override = find_override_by_md5 (md5);
        if (md5_override != null)
            return { md5_override.players, md5_override.paks };

        var crc_override = find_override_by_crc (crc);
        if (crc_override != null)
            return { crc_override.players, crc_override.paks };

        return { DEFAULT_PLAYERS, DEFAULT_PAKS };
    }
}
