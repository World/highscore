// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Nintendo64.Metadata : Object, GameMetadata {
    public uint8 players { get; set; }
    public SupportedPaks paks1 { get; set; }
    public SupportedPaks paks2 { get; set; }
    public SupportedPaks paks3 { get; set; }
    public SupportedPaks paks4 { get; set; }

    public Metadata (uint8 players, SupportedPaks[] paks) {
        assert (paks.length == 4);

        Object (
            players: players,
            paks1: paks[0],
            paks2: paks[1],
            paks3: paks[2],
            paks4: paks[3]
        );
    }

    protected VariantType serialize_type () {
        return new VariantType.tuple ({
            VariantType.UINT16,
            VariantType.UINT16
        });
    }

    private uint16 serialize_paks () {
        uint16 ret = 0;
        for (int i = 0; i < 4; i++)
            ret |= get_paks (i) << (i * 4);

        return ret;
    }

    protected Variant serialize () {
        return new Variant.tuple ({
            (uint16) players,
            serialize_paks (),
        });
    }

    protected void deserialize (Variant variant) {
        players = (uint8) variant.get_child_value (0).get_uint16 ();

        uint16 paks = variant.get_child_value (1).get_uint16 ();
        paks1 = (SupportedPaks) ((paks >> 0)  & 0xF);
        paks2 = (SupportedPaks) ((paks >> 4)  & 0xF);
        paks3 = (SupportedPaks) ((paks >> 8)  & 0xF);
        paks4 = (SupportedPaks) ((paks >> 12) & 0xF);
    }

    public SupportedPaks get_paks (uint player) {
        switch (player) {
            case 0: return paks1;
            case 1: return paks2;
            case 2: return paks3;
            case 3: return paks4;
            default:
                assert_not_reached ();
        }
    }
}
