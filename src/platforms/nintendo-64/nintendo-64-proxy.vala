// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.Nintendo64.Proxy : Object {
    public abstract async uint get_players () throws Error;

    public abstract async void set_controller (uint player, bool present, Hs.Nintendo64Pak pak) throws Error;
}
