// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Nintendo64.Controls : PlatformControls {
    construct {
        n_players = Hs.NINTENDO_64_MAX_PLAYERS;

        var controller = new PlatformController (
            "nintendo-64", _("Nintendo 64 Controller")
        );
        controller.add_sections (
            "stick", "dpad", "face-buttons", "c-buttons",
            "shoulder-buttons", "menu", null
        );

        var stick = new StickControl ("stick", _("Control Stick"));
        stick.moved.connect (control_stick_moved_cb);
        controller.add_control ("stick", stick);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Control Pad"),
            Hs.Nintendo64Button.UP,
            Hs.Nintendo64Button.DOWN,
            Hs.Nintendo64Button.LEFT,
            Hs.Nintendo64Button.RIGHT
        );

        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.Nintendo64Button.A,
            "b", _("B Button"), Hs.Nintendo64Button.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "c-buttons",
            "c-up",    _("Up C Button"),    Hs.Nintendo64Button.C_UP,
            "c-down",  _("Down C Button"),  Hs.Nintendo64Button.C_DOWN,
            "c-left",  _("Left C Button"),  Hs.Nintendo64Button.C_LEFT,
            "c-right", _("Right C Button"), Hs.Nintendo64Button.C_RIGHT,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "shoulder-buttons",
            "l", _("L Button"), Hs.Nintendo64Button.L,
            "r", _("R Button"), Hs.Nintendo64Button.R,
            "z", _("Z Button"), Hs.Nintendo64Button.Z,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "start", _("Start"), Hs.Nintendo64Button.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,         "stick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,       "stick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,       "stick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,      "stick:right");
        kbd_mapping.map (Linux.Input.KEY_I,          "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_K,          "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_J,          "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_L,          "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,          "c-up");
        kbd_mapping.map (Linux.Input.KEY_S,          "c-down");
        kbd_mapping.map (Linux.Input.KEY_A,          "c-left");
        kbd_mapping.map (Linux.Input.KEY_D,          "c-right");
        kbd_mapping.map (Linux.Input.KEY_X,          "a");
        kbd_mapping.map (Linux.Input.KEY_Z,          "b");
        kbd_mapping.map (Linux.Input.KEY_LEFTSHIFT,  "l");
        kbd_mapping.map (Linux.Input.KEY_RIGHTSHIFT, "r");
        kbd_mapping.map (Linux.Input.KEY_SPACE,      "z");
        kbd_mapping.map (Linux.Input.KEY_ENTER,      "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_stick  (LEFT,  "stick");
        gamepad_mapping.map_button (SOUTH, "a");
        gamepad_mapping.map_button (WEST,  "b");
        gamepad_mapping.map_button (L,     "l");
        gamepad_mapping.map_button (R,     "r");
        gamepad_mapping.map_button (L2,    "z");
        gamepad_mapping.map_button (START, "start");
        gamepad_mapping.map_stick_to_buttons (RIGHT, "c-up", "c-down", "c-left", "c-right");

        var c_buttons_layer = new PlatformGamepadMapping ();
        c_buttons_layer.map_button (NORTH, "c-up");
        c_buttons_layer.map_button (SOUTH, "c-down");
        c_buttons_layer.map_button (WEST,  "c-left");
        c_buttons_layer.map_button (EAST,  "c-right");
        gamepad_mapping.add_layer (R2, _("C Buttons"), c_buttons_layer);

        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "nintendo-64";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->nintendo_64.pad_buttons[player], button, pressed);
        });
    }

    private static void control_stick_moved_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->nintendo_64.pad_control_stick_x[player] = x;
            state->nintendo_64.pad_control_stick_y[player] = y;
        });
    }
}
