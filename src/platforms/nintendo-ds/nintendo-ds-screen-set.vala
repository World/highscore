// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.NintendoDs.ScreenSet : Highscore.ScreenSet {
    public const int MAIN_ID = 0;
    public const int TOUCH_ID = 1;

    public LayoutMode layout_mode { get; set; }
    public bool bottom_screen_left { get; set; }
    public bool show_bottom_screen { get; set; }
    public bool flipped { get; set; }
    public bool ds_phat_colors { get; set; }

    private Settings settings;

    construct {
        notify["layout-mode"].connect (() => update_layout (true));
        notify["bottom-screen-left"].connect (() => update_layout (true));
        notify["show-bottom-screen"].connect (() => update_layout (true));
        notify["flipped"].connect (() => update_layout (true));
        notify["ds-phat-colors"].connect (() => filter_changed ());

        update_layout (false);

        settings = new GameSettings (game).get_platform ();

        LayoutMode.bind_settings (
            settings, "layout-mode", this, "layout-mode", GET
        );

        settings.bind ("bottom-screen-left", this, "bottom-screen-left", GET);
        settings.bind ("show-bottom-screen", this, "show-bottom-screen", GET);
        settings.bind ("flipped",            this, "flipped",            GET);
        settings.bind ("ds-phat-colors",     this, "ds-phat-colors",     GET);
    }

    private void update_layout (bool animate) {
        var metadata = game.metadata as Metadata;
        float gap = metadata.get_screen_gap ();

        switch (layout_mode) {
            case VERTICAL:
                layout = new VerticalLayout (gap);
                secondary_layout = null;
                break;
            case BOOK:
                if (flipped)
                    layout = new VerticalLayout (gap, 90_DEG);
                else
                    layout = new VerticalLayout (gap, 270_DEG);
                secondary_layout = null;
                break;
            case SIDE_BY_SIDE:
                layout = new HorizontalLayout (bottom_screen_left);
                secondary_layout = null;
                break;
            case SINGLE_SCREEN:
                layout = new FocusLayout (
                    show_bottom_screen ? TOUCH_ID : MAIN_ID, gap
                );
                secondary_layout = null;
                break;
            case SEPARATE_WINDOWS:
                layout = new SingleLayout (
                    show_bottom_screen ? TOUCH_ID : MAIN_ID
                );
                secondary_layout = new SingleLayout (
                    show_bottom_screen ? MAIN_ID : TOUCH_ID
                );
                break;
            default:
                assert_not_reached ();
        }

        layout_changed (animate);
        secondary_layout_changed (animate);
    }

    public override Screen[] get_screens () {
        var top_screen    = new Screen (MAIN_ID,  {{ 0, 0    }, { 1, 0.5f }});
        var bottom_screen = new Screen (TOUCH_ID, {{ 0, 0.5f }, { 1, 0.5f }});

        return ({ top_screen, bottom_screen });
    }

    public override string get_filter_path () {
        return "/app/drey/Highscore/platforms/nintendo-ds/nintendo-ds.glsl";
    }

    public override void setup_filter (GLShader shader, int screen_id) {
        shader.set_uniform_bool ("u_dsPhatColors", ds_phat_colors);
    }

    public override ScreenType get_screen_type (Hs.Region region) {
        return NINTENDO_DS;
    }
}
