// This file is part of Highscore. License: GPL-3.0-or-later

public enum Highscore.NintendoDs.LayoutMode {
    VERTICAL,
    BOOK,
    SIDE_BY_SIDE,
    SINGLE_SCREEN,
    SEPARATE_WINDOWS;

    public string to_string () {
        switch (this) {
            case VERTICAL:
                return "vertical";
            case BOOK:
                return "book";
            case SIDE_BY_SIDE:
                return "side-by-side";
            case SINGLE_SCREEN:
                return "single-screen";
            case SEPARATE_WINDOWS:
                return "separate-windows";
            default:
                assert_not_reached ();
        }
    }

    public static LayoutMode? from_string (string str) {
        if (str == "vertical")
            return VERTICAL;
        if (str == "book")
            return BOOK;
        if (str == "side-by-side")
            return SIDE_BY_SIDE;
        if (str == "single-screen")
            return SINGLE_SCREEN;
        if (str == "separate-windows")
            return SEPARATE_WINDOWS;

        return null;
    }

    public static void bind_settings (
        Settings settings,
        string key,
        Object obj,
        string prop,
        SettingsBindFlags flags
    ) {
        settings.bind_with_mapping (
            key, obj, prop, flags,
            (value, variant, user_data) => {
                var str = variant.get_string ();
                var mode = from_string (str);

                if (mode == null) {
                    critical ("Unknown layout mode: %s", str);
                    value = VERTICAL;
                } else {
                    value = (LayoutMode) mode;
                }

                return true;
            },
            (value, variant_type, user_data) => {
                var mode = (LayoutMode) value.get_enum ();
                return mode.to_string ();
            },
            null, null
        );
    }
}
