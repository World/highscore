// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://gbdev.io/pandocs/The_Cartridge_Header.html
public class Highscore.NintendoDs.Parser : GameParser {
    private const size_t GAME_CODE_OFFSET = 12;
    private const size_t GAME_CODE_LENGTH = 4;

    private string game_code;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    public override bool parse () throws Error {
        var stream = new DataInputStream (file.read ());

        stream.seek (GAME_CODE_OFFSET, SET);
        var game_code_buffer = new uint8[GAME_CODE_LENGTH];

        stream.read (game_code_buffer);

        var game_code_builder = new StringBuilder ();
        game_code_builder.append_len (
            (string) game_code_buffer, (ssize_t) GAME_CODE_LENGTH
        );
        game_code = game_code_builder.free_and_steal ();

        parser_debug ("Found game code: %s", game_code);

        return true;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (game_code);
    }
}
