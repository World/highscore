// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.NintendoDs.RunnerDelegate : Highscore.RunnerDelegate {
    public LayoutMode layout_mode { get; set; }
    public bool flipped { get; set; }
    public bool bottom_screen_left { get; set; }
    public bool show_bottom_screen { get; set; }

    public bool mic_active { get; set; }

    public LayoutMode last_layout_mode = VERTICAL;

    private Settings settings;

    construct {
        notify["layout-mode"].connect (() => {
            update_view_names ();

            if (layout_mode != SEPARATE_WINDOWS)
                last_layout_mode = layout_mode;

            if (layout_mode == VERTICAL)
                runner.set_controller_type (0, "nintendo-ds-vertical");
            else
                runner.set_controller_type (0, "nintendo-ds");
        });
        notify["show-bottom-screen"].connect (update_view_names);

        notify["mic-active"].connect (() => {
            runner.input_buffer.modify_input_state (state => {
                state->nintendo_ds.mic_active = mic_active;
            });
        });

        var game_settings = new GameSettings (runner.game);

        settings = game_settings.get_platform ();

        LayoutMode.bind_settings (
            settings, "layout-mode", this, "layout-mode", DEFAULT
        );

        settings.bind ("flipped",            this, "flipped",            DEFAULT);
        settings.bind ("bottom-screen-left", this, "bottom-screen-left", DEFAULT);
        settings.bind ("show-bottom-screen", this, "show-bottom-screen", DEFAULT);
    }

    private void update_view_names () {
        string name, secondary_name;

        if (layout_mode == SEPARATE_WINDOWS) {
            name = show_bottom_screen ? _("Bottom Screen") : _("Top Screen");
            secondary_name = show_bottom_screen ? _("Top Screen") : _("Bottom Screen");
        } else {
            name = secondary_name = "";
        }

        runner.view_name = name;
        runner.secondary_view_name = secondary_name;
    }

    public override void secondary_view_closed () {
        if (layout_mode == SEPARATE_WINDOWS)
            layout_mode = last_layout_mode;
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        var box = new Gtk.Box (HORIZONTAL, 6);

        var mic_button = new Gtk.ToggleButton () {
            icon_name = "audio-input-microphone-symbolic",
        };

        bind_property (
            "mic-active", mic_button, "active",
            SYNC_CREATE | BIDIRECTIONAL
        );

        var switcher = new LayoutSwitcher ();

        switcher.track_fullscreen (view);

        bind_property (
            "layout-mode", switcher, "layout-mode", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "flipped", switcher, "flipped", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "bottom-screen-left", switcher, "bottom-screen-left", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "show-bottom-screen", switcher, "show-bottom-screen", SYNC_CREATE | BIDIRECTIONAL
        );

        box.append (mic_button);
        box.append (switcher);

        return box;
    }

    public override OverlayMenuAddin? create_overlay_menu () {
        var menu = new OverlayMenu ();

        bind_property (
            "layout-mode", menu, "layout-mode", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "flipped", menu, "flipped", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "bottom-screen-left", menu, "bottom-screen-left", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "show-bottom-screen", menu, "show-bottom-screen", SYNC_CREATE | BIDIRECTIONAL
        );

        return menu;
    }

    public void swap_screens () {
        if (layout_mode == BOOK)
            flipped = !flipped;

        if (layout_mode == SIDE_BY_SIDE)
            bottom_screen_left = !bottom_screen_left;

        if (layout_mode == SINGLE_SCREEN || layout_mode == SEPARATE_WINDOWS)
            show_bottom_screen = !show_bottom_screen;
    }
}
