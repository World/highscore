// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.NintendoDs.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("nintendo-ds", _("Nintendo DS"));
        controller.add_sections (
            "dpad", "face-buttons", "shoulder-buttons",
            "menu", "touchscreen", null
        );

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Control Pad"),
            Hs.NintendoDsButton.UP,
            Hs.NintendoDsButton.DOWN,
            Hs.NintendoDsButton.LEFT,
            Hs.NintendoDsButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.NintendoDsButton.A,
            "b", _("B Button"), Hs.NintendoDsButton.B,
            "x", _("X Button"), Hs.NintendoDsButton.X,
            "y", _("Y Button"), Hs.NintendoDsButton.Y,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "shoulder-buttons",
            "l", _("L Button"), Hs.NintendoDsButton.L,
            "r", _("R Button"), Hs.NintendoDsButton.R,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.NintendoDsButton.SELECT,
            "start",  _("Start"),  Hs.NintendoDsButton.START,
            null
        );

        var touchscreen = new PointerControl ("touchscreen", _("Touchscreen"));
        touchscreen.pressed.connect (touchscreen_moved_cb);
        touchscreen.moved.connect (touchscreen_moved_cb);
        touchscreen.released.connect (touchscreen_released_cb);
        controller.add_control ("touchscreen", touchscreen);

        controller.set_for_screen (ScreenSet.TOUCH_ID, touchscreen);

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,         "x");
        kbd_mapping.map (Linux.Input.KEY_A,         "y");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "a");
        kbd_mapping.map (Linux.Input.KEY_Q,         "l");
        kbd_mapping.map (Linux.Input.KEY_E,         "r");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (WEST,   "y");
        gamepad_mapping.map_button (NORTH,  "x");
        gamepad_mapping.map_button (L,      "l");
        gamepad_mapping.map_button (R,      "r");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        controller = new PlatformController ("nintendo-ds-vertical", _("Nintendo DS"));
        controller.similar_to = "nintendo-ds";
        controller.add_sections (
            "dpad", "face-buttons", "shoulder-buttons",
            "menu", "touchscreen", null
        );

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Control Pad"),
            Hs.NintendoDsButton.UP,
            Hs.NintendoDsButton.DOWN,
            Hs.NintendoDsButton.LEFT,
            Hs.NintendoDsButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.NintendoDsButton.A,
            "b", _("B Button"), Hs.NintendoDsButton.B,
            "x", _("X Button"), Hs.NintendoDsButton.X,
            "y", _("Y Button"), Hs.NintendoDsButton.Y,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "shoulder-buttons",
            "l", _("L Button"), Hs.NintendoDsButton.L,
            "r", _("R Button"), Hs.NintendoDsButton.R,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.NintendoDsButton.SELECT,
            "start",  _("Start"),  Hs.NintendoDsButton.START,
            null
        );

        touchscreen = new PointerControl ("touchscreen", _("Touchscreen"));
        touchscreen.pressed.connect (touchscreen_moved_cb);
        touchscreen.moved.connect (touchscreen_moved_cb);
        touchscreen.released.connect (touchscreen_released_cb);
        controller.add_control ("touchscreen", touchscreen);

        controller.set_for_screen (ScreenSet.TOUCH_ID, touchscreen);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,         "x");
        kbd_mapping.map (Linux.Input.KEY_A,         "y");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "a");
        kbd_mapping.map (Linux.Input.KEY_Q,         "l");
        kbd_mapping.map (Linux.Input.KEY_E,         "r");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (WEST,   "y");
        gamepad_mapping.map_button (NORTH,  "x");
        gamepad_mapping.map_button (L,      "l");
        gamepad_mapping.map_button (R,      "r");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "nintendo-ds-vertical";

        // global

        global_controller = new PlatformController ("shortcuts", _("Shortcuts"));
        global_controller.add_section ("shortcuts");

        var shortcut = new ButtonControl ("swap-screens", _("Swap Screens"));
        shortcut.activate.connect (swap_screens_cb);
        global_controller.add_control ("shortcuts", shortcut);

        shortcut = new ButtonControl ("toggle-mic", _("Toggle Mic"));
        shortcut.activate.connect (toggle_mic_cb);
        global_controller.add_control ("shortcuts", shortcut);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_SPACE, "swap-screens");
        kbd_mapping.map (Linux.Input.KEY_M, "toggle-mic");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (L2, "swap-screens");
        gamepad_mapping.map_button (R2, "toggle-mic");
        global_controller.default_gamepad_mapping = gamepad_mapping;
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->nintendo_ds.buttons, button, pressed);
        });
    }

    private static void touchscreen_moved_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->nintendo_ds.touch_pressed = true;
            state->nintendo_ds.touch_x = x;
            state->nintendo_ds.touch_y = y;
        });
    }

    private static void touchscreen_released_cb (Runner runner, uint player) {
        runner.input_buffer.modify_input_state (state => {
            state->nintendo_ds.touch_pressed = false;
        });
    }

    private static void swap_screens_cb (Runner runner, uint player, bool pressed) {
        if (!pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;
        delegate.swap_screens ();
    }

    private static void toggle_mic_cb (Runner runner, uint player, bool pressed) {
        var delegate = runner.delegate as RunnerDelegate;

        delegate.mic_active = pressed;
    }
}
