// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.NintendoDs.Overrides {
    private const int DEFAULT_SCREEN_GAP = 80;

    private struct ScreenGapOverride {
        string code_prefix;
        int gap;
    }

    private const ScreenGapOverride[] SCREEN_GAP_OVERRIDES = {
        { "A27", 32  }, // Yoshi Touch & Go (Demo)
        { "AB6", 0   }, // Bubble Bobble Revolution / Bubble Bobble DS
        { "ACB", 0   }, // Castlevania - Portrait of Ruin
        { "ACV", 0   }, // Castlevania - Dawn of Sorrow
        { "AP2", 50  }, // Metroid Prime Pinball
        { "ARI", 0   }, // Rainbow Islands Revolution / New Rainbow Islands
        { "ASC", 80  }, // Sonic Rush
        { "AYI", 32  }, // Yoshi Touch & Go
        { "AYW", 64  }, // Yoshi's Island DS
        { "BXS", 80  }, // Sonic Colors
        { "BZE", 0   }, // Sillybandz DS
        { "CBB", 64  }, // Big Bang Mini
        { "CQ7", 0   }, // Henry Hatsworth in the Puzzling Adventure
        { "YAA", 100 }, // Arkanoid DS
        { "YAW", 0   }, // Away - Shuffle Dungeon
        { "YCT", 50  }, // Contra 4
        { "YIV", 15  }, // Dragon Quest IV
        { "YLK", 64  }, // Legend of Kage 2, The
        { "YR9", 0   }, // Castlevania - Order of Ecclesia
        { "YSZ", 0   }, // Simpsons Game, The
        { "YV5", 15  }, // Dragon Quest V
        { "YVI", 15  }, // Dragon Quest VI
    };

    private inline ScreenGapOverride? find_screen_gap_override (string code_prefix) {
        void *ret = Posix.bsearch (
            code_prefix,
            SCREEN_GAP_OVERRIDES,
            SCREEN_GAP_OVERRIDES.length,
            sizeof (ScreenGapOverride),
            (a, b) => {
                var needle = (string) a;
                var override = (ScreenGapOverride *) b;
                return strcmp (needle, override.code_prefix);
            }
        );

        if (ret == null)
            return null;

        return * ((ScreenGapOverride *) ret);
    }

    public int get_screen_gap (string game_code) {
        var code_prefix = game_code.substring (0, 3);

        var @override = find_screen_gap_override (code_prefix);
        if (@override != null)
            return @override.gap;

        return DEFAULT_SCREEN_GAP;
    }
}
