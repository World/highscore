// Shader that replicates the LCD dynamics from a GameBoy Advance
// Author: hunterk, Pokefan531
// Source: https://github.com/libretro/slang-shaders/blob/master/handheld/shaders/color/nds-color.slang (DS Phat)
// Source: https://github.com/libretro/slang-shaders/blob/master/handheld/shaders/color/sp101-color.slang (DS Lite)
// License: Public domain

#ifdef VERTEX

void hs_main() {}

#else // FRAGMENT

uniform bool u_dsPhatColors;

#define WHITE_BALANCE

#define target_gamma 2.0
#define display_gamma 2.0

vec4 ds_correct_color(vec4 rgba) {
  mat4 profile;

  if (u_dsPhatColors) {
    profile = mat4(
#ifdef WHITE_BALANCE
      0.815, 0.10, 0.1075, 0.0,  //red channel
      0.275, 0.64, 0.1725, 0.0,  //green channel
     -0.09,  0.26, 0.72,   0.0,  //blue channel
      0.0,   0.0,   0.0,   0.915 //alpha
#else
      0.705, 0.09,  0.1075, 0.0, //red channel
      0.235, 0.585, 0.1725, 0.0, //green channel
     -0.075, 0.24,  0.72,   0.0, //blue channel
      0.0,   0.0,   0.0,    1.0  //alpha
#endif
    );
  } else {
    profile = mat4(
#ifdef WHITE_BALANCE
      0.955, 0.0375,  0.0025, 0.0, //red channel
      0.11,  0.885,  -0.03,   0.0, //green channel
     -0.065, 0.0775,  1.0275, 0.0, //blue channel
      0.0,   0.0,     0.0,    0.94 //alpha channel
#else
      0.80,  0.135, 0.195, 0.0, //red channel
      0.275, 0.64,  0.155, 0.0, //green channel
     -0.075, 0.225, 0.65,  0.0, //blue channel
      0.0,   0.0,   0.0,   0.93 //alpha
#endif
    );
  }

  // bring out our stored luminance value
  float lum = profile[3].w;

  // our adjustments need to happen in linear gamma
  vec4 screen = pow(rgba, vec4(target_gamma)).rgba;

  screen = clamp(screen * lum, 0.0, 1.0);
  screen = profile * screen;

  return pow(screen, vec4(1.0 / display_gamma));
}

vec4 hs_main() {
  return ds_correct_color(texture(u_source, v_texCoord));
}

#endif