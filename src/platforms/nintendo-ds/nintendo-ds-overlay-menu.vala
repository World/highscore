// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.NintendoDs.OverlayMenu : OverlayMenuAddin {
    public LayoutMode layout_mode { get; set; }
    public bool flipped { get; set; }
    public bool bottom_screen_left { get; set; }
    public bool show_bottom_screen { get; set; }

    private SimpleAction flip_action;
    private SimpleAction swap_screens_action;
    private SimpleAction change_screen_action;

    public override string get_action_prefix () {
        return "ds";
    }

    construct {
        menu_model = ResourceUtils.load_menu (
            "/app/drey/Highscore/platforms/nintendo-ds/nintendo-ds-overlay-menu.ui"
        );

        add_action (new PropertyAction ("layout-mode", this, "layout-mode"));

        flip_action = new SimpleAction ("flip", null);
        flip_action.activate.connect (() => flipped = !flipped);
        add_action (flip_action);

        swap_screens_action = new SimpleAction ("swap-screens", null);
        swap_screens_action.activate.connect (() => {
            if (layout_mode == SIDE_BY_SIDE)
                bottom_screen_left = !bottom_screen_left;
            else
                show_bottom_screen = !show_bottom_screen;
        });
        add_action (swap_screens_action);

        change_screen_action = new SimpleAction ("change-screen", null);
        change_screen_action.activate.connect (() => {
            show_bottom_screen = !show_bottom_screen;
        });
        add_action (change_screen_action);

        notify["layout-mode"].connect (update_actions);

        update_actions ();
    }

    private void update_actions () {
        flip_action.set_enabled (layout_mode == BOOK);
        swap_screens_action.set_enabled (
            layout_mode == SIDE_BY_SIDE || layout_mode == SEPARATE_WINDOWS
        );
        change_screen_action.set_enabled (layout_mode == SINGLE_SCREEN);
    }
}
