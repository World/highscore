// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/nintendo-ds/nintendo-ds-properties.ui")]
public class Highscore.NintendoDs.Properties : PlatformProperties {
    private Settings settings;

    public bool ds_phat_colors { get; set; }

    construct {
        settings = new GameSettings (game).get_platform ();

        settings.bind ("ds-phat-colors", this, "ds-phat-colors", DEFAULT);
    }
}
