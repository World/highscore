// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.NintendoDs.Metadata : Object, GameMetadata {
    private const float SCREEN_HEIGHT = 192;

    public string game_code { get; set; }

    public Metadata (string game_code) {
        Object (game_code: game_code);
    }

    public float get_screen_gap () {
        return Overrides.get_screen_gap (game_code) / SCREEN_HEIGHT;
    }

    protected VariantType serialize_type () {
        return VariantType.BYTESTRING;
    }

    protected Variant serialize () {
        return new Variant.bytestring (game_code);
    }

    protected void deserialize (Variant variant) {
        game_code = variant.get_bytestring ();
    }
}
