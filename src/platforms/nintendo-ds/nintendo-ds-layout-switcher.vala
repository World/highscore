// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/nintendo-ds/nintendo-ds-layout-switcher.ui")]
public class Highscore.NintendoDs.LayoutSwitcher : Adw.Bin {
    public LayoutMode layout_mode { get; set; }
    public bool flipped { get; set; }
    public bool bottom_screen_left { get; set; }
    public bool show_bottom_screen { get; set; }

    [GtkChild]
    private unowned Gtk.MenuButton menu_button;

    construct {
        update_state ();

        notify["layout-mode"].connect (update_state);
    }

    static construct {
        install_property_action ("ds.layout-mode", "layout-mode");

        install_action ("ds.swap-screens", null, widget => {
            var switcher = widget as LayoutSwitcher;

            if (switcher.layout_mode == SIDE_BY_SIDE)
                switcher.bottom_screen_left = !switcher.bottom_screen_left;
            else
                switcher.show_bottom_screen = !switcher.show_bottom_screen;
        });

        install_action ("ds.flip", null, widget => {
            var switcher = widget as LayoutSwitcher;

            switcher.flipped = !switcher.flipped;
        });

        install_action ("ds.change-screen", null, widget => {
            var switcher = widget as LayoutSwitcher;

            switcher.show_bottom_screen = !switcher.show_bottom_screen;
        });
    }

    private void update_state () {
        action_set_enabled (
            "ds.swap-screens",
            layout_mode == SIDE_BY_SIDE || layout_mode == SEPARATE_WINDOWS
        );
        action_set_enabled ("ds.flip", layout_mode == BOOK);
        action_set_enabled ("ds.change-screen", layout_mode == SINGLE_SCREEN);
    }

    public void track_fullscreen (FullscreenView view) {
        view.track_menu_button (menu_button);
    }
}
