// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.NeoGeoPocket.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("neo-geo-pocket", _("Neo Geo Pocket"));
        controller.add_sections ("joystick", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joystick", _("Joystick"),
            Hs.NeoGeoPocketButton.UP,
            Hs.NeoGeoPocketButton.DOWN,
            Hs.NeoGeoPocketButton.LEFT,
            Hs.NeoGeoPocketButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.NeoGeoPocketButton.A,
            "b", _("B Button"), Hs.NeoGeoPocketButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "option", _("Option Button"), Hs.NeoGeoPocketButton.OPTION,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joystick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joystick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joystick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joystick:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "a");
        kbd_mapping.map (Linux.Input.KEY_D,     "b");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "option");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("joystick");
        gamepad_mapping.map_button (SOUTH, "a");
        gamepad_mapping.map_button (EAST,  "b");
        gamepad_mapping.map_button (START, "option");
        gamepad_mapping.map_stick  (LEFT,  "joystick");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "neo-geo-pocket";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->neo_geo_pocket.buttons, button, pressed);
        });
    }
}
