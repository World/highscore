// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.NeoGeoPocket.Overrides {
    private struct ColorOverride {
        uint16 catalog;
        uint8 subcatalog;
        bool color;
    }

    // Fix incorrect modes
    private const ColorOverride[] COLOR_OVERRIDES = {
        { 0,    16,  true  }, // Neo-Neo! V1.0 (PD)
        { 51,   33,  false }, // Dokodemo Mahjong (Japan)
        { 4660, 161, true  }, // Cool Cool Jam (USA, Europe) (Sample)
    };

    public bool? get_color_override (uint16 catalog, uint8 subcatalog) {
        foreach (var o in COLOR_OVERRIDES) {
            if (o.catalog == catalog && o.subcatalog == subcatalog)
                return o.color;
        }

        return null;
    }
}
