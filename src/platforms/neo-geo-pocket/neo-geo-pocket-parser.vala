// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://gbdev.io/pandocs/The_Cartridge_Header.html
public class Highscore.NeoGeoPocket.Parser : GameParser {
    private const size_t CATALOG_OFFSET = 0x20;
    private const uint8 MODE_GRAYSCALE = 0;
    private const uint8 MODE_COLOR = 0x10;

    private bool color;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    public override bool parse () throws Error {
        var stream = new DataInputStream (file.read ());

        stream.seek (CATALOG_OFFSET, SET);

        uint8 catalog1 = stream.read_byte ();
        uint8 catalog2 = stream.read_byte ();
        uint8 subcatalog = stream.read_byte ();
        uint8 mode = stream.read_byte ();

        if (mode != MODE_GRAYSCALE && mode != MODE_COLOR) {
            parser_debug ("Unknown mode: %u", mode);
            return false;
        }

        var override_color = Overrides.get_color_override (
            catalog2 << 8 | catalog1, subcatalog
        );

        color = override_color ?? (mode == MODE_COLOR);

        if (override_color != null) {
            parser_debug (
                "Found mode override for (0x%X, 0x%X): %s",
                catalog2 << 8 | catalog1, subcatalog,
                override_color ? "color" : "monochrome"
            );
        } else {
            parser_debug ("Found mode: %s", color ? "color" : "monochrome");
        }

        return true;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (color);
    }
}
