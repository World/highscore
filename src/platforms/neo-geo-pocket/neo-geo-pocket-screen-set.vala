// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.NeoGeoPocket.ScreenSet : Highscore.ScreenSet {
    public bool is_accurate { get; set; }

    private GameSettings game_settings;

    construct {
        game_settings = new GameSettings (game);
        game_settings.bind_property ("is-accurate-filter", this, "is-accurate", SYNC_CREATE);

        notify["is-accurate"].connect (() => filter_changed ());
    }

    public override string get_filter_path () {
        return "/app/drey/Highscore/platforms/neo-geo-pocket/neo-geo-pocket.glsl";
    }

    public override void setup_filter (GLShader shader, int screen_id) {
        var metadata = game.metadata as Metadata;

        if (metadata.color) {
            shader.set_uniform_bool ("u_correctColors", is_accurate);
            shader.set_uniform_bool ("u_monochromeColors", false);
        } else {
            shader.set_uniform_bool ("u_correctColors", false);
            shader.set_uniform_bool ("u_monochromeColors", is_accurate && mode != DISPLAY);
        }
    }

    public override ScreenType get_screen_type (Hs.Region region) {
        var metadata = game.metadata as Metadata;

        if (metadata.color)
            return NEO_GEO_POCKET_COLOR;

        return NEO_GEO_POCKET;
    }
}
