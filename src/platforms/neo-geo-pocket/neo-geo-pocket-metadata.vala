// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.NeoGeoPocket.Metadata : Object, GameMetadata {
    public bool color { get; set; }

    public Metadata (bool color) {
        Object (color: color);
    }

    protected VariantType serialize_type () {
        return VariantType.BOOLEAN;
    }

    protected Variant serialize () {
        return color;
    }

    protected void deserialize (Variant variant) {
        color = variant.get_boolean ();
    }
}
