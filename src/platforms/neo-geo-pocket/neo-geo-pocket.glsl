// Shader that replicates the LCD dynamics from a Game Boy Advance
// Author: hunterk, Pokefan531
// Source: https://github.com/libretro/slang-shaders/blob/master/handheld/shaders/color/gba-color.slang
// License: Public domain

#ifdef VERTEX

void hs_main() {}

#else // FRAGMENT

uniform bool u_correctColors;
uniform bool u_monochromeColors;

#define TARGET_GAMMA 1.6
#define DISPLAY_GAMMA 2.2

#define MinInputLuminance 0.066666667
#define MaxInputLuminance 1.0
#define BackgroundColor vec3(0.16470588235294117, 0.16470588235294117, 0.14901960784313725)
#define ForegroundColor vec3(0.5333333333333333, 0.596078431372549, 0.5372549019607843)

vec4 apply_monochrome_colors (vec3 color) {
  float luminance = 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;

  luminance = clamp(luminance, MinInputLuminance, MaxInputLuminance);
  luminance = (luminance - MinInputLuminance) / (MaxInputLuminance - MinInputLuminance);

  color = mix(BackgroundColor, ForegroundColor, luminance);

  return vec4(color, 1.0);
}

vec4 correct_color(vec4 rgba) {
  float darken_intensity = 0.5;
  mat4 profile = mat4(
    0.80,  0.135, 0.195, 0.0, //red channel
    0.275, 0.64,  0.155, 0.0, //green channel
   -0.075, 0.30,  0.65,  0.0, //blue channel
    0.0,   0.0,   0.0,   0.93 //alpha
  );

  // bring out our stored luminance value
  float lum = profile[3].w;

  vec4 screen = pow(rgba, vec4(TARGET_GAMMA + darken_intensity)).rgba;

  screen = clamp(screen * lum, 0.0, 1.0);
  screen = profile * screen;

  return pow(screen, vec4(1.0 / DISPLAY_GAMMA));
}

vec4 hs_main() {
  vec4 color = texture(u_source, v_texCoord);

  if (u_correctColors)
    return correct_color(color);

  if (u_monochromeColors)
    return apply_monochrome_colors(color.rgb);

  return color;
}

#endif