// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.PcEngineCd.Proxy : Object {
    public abstract async void set_bios_path (string path) throws Error;
}
