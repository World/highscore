// This file is part of Highscore. License: GPL-3.0-or-later
// vala-lint=skip-file

public class Highscore.PcEngineCd.FirmwareList : Highscore.FirmwareList {
    construct {
        var firmware = new Firmware ("syscard3", "System Card 3");

        firmware.filename = "syscard3.pce";

        // Without header
        firmware.add_checksum (
            "38179df8f4ac870017db21ebcbf53114",
            "7880d3adf175ce7423e861cfc4f70d7493a89bb7950340732359f0c9bd9eacf8823602b4ad1217f2c42884f2b226ac8a461c39913bfef390c259bf88d0214040"
        );

        // With header
        firmware.add_checksum (
            "ff1a674273fe3540ccef576376407d1d",
            "6e771b9f660ae227256ecaf173e1a7e4073c43a606747a4b4550addd96b0ee3876400641069168d370a09a34520104bb7c5c5bc34f78c5c2617dcd472896de18"
        );

        add_firmware (firmware);
    }

    public override async void load (string id, File file, Runner runner) throws Error {
        var proxy = runner.addon_proxy as Proxy;

        if (id == "syscard3")
            yield proxy.set_bios_path (file.get_path ());
    }
}
