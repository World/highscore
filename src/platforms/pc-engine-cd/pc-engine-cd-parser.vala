// This file is part of Highscore. License: GPL-3.0-or-later

// Reference: https://retrocomputing.stackexchange.com/questions/27518/did-the-pc-engine-turbografx-super-cd-rom-have-a-standardized-file-system
public class Highscore.PcEngineCd.Parser : GameParser {
    private const string CD_MAGIC_VALUE = "PC Engine CD-ROM SYSTEM";

    private string[] data_paths;
    private string checksum;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    public override bool parse () throws Error {
        var context = Object.new (typeof (Mirage.Context)) as Mirage.Context;
        var disc = context.load_image ({ file.get_path () });

        if (disc.get_medium_type () != CD) {
            parser_debug ("Not a CD, discarding");
            return false;
        }

        if (disc.get_number_of_tracks () < 2) {
            parser_debug ("Game must have at least 2 tracks, discarding");
            return false;
        }

        var track = disc.get_track_by_index (1);
        if (track.get_sector_type () != MODE1) {
            parser_debug ("Track 1 is not Mode 1, discarding");
            return false;
        }

        if (!CdUtils.find_magic_string (track, CD_MAGIC_VALUE)) {
            parser_debug ("Failed to find magic string, discarding");
            return false;
        }

        data_paths = CdUtils.get_disc_extra_paths (file.get_path (), disc);
        checksum = CdUtils.get_disc_checksum (disc);

        parser_debug ("Success");
        return true;
    }

    public override string get_media_uid () throws Error {
        return @"$(platform.id)-$checksum";
    }

    public override string[]? get_extra_paths () {
        return data_paths;
    }
}
