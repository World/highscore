// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.MegaDrive.RunnerDelegate : Highscore.RunnerDelegate {
    private uint n_players;

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        n_players = yield proxy.get_players ();

        var metadata = runner.game.metadata as Metadata;
        if (metadata.supports_6_button_pad ()) {
            for (int i = 0; i < n_players; i++)
                runner.set_controller_type (i, "mega-drive-6button");
        }
    }

    public override uint get_n_players () {
        return n_players;
    }
}
