// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.MegaDrive.Controls : PlatformControls {
    construct {
        n_players = Hs.MEGA_DRIVE_MAX_PLAYERS;

        var controller = new PlatformController ("mega-drive", _("Control Pad"));
        controller.add_sections ("dpad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Directional Button"),
            Hs.MegaDriveButton.UP,
            Hs.MegaDriveButton.DOWN,
            Hs.MegaDriveButton.LEFT,
            Hs.MegaDriveButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("Button A"), Hs.MegaDriveButton.A,
            "b", _("Button B"), Hs.MegaDriveButton.B,
            "c", _("Button C"), Hs.MegaDriveButton.C,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "start", _("Start Button"), Hs.MegaDriveButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_A,         "a");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "c");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (WEST,   "a");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "c");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // 6-button controller

        controller = new PlatformController (
            "mega-drive-6button",
            _("Control Pad (6-Button)"),
            "platform-mega-drive-6button-symbolic"
        );
        controller.add_sections (
            "dpad", "face-buttons", "face-buttons-xyz", "menu", null
        );

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Directional Button"),
            Hs.MegaDriveButton.UP,
            Hs.MegaDriveButton.DOWN,
            Hs.MegaDriveButton.LEFT,
            Hs.MegaDriveButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("Button A"), Hs.MegaDriveButton.A,
            "b", _("Button B"), Hs.MegaDriveButton.B,
            "c", _("Button C"), Hs.MegaDriveButton.C,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons-xyz",
            "x", _("Button X"), Hs.MegaDriveButton.X,
            "y", _("Button Y"), Hs.MegaDriveButton.Y,
            "z", _("Button Z"), Hs.MegaDriveButton.Z,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "mode",  _("Mode Button"),  Hs.MegaDriveButton.MODE,
            "start", _("Start Button"), Hs.MegaDriveButton.START,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_A,         "a");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "c");
        kbd_mapping.map (Linux.Input.KEY_Q,         "x");
        kbd_mapping.map (Linux.Input.KEY_W,         "y");
        kbd_mapping.map (Linux.Input.KEY_E,         "z");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "mode");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (WEST,   "a");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "c");
        gamepad_mapping.map_button (NORTH,  "z");
        gamepad_mapping.map_button (L,      "x");
        gamepad_mapping.map_button (R,      "y");
        gamepad_mapping.map_button (SELECT, "mode");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        default_controller = "mega-drive";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->mega_drive.pad_buttons[player], button, pressed);
        });
    }
}
