// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://plutiedev.com/rom-header
public class Highscore.MegaDrive.Parser : GameParser {
    private const size_t OFFSET = 0x100;
    private const size_t SIZE = 0xFF;

    private const size_t MAGIC_LENGTH = 16;
    private const string MAGIC_SEGA = "SEGA ";
    private const string MAGIC_GENESIS = "GENESIS";
    private const string MAGIC_MEGA_DRIVE = "MEGA DRIVE";

    private const size_t SERIAL_OFFSET = 0x180;
    private const size_t SERIAL_LENGTH = 14;

    private const size_t DEVICE_OFFSET = 0x190;
    private const size_t DEVICE_LENGTH = 16;

    private string serial;
    private string devices;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    private bool check_magic (uint8[] magic) {
        for (int i = 0; i < MAGIC_SEGA.length; i++) {
            if (magic[i] != ((uint8[]) MAGIC_SEGA)[i])
                return false;
        }

        bool is_genesis = true, is_mega_drive = true;

        for (int i = 0; i < MAGIC_GENESIS.length; i++) {
            if (magic[MAGIC_SEGA.length + i] != ((uint8[]) MAGIC_GENESIS)[i]) {
                is_genesis = false;
                break;
            }
        }

        for (int i = 0; i < MAGIC_MEGA_DRIVE.length; i++) {
            if (magic[MAGIC_SEGA.length + i] != ((uint8[]) MAGIC_MEGA_DRIVE)[i]) {
                is_mega_drive = false;
                break;
            }
        }

        return is_genesis || is_mega_drive;
    }

    public override bool parse () throws Error {
        var stream = new DataInputStream (file.read ());

        var info = file.query_info (FileAttribute.STANDARD_SIZE, NONE);
        int64 size = info.get_size ();

        if (size < OFFSET + SIZE) {
            parser_debug ("File is too small, discarding");
            return false;
        }

        stream.seek (OFFSET, SET);

        var magic_buffer = new uint8[MAGIC_LENGTH];
        stream.read (magic_buffer);

        if (!check_magic (magic_buffer)) {
            parser_debug ("Missing header");
            serial = "";
            devices = "";
            return true;
        }

        stream.seek (SERIAL_OFFSET, SET);

        var serial_buffer = new uint8[SERIAL_LENGTH];
        stream.read (serial_buffer);

        var serial_builder = new StringBuilder ();
        serial_builder.append_len ((string) serial_buffer, (ssize_t) SERIAL_LENGTH);
        serial = serial_builder.free_and_steal ();

        stream.seek (DEVICE_OFFSET, SET);

        var device_buffer = new uint8[DEVICE_LENGTH];
        stream.read (device_buffer);

        var device_builder = new StringBuilder ();

        for (int i = 0; i < DEVICE_LENGTH; i++) {
            uint8 c = device_buffer[i];

            if (c == ' ' || c == 0 || c == 0xFF)
                continue;

            device_builder.append_c ((char) c);
        }

        devices = device_builder.free_and_steal ();
        parser_debug ("Found metadata: serial '%s', devices '%s'", serial, devices);

        return true;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (serial, devices);
    }
}
