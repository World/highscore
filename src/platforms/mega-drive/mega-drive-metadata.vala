// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.MegaDrive.Metadata : Object, GameMetadata {
    public string serial { get; set; }
    public string devices { get; set; }

    public Metadata (string serial, string devices) {
        Object (serial: serial, devices: devices);
    }

    protected VariantType serialize_type () {
        return new VariantType.tuple ({
            VariantType.BYTESTRING,
            VariantType.BYTESTRING
        });
    }

    protected Variant serialize () {
        return new Variant.tuple ({
            new Variant.bytestring (serial),
            new Variant.bytestring (devices),
        });
    }

    protected void deserialize (Variant variant) {
        serial = variant.get_child_value (0).get_bytestring ();
        devices = variant.get_child_value (1).get_bytestring ();
    }

    public bool supports_6_button_pad () {
        return devices == "" || devices.contains ("6");
    }
}
