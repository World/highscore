// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Sg1000.RunnerDelegate : Highscore.RunnerDelegate {
    private uint n_players;

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        n_players = yield proxy.get_players ();
    }

    public override uint get_n_players () {
        return n_players;
    }
}
