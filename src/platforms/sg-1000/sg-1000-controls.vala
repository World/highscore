// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Sg1000.Controls : PlatformControls {
    construct {
        n_players = Hs.SG1000_MAX_PLAYERS;

        var controller = new PlatformController ("sg-1000", _("SG-1000 Joystick"));
        controller.add_sections ("joystick", "buttons", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joystick", _("Joystick"),
            Hs.Sg1000Button.UP,
            Hs.Sg1000Button.DOWN,
            Hs.Sg1000Button.LEFT,
            Hs.Sg1000Button.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "buttons",
            "left", _("Left Button"),  Hs.Sg1000Button.ONE,
            "right", _("Right Button"), Hs.Sg1000Button.TWO,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joystick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joystick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joystick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joystick:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "left");
        kbd_mapping.map (Linux.Input.KEY_D,     "right");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("joystick");
        gamepad_mapping.map_button (SOUTH, "left");
        gamepad_mapping.map_button (EAST,  "right");
        gamepad_mapping.map_stick  (LEFT,  "joystick");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "sg-1000";

        // global

        global_controller = new PlatformController ("console", _("Built-in"));
        global_controller.add_section ("system");

        var pause_button = new ButtonControl ("pause", _("Pause Button"));
        pause_button.activate.connect ((runner, player, pressed) => {
            set_pause_button_pressed (runner, pressed);
        });
        global_controller.add_control ("system", pause_button);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_ENTER, "pause");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (START, "pause");
        global_controller.default_gamepad_mapping = gamepad_mapping;
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->sg1000.pad_buttons[player], button, pressed);
        });
    }

    private void set_pause_button_pressed (Runner runner, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->sg1000.pause_button = pressed;
        });
    }
}
