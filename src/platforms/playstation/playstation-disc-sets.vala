// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.PlayStation.DiscSets {
    private inline DiscIdIndexEntry? find_disc_set_index_entry (string disc_id) {
        void *ret = Posix.bsearch (
            disc_id,
            DISC_ID_INDEX,
            DISC_ID_INDEX.length,
            sizeof (DiscIdIndexEntry),
            (a, b) => {
                var needle = (string) a;
                var entry = (DiscIdIndexEntry *) b;
                return strcmp (needle, entry.disc_id);
            }
        );

        if (ret == null)
            return null;

        return * ((DiscIdIndexEntry *) ret);
    }

    public string[]? get_disc_set (string disc_id) {
        var index_entry = find_disc_set_index_entry (disc_id);
        if (index_entry == null)
            return null;

        string[] ret = {};

        assert (index_entry.disc_set_index < DISC_SETS.length);

        var disc_set = DISC_SETS[index_entry.disc_set_index];

        for (int i = 0; i < disc_set.n_discs; i++) {
            int index = disc_set.disc_ids[i];

            assert (index < DISC_ID_INDEX.length);

            ret += DISC_ID_INDEX[index].disc_id;
        }

        return ret;
    }
}
