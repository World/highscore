// This file is part of Highscore. License: GPL-3.0-or-later
// vala-lint=skip-file

public class Highscore.PlayStation.FirmwareList : Highscore.FirmwareList {
    construct {
        var jp_firmware = new Firmware ("jp", "BIOS (Japan)");

        jp_firmware.filename = "psx_bios_jp.bin";

        // BIOS v1.0 (Japan)
        jp_firmware.add_checksum (
            "239665b1a3dade1b5a52c06338011044",
            "a8b1ac54379bef44f605940f1f6615daeaefb41f85c3d46f1ff007017c8c61bc602d2e83af5be958aa71dccdee531ff79933a69082a17b743ff4f8dd417f5e47"
        );

        // BIOS v1.1 (Japan)
        jp_firmware.add_checksum (
            "849515939161e62f6b866f6853006780",
            "66b1f9381143a20baf256f836ddde319ffd522954bdcbfcec6106dbf7ebe45ab076d3f44d8d152d95d82e441755dce4e48dbd36bc5dee64d79a4c9ef40193cda"
        );

        // BIOS v2.1 (Japan)
        jp_firmware.add_checksum (
            "cba733ceeff5aef5c32254f1d617fa62",
            "6becb8125f3b7f279a703977a1bbf0b53d23547d391d1205127898980c276703f03aa5e5ce0b1ca1224e60ec9ed693a7926d9b7065e3bdb6bfc39e21470360d9"
        );

        // BIOS v2.2 (Japan)
        jp_firmware.add_checksum (
            "57a06303dfa9cf9351222dfcbb4a29d9",
            "d1caa96f3856f6a807bddea1875d7dcfb75f1a45a3536b1e64d5b35b028c0b572eb23f1d2dc7eb3136374121c5085c7c41a345fb5c625b326adbbb5ebf3df17e"
        );

        // BIOS v3.0 (Japan)
        jp_firmware.add_checksum (
            "8dd7d5296a650fac7319bce665a6a53c",
            "45afbc30b189ee84db54f329b61dea51336b5b38c47a0f0a7035edfaeb61981c8c8babdb6c8d743f7e7b6a05b8c6bd4e73e4f7a4a81fa459107407eceae345d7"
        );

        // BIOS v4.0 (Japan)
        jp_firmware.add_checksum (
            "8e4c14f567745eff2f0408c8129f72a6",
            "5d6499ab8bf17e45d574ca57e636ce36f9c71183a4a09b06b9d97912b31e3fb0f8daf086bf90853f87574f73033048d9768d7f4d0704dea1f3a235588c81068f"
        );

        // PS Classic BIOS v4.5 (Region-Free)
        jp_firmware.add_checksum (
            "c53ca5908936d412331790f4426c6c33",
            "8220ba9b30c3cdcf38db0648bc1f1bfe42cb995c2fd43ba6645c812cf47e4203d592c64ca922a91220e498a75dfb9ee2ba584bc4bb112e85995f03fca0e13286"
        );

        add_firmware (jp_firmware);

        var us_firmware = new Firmware ("us", "BIOS (North America)");

        us_firmware.filename = "psx_bios_us.bin";

        // BIOS v2.2 (North America)
        us_firmware.add_checksum (
            "924e392ed05558ffdb115408c263dccf",
            "ea097d56a3a6fa41e23908fbf73fab555c951059280d229815a15d79a168285c4bf993c8be43275912c2eccd40a0c76c797a95c13e82d532e0d48aaba6597430"
        );

        // BIOS v3.0 (North America)
        us_firmware.add_checksum (
            "490f666e1afb15b7362b406ed1cea246",
            "6fb786f2bc77c29a52999d23f85d3ebf8dc0a23e849ab4ec5cd3c7cca42055332004263f674152846fc0cb1500f67d52a1ec244a8224b59e48b245c6adb4d5fb"
        );

        // BIOS v3.0 (North America)
        // Actually closer to v3.5
        us_firmware.add_checksum (
            "490f666e1afb15b7362b406ed1cea246",
            "6fb786f2bc77c29a52999d23f85d3ebf8dc0a23e849ab4ec5cd3c7cca42055332004263f674152846fc0cb1500f67d52a1ec244a8224b59e48b245c6adb4d5fb"
        );

        // BIOS v4.0 (North America)
        us_firmware.add_checksum (
            "1e68c231d0896b7eadcad1d7d8e76129",
            "8b20252f4418bde9d68763c203e04a3b61e5fd3f06e2a7c136895eae521b7927a3e52865533855fd079ce7284214091744a656536c45decd1d47fb98da6fe9d6"
        );

        // PSOne BIOS v4.5 (North America)
        us_firmware.add_checksum (
            "6e3735ff4c7dc899ee98981385f6f3d0",
            "73e289afcd618d253e68893497f0849d6d6d3ac2e4f57f3d292b7b608982719e2e7a1db020bc3b4c1e1582d2a60aa90089d648bda335d6d48b23290244711649"
        );

        // PS Classic BIOS v4.5 (Region-Free)
        us_firmware.add_checksum (
            "c53ca5908936d412331790f4426c6c33",
            "8220ba9b30c3cdcf38db0648bc1f1bfe42cb995c2fd43ba6645c812cf47e4203d592c64ca922a91220e498a75dfb9ee2ba584bc4bb112e85995f03fca0e13286"
        );

        add_firmware (us_firmware);

        var eu_firmware = new Firmware ("eu", "BIOS (Europe)");

        eu_firmware.filename = "psx_bios_eu.bin";

        // BIOS v2.0 (Europe)
        eu_firmware.add_checksum (
            "54847e693405ffeb0359c6287434cbef",
            "57428fafb68945dc497ce79d65ea436ee4ab36c7dee7682ae7fceb3b419678c0ea5ba0d34fd8c4cfdb59d9a20bec4fb391b64583ef4ef43b3845c7799c72e39f"
        );

        // BIOS v2.2 (Europe)
        eu_firmware.add_checksum (
            "e2110b8a2b97a8e0b857a45d32f7e187",
            "9de24aaf580fca7c87cb5c81167743b090215e7db76398766ecd53c496e3c04df170862bfc2ecd06b5836c07266eb140c93346995a79baae88c2bcb9088eea83"
        );

        // BIOS v3.0 (Europe)
        eu_firmware.add_checksum (
            "32736f17079d0b2b7024407c39bd3050",
            "45d3c9549030a187ce174f222238637c534a4b17eb28047a02aaa9c05f002f68a9724392d03800294e1246de86386de2ef00a242c7306dd6aaa50b41906ae76a"
        );

        // BIOS v4.1 (Europe)
        eu_firmware.add_checksum (
            "b9d9a0286c33dc6b7237bb13cd46fdee",
            "65d56e55be7dfdfd549137a877a831f17e0e22bbb00d01f35e7ab1baa274f33cff9d786d804cdfb911504f07a7d41b6e9c8bbad409cb91fbd95fe44442979a4c"
        );

        // PS Classic BIOS v4.5 (Region-Free)
        eu_firmware.add_checksum (
            "c53ca5908936d412331790f4426c6c33",
            "8220ba9b30c3cdcf38db0648bc1f1bfe42cb995c2fd43ba6645c812cf47e4203d592c64ca922a91220e498a75dfb9ee2ba584bc4bb112e85995f03fca0e13286"
        );

        add_firmware (eu_firmware);
    }

    public override async void load (string id, File file, Runner runner) throws Error {
        var proxy = runner.platform_proxy as Proxy;

        if (id == "jp")
            yield proxy.set_bios_path (JP, file.get_path ());
        if (id == "us")
            yield proxy.set_bios_path (US, file.get_path ());
        if (id == "eu")
            yield proxy.set_bios_path (EU, file.get_path ());
    }

    public override async Firmware[] get_used_firmware (Runner runner) throws Error {
        var proxy = runner.platform_proxy as Proxy;

        switch (yield proxy.get_used_bios ()) {
            case JP:
                return { get_firmware ("jp") };
            case US:
                return { get_firmware ("us") };
            case EU:
                return { get_firmware ("eu") };
            default:
                assert_not_reached ();
        }
    }
}
