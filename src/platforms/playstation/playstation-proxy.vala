// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.PlayStation.Proxy : Object {
    public signal void dualshock_mode_changed (uint player);
    public abstract async uint get_players () throws Error;
    public abstract async Hs.PlayStationDualShockMode get_dualshock_mode (uint player) throws Error;
    public abstract async bool set_dualshock_mode (uint player, Hs.PlayStationDualShockMode mode) throws Error;
    public abstract async void set_bios_path (Hs.PlayStationBios bios, string path) throws Error;
    public abstract async Hs.PlayStationBios get_used_bios () throws Error;
}
