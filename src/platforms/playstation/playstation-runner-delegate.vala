// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlayStation.RunnerDelegate : Highscore.RunnerDelegate {
    private uint n_players;
    public bool dualshock_analog { get; set; }

    private Overrides.ControllerFlags controller_flags;
    private Hs.PlayStationDualShockMode ds_mode[Hs.PLAYSTATION_MAX_PLAYERS];

    private Settings settings;

    construct {
        settings = new GameSettings (runner.game).get_platform ();

        var metadata = runner.game.metadata as Metadata;

        controller_flags = Overrides.get_controllers (metadata.disc_id);

        for (uint player = 0; player < Hs.PLAYSTATION_MAX_PLAYERS; player++)
            ds_mode[player] = ANALOG;

        notify["dualshock-analog"].connect (() => {
            update_controllers.begin ();
        });

        settings.bind ("dualshock-analog", this, "dualshock-analog", DEFAULT);

        if (settings.get_user_value ("dualshock-analog") == null)
            settings.set_boolean ("dualshock-analog", ANALOG in controller_flags);
    }

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        n_players = yield proxy.get_players ();

        proxy.dualshock_mode_changed.connect (player => {
            dualshock_mode_changed_cb.begin (player);
        });

        for (uint player = 0; player < n_players; player++)
            runner.set_rumble_enabled (player, true);

        yield update_controllers ();
    }

    public override async void after_reset (bool hard) throws Error {
        yield update_controllers ();
    }

    public override async void load_state (SnapshotPlatformMetadata? metadata) throws Error {
        yield update_controllers ();
    }

    public override uint get_n_players () {
        return n_players;
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        var n_discs = runner.game.get_media ().length;
        if (n_discs < 2)
            return null;

        string[] discs = {};

        for (int i = 0; i < n_discs; i++)
            discs += _("Disc %u").printf (i + 1);

        var switcher = new MediaSwitcher (CD, _("Switch Disc"), discs);

        switcher.track_fullscreen (view);

        runner.bind_property (
            "current-media", switcher, "selected-media", SYNC_CREATE | BIDIRECTIONAL
        );

        return switcher;
    }

    public override OverlayMenuAddin? create_overlay_menu () {
        var n_discs = runner.game.get_media ().length;
        if (n_discs < 2)
            return null;

        string[] discs = {};

        for (int i = 0; i < n_discs; i++)
            discs += _("Disc %u").printf (i + 1);

        var switcher = new OverlayMenuMediaSwitcher ("psx", _("Switch Disc"), discs);

        runner.bind_property (
            "current-media", switcher, "selected-media", SYNC_CREATE | BIDIRECTIONAL
        );

        return switcher;
    }

    private async void update_controllers () throws Error {
        for (uint player = 0; player < n_players; player++)
            yield update_controller_for_player (player);
    }

    private async void update_controller_for_player (uint player) throws Error {
        var proxy = runner.platform_proxy as Proxy;
        bool is_digital = DIGITAL in controller_flags;
        bool is_analog = ANALOG in controller_flags;
        bool is_dualshock = DUALSHOCK in controller_flags;

        if (is_digital && (is_dualshock || is_analog)) {
            // User-configurable
            if (dualshock_analog)
                ds_mode[player] = ANALOG;
            else
                ds_mode[player] = DIGITAL;
        } else if (is_digital) {
            ds_mode[player] = DIGITAL;
        } else {
            ds_mode[player] = ANALOG;
        }

        // We don't know if the current game allows switching modes,
        // so make sure to check results
        if (!(yield proxy.set_dualshock_mode (player, ds_mode[player])))
            ds_mode[player] = yield proxy.get_dualshock_mode (player);

        if (ds_mode[player] == ANALOG)
            runner.set_controller_type (player, "playstation-dualshock");
        else
            runner.set_controller_type (player, "playstation");
    }

    private async void dualshock_mode_changed_cb (uint player) throws Error {
        var proxy = runner.platform_proxy as Proxy;
        var mode = yield proxy.get_dualshock_mode (player);

        if (mode == ds_mode[player])
            return;

        ds_mode[player] = mode;
        yield update_controller_for_player (player);
    }
}
