// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlayStation.Parser : GameParser {
    private static Regex? disc_id_regex;

    private string[] data_paths;
    private string disc_id;
    private string checksum;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    private void ensure_disc_id_regex () {
        if (disc_id_regex != null)
            return;

        try {
            disc_id_regex = new Regex ("([A-Z]{3,5})[-_]?([0-9X]{2,6})");
        } catch (Error e) {
            error (e.message);
        }
    }

    private string fixup_disc_id (string letters, string numbers) {
        if (letters == "SLUSP")
            letters = "SLUS";

        return @"$letters-$numbers";
    }

    private string? get_disc_id_from_volume_id (owned string volume_id) {
        if (volume_id == "")
            return null;

        ensure_disc_id_regex ();

        volume_id = volume_id.ascii_up ();

        MatchInfo info;
        if (disc_id_regex.match (volume_id, 0, out info)) {
            string letters = info.fetch (1);
            string numbers = info.fetch (2);

            return fixup_disc_id (letters, numbers);
        }

        return null;
    }

    private string? get_disc_id_from_system_cnf (uint8[] data) {
        var builder = new StringBuilder ();
        builder.append_len ((string) data, data.length);
        var str = builder.free_and_steal ();

        var lines = str.split ("\n", 2);
        if (lines.length < 2)
            return null;

        var boot_line = lines[0];

        boot_line = boot_line.replace (".", "");
        boot_line = boot_line.up ();

        ensure_disc_id_regex ();

        MatchInfo info;
        if (disc_id_regex.match (boot_line, 0, out info)) {
            string letters = info.fetch (1);
            string numbers = info.fetch (2);

            return fixup_disc_id (letters, numbers);
        }

        return null;
    }

    private void ensure_checksum (Mirage.Disc disc) throws Error {
        if (checksum != null)
            return;

        checksum = CdUtils.get_disc_checksum (disc);
    }

    private void find_disc_set () {
        if (disc_id == null)
            return;

        var disc_set = DiscSets.get_disc_set (disc_id);
        if (disc_set == null)
            return;

        set_incomplete (disc_id, disc_set);
    }

    private void ensure_sbi () throws Error {
        bool needs_sbi = Overrides.get_needs_sbi (disc_id);
        if (!needs_sbi)
            return;

        var sbi_file = FileUtils.replace_extension (file, "sbi");
        if (sbi_file.query_exists ()) {
            data_paths += sbi_file.get_path ();
            parser_debug ("Found SBI file");
            return;
        }

        string basename;
        FileUtils.split_filename (file.get_basename (), out basename, null);

        parser_debug ("Missing SBI file");
        throw new ParserError.INCOMPLETE_GAME (
            "“%s” is copy-protected and needs an SBI file to run", basename
        );
    }

    public override bool parse () throws Error {
        var context = Object.new (typeof (Mirage.Context)) as Mirage.Context;
        var disc = context.load_image ({ file.get_path () });

        if (disc.get_medium_type () != CD) {
            parser_debug ("Not a CD, discarding");
            return false;
        }

        if (disc.get_number_of_tracks () == 0) {
            parser_debug ("Game must have at least 1 track, discarding");
            return false;
        }

        var track = disc.get_track_by_index (0);

        if (!track.enumerate_fragments (fragment => {
            var size = fragment.main_data_get_size ();

            if (size == 0)
                return true;

            if (size != 2352)
                return false;
        })) {
            parser_debug ("Wrong track size, discarding");
            return false;
        }

        var parser = new ISO9660.Parser (track);

        try {
            parser.parse ();
        } catch (ISO9660.ParserError.NOT_ISO9660 e) {
            parser_debug ("Track 1 is not ISO9660, discarding");
            return false;
        }

        if (parser.system_id != "PLAYSTATION") {
            parser_debug ("Wrong system ID, discarding");
            return false;
        }

        disc_id = get_disc_id_from_volume_id (parser.volume_id);
        if (disc_id != null) {
            parser_debug ("Found disc ID in volume ID: %s", disc_id);

            data_paths = CdUtils.get_disc_extra_paths (file.get_path (), disc);
            ensure_checksum (disc);
            find_disc_set ();
            ensure_sbi ();

            return true;
        }


        try {
            var system_cnf = parser.read_file ("/SYSTEM.CNF;1");

            disc_id = get_disc_id_from_system_cnf (system_cnf);

            parser_debug ("Found disc ID in SYSTEM.CNF: %s", disc_id);
        } catch (Error e) {
        }

        if (disc_id != null) {
            data_paths = CdUtils.get_disc_extra_paths (file.get_path (), disc);
            ensure_checksum (disc);
            find_disc_set ();
            ensure_sbi ();

            return true;
        }

        try {
            if (!parser.file_exists ("/PSX.EXE;1")) {
                parser_debug ("PSX.EXE doesn't exist, discarding");
                return false;
            }
        } catch (Error e) {
            parser_debug ("Failed to check PSX.EXE (%s), discarding", e.message);
            return false;
        }

        ensure_checksum (disc);
        disc_id = Overrides.get_disc_id (checksum);

        if (disc_id != null)
            parser_debug ("Found a disc ID override: %s", disc_id);

        data_paths = CdUtils.get_disc_extra_paths (file.get_path (), disc);
        find_disc_set ();

        if (disc_id == null)
            warning ("Failed to find disc ID for %s", file.get_basename ());

        ensure_sbi ();

        return true;
    }

    public override string get_game_uid (Media[] media) throws Error {
        var prefix = @"$(platform.id)-";
        var builder = new StringBuilder ();

        foreach (var m in media) {
            var checksum = m.uid.replace (prefix, "");

            builder.append (checksum);
        }

        var checksum = Checksum.compute_for_string (
            MD5, builder.free_and_steal ()
        );

        return @"$prefix$checksum";
    }

    public override string get_media_uid () throws Error {
        return @"$(platform.id)-$checksum";
    }

    public override string[]? get_extra_paths () {
        return data_paths;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (disc_id);
    }
}
