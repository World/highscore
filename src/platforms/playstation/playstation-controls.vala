// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlayStation.Controls : PlatformControls {
    construct {
        n_players = Hs.PLAYSTATION_MAX_PLAYERS;

        var controller = new PlatformController ("playstation", _("PlayStation Controller"));
        controller.add_sections (
            "dpad", "face-buttons", "shoulder-buttons", "menu", null
        );

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Directional Buttons"),
            Hs.PlayStationButton.UP,
            Hs.PlayStationButton.DOWN,
            Hs.PlayStationButton.LEFT,
            Hs.PlayStationButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            // Translators: @PS_TRIANGLE@ will be shown as an icon, don't change it
            "triangle", _("@PS_TRIANGLE@ Button"), Hs.PlayStationButton.TRIANGLE,
            // Translators: @PS_SQUARE@ will be shown as an icon, don't change it
            "square",   _("@PS_SQUARE@ Button"),   Hs.PlayStationButton.SQUARE,
            // Translators: @PS_CIRCLE@ will be shown as an icon, don't change it
            "circle",   _("@PS_CIRCLE@ Button"),   Hs.PlayStationButton.CIRCLE,
            // Translators: @PS_X@ will be shown as an icon, don't change it
            "x",        _("@PS_X@ Button"),        Hs.PlayStationButton.CROSS,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "shoulder-buttons",
            "l1", _("L1 Button"), Hs.PlayStationButton.L1,
            "l2", _("L2 Button"), Hs.PlayStationButton.L2,
            "r1", _("R1 Button"), Hs.PlayStationButton.R1,
            "r2", _("R2 Button"), Hs.PlayStationButton.R2,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select Button"), Hs.PlayStationButton.SELECT,
            "start",  _("Start Button"),  Hs.PlayStationButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,         "triangle");
        kbd_mapping.map (Linux.Input.KEY_A,         "square");
        kbd_mapping.map (Linux.Input.KEY_S,         "x");
        kbd_mapping.map (Linux.Input.KEY_D,         "circle");
        kbd_mapping.map (Linux.Input.KEY_Q,         "l1");
        kbd_mapping.map (Linux.Input.KEY_E,         "r1");
        kbd_mapping.map (Linux.Input.KEY_Z,         "l2");
        kbd_mapping.map (Linux.Input.KEY_X,         "r2");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH,  "x");
        gamepad_mapping.map_button (EAST,   "circle");
        gamepad_mapping.map_button (WEST,   "square");
        gamepad_mapping.map_button (NORTH,  "triangle");
        gamepad_mapping.map_button (L,      "l1");
        gamepad_mapping.map_button (R,      "r1");
        gamepad_mapping.map_button (L2,     "l2");
        gamepad_mapping.map_button (R2,     "r2");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        controller = new PlatformController (
            "playstation-dualshock",
            _("DualShock"),
            "platform-playstation-dualshock-symbolic"
        );
        controller.add_sections (
            "left-stick", "face-buttons", "dpad", "right-stick",
            "shoulder-buttons", "stick-buttons", "menu", null
        );

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Directional Buttons"),
            Hs.PlayStationButton.UP,
            Hs.PlayStationButton.DOWN,
            Hs.PlayStationButton.LEFT,
            Hs.PlayStationButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            // Translators: @PS_TRIANGLE@ will be shown as an icon, don't change it
            "triangle", _("@PS_TRIANGLE@ Button"), Hs.PlayStationButton.TRIANGLE,
            // Translators: @PS_SQUARE@ will be shown as an icon, don't change it
            "square",   _("@PS_SQUARE@ Button"),   Hs.PlayStationButton.SQUARE,
            // Translators: @PS_CIRCLE@ will be shown as an icon, don't change it
            "circle",   _("@PS_CIRCLE@ Button"),   Hs.PlayStationButton.CIRCLE,
            // Translators: @PS_X@ will be shown as an icon, don't change it
            "x",        _("@PS_X@ Button"),        Hs.PlayStationButton.CROSS,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "shoulder-buttons",
            "l1", _("L1 Button"), Hs.PlayStationButton.L1,
            "l2", _("L2 Button"), Hs.PlayStationButton.L2,
            "r1", _("R1 Button"), Hs.PlayStationButton.R1,
            "r2", _("R2 Button"), Hs.PlayStationButton.R2,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select Button"), Hs.PlayStationButton.SELECT,
            "start",  _("Start Button"),  Hs.PlayStationButton.START,
            null
        );

        var left_stick = new StickControl ("left-stick", _("Left Stick"));
        left_stick.moved.connect (left_stick_moved_cb);
        controller.add_control ("left-stick", left_stick);
        var right_stick = new StickControl ("right-stick", _("Right Stick"));
        right_stick.moved.connect (right_stick_moved_cb);
        controller.add_control ("right-stick", right_stick);

        ControlHelpers.add_buttons (
            controller, set_pressed, "stick-buttons",
            "l3", _("L3 Button"), Hs.PlayStationButton.L3,
            "r3", _("R3 Button"), Hs.PlayStationButton.R3,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "left-stick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "left-stick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "left-stick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "left-stick:right");
        kbd_mapping.map (Linux.Input.KEY_T,         "right-stick:up");
        kbd_mapping.map (Linux.Input.KEY_F,         "right-stick:left");
        kbd_mapping.map (Linux.Input.KEY_G,         "right-stick:down");
        kbd_mapping.map (Linux.Input.KEY_H,         "right-stick:right");
        kbd_mapping.map (Linux.Input.KEY_I,         "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_J,         "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_K,         "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_L,         "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,         "triangle");
        kbd_mapping.map (Linux.Input.KEY_A,         "square");
        kbd_mapping.map (Linux.Input.KEY_S,         "x");
        kbd_mapping.map (Linux.Input.KEY_D,         "circle");
        kbd_mapping.map (Linux.Input.KEY_Q,         "l1");
        kbd_mapping.map (Linux.Input.KEY_E,         "r1");
        kbd_mapping.map (Linux.Input.KEY_Z,         "l2");
        kbd_mapping.map (Linux.Input.KEY_X,         "r2");
        kbd_mapping.map (Linux.Input.KEY_C,         "l3");
        kbd_mapping.map (Linux.Input.KEY_V,         "r3");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_stick  (LEFT,   "left-stick");
        gamepad_mapping.map_stick  (RIGHT,  "right-stick");
        gamepad_mapping.map_button (SOUTH,  "x");
        gamepad_mapping.map_button (EAST,   "circle");
        gamepad_mapping.map_button (WEST,   "square");
        gamepad_mapping.map_button (NORTH,  "triangle");
        gamepad_mapping.map_button (L,      "l1");
        gamepad_mapping.map_button (R,      "r1");
        gamepad_mapping.map_button (L2,     "l2");
        gamepad_mapping.map_button (R2,     "r2");
        gamepad_mapping.map_button (L3,     "l3");
        gamepad_mapping.map_button (R3,     "r3");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        default_controller = "playstation";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->psx.pad_buttons[player], button, pressed);
        });
    }

    private static void left_stick_moved_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            var stick_index = (player * Hs.PLAYSTATION_N_STICKS) + Hs.PlayStationStick.LEFT;
            state->psx.pad_sticks_x[stick_index] = x;
            state->psx.pad_sticks_y[stick_index] = y;
        });
    }

    private static void right_stick_moved_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            var stick_index = (player * Hs.PLAYSTATION_N_STICKS) + Hs.PlayStationStick.RIGHT;
            state->psx.pad_sticks_x[stick_index] = x;
            state->psx.pad_sticks_y[stick_index] = y;
        });
    }
}
