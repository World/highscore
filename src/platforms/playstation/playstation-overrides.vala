// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.PlayStation.Overrides {
    private struct DiscIdOverride {
        string checksum;
        string disc_id;
    }

    [Flags]
    public enum ControllerFlags {
        DIGITAL,
        ANALOG,
        DUALSHOCK,
        DEFAULT = DIGITAL | ANALOG | DUALSHOCK,
    }

    // Provide missing disc IDs for games using PSX.EXE
    private const DiscIdOverride[] DISC_ID_OVERRIDES = {
        { "366db16c997cc26e83a465d55eee7bd1", "SLPS-00001" }, // Ridge Racer (Japan)
        { "4013da4d717d4e45a892faa5efb2aebc", "SLPS-00134" }, // D no Shokutaku - Complete Graphics (Disc 2)
        { "4e9ab8c606910cf293c9240aebc07f4e", "SLPS-00061" }, // Ace Combat (Japan)
        { "6674c5259b301c14cf157028cba0c670", "SLPS-00057" }, // Cyberwar (Disc 3)
        { "7620e782155be39a27dca64f28bea831", "SLPS-00056" }, // Cyberwar (Disc 2)
        { "91bd86487f7b70741825b6305830cdcd", "SLPS-00133" }, // D no Shokutaku - Complete Graphics (Disc 1)
        { "a4b4141958826cfbbad282e695f46a98", "SLPS-91020" }, // Tekken (Playstation the Best) (Japan)
        { "c281d3cedabbbf0fe2f3c444f8b0f88a", "SLPS-00071" }, // 3x3 Eyes - Kyuusei Koushu (Disc 1)
        { "c2e60f02a1736b1b2713ffdf19066723", "SLPS-00072" }, // 3x3 Eyes - Kyuusei Koushu (Disc 2)
        { "cc6129d6a6fc9db8718343379b5c8dd1", "SLPS-00055" }, // Cyberwar (Disc 1)
        { "df18210b3f2ec7f3bfa21dbd180046c6", "SLPS-00135" }, // D no Shokutaku - Complete Graphics (Disc 3)
        { "e33e347c179ebb14f0e5bfb5d21e8e49", "SLPS-00040" }, // Tekken (Japan)
    };

    private const string[] SBI_GAMES = {
        "SCES-00311", // MediEvil (Europe)
        "SCES-01431", // Disney's Tarzan (Europe)
        "SCES-01444", // Jackie Chan Stuntmaster (Europe)
        "SCES-01492", // MediEvil (France)
        "SCES-01493", // MediEvil (Germany)
        "SCES-01494", // MediEvil (Italy)
        "SCES-01495", // MediEvil (Spain)
        "SCES-01516", // Disney Tarzan (France)
        "SCES-01517", // Disneys Tarzan (Germany)
        "SCES-01518", // Disneys Tarzan (Italy)
        "SCES-01519", // Disney Tarzan (Spain)
        "SCES-01564", // Ape Escape (Europe)
        "SCES-01695", // Disney's Story Studio - Mulan (Europe)
        "SCES-01700", // This Is Football (Europe)
        "SCES-01701", // Monde des Bleus, Le - Le jeu officiel de l'equipe de France (France)
        "SCES-01702", // Fussball Live (Germany)
        "SCES-01703", // This Is Football (Italy)
        "SCES-01704", // Esto es Futbol (Spain)
        "SCES-01763", // Speed Freaks (Europe)
        "SCES-01882", // This Is Football (Europe) (Fr,Nl)
        "SCES-01909", // Wip3out (Europe) (En,Fr,De,Es,It)
        "SCES-01979", // Formula One 99 (Europe) (En,Fr,De,It)
        "SCES-02004", // Disney Fais Ton Histoire! - Mulan (France)
        "SCES-02005", // Disneys Interaktive Abenteuer - Mulan (Germany)
        "SCES-02006", // Disney Libro Animato Creativo - Mulan (Italy)
        "SCES-02007", // Disney's Aventura Interactiva - Mulan (Spain)
        "SCES-02028", // Ape Escape (France)
        "SCES-02029", // Ape Escape (Germany)
        "SCES-02030", // Ape Escape (Italy)
        "SCES-02031", // Ape Escape - La Invasion de los Monos (Spain)
        "SCES-02104", // Spyro 2 - Gateway to Glimmer (Europe) (En,Fr,De,Es,It)
        "SCES-02105", // CTR - Crash Team Racing (Europe) (En,Fr,De,Es,It,Nl)
        "SCES-02182", // Disney's Tarzan (Sweden)
        "SCES-02185", // Disney's Tarzan (Netherlands)
        "SCES-02222", // Formula One 99 (Europe) (En,Es,Fi)
        "SCES-02264", // Disney's Verhalenstudio - Mulan (Netherlands)
        "SCES-02290", // Space Debris (Europe)
        "SCES-02365", // Barbie - Race & Ride (Europe)
        "SCES-02366", // Barbie - Aventure Equestre (France)
        "SCES-02367", // Barbie - Race & Ride (Germany)
        "SCES-02368", // Barbie - Race & Ride (Italy)
        "SCES-02369", // Barbie - Race & Ride (Spain)
        "SCES-02430", // Space Debris (France)
        "SCES-02431", // Space Debris (Germany)
        "SCES-02432", // Space Debris (Italy)
        "SCES-02487", // Barbie - Super Sports (Europe)
        "SCES-02488", // Barbie - Sports Extreme (France)
        "SCES-02489", // Barbie - Super Sport (Germany)
        "SCES-02490", // Barbie - Super Sports (Italy)
        "SCES-02491", // Barbie - Super Sports (Spain)
        "SCES-02544", // MediEvil 2 (Europe) (En,Fr,De)
        "SCES-02545", // MediEvil 2 (Europe) (Es,It,Pt)
        "SCES-02546", // MediEvil 2 (Russia)
        "SCES-02834", // Crash Bash (Europe) (En,Fr,De,Es,It)
        "SCES-02835", // Spyro - Year of the Dragon (Europe) (En,Fr,De,Es,It)
        "SLES-00017", // Prince Naseem Boxing (Europe) (En,Fr,De,Es,It)
        "SLES-00995", // Ronaldo V-Football (Europe) (En,Fr,Nl,Sv)
        "SLES-01041", // Hogs of War (Europe)
        "SLES-01226", // Actua Ice Hockey 2 (Europe)
        "SLES-01241", // Gekido - Urban Fighters (Europe) (En,Fr,De,Es,It)
        "SLES-01301", // Legacy of Kain - Soul Reaver (Europe)
        "SLES-01362", // Le Mans 24 Hours (Europe) (En,Fr,De,Es,It,Pt)
        "SLES-01545", // Michelin Rally Masters - Race of Champions (Europe) (En,De,Sv)
        "SLES-01715", // Eagle One - Harrier Attack (Europe) (En,Fr,De,Es,It)
        "SLES-01733", // UEFA Striker (Europe) (En,Fr,De,Es,It,Nl)
        "SLES-01906", // Mission - Impossible (Europe) (En,Fr,De,Es,It)
        "SLES-01907", // V-Rally - Championship Edition 2 (Europe) (En,Fr,De)
        "SLES-01943", // Radikal Bikers (Europe) (En,Fr,De,Es,It)
        "SLES-02024", // Legacy of Kain - Soul Reaver (France)
        "SLES-02025", // Legacy of Kain - Soul Reaver (Germany)
        "SLES-02026", // Legacy of Kain - Soul Reaver (Spain)
        "SLES-02027", // Legacy of Kain - Soul Reaver (Italy)
        "SLES-02061", // PGA European Tour Golf (Europe) (En,De)
        "SLES-02071", // Urban Chaos (Europe) (En,Es,It)
        "SLES-02080", // Final Fantasy VIII (Europe, Australia) (Disc 1)
        "SLES-02081", // Final Fantasy VIII (France) (Disc 1)
        "SLES-02082", // Final Fantasy VIII (Germany) (Disc 1)
        "SLES-02083", // Final Fantasy VIII (Italy) (Disc 1)
        "SLES-02084", // Final Fantasy VIII (Spain) (Disc 1)
        "SLES-02086", // N-Gen Racing (Europe) (En,Fr,De,Es,It)
        "SLES-02112", // SaGa Frontier 2 (Europe)
        "SLES-02113", // SaGa Frontier 2 (France)
        "SLES-02118", // SaGa Frontier 2 (Germany)
        "SLES-02207", // Dino Crisis (Europe)
        "SLES-02208", // Dino Crisis (France)
        "SLES-02209", // Dino Crisis (Germany)
        "SLES-02210", // Dino Crisis (Italy)
        "SLES-02211", // Dino Crisis (Spain)
        "SLES-02292", // Premier Manager 2000 (Europe)
        "SLES-02293", // Canal+ Premier Manager (Europe) (Fr,Es,It)
        "SLES-02328", // Galerians (Europe) (Disc 1)
        "SLES-02329", // Galerians (France) (Disc 1)
        "SLES-02330", // Galerians (Germany) (Disc 1)
        "SLES-02355", // Urban Chaos (Germany)
        "SLES-02395", // Michelin Rally Masters - Race of Champions (Europe) (Fr,Es,It)
        "SLES-02529", // Resident Evil 3 - Nemesis (Europe)
        "SLES-02530", // Resident Evil 3 - Nemesis (France)
        "SLES-02531", // Resident Evil 3 - Nemesis (Germany)
        "SLES-02532", // Resident Evil 3 - Nemesis (Spain)
        "SLES-02533", // Resident Evil 3 - Nemesis (Italy)
        "SLES-02538", // EA Sports Superbike 2000 (Europe) (En,Fr,De,Es,It,Sv)
        "SLES-02558", // Parasite Eve II (Europe) (Disc 1)
        "SLES-02559", // Parasite Eve II (France) (Disc 1)
        "SLES-02560", // Parasite Eve II (Germany) (Disc 1)
        "SLES-02561", // Parasite Eve II (Spain) (Disc 1)
        "SLES-02562", // Parasite Eve II (Italy) (Disc 1)
        "SLES-02563", // Anstoss - Premier Manager (Germany)
        "SLES-02572", // TOCA World Touring Cars (Europe) (En,Fr,De)
        "SLES-02573", // TOCA World Touring Cars (Europe) (Es,It)
        "SLES-02681", // Ronaldo V-Football (Europe) (De,Es,It,Pt)
        "SLES-02688", // Theme Park World (Europe) (En,Fr,De,Es,It,Nl,Sv)
        "SLES-02689", // Need for Speed - Porsche 2000 (Europe) (En,De,Sv)
        "SLES-02698", // Resident Evil 3 - Nemesis (Ireland)
        "SLES-02700", // Need for Speed - Porsche 2000 (Europe) (Fr,Es,It)
        "SLES-02704", // UEFA Euro 2000 (Europe)
        "SLES-02705", // UEFA Euro 2000 (France)
        "SLES-02706", // UEFA Euro 2000 (Germany)
        "SLES-02707", // UEFA Euro 2000 (Italy)
        "SLES-02722", // F1 2000 (Europe) (En,Fr,De,Nl)
        "SLES-02724", // F1 2000 (Italy)
        "SLES-02733", // Walt Disney World Quest - Magical Racing Tour (Europe) (En,Fr,De,Es,It,Nl,Sv,No,Da)
        "SLES-02754", // Vagrant Story (Europe)
        "SLES-02755", // Vagrant Story (France)
        "SLES-02756", // Vagrant Story (Germany)
        "SLES-02763", // SnoCross Championship Racing (Europe) (En,Fr,De,Es,It)
        "SLES-02766", // Les Cochons de Guerre (France)
        "SLES-02767", // Frontschweine (Germany)
        "SLES-02824", // RC Revenge (Europe) (En,Fr,De,Es)
        "SLES-02830", // MoHo (Europe) (En,Fr,De,Es,It)
        "SLES-02831", // TechnoMage - Die Rueckkehr der Ewigkeit (Germany)
        "SLES-02839", // Mike Tyson Boxing (Europe) (En,Fr,De,Es,It)
        "SLES-02857", // Sydney 2000 (Europe)
        "SLES-02858", // Sydney 2000 (France)
        "SLES-02859", // Sydney 2000 (Germany)
        "SLES-02861", // Sydney 2000 (Spain)
        "SLES-02965", // Final Fantasy IX (Europe) (Disc 1)
        "SLES-02966", // Final Fantasy IX (France) (Disc 1)
        "SLES-02967", // Final Fantasy IX (Germany) (Disc 1)
        "SLES-02968", // Final Fantasy IX (Italy) (Disc 1)
        "SLES-02969", // Final Fantasy IX (Spain) (Disc 1)
        "SLES-02975", // LMA Manager 2001 (Europe)
        "SLES-02977", // BDFL Manager 2001 (Germany)
        "SLES-02978", // Football Manager Campionato 2001 (Italy)
        "SLES-02992", // Premier Manager 2000 (Europe)
        "SLES-03061", // F.A. Premier League Football Manager 2001, The (Europe)
        "SLES-03062", // Fussball Manager 2001 (Germany)
        "SLES-03189", // Disney's 102 Dalmatians - Puppies to the Rescue (Europe)
        "SLES-03191", // Disney's 102 Dalmatians - Puppies to the Rescue (Europe) (Fr,De,Es,It,Nl)
        "SLES-03241", // TechnoMage - Return of Eternity (Europe)
        "SLES-03242", // TechnoMage - En Quete de L'Eternite (France)
        "SLES-03245", // TechnoMage - De Terugkeer der Eeuwigheid (Netherlands)
        "SLES-03324", // Asterix - Mega Madness (Europe) (En,Fr,De,Es,It,Nl)
        "SLES-03489", // Italian Job, The (Europe)
        "SLES-03519", // Men in Black - The Series - Crashdown (Europe)
        "SLES-03520", // Men in Black - The Series - Crashdown (France)
        "SLES-03521", // Men in Black - The Series - Crashdown (Germany)
        "SLES-03522", // Men in Black - The Series - Crashdown (Italy)
        "SLES-03523", // Men in Black - The Series - Crashdown (Spain)
        "SLES-03530", // Lucky Luke - Western Fever (Europe) (En,Fr,De,Es,It,Nl)
        "SLES-03603", // LMA Manager 2002 (Europe)
        "SLES-03605", // BDFL Manager 2002 (Germany)
        "SLES-03626", // Italian Job, The (Germany)
        "SLES-12080", // Final Fantasy VIII (Europe, Australia) (Disc 2)
        "SLES-12081", // Final Fantasy VIII (France) (Disc 2)
        "SLES-12082", // Final Fantasy VIII (Germany) (Disc 2)
        "SLES-12083", // Final Fantasy VIII (Italy) (Disc 2)
        "SLES-12084", // Final Fantasy VIII (Spain) (Disc 2)
        "SLES-12328", // Galerians (Europe) (Disc 2)
        "SLES-12329", // Galerians (France) (Disc 2)
        "SLES-12330", // Galerians (Germany) (Disc 2)
        "SLES-12558", // Parasite Eve II (Europe) (Disc 2)
        "SLES-12559", // Parasite Eve II (France) (Disc 2)
        "SLES-12560", // Parasite Eve II (Germany) (Disc 2)
        "SLES-12561", // Parasite Eve II (Spain) (Disc 2)
        "SLES-12562", // Parasite Eve II (Italy) (Disc 2)
        "SLES-12965", // Final Fantasy IX (Europe) (Disc 2)
        "SLES-12966", // Final Fantasy IX (France) (Disc 2)
        "SLES-12967", // Final Fantasy IX (Germany) (Disc 2)
        "SLES-12968", // Final Fantasy IX (Italy) (Disc 2)
        "SLES-12969", // Final Fantasy IX (Spain) (Disc 2)
        "SLES-22080", // Final Fantasy VIII (Europe, Australia) (Disc 3)
        "SLES-22081", // Final Fantasy VIII (France) (Disc 3)
        "SLES-22082", // Final Fantasy VIII (Germany) (Disc 3)
        "SLES-22083", // Final Fantasy VIII (Italy) (Disc 3)
        "SLES-22084", // Final Fantasy VIII (Spain) (Disc 3)
        "SLES-22328", // Galerians (Europe) (Disc 3)
        "SLES-22329", // Galerians (France) (Disc 3)
        "SLES-22330", // Galerians (Germany) (Disc 3)
        "SLES-22965", // Final Fantasy IX (Europe) (Disc 3)
        "SLES-22966", // Final Fantasy IX (France) (Disc 3)
        "SLES-22967", // Final Fantasy IX (Germany) (Disc 3)
        "SLES-22968", // Final Fantasy IX (Italy) (Disc 3)
        "SLES-22969", // Final Fantasy IX (Spain) (Disc 3)
        "SLES-32080", // Final Fantasy VIII (Europe, Australia) (Disc 4)
        "SLES-32081", // Final Fantasy VIII (France) (Disc 4)
        "SLES-32082", // Final Fantasy VIII (Germany) (Disc 4)
        "SLES-32083", // Final Fantasy VIII (Italy) (Disc 4)
        "SLES-32084", // Final Fantasy VIII (Spain) (Disc 4)
        "SLES-32965", // Final Fantasy IX (Europe) (Disc 4)
        "SLES-32966", // Final Fantasy IX (France) (Disc 4)
        "SLES-32967", // Final Fantasy IX (Germany) (Disc 4)
        "SLES-32968", // Final Fantasy IX (Italy) (Disc 4)
        "SLES-32969", // Final Fantasy IX (Spain) (Disc 4)
    };

    private inline DiscIdOverride? find_disc_id_override (string checksum) {
        void *ret = Posix.bsearch (
            checksum,
            DISC_ID_OVERRIDES,
            DISC_ID_OVERRIDES.length,
            sizeof (DiscIdOverride),
            (a, b) => {
                var needle = (string) a;
                var override = (DiscIdOverride *) b;
                return strcmp (needle, override.checksum);
            }
        );

        if (ret == null)
            return null;

        return * ((DiscIdOverride *) ret);
    }

    public string? get_disc_id (string checksum) {
        var @override = find_disc_id_override (checksum);
        if (@override != null)
            return @override.disc_id;

        return null;
    }

    public bool get_needs_sbi (string disc_id) {
        void *ret = Posix.bsearch (
            disc_id,
            SBI_GAMES,
            SBI_GAMES.length,
            sizeof (string),
            (a, b) => {
                var needle = (string) a;
                var hay_pointer = (string **) b;

                return strcmp (needle, *hay_pointer);
            }
        );

        return ret != null;
    }

     private inline GameInputEntry? get_game_entry (string disc_id) {
        void *ret = Posix.bsearch (
            disc_id,
            GAME_INPUT_DATA,
            GAME_INPUT_DATA.length,
            sizeof (GameInputEntry),
            (a, b) => {
                var needle = (string) a;
                var entry = (GameInputEntry *) b;

                return strcmp (needle, entry.disc_id);
            }
        );

        if (ret == null)
            return null;

        return * ((GameInputEntry *) ret);
    }

    public ControllerFlags get_controllers (string disc_id) {
        var entry = get_game_entry (disc_id);

        if (entry == null)
            return DEFAULT;

        return entry.controllers;
    }
}
