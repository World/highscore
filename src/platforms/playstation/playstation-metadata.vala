// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlayStation.Metadata : Object, GameMetadata {
    public string disc_id { get; set; }

    public Metadata (string disc_id) {
        Object (disc_id: disc_id);
    }

    protected VariantType serialize_type () {
        return VariantType.STRING;
    }

    protected Variant serialize () {
        return disc_id;
    }

    protected void deserialize (Variant variant) {
        disc_id = variant.get_string ();
    }
}
