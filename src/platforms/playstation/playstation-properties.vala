// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/playstation/playstation-properties.ui")]
public class Highscore.PlayStation.Properties : PlatformProperties {
    public bool dualshock_analog { get; set; }

    private Settings settings;

    construct {
        settings = new GameSettings (game).get_platform ();
        settings.bind ("dualshock-analog", this, "dualshock-analog", DEFAULT);

        var metadata = game.metadata as Metadata;
        var controller_flags = Overrides.get_controllers (metadata.disc_id);
        bool is_digital = DIGITAL in controller_flags;
        bool is_analog = ANALOG in controller_flags;
        bool is_dualshock = DUALSHOCK in controller_flags;

        if (settings.get_user_value ("dualshock-analog") == null)
            settings.set_boolean ("dualshock-analog", ANALOG in controller_flags);

        if (is_digital && (is_dualshock || is_analog))
            visible = true;
    }
}
