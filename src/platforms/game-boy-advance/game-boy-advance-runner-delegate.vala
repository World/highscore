// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameBoyAdvance.RunnerDelegate : Highscore.RunnerDelegate {
    construct {
        runner.set_rumble_enabled (0, true);
    }
}
