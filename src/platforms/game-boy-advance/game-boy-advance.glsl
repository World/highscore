// Shader that replicates the LCD dynamics from a Game Boy Advance
// Author: hunterk, Pokefan531
// Source: https://github.com/libretro/slang-shaders/blob/master/handheld/shaders/color/sp101-color.slang (backlit colors)
// Source: https://github.com/libretro/slang-shaders/blob/master/handheld/shaders/color/gba-color.slang (non-backlit colors)
// License: Public domain

#ifdef VERTEX

void hs_main() {}

#else // FRAGMENT

uniform bool u_backlight;

#define WHITE_BALANCE

#define TARGET_GAMMA 2.2
#define DISPLAY_GAMMA 2.2

vec4 gba_correct_color(vec4 rgba) {
  float darken_intensity;
  mat4 profile;

  if (u_backlight) {
    darken_intensity = 0.0;
    profile = mat4(
#ifdef WHITE_BALANCE
      0.955, 0.0375,  0.0025, 0.0, //red channel
      0.11,  0.885,  -0.03,   0.0, //green channel
     -0.065, 0.0775,  1.0275, 0.0, //blue channel
      0.0,   0.0,     0.0,    0.94 //alpha channel
#else
      0.86, 0.03,   0.0025, 0.0, //red channel
      0.10, 0.745, -0.03,   0.0, //green channel
     -0.06, 0.0675, 1.0275, 0.0, //blue channel
      0.0,  0.0,    0.0,    0.97 //alpha channel
#endif
    );
  } else {
    darken_intensity = 0.5;
    profile = mat4(
      0.80,  0.135, 0.195, 0.0, //red channel
      0.275, 0.64,  0.155, 0.0, //green channel
     -0.075, 0.225, 0.65,  0.0, //blue channel
      0.0,   0.0,   0.0,   0.93 //alpha
    );
  }

  // bring out our stored luminance value
  float lum = profile[3].w;

  vec4 screen = pow(rgba, vec4(TARGET_GAMMA + darken_intensity)).rgba;

  screen = clamp(screen * lum, 0.0, 1.0);
  screen = profile * screen;

  return pow(screen, vec4(1.0 / DISPLAY_GAMMA));
}

vec4 hs_main() {
  return gba_correct_color(texture(u_source, v_texCoord));
}

#endif