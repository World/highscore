// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/game-boy-advance/game-boy-advance-properties.ui")]
public class Highscore.GameBoyAdvance.Properties : PlatformProperties {
    private Settings settings;

    public bool backlight { get; set; }
    public bool darken_colors { get; set; }

    construct {
        settings = new GameSettings (game).get_platform ();

        settings.bind ("backlight", this, "backlight", DEFAULT);
    }
}
