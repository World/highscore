// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameBoyAdvance.ScreenSet : Highscore.ScreenSet {
    public bool backlight { get; set; }

    private Settings settings;

    construct {
        settings = new GameSettings (game).get_platform ();

        notify["backlight"].connect (() => {
            filter_changed ();
            screen_type_changed ();
        });

        settings.bind ("backlight", this, "backlight", GET);
    }

    public override string get_filter_path () {
        return "/app/drey/Highscore/platforms/game-boy-advance/game-boy-advance.glsl";
    }

    public override void setup_filter (GLShader shader, int screen_id) {
        shader.set_uniform_bool ("u_backlight", backlight);
    }

    public override ScreenType get_screen_type (Hs.Region region) {
        if (backlight)
            return GAME_BOY_ADVANCE_BACKLIT;

        return GAME_BOY_ADVANCE;
    }
}
