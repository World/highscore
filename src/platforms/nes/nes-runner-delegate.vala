// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Nes.RunnerDelegate : Highscore.RunnerDelegate {
    private uint n_players;

    public bool has_mic { get; private set; }
    public Hs.NesMicLevel mic_level { get; set; default = QUIET; }

    construct {
        notify["mic-level"].connect (() => {
            runner.input_buffer.modify_input_state (state => {
                state->nes.mic_level = mic_level;
            });
        });
    }

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        n_players = yield proxy.get_players ();

        var accessory = yield proxy.get_accessory ();
        switch (accessory) {
            case ZAPPER:
                runner.set_controller_type (0, "nes-zapper");
                break;
            case PADDLE:
                runner.set_controller_type (0, "nes-paddle");
                break;
            default:
            case NONE:
                break;
        }

        has_mic = yield proxy.get_has_mic ();

        if (has_mic) {
            for (int i = 0; i < n_players; i++) {
                if (runner.get_controller_type (i) == "nes")
                    runner.set_controller_type (i, "nes-mic");
            }
        }
    }

    public override async void after_reset (bool hard) throws Error {
        mic_level = QUIET;
    }

    public override async void load_state (SnapshotPlatformMetadata? metadata) throws Error {
        mic_level = QUIET;
    }

    public override uint get_n_players () {
        return n_players;
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        var mic_button = new Gtk.ToggleButton () {
            icon_name = "audio-input-microphone-symbolic",
        };

        bind_property ("has-mic", mic_button, "visible", SYNC_CREATE);
        bind_property (
            "mic-level", mic_button, "active",
            SYNC_CREATE | BIDIRECTIONAL,
            (binding, from_value, ref to_value) => {
                to_value = from_value == Hs.NesMicLevel.LOUD;
                return true;
            },
            (binding, from_value, ref to_value) => {
                if (from_value == true)
                    to_value = Hs.NesMicLevel.LOUD;
                else
                    to_value = Hs.NesMicLevel.QUIET;
                return true;
            }
        );

        return mic_button;
    }
}
