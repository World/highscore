// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Nes.Controls : PlatformControls {
    private const double PADDLE_SPEED = 4;

    construct {
        n_players = Hs.NES_MAX_PLAYERS;

        var controller = new PlatformController ("nes", _("NES Controller"));
        controller.add_sections ("dpad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Control Pad"),
            Hs.NesButton.UP,
            Hs.NesButton.DOWN,
            Hs.NesButton.LEFT,
            Hs.NesButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.NesButton.A,
            "b", _("B Button"), Hs.NesButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.NesButton.SELECT,
            "start",  _("Start"),  Hs.NesButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "a");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        controller = new PlatformController ("nes-mic", _("NES Controller"));
        controller.add_sections ("dpad", "face-buttons", "menu", "mic", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Control Pad"),
            Hs.NesButton.UP,
            Hs.NesButton.DOWN,
            Hs.NesButton.LEFT,
            Hs.NesButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.NesButton.A,
            "b", _("B Button"), Hs.NesButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.NesButton.SELECT,
            "start",  _("Start"),  Hs.NesButton.START,
            null
        );

        var toggle_mic_button = new ButtonControl ("toggle-mic", _("Toggle Mic"));
        toggle_mic_button.activate.connect ((runner, player, pressed) => {
            set_toggle_mic_button_pressed (runner, pressed);
        });
        controller.add_control ("mic", toggle_mic_button);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "a");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        kbd_mapping.map (Linux.Input.KEY_M, "toggle-mic");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        gamepad_mapping.map_button (L, "toggle-mic");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // Zapper

        controller = new PlatformController (
            "nes-zapper",
            _("NES Zapper"),
            "platform-nes-zapper-symbolic"
        );
        controller.add_sections ("dpad", "face-buttons", "menu", "zapper", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Control Pad"),
            Hs.NesButton.UP,
            Hs.NesButton.DOWN,
            Hs.NesButton.LEFT,
            Hs.NesButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.NesButton.A,
            "b", _("B Button"), Hs.NesButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.NesButton.SELECT,
            "start",  _("Start"),  Hs.NesButton.START,
            null
        );

        var zapper = new PointerControl ("zapper", _("Zapper"));
        zapper.pressed.connect (zapper_fire_cb);
        zapper.moved.connect (zapper_fire_cb);
        zapper.released.connect (zapper_stop_cb);
        controller.add_control ("zapper", zapper);

        controller.set_for_screen (Highscore.ScreenSet.DEFAULT_SCREEN, zapper);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "a");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        controller.touch_overlay_name = "nes";

        add_controller (controller);

        // Arkanoid paddle controller

        controller = new PlatformController (
            "nes-paddle",
            _("Arkanoid Controller"),
            "platform-nes-arkanoid-symbolic"
        );
        controller.add_sections ("paddle", "buttons", "menu", null);

        var paddle = new RotaryControl ("paddle", _("Paddle"));
        paddle.rotated.connect (paddle_rotated_cb);
        controller.add_control ("paddle", paddle);

        var button = new ButtonControl ("button", _("Button"));
        button.activate.connect (paddle_button_cb);
        controller.add_control ("buttons", button);

        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.NesButton.SELECT,
            "start",  _("Start"),  Hs.NesButton.START,
            null
        );

        add_controller (controller);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "paddle:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "paddle:right");
        kbd_mapping.map (Linux.Input.KEY_S,         "button");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_stick (LEFT,    "paddle");
        gamepad_mapping.map_button (LEFT,   "paddle:left");
        gamepad_mapping.map_button (RIGHT,  "paddle:right");
        gamepad_mapping.map_button (SOUTH,  "button");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        controller.default_gamepad_mapping = gamepad_mapping;

        default_controller = "nes";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->nes.pad_buttons[player], button, pressed);
        });
    }

    private static void set_toggle_mic_button_pressed (Runner runner, bool pressed) {
        var delegate = runner.delegate as RunnerDelegate;

        if (pressed)
            delegate.mic_level = LOUD;
        else
            delegate.mic_level = QUIET;
    }

    private static void zapper_fire_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->nes.zapper_fire = true;
            state->nes.zapper_x = x;
            state->nes.zapper_y = y;
        });
    }

    private static void zapper_stop_cb (Runner runner, uint player) {
        runner.input_buffer.modify_input_state (state => {
            state->nes.zapper_fire = false;
        });
    }

    private static void paddle_rotated_cb (Runner runner, uint player, double delta) {
        runner.input_buffer.modify_input_state (state => {
            state->nes.paddle_speed = delta * PADDLE_SPEED;
            state->nes.paddle_position = double.NAN;
        });
    }

    private static void paddle_button_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->nes.paddle_button = pressed;
        });
    }
}
