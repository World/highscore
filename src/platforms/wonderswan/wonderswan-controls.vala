// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.WonderSwan.Controls : PlatformControls {
    construct {
        var controller = new PlatformController (
            "wonderswan-landscape", _("WonderSwan (Landscape)")
        );
        controller.add_sections ("x", "y", "ab", "menu", null);

        ControlHelpers.add_buttons (
            controller, set_pressed, "x",
            "x1", _("X1 Button"), Hs.WonderSwanButton.X1,
            "x2", _("X2 Button"), Hs.WonderSwanButton.X2,
            "x3", _("X3 Button"), Hs.WonderSwanButton.X3,
            "x4", _("X4 Button"), Hs.WonderSwanButton.X4,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "y",
            "y1", _("Y1 Button"), Hs.WonderSwanButton.Y1,
            "y2", _("Y2 Button"), Hs.WonderSwanButton.Y2,
            "y3", _("Y3 Button"), Hs.WonderSwanButton.Y3,
            "y4", _("Y4 Button"), Hs.WonderSwanButton.Y4,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "ab",
            "a", _("A Button"), Hs.WonderSwanButton.A,
            "b", _("B Button"), Hs.WonderSwanButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "start", _("Start Button"), Hs.WonderSwanButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "x1");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "x2");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "x3");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "x4");
        kbd_mapping.map (Linux.Input.KEY_W,         "y1");
        kbd_mapping.map (Linux.Input.KEY_D,         "y2");
        kbd_mapping.map (Linux.Input.KEY_S,         "y3");
        kbd_mapping.map (Linux.Input.KEY_A,         "y4");
        kbd_mapping.map (Linux.Input.KEY_Z,         "b");
        kbd_mapping.map (Linux.Input.KEY_X,         "a");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,     "x1");
        gamepad_mapping.map_button (RIGHT,  "x2");
        gamepad_mapping.map_button (DOWN,   "x3");
        gamepad_mapping.map_button (LEFT,   "x4");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick_to_buttons (LEFT, "y1", "y3", "y4", "y2");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // portrait

        controller = new PlatformController (
            "wonderswan-portrait",
            _("WonderSwan (Portrait)"),
            "platform-wonderswan-portrait-symbolic"
        );
        controller.add_sections ("x", "y", "ab", "menu", null);

        ControlHelpers.add_buttons (
            controller, set_pressed, "x",
            "x1", _("X1 Button"), Hs.WonderSwanButton.X1,
            "x2", _("X2 Button"), Hs.WonderSwanButton.X2,
            "x3", _("X3 Button"), Hs.WonderSwanButton.X3,
            "x4", _("X4 Button"), Hs.WonderSwanButton.X4,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "y",
            "y1", _("Y1 Button"), Hs.WonderSwanButton.Y1,
            "y2", _("Y2 Button"), Hs.WonderSwanButton.Y2,
            "y3", _("Y3 Button"), Hs.WonderSwanButton.Y3,
            "y4", _("Y4 Button"), Hs.WonderSwanButton.Y4,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "ab",
            "a", _("A Button"), Hs.WonderSwanButton.A,
            "b", _("B Button"), Hs.WonderSwanButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "start", _("Start Button"), Hs.WonderSwanButton.START,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "y1");
        kbd_mapping.map (Linux.Input.KEY_UP,        "y2");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "y3");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "y4");
        kbd_mapping.map (Linux.Input.KEY_A,         "x1");
        kbd_mapping.map (Linux.Input.KEY_W,         "x2");
        kbd_mapping.map (Linux.Input.KEY_D,         "x3");
        kbd_mapping.map (Linux.Input.KEY_S,         "x4");
        kbd_mapping.map (Linux.Input.KEY_Z,         "b");
        kbd_mapping.map (Linux.Input.KEY_X,         "a");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (LEFT,   "y1");
        gamepad_mapping.map_button (UP,     "y2");
        gamepad_mapping.map_button (RIGHT,  "y3");
        gamepad_mapping.map_button (DOWN,   "y4");
        gamepad_mapping.map_button (WEST,   "x1");
        gamepad_mapping.map_button (NORTH,  "x2");
        gamepad_mapping.map_button (EAST,   "x3");
        gamepad_mapping.map_button (SOUTH,  "x4");
        gamepad_mapping.map_button (R,      "b");
        gamepad_mapping.map_button (R2,     "a");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick_to_buttons (LEFT, "y2", "y4", "y1", "y3");
        gamepad_mapping.map_stick_to_buttons (RIGHT, "x2", "x4", "x1", "x3");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        default_controller = "wonderswan-landscape";

        // global

        global_controller = new PlatformController ("shortcuts", _("Shortcuts"));
        global_controller.add_section ("shortcuts");

        var shortcut = new ButtonControl ("rotate", _("Rotate Screen"));
        shortcut.activate.connect (rotate_cb);
        global_controller.add_control ("shortcuts", shortcut);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_R, "rotate");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (SELECT, "rotate");
        global_controller.default_gamepad_mapping = gamepad_mapping;
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->wonderswan.buttons, button, pressed);
        });
    }

    private static void rotate_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;
        delegate.rotated = !delegate.rotated;
    }
}
