// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.WonderSwan.Metadata : Object, GameMetadata {
    public bool vertical { get; set; }
    public bool color { get; set; }

    public Metadata (bool vertical, bool color) {
        Object (vertical: vertical, color: color);
    }

    protected VariantType serialize_type () {
        return new VariantType.tuple ({
            VariantType.BOOLEAN,
            VariantType.BOOLEAN
        });
    }

    protected Variant serialize () {
        return new Variant.tuple ({ vertical, color });
    }

    protected void deserialize (Variant variant) {
        vertical = variant.get_child_value (0).get_boolean ();
        color = variant.get_child_value (1).get_boolean ();
    }
}
