#ifdef VERTEX

void hs_main() {}

#else // FRAGMENT

uniform bool u_monochromeColors;

#define MinInputLuminance 0.0
#define MaxInputLuminance 1.0
#define Gamma 0.8
#define OutputGamma 1.0
#define BackgroundColor vec3(0.43529411764705883, 0.5333333333333333, 0.41568627450980394)
#define ForegroundColor vec3(0.13333333333333333, 0.13725490196078433, 0.10588235294117647)

vec4 apply_monochrome_colors(vec3 color) {
  float luminance = 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;

  luminance = clamp(luminance, MinInputLuminance, MaxInputLuminance);
  luminance = (luminance - MinInputLuminance) / (MaxInputLuminance - MinInputLuminance);

  float opacity = 1.0 - luminance;
  opacity = pow(opacity, OutputGamma / Gamma);

  color = mix(BackgroundColor, ForegroundColor, opacity);

  return vec4(color, 1.0);
}

vec4 hs_main() {
  vec4 color = texture(u_source, v_texCoord);

  if (u_monochromeColors)
    return apply_monochrome_colors(color.rgb);

  return color;
}

#endif