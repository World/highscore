// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.WonderSwan.RunnerDelegate : Highscore.RunnerDelegate {
    public bool rotated { get; set; }

    private Settings settings;

    construct {
        var metadata = runner.game.metadata as Metadata;

        settings = new GameSettings (runner.game).get_platform ();

        if (settings.get_user_value ("rotated") == null)
            settings.set_boolean ("rotated", metadata.vertical);

        notify["rotated"].connect (update_rotation);

        settings.bind ("rotated", this, "rotated", DEFAULT);
    }

    public override async void after_load () throws Error {
        update_rotation ();
    }

    private void update_rotation () {
        runner.set_controller_type (
            0, rotated ? "wonderswan-portrait" : "wonderswan-landscape"
        );
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        var button = new OrientationButton ();

        bind_property ("rotated", button, "rotated", SYNC_CREATE | BIDIRECTIONAL);

        return button;
    }
}
