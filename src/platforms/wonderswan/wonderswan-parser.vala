// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://ws.nesdev.org/wiki/ROM_header
public class Highscore.WonderSwan.Parser : GameParser {
    private const size_t HEADER_LENGTH = 0x10;
    // Not really a magic number, but every WS ROM has this, might as well
    private const uint8 MAGIC_NUMBER = 0xEA;
    private const size_t FLAGS_OFFSET = 0xC;

    private bool color;
    private bool vertical;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    public override bool parse () throws Error {
        var stream = new DataInputStream (file.read ());

        var info = file.query_info (
            FileAttribute.STANDARD_SIZE + "," +
            FileAttribute.STANDARD_CONTENT_TYPE,
            NONE
        );
        int64 size = info.get_size ();

        if (size < HEADER_LENGTH) {
            parser_debug ("File is too short, discarding");
            return false;
        }

        var content_type = info.get_content_type ();
        var mime_type = ContentType.get_mime_type (content_type);

        stream.seek (size - HEADER_LENGTH, SET);

        uint8 magic = stream.read_byte ();
        if (magic != MAGIC_NUMBER) {
            parser_debug ("Magic file mismatch, discarding");
            return false;
        }

        stream.seek (FLAGS_OFFSET - 1, CUR);

        uint8 flags = stream.read_byte ();

        vertical = (flags & 1) > 0;
        color = mime_type == "application/x-wonderswan-color-rom";

        parser_debug (
            "Found metadata: %s, %s",
            vertical ? "portrait" : "landscape",
            color ? "color" : "monochrome"
        );

        return true;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (vertical, color);
    }
}
