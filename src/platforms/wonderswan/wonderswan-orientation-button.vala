// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.WonderSwan.OrientationButton : Adw.Bin {
    public bool rotated { get; set; }

    private Gtk.Button button;

    construct {
        button = new Gtk.Button () {
            icon_name = "wonderswan-portrait-symbolic",
        };

        button.add_css_class ("orientation");

        button.clicked.connect (() => rotated = !rotated);

        child = button;

        update_button ();

        notify["rotated"].connect (update_button);
    }

    private void update_button () {
        if (rotated) {
            button.add_css_class ("rotated");
            button.tooltip_text = _("Rotate to Landscape");
        } else {
            button.remove_css_class ("rotated");
            button.tooltip_text = _("Rotate to Portrait");
        }
    }
}
