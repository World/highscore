// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.WonderSwan.ScreenSet : Highscore.ScreenSet {
    public bool rotated { get; set; }
    public bool is_accurate { get; set; }

    private GameSettings game_settings;
    private Settings platform_settings;

    construct {
        notify["rotated"].connect (() => {
            if (rotated)
                layout = new SingleLayout (DEFAULT_SCREEN, 270_DEG);
            else
                layout = new SingleLayout (DEFAULT_SCREEN);

            layout_changed (true);
        });

        game_settings = new GameSettings (game);
        game_settings.bind_property ("is-accurate-filter", this, "is-accurate", SYNC_CREATE);

        platform_settings = game_settings.get_platform ();
        platform_settings.bind ("rotated", this, "rotated", GET);

        notify["is-accurate"].connect (() => filter_changed ());
    }

    public override string get_filter_path () {
        return "/app/drey/Highscore/platforms/wonderswan/wonderswan.glsl";
    }

    public override void setup_filter (GLShader shader, int screen_id) {
        var metadata = game.metadata as Metadata;

        shader.set_uniform_bool (
            "u_monochromeColors",
            !metadata.color && mode != DISPLAY && is_accurate
        );
    }

    public override ScreenType get_screen_type (Hs.Region region) {
        var metadata = game.metadata as Metadata;

        if (metadata.color)
            return LCD_RGB;

        return WONDERSWAN;
    }
}
