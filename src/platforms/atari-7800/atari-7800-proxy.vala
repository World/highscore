// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.Atari7800.Proxy : Object {
    public abstract async Hs.Atari7800Controller get_controller (uint player) throws Error;
    public abstract async Hs.Atari7800Difficulty get_default_difficulty (uint player) throws Error;
    public abstract async void press_pause () throws Error;
    public abstract async void press_select () throws Error;
}
