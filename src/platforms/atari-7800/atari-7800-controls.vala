// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Atari7800.Controls : PlatformControls {
    construct {
        n_players = Hs.ATARI_7800_MAX_PLAYERS;

        var controller = new PlatformController ("atari-7800", _("Pro-Line Joystick"));
        controller.add_sections ("joystick", "buttons", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joystick", _("Joystick"),
            Hs.Atari7800JoystickButton.UP,
            Hs.Atari7800JoystickButton.DOWN,
            Hs.Atari7800JoystickButton.LEFT,
            Hs.Atari7800JoystickButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "buttons",
            "left",  _("Left Button"),  Hs.Atari7800JoystickButton.ONE,
            "right", _("Right Button"), Hs.Atari7800JoystickButton.TWO,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joystick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joystick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joystick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joystick:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "left");
        kbd_mapping.map (Linux.Input.KEY_D,     "right");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("joystick");
        gamepad_mapping.map_button (SOUTH, "left");
        gamepad_mapping.map_button (EAST,  "right");
        gamepad_mapping.map_stick  (LEFT,  "joystick");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // lightgun

        controller = new PlatformController (
            "atari-7800-lightgun",
            _("Light Gun"),
            "platform-atari-7800-lightgun-symbolic"
        );
        controller.add_section ("lightgun");

        var lightgun = new PointerControl ("lightgun", _("Light Gun"));
        lightgun.pressed.connect (lightgun_fire_cb);
        lightgun.moved.connect (lightgun_fire_cb);
        lightgun.released.connect (lightgun_stop_cb);
        controller.add_control ("lightgun", lightgun);

        controller.set_for_screen (Highscore.ScreenSet.DEFAULT_SCREEN, lightgun);

        add_controller (controller);

        // global

        global_controller = new PlatformController ("console", _("Built-in"));
        global_controller.add_section ("system");

        var button = new ButtonControl ("left-difficulty", _("Left Difficulty"));
        button.activate.connect (left_difficulty_cb);
        global_controller.add_control ("system", button);

        button = new ButtonControl ("right-difficulty", _("Right Difficulty"));
        button.activate.connect (right_difficulty_cb);
        global_controller.add_control ("system", button);

        button = new ButtonControl ("pause", _("Pause Button"));
        button.activate.connect (pause_cb);
        global_controller.add_control ("system", button);

        button = new ButtonControl ("select", _("Select Button"));
        button.activate.connect (select_cb);
        global_controller.add_control ("system", button);

        button = new ButtonControl ("reset", _("Reset Button"));
        button.activate.connect (reset_cb);
        global_controller.add_control ("system", button);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_1,         "left-difficulty");
        kbd_mapping.map (Linux.Input.KEY_2,         "right-difficulty");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "pause");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_W,         "reset");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (L,      "left-difficulty");
        gamepad_mapping.map_button (R,      "right-difficulty");
        gamepad_mapping.map_button (START,  "pause");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (NORTH,  "reset");
        global_controller.default_gamepad_mapping = gamepad_mapping;

        default_controller = "atari-7800";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->atari_7800.joystick[player], button, pressed);
        });
    }

    private static void lightgun_fire_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_7800.lightgun_fire = true;
            state->atari_7800.lightgun_x = x;
            state->atari_7800.lightgun_y = y;
        });
    }

    private static void lightgun_stop_cb (Runner runner, uint player) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_7800.lightgun_fire = false;
        });
    }

    private static void left_difficulty_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.left_difficulty == ADVANCED)
            delegate.left_difficulty = BEGINNER;
        else
            delegate.left_difficulty = ADVANCED;
    }

    private static void right_difficulty_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.right_difficulty == ADVANCED)
            delegate.right_difficulty = BEGINNER;
        else
            delegate.right_difficulty = ADVANCED;
    }

    private static void pause_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_7800.pause_button = pressed;
        });
    }

    private static void select_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_7800.select_button = pressed;
        });
    }

    private static void reset_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            runner.reset.begin (false);
    }
}
