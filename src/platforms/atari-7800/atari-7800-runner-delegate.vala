// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Atari7800.RunnerDelegate : Highscore.RunnerDelegate {
    private Hs.Atari7800Difficulty difficulty[2];
    private uint n_players;

    public Hs.Atari7800Difficulty left_difficulty {
        get { return difficulty[0]; }
        set {
            if (left_difficulty == value)
                return;

            difficulty[0] = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_7800.difficulty[0] = left_difficulty;
            });
        }
    }

    public Hs.Atari7800Difficulty right_difficulty {
        get { return difficulty[1]; }
        set {
            if (right_difficulty == value)
                return;

            difficulty[1] = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_7800.difficulty[1] = right_difficulty;
            });
        }
    }

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;
        Hs.Atari7800Controller controllers[2];

        for (int i = 0; i < 2; i++) {
            controllers[i] = yield proxy.get_controller (i);

            if (controllers[i] == LIGHTGUN)
                runner.set_controller_type (i, "atari-7800-lightgun");
        }

        n_players = (controllers[1] == NONE) ? 1 : 2;

        left_difficulty = yield proxy.get_default_difficulty (0);
        right_difficulty = yield proxy.get_default_difficulty (1);
    }

    public override async void after_reset (bool hard) throws Error {
        if (hard) {
            var proxy = runner.platform_proxy as Proxy;

            left_difficulty = yield proxy.get_default_difficulty (0);
            right_difficulty = yield proxy.get_default_difficulty (1);
        }
    }

    public override async void load_state (SnapshotPlatformMetadata? metadata) throws Error {
        var platform_metadata = metadata as SnapshotMetadata;

        left_difficulty = platform_metadata.left_difficulty;
        right_difficulty = platform_metadata.right_difficulty;
    }

    public override async void save_state (SnapshotPlatformMetadata metadata) throws Error {
        var platform_metadata = metadata as SnapshotMetadata;

        platform_metadata.left_difficulty = left_difficulty;
        platform_metadata.right_difficulty = right_difficulty;
    }

    public override uint get_n_players () {
        return n_players;
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        var menu = new SwitchMenu ();

        menu.track_fullscreen (view);

        bind_property (
            "left-difficulty", menu, "left-difficulty", BIDIRECTIONAL | SYNC_CREATE
        );
        bind_property (
            "right-difficulty", menu, "right-difficulty", BIDIRECTIONAL | SYNC_CREATE
        );
        menu.pause.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.press_pause.begin ();
        });
        menu.select.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.press_select.begin ();
        });
        menu.reset.connect (() => {
            runner.reset.begin (false);
        });

        return menu;
    }
}
