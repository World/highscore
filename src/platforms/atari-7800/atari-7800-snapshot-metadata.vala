// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Atari7800.SnapshotMetadata : SnapshotPlatformMetadata {
    public Hs.Atari7800Difficulty left_difficulty { get; set; }
    public Hs.Atari7800Difficulty right_difficulty { get; set; }

    public override void load (KeyFile keyfile) {
        string[] names = { "Left", "Right" };
        for (int i = 0; i < Hs.ATARI_7800_MAX_PLAYERS; i++) {
            var name = names[i];
            Hs.Atari7800Difficulty diff;
            string? diff_str;

            try {
                diff_str = keyfile.get_string ("Atari 7800", @"$name Difficulty");
            } catch (Error e) {
                diff_str = "Advanced";
            }

            if (diff_str == "Advanced") {
                diff = ADVANCED;
            } else if (diff_str == "Beginner") {
                diff = BEGINNER;
            } else {
                warning ("Unsupported Atari 7800 difficulty: %s", diff_str);
                diff = ADVANCED;
            }

            if (i == 0)
                left_difficulty = diff;
            else
                right_difficulty = diff;
        }
    }

    public override void save (KeyFile keyfile) {
        string[] names = { "Left", "Right" };
        Hs.Atari7800Difficulty[] difficulty = {
            left_difficulty, right_difficulty
        };

        for (int i = 0; i < Hs.ATARI_7800_MAX_PLAYERS; i++) {
            var name = names[i];
            var diff_str = "";

            switch (difficulty[i]) {
                case ADVANCED:
                    diff_str = "Advanced";
                    break;
                case BEGINNER:
                    diff_str = "Beginner";
                    break;
                default:
                    assert_not_reached ();
            }

            keyfile.set_string ("Atari 7800", @"$name Difficulty", diff_str);
        }
    }
}
