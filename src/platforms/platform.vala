// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Platform : Object {
    public Hs.Platform platform { get; construct; }
    public string id { get; construct; }
    public string name { get; construct; }

    public string icon_name { get; set; }

    public Type parser_type { get; set; default = Type.NONE; }
    public Type runner_delegate_type { get; set; default = Type.NONE; }
    public Type controls_type { get; set; default = Type.NONE; }
    public Type metadata_type { get; set; default = Type.NONE; }
    public Type properties_type { get; set; default = Type.NONE; }
    public Type screen_set_type { get; set; default = Type.NONE; }
    public Type firmware_list_type { get; set; default = Type.NONE; }
    public Type snapshot_metadata_type { get; set; default = Type.NONE; }

    public Platform parent { get; set; }

    private bool firmware_list_created;
    private FirmwareList? _firmware_list;
    public FirmwareList? firmware_list {
        get {
            if (!firmware_list_created) {
                _firmware_list = FirmwareList.create (this);
                firmware_list_created = true;
            }

            return _firmware_list;
        }
    }

    private string[] mime_types;
    private string[] extensions;

    public Platform (Hs.Platform platform, string id, string name, string[] mime_types, string[] extensions) {
        Object (platform: platform, id: id, name: name);

        icon_name = @"platform-$id-symbolic";
        this.mime_types = mime_types;
        this.extensions = extensions;
    }

    public string[] get_mime_types () {
        return mime_types;
    }

    public string[] get_extensions () {
        return extensions;
    }

    public static uint hash (Platform platform) {
        return int_hash (platform.platform);
    }

    public static bool equal (Platform a, Platform b) {
        return a == b || a.platform == b.platform;
    }

    public static int compare (Platform a, Platform b) {
        return a.name.collate (b.name);
    }
}
