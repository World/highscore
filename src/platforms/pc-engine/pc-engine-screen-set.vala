// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PcEngine.ScreenSet : Highscore.ScreenSet {
    public override ScreenType get_screen_type (Hs.Region region) {
        return NTSC;
    }
}
