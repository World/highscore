// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PcEngine.RunnerDelegate : Highscore.RunnerDelegate {
    public bool six_button_controller { get; set; }

    private uint n_players;
    private Settings settings;

    construct {
        settings = new GameSettings (runner.game).get_platform ();

        settings.bind ("six-button-controller", this, "six-button-controller", GET);

        notify["six-button-controller"].connect (update_pad_mode);
    }

    private void update_pad_mode () {
        Hs.PcEnginePadMode pad_mode = TWO_BUTTONS;

        if (six_button_controller)
            pad_mode = SIX_BUTTONS;

        for (int i = 0; i < Hs.PC_ENGINE_MAX_PLAYERS; i++) {
            runner.set_controller_type (
                i, six_button_controller ? "pc-engine-6button" : "pc-engine"
            );
        }

        runner.input_buffer.modify_input_state (state => {
            for (int i = 0; i < Hs.PC_ENGINE_MAX_PLAYERS; i++)
                state->pc_engine.pad_mode[i] = pad_mode;
        });
    }

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        n_players = yield proxy.get_players ();

        update_pad_mode ();
    }

    public override uint get_n_players () {
        return n_players;
    }
}
