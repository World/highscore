// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PcEngine.Controls : PlatformControls {
    construct {
        n_players = Hs.PC_ENGINE_MAX_PLAYERS;

        // 2 button controller
        var controller = new PlatformController ("pc-engine", _("Turbo Pad"));
        controller.add_sections ("dpad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Direction Key"),
            Hs.PcEngineButton.UP,
            Hs.PcEngineButton.DOWN,
            Hs.PcEngineButton.LEFT,
            Hs.PcEngineButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "i",  _("Button I"),  Hs.PcEngineButton.I,
            "ii", _("Button II"), Hs.PcEngineButton.II,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select Button"), Hs.PcEngineButton.SELECT,
            "run",    _("Run Button"),    Hs.PcEngineButton.RUN,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_D,         "i");
        kbd_mapping.map (Linux.Input.KEY_S,         "ii");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "run");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (EAST,   "i");
        gamepad_mapping.map_button (SOUTH,  "ii");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "run");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // 6 button controller

        controller = new PlatformController (
            "pc-engine-6button",
            _("Arcade Pad 6"),
            "platform-pc-engine-6button-symbolic"
        );
        controller.add_sections ("dpad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Direction Key"),
            Hs.PcEngineButton.UP,
            Hs.PcEngineButton.DOWN,
            Hs.PcEngineButton.LEFT,
            Hs.PcEngineButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "i",   _("Button I"),   Hs.PcEngineButton.I,
            "ii",  _("Button II"),  Hs.PcEngineButton.II,
            "iii", _("Button III"), Hs.PcEngineButton.III,
            "iv",  _("Button IV"),  Hs.PcEngineButton.IV,
            "v",   _("Button V"),   Hs.PcEngineButton.V,
            "vi",  _("Button VI"),  Hs.PcEngineButton.VI,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select Button"), Hs.PcEngineButton.SELECT,
            "run",    _("Run Button"),    Hs.PcEngineButton.RUN,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_D,         "i");
        kbd_mapping.map (Linux.Input.KEY_S,         "ii");
        kbd_mapping.map (Linux.Input.KEY_A,         "iii");
        kbd_mapping.map (Linux.Input.KEY_Q,         "iv");
        kbd_mapping.map (Linux.Input.KEY_W,         "v");
        kbd_mapping.map (Linux.Input.KEY_E,         "vi");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "run");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad ("dpad");
        gamepad_mapping.map_button (EAST,   "i");
        gamepad_mapping.map_button (SOUTH,  "ii");
        gamepad_mapping.map_button (WEST,   "iii");
        gamepad_mapping.map_button (NORTH,  "iv");
        gamepad_mapping.map_button (L,      "v");
        gamepad_mapping.map_button (R,      "vi");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "run");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        default_controller = "pc-engine";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->pc_engine.pad_buttons[player], button, pressed);
        });
    }
}
