// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/pc-engine/pc-engine-properties.ui")]
public class Highscore.PcEngine.Properties : PlatformProperties {
    private Settings settings;

    public bool six_button_controller { get; set; }

    construct {
        settings = new GameSettings (game).get_platform ();

        settings.bind ("six-button-controller", this, "six-button-controller", DEFAULT);
    }
}
