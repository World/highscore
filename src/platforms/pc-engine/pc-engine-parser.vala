// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PcEngine.Parser : GameParser {
    private string fingerprint;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    public override bool parse () throws Error {
        fingerprint = Fingerprint.get_uid (file, platform.id);

        var checksum = fingerprint.replace ("pc-engine-", "");

        var cd_platform = PlatformRegister.get_register ().get_platform ("pc-engine-cd");
        var list = FirmwareList.create (cd_platform);
        var syscard = list.get_firmware ("syscard3");

        // syscard3.rom is a valid PC Engine rom, but we don't want to
        // show it as a game.
        if (syscard.matches (checksum, null))
            return false;

        return true;
    }

    public override string get_media_uid () throws Error {
        return fingerprint;
    }
}
