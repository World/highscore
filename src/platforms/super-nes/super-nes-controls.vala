// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.SuperNes.Controls : PlatformControls {
    construct {
        n_players = Hs.SUPER_NES_MAX_PLAYERS;

        var controller = new PlatformController ("super-nes", _("Super NES Controller"));
        controller.add_sections (
            "dpad", "face-buttons", "shoulder-buttons", "menu", null
        );

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Control Pad"),
            Hs.SuperNesButton.UP,
            Hs.SuperNesButton.DOWN,
            Hs.SuperNesButton.LEFT,
            Hs.SuperNesButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.SuperNesButton.A,
            "b", _("B Button"), Hs.SuperNesButton.B,
            "x", _("X Button"), Hs.SuperNesButton.X,
            "y", _("Y Button"), Hs.SuperNesButton.Y,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "shoulder-buttons",
            "l", _("L Button"), Hs.SuperNesButton.L,
            "r", _("R Button"), Hs.SuperNesButton.R,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.SuperNesButton.SELECT,
            "start",  _("Start"),  Hs.SuperNesButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,         "x");
        kbd_mapping.map (Linux.Input.KEY_A,         "y");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "a");
        kbd_mapping.map (Linux.Input.KEY_Q,         "l");
        kbd_mapping.map (Linux.Input.KEY_E,         "r");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (WEST,   "y");
        gamepad_mapping.map_button (NORTH,  "x");
        gamepad_mapping.map_button (L,      "l");
        gamepad_mapping.map_button (R,      "r");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "super-nes";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->super_nes.pad_buttons[player], button, pressed);
        });
    }
}
