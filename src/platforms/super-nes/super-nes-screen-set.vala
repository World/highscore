// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.SuperNes.ScreenSet : Highscore.ScreenSet {
    public override ScreenType get_screen_type (Hs.Region region) {
        if (region == PAL)
            return PAL;

        return NTSC_SUPER_NES;
    }
}
