// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.Fds.Proxy : Object {
    public abstract async void set_bios_path (string path) throws Error;

    public abstract async uint get_n_sides () throws Error;

    public abstract async uint get_side () throws Error;

    public abstract async void set_side (uint side) throws Error;
}
