// This file is part of Highscore. License: GPL-3.0-or-later
// vala-lint=skip-file

public class Highscore.Fds.FirmwareList : Highscore.FirmwareList {
    construct {
        var firmware = new Firmware ("fds", "BIOS");

        firmware.filename = "disksys.rom";

        firmware.add_checksum (
            "ca30b50f880eb660a320674ed365ef7a",
            "d7692af63b107b7e79ccf0d90e5ff7e70325e3196a41b6d4d83aaf8ffa5ca976351f8f31bcb1d497c02f7c699e94e77c27a0813f72aa96b576685acaae5bdad5"
        );

        add_firmware (firmware);
    }

    public override async void load (string id, File file, Runner runner) throws Error {
        var proxy = runner.addon_proxy as Proxy;

        if (id == "fds")
            yield proxy.set_bios_path (file.get_path ());
    }
}
