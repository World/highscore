// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Fds.RunnerDelegate : Nes.RunnerDelegate {
    public uint n_sides { get; private set; }
    public uint side { get; set; }

    construct {
        notify["side"].connect (() => {
            var proxy = runner.addon_proxy as Proxy;
            proxy.set_side.begin (side);
        });
    }

    public override async void after_load () throws Error {
        yield base.after_load ();

        var proxy = runner.addon_proxy as Proxy;

        n_sides = yield proxy.get_n_sides ();
    }

    public override async void after_reset (bool hard) throws Error {
        yield base.after_reset (hard);

        if (hard)
            side = 0;
    }

    public override async void load_state (SnapshotPlatformMetadata? metadata) throws Error {
        yield base.load_state (metadata);

        var proxy = runner.addon_proxy as Proxy;

        side = yield proxy.get_side ();
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        if (n_sides < 2)
            return base.create_header_widget (view);

        string[] sides = {};
        for (uint i = 0; i < n_sides; i++)
            sides += _("Side %c").printf ('A' + (int) i);

        var switcher = new MediaSwitcher (FLOPPY, _("Switch Disk Side"), sides);

        switcher.track_fullscreen (view);

        bind_property ("side", switcher, "selected-media", SYNC_CREATE | BIDIRECTIONAL);

        var box = new Gtk.Box (HORIZONTAL, 6);
        box.append (switcher);
        box.append (base.create_header_widget (view));

        return box;
    }

    public override OverlayMenuAddin? create_overlay_menu () {
        if (n_sides < 2)
            return null;

        string[] sides = {};
        for (uint i = 0; i < n_sides; i++)
            sides += _("Side %c").printf ('A' + (int) i);

        var switcher = new OverlayMenuMediaSwitcher ("fds", _("Switch Disk Side"), sides);

        bind_property ("side", switcher, "selected-media", SYNC_CREATE | BIDIRECTIONAL);

        return switcher;
    }
}
