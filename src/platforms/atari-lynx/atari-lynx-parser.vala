// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://atarigamer.com/lynx/lnxhdrgen
public class Highscore.AtariLynx.Parser : GameParser {
    private const string MAGIC_NUMBER = "LYNX";
    private const size_t ROTATION_OFFSET = 0x3A;
    private const size_t HEADER_SIZE = 0x40;

    private Rotation rotation;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    private string get_md5 (DataInputStream stream) throws Error {
        size_t size;

        stream.seek (0, SeekType.END);
        size = (size_t) stream.tell () - HEADER_SIZE;

        stream.seek (HEADER_SIZE, SeekType.SET);
        var bytes = stream.read_bytes (size);

        return Checksum.compute_for_bytes (ChecksumType.MD5, bytes);
    }

    public override bool parse () throws Error {
        var stream = new DataInputStream (file.read ());

        // Check magic number
        var magic_buffer = new uint8[MAGIC_NUMBER.length];
        stream.read (magic_buffer);

        for (int i = 0; i < MAGIC_NUMBER.length; i++) {
            if (magic_buffer[i] != ((uint8[]) MAGIC_NUMBER)[i]) {
                parser_debug ("Wrong magic number, discarding");
                return false;
            }
        }

        stream.seek (ROTATION_OFFSET, SET);
        uint8 rotation_byte = stream.read_byte ();

        var md5 = get_md5 (stream);

        switch (rotation_byte) {
            case 0:
                rotation = NONE;
                break;
            case 1:
                rotation = LEFT;
                break;
            case 2:
                rotation = RIGHT;
                break;
            default:
                rotation = NONE;
                break;
        }

        var @override = Overrides.find_rotation_override (md5);
        if (@override != null) {
            parser_debug (
                "Found orientation: %s, corrected to %s",
                rotation.to_string (),
                @override.to_string ()
            );

            rotation = @override;
        } else {
            parser_debug ( "Found orientation: %s", rotation.to_string () );
        }


        return true;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (rotation);
    }
}
