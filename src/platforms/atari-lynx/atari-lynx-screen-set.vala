// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.AtariLynx.ScreenSet : Highscore.ScreenSet {
    construct {
        var metadata = game.metadata as Metadata;

        switch (metadata.rotation) {
            case NONE:
                layout = new SingleLayout (DEFAULT_SCREEN);
                break;
            case LEFT:
                layout = new SingleLayout (DEFAULT_SCREEN, 90_DEG);
                break;
            case RIGHT:
                layout = new SingleLayout (DEFAULT_SCREEN, 270_DEG);
                break;
            default:
                assert_not_reached ();
        }
    }

    public override ScreenType get_screen_type (Hs.Region region) {
        return LCD_RGB;
    }
}
