// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.AtariLynx.Metadata : Object, GameMetadata {
    public Rotation rotation { get; set; }

    public Metadata (Rotation rotation) {
        Object (rotation: rotation);
    }

    protected VariantType serialize_type () {
        return VariantType.UINT16;
    }

    protected Variant serialize () {
        return rotation;
    }

    protected void deserialize (Variant variant) {
        rotation = (Rotation) variant.get_uint16 ();
    }
}
