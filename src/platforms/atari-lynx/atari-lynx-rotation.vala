// This file is part of Highscore. License: GPL-3.0-or-later

public enum Highscore.AtariLynx.Rotation {
    NONE,
    LEFT,  // clockwise
    RIGHT; // counterclockwise

    public string to_string () {
        switch (this) {
            case NONE:
                return "none";
            case LEFT:
                return "left";
            case RIGHT:
                return "right";
            default:
                assert_not_reached ();
        }
    }

    public static Rotation? from_string (string str) {
        if (str == "none")
            return NONE;
        if (str == "left")
            return LEFT;
        if (str == "right")
            return RIGHT;

        return null;
    }

    public static void bind_settings (
        Settings settings,
        string key,
        Object obj,
        string prop,
        SettingsBindFlags flags
    ) {
        settings.bind_with_mapping (
            key, obj, prop, flags,
            (value, variant, user_data) => {
                var str = variant.get_string ();
                var mode = from_string (str);

                if (mode == null) {
                    critical ("Unknown rotation: %s", str);
                    value = NONE;
                } else {
                    value = (Rotation) mode;
                }

                return true;
            },
            (value, variant_type, user_data) => {
                var mode = (Rotation) value.get_enum ();
                return mode.to_string ();
            },
            null, null
        );
    }
}
