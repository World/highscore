// This file is part of Highscore. License: GPL-3.0-or-later

// Reference: https://atarilynxvault.com/lynx
namespace Highscore.AtariLynx.Overrides {
    private struct RotationOverride {
        string md5;
        Rotation rotation;
    }

    private const RotationOverride[] ROTATION_OVERRIDES = {
        { "0b572c0dfb938849eeec39b2c9583547", LEFT  }, // Gauntlet - The Third Encounter (USA, Europe)
        { "13cb869b95c67c532efdd0e924e37299", RIGHT }, // Centipede (USA) (Proto)
        { "276b9be28571189912f05d321fcb04ef", RIGHT }, // NFL Football (USA, Europe)
        { "57043e8e79588c067118a4d5f307cd76", LEFT  }, // Klax (USA, Europe) (Beta)
        { "7ee41edaef283459c9df93366c5da267", RIGHT }, // Lexis (USA)
        { "8815ea087af1aa89b4f7b65bd3cb8534", LEFT  }, // Gauntlet - The Third Encounter (USA, Europe) (Beta) (1990-06-04)
        { "abfd6ae93c31e8f59aa934ad922cb4dd", RIGHT }, // Raiden (USA) (Proto)
        { "f96a0ddcc72c971226e8fdfd95607c88", LEFT  }, // Klax (USA, Europe)
    };

    public Rotation? find_rotation_override (string md5) {
        void *ret = Posix.bsearch (
            md5,
            ROTATION_OVERRIDES,
            ROTATION_OVERRIDES.length,
            sizeof (RotationOverride),
            (a, b) => {
                var needle = (string) a;
                var override = (RotationOverride *) b;
                return strcmp (needle, override.md5);
            }
        );

        if (ret == null)
            return null;

        return (* ((RotationOverride *) ret)).rotation;
    }
}
