// This file is part of Highscore. License: GPL-3.0-or-later
// vala-lint=skip-file

public class Highscore.AtariLynx.FirmwareList : Highscore.FirmwareList {
    construct {
        var firmware = new Firmware ("lynxboot", "Lynx boot ROM");

        firmware.filename = "lynxboot.img";

        firmware.add_checksum (
            "fcd403db69f54290b51035d82f835e7b",
            "d7d0f9379dcefc253127357fbf7fafce9c630878f3ff987b19692fdbafb6419490c8320f192f6eb0933f68add413537cee7cc31e9c01c9ab8c478bc1167f1575"
        );

        add_firmware (firmware);
    }

    public override async void load (string id, File file, Runner runner) throws Error {
        var proxy = runner.platform_proxy as Proxy;

        if (id == "lynxboot")
            yield proxy.set_bios_path (file.get_path ());
    }
}
