// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.AtariLynx.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("atari-lynx-landscape", _("Atari Lynx"));
        controller.add_sections ("joypad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joypad", _("Joypad"),
            Hs.AtariLynxButton.UP,
            Hs.AtariLynxButton.DOWN,
            Hs.AtariLynxButton.LEFT,
            Hs.AtariLynxButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.AtariLynxButton.A,
            "b", _("B Button"), Hs.AtariLynxButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "option1", _("Option 1"), Hs.AtariLynxButton.OPTION1,
            "option2", _("Option 2"), Hs.AtariLynxButton.OPTION2,
            "pause",   _("Pause"),    Hs.AtariLynxButton.PAUSE,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joypad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joypad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joypad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joypad:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "b");
        kbd_mapping.map (Linux.Input.KEY_D,     "a");
        kbd_mapping.map (Linux.Input.KEY_Q,     "option1");
        kbd_mapping.map (Linux.Input.KEY_E,     "option2");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "pause");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("joypad");
        gamepad_mapping.map_button (SOUTH, "b");
        gamepad_mapping.map_button (EAST,  "a");
        gamepad_mapping.map_button (L,     "option1");
        gamepad_mapping.map_button (R,     "option2");
        gamepad_mapping.map_button (START, "pause");
        gamepad_mapping.map_stick  (LEFT,  "joypad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // Portrait (left)

        controller = new PlatformController (
            "atari-lynx-portrait-left",
            _("Atari Lynx"),
            "platform-atari-lynx-left-symbolic"
        );
        controller.add_sections ("joypad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joypad", _("Joypad"),
            Hs.AtariLynxButton.LEFT,
            Hs.AtariLynxButton.RIGHT,
            Hs.AtariLynxButton.DOWN,
            Hs.AtariLynxButton.UP
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.AtariLynxButton.A,
            "b", _("B Button"), Hs.AtariLynxButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "option1", _("Option 1"), Hs.AtariLynxButton.OPTION1,
            "option2", _("Option 2"), Hs.AtariLynxButton.OPTION2,
            "pause",   _("Pause"),    Hs.AtariLynxButton.PAUSE,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joypad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joypad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joypad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joypad:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "b");
        kbd_mapping.map (Linux.Input.KEY_D,     "a");
        kbd_mapping.map (Linux.Input.KEY_Q,     "option1");
        kbd_mapping.map (Linux.Input.KEY_E,     "option2");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "pause");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad ("joypad");
        gamepad_mapping.map_button (SOUTH, "b");
        gamepad_mapping.map_button (EAST,  "a");
        gamepad_mapping.map_button (L,     "option1");
        gamepad_mapping.map_button (R,     "option2");
        gamepad_mapping.map_button (START, "pause");
        gamepad_mapping.map_stick  (LEFT,  "joypad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        // Portrait (right)

        controller = new PlatformController (
            "atari-lynx-portrait-right",
            _("Atari Lynx"),
            "platform-atari-lynx-right-symbolic"
        );
        controller.add_sections ("joypad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joypad", _("Joypad"),
            Hs.AtariLynxButton.RIGHT,
            Hs.AtariLynxButton.LEFT,
            Hs.AtariLynxButton.UP,
            Hs.AtariLynxButton.DOWN
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.AtariLynxButton.A,
            "b", _("B Button"), Hs.AtariLynxButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "option1", _("Option 1"), Hs.AtariLynxButton.OPTION1,
            "option2", _("Option 2"), Hs.AtariLynxButton.OPTION2,
            "pause",   _("Pause"),    Hs.AtariLynxButton.PAUSE,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joypad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joypad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joypad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joypad:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "b");
        kbd_mapping.map (Linux.Input.KEY_D,     "a");
        kbd_mapping.map (Linux.Input.KEY_Q,     "option1");
        kbd_mapping.map (Linux.Input.KEY_E,     "option2");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "pause");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("joypad");
        gamepad_mapping.map_button (SOUTH, "b");
        gamepad_mapping.map_button (EAST,  "a");
        gamepad_mapping.map_button (L,     "option1");
        gamepad_mapping.map_button (R,     "option2");
        gamepad_mapping.map_button (START, "pause");
        gamepad_mapping.map_stick  (LEFT,  "joypad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);

        default_controller = "atari-lynx-landscape";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->atari_lynx.buttons, button, pressed);
        });
    }
}
