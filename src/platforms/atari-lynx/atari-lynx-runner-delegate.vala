// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.AtariLynx.RunnerDelegate : Highscore.RunnerDelegate {
    public override async void after_load () throws Error {
        var metadata = runner.game.metadata as Metadata;
        var controller_type = "";

        switch (metadata.rotation) {
            case NONE:
                controller_type = "atari-lynx-landscape";
                break;
            case LEFT:
                controller_type = "atari-lynx-portrait-left";
                break;
            case RIGHT:
                controller_type = "atari-lynx-portrait-right";
                break;
            default:
                assert_not_reached ();
        }

        runner.set_controller_type (0, controller_type);
    }
}
