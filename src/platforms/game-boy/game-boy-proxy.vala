// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.GameBoy.Proxy : Object {
    public abstract async void set_model (Hs.GameBoyModel model) throws Error;

    public abstract async void set_palette (int[] colors) throws Error;
}
