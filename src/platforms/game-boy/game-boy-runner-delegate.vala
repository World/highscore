// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameBoy.RunnerDelegate : Highscore.RunnerDelegate {
    public Hs.GameBoyModel model { get; private set; }
    public string palette { get; set; }
    public bool has_palettes { get; private set; }

    private Settings settings;

    construct {
        notify["has-palettes"].connect (() => {
            if (has_palettes)
                settings.bind ("palette", this, "palette", DEFAULT);
            else
                Settings.unbind (this, "palette");
        });

        settings = new GameSettings (runner.game).get_platform ();

        if (settings.get_user_value ("model") == null) {
            var model = Model.get_auto_for_game (runner.game);
            settings.set_string ("model", Model.to_string (model));
        }

        var palette = settings.get_user_value ("palette");
        if (palette == null || !Palette.exists (palette.get_string ())) {
            var metadata = runner.game.metadata as Metadata;

            settings.set_string ("palette", metadata.get_default_palette ());
        }

        runner.set_rumble_enabled (0, true);
    }

    public override async void after_load () throws Error {
        yield reset_model ();
    }

    public override async void before_reset (bool hard) throws Error {
        yield reset_model ();
    }

    public override async void load_state (SnapshotPlatformMetadata? metadata) throws Error {
        var platform_metadata = metadata as SnapshotMetadata;

        model = platform_metadata.model;

        yield update_palette ();

        var proxy = runner.platform_proxy as Proxy;
        yield proxy.set_model (model);
    }

    public override async void save_state (SnapshotPlatformMetadata metadata) throws Error {
        var platform_metadata = metadata as SnapshotMetadata;

        platform_metadata.model = model;
    }

    private async void reset_model () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        var settings = new GameSettings (runner.game).get_platform ();
        bool model_set = settings.get_user_value ("model") != null;

        if (model_set) {
            var model_str = settings.get_string ("model");
            model = Model.from_string (model_str);
        } else {
            model = Model.get_auto_for_game (runner.game);
        }

        yield update_palette ();
        yield proxy.set_model (model);
    }

    private async void update_palette () throws Error {
        var proxy = runner.platform_proxy as Proxy;
        var metadata = runner.game.metadata as Metadata;

        switch (model) {
            case GAME_BOY:
            case GAME_BOY_POCKET:
                has_palettes = true;

                proxy.set_palette.begin (ScreenSet.SOURCE_PALETTE);

                break;
            case GAME_BOY_COLOR:
            case GAME_BOY_ADVANCE:
                yield proxy.set_palette (
                    DEFAULT_GBC_PALETTE.get_raw_colors ()
                );

                has_palettes = false;

                break;
            case SUPER_GAME_BOY:
            case SUPER_GAME_BOY_2:
                var sgb_palette = metadata.get_special_sgb_palette ();
                if (sgb_palette == null)
                    sgb_palette = DEFAULT_SGB_PALETTE;

                yield proxy.set_palette (sgb_palette.get_colors ());

                has_palettes = false;

                break;
            default:
                assert_not_reached ();
        }
    }
}
