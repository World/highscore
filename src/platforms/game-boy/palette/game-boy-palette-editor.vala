// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameBoy.ColorLayerRow : Adw.ActionRow {
    public signal void colors_changed ();

    public Gdk.RGBA rgba1 { get; set; }
    public Gdk.RGBA rgba2 { get; set; }
    public Gdk.RGBA rgba3 { get; set; }
    public Gdk.RGBA rgba4 { get; set; }

    construct {
        add_css_class ("color");

        for (int i = 0; i < 4; i++) {
            var dialog = new Gtk.ColorDialog () {
                title = _("Color %u").printf (i + 1),
                with_alpha = false,
            };

            var color_button = new Gtk.ColorDialogButton (dialog) {
                valign = CENTER,
            };

            bind_property (
                @"rgba$(i + 1)", color_button, "rgba", SYNC_CREATE | BIDIRECTIONAL
            );

            color_button.notify["rgba"].connect (() => colors_changed ());

            add_suffix (color_button);
        }
    }

    public void set_rgba (int i, Gdk.RGBA rgba) {
        switch (i) {
            case 0: rgba1 = rgba; break;
            case 1: rgba2 = rgba; break;
            case 2: rgba3 = rgba; break;
            case 3: rgba4 = rgba; break;
            default: assert_not_reached ();
        }
    }

    public Gdk.RGBA get_rgba (int i) {
        switch (i) {
            case 0: return rgba1;
            case 1: return rgba2;
            case 2: return rgba3;
            case 3: return rgba4;
            default: assert_not_reached ();
        }
    }
}

[GtkTemplate (ui = "/app/drey/Highscore/platforms/game-boy/palette/game-boy-palette-editor.ui")]
public class Highscore.GameBoy.PaletteEditor : PropertiesPage {
    public signal void saved ();

    public CustomPalette palette { get; construct; }
    public bool editing { get; construct; }

    public string palette_name { get; set; default = _("New Palette"); }
    public bool sprite_colors { get; set; }

    [GtkChild]
    private unowned PaletteThumbnail thumbnail;
    [GtkChild]
    private unowned ColorLayerRow bg_row;
    [GtkChild]
    private unowned ColorLayerRow obj0_row;
    [GtkChild]
    private unowned ColorLayerRow obj1_row;

    private int colors[12];

    public PaletteEditor (CustomPalette palette, Gdk.Texture? frame, bool editing) {
        Object (palette: palette, editing: editing);

        thumbnail.frame = frame;
    }

    construct {
        title = editing ? _("Edit Palette") : _("Add Palette");

        if (palette.name != null)
            palette_name = palette.name;

        var c = palette.get_colors ();

        for (int i = 0; i < 4; i++) {
            bg_row.set_rgba (i, ColorUtils.color_to_rgba (c[i]));
            obj0_row.set_rgba (i, ColorUtils.color_to_rgba (c[i + 4]));
            obj1_row.set_rgba (i, ColorUtils.color_to_rgba (c[i + 8]));
        }

        sprite_colors = Palette.get_uses_sprite_colors (c);

        notify["sprite-colors"].connect (sync_colors);
        notify["palette-name"].connect (update_actions);

        sync_colors ();
    }

    static construct {
        install_action ("palette.save", null, widget => {
            var self = widget as PaletteEditor;
            self.save_palette ();
        });
        install_action ("palette.enter-sgb-password", null, widget => {
            var self = widget as PaletteEditor;
            self.enter_sgb_password.begin ();
        });
    }

    [GtkCallback]
    private void sync_colors () {
        for (int i = 0; i < 4; i++) {
            colors[i] = ColorUtils.rgba_to_color (bg_row.get_rgba (i));

            if (sprite_colors) {
                colors[i + 4] = ColorUtils.rgba_to_color (obj0_row.get_rgba (i));
                colors[i + 8] = ColorUtils.rgba_to_color (obj1_row.get_rgba (i));
            } else {
                colors[i + 4] = ColorUtils.rgba_to_color (bg_row.get_rgba (i));
                colors[i + 8] = ColorUtils.rgba_to_color (bg_row.get_rgba (i));
            }
        }

        thumbnail.set_colors (colors);

        update_actions ();
    }

    private void update_actions () {
        bool name_empty = palette_name == "";

        bool changed = true;

        if (editing) {
            changed = false;

            if (palette_name != palette.name)
                changed = true;

            if (!changed) {
                var c = palette.get_colors ();

                for (int i = 0; i < 12; i++) {
                    if (colors[i] != c[i]) {
                        changed = true;
                        break;
                    }
                }
            }
        }

        action_set_enabled ("palette.save", changed && !name_empty);
    }

    private void save_palette () {
        palette.name = palette_name;
        palette.set_colors (colors);

        var manager = CustomPaletteManager.get_instance ();

        if (editing)
            manager.save_palette (palette);
        else
            manager.add_palette (palette);

        saved ();
    }

    private async void enter_sgb_password () {
        var dialog = new Adw.AlertDialog (
            "Enter SGB Password",
            "Example: <b>7047-0470-4704</b>"
        );

        dialog.body_use_markup = true;
        dialog.add_css_class ("numeric");

        dialog.add_response ("cancel", _("_Cancel"));
        dialog.add_response ("enter", _("_Enter"));

        dialog.set_response_appearance ("enter", SUGGESTED);

        dialog.default_response = "enter";

        var group = new Adw.PreferencesGroup ();
        var entry = new Adw.EntryRow () {
            title = _("Password"),
            activates_default = true,
        };
        var error_label = new Gtk.Label (null) {
            ellipsize = END,
            xalign = 0,
            margin_top = 6,
        };
        error_label.add_css_class ("error");
        error_label.add_css_class ("caption");

        group.add (entry);
        group.add (error_label);

        dialog.set_response_enabled ("enter", false);

        entry.notify["text"].connect (() => {
            bool valid = SgbPassword.is_valid (entry.text.strip ());

            dialog.set_response_enabled ("enter", valid);

            if (valid) {
                entry.remove_css_class ("error");
                error_label.label = "";
            } else {
                entry.add_css_class ("error");
                error_label.label = _("Invalid password");
            }
        });

        dialog.extra_child = group;

        var response = yield dialog.choose (this, null);

        if (response == "enter") {
            var colors = SgbPassword.parse (entry.text.strip ());

            assert (colors != null);

            sprite_colors = false;

            for (int i = 0; i < 4; i++) {
                var rgba = ColorUtils.color_to_rgba (colors[i]);

                bg_row.set_rgba (i, rgba);
                obj0_row.set_rgba (i, rgba);
                obj1_row.set_rgba (i, rgba);
            }
        }
    }
}
