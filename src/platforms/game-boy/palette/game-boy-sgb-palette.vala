// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.GameBoy {
    private static Regex? sgb_regex = null;
    private static Regex? password_regex = null;

    public const SgbPalette DEFAULT_SGB_PALETTE = 1_A;

    public enum SgbPalette {
        1_A, 1_B, 1_C, 1_D, 1_E, 1_F, 1_G, 1_H,
        2_A, 2_B, 2_C, 2_D, 2_E, 2_F, 2_G, 2_H,
        3_A, 3_B, 3_C, 3_D, 3_E, 3_F, 3_G, 3_H,
        4_A, 4_B, 4_C, 4_D, 4_E, 4_F, 4_G, 4_H;

        public int[] get_colors () {
            return SgbPassword.lookup_preset_colors (this);
        }

        public string get_id () {
            var name = get_name ();

            return @"sgb($name)";
        }

        public string get_name () {
            char column = 'A' + (char) this % 8;
            int row = this / 8 + 1;

            return @"$row-$column";
        }

        public static SgbPalette[] all () {
            return {
                1_A, 1_B, 1_C, 1_D, 1_E, 1_F, 1_G, 1_H,
                2_A, 2_B, 2_C, 2_D, 2_E, 2_F, 2_G, 2_H,
                3_A, 3_B, 3_C, 3_D, 3_E, 3_F, 3_G, 3_H,
                4_A, 4_B, 4_C, 4_D, 4_E, 4_F, 4_G, 4_H,
            };
        }

        public static SgbPalette? parse (string str) {
            if (str == "sgb-default")
                return 1_A;

            if (sgb_regex == null) {
                try {
                    sgb_regex = new Regex ("sgb\\(([1-8])-([A-H])\\)");
                } catch (Error e) {
                    error ("Invalid regex: %s", e.message);
                }
            }

            MatchInfo info;
            if (sgb_regex.match (str, 0, out info)) {
                int row = info.fetch (1)[0] - '1';
                int column = info.fetch (2)[0] - 'A';

                return row * 8 + column;
            }

            return null;
        }
    }

    namespace SgbPassword {
        public bool is_valid (string password) {
            if (password_regex == null) {
                try {
                    password_regex = new Regex ("^[0-9]{4}-[0-9]{4}-[0-9]{4}$");
                } catch (Error e) {
                    error ("Invalid regex: %s", e.message);
                }
            }

            return password_regex.match (password, 0, null);
        }

        public int[]? parse (string password) {
            if (!is_valid (password))
                return null;

            var digits = password.replace ("-", "");

            int colors[4];

            for (int i = 0; i < 4; i++) {
                var group = digits.substring (i * 3, 3);
                int index = int.parse (group);

                if (index < 512)
                    colors[i] = lookup_edit_mode_color (index, i);
                else
                    colors[i] = lookup_preset_color (index, i);
            }

            return colors;
        }

        private int lookup_edit_mode_color (int index, int i) {
            int hue = index / 64;
            int shade = index % 64;

            if (shade == 52)
                shade = 51;

            if (shade > 51 || hue >= 8)
                return lookup_preset_color (FIRST_PRESET_INDEX, i);

            int lookup_index = hue * 52 + shade;

            assert (lookup_index < EDIT_MODE_COLORS.length);

            return EDIT_MODE_COLORS[lookup_index];
        }

        private int lookup_preset_color (int index, int i) {
            index -= 512;

            int shade = index / 64;
            int preset = index % 64;

            if (preset == 32)
                preset = 0;

            // Dead values, fall back to default color
            if (shade >= 7 || preset > 32)
                return lookup_preset_color (FIRST_PRESET_INDEX, i);

            int lookup_index = (preset * 7 + shade) * 4 + i;

            assert (lookup_index < PRESET_PALETTES.length);

            return PRESET_PALETTES[lookup_index];
        }

        public int[] lookup_preset_colors (int preset) {
            int colors[4];

            for (int i = 0; i < 4; i++)
                colors[i] = lookup_preset_color (FIRST_PRESET_INDEX + preset, i);

            return colors;
        }
    }
}
