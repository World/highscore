// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://tcrf.net/Notes:Game_Boy_Color_Bootstrap_ROM
namespace Highscore.GameBoy {
    private static Regex? gbc_regex = null;

    public struct GbcPalette {
        uint8 entry;
        uint8 shuffle_flags;

        public int[] get_colors () {
            var colors = get_raw_colors ();

            return correct_gbc_colors (colors);
        }

        public int[] get_raw_colors () {
            assert (entry <= MAX_GBC_ENTRY);
            assert (shuffle_flags <= MAX_GBC_FLAGS);

            int entry_start = entry * 12;

            if (shuffle_flags == 0) {
                return {
                    GBC_PALETTES[entry_start],
                    GBC_PALETTES[entry_start + 1],
                    GBC_PALETTES[entry_start + 2],
                    GBC_PALETTES[entry_start + 3],
                };
            }

            // Always use the first row for background,
            // and vary the rest depending on the shuffle flags
            int bg_index = entry_start;
            int obj0_index = entry_start + (shuffle_flags &  1) * 4;
            int obj1_index = entry_start + (shuffle_flags >> 1) * 4;

            return {
                GBC_PALETTES[bg_index],
                GBC_PALETTES[bg_index + 1],
                GBC_PALETTES[bg_index + 2],
                GBC_PALETTES[bg_index + 3],

                GBC_PALETTES[obj0_index],
                GBC_PALETTES[obj0_index + 1],
                GBC_PALETTES[obj0_index + 2],
                GBC_PALETTES[obj0_index + 3],

                GBC_PALETTES[obj1_index],
                GBC_PALETTES[obj1_index + 1],
                GBC_PALETTES[obj1_index + 2],
                GBC_PALETTES[obj1_index + 3],
            };
        }
    }

    public enum ManualGbcPalette {
        UP,   LEFT,   DOWN,   RIGHT,
        UP_A, LEFT_A, DOWN_A, RIGHT_A,
        UP_B, LEFT_B, DOWN_B, RIGHT_B;

        public GbcPalette get_palette () {
            return MANUAL_GBC_PALETTE_INFO[this].palette;
        }

        public int[] get_colors () {
            return get_palette ().get_colors ();
        }

        public int[] get_raw_colors () {
            return get_palette ().get_raw_colors ();
        }

        public string get_id () {
            return MANUAL_GBC_PALETTE_INFO[this].id;
        }

        public string get_name () {
            return MANUAL_GBC_PALETTE_INFO[this].name;
        }

        public int get_preview_bg_color () {
            return MANUAL_GBC_PALETTE_INFO[this].preview_bg_color;
        }

        public int get_preview_fg_color () {
            return MANUAL_GBC_PALETTE_INFO[this].preview_fg_color;
        }

        public static ManualGbcPalette[] all () {
            return {
                UP,   LEFT,   DOWN,   RIGHT,
                UP_A, LEFT_A, DOWN_A, RIGHT_A,
                UP_B, LEFT_B, DOWN_B, RIGHT_B
            };
        }

        public static ManualGbcPalette? parse (string str) {
            if (str == "gbc-default")
                return DEFAULT_GBC_PALETTE;

            for (int i = 0; i < MANUAL_GBC_PALETTE_INFO.length; i++) {
                var palette = MANUAL_GBC_PALETTE_INFO[i];

                if (str == palette.id)
                    return i;
            }

            if (gbc_regex == null) {
                try {
                    gbc_regex = new Regex ("gbc\\((up|left|down|right)(-[ab])?\\)");
                } catch (Error e) {
                    error ("Invalid regex: %s", e.message);
                }
            }

            MatchInfo info;
            if (gbc_regex.match (str, 0, out info)) {
                string dir = info.fetch (1);
                string mod = info.fetch (2);

                int dir_index;
                if (dir == "up")
                    dir_index = 0;
                else if (dir == "left")
                    dir_index = 1;
                else if (dir == "down")
                    dir_index = 2;
                else if (dir == "right")
                    dir_index = 3;
                else
                    assert_not_reached ();

                int mod_index;
                if (mod == "")
                    mod_index = 0;
                else if (mod == "-a")
                    mod_index = 1;
                else if (mod == "-b")
                    mod_index = 2;
                else
                    assert_not_reached ();

                return mod_index * 4 + dir_index;
            }

            return null;
        }
    }

    private struct ManualGbcPaletteInfo {
        GbcPalette palette;
        int preview_bg_color;
        int preview_fg_color;
        string id;
        string name;
    }

    private const ManualGbcPaletteInfo[] MANUAL_GBC_PALETTE_INFO = {
        {{ 0x12, 0 }, 0xF7C5A5, 0x843100, "brown",       N_("Brown")       }, // UP
        {{ 0x18, 5 }, 0x63A5FF, 0x0000FF, "blue",        N_("Blue")        }, // LEFT
        {{ 0x17, 0 }, 0xFFFFA5, 0xFF00FF, "pale-yellow", N_("Pale Yellow") }, // DOWN
        {{ 0x05, 0 }, 0x7BFF31, 0xFF0000, "green",       N_("Green"),      }, // RIGHT
        {{ 0x10, 5 }, 0xFF8484, 0xE60000, "red",         N_("Red"),        }, // UP_A
        {{ 0x0D, 5 }, 0x8C8CDE, 0x52528C, "dark-blue",   N_("Dark Blue")   }, // LEFT_A
        {{ 0x07, 0 }, 0xFFFF00, 0xFF0000, "orange",      N_("Orange"),     }, // DOWN_A
        {{ 0x1C, 3 }, 0x7BFF31, 0x0000FF, "dark-green",  N_("Dark Green")  }, // RIGHT_A
        {{ 0x19, 3 }, 0xA58452, 0x6B5231, "dark-brown",  N_("Dark Brown")  }, // UP_B
        {{ 0x16, 0 }, 0x949494, 0x000000, "gray",        N_("Gray")        }, // LEFT_B
        {{ 0x1A, 5 }, 0xFFFF3A, 0x3A2900, "yellow",      N_("Yellow")      }, // DOWN_B
        {{ 0x13, 0 }, 0x000000, 0xFFFF00, "reverse",     N_("Reverse")     }, // RIGHT_B
    };

    public const ManualGbcPalette DEFAULT_GBC_PALETTE = RIGHT_A;

    // Adapted from https://github.com/libretro/slang-shaders/blob/master/handheld/shaders/color/gbc-color.slang
    // Authors: hunterk, Pokefan531
    // License: Public domain
    private int[] correct_gbc_colors (int[] colors) {
        const float LIGHTEN_INTENSITY = 0.5f;
        const float TARGET_GAMMA = 2.2f;
        const float DISPLAY_GAMMA = 2.2f;
        const float LUM = 0.93f;

        Graphene.Matrix color_matrix = {};
        color_matrix.init_from_float ({
             0.80f,  0.135f, 0.195f, 0.0f, // red
             0.275f, 0.64f,  0.155f, 0.0f, // green
            -0.075f, 0.225f, 0.65f,  0.0f, // blue
             0.0f,   0.0f,   0.0f,   0.0f, // alpha
        });

        int[] ret = {};

        foreach (int color in colors) {
            var screen = ColorUtils.rgb_to_vec3 (color);

            screen = VectorUtils.pow3 (screen, TARGET_GAMMA - LIGHTEN_INTENSITY);
            screen = screen.scale (LUM);
            screen = VectorUtils.clamp3 (screen, 0.0f, 1.0f);
            screen = color_matrix.transform_vec3 (screen);
            screen = VectorUtils.pow3 (screen, 1.0f / DISPLAY_GAMMA);

            ret += ColorUtils.vec3_to_rgb (screen);
        }

        return ret;
    }
}
