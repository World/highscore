// This file is part of Highscore. License: GPL-3.0-or-later

public errordomain Highscore.GameBoy.CustomPaletteError {
    INVALID_COLORS,
}

public class Highscore.GameBoy.CustomPalette : Object {
    public signal void changed ();
    public signal void removed ();

    public string id { get; construct; }
    public string name { get; set; }
    private int[] colors;

    public CustomPalette.load (string id, string name, int[] colors) {
        Object (id: id, name: name);

        this.colors = colors;
    }

    public CustomPalette () {
        Object (id: Uuid.string_random ());

        colors = {};
        var grayscale_colors = Palette.SimplePalette.GRAYSCALE.get_colors ();

        for (int i = 0; i < 3; i++) {
            colors += grayscale_colors[0];
            colors += grayscale_colors[1];
            colors += grayscale_colors[2];
            colors += grayscale_colors[3];
        }
    }

    public int[] get_colors () {
        return colors;
    }

    public void set_colors (int[] colors) {
        this.colors = colors;
    }
}

public class Highscore.GameBoy.CustomPaletteManager : Object {
    private static CustomPaletteManager? instance;

    private Settings common_settings;

    private ListStore palettes;
    private HashTable<string, CustomPalette> palettes_map;

    public static CustomPaletteManager get_instance () {
        if (instance == null)
            instance = new CustomPaletteManager ();

        return instance;
    }

    construct {
        common_settings = new Settings ("app.drey.Highscore.platforms.game-boy");

        var palettes_var = common_settings.get_value ("custom-palettes");

        palettes = new ListStore (typeof (CustomPalette));
        palettes_map = new HashTable<string, CustomPalette> (str_hash, str_equal);

        size_t n = palettes_var.n_children ();
        for (size_t i = 0; i < n; i++) {
            var id = palettes_var.get_child_value (i).get_string ();

            try {
                var palette = load_palette (id);

                palettes.append (palette);
                palettes_map[palette.id] = palette;
            } catch (Error e) {
                warning ("Failed to load palette %s: %s", id, e.message);
            }
        }
    }

    private inline Settings get_settings_for_id (string id) {
        return new Settings.with_path (
            "app.drey.Highscore.platforms.game-boy.palette",
            @"/app/drey/Highscore/platforms/game-boy/palette/$id/"
        );
    }

    private CustomPalette load_palette (string id) throws CustomPaletteError {
        var settings = get_settings_for_id (id);

        string name = settings.get_string ("name");
        var colors_var = settings.get_value ("colors");
        int[] colors = {};

        size_t n = colors_var.n_children ();
        for (size_t i = 0; i < n; i++)
            colors += colors_var.get_child_value (i).get_int32 ();

        if (colors.length != 12) {
            throw new CustomPaletteError.INVALID_COLORS (
                "Invalid colors: %s", colors_var.print (false)
            );
        }

        return new CustomPalette.load (id, name, colors);
    }

    public ListModel get_palettes () {
        return palettes;
    }

    public CustomPalette? get_palette (string id) {
        if (id in palettes_map)
            return palettes_map[id];

        return null;
    }

    public void add_palette (CustomPalette palette) {
        palettes.append (palette);
        palettes_map[palette.id] = palette;

        save_palette (palette);
        save_palettes ();
    }

    public void remove_palette (CustomPalette palette) {
        uint index;
        assert (palettes.find (palette, out index));

        palettes.remove (index);
        palettes_map.remove (palette.id);

        var settings = get_settings_for_id (palette.id);
        settings.reset ("name");
        settings.reset ("colors");

        save_palettes ();

        palette.removed ();
    }

    public void save_palette (CustomPalette palette) {
        Variant[] colors_children = {};

        foreach (int color in palette.get_colors ())
            colors_children += new Variant.int32 ((int32) color);

        var colors_var = new Variant.array (VariantType.INT32, colors_children);

        var settings = get_settings_for_id (palette.id);
        settings.set_string ("name", palette.name);
        settings.set_value ("colors", colors_var);

        palette.changed ();
    }

    private void save_palettes () {
        string[] ids = {};

        uint n = palettes.get_n_items ();
        for (uint i = 0; i < n; i++) {
            var palette = palettes.get_item (i) as CustomPalette;

            ids += palette.id;
        }

        var palettes_var = new Variant.strv (ids);

        common_settings.set_value ("custom-palettes", palettes_var);
    }
}
