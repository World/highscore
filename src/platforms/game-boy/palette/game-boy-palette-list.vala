// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/game-boy/palette/game-boy-palette-list.ui")]
public class Highscore.GameBoy.PaletteList : PropertiesPage {
    private string? _palette;
    public string? palette {
        get { return _palette; }
        set {
            if (value == null || !(value in rows)) {
                reset_palette ();
                return;
            }

            select_palette (value);
        }
    }

    [GtkChild]
    private unowned PaletteThumbnail thumbnail;
    [GtkChild]
    private unowned Gtk.ListBox simple_list;
    [GtkChild]
    private unowned Gtk.ListBox special_list;
    [GtkChild]
    private unowned Gtk.ListBox gbc_list;
    [GtkChild]
    private unowned Gtk.ListBox sgb_list;
    [GtkChild]
    private unowned Gtk.ListBox custom_list;

    private ListModel custom_palettes;

    private HashTable<string, PaletteRow> rows;

    private Settings settings;
    private bool closing_editor;

    public PaletteList (Game game) {
        Object (game: game);
    }

    private PaletteRow create_row (string id, string title, owned int[] colors) {
        var row = new PaletteRow (id, title, colors, false);

        row.activated.connect (() => palette = row.id);
        row.duplicate_palette.connect (() => duplicate_palette (id));

        rows[id] = row;

        return row;
    }

    private PaletteRow create_custom_row (CustomPalette palette) {
        var row = new PaletteRow (
            palette.id, palette.name, palette.get_colors (), true
        );

        palette.changed.connect (() => {
            row.title = palette.name;
            row.set_colors (palette.get_colors ());
        });

        row.activated.connect (() => this.palette = row.id);
        row.edit_palette.connect (() => edit_palette (palette.id));
        row.duplicate_palette.connect (() => duplicate_palette (palette.id));
        row.delete_palette.connect (() => delete_palette.begin (palette.id));

        rows[palette.id] = row;

        return row;
    }

    construct {
        rows = new HashTable<string, PaletteRow> (str_hash, str_equal);

        var metadata = game.metadata as Metadata;
        var gbc_palette = metadata.get_special_gbc_palette ();
        var sgb_palette = metadata.get_special_sgb_palette ();

        foreach (var palette in Palette.SimplePalette.all ()) {
            var list = simple_list;

            if (palette == GRAYSCALE)
                list = special_list;

            list.append (create_row (
                palette.get_id (), palette.get_name (), palette.get_colors ()
            ));
        }

        foreach (var palette in ManualGbcPalette.all ()) {
            var name = palette.get_name ();

            if (gbc_palette == null && palette == DEFAULT_GBC_PALETTE)
                name = _("%s (Default)").printf (name);

            gbc_list.append (create_row (
                palette.get_id (), name, palette.get_colors ()
            ));
        }

        foreach (var palette in SgbPalette.all ()) {
            string name = palette.get_name ();

            if (sgb_palette == null && palette == DEFAULT_SGB_PALETTE)
                name = _("%s (Default)").printf (name);

            sgb_list.append (create_row (
                palette.get_id (), name, palette.get_colors ()
            ));
        }

        if (gbc_palette != null) {
            special_list.append (create_row (
                "gbc-special",
                _("Special (Game Boy Color)"),
                gbc_palette.get_colors ()
            ));
        } else {
            rows["gbc-special"] = rows[DEFAULT_GBC_PALETTE.get_id ()];
        }

        if (sgb_palette != null) {
            char column = 'A' + (char) sgb_palette % 8;
            int row = sgb_palette / 8 + 1;

            special_list.append (create_row (
                "sgb-special",
                _("Special (Super Game Boy, %d-%c)").printf (row, column),
                sgb_palette.get_colors ()
            ));
        } else {
            rows["sgb-special"] = rows[DEFAULT_SGB_PALETTE.get_id ()];
        }

        var manager = CustomPaletteManager.get_instance ();
        custom_palettes = manager.get_palettes ();

        uint n = custom_palettes.get_n_items ();
        for (uint i = 0; i < n; i++) {
            var palette = custom_palettes.get_item (i) as CustomPalette;

            custom_list.insert (create_custom_row (palette), (int) i);
        }

        custom_palettes.items_changed.connect ((pos, removed, added) => {
            for (uint i = 0; i < removed; i++) {
                var row = custom_list.get_row_at_index ((int) (pos + i));
                custom_list.remove (row);
            }

            for (uint i = 0; i < added; i++) {
                var palette = custom_palettes.get_item (pos + i) as CustomPalette;

                custom_list.insert (create_custom_row (palette), (int) (pos + i));
            }
        });

        settings = new GameSettings (game).get_platform ();

        settings.bind ("palette", this, "palette", DEFAULT);

        load_thumbnail.begin ();
    }

    static construct {
        install_action ("palette.add", null, widget => {
            var self = widget as PaletteList;

            self.add_palette ();
        });
    }

    private Gdk.Texture? get_thumbnail_from_runner () {
        var app = GLib.Application.get_default () as Application;
        var window = app.get_window_for_game (game);
        if (window == null)
            return null;

        var runner = window.runner;
        var delegate = runner.delegate as RunnerDelegate;

        var model = delegate.model;
        if (model != GAME_BOY && model != GAME_BOY_POCKET)
            return null;

        return runner.texture;
    }

    private async Gdk.Texture? get_thumbnail_from_snapshots () {
        var manager = new SnapshotManager (game, null, true);

        yield manager.scan ();

        var snapshots = manager.get_snapshots ();

        uint n = snapshots.get_n_items ();
        for (uint i = 0; i < n; i++) {
            var snapshot = snapshots.get_item (i) as Snapshot;
            var metadata = snapshot.platform_metadata as SnapshotMetadata;

            var model = metadata.model;
            if (model != GAME_BOY && model != GAME_BOY_POCKET)
                continue;

            try {
                return snapshot.get_screenshot ();
            } catch (Error e) {
                critical ("Failed to get screenshot: %s", e.message);
            }
        }

        return null;
    }

    private async void load_thumbnail () {
        var texture = get_thumbnail_from_runner ();
        if (texture != null) {
            thumbnail.frame = texture;
            return;
        }

        texture = yield get_thumbnail_from_snapshots ();
        if (texture != null)
            thumbnail.frame = texture;
    }

    private void select_palette (string id) {
        _palette = id;

        update_selected ();

        thumbnail.set_colors (get_colors ());

        notify_property ("palette");
    }

    private int[] get_colors () {
        return Palette.get_colors (game, palette);
    }

    private void update_selected () {
        foreach (var id in rows.get_keys ()) {
            var row = rows[id];

            row.selected = id == palette;
        }
    }

    protected override void map () {
        base.map ();

        if (closing_editor) {
            closing_editor = false;
            return;
        }

        Idle.add_once (() => {
            if (!(palette in rows))
                return;

            var row = rows[palette];

            row.grab_focus ();
        });
    }

    private void add_palette () {
        var palette = new CustomPalette ();

        open_editor (palette, false);
    }

    private void edit_palette (string id) {
        var manager = CustomPaletteManager.get_instance ();
        var palette = manager.get_palette (id);

        open_editor (palette, true);
    }

    private void duplicate_palette (string id) {
        var row = rows[id];

        var palette = new CustomPalette ();

        if (row.is_custom) {
            var manager = CustomPaletteManager.get_instance ();
            var original_palette = manager.get_palette (id);

            palette.name = _("%s (Copy)").printf (original_palette.name);
            palette.set_colors (original_palette.get_colors ());
        } else {
            palette.name = _("%s (Copy)").printf (row.title);

            var colors = Palette.get_colors (game, id);

            if (colors.length == 4) {
                for (int i = 0; i < 2; i++) {
                    colors += colors[0];
                    colors += colors[1];
                    colors += colors[2];
                    colors += colors[3];
                }
            }

            palette.set_colors (colors);
        }

        open_editor (palette, false);
    }

    private void open_editor (CustomPalette palette, bool edit) {
        var editor = new PaletteEditor (palette, thumbnail.frame, edit);

        editor.saved.connect (() => {
            select_palette (palette.id);
            pop ();
        });

        push_page (editor);
    }

    private async void delete_palette (string id) {
        var manager = CustomPaletteManager.get_instance ();
        var palette = manager.get_palette (id);

        var dialog = new Adw.AlertDialog (
            _("Delete “%s”?").printf (palette.name),
            _("Deleted palettes cannot be restored")
        );

        dialog.add_response ("cancel", _("_Cancel"));
        dialog.add_response ("delete", _("_Delete"));

        dialog.set_response_appearance ("delete", DESTRUCTIVE);

        dialog.default_response = "delete";

        if ((yield dialog.choose (this, null)) != "delete")
            return;

        if (this.palette == id)
            reset_palette ();

        manager.remove_palette (palette);
    }

    private void reset_palette () {
        var metadata = game.metadata as Metadata;

        palette = metadata.get_default_palette ();
    }
}
