// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.GameBoy.Palette {
    private struct SimplePaletteInfo {
        int colors[4];
        string id;
        string name;
    }

    private const SimplePaletteInfo[] SIMPLE_PALETTES = {
        {{ 0xFFFFFF, 0xAAAAAA, 0x555555, 0x000000 }, "grayscale", N_("Grayscale") },
        {{ 0x738414, 0x4E6227, 0x3A5031, 0x294139 }, "dmg",       N_("Game Boy") },
        {{ 0x6E8D71, 0x425A41, 0x2A3F28, 0x172813 }, "pocket",    N_("Game Boy Pocket") },
        {{ 0x00D5EA, 0x00AFB3, 0x009A95, 0x00897C }, "light",     N_("Game Boy Light") },
    };

    public enum SimplePalette {
        GRAYSCALE, GAME_BOY, GAME_BOY_POCKET, GAME_BOY_LIGHT;

        public int[] get_colors () {
            return SIMPLE_PALETTES[this].colors;
        }

        public string get_id () {
            return SIMPLE_PALETTES[this].id;
        }

        public string get_name () {
            return SIMPLE_PALETTES[this].name;
        }

        public static SimplePalette[] all () {
            return {
                GRAYSCALE, GAME_BOY, GAME_BOY_POCKET, GAME_BOY_LIGHT,
            };
        }

        public static SimplePalette? parse (string str) {
            for (int i = 0; i < SIMPLE_PALETTES.length; i++) {
                var palette = SIMPLE_PALETTES[i];

                if (str == palette.id)
                    return i;
            }

            return null;
        }
    }

    public bool exists (string str) {
        if (str == "gbc-special" || str == "sgb-special")
            return true;

        var simple = SimplePalette.parse (str);
        if (simple != null)
            return true;

        var gbc = ManualGbcPalette.parse (str);
        if (gbc != null)
            return true;

        var sgb = SgbPalette.parse (str);
        if (sgb != null)
            return true;

        var custom = CustomPaletteManager.get_instance ().get_palette (str);
        if (custom != null)
            return true;

        return false;
    }

    public int[] get_colors (Game game, string str) {
        var metadata = game.metadata as Metadata;

        if (str == "gbc-special") {
            var gbc = metadata.get_special_gbc_palette () ?? DEFAULT_GBC_PALETTE.get_palette ();

            return gbc.get_colors ();
        }

        if (str == "sgb-special") {
            var sgb = metadata.get_special_sgb_palette () ?? DEFAULT_SGB_PALETTE;

            return sgb.get_colors ();
        }

        var simple = SimplePalette.parse (str);
        if (simple != null)
            return simple.get_colors ();

        var gbc = ManualGbcPalette.parse (str);
        if (gbc != null)
            return gbc.get_colors ();

        var sgb = SgbPalette.parse (str);
        if (sgb != null)
            return sgb.get_colors ();

        var custom = CustomPaletteManager.get_instance ().get_palette (str);
        if (custom != null)
            return custom.get_colors ();

        return SimplePalette.GRAYSCALE.get_colors ();
    }

    public inline bool get_has_obj0_colors (int[] colors) {
        if (colors.length == 4)
            return false;

        return colors[0] != colors[4] ||
               colors[1] != colors[5] ||
               colors[2] != colors[6] ||
               colors[3] != colors[7];
    }

    public inline bool get_has_obj1_colors (int[] colors) {
        if (colors.length == 4)
            return false;

        return colors[0] != colors[8] ||
               colors[1] != colors[9] ||
               colors[2] != colors[10] ||
               colors[3] != colors[11];
    }

    public inline bool get_obj0_obj1_differ (int[] colors) {
        if (colors.length == 4)
            return false;

        return colors[4] != colors[8] ||
               colors[5] != colors[9] ||
               colors[6] != colors[10] ||
               colors[7] != colors[11];
    }

    public inline bool get_uses_sprite_colors (int[] colors) {
        return get_has_obj0_colors (colors) ||
               get_has_obj1_colors (colors);
    }
}
