// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.GameBoy.PalettePreview : Gtk.Widget {
    private const float SPRITE_HEIGHT = 8;

    private int[] colors;

    construct {
        overflow = HIDDEN;
    }

    static construct {
        set_css_name ("palette-preview");
    }

    private int[] get_display_colors (int[] colors) {
        int[] ret = {};

        // Always have bg colors
        for (int i = 0; i < 4; i++)
            ret += colors[i];

        if (Palette.get_has_obj0_colors (colors)) {
            for (int i = 4; i < 8; i++)
                ret += colors[i];
        }

        if (Palette.get_has_obj1_colors (colors) && Palette.get_obj0_obj1_differ (colors)) {
            for (int i = 8; i < 12; i++)
                ret += colors[i];
        }

        return ret;
    }

    public void set_colors (owned int[] colors) {
        this.colors = get_display_colors (colors);
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        if (colors == null)
            return;

        int w = get_width ();
        int h = get_height ();

        for (int i = 0; i < colors.length; i++) {
            var rgba = ColorUtils.color_to_rgba (colors[i % colors.length]);

            int col = i % 4;
            int row = i / 4;
            int rows = colors.length / 4;

            float x = w * col / 4.0f;
            float width = w * 1.0f / 4.0f;
            float y, height;

            if (row == 0) {
                y = 0;
                height = h - (SPRITE_HEIGHT * (rows - 1));
            } else {
                y = h - SPRITE_HEIGHT * (rows - row);
                height = SPRITE_HEIGHT;
            }

            snapshot.append_color (rgba, {{ x, y }, { width, height }});
        }
    }
}

[GtkTemplate (ui = "/app/drey/Highscore/platforms/game-boy/palette/game-boy-palette-row.ui")]
private class Highscore.GameBoy.PaletteRow : Gtk.ListBoxRow {
    public signal void activated ();
    public signal void edit_palette ();
    public signal void duplicate_palette ();
    public signal void delete_palette ();

    public string id { get; construct; }
    public string title { get; set; }
    public bool is_custom { get; construct; }
    public bool selected { get; set; }
    public bool has_popup { get; set; }

    [GtkChild]
    private unowned Gtk.Image checkmark;
    [GtkChild]
    private unowned PalettePreview preview;

    public PaletteRow (string id, string title, owned int[] colors, bool is_custom) {
        Object (id: id, title: title, is_custom: is_custom);

        assert (colors != null);
        assert (colors.length == 4 || colors.length == 8 || colors.length == 12);

        preview.set_colors (colors);
    }

    construct {
        notify["selected"].connect (() => {
            checkmark.opacity = selected ? 1 : 0;
        });

        notify["has-popup"].connect (() => {
            if (has_popup)
                add_css_class ("has-open-popup");
            else
                remove_css_class ("has-open-popup");
        });

        action_set_enabled ("palette.edit", is_custom);
        action_set_enabled ("palette.delete", is_custom);
    }

    static construct {
        install_action ("palette.edit", null, widget => {
            var self = widget as PaletteRow;

            self.edit_palette ();
        });
        install_action ("palette.duplicate", null, widget => {
            var self = widget as PaletteRow;

            self.duplicate_palette ();
        });
        install_action ("palette.delete", null, widget => {
            var self = widget as PaletteRow;

            self.delete_palette ();
        });
    }

    protected override void map () {
        base.map ();

        var parent = get_parent ();

        assert (parent is Gtk.ListBox);

        var list = parent as Gtk.ListBox;
        list.row_activated.connect (row_activated);
    }

    protected override void unmap () {
        var parent = get_parent ();

        assert (parent is Gtk.ListBox);

        var list = parent as Gtk.ListBox;
        list.row_activated.disconnect (row_activated);

        base.unmap ();
    }

    private void row_activated (Gtk.ListBoxRow row) {
        if (row == this)
            activated ();
    }

    public void set_colors (owned int[] colors) {
        preview.set_colors (colors);
    }
}
