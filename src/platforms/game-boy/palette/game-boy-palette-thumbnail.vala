// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public class Highscore.GameBoy.PaletteThumbnail : Gtk.GLArea {
    private Gdk.Texture? _frame;
    public Gdk.Texture? frame {
        get { return _frame; }
        set {
            _frame = value;
            frame_changed = true;
            queue_render ();
        }
    }

    private bool frame_changed;
    private bool context_recreated;

    private GLVertexArray vertex_array;
    private GLVertexBuffer vertex_buffer;
    private GLElementBuffer element_buffer;
    private GLShader shader;
    private GLTexture texture;

    private Graphene.Vec3 shader_colors[12];

    construct {
        width_request = 160;
        height_request = 144;

        auto_render = false;
    }

    static construct {
        set_css_name ("palette-thumbnail");
    }

    public override void css_changed (Gtk.CssStyleChange change) {
        base.css_changed (change);

        queue_render ();
    }

    public override void realize () {
        base.realize ();

        make_current ();

        if (get_error () != null)
            return;

        vertex_array = new GLVertexArray ();
        vertex_array.bind ();

        vertex_buffer = new GLVertexBuffer ();
        vertex_buffer.bind ();
        vertex_buffer.set_data ({
             0,  1,
             1,  1,
             1,  0,
             0,  0,
        });

        element_buffer = new GLElementBuffer ();
        element_buffer.bind ();
        element_buffer.set_data ({
            0, 1, 2,
            2, 3, 0
        });

        texture = new GLTexture ();

        var shared = ResourceUtils.load_to_string (
            "/app/drey/Highscore/runner/screen/platform-shared.glsl"
        );

        var main = ResourceUtils.load_to_string (
            "/app/drey/Highscore/platforms/game-boy/game-boy.glsl"
        );

        try {
            shader = new GLShader (context, @"$shared\n\n$main");
        } catch (GLShaderError e) {
            error ("Error in Game Boy shader: %s", e.message);
        }

        context_recreated = true;

        get_native ().get_surface ().notify["scale"].connect (queue_render);
    }

    public override void unrealize () {
        get_native ().get_surface ().notify["scale"].disconnect (queue_render);

        make_current ();

        if (get_error () != null) {
            base.unrealize ();
            return;
        }

        vertex_array = null;
        vertex_buffer = null;
        element_buffer = null;
        shader = null;
        texture = null;

        base.unrealize ();
    }

    public override bool render (Gdk.GLContext context) {
        var error = get_error ();
        if (error != null)
            critical (error.message);

        // Use background color from the palette in case the image isn't set
        glClearColor (
            shader_colors[3].get_x (),
            shader_colors[3].get_y (),
            shader_colors[3].get_z (),
            1.0f
        );

        glClear (GL_COLOR_BUFFER_BIT);

        draw_frame ();

        glFlush ();

        return Gdk.EVENT_STOP;
    }

    private void draw_frame () {
        if (frame == null)
            return;

        if (frame_changed || context_recreated) {
            texture.bind ();
            texture.upload (frame);
            texture.unbind ();
        }

        int scale = (int) Math.ceil (get_native ().get_surface ().scale);

        glViewport (
            0, 0,
            get_width () * scale,
            get_height () * scale
        );

        shader.bind ();
        shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

        texture.bind ();
        GLUtils.set_active_texture (0);
        texture.set_params (NEAREST, NEAREST, EDGE);
        shader.set_uniform_int ("u_source", 0);

        Graphene.Matrix mvp = {};
        mvp.init_identity ();
        mvp.scale (2, -2, 1);
        mvp.translate ({ -1, 1, 0 });

        shader.set_uniform_vec2 ("u_screenPosition", Graphene.Vec2.zero ());
        shader.set_uniform_vec2 ("u_screenSize", Graphene.Vec2.one ());
        shader.set_uniform_bool ("u_flipped", false);
        shader.set_uniform_matrix ("u_mvp", mvp);

        shader.set_uniform_bool ("u_recolor", true);
        shader.set_uniform_vec3_array ("u_palette", shader_colors);

        element_buffer.draw ();

        texture.unbind ();
        shader.unbind ();

        frame_changed = false;
        context_recreated = false;
    }

    public void set_colors (owned int[] colors) {
        for (int i = 0; i < colors.length; i++) {
            var vec = ColorUtils.rgb_to_vec3 (colors[i]);

            shader_colors[i] = vec;

            if (colors.length == 4) {
                shader_colors[i + 4] = vec;
                shader_colors[i + 8] = vec;
            }
        }

        queue_render ();
    }
}
