// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameBoy.SnapshotMetadata : SnapshotPlatformMetadata {
    public Hs.GameBoyModel model { get; set; }

    public override void load (KeyFile keyfile) {
        try {
            var model_str = keyfile.get_string ("Game Boy", "Model");

            model = Model.from_string (model_str);
        } catch (Error e) {
            model = GAME_BOY;
        }
    }

    public override void save (KeyFile keyfile) {
        keyfile.set_string ("Game Boy", "Model", Model.to_string (model));
    }
}
