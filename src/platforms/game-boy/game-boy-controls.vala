// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameBoy.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("game-boy", _("Game Boy"));
        controller.add_sections ("dpad", "face-buttons", "menu", null);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("Control Pad"),
            Hs.GameBoyButton.UP,
            Hs.GameBoyButton.DOWN,
            Hs.GameBoyButton.LEFT,
            Hs.GameBoyButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.GameBoyButton.A,
            "b", _("B Button"), Hs.GameBoyButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.GameBoyButton.SELECT,
            "start",  _("Start"),  Hs.GameBoyButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "a");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("dpad");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "dpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "game-boy";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->game_boy.buttons, button, pressed);
        });
    }
}
