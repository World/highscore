// Author of the color correction function: hunterk, Pokefan531
// Source: https://github.com/libretro/slang-shaders/blob/master/handheld/shaders/color/gbc-color.slang
// Source: https://github.com/libretro/slang-shaders/blob/master/handheld/shaders/color/gba-color.slang
// License: Public domain

#ifdef VERTEX

void hs_main() {}

#else // FRAGMENT

uniform bool u_recolor;
uniform bool u_correctColors;
uniform bool u_isGBA;
uniform vec3 u_palette[12];

#define gbc_lighten_intensity 0.5 /* 0 to 1 */
#define gba_darken_intensity 0.5 /* -0.25 to 1 */
#define target_gamma_gbc 2.2
#define display_gamma_gbc 2.2
#define target_gamma_gba 2.0
#define display_gamma_gba 2.0

vec4 gbc_correct_color(vec4 rgba) {
  mat4 profile = mat4(
      0.80,  0.135, 0.195, 0.0, //red
      0.275, 0.64,  0.155, 0.0, //green
     -0.075, 0.225, 0.65,  0.0, //blue
      0.0,   0.0,   0.0,   0.93 //alpha
  );

  // bring out our stored luminance value
  float lum = profile[3].w;

  float display_gamma;
  vec4 screen;

  if (u_isGBA) {
    screen = pow(rgba, vec4(target_gamma_gba + gba_darken_intensity)).rgba;
    display_gamma = display_gamma_gba;
  } else {
    screen = pow(rgba, vec4(target_gamma_gbc - gbc_lighten_intensity)).rgba;
    display_gamma = display_gamma_gbc;
  }

  screen = clamp(screen * lum, 0.0, 1.0);
  screen = profile * screen;

  return pow(screen, vec4(1.0 / display_gamma));
}

vec4 gb_apply_palette(vec4 rgba) {
  int r = int(rgba.r * 255.0);
  int g = int(rgba.g * 255.0);
  int b = int(rgba.b * 255.0);

  int layer, brightness;

  if (b > 0)
    layer = 0;
  else if (r > 0)
    layer = 1;
  else
    layer = 2;

  if (layer == 2)
    brightness = g;
  else
    brightness = r;

  if (brightness == 0xFF)
    return vec4(u_palette[layer * 4], 1);

  if (brightness == 0xC6)
    return vec4(u_palette[layer * 4 + 1], 1);

  if (brightness == 0x84)
    return vec4(u_palette[layer * 4 + 2], 1);

  if (brightness == 0x42)
    return vec4(u_palette[layer * 4 + 3], 1);

  return vec4(1, 0, 1, 1);
}

vec4 hs_main() {
  vec4 rgba = texture(u_source, v_texCoord);

  if (u_recolor)
    rgba = gb_apply_palette(rgba);

  if (u_correctColors)
    rgba = gbc_correct_color(rgba);

  return rgba;
}

#endif