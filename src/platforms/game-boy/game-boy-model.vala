// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.GameBoy.Model {
    public Hs.GameBoyModel from_string (string str) {
        if (str == "game-boy" || str == "Game Boy")
            return GAME_BOY;
        if (str == "game-boy-pocket" || str == "Game Boy Pocket")
            return GAME_BOY_POCKET;
        if (str == "game-boy-color" || str == "Game Boy Color")
            return GAME_BOY_COLOR;
        if (str == "game-boy-advance" || str == "Game Boy Advance")
            return GAME_BOY_ADVANCE;
        if (str == "super-game-boy" || str == "Super Game Boy")
            return SUPER_GAME_BOY;
        if (str == "super-game-boy-2" || str == "Super Game Boy 2")
            return SUPER_GAME_BOY_2;

        critical ("Unknown Game Boy model: %s", str);
        return GAME_BOY;
    }

    public string to_string (Hs.GameBoyModel model) {
        switch (model) {
            case GAME_BOY:
                return "game-boy";
            case GAME_BOY_POCKET:
                return "game-boy-pocket";
            case GAME_BOY_COLOR:
                return "game-boy-color";
            case GAME_BOY_ADVANCE:
                return "game-boy-advance";
            case SUPER_GAME_BOY:
                return "super-game-boy";
            case SUPER_GAME_BOY_2:
                return "super-game-boy-2";
            default:
                assert_not_reached ();
        }
    }

    public string to_display_name (Hs.GameBoyModel model) {
        switch (model) {
            case GAME_BOY:
                return _("Game Boy");
            case GAME_BOY_POCKET:
                return _("Game Boy Pocket");
            case GAME_BOY_COLOR:
                return _("Game Boy Color");
            case GAME_BOY_ADVANCE:
                return _("Game Boy Advance");
            case SUPER_GAME_BOY:
                return _("Super Game Boy");
            case SUPER_GAME_BOY_2:
                return _("Super Game Boy 2");
            default:
                assert_not_reached ();
        }
    }

    public Hs.GameBoyModel get_auto_for_game (Game game) {
        var metadata = game.metadata as Metadata;

        if (metadata.supports_gbc) {
            if (metadata.is_gba_enhanced ())
                return GAME_BOY_ADVANCE;

            return GAME_BOY_COLOR;
        }

        if (metadata.supports_sgb) {
            if (metadata.is_sgb2_enhanced ())
                return SUPER_GAME_BOY_2;

            return SUPER_GAME_BOY;
        }

        return GAME_BOY;
    }
}
