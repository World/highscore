// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameBoy.ScreenSet : Highscore.ScreenSet {
    public const int[] SOURCE_PALETTE = {
        0xFFFFFF, 0xC6C6C6, 0x848484, 0x424242,
        0xFF0000, 0xC60000, 0x840000, 0x420000,
        0x00FF00, 0x00C600, 0x008400, 0x004200,
    };

    public const int SGB_WITH_BORDER = 1;
    public const int SGB_WITHOUT_BORDER = 2;

    public const int SGB_SCREEN_WIDTH = 256;
    public const int SGB_SCREEN_HEIGHT = 224;
    public const int SGB_BORDER_WIDTH = 48;
    public const int SGB_BORDER_HEIGHT = 40;

    private const float SGB_ASPECT_RATIO = 256.0f / 224.0f * 8.0f / 7.0f;

    public string palette { get; set; }
    public Hs.GameBoyModel model { get; set; }
    public bool sgb_border { get; set; }

    public bool is_accurate { get; set; }

    private GameSettings game_settings;
    private Settings platform_settings;
    private Graphene.Vec3 shader_colors[12];

    private CustomPalette custom_palette;

    construct {
        notify["palette"].connect (() => {
            if (custom_palette != null) {
                custom_palette.changed.disconnect (palette_changed);
                custom_palette.removed.disconnect (palette_removed);
            }

            custom_palette = CustomPaletteManager.get_instance ().get_palette (palette);

            if (custom_palette != null) {
                custom_palette.changed.connect (palette_changed);
                custom_palette.removed.connect (palette_removed);
            }

            palette_changed ();
            screen_type_changed ();
        });

        notify["model"].connect (() => {
            update_layout ();
            filter_changed ();
            screen_type_changed ();
        });

        notify["sgb-border"].connect (() => {
            update_layout ();
            screen_type_changed ();
        });

        game_settings = new GameSettings (game);
        game_settings.bind_property ("is-accurate-filter", this, "is-accurate", SYNC_CREATE);

        platform_settings = game_settings.get_platform ();
        platform_settings.bind ("palette", this, "palette", DEFAULT);
        platform_settings.bind ("sgb-border", this, "sgb-border", GET);

        notify["is-accurate"].connect (palette_changed);

        if (snapshot != null) {
            var metadata = snapshot.platform_metadata as SnapshotMetadata;
            model = metadata.model;

            if (runner != null) {
                var delegate = runner.delegate as RunnerDelegate;

                delegate.bind_property ("model", this, "model", DEFAULT);
            }
        } else if (runner != null) {
            var delegate = runner.delegate as RunnerDelegate;

            delegate.bind_property ("model", this, "model", SYNC_CREATE);
        }
    }

    ~ScreenSet () {
        if (custom_palette != null) {
            custom_palette.changed.disconnect (palette_changed);
            custom_palette.removed.disconnect (palette_removed);
        }
    }

    private void palette_changed () {
        var effective_palette = palette;

        if (is_accurate) {
            if (mode == DISPLAY && (palette == "dmg" || palette == "pocket" || palette == "light"))
                effective_palette = "grayscale";

            if (mode != DISPLAY && palette == "grayscale")
                effective_palette = "dmg";
        }

        var colors = Palette.get_colors (game, effective_palette);

        for (int i = 0; i < colors.length; i++) {
            var vec = ColorUtils.rgb_to_vec3 (colors[i]);

            shader_colors[i] = vec;

            if (colors.length == 4) {
                shader_colors[i + 4] = vec;
                shader_colors[i + 8] = vec;
            }
        }

        filter_changed ();
    }

    private void palette_removed () {
        var metadata = game.metadata as Metadata;
        palette = metadata.get_default_palette ();
    }

    private void update_layout () {
        if (model == SUPER_GAME_BOY || model == SUPER_GAME_BOY_2) {
            layout = new SingleLayout (
                sgb_border ? SGB_WITH_BORDER : SGB_WITHOUT_BORDER
            );
        } else {
            layout = new SingleLayout (DEFAULT_SCREEN);
        }

        layout_changed (false);
    }

    public override Screen[] get_screens () {
        float small_x = (float) SGB_BORDER_WIDTH / SGB_SCREEN_WIDTH;
        float small_y = (float) SGB_BORDER_HEIGHT / SGB_SCREEN_HEIGHT;
        float small_w = (float) (SGB_SCREEN_WIDTH - SGB_BORDER_WIDTH * 2) / SGB_SCREEN_WIDTH;
        float small_h = (float) (SGB_SCREEN_HEIGHT - SGB_BORDER_HEIGHT * 2) / SGB_SCREEN_HEIGHT;

        var whole_screen  = new Screen (DEFAULT_SCREEN, {{ 0, 0 }, { 1, 1 }});
        var sgb_with_border = new Screen (
            SGB_WITH_BORDER, {{ 0, 0 }, { 1, 1 }}, SGB_ASPECT_RATIO
        );
        var sgb_without_border = new Screen (SGB_WITHOUT_BORDER, {
            { small_x, small_y }, { small_w, small_h }
        });

        return ({ whole_screen, sgb_with_border, sgb_without_border });
    }

    public override string get_filter_path () {
        return "/app/drey/Highscore/platforms/game-boy/game-boy.glsl";
    }

    public override void setup_filter (GLShader shader, int screen_id) {
        bool has_palettes = model == GAME_BOY || model == GAME_BOY_POCKET;
        bool is_gbc = model == GAME_BOY_COLOR;
        bool is_gba = model == GAME_BOY_ADVANCE;

        shader.set_uniform_bool ("u_recolor", has_palettes);
        shader.set_uniform_bool ("u_correctColors", is_gbc || is_gba);
        shader.set_uniform_bool ("u_isGBA", is_gba);
        shader.set_uniform_vec3_array ("u_palette", shader_colors);
    }

    public override ScreenType get_screen_type (Hs.Region region) {
        bool is_sgb = model == SUPER_GAME_BOY || model == SUPER_GAME_BOY_2;
        bool is_gbc = model == GAME_BOY_COLOR;
        bool is_gba = model == GAME_BOY_ADVANCE;

        if (is_sgb && sgb_border)
            return NTSC_SUPER_NES;

        if (is_gba)
            return GAME_BOY_ADVANCE;

        if (is_gbc || is_sgb)
            return GAME_BOY_COLOR;

        if (palette == "grayscale" || palette == "dmg")
            return GAME_BOY_DMG;

        if (palette == "pocket")
            return GAME_BOY_POCKET;

        if (palette == "light")
            return GAME_BOY_LIGHT;

        return GAME_BOY_COLOR;
    }
}
