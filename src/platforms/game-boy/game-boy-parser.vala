// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://gbdev.io/pandocs/The_Cartridge_Header.html
public class Highscore.GameBoy.Parser : GameParser {
    private const size_t TITLE_OFFSET = 0x134;
    private const size_t TITLE_LENGTH = 0x10;

    private const uint8 GBC_SUPPORTED = 0x80;
    private const uint8 GBC_REQUIRED = 0xC0;

    private const size_t NEW_LICENSEE_LENGTH = 2;
    private const string NEW_LICENSEE_NINTENDO = "01";

    private const uint8 SGB_SUPPORTED = 0x03;

    // Relative offset after reading the SGB features byte
    private const size_t OLD_LICENSEE_OFFSET = 4;
    private const uint8 OLD_LICENSEE_POST_SGB = 0x33;
    private const uint8 OLD_LICENSEE_NINTENDO = 0x01;

    private const uint8[] MANUFACTURER_CODE_PREFIXES = { 'A', 'B', 'K', 'V' };
    private const uint8[] MANUFACTURER_CODE_SUFFIXES = {
        'D', 'E', 'F', 'H', 'I', 'J', 'K', 'P', 'S', 'X', 'Y'
    };

    private string title;
    private bool supports_sgb;
    private bool supports_gbc;
    private bool requires_gbc;
    private bool is_nintendo;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    public override bool parse () throws Error {
        var stream = new DataInputStream (file.read ());

        stream.seek (TITLE_OFFSET, SET);

        var title_buffer = new uint8[TITLE_LENGTH];
        stream.read (title_buffer);

        // The last 5 bytes of the title can also be used for other purposes:
        // GBC feature and manufacturer code. If possible, we want to exclude
        // them from the title
        uint8 gbc_features = title_buffer[0xF];
        if (gbc_features == GBC_SUPPORTED || gbc_features == GBC_REQUIRED) {
            // Try our best to guess if the last 4 bytes are the manufacturer
            // code rather than a part of the title, and cut it off
            if (title_buffer[TITLE_LENGTH - 5] in MANUFACTURER_CODE_PREFIXES &&
                title_buffer[TITLE_LENGTH - 2] in MANUFACTURER_CODE_SUFFIXES) {
                title_buffer[TITLE_LENGTH - 5] = 0;
            } else {
                // Otherwise, just cut off the GBC features byte
                title_buffer[TITLE_LENGTH - 1] = 0;
            }
        }

        var new_licensee = new uint8[NEW_LICENSEE_LENGTH];
        stream.read (new_licensee);

        uint8 sgb_features = stream.read_byte ();

        stream.seek (OLD_LICENSEE_OFFSET, CUR);
        uint8 old_licensee = stream.read_byte ();

        bool new_licensee_is_nintendo = Posix.strncmp (
            NEW_LICENSEE_NINTENDO,
            (string) new_licensee,
            NEW_LICENSEE_LENGTH
        ) == 0;

        var title_builder = new StringBuilder ();
        title_builder.append_len ((string) title_buffer, (ssize_t) TITLE_LENGTH);
        title = title_builder.free_and_steal ();

        supports_sgb = sgb_features == SGB_SUPPORTED &&
                       old_licensee == OLD_LICENSEE_POST_SGB;
        supports_gbc = gbc_features == GBC_SUPPORTED ||
                       gbc_features == GBC_REQUIRED;
        requires_gbc = gbc_features == GBC_REQUIRED;
        is_nintendo = old_licensee == OLD_LICENSEE_NINTENDO ||
                      new_licensee_is_nintendo;

        parser_debug (
            "Found metadata: title '%s'%s%s%s%s", title,
            supports_sgb ? ", SGB" : "",
            supports_gbc ? ", GBC" : "",
            requires_gbc ? ", GBC-only" : "",
            is_nintendo ? ", first-party" : ""
        );

        return true;
    }

    public override GameMetadata? get_metadata () throws Error {
        return new Metadata (
            title, supports_sgb, supports_gbc, requires_gbc, is_nintendo
        );
    }
}
