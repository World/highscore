// This file is part of Highscore. License: GPL-3.0-or-later

// Documentation: https://www.planetvb.com/content/downloads/documents/stsvb.html
public class Highscore.VirtualBoy.Parser : GameParser {
    private const size_t MAGIC_OFFSET = 0x20C;
    private const uint8[] MAGIC_VALUE = { 0, 0, 0, 0, 0 };

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    public override bool parse () throws Error {
        FileInputStream stream = file.read ();

        var info = file.query_info (FileAttribute.STANDARD_SIZE, FileQueryInfoFlags.NONE);
        int64 size = info.get_size ();

        ssize_t read = 0;

        if (size < MAGIC_OFFSET) {
            parser_debug ("File is too small, discarding");
            return false;
        }

        stream.seek (size - MAGIC_OFFSET, SeekType.SET);

        var buffer = new uint8[MAGIC_VALUE.length];
        read = stream.read (buffer);

        if (read < MAGIC_VALUE.length) {
            parser_debug ("Failed to read magic number");
            return false;
        }

        for (var i = 0; i < MAGIC_VALUE.length; i++) {
            if (buffer[i] != MAGIC_VALUE[i]) {
                parser_debug ("Wrong magic number");
                return false;
            }
        }

        parser_debug ("Success");
        return true;
    }
}
