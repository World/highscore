// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.VirtualBoy.RunnerDelegate : Highscore.RunnerDelegate {
    public StereoMode stereo_mode { get; set; }
    public bool gray_colors { get; set; }

    private Settings settings;

    construct {
        settings = new Settings ("app.drey.Highscore.platforms.virtual-boy");

        StereoMode.bind_settings (
            settings, "stereo-mode", this, "stereo-mode", DEFAULT
        );

        settings.bind ("gray-colors", this, "gray-colors", DEFAULT);
    }

    public override Gtk.Widget? create_header_widget (FullscreenView view) {
        var switcher = new StereoSwitcher ();

        switcher.track_fullscreen (view);

        bind_property ("stereo-mode", switcher, "stereo-mode", SYNC_CREATE | BIDIRECTIONAL);
        bind_property ("gray-colors", switcher, "gray-colors", SYNC_CREATE | BIDIRECTIONAL);

        return switcher;
    }

    public override OverlayMenuAddin? create_overlay_menu () {
        var menu = new OverlayMenu ();

        bind_property ("stereo-mode", menu, "stereo-mode", SYNC_CREATE | BIDIRECTIONAL);
        bind_property ("gray-colors", menu, "gray-colors", SYNC_CREATE | BIDIRECTIONAL);

        return menu;
    }
}
