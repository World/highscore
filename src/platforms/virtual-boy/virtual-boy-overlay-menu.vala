// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.VirtualBoy.OverlayMenu : OverlayMenuAddin {
    public StereoMode stereo_mode { get; set; }
    public bool gray_colors { get; set; }

    private SimpleAction gray_colors_action;

    public override string get_action_prefix () {
        return "vb";
    }

    construct {
        menu_model = ResourceUtils.load_menu (
            "/app/drey/Highscore/platforms/virtual-boy/virtual-boy-overlay-menu.ui"
        );

        add_action (new PropertyAction ("stereo-mode", this, "stereo-mode"));

        gray_colors_action = new SimpleAction.stateful (
            "gray-colors", null, gray_colors
        );
        gray_colors_action.change_state.connect (state => {
            gray_colors = state.get_boolean ();
        });
        add_action (gray_colors_action);

        notify["stereo-mode"].connect (update_actions);
        notify["gray-colors"].connect (() => {
            gray_colors_action.set_state (gray_colors);
        });

        update_actions ();
    }

    private void update_actions () {
        gray_colors_action.set_enabled (
            stereo_mode == LEFT_ONLY ||
            stereo_mode == RIGHT_ONLY ||
            stereo_mode == SIDE_BY_SIDE
        );
    }
}
