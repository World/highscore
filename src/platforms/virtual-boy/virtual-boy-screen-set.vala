// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.VirtualBoy.ScreenSet : Highscore.ScreenSet {
    public const int LEFT_ID = 0;
    public const int RIGHT_ID = 1;

    public StereoMode stereo_mode { get; set; }
    public bool gray_colors { get; set; }

    private Settings settings;

    construct {
        notify["stereo-mode"].connect (() => update_layout ());
        notify["gray-colors"].connect (() => filter_changed ());

        update_layout ();

        settings = new Settings ("app.drey.Highscore.platforms.virtual-boy");

        StereoMode.bind_settings (
            settings, "stereo-mode", this, "stereo-mode", GET
        );

        settings.bind ("gray-colors", this, "gray-colors", GET);
    }

    private void update_layout () {
        if (stereo_mode == SIDE_BY_SIDE)
            layout = new HorizontalLayout ();
        else
            layout = new SingleLayout (LEFT_ID);

        layout_changed (false);
        filter_changed ();
    }

    public override Screen[] get_screens () {
        var left_screen  = new Screen (LEFT_ID,  {{ 0, 0 }, { 1, 1 }});
        var right_screen = new Screen (RIGHT_ID, {{ 0, 0 }, { 1, 1 }});

        return ({ left_screen, right_screen });
    }

    public override string get_filter_path () {
        return "/app/drey/Highscore/platforms/virtual-boy/virtual-boy.glsl";
    }

    public override void setup_filter (GLShader shader, int screen_id) {
        var matrix = get_color_matrix (screen_id);

        shader.set_uniform_matrix ("u_colorMatrix", matrix);
    }

    public override ScreenType get_screen_type (Hs.Region region) {
        // TODO find a filter for VB
        return UNKNOWN;
    }

    private Graphene.Matrix get_color_matrix (int screen_id) {
        Graphene.Matrix matrix = {};

        float r = 1;
        float g = gray_colors ? 1 : 0;
        float b = gray_colors ? 1 : 0;

        float r1 = 0, g1 = 0, b1 = 0, r2 = 0, g2 = 0, b2 = 0;
        if (stereo_mode == LEFT_ONLY) {
            r1 = r;
            g1 = g;
            b1 = b;
        } else if (stereo_mode == RIGHT_ONLY) {
            r2 = r;
            g2 = g;
            b2 = b;
        } else if (stereo_mode == RED_BLUE) {
            r1 = 1;
            b2 = 1;
        } else if (stereo_mode == RED_CYAN) {
            r1 = 1;
            g2 = 1;
            b2 = 1;
       } else {
            if (screen_id == ScreenSet.RIGHT_ID) {
                r2 = r;
                g2 = g;
                b2 = b;
            } else {
                r1 = r;
                g1 = g;
                b1 = b;
            }
        }

        matrix.init_from_float ({
            r1, g1, b1, 0,
            0,  0,  0,  0,
            r2, g2, b2, 0,
            0,  0,  0,  1,
        });

        return matrix;
    }
}
