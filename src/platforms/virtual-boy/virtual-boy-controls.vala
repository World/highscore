// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.VirtualBoy.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("virtual-boy", _("Virtual Boy"));
        controller.add_sections (
            "ldpad", "rdpad", "face-buttons", "shoulder-buttons", "menu", null
        );

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "ldpad", _("Left Control Pad"),
            Hs.VirtualBoyButton.L_UP,
            Hs.VirtualBoyButton.L_DOWN,
            Hs.VirtualBoyButton.L_LEFT,
            Hs.VirtualBoyButton.L_RIGHT
        );
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "rdpad", _("Right Control Pad"),
            Hs.VirtualBoyButton.R_UP,
            Hs.VirtualBoyButton.R_DOWN,
            Hs.VirtualBoyButton.R_LEFT,
            Hs.VirtualBoyButton.R_RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "face-buttons",
            "a", _("A Button"), Hs.VirtualBoyButton.A,
            "b", _("B Button"), Hs.VirtualBoyButton.B,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "shoulder-buttons",
            "l", _("L Button"), Hs.VirtualBoyButton.L,
            "r", _("R Button"), Hs.VirtualBoyButton.R,
            null
        );
        ControlHelpers.add_buttons (
            controller, set_pressed, "menu",
            "select", _("Select"), Hs.VirtualBoyButton.SELECT,
            "start",  _("Start"),  Hs.VirtualBoyButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "ldpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "ldpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "ldpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "ldpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,         "rdpad:up");
        kbd_mapping.map (Linux.Input.KEY_S,         "rdpad:down");
        kbd_mapping.map (Linux.Input.KEY_A,         "rdpad:left");
        kbd_mapping.map (Linux.Input.KEY_D,         "rdpad:right");
        kbd_mapping.map (Linux.Input.KEY_Z,         "b");
        kbd_mapping.map (Linux.Input.KEY_X,         "a");
        kbd_mapping.map (Linux.Input.KEY_Q,         "l");
        kbd_mapping.map (Linux.Input.KEY_E,         "r");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_dpad   ("ldpad");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (L,      "l");
        gamepad_mapping.map_button (R,      "r");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_stick  (LEFT,   "ldpad");
        gamepad_mapping.map_stick  (RIGHT,  "rdpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "virtual-boy";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->virtual_boy.buttons, button, pressed);
        });
    }
}
