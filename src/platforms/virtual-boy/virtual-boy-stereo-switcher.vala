// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/platforms/virtual-boy/virtual-boy-stereo-switcher.ui")]
public class Highscore.VirtualBoy.StereoSwitcher : Adw.Bin {
    public StereoMode stereo_mode { get; set; }
    public bool gray_colors { get; set; }

    [GtkChild]
    private unowned Gtk.MenuButton button;

    construct {
        notify["stereo-mode"].connect (() => {
            action_set_enabled (
                "vb.gray-colors",
                stereo_mode == LEFT_ONLY ||
                stereo_mode == RIGHT_ONLY ||
                stereo_mode == SIDE_BY_SIDE
            );
        });
    }

    static construct {
        install_property_action ("vb.stereo-mode", "stereo-mode");
        install_property_action ("vb.gray-colors", "gray-colors");
    }

    public void track_fullscreen (FullscreenView view) {
        view.track_menu_button (button);
    }
}
