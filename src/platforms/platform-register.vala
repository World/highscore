// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.PlatformRegister : Object {
    private static PlatformRegister instance;
    private HashTable<string, Platform> platforms;

    private PlatformRegister () {
        platforms = new HashTable<string, Platform> (str_hash, str_equal);

        init_platforms ();
    }

    public static PlatformRegister get_register () {
        if (instance == null)
            instance = new PlatformRegister ();

        return instance;
    }

    public void add_platform (Platform platform) {
        assert (!platforms.contains (platform.id));

        platforms[platform.id] = platform;
    }

    public List<Platform> get_all_platforms () {
        var values = platforms.get_values ();

        var result = new List<Platform> ();
        foreach (var platform in values)
            result.prepend (platform);

        result.sort (Platform.compare);

        return result;
    }

    public Platform? get_platform (string id) {
        return platforms[id];
    }

    public List<Platform> list_platforms_for_mime_type (string mime) {
        var values = platforms.get_values ();
        var result = new List<Platform> ();

        foreach (var platform in values) {
            var mime_types = platform.get_mime_types ();

            if (mime in mime_types)
                result.prepend (platform);
        }

        result.sort (Platform.compare);

        return result;
    }

    public Gee.TreeSet<string> list_mime_types () {
        var values = platforms.get_values ();
        var ret = new Gee.TreeSet<string> ();

        foreach (var platform in values) {
            var mime_types = platform.get_mime_types ();

            ret.add_all_array (mime_types);
        }

        return ret;
    }

    private void init_platforms () {
        var platform = new Platform (
            ATARI_2600,
            "atari-2600",
            _("Atari 2600"),
            { "application/x-atari-2600-rom" },
            { "a26" }
        );
        platform.controls_type = typeof (Atari2600.Controls);
        platform.runner_delegate_type = typeof (Atari2600.RunnerDelegate);
        platform.screen_set_type = typeof (Atari2600.ScreenSet);
        platform.snapshot_metadata_type = typeof (Atari2600.SnapshotMetadata);
        add_platform (platform);

        platform = new Platform (
            ATARI_7800,
            "atari-7800",
            _("Atari 7800"),
            { "application/x-atari-7800-rom" },
            { "a78" }
        );
        platform.controls_type = typeof (Atari7800.Controls);
        platform.runner_delegate_type = typeof (Atari7800.RunnerDelegate);
        platform.screen_set_type = typeof (Atari7800.ScreenSet);
        platform.snapshot_metadata_type = typeof (Atari7800.SnapshotMetadata);
        add_platform (platform);

        platform = new Platform (
            ATARI_LYNX,
            "atari-lynx",
            _("Atari Lynx"),
            { "application/x-atari-lynx-rom" },
            { "lnx" }
        );
        platform.controls_type = typeof (AtariLynx.Controls);
        platform.firmware_list_type = typeof (AtariLynx.FirmwareList);
        platform.metadata_type = typeof (AtariLynx.Metadata);
        platform.parser_type = typeof (AtariLynx.Parser);
        platform.runner_delegate_type = typeof (AtariLynx.RunnerDelegate);
        platform.screen_set_type = typeof (AtariLynx.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            GAME_BOY,
            "game-boy",
            _("Game Boy"),
            {
                "application/x-gameboy-rom",
                "application/x-gameboy-color-rom"
            },
            { "gb", "gbc", "cgb", "sgb" }
        );
        platform.controls_type = typeof (GameBoy.Controls);
        platform.parser_type = typeof (GameBoy.Parser);
        platform.runner_delegate_type = typeof (GameBoy.RunnerDelegate);
        platform.metadata_type = typeof (GameBoy.Metadata);
        platform.properties_type = typeof (GameBoy.Properties);
        platform.screen_set_type = typeof (GameBoy.ScreenSet);
        platform.snapshot_metadata_type = typeof (GameBoy.SnapshotMetadata);
        add_platform (platform);

        platform = new Platform (
            GAME_BOY_ADVANCE,
            "game-boy-advance",
            _("Game Boy Advance"),
            { "application/x-gba-rom" },
            { "gba", "agb" }
        );
        platform.controls_type = typeof (GameBoyAdvance.Controls);
        platform.properties_type = typeof (GameBoyAdvance.Properties);
        platform.runner_delegate_type = typeof (GameBoyAdvance.RunnerDelegate);
        platform.screen_set_type = typeof (GameBoyAdvance.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            GAME_GEAR,
            "game-gear",
            _("Game Gear"),
            { "application/x-gamegear-rom" },
            { "gg" }
        );
        platform.controls_type = typeof (GameGear.Controls);
        platform.metadata_type = typeof (GameGear.Metadata);
        platform.parser_type = typeof (GameGear.Parser);
        platform.screen_set_type = typeof (GameGear.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            MASTER_SYSTEM,
            "master-system",
            /* translators: also known as "Sega Mark III" in eastern Asia */
            _("Master System"),
            { "application/x-sms-rom" },
            { "sms" }
        );
        platform.controls_type = typeof (MasterSystem.Controls);
        platform.metadata_type = typeof (MasterSystem.Metadata);
        platform.parser_type = typeof (MasterSystem.Parser);
        platform.runner_delegate_type = typeof (MasterSystem.RunnerDelegate);
        platform.properties_type = typeof (MasterSystem.Properties);
        platform.snapshot_metadata_type = typeof (MasterSystem.SnapshotMetadata);
        platform.screen_set_type = typeof (MasterSystem.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            MEGA_DRIVE,
            "mega-drive",
            /* translators: known as "Mega Drive" in most of the world */
            _("Sega Genesis"),
            { "application/x-genesis-rom" },
            { "gen", "smd", "sgd" }
        );
        platform.controls_type = typeof (MegaDrive.Controls);
        platform.metadata_type = typeof (MegaDrive.Metadata);
        platform.parser_type = typeof (MegaDrive.Parser);
        platform.runner_delegate_type = typeof (MegaDrive.RunnerDelegate);
        platform.screen_set_type = typeof (MegaDrive.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            NEO_GEO_POCKET,
            "neo-geo-pocket",
            _("Neo Geo Pocket"),
            {
                "application/x-neo-geo-pocket-rom",
                "application/x-neo-geo-pocket-color-rom"
            },
            { "ngp", "ngc" }
        );
        platform.controls_type = typeof (NeoGeoPocket.Controls);
        platform.metadata_type = typeof (NeoGeoPocket.Metadata);
        platform.parser_type = typeof (NeoGeoPocket.Parser);
        platform.screen_set_type = typeof (NeoGeoPocket.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            NES,
            "nes",
            /* translators: known as "Famicom" in eastern Asia. "Nintendo (NES)"
               is used instead of "Nintendo Entertainment System" because the
               full name doesn't fit into the sidebar */
            _("Nintendo (NES)"),
            { "application/x-nes-rom" },
            { "nes", "nez", "unf", "unif" }
        );
        /* translators: "NES" is known as "Famicom" in eastern Asia. This string
           must be *exactly* "nes" or "famicom" depending on which name you use
           elsewhere. This string selects an icon and only those two values are
           allowed. */
        platform.icon_name = "platform-%s-symbolic".printf (_("nes"));
        platform.controls_type = typeof (Nes.Controls);
        platform.runner_delegate_type = typeof (Nes.RunnerDelegate);
        platform.screen_set_type = typeof (Nes.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            NINTENDO_64,
            "nintendo-64",
            _("Nintendo 64"),
            { "application/x-n64-rom" },
            { "n64", "z64", "v64" }
        );
        platform.controls_type = typeof (Nintendo64.Controls);
        platform.runner_delegate_type = typeof (Nintendo64.RunnerDelegate);
        platform.metadata_type = typeof (Nintendo64.Metadata);
        platform.parser_type = typeof (Nintendo64.Parser);
        platform.properties_type = typeof (Nintendo64.Properties);
        platform.screen_set_type = typeof (Nintendo64.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            NINTENDO_DS,
            "nintendo-ds",
            _("Nintendo DS"),
            { "application/x-nintendo-ds-rom" },
            { "nds" }
        );
        platform.controls_type = typeof (NintendoDs.Controls);
        platform.metadata_type = typeof (NintendoDs.Metadata);
        platform.parser_type = typeof (NintendoDs.Parser);
        platform.properties_type = typeof (NintendoDs.Properties);
        platform.runner_delegate_type = typeof (NintendoDs.RunnerDelegate);
        platform.screen_set_type = typeof (NintendoDs.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            PC_ENGINE,
            "pc-engine",
            /* translators: known as "PC Engine" in eastern Asia and France */
            _("TurboGrafx-16"),
            { "application/x-pc-engine-rom" },
            { "pce" }
        );
        platform.controls_type = typeof (PcEngine.Controls);
        platform.parser_type = typeof (PcEngine.Parser);
        platform.properties_type = typeof (PcEngine.Properties);
        platform.runner_delegate_type = typeof (PcEngine.RunnerDelegate);
        platform.screen_set_type = typeof (PcEngine.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            PLAYSTATION,
            "playstation",
            _("PlayStation"),
            { "application/x-cue" },
            { "bin+cue", "bin+cue+sbi" }
        );
        platform.controls_type = typeof (PlayStation.Controls);
        platform.firmware_list_type = typeof (PlayStation.FirmwareList);
        platform.metadata_type = typeof (PlayStation.Metadata);
        platform.parser_type = typeof (PlayStation.Parser);
        platform.properties_type = typeof (PlayStation.Properties);
        platform.runner_delegate_type = typeof (PlayStation.RunnerDelegate);
        platform.screen_set_type = typeof (PlayStation.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            SEGA_SATURN,
            "sega-saturn",
            _("Sega Saturn"),
            { "application/x-cue" },
            { "bin+cue" }
        );
        platform.controls_type = typeof (SegaSaturn.Controls);
        platform.firmware_list_type = typeof (SegaSaturn.FirmwareList);
        platform.metadata_type = typeof (SegaSaturn.Metadata);
        platform.parser_type = typeof (SegaSaturn.Parser);
        platform.runner_delegate_type = typeof (SegaSaturn.RunnerDelegate);
        platform.screen_set_type = typeof (SegaSaturn.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            SG1000,
            "sg-1000",
            _("SG-1000"),
           {  "application/x-sg1000-rom" },
            { "sg" }
        );
        platform.controls_type = typeof (Sg1000.Controls);
        platform.runner_delegate_type = typeof (Sg1000.RunnerDelegate);
        platform.screen_set_type = typeof (Sg1000.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            SUPER_NES,
            "snes",
            /* translators: known as "Super Famicom" in eastern Asia.
               "Super Nintendo (SNES)" is used instead of
               "Super Nintendo Entertainment System" because the full name
               doesn't fit into the sidebar */
            _("Super Nintendo (SNES)"),
            { "application/vnd.nintendo.snes.rom" },
            { "sfc", "smc" }
        );
        platform.controls_type = typeof (SuperNes.Controls);
        platform.runner_delegate_type = typeof (SuperNes.RunnerDelegate);
        platform.screen_set_type = typeof (SuperNes.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            VIRTUAL_BOY,
            "virtual-boy",
            _("Virtual Boy"),
            { "application/x-virtual-boy-rom" },
            { "vb" }
        );
        platform.controls_type = typeof (VirtualBoy.Controls);
        platform.parser_type = typeof (VirtualBoy.Parser);
        platform.runner_delegate_type = typeof (VirtualBoy.RunnerDelegate);
        platform.screen_set_type = typeof (VirtualBoy.ScreenSet);
        add_platform (platform);

        platform = new Platform (
            WONDERSWAN,
            "wonderswan",
            _("WonderSwan"),
            {
                "application/x-wonderswan-rom",
                "application/x-wonderswan-color-rom",
            },
            { "ws", "wsc" }
        );
        platform.controls_type = typeof (WonderSwan.Controls);
        platform.metadata_type = typeof (WonderSwan.Metadata);
        platform.parser_type = typeof (WonderSwan.Parser);
        platform.runner_delegate_type = typeof (WonderSwan.RunnerDelegate);
        platform.screen_set_type = typeof (WonderSwan.ScreenSet);
        add_platform (platform);

        // Set up add-on platforms

        platform = new Platform (
            FAMICOM_DISK_SYSTEM,
            "fds",
            /* translators: only released in eastern Asia */
            _("Famicom Disk System"),
            { "application/x-fds-disk" },
            { "fds" }
        );
        platform.firmware_list_type = typeof (Fds.FirmwareList);
        platform.runner_delegate_type = typeof (Fds.RunnerDelegate);
        platform.parent = get_platform ("nes");
        add_platform (platform);

        platform = new Platform (
            PC_ENGINE_CD,
            "pc-engine-cd",
            /* translators: known as "CD-ROM²" in eastern Asia and France */
            _("TurboGrafx-CD"),
            { "application/x-cue" },
            { "bin+cue" }
        );
        platform.firmware_list_type = typeof (PcEngineCd.FirmwareList);
        platform.parser_type = typeof (PcEngineCd.Parser);
        platform.parent = get_platform ("pc-engine");
        add_platform (platform);
    }
}
