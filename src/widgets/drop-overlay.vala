// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.DropOverlay : Gtk.Widget {
    public signal bool drop (Gdk.FileList list);

    private Gtk.Overlay overlay;
    private Gtk.Revealer revealer;

    public Gtk.Widget? child {
        get { return overlay.child; }
        set { overlay.child = value; }
    }

    public string title { get; set; }
    public string icon_name { get; set; }

    construct {
        var page = new Adw.StatusPage ();
        bind_property ("title", page, "title", SYNC_CREATE);
        bind_property ("icon-name", page, "icon-name", SYNC_CREATE);
        page.add_css_class ("overlay-status");

        var scrim = new Adw.Bin () {
            child = page,
        };
        scrim.add_css_class ("background");

        revealer = new Gtk.Revealer () {
            transition_type = CROSSFADE,
            can_target = false,
            child = scrim,
        };

        overlay = new Gtk.Overlay ();
        overlay.add_overlay (revealer);
        overlay.set_parent (this);

        var target = new Gtk.DropTarget (typeof (Gdk.FileList), COPY);
        target.notify["current-drop"].connect (() => {
            revealer.reveal_child = target.current_drop != null;
        });
        target.drop.connect ((value, x, y) => {
            var list = (Gdk.FileList) value.get_boxed ();

            return drop (list);
        });

        add_controller (target);
    }

    static construct {
        set_layout_manager_type (typeof (Gtk.BinLayout));
        set_css_name ("drop-overlay");
    }

    protected override void dispose () {
        if (overlay != null) {
            overlay.dispose ();
            overlay = null;
        }

        base.dispose ();
    }
}
