// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.ResetSwitchRow : Adw.ActionRow {
    public signal void reset ();

    public bool active { get; set; }
    public bool modified { get; set; }

    construct {
        var reset_btn = new Gtk.Button () {
            icon_name = "edit-undo-symbolic",
            tooltip_text = _("Reset"),
            valign = CENTER,
        };

        reset_btn.clicked.connect (() => reset ());

        reset_btn.add_css_class ("flat");
        bind_property ("modified", reset_btn, "visible", SYNC_CREATE);

        add_suffix (reset_btn);

        var the_switch = new Gtk.Switch () {
            valign = CENTER,
        };

        bind_property ("active", the_switch, "active", SYNC_CREATE | BIDIRECTIONAL);

        add_suffix (the_switch);

        activatable_widget = the_switch;
    }
}
