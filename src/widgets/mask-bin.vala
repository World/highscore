// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.MaskBin : Adw.Bin {
    construct {
        overflow = HIDDEN;
    }

    static construct {
        set_css_name ("mask-bin");
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        var snapshot2 = new Gtk.Snapshot ();
        base.snapshot (snapshot2);
        var node = snapshot2.free_to_node ();

        if (node == null)
            return;

        snapshot.push_mask (INVERTED_ALPHA);

        snapshot.append_node (node);
        snapshot.pop ();

        snapshot.append_color (
            get_color (), {{ 0, 0 }, { get_width (), get_height () }}
        );
        snapshot.pop ();
    }
}
