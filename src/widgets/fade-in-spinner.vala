// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.FadeInSpinner : Adw.Bin {
    private const int DELAY_MS = 500;
    private const int DURATION_MS = 1000;

    private Gtk.Revealer revealer;
    private Adw.Animation timeout_animation;

    construct {
        can_target = false;

        var spinner = new Adw.Spinner ();

        revealer = new Gtk.Revealer () {
            transition_type = CROSSFADE,
            transition_duration = DURATION_MS,
            child = spinner,
        };

        child = revealer;

        timeout_animation = new Adw.TimedAnimation (
            this, 0, 1, DELAY_MS, new Adw.CallbackAnimationTarget (value => {})
        );
        timeout_animation.follow_enable_animations_setting = false;
        timeout_animation.done.connect (() => {
            revealer.reveal_child = true;
        });
    }

    protected override void map () {
        base.map ();

        timeout_animation.play ();
    }

    protected override void unmap () {
        timeout_animation.reset ();

        base.unmap ();

        revealer.reveal_child = false;
    }
}
