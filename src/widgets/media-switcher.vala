// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/widgets/media-switcher.ui")]
public class Highscore.MediaSwitcher : Adw.Bin {
    public enum MediaType {
        FLOPPY,
        CD;

        public string get_icon_name () {
            switch (this) {
                case FLOPPY:
                    return "media-floppy-symbolic";
                case CD:
                    return "media-optical-cd-symbolic";
                default:
                    assert_not_reached ();
            }
        }
    }

    public MediaType media_type { get; construct; }
    public string tooltip { get; construct; }
    public string[] media { get; construct; }
    public uint selected_media { get; set; }

    [GtkChild]
    private unowned Gtk.MenuButton button;
    [GtkChild]
    private unowned Gtk.ListBox listbox;

    private Gtk.ListBoxRow[] rows;
    private Gtk.Image[] checks;

    public MediaSwitcher (MediaType media_type, string tooltip, string[] media) {
        Object (media_type: media_type, tooltip: tooltip, media: media);
    }

    construct {
        button.icon_name = media_type.get_icon_name ();

        rows = {};
        checks = {};

        for (uint i = 0; i < media.length; i++) {
            var box = new Gtk.Box (HORIZONTAL, 6);

            var label = new Gtk.Label (media[i]) {
                xalign = 0,
                ellipsize = END,
            };
            label.add_css_class ("numeric");

            var check = new Gtk.Image.from_icon_name ("object-select-symbolic") {
                opacity = (i == selected_media) ? 1 : 0,
            };

            box.append (label);
            box.append (check);

            var row = new Gtk.ListBoxRow () {
                child = box,
            };

            listbox.append (row);

            rows += row;
            checks += check;
        }

        notify["selected-media"].connect (() => {
            for (uint i = 0; i < media.length; i++)
                checks[i].opacity = (i == selected_media) ? 1 : 0;
        });
    }

    [GtkCallback]
    private void row_activated_cb (Gtk.ListBoxRow row) {
        for (uint i = 0; i < media.length; i++) {
            if (row == rows[i]) {
                selected_media = i;
                button.popdown ();
                return;
            }
        }
    }

    public void track_fullscreen (FullscreenView view) {
        view.track_menu_button (button);
    }
}
