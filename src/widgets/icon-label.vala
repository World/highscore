// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.IconLabel : Adw.Bin {
    private static Regex icon_regex;

    public string label { get; set; }

    public IconLabel (string label) {
        Object (label: label);
    }

    construct {
        notify["label"].connect (rebuild_label);
    }

    static construct {
        set_css_name ("icon-label");
    }

    private void rebuild_label () {
        if (!label.contains ("@")) {
            child = new Gtk.Label (label) {
                ellipsize = MIDDLE
            };

            update_property (
                Gtk.AccessibleProperty.LABEL, label,
                -1
            );

            return;
        }

        if (icon_regex == null) {
            try {
                icon_regex = new Regex ("(@\\w+@)");
            } catch (Error e) {
                error ("Invalid regex: %s", e.message);
            }
        }

        var alt_text_builder = new StringBuilder ();

        var box = new Gtk.Box (HORIZONTAL, 0);

        foreach (var part in icon_regex.split (label)) {
            if (part == "")
                continue;

            if (part.has_prefix ("@") && part.has_suffix ("@")) {
                var name = part.substring (1, part.length - 2);

                string icon_name = null, alt_text = null;

                if (name == "PS_TRIANGLE") {
                    icon_name = "ps-button-triangle";
                    alt_text = _("Triangle");
                } else if (name == "PS_SQUARE") {
                    icon_name = "ps-button-square";
                    alt_text = _("Square");
                } else if (name == "PS_CIRCLE") {
                    icon_name = "ps-button-circle";
                    alt_text = _("Circle");
                } else if (name == "PS_X") {
                    icon_name = "ps-button-cross";
                    alt_text = _("X");
                } else {
                    assert_not_reached ();
                }

                var icon = new Gtk.Image.from_icon_name (icon_name);
                icon.add_css_class ("inline-icon");

                box.append (icon);

                alt_text_builder.append (alt_text);

                continue;
            }

            box.append (new Gtk.Label (part) {
                ellipsize = MIDDLE,
            });

            alt_text_builder.append (part);
        }

        update_property (
            Gtk.AccessibleProperty.LABEL, alt_text_builder.str,
            -1
        );

        child = box;
    }
}
