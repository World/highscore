// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.ContextMenuBin : Adw.Bin {
    private Gtk.PopoverMenu? popover;

    public MenuModel? menu_model { get; set; }

    public signal void setup_menu ();

    construct {
        var click_gesture = new Gtk.GestureClick () {
            button = 0,
        };
        click_gesture.pressed.connect (right_click_pressed_cb);
        add_controller (click_gesture);

        var long_press_gesture = new Gtk.GestureLongPress () {
            touch_only = true,
        };
        long_press_gesture.pressed.connect (long_pressed_cb);
        add_controller (long_press_gesture);
    }

    static construct {
        install_action ("menu.popup", null, widget => {
            var self = widget as ContextMenuBin;
            self.open_context_menu (-1, -1);
        });

        add_binding_action (Gdk.Key.F10, Gdk.ModifierType.SHIFT_MASK, "menu.popup", null);
        add_binding_action (Gdk.Key.Menu, 0, "menu.popup", null);

        set_css_name ("context-menu-bin");
    }

    private void open_context_menu (double x, double y) {
        if (x < 0 && y < 0) {
            x = 0;
            y = 0;
        }

        if (popover == null) {
            popover = new Gtk.PopoverMenu.from_model (menu_model) {
                has_arrow = false,
            };

            popover.bind_property ("menu-model", this, "menu-model", DEFAULT);

            popover.closed.connect (() => {
                Idle.add_once (() => {
                    popover.unparent ();
                    popover = null;
                });
            });

            popover.set_parent (this);
        }

        popover.set_pointing_to ({ (int) x, (int) y, 0, 0 });

        if (get_direction () == RTL)
            popover.halign = END;
        else
            popover.halign = START;

        setup_menu ();

        popover.popup ();
    }

    private void right_click_pressed_cb (Gtk.GestureClick gesture, int n_click, double x, double y) {
        var event = gesture.get_current_event ();

        if (!event.triggers_context_menu ()) {
            gesture.set_state (DENIED);
            return;
        }

        if (!contains (x, y)) {
            gesture.set_state (DENIED);
            return;
        }

        open_context_menu (x, y);

        gesture.set_state (CLAIMED);
    }

    private void long_pressed_cb (Gtk.GestureLongPress gesture, double x, double y) {
        if (!contains (x, y)) {
            gesture.set_state (DENIED);
            return;
        }

        open_context_menu (x, y);

        gesture.set_state (CLAIMED);
    }
}
