#ifdef VERTEX

void hs_main() {}

#else // FRAGMENT

vec4 hs_main() {
  return texture(u_source, v_texCoord);
}

#endif