// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.ScreenSet : Object {
    public enum Mode {
        DISPLAY,
        THUMBNAIL,
        SCREENSHOT,
    }

    public const int DEFAULT_SCREEN = 0;

    public signal void layout_changed (bool animate);
    public signal void secondary_layout_changed (bool animate);
    public signal void filter_changed ();
    public signal void screen_type_changed ();

    public Mode mode { get; construct; }
    public Game game { get; construct; }
    public unowned Runner? runner { get; construct; }
    public Snapshot? snapshot { get; construct; }

    public ScreenLayout layout { get; set; }
    public ScreenLayout? secondary_layout { get; set; }

    construct {
        layout = new SingleLayout (DEFAULT_SCREEN);
    }

    public virtual Screen[] get_screens () {
        return {
            new Screen (DEFAULT_SCREEN, {{ 0, 0 }, { 1, 1 }})
        };
    }

    public virtual string get_filter_path () {
        return "/app/drey/Highscore/runner/screen/platform-default.glsl";
    }

    public virtual void setup_filter (GLShader shader, int screen_id) {
    }

    public virtual ScreenType get_screen_type (Hs.Region region) {
        return UNKNOWN;
    }

    public static ScreenSet? create (
        Mode mode,
        Runner? runner,
        Snapshot? snapshot
    ) requires (runner != null || snapshot != null) {
        Game game;
        if (runner != null)
            game = runner.game;
        else
            game = snapshot.manager.game;

        var type = game.platform.screen_set_type;

        if (type == Type.NONE && game.platform.parent != null)
            type = game.platform.parent.screen_set_type;

        if (type == Type.NONE)
            type = typeof (ScreenSet);

        return Object.new (
            type, mode: mode, game: game, runner: runner, snapshot: snapshot
        ) as ScreenSet;
    }
}
