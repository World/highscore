// This file is part of Highscore. License: GPL-3.0-or-later

public enum Highscore.Rotation {
    0_DEG,
    90_DEG,
    180_DEG,
    270_DEG;

    public float get_angle () {
        return (int) this * 90;
    }

    public bool is_perpendicular () {
        return this == 90_DEG || this == 270_DEG;
    }
}
