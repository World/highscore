// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Screen : Object {
    public int id { get; construct; }
    public Graphene.Rect? area { get; construct; }
    public float aspect_ratio { get; construct; }

    public Screen (int id, Graphene.Rect? area, float aspect_ratio = -1) {
        Object (id: id, area: area, aspect_ratio: aspect_ratio);
    }

    public float get_effective_aspect_ratio (float core_aspect_ratio) {
        if (aspect_ratio >= 0)
            return aspect_ratio;

        return core_aspect_ratio * area.size.width / area.size.height;
    }
}
