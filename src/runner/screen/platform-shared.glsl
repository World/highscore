#ifndef CORE
#  define out varying

#  ifdef VERTEX
#    define in attribute
#  else
#    define in varying
#  endif

#  define texture texture2D
#endif

#ifndef LEGACY
precision mediump float;
#endif

#ifdef VERTEX

in vec2 position;
in vec2 texCoord;

out vec2 v_texCoord;

uniform vec2 u_screenPosition;
uniform vec2 u_screenSize;
uniform bool u_flipped;
uniform mat4 u_mvp;

void hs_main();

void main() {
  if (u_flipped)
    v_texCoord = vec2(0, 1) + vec2(1, -1) * texCoord;
  else
    v_texCoord = texCoord;

  v_texCoord = u_screenPosition + v_texCoord * u_screenSize;

  gl_Position = u_mvp * vec4(position, 0, 1);

  hs_main();
}

#else // FRAGMENT

in vec2 v_texCoord;

#ifdef CORE
out vec4 outputColor;
#endif

uniform sampler2D u_source;

vec4 hs_main();

void main() {
#ifdef CORE
  outputColor = hs_main();
#else
  gl_FragColor = hs_main();
  gl_FragColor = vec4(gl_FragColor.rgb, 1);
#endif
}

#endif