// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "app.drey.Highscore.Runner")]
public interface Highscore.RunnerProxy : Object {
    public signal void redraw ();
    public signal void render_mode_changed (bool dmabuf);
    public signal void rumble (uint player, double strong_magnitude, double weak_magnitude, uint16 milliseconds);
    public signal void current_media_changed (uint media);

    public abstract async void load_rom (string[] rom_paths, string save_path) throws Error;
    public abstract async void start () throws Error;
    public abstract async void stop () throws Error;
    public abstract async void reset (bool hard) throws Error;

    public abstract async void pause () throws Error;
    public abstract async void resume () throws Error;

    public abstract async void reload_save (string path) throws Error;
    public abstract async void sync_save () throws Error;
    public abstract async void save_state (string path) throws Error;
    public abstract async void load_state (string path) throws Error;

    public abstract async Hs.Region get_region () throws Error;

    public abstract async uint get_current_media () throws Error;
    public abstract async void set_current_media (uint media) throws Error;

    public abstract async UnixInputStream get_input_buffer () throws Error;
    public abstract async UnixInputStream get_video_buffer () throws Error;
    public abstract async UnixInputStream get_dmabuf (out Variant metadata) throws Error;
}
