// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.RunnerProcess : Object {
    private static string runner_path_override;

    public signal void stopped (string? error);

    public RunnerProxy proxy { get; private set; }
    public Object? platform_proxy { get; private set; }
    public Object? addon_proxy { get; private set; }
    public bool running { get; private set; }

    private DBusConnection connection;
    private Cancellable cancellable;
    private string filename;
    private string platform_name;
    private string game_title;

    public RunnerProcess (string game_title, string filename, string platform_name) {
        this.game_title = game_title;
        this.filename = filename;
        this.platform_name = platform_name;
    }

    ~RunnerProcess () {
        assert (!running);
    }

    private SocketConnection? create_connection (SubprocessLauncher launcher, int subprocess_fd) throws Error {
        int sv[2];
        if (Posix.socketpair (Posix.AF_UNIX, Posix.SOCK_STREAM, 0, sv) != 0)
            return null;

        Unix.set_fd_nonblocking (sv[0], true);
        Unix.set_fd_nonblocking (sv[1], true);

        launcher.take_fd (sv[1], subprocess_fd);

        var socket = new Socket.from_fd (sv[0]);
        var conn = SocketConnection.factory_create_connection (socket);

        if (conn != null)
            assert (conn is UnixConnection);

        return conn;
    }

    private async void wait_for_errors (Subprocess process) {
        bool success = false;
        string message = null;

        try {
            success = yield process.wait_check_async (cancellable);
        } catch (Error e) {
            if (e.domain == IOError.quark () && e.code == IOError.CANCELLED)
                success = true;
            else
                message = e.message;
        }

        proxy = null;
        connection = null;
        cancellable = null;
        running = false;

        if (success) {
            stopped (null);
            return;
        }

        warning ("Subprocess stopped unexpectedly: %s", message);
        stopped (message);
    }

    public async void start () throws Error requires (!running) {
        var launcher = new SubprocessLauncher (NONE);
        var conn = create_connection (launcher, 3);

        if (runner_path_override == null)
            runner_path_override = Environment.get_variable ("HIGHSCORE_RUNNER");

        var path = runner_path_override ?? Config.RUNNER_PATH;

        string[] args = {};

        if (VALGRIND in Debug.get_flags ())
            args += "valgrind";

        args += path;
        args += game_title;
        args += filename;
        args += platform_name;

        var process = launcher.spawnv (args);

        connection = yield new DBusConnection (
            conn, null,
            DELAY_MESSAGE_PROCESSING |
            AUTHENTICATION_CLIENT
        );
        connection.start_message_processing ();

        cancellable = new Cancellable ();
        wait_for_errors.begin (process);

        proxy = yield connection.get_proxy<RunnerProxy> (
            null,
            "/app/drey/Highscore/Runner",
            DO_NOT_LOAD_PROPERTIES
        );

        var platform = Hs.Platform.get_from_name (platform_name);
        var base_platform = platform.get_base_platform ();

        platform_proxy = yield create_platform_proxy (
            base_platform,
            connection,
            "/app/drey/Highscore/Runner/"
        );

        if (platform != base_platform) {
            addon_proxy = yield create_platform_proxy (
                platform,
                connection,
                "/app/drey/Highscore/Runner/"
            );
        }

        running = true;
    }

    public async void stop () throws Error requires (running) {
        cancellable.cancel ();

        yield proxy.stop ();

        if (connection != null)
            yield connection.close ();

        proxy = null;
        connection = null;
        cancellable = null;
        running = false;
    }
}
