// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.SingleLayout : ScreenLayout {
    private int screen_id;
    private Rotation rotation;

    public SingleLayout (int screen_id, Rotation rotation = 0_DEG) {
        this.screen_id = screen_id;
        this.rotation = rotation;
    }

    private int find_visible_screen_index (Screen[] screens) {
        for (int i = 0; i < screens.length; i++) {
            if (screens[i].id == screen_id)
                return i;
        }

        return -1;
    }

    public override void measure (
        float source_width,
        float source_height,
        Screen[] screens,
        out float width,
        out float height
    ) {
        width = 0;
        height = 0;

        var visible_screen_index = find_visible_screen_index (screens);
        if (visible_screen_index < 0) {
            width = source_width;
            height = source_height;
        } else {
            width = source_width * screens[visible_screen_index].area.size.width;
            height = source_height * screens[visible_screen_index].area.size.height;
        }

        if (rotation.is_perpendicular ()) {
            float tmp = width;
            width = height;
            height = tmp;
        }
    }

    public override Result[] layout (ViewDimensions dimensions, float aspect_ratio, Screen[] screens) {
        float width = dimensions.width - dimensions.left_inset - dimensions.right_inset;
        float height = dimensions.height - dimensions.top_inset - dimensions.bottom_inset;

        float view_aspect_ratio = width / height;
        Result[] ret = {};

        var visible_screen_index = find_visible_screen_index (screens);
        if (visible_screen_index < 0)
            return {};

        foreach (var screen in screens) {
            if (screen != screens[visible_screen_index]) {
                Result result = { false, 0, 0, 0, null };
                ret += result;
                continue;
            }

            float screen_aspect_ratio = screen.get_effective_aspect_ratio (aspect_ratio);

            if (rotation.is_perpendicular ())
                screen_aspect_ratio = 1 / screen_aspect_ratio;

            float screen_width, screen_height;

            if (view_aspect_ratio > screen_aspect_ratio) {
                screen_width = Math.ceilf (height * screen_aspect_ratio);
                screen_height = height;
            } else {
                screen_height = Math.ceilf (width / screen_aspect_ratio);
                screen_width = width;
            }

            if (rotation.is_perpendicular ()) {
                float tmp = screen_width;
                screen_width = screen_height;
                screen_height = tmp;
            }

            var transform = new Gsk.Transform ();

            transform = transform.translate ({
                dimensions.left_inset, dimensions.top_inset
            });

            transform = transform.translate ({
                Math.floorf ((width - screen_width) / 2),
                Math.floorf ((height - screen_height) / 2)
            });

            Result result = {
                true,
                screen_width,
                screen_height,
                rotation.get_angle (),
                transform,
            };

            ret += result;
        }

        return ret;
    }
}
