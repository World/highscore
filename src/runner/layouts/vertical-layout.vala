// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.VerticalLayout : ScreenLayout {
    private float screen_gap;
    private Rotation rotation;

    public VerticalLayout (float screen_gap = 0, Rotation rotation = 0_DEG) {
        this.screen_gap = screen_gap;
        this.rotation = rotation;
    }

    public override void measure (
        float source_width,
        float source_height,
        Screen[] screens,
        out float width,
        out float height
    ) {
        width = 0;
        height = 0;

        float max_screen_height = 0;
        foreach (var screen in screens)
            max_screen_height = float.max (max_screen_height, screen.area.size.height);

        height = source_height * max_screen_height * (screens.length + screen_gap * (screens.length - 1));

        foreach (var screen in screens) {
            float screen_width = source_width * screen.area.size.width * max_screen_height / screen.area.size.height;
            width = float.max (width, screen_width);
        }

        if (rotation.is_perpendicular ()) {
            float tmp = width;
            width = height;
            height = tmp;
        }
    }

    public override Result[] layout (ViewDimensions dimensions, float aspect_ratio, Screen[] screens) {
        float width = dimensions.width - dimensions.left_inset - dimensions.right_inset;
        float height = dimensions.height - dimensions.top_inset - dimensions.bottom_inset;

        float view_aspect_ratio = width / height;
        Result[] ret = {};

        float max_aspect_ratio = 0, box_aspect_ratio = 0;
        foreach (var screen in screens) {
            float screen_aspect_ratio = screen.get_effective_aspect_ratio (aspect_ratio);

            max_aspect_ratio = float.max (max_aspect_ratio, screen_aspect_ratio);
        }

        box_aspect_ratio = max_aspect_ratio / (screens.length + screen_gap * (screens.length - 1));

        float max_size;
        if (rotation.is_perpendicular ()) {
            box_aspect_ratio = 1 / box_aspect_ratio;

            if (view_aspect_ratio > box_aspect_ratio)
                max_size = height * box_aspect_ratio;
            else
                max_size = width;
        } else {
            if (view_aspect_ratio > box_aspect_ratio)
                max_size = height;
            else
                max_size = width / box_aspect_ratio;
        }

        float screen_size;

        if (rotation.is_perpendicular ()) {
            float opposite_screen_size = max_size / box_aspect_ratio;
            screen_size = opposite_screen_size / max_aspect_ratio;
        } else {
            float opposite_screen_size = max_size * box_aspect_ratio;
            screen_size = opposite_screen_size / max_aspect_ratio;
        }

        float gap = (max_size - screen_size * screens.length) / (screens.length - 1);
        float pos = (height - max_size) / 2;

        foreach (var screen in screens) {
            float screen_aspect_ratio = screen.get_effective_aspect_ratio (aspect_ratio);

            float screen_width = Math.ceilf (screen_size * screen_aspect_ratio);
            float screen_height = Math.ceilf (screen_size);

            var transform = new Gsk.Transform ();

            transform = transform.translate ({
                dimensions.left_inset, dimensions.top_inset
            });

            transform = transform.translate ({
                Math.floorf ((width - screen_width) / 2),
                Math.floorf (pos),
            });

            Result result = {
                true,
                screen_width,
                screen_height,
                rotation.get_angle (),
                transform,
            };

            ret += result;
            pos += screen_size + gap;
        }

        return ret;
    }
}
