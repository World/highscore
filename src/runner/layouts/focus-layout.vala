// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.FocusLayout : ScreenLayout {
    private int focused_screen_id;
    private float screen_gap;

    public FocusLayout (int focused_screen_id, float screen_gap = 0) {
        this.focused_screen_id = focused_screen_id;
        this.screen_gap = screen_gap;
    }

    private int find_focused_screen_index (Screen[] screens) {
        for (int i = 0; i < screens.length; i++) {
            if (screens[i].id == focused_screen_id)
                return i;
        }

        return -1;
    }

    public override void measure (
        float source_width,
        float source_height,
        Screen[] screens,
        out float width,
        out float height
    ) {
        width = 0;
        height = 0;

        var focused_screen_index = find_focused_screen_index (screens);
        if (focused_screen_index < 0) {
            width = source_width;
            height = source_height;
            return;
        }

        width = source_width * screens[focused_screen_index].area.size.width;
        height = source_height * screens[focused_screen_index].area.size.height;
    }

    private Result layout_screen (Screen screen, float width, float height, float aspect_ratio) {
        float screen_aspect_ratio = screen.get_effective_aspect_ratio (aspect_ratio);
        float view_aspect_ratio = width / height;

        float screen_width, screen_height;

        if (view_aspect_ratio > screen_aspect_ratio) {
            screen_width = Math.ceilf (height * screen_aspect_ratio);
            screen_height = height;
        } else {
            screen_height = Math.ceilf (width / screen_aspect_ratio);
            screen_width = width;
        }

        var transform = new Gsk.Transform ();

        transform = transform.translate ({
            Math.floorf ((width - screen_width) / 2),
            0,
        });

        Result result = {
            true,
            screen_width,
            screen_height,
            0,
            transform,
        };

        return result;
    }

    public override Result[] layout (ViewDimensions dimensions, float aspect_ratio, Screen[] screens) {
        float top_inset = dimensions.top_inset;
        float bottom_inset = dimensions.bottom_inset;
        float left_inset = dimensions.left_inset;
        float right_inset = dimensions.right_inset;
        float width = dimensions.width - left_inset - right_inset;
        float height = dimensions.height - top_inset - bottom_inset;

        var focused_screen_index = find_focused_screen_index (screens);
        if (focused_screen_index < 0)
            return {};

        Result[] ret = {};

        var focused_result = layout_screen (
            screens[focused_screen_index], width, height, aspect_ratio
        );
        float empty_area = (height - focused_result.height) / 2;
        focused_result.transform = focused_result.transform.translate ({
            left_inset, Math.floorf (empty_area) + top_inset
        });

        float gap_size = float.max (
            screen_gap * focused_result.height,
            empty_area + float.max (top_inset, bottom_inset)
        );

        Result[] prev_results = {};
        float pos = empty_area;
        for (int i = focused_screen_index - 1; i >= 0; i--) {
            var res = layout_screen (
                screens[i], width, height, aspect_ratio
            );

            pos -= res.height + gap_size;

            res.transform = res.transform.translate ({
                left_inset, Math.floorf (pos) + top_inset
            });
            prev_results += res;
        }

        Result[] next_results = {};
        pos = height - empty_area + gap_size;
        for (int i = focused_screen_index + 1; i < screens.length; i++) {
            var res = layout_screen (
                screens[i], width, height, aspect_ratio
            );

            res.transform = res.transform.translate ({
                left_inset, Math.floorf (pos) + top_inset
            });
            next_results += res;

            pos += res.height + gap_size;
        }

        for (int i = prev_results.length - 1; i >= 0; i--)
            ret += prev_results[i];

        ret += focused_result;

        for (int i = 0; i < next_results.length; i++)
            ret += next_results[i];

        return ret;
    }
}
