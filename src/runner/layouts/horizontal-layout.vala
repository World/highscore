// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.HorizontalLayout : ScreenLayout {
    private bool reversed;

    public HorizontalLayout (bool reversed = false) {
        this.reversed = reversed;
    }

    public override void measure (
        float source_width,
        float source_height,
        Screen[] screens,
        out float width,
        out float height
    ) {
        float max_screen_height = 0;
        foreach (var screen in screens)
            max_screen_height = float.max (max_screen_height, screen.area.size.height);

        width = 0;
        foreach (var screen in screens)
            width += max_screen_height * screen.area.size.width / screen.area.size.height;

        width *= source_width;
        height = source_height * max_screen_height;
    }

    public override Result[] layout (ViewDimensions dimensions, float aspect_ratio, Screen[] screens) {
        float width = dimensions.width - dimensions.left_inset - dimensions.right_inset;
        float height = dimensions.height - dimensions.top_inset - dimensions.bottom_inset;

        float view_aspect_ratio = width / height;
        Result[] ret = {};

        float total_aspect_ratio = 0;
        foreach (var screen in screens) {
            float screen_aspect_ratio = screen.get_effective_aspect_ratio (aspect_ratio);

            total_aspect_ratio += screen_aspect_ratio;
        }

        float max_width;
        if (view_aspect_ratio > total_aspect_ratio)
            max_width = height * total_aspect_ratio;
        else
            max_width = width;

        float max_height = max_width / total_aspect_ratio;

        float pos = (width - max_width) / 2;

        if (reversed)
            pos = width - pos;

        foreach (var screen in screens) {
            float screen_aspect_ratio = screen.get_effective_aspect_ratio (aspect_ratio);
            float screen_width = Math.ceilf (max_height * screen_aspect_ratio);
            float screen_height = Math.ceilf (max_height);

            var transform = new Gsk.Transform ();

            if (reversed)
                pos -= screen_width;

            transform = transform.translate ({
                dimensions.left_inset, dimensions.top_inset
            });

            transform = transform.translate ({
                Math.floorf (pos),
                Math.floorf ((height - screen_height) / 2),
            });

            Result result = {
                true,
                screen_width,
                screen_height,
                0,
                transform,
            };

            ret += result;

            if (!reversed)
                pos += screen_width;
        }

        return ret;
    }
}
