// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.RunnerDelegate : Object {
    public unowned Runner runner { get; construct; }

    protected override void constructed () {
        base.constructed ();

        assert (runner != null);
    }

    public virtual async void before_load () throws Error {}
    public virtual async void after_load () throws Error {}

    public virtual async void before_reset (bool hard) throws Error {}
    public virtual async void after_reset (bool hard) throws Error {}

    public virtual async void load_state (SnapshotPlatformMetadata? metadata) throws Error {}
    public virtual async void save_state (SnapshotPlatformMetadata metadata) throws Error {}

    public virtual async void pause () {}
    public virtual async void resume () {}

    public virtual uint get_n_players () {
        return runner.get_max_players ();
    }

    public virtual Gtk.Widget? create_header_widget (FullscreenView view) {
        return null;
    }

    public virtual OverlayMenuAddin? create_overlay_menu () {
        return null;
    }

    public virtual void secondary_view_closed () {}
}
