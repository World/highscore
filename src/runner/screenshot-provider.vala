// This file is part of Highscore. License: GPL-3.0-or-later

public interface Highscore.ScreenshotProvider : Object {
    public abstract Gdk.Texture? take_raw_screenshot ();
    public abstract Gdk.Texture? take_screenshot ();
}
