// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Runner : Object {
    public enum CursorType {
        DEFAULT,
        CROSSHAIR,
    }

    public signal void stopped (string? error);
    public signal void redraw ();
    public signal void loading_snapshot (Snapshot snapshot);
    public signal void rumble_indicators_changed ();
    public signal void controller_type_changed (uint player);

    public signal void home_pressed ();

    private uint last_current_media;
    public uint current_media { get; set; }

    public Game game { get; construct; }

    public bool uses_pointer { get; private set; }
    public bool running { get; private set; }
    public bool paused { get; private set; }
    public bool loading { get; private set; }
    public ScreenSet screen_set { get; private set; }

    public Hs.Region region { get; private set; }
    public bool flipped { get; private set; }
    public Gdk.Texture? texture { get; private set; }

    private unowned Core core;
    private RunnerProcess process;
    private RunnerProxy proxy;
    private InputHandler input_handler;

    public Object? platform_proxy { get; private set; }
    public Object? addon_proxy { get; private set; }
    public SharedInputBuffer input_buffer { get; private set; }
    public RunnerDelegate delegate { get; private set; }
    public string view_name { get; set; }
    public string secondary_view_name { get; set; }

    public int n_rumble_indicators { get; private set; }
    public string? touch_overlay_name { get; private set; }

    private SharedVideoBuffer video_buffer;
    public Dmabuf? dmabuf { get; private set; }
    private double aspect_ratio;

    private bool changing_render_mode;

    private File save_location;
    private bool loading_state;

    private Screen[] screens;
    private CursorType[] screen_cursors;

    public Snapshot? initial_snapshot { get; private set; }

    private uint redraw_idle_id;

    private uint frame_number;

    public Runner (Core core, Game game, Snapshot? initial_snapshot, bool will_show_snapshot) {
        Object (game: game);

        this.core = core;
        this.initial_snapshot = initial_snapshot;

        process = new RunnerProcess (
            game.title, core.filename, game.platform.platform.get_name ()
        );

        process.stopped.connect (error => {
            running = false;
            stopped (error);
        });

        input_handler = new InputHandler (this);

        bind_property ("paused", input_handler, "paused", SYNC_CREATE);

        n_rumble_indicators = input_handler.n_rumble_indicators;
        input_handler.notify["n-rumble-indicators"].connect (() => {
            n_rumble_indicators = input_handler.n_rumble_indicators;
        });

        input_handler.pointer_input_changed.connect (update_pointer_input);

        touch_overlay_name = input_handler.touch_overlay_name;

        input_handler.notify["touch-overlay-name"].connect (() => {
            touch_overlay_name = input_handler.touch_overlay_name;
        });

        input_handler.rumble_indicators_changed.connect (() => {
            rumble_indicators_changed ();
        });

        input_handler.home_pressed.connect (() => home_pressed ());

        delegate = create_delegate ();

        screen_set = ScreenSet.create (DISPLAY, this, will_show_snapshot ? initial_snapshot : null);
        screens = screen_set.get_screens ();

        screen_cursors = {};
        foreach (var screen in screens)
            screen_cursors += CursorType.DEFAULT;

        update_pointer_input ();

        if (will_show_snapshot) {
            for (uint i = 0; i < initial_snapshot.controllers.length; i++) {
                var type = initial_snapshot.controllers[i];

                input_handler.set_controller_type (i, type);

                controller_type_changed (i);
            }

            for (uint i = initial_snapshot.controllers.length; i < get_max_players (); i++)
                input_handler.reset_controller (i);
        }
    }

    ~Runner () {
        if (redraw_idle_id > 0)
            Source.remove (redraw_idle_id);
    }

    private RunnerDelegate create_delegate () {
        var type = game.platform.runner_delegate_type;

        if (type == Type.NONE && game.platform.parent != null)
            type = game.platform.parent.runner_delegate_type;

        if (type == Type.NONE)
            type = typeof (RunnerDelegate);

        return Object.new (type, runner: this) as RunnerDelegate;
    }

    private void update_pointer_input () {
        uses_pointer = input_handler.uses_pointer;

        for (int i = 0; i < screens.length; i++) {
            var screen = screens[i];

            if (input_handler.handles_screen (screen.id))
                screen_cursors[i] = CROSSHAIR;
            else
                screen_cursors[i] = DEFAULT;
        }
    }

    private async void start_process () throws Error {
        yield process.start ();

        proxy = process.proxy;
        platform_proxy = process.platform_proxy;
        addon_proxy = process.addon_proxy;

        input_buffer = new SharedInputBuffer (yield proxy.get_input_buffer ());
        video_buffer = new SharedVideoBuffer (yield proxy.get_video_buffer ());

        proxy.render_mode_changed.connect (is_dmabuf => {
            change_render_mode.begin (is_dmabuf);
        });

        running = true;

        proxy.redraw.connect (() => {
            if (redraw_idle_id > 0 || changing_render_mode)
                return;

            redraw_idle_id = Idle.add_once (handle_redraw);
        });

        proxy.rumble.connect ((player, strong_magnitude, weak_magnitude, milliseconds) => {
            if (!paused) {
                input_handler.rumble (
                    player, strong_magnitude, weak_magnitude, milliseconds
                );
            }
        });

        proxy.current_media_changed.connect (media => {
            last_current_media = current_media = media;
        });

        last_current_media = current_media = yield proxy.get_current_media ();

        notify["current-media"].connect (() => {
            if (last_current_media == current_media)
                return;

            last_current_media = current_media;

            if (proxy == null)
                return;

            proxy.set_current_media.begin (current_media);
        });

        video_buffer.lock ();
        bool empty = video_buffer.is_empty ();
        video_buffer.unlock ();

        if (!empty)
            handle_redraw ();
    }

    private async void change_render_mode (bool is_dmabuf) {
        changing_render_mode = true;

        try {
            if (is_dmabuf) {
                Variant metadata;
                int fd = unwrap_fd (yield proxy.get_dmabuf (out metadata));

                dmabuf = new Dmabuf (fd, metadata);
            } else {
                dmabuf = null;
            }
        } catch (Error e) {
            critical ("Failed to change video mode: %s", e.message); // TODO
        }

        changing_render_mode = false;
    }

    private void handle_redraw () {
        redraw_idle_id = 0;

        if (loading_state || changing_render_mode)
            return;

        video_buffer.lock ();

        uint new_frame_number = video_buffer.get_frame_number ();
        if (new_frame_number == frame_number) {
            video_buffer.unlock ();
            return;
        }

        frame_number = new_frame_number;
        aspect_ratio = video_buffer.get_aspect_ratio ();
        flipped = video_buffer.get_flipped ();

        if (dmabuf != null) {
            video_buffer.unlock ();

            texture = null;
            redraw ();

            return;
        }

        var area = video_buffer.get_area ();
        var row_stride = video_buffer.get_row_stride ();
        var format = video_buffer.get_pixel_format ();
        var pixels = video_buffer.get_pixels ();

        int pixel_size = format.get_pixel_size ();
        size_t offset = area.y * row_stride + area.x * pixel_size;
        size_t size = row_stride * (area.height - 1) + area.width * pixel_size;

        var bytes = create_bytes (pixels + offset, size);
        video_buffer.unlock ();

        Gdk.MemoryFormat gdk_format;

        switch (format) {
            case R8G8B8:
                gdk_format = R8G8B8;
                break;
            case R8G8B8X8:
                gdk_format = R8G8B8X8;
                break;
            case B8G8R8X8:
                gdk_format = B8G8R8X8;
                break;
            default:
                assert_not_reached ();
        }

        texture = new Gdk.MemoryTexture (
            (int) area.width,
            (int) area.height,
            gdk_format,
            bytes,
            row_stride
        );

        redraw ();
    }

    private async File create_save_location () throws Error {
        var tmp_dir = yield File.new_tmp_dir_async ("highscore_save_XXXXXX");

        return tmp_dir.get_child ("save");
    }

    public async void start () throws Error requires (!running) {
        yield start_process ();

        var list = game.platform.firmware_list;
        if (list != null) {
            foreach (var f in list.list_firmware ()) {
                var file = f.get_file ();

                if (file.query_exists ())
                    yield list.load (f.id, file, this);
            }
        }

        yield delegate.before_load ();

        save_location = yield create_save_location ();

        if (initial_snapshot != null)
            yield initial_snapshot.load_data (save_location);

        string[] rom_paths = {};
        foreach (var media in game.get_media ())
            rom_paths += media.file.get_path ();

        yield proxy.load_rom (rom_paths, save_location.get_path ());

        region = yield proxy.get_region ();

        input_handler.freeze_controller_notify ();

        for (uint i = 0; i < get_max_players (); i++)
            input_handler.reset_controller (i);

        yield delegate.after_load ();

        input_handler.thaw_controller_notify ();

        yield proxy.start ();

        input_handler.start ();
    }

    public async void stop () throws Error {
        if (!running)
            return;

        dmabuf = null;

        input_handler.stop_rumble ();

        yield process.stop ();
        proxy = null;
        stopped (null);

        if (save_location != null) {
            yield FileUtils.delete_recursively (save_location.get_parent ());
            save_location = null;
        }
    }

    public async void reset (bool hard) throws Error requires (running) {
        input_handler.stop_rumble ();

        yield delegate.before_reset (hard);
        yield proxy.reset (hard);
        yield delegate.after_reset (hard);
    }

    public async void pause () throws Error requires (running) {
        if (paused)
            return;

        paused = true;
        yield proxy.pause ();
        yield delegate.pause ();

        input_handler.stop_rumble ();
    }

    public async void resume () throws Error requires (running) {
        if (!paused)
            return;

        try {
            yield delegate.resume ();
            yield proxy.resume ();
            paused = false;
        } catch (Error e) {
            stopped (e.message);
        }
    }

    public async bool load_snapshot (
        Snapshot snapshot,
        bool reload_data
    ) throws Error requires (running) {
        if (!snapshot.is_valid ())
            return false;

        try {
            loading_state = true;

            loading_snapshot (snapshot);

            if (reload_data) {
                var old_save_location = save_location;

                save_location = yield create_save_location ();

                yield snapshot.load_data (save_location);
                yield proxy.reload_save (save_location.get_path ());

                yield FileUtils.delete_recursively (
                    old_save_location.get_parent ()
                );
            }

            var savestate_path = snapshot.get_savestate_path ();
            yield proxy.load_state (savestate_path);

            yield delegate.load_state (snapshot.platform_metadata);
        } finally {
            loading_state = false;
        }

        input_handler.stop_rumble ();

        return true;
    }

    public async void save_into_snapshot (
        Snapshot snapshot,
        string? name,
        ScreenshotProvider provider
    ) throws Error requires (running) {
        // Save
        yield proxy.sync_save ();
        yield snapshot.save_data (save_location);

        // Savestate
        var savestate_path = snapshot.get_savestate_path ();
        yield proxy.save_state (savestate_path);

        // Screenshot
        var screenshot = provider.take_raw_screenshot ();
        snapshot.save_screenshot (screenshot);

        // Metadata
        string[] controllers = {};
        for (uint i = 0; i < get_max_players (); i++)
            controllers += get_controller_type (i);

        var platform_metadata = SnapshotPlatformMetadata.create (game);
        if (platform_metadata != null)
            yield delegate.save_state (platform_metadata);

        Firmware[] firmware = {};
        var list = game.platform.firmware_list;
        if (list != null)
            firmware = yield list.get_used_firmware (this);

        snapshot.save_metadata (
            new DateTime.now (),
            core,
            name,
            region,
            controllers,
            flipped,
            get_aspect_ratio (),
            firmware,
            platform_metadata
        );
    }

    public double get_aspect_ratio () requires (running) {
        return aspect_ratio;
    }

    public string? get_controller_type (uint player) {
        return input_handler.get_controller_type (player);
    }

    public void set_controller_type (uint player, string? type) {
        if (type == get_controller_type (player))
            return;

        input_handler.set_controller_type (player, type);

        controller_type_changed (player);
    }

    public bool key_pressed (int keycode) {
        if (running)
            return input_handler.key_pressed (keycode);

        return false;
    }

    public void key_released (int keycode) {
        if (running)
            input_handler.key_released (keycode);
    }

    public void focus_leave () {
        if (running)
            input_handler.focus_leave ();
    }

    public bool drag_begin (int screen_id, double x, double y) {
        if (running)
            return input_handler.drag_begin (screen_id, x, y);

        return false;
    }

    public void drag_update (double x, double y) {
        if (running)
            input_handler.drag_update (x, y);
    }

    public void drag_end () {
        if (running)
            input_handler.drag_end ();
    }

    public void touch_button_pressed (string element) {
        if (running)
            input_handler.touch_button_pressed (element);
    }

    public void touch_button_released (string element) {
        if (running)
            input_handler.touch_button_released (element);
    }

    public void touch_stick_moved (string element, double x, double y) {
        if (running)
            input_handler.touch_stick_moved (element, x, y);
    }

    public void clear_touch_controls () {
        if (input_buffer == null)
            return;

        input_handler.clear_touch_controls ();
    }

    public Gtk.Widget? create_header_widget (FullscreenView view) {
        return delegate.create_header_widget (view);
    }

    public void secondary_view_closed () {
        delegate.secondary_view_closed ();
    }

    public CursorType get_cursor (Screen screen) {
        int index = -1;

        for (int i = 0; i < screens.length; i++) {
            if (screens[i].id == screen.id) {
                index = i;
                break;
            }
        }

        return screen_cursors[index];
    }

    public void set_rumble_enabled (uint player, bool enabled) {
        input_handler.set_rumble_enabled (player, enabled);
    }

    public void get_rumble_indicator (int i, out double strong_magnitude, out double weak_magnitude) {
        input_handler.get_rumble_indicator (i, out strong_magnitude, out weak_magnitude);
    }

    public uint get_n_players () {
        return delegate.get_n_players ();
    }

    public uint get_max_players () {
        return input_handler.get_max_players ();
    }

    public uint get_frame_number () {
        return frame_number;
    }
}
