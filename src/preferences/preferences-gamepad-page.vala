// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/preferences/preferences-gamepad-page.ui")]
private class Highscore.PreferencesGamepadPage : Adw.NavigationPage {
    public Gamepad gamepad { get; construct; }

    [GtkChild]
    private unowned GamepadMappingView view;

    public PreferencesGamepadPage (Gamepad gamepad) {
        Object (gamepad: gamepad);
    }

    construct {
        view.gamepad = gamepad;

        gamepad.bind_property ("name", this, "title", SYNC_CREATE);

        view.notify["modified"].connect (() => {
            action_set_enabled ("gamepad.reset", view.modified);
        });

        gamepad.notify["connected"].connect (() => {
            action_set_enabled ("gamepad.forget", !gamepad.connected);
        });

        action_set_enabled ("gamepad.reset", view.modified);
        action_set_enabled ("gamepad.forget", !gamepad.connected);
    }

    static construct {
        install_action ("gamepad.reset", null, widget => {
            var self = widget as PreferencesGamepadPage;

            self.view.reset ();
        });
        install_action ("gamepad.forget", null, widget => {
            var self = widget as PreferencesGamepadPage;

            GamepadManager.get_instance ().forget_gamepad (self.gamepad);

            self.activate_action ("navigation.pop", null, null);
        });
    }
}
