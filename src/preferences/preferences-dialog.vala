// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/preferences/preferences-dialog.ui")]
public class Highscore.PreferencesDialog : Adw.Dialog {
    [GtkChild]
    private unowned Adw.ToastOverlay toast_overlay;
    [GtkChild]
    private unowned Adw.NavigationSplitView split_view;
    [GtkChild]
    private unowned Adw.ViewStack stack;
    [GtkChild]
    private unowned PreferencesFirmwarePage firmware_page;

    private PreferencesPage? last_page;

    public bool firmware_installed {
        get { return firmware_page.firmware_installed; }
    }

    public void show_page (string name) {
        stack.visible_child_name = name;
    }

    public void add_toast (Adw.Toast toast) {
        toast_overlay.add_toast (toast);
    }

    [GtkCallback]
    private void sidebar_activated_cb () {
        split_view.show_content = true;
    }

    [GtkCallback]
    private void stack_page_changed_cb () {
        if (last_page != null)
            last_page.reset ();

        last_page = stack.visible_child as PreferencesPage;
    }
}
