// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/preferences/preferences-general-page.ui")]
public class Highscore.PreferencesGeneralPage : PreferencesPage {
    public string library_dir { get; set; }
    public bool show_empty_platforms { get; set; }

    public bool pause_when_inactive { get; set; }
    public bool load_state_on_startup { get; set; }
    public bool autosave { get; set; }

    public string video_filter { get; set; }
    public bool enable_crt { get; set; }
    public bool interframe_blending { get; set; }

    public bool is_accurate_filter { get; private set; }

    [GtkChild]
    private unowned Adw.ComboRow video_filter_row;
    [GtkChild]
    private unowned VideoFilterModel video_filter_model;

    private Settings settings;

    construct {
        settings = new Settings ("app.drey.Highscore");

        settings.bind ("library-dir", this, "library-dir", DEFAULT);
        settings.bind ("show-empty-platforms", this, "show-empty-platforms", DEFAULT);

        settings.bind ("pause-when-inactive", this, "pause-when-inactive", DEFAULT);
        settings.bind ("load-state-on-startup", this, "load-state-on-startup", DEFAULT);
        settings.bind ("autosave", this, "autosave", DEFAULT);

        settings.bind ("video-filter", this, "video-filter", DEFAULT);
        settings.bind ("enable-crt", this, "enable-crt", DEFAULT);
        settings.bind ("interframe-blending", this, "interframe-blending", DEFAULT);

        video_filter_row.selected = video_filter_model.find_by_id (video_filter);
        is_accurate_filter = video_filter == "accurate";

        notify["video-filter"].connect (() => {
            video_filter_row.selected = video_filter_model.find_by_id (video_filter);
            is_accurate_filter = video_filter == "accurate";
        });
    }

    static construct {
        install_action ("preferences.select-library-folder", null, widget => {
            var self = widget as PreferencesGeneralPage;

            self.select_library_folder.begin ();
        });
    }

    private async void select_library_folder () {
        var dialog = new Gtk.FileDialog () {
            title = _("Select Library Folder"),
            modal = true,
        };

        File folder = null;

        try {
            folder = yield dialog.select_folder (get_root () as Gtk.Window, null);
        } catch (Gtk.DialogError e) {
            return;
        } catch (Error e) {
            show_toast (new Adw.Toast.format (
                _("Failed to select library folder: %s"), e.message
            ));
            return;
        }

        settings.set_string ("library-dir", folder.get_path ());
    }
}
