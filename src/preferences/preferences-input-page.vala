// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.PreferencesGamepadRow : Adw.ActionRow {
    public Gamepad gamepad { get; construct; }

    public PreferencesGamepadRow (Gamepad gamepad) {
        Object (gamepad: gamepad);
    }

    construct {
        activatable = true;

        gamepad.bind_property ("name", this, "title", SYNC_CREATE);
        gamepad.bind_property (
            "connected", this, "subtitle", SYNC_CREATE,
            (binding, from_value, ref to_value) => {
                if (from_value.get_boolean ())
                    to_value = _("Connected");
                else
                    to_value = "";

                return true;
            }
        );

        var image = new Gtk.Image () {
            icon_name = "go-next-symbolic",
        };

        add_suffix (image);
    }
}

[GtkTemplate (ui = "/app/drey/Highscore/preferences/preferences-input-page.ui")]
public class Highscore.PreferencesInputPage : PreferencesPage {
    [GtkChild]
    private unowned Gtk.ListBox gamepads_list;
    [GtkChild]
    private unowned Gtk.ListBox no_gamepads_list;
    [GtkChild]
    private unowned Adw.PreferencesGroup controllers_group;
    [GtkChild]
    private unowned Adw.NavigationView nav_view;

    public bool touch_controls { get; set; }
    public bool touch_feedback { get; set; }

    public bool touch_feedback_available {
        get {
            return Lfb.is_initted ();
        }
    }

    private Settings settings;

    construct {
        settings = new Settings ("app.drey.Highscore");

        settings.bind ("touch-controls", this, "touch-controls", DEFAULT);
        settings.bind ("touch-feedback", this, "touch-feedback", DEFAULT);

        var gamepads = GamepadManager.get_instance ().gamepads;

        gamepads_list.bind_model (gamepads, item => {
            var gamepad = item as Gamepad;

            var row = new PreferencesGamepadRow (gamepad);

            row.activated.connect (() => {
                nav_view.push (new PreferencesGamepadPage (gamepad));
            });

            return row;
        });

        gamepads.items_changed.connect (() => {
            gamepads_list.visible = gamepads.n_items > 0;
            no_gamepads_list.visible = gamepads.n_items == 0;
        });

        gamepads_list.visible = gamepads.n_items > 0;
        no_gamepads_list.visible = gamepads.n_items == 0;

        for (uint i = 0; i < ControllerManager.N_PLAYERS; i++)
            controllers_group.add (new PreferencesControllerRow (i));
    }

    public override void reset () {
        nav_view.pop_to_tag ("root");
    }
}
