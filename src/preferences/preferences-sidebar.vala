// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PreferencesSidebar : Adw.Bin {
    public signal void activated ();

    private Adw.ViewStack _stack;
    public Adw.ViewStack stack {
        get { return _stack; }
        set {
            if (pages != null)
                pages.notify["selected-page"].disconnect (selected_page_changed_cb);

            _stack = value;
            pages = stack.pages;
            rebuild ();

            if (pages != null)
                pages.notify["selected-page"].connect (selected_page_changed_cb);
        }
    }

    public bool is_sidebar { get; set; default = true; }

    private Adw.PreferencesPage pref_page;
    private Gtk.ScrolledWindow scrolled_window;
    private Gtk.ListBox list;

    private Gtk.SelectionModel pages;

    construct {
        notify["is-sidebar"].connect (rebuild);
        rebuild ();
    }

    static construct {
        set_css_name ("preferences-sidebar");
    }

    private Gtk.Widget create_sidebar_row (Object object) {
        var page = object as Adw.ViewStackPage;

        var row = new Gtk.ListBoxRow ();

        var box = new Gtk.Box (HORIZONTAL, 0);
        row.child = box;

        var icon = new Gtk.Image.from_icon_name (page.icon_name);
        icon.add_css_class ("sidebar-icon");
        box.append (icon);

        var title = new Gtk.Label (page.title) {
            ellipsize = END,
            xalign = 0,
            hexpand = true,
        };
        box.append (title);

        return row;
    }

    private Gtk.Widget create_boxed_list_row (Object object) {
        var page = object as Adw.ViewStackPage;

        var row = new Adw.ActionRow () {
            activatable = true,
            title = page.title,
        };

        var icon = new Gtk.Image.from_icon_name (page.icon_name) {
            valign = CENTER,
        };
        row.add_prefix (icon);

        var arrow = new Gtk.Image.from_icon_name ("go-next-symbolic");
        row.add_suffix (arrow);

        return row;
    }

    private void rebuild () {
        if (pref_page != null) {
            pref_page.can_focus = false;
            pref_page = null;
        }

        if (scrolled_window != null) {
            list = null;

            scrolled_window.can_focus = false;
            scrolled_window = null;
        }

        child = null;

        if (stack == null)
            return;

        if (is_sidebar) {
            scrolled_window = new Gtk.ScrolledWindow () {
                hscrollbar_policy = NEVER,
            };
            child = scrolled_window;

            list = new Gtk.ListBox ();
            list.add_css_class ("navigation-sidebar");
            list.row_selected.connect (selected_cb);
            list.row_activated.connect (activated_cb);
            scrolled_window.child = list;

            list.bind_model (pages, create_sidebar_row);

            selected_page_changed_cb ();
        } else {
            pref_page = new Adw.PreferencesPage ();
            child = pref_page;

            var group = new Adw.PreferencesGroup ();
            pref_page.add (group);

            var boxed_list = new Gtk.ListBox () {
                selection_mode = NONE,
            };
            boxed_list.add_css_class ("boxed-list");
            boxed_list.row_activated.connect (activated_cb);
            group.add (boxed_list);

            boxed_list.bind_model (pages, create_boxed_list_row);
        }
    }

    private void selected_page_changed_cb () {
        if (list == null)
            return;

        for (uint i = 0; i < pages.get_n_items (); i++) {
            if (pages.is_selected (i)) {
                list.select_row (list.get_row_at_index ((int) i));
                return;
            }
        }
    }

    private void selected_cb (Gtk.ListBoxRow? row) {
        if (row == null)
            return;

        int index = row.get_index ();
        pages.select_item (index, true);
    }

    private void activated_cb (Gtk.ListBoxRow row) {
        selected_cb (row);
        activated ();
    }
}
