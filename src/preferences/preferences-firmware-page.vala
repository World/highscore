// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.PreferencesFirmwareRow : Adw.ActionRow {
    public Firmware firmware { get; construct; }
    public bool installed { get; set; }

    private Gtk.Label status;

    public PreferencesFirmwareRow (Firmware firmware) {
        Object (firmware: firmware);
    }

    construct {
        title = firmware.name;

        status = new Gtk.Label (null) {
            ellipsize = END,
        };
        add_suffix (status);

        update_status ();

        notify["installed"].connect (update_status);
    }

    private void update_status () {
        if (installed) {
            status.label = _("Installed");
            status.add_css_class ("dimmed");
            status.remove_css_class ("error");
        } else {
            status.label = _("Missing");
            status.add_css_class ("error");
            status.remove_css_class ("dimmed");
        }
    }
}

[GtkTemplate (ui = "/app/drey/Highscore/preferences/preferences-firmware-page.ui")]
public class Highscore.PreferencesFirmwarePage : PreferencesPage {
    [GtkChild]
    private unowned Adw.PreferencesPage contents;

    public bool firmware_installed { get; private set; }

    private HashTable<Firmware, PreferencesFirmwareRow> rows;
    private Firmware[] all_firmware;

    construct {
        all_firmware = {};
        rows = new HashTable<Firmware, PreferencesFirmwareRow> (direct_hash, direct_equal);

        var platforms = PlatformRegister.get_register ().get_all_platforms ();
        foreach (var platform in platforms) {
            var list = platform.firmware_list;
            if (list == null)
                continue;

            var firmware = list.list_firmware ();

            if (firmware.length == 0)
                continue;

            var group = new Adw.PreferencesGroup () {
                title = platform.name,
            };

            foreach (var f in firmware) {
                var row = new PreferencesFirmwareRow (f);
                rows[f] = row;
                group.add (row);

                try {
                    f.check ();
                    row.installed = true;
                } catch (Error e) {
                }

                all_firmware += f;
            }

            contents.add (group);
        }
    }

    static construct {
        install_action ("firmware.install", null, widget => {
            var self = widget as PreferencesFirmwarePage;

            self.open_and_install_firmware.begin ();
        });
    }

    private async void open_and_install_firmware () {
        var root = get_root () as Gtk.Window;
        var dialog = new Gtk.FileDialog ();
        File? file = null;

        dialog.title = _("Select Firmware");

        try {
            file = yield dialog.open (root, null);
        } catch (Gtk.DialogError e) {
            return;
        } catch (Error e) {
            show_toast (new Adw.Toast (e.message));
            return;
        }

        if (file == null)
            return;

        yield install_file (file);
    }

    private async void install_file (File file) {
        Bytes bytes;

        try {
            bytes = yield file.load_bytes_async (null, null);
        } catch (Error e) {
            show_toast (new Adw.Toast (e.message));
            return;
        }

        var md5 = Checksum.compute_for_bytes (MD5, bytes);
        var sha512 = Checksum.compute_for_bytes (SHA512, bytes);

        var data_dir = Environment.get_user_data_dir ();
        var bios_path = Path.build_filename (data_dir, "highscore", "bios");
        var bios_dir = File.new_for_path (bios_path);

        if (!bios_dir.query_exists ()) {
            try {
                bios_dir.make_directory_with_parents ();
            } catch (Error e) {
                show_toast (new Adw.Toast (e.message));
                return;
            }
        }

        bool did_install = false;
        bool did_reinstall = false;

        foreach (var f in all_firmware) {
            if (!f.matches (md5, sha512))
                continue;

            var dest = f.get_file ();

            try {
                f.check ();
                did_reinstall = true;
                yield dest.delete_async ();
            } catch (Error e) {}

            try {
                yield file.copy_async (dest, TARGET_DEFAULT_PERMS);
            } catch (Error e) {
                show_toast (new Adw.Toast (e.message));
                return;
            }

            f.changed ();

            rows[f].installed = true;

            did_install = true;
            firmware_installed = true;
        }

        if (!did_install)
            show_toast (new Adw.Toast (_("Not a firmware")));

        if (did_reinstall)
            show_toast (new Adw.Toast (_("Replaced existing firmware")));
    }

    private async void install_files (Gdk.FileList list) {
        foreach (var file in list.get_files ())
            yield install_file (file);
    }

    [GtkCallback]
    private bool firmware_drop_cb (Gdk.FileList list) {
        install_files.begin (list);

        return true;
    }
}
