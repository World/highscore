// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.PreferencesPage : Adw.Bin {
    protected void show_toast (Adw.Toast toast) {
        var dialog = get_ancestor (typeof (PreferencesDialog)) as PreferencesDialog;

        assert (dialog != null);

        dialog.add_toast (toast);
    }

    public virtual void reset () {
    }
}
