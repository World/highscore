// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PreferencesControllerRow : Adw.ActionRow {
    private const int N_BUILTIN = 4;

    public uint player { get; construct; }

    private Adw.Bin dropdown_bin;

    private ControllerManager manager;
    private Gtk.Popover popover;
    private Gtk.ListBox listbox;

    private Gtk.ListBoxRow auto_row;
    private Gtk.ListBoxRow none_row;
    private Gtk.ListBoxRow keyboard_row;
    private Gtk.ListBoxRow touchscreen_row;

    public PreferencesControllerRow (uint player) {
        Object (player: player);
    }

    construct {
        title = _("Player %u").printf (player + 1);
        activatable = true;

        var dropdown = new Gtk.Image.from_icon_name ("pan-down-symbolic");

        dropdown_bin = new Adw.Bin () {
            child = dropdown,
        };
        add_suffix (dropdown_bin);

        manager = ControllerManager.get_instance ();
        subtitle = manager.get_display_string (player);

        manager.controller_changed.connect (controller_changed);

        popover = new Gtk.Popover ();
        popover.add_css_class ("menu");
        popover.add_css_class ("controllers-list");
        popover.set_parent (dropdown_bin);

        popover.closed.connect (() => remove_css_class ("has-open-popup"));

        var swindow = new Gtk.ScrolledWindow () {
            hscrollbar_policy = NEVER,
            propagate_natural_width = true,
            propagate_natural_height = true,
            max_content_height = 500,
        };
        popover.child = swindow;

        listbox = new Gtk.ListBox ();
        swindow.child = listbox;

        listbox.append (auto_row = make_row (_("Auto")));
        listbox.append (none_row = make_row (_("None")));
        listbox.append (keyboard_row = make_row (_("Keyboard")));
        listbox.append (touchscreen_row = make_row (_("Touchscreen")));

        update_touchscreen ();

        manager.notify["touch-enabled"].connect (update_touchscreen);

        var gamepad_manager = GamepadManager.get_instance ();
        uint n = gamepad_manager.gamepads.n_items;
        for (uint i = 0; i < n; i++) {
            var gamepad = gamepad_manager.gamepads.get_item (i) as Gamepad;

            gamepad_added (gamepad);
        }

        gamepad_manager.gamepad_added.connect (gamepad_added);
        gamepad_manager.gamepad_forgotten.connect (gamepad_forgotten);

        listbox.set_header_func ((row, before) => {
            var last_row = touchscreen_row.visible ? touchscreen_row : keyboard_row;
            if (before == last_row)
                row.set_header (new Gtk.Separator (HORIZONTAL));
            else
                row.set_header (null);
        });

        listbox.row_activated.connect (row => {
            if (row == auto_row) {
                set_controller (true, null, null);
            } else if (row == none_row) {
                set_controller (false, NONE, null);
            } else if (row == keyboard_row) {
                set_controller (false, KEYBOARD, null);
            } else if (row == touchscreen_row) {
                set_controller (false, TOUCHSCREEN, null);
            } else {
                var index = row.get_index () - N_BUILTIN;
                var gamepad = gamepad_manager.gamepads.get_item (index) as Gamepad;

                set_controller (false, GAMEPAD, gamepad);
            }

            popover.popdown ();
        });

        update_checkmarks ();
    }

    ~PreferencesControllerRow () {
        var gamepad_manager = GamepadManager.get_instance ();
        uint n = gamepad_manager.gamepads.n_items;
        for (uint i = 0; i < n; i++) {
            var gamepad = gamepad_manager.gamepads.get_item (i) as Gamepad;

            gamepad.notify["name"].disconnect (update_gamepad_row);
        }

        gamepad_manager.gamepad_added.disconnect (gamepad_added);
        gamepad_manager.gamepad_forgotten.disconnect (gamepad_forgotten);

        manager.controller_changed.disconnect (controller_changed);
        manager.notify["touch-enabled"].disconnect (update_touchscreen);
    }

    private void controller_changed (uint p) {
        if (p != player)
            return;

        subtitle = manager.get_display_string (player);
        update_checkmarks ();
    }

    private void gamepad_added (Gamepad gamepad) {
        gamepad.notify["name"].connect (update_gamepad_row);

        listbox.append (make_row (gamepad.name));
    }

    private void gamepad_forgotten (Gamepad gamepad, uint index) {
        var row = listbox.get_row_at_index ((int) index + N_BUILTIN);

        gamepad.notify["name"].disconnect (update_gamepad_row);

        listbox.remove (row);
    }

    private void update_gamepad_row (Object object, ParamSpec pspec) {
        var gamepad_manager = GamepadManager.get_instance ();
        var gamepad = object as Gamepad;

        uint index = Gtk.INVALID_LIST_POSITION;
        uint n = gamepad_manager.gamepads.n_items;
        for (uint i = 0; i < n; i++) {
            var g = gamepad_manager.gamepads.get_item (i) as Gamepad;

            if (g == gamepad) {
                index = i;
                break;
            }
        }

        assert (index != Gtk.INVALID_LIST_POSITION);

        var row = listbox.get_row_at_index ((int) index + N_BUILTIN);
        var label = row.get_data<Gtk.Label> ("label");

        label.label = gamepad.name;

        if (manager.get_gamepad (player) == gamepad)
            subtitle = manager.get_display_string (player);
    }

    private void update_checkmarks () {
        var gamepad_manager = GamepadManager.get_instance ();
        int selected_index = -1;

        if (manager.get_automatic (player)) {
            selected_index = auto_row.get_index ();
        } else if (manager.get_controller_type (player) == NONE) {
            selected_index = none_row.get_index ();
        } else if (manager.get_controller_type (player) == KEYBOARD) {
            selected_index = keyboard_row.get_index ();
        } else if (manager.get_controller_type (player) == TOUCHSCREEN) {
            selected_index = touchscreen_row.get_index ();
        } else {
            var gamepad = manager.get_gamepad (player);
            uint n = gamepad_manager.gamepads.n_items;
            for (uint i = 0; i < n; i++) {
                var g = gamepad_manager.gamepads.get_item (i) as Gamepad;

                if (g == gamepad) {
                    selected_index = (int) i + N_BUILTIN;
                    break;
                }
            }
        }

        int i = 0;
        Gtk.ListBoxRow row;
        while ((row = listbox.get_row_at_index (i)) != null) {
            var checkmark = row.get_data<Gtk.Image> ("checkmark");

            checkmark.opacity = (i == selected_index) ? 1 : 0;

            i++;
        }
    }

    private Gtk.ListBoxRow make_row (string title) {
        var label = new Gtk.Label (title) {
            ellipsize = END,
            xalign = 0,
            max_width_chars = 20,
        };

        var checkmark = new Gtk.Image.from_icon_name ("object-select-symbolic");

        var box = new Gtk.Box (HORIZONTAL, 0);
        box.append (label);
        box.append (checkmark);

        var row = new Gtk.ListBoxRow () {
            activatable = true,
            child = box,
        };

        row.set_data<Gtk.Label> ("label", label);
        row.set_data<Gtk.Image> ("checkmark", checkmark);

        return row;
    }

    private void set_controller (bool auto, ControllerManager.ControllerType? type, Gamepad? gamepad) {
        manager.set_controller (player, auto, type, gamepad);
    }

    protected override void dispose () {
        if (popover != null) {
            popover.unparent ();
            popover = null;
        }

        base.dispose ();
    }

    protected override void activate () {
        popover.popup ();

        add_css_class ("has-open-popup");
    }

    private void update_touchscreen () {
        touchscreen_row.visible = manager.touch_enabled;
    }
}
