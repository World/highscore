// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/properties/properties-main-page.ui")]
public class Highscore.PropertiesMainPage : PropertiesPage {
    [GtkChild]
    private unowned Gtk.Box main_box;

    public bool interframe_blending { get; set; }

    public PropertiesMainPage (Game game) {
        Object (game: game);
    }

    construct {
        var platform_props = create_platform_properties ();

        if (platform_props != null)
            main_box.append (platform_props);

        action_set_enabled (
            "game.remove-custom-cover",
            Library.get_instance ().has_cover_override (game)
        );
    }

    static construct {
        install_action ("properties.preferences", null, widget => {
            var self = widget as PropertiesMainPage;

            self.push_page (new PropertiesPreferencesPage (self.game));
        });

        install_action ("game.edit-title", null, widget => {
            var self = widget as PropertiesMainPage;
            self.edit_title.begin ();
        });

        install_action ("game.set-custom-cover", null, widget => {
            var self = widget as PropertiesMainPage;
            self.set_custom_cover.begin ();
        });

        install_action ("game.remove-custom-cover", null, widget => {
            var self = widget as PropertiesMainPage;
            self.remove_custom_cover.begin ();
        });
    }

    private async void edit_title () {
        var dialog = new Adw.AlertDialog (
            _("Edit Title"),
            _("Enter a new title for this game")
        );

        dialog.add_response ("cancel", _("_Cancel"));
        dialog.add_response ("rename", _("_Rename"));

        dialog.set_response_appearance ("rename", SUGGESTED);

        dialog.default_response = "rename";

        var initial_title = game.title;

        var group = new Adw.PreferencesGroup ();
        var entry = new Adw.EntryRow () {
            title = _("Enter Name"),
            activates_default = true,
            text = initial_title,
        };

        var reset_button = new Gtk.Button.from_icon_name ("edit-undo-symbolic") {
            tooltip_text = _("Reset title"),
            valign = CENTER,
            sensitive = initial_title != game.default_title,
            focus_on_click = false,
        };
        reset_button.add_css_class ("flat");
        entry.add_suffix (reset_button);

        reset_button.clicked.connect (() => {
            entry.text = game.default_title;
        });

        group.add (entry);

        dialog.set_response_enabled ("rename", false);

        entry.notify["text"].connect (() => {
            dialog.set_response_enabled (
                "rename", entry.text != "" && entry.text != initial_title
            );
            reset_button.sensitive = entry.text != game.default_title;
        });

        dialog.extra_child = group;

        var response = yield dialog.choose (this, null);

        if (response == "rename") {
            var text = entry.text;

            try {
                var library = Library.get_instance ();

                if (text == game.default_title)
                    library.rename_game (game, null);
                else
                    library.rename_game (game, text);
            } catch (Error e) {
                add_toast (new Adw.Toast.format (
                    _("Failed to rename game: %s"), e.message
                ));
            }
        }
    }

    private async void set_custom_cover () {
        var root = get_root () as Gtk.Window;
        var dialog = new Gtk.FileDialog ();
        File? file = null;

        dialog.title = _("Select Cover");

        var filters = new ListStore (typeof (Gtk.FileFilter));

        var filter = new Gtk.FileFilter ();
        filter.name = _("Image Files");
        filter.add_mime_type ("image/png");
        filter.add_mime_type ("image/jpeg");
        filter.add_mime_type ("image/tiff");
        filters.append (filter);

        dialog.filters = filters;

        try {
            file = yield dialog.open (root, null);
        } catch (Gtk.DialogError e) {
            return;
        } catch (Error e) {
            add_toast (new Adw.Toast (e.message));
            return;
        }

        if (file == null)
            return;

        try {
            yield Library.get_instance ().override_cover (game, file);
        } catch (Error e) {
            add_toast (new Adw.Toast.format (
                _("Failed to set custom cover: %s"), e.message
            ));
        }

        action_set_enabled ("game.remove-custom-cover", true);
    }

    private async void remove_custom_cover () {
        try {
            yield Library.get_instance ().remove_cover_override (game);
        } catch (Error e) {
            add_toast (new Adw.Toast.format (
                _("Failed to remove custom cover: %s"), e.message
            ));
        }

        action_set_enabled ("game.remove-custom-cover", false);
    }

    private PlatformProperties? create_platform_properties () {
        var platform = game.platform;
        var type = platform.properties_type;

        if (type == Type.NONE && platform.parent != null)
            type = platform.parent.properties_type;

        if (type == Type.NONE)
            return null;

        return Object.new (
            type, game: game
        ) as PlatformProperties;
    }
}
