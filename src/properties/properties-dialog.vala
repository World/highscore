// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/properties/properties-dialog.ui")]
public class Highscore.PropertiesDialog : Adw.Dialog {
    public Game game { get; construct; }

    [GtkChild]
    private unowned Adw.ToastOverlay toast_overlay;
    [GtkChild]
    private unowned Adw.NavigationView nav_view;

    public PropertiesDialog (Game game) {
        Object (game: game);
    }

    construct {
        push_page (new PropertiesMainPage (game));
    }

    public void add_toast (Adw.Toast toast) {
        toast_overlay.add_toast (toast);
    }

    public void push_page (PropertiesPage page) {
        nav_view.push (page);
    }

    public void pop_page () {
        nav_view.pop ();
    }
}
