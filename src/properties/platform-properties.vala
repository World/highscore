// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlatformProperties : Adw.Bin {
    public Game game { get; construct; }

    private inline PropertiesDialog get_dialog () {
        var dialog = get_ancestor (typeof (PropertiesDialog)) as PropertiesDialog;

        assert (dialog != null);

        return dialog;
    }

    protected void show_toast (Adw.Toast toast) {
        get_dialog ().add_toast (toast);
    }

    protected void push_page (PropertiesPage page) {
        get_dialog ().push_page (page);
    }
}
