// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/properties/properties-preferences-page.ui")]
public class Highscore.PropertiesPreferencesPage : PropertiesPage {
    [GtkChild]
    private unowned Gtk.Label description;
    [GtkChild]
    private unowned Adw.ComboRow video_filter_row;
    [GtkChild]
    private unowned VideoFilterModel video_filter_model;

    public bool load_state_on_startup { get; set; }
    public bool load_state_on_startup_modified { get; set; }

    public string video_filter { get; set; }

    public bool interframe_blending { get; set; }
    public bool interframe_blending_modified { get; set; }

    private GameSettings game_settings;

    public PropertiesPreferencesPage (Game game) {
        Object (game: game);
    }

    construct {
        description.label = _("Change preferences for %s").printf (game.title);

        game_settings = new GameSettings (game);

        game_settings.bind_property (
            "load-state-on-startup", this,
            "load-state-on-startup", SYNC_CREATE | BIDIRECTIONAL
        );

        game_settings.bind_property (
            "load-state-on-startup-modified", this,
            "load-state-on-startup-modified", SYNC_CREATE
        );

        game_settings.bind_property (
            "local-video-filter", this,
            "video-filter", SYNC_CREATE | BIDIRECTIONAL
        );

        game_settings.bind_property (
            "interframe-blending", this,
            "interframe-blending", SYNC_CREATE | BIDIRECTIONAL
        );

        game_settings.bind_property (
            "interframe-blending-modified", this,
            "interframe-blending-modified", SYNC_CREATE
        );

        video_filter_row.selected = video_filter_model.find_by_id (video_filter);

        notify["video-filter"].connect (() => {
            video_filter_row.selected = video_filter_model.find_by_id (video_filter);
        });
    }

    [GtkCallback]
    private void reset_load_state_on_startup () {
        game_settings.reset_load_state_on_startup ();
    }

    [GtkCallback]
    private void reset_interframe_blending () {
        game_settings.reset_interframe_blending ();
    }
}
