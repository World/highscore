// This file is part of Highscore. License: GPL-3.0-or-later

public interface Highscore.RunnerWindow : Gtk.Window {
    public abstract Runner? runner { get; }
}
