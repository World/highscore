// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.SnapshotManager : Object {
    private const uint MAX_AUTOSAVES = 10;

    [Flags]
    public enum SaveFlags {
        AUTOMATIC,
        MARK_AS_AUTOLOAD,
    }

    public Game game { get; construct; }
    public unowned Core? core { get; construct; }
    public bool readonly { get; construct; }

    public Snapshot? autoload_snapshot { get; set; }

    private ListStore snapshots;

    private static Regex new_snapshot_regex = null;

    public SnapshotManager (Game game, Core? core, bool readonly = false) {
        Object (game: game, core: core, readonly: readonly);
    }

    private File get_save_dir () {
        var data_dir = Environment.get_user_data_dir ();
        var save_dir_path = Path.build_filename (
            data_dir, "highscore", "saves", game.uid
        );

        return File.new_for_path (save_dir_path);
    }

    construct {
        snapshots = new ListStore (typeof (Snapshot));
    }

    private void check_compatible (Snapshot snapshot) throws Error {
        if (core == null)
            return;

        if (core.filename != snapshot.core_name)
            snapshot.set_incompatible (CORE);

        if (core.state_version != snapshot.state_version)
            snapshot.set_incompatible (VERSION);

        var list = game.platform.firmware_list;
        if (list == null)
            return;

        var keys = snapshot.firmware_checksums.get_keys_as_array ();
        if (keys.length == 0)
            return;

        foreach (var k in keys) {
            var sums = snapshot.firmware_checksums[k];
            var f = list.get_firmware (k);

            if (f.get_md5sum () != sums.md5 || f.get_sha512sum () != sums.sha512) {
                snapshot.set_incompatible (BIOS);
                return;
            }
        }
    }

    public async void scan () {
        var save_dir = get_save_dir ();
        if (!save_dir.query_exists ())
            return;

        snapshots.remove_all ();

        try {
            var enumerator = yield save_dir.enumerate_children_async (
                FileAttribute.STANDARD_NAME, FileQueryInfoFlags.NONE
            );

            for (var info = enumerator.next_file (); info != null; info = enumerator.next_file ()) {
                var name = info.get_name ();
                var child = save_dir.get_child (name);
                var snapshot = new Snapshot (this, child);

                if (!snapshot.is_valid ())
                    continue;

                try {
                    snapshot.load_metadata ();
                } catch (Error e) {
                    warning ("Invalid snapshot: %s", e.message);
                    continue;
                }

                check_compatible (snapshot);
                snapshots.append (snapshot);
            }

            snapshots.sort ((CompareDataFunc<Object>) Snapshot.compare);
        } catch (Error e) {
            critical ("Failed to enumerate snapshots: %s", e.message);
        }
    }

    public Snapshot? get_latest (bool include_incompatible) {
        uint n = snapshots.get_n_items ();

        for (uint i = 0; i < n; i++) {
            var snapshot = snapshots.get_item (i) as Snapshot;

            if (snapshot.is_compatible () || include_incompatible)
                return snapshot;
        }

        return null;
    }

    public ListStore get_snapshots () {
        return snapshots;
    }

    private Snapshot create_snapshot () throws Error {
        var date = new DateTime.now_utc ().format ("%FT%T");
        var save_dir = get_save_dir ().get_child (date);

        while (save_dir.query_exists ()) {
            date = @"$(date)_";
            save_dir = get_save_dir ().get_child (date);
        }

        var snapshot = new Snapshot (this, save_dir);
        snapshot.ensure_dir ();

        return snapshot;
    }

    public async Snapshot save_snapshot (
        Runner runner,
        ScreenshotProvider provider,
        SaveFlags flags
    ) throws Error requires (!readonly) {
        var snapshot = create_snapshot ();

        string? name = null;
        if (!(AUTOMATIC in flags))
            name = make_name ();

        yield runner.save_into_snapshot (snapshot, name, provider);

        snapshots.insert_sorted (
            snapshot, (CompareDataFunc<Object>) Snapshot.compare
        );

        if (MARK_AS_AUTOLOAD in flags)
            autoload_snapshot = snapshot;

        return snapshot;
    }

    public async void trim_autosaves () throws Error {
        if (readonly)
            return;

        uint n = snapshots.get_n_items ();
        uint n_autosaves = MAX_AUTOSAVES;

        for (uint i = 0; i < n; i++) {
            var snapshot = snapshots.get_item (i) as Snapshot;

            if (!snapshot.automatic)
                continue;

             if (snapshot == autoload_snapshot) {
                if (n_autosaves > 0)
                    n_autosaves--;
                continue;
            }

            if (n_autosaves > 0) {
                n_autosaves--;
                continue;
            }

            yield delete_snapshot (snapshot);
            i--;
            n--;
        }
    }

    public async void delete_snapshot (Snapshot snapshot) throws Error requires (!readonly) {
         uint index;
         if (!snapshots.find (snapshot, out index)) {
             critical ("Trying to delete a nonexistent snapshot");
             return;
         }

         snapshots.remove (index);

         if (snapshot == autoload_snapshot)
            autoload_snapshot = null;

         yield snapshot.delete_files ();
    }

    public bool is_valid_name (string name) {
        var stripped_name = name.strip ();

        if (stripped_name == "" || stripped_name == _("Autosave"))
            return false;

        uint n = snapshots.get_n_items ();

        for (uint i = 0; i < n; i++) {
            var snapshot = snapshots.get_item (i) as Snapshot;

            if (snapshot.name == stripped_name)
                return false;
        }

        return true;
    }

    private string make_name () {
        if (new_snapshot_regex == null) {
            try {
                new_snapshot_regex = new Regex (_("New Snapshot %s").printf ("([1-9]\\d*)"));
            } catch (Error e) {
                error ("Invalid regex: %s", e.message);
            }
        }

        var numbers = new List<int> ();
        uint n = snapshots.get_n_items ();

        for (uint i = 0; i < n; i++) {
            var snapshot = snapshots.get_item (i) as Snapshot;
            if (snapshot.name == null)
                continue;

            MatchInfo match_info = null;
            if (new_snapshot_regex.match (snapshot.name, 0, out match_info)) {
                var number = match_info.fetch (1);
                numbers.prepend (int.parse (number));
            }
        }

        numbers.sort ((a, b) => a - b);

        int next_number = 1;

        foreach (var number in numbers) {
            if (number != next_number)
                break;

            next_number++;
        }

        return _("New Snapshot %s").printf (next_number.to_string ());
    }
}
