// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/snapshot/snapshot-row.ui")]
public class Highscore.SnapshotRow : Adw.Bin {
    public new unowned Snapshot? snapshot { get; set; }

    public bool compatible { get; set; }

    [GtkChild]
    private unowned Gtk.MenuButton menu_button;

    static construct {
        set_css_name ("snapshot-row");

        install_action ("snapshot.rename", null, widget => {
            var self = widget as SnapshotRow;

            self.rename.begin ();
        });

        install_action ("snapshot.delete", null, widget => {
            var self = widget as SnapshotRow;

            self.delete.begin ();
        });
    }

    private async void rename () {
        var manager = snapshot.manager;

        string subtitle;
        if (snapshot.name == null)
            subtitle = _("Enter a name for this snapshot");
        else
            subtitle = _("Enter new name for %s".printf (snapshot.name));

        var dialog = new Adw.AlertDialog (_("Rename"), subtitle);

        dialog.add_response ("cancel", _("_Cancel"));
        dialog.add_response ("rename", _("_Rename"));

        dialog.set_response_appearance ("rename", SUGGESTED);

        dialog.default_response = "rename";

        string initial_name = snapshot.name ?? "";
        var group = new Adw.PreferencesGroup ();
        var entry = new Adw.EntryRow () {
            title = _("Enter Name"),
            activates_default = true,
            text = initial_name,
        };
        group.add (entry);

        dialog.set_response_enabled ("rename", false);

        entry.notify["text"].connect (() => {
            bool valid = manager.is_valid_name (entry.text);
            bool changed = entry.text.strip () != initial_name;

            dialog.set_response_enabled ("rename", changed && valid);

            if (valid || !changed)
                entry.remove_css_class ("error");
            else
                entry.add_css_class ("error");
        });

        dialog.extra_child = group;

        var response = yield dialog.choose (this, null);

        if (response == "rename")
            snapshot.name = entry.text.strip ();
    }

    private async void delete () {
        var manager = snapshot.manager;

        // TODO have confirmation

        try {
            yield manager.delete_snapshot (snapshot);
        } catch (Error e) {
            critical ("Failed to delete snapshot: %s", e.message);
        }
    }

    [GtkCallback]
    private string get_display_name (string? name, bool automatic) {
        if (name != null)
            return name;

        return _("Autosave");
    }

    [GtkCallback]
    private string get_date_label (DateTime? date) {
        if (date == null)
            return "";

        return date.format (get_date_format (date));
    }

    [GtkCallback]
    private string get_tooltip (Snapshot.IncompatibleFlags flags) {
        if (flags == 0)
            return "";

        if (flags == CORE)
            return _("This snapshot was created with a different core");

        if (flags == VERSION)
            return _("This snapshot was created with an older core version");

        if (flags == BIOS)
            return _("This snapshot was created with different firmware");

        string[] reasons = {};

        if (CORE in flags)
            reasons += _("Different core");
        if (VERSION in flags)
            reasons += _("Older core version");
        if (BIOS in flags)
            reasons += _("Different firmware");

        var builder = new StringBuilder ();

        builder.append (_("This snapshot is incompatible:"));
        builder.append ("\n");

        foreach (var reason in reasons)
            builder.append_printf (" • %s", reason);

        return builder.free_and_steal ();
    }

    [GtkCallback]
    private bool is_compatible (Snapshot? snapshot) {
        return snapshot != null && snapshot.is_compatible ();
    }

    // Adapted from nautilus-file.c, nautilus_file_get_date_as_string()
    private string get_date_format (DateTime date) {
        var date_midnight = new DateTime.local (date.get_year (),
                                                date.get_month (),
                                                date.get_day_of_month (),
                                                0, 0, 0);
        var now = new DateTime.now ();
        var today_midnight = new DateTime.local (
            now.get_year (), now.get_month (), now.get_day_of_month (), 0, 0, 0
        );
        var days_ago = (today_midnight.difference (date_midnight)) / GLib.TimeSpan.DAY;

        if (days_ago == 0) {
            /* Translators: Time in locale format */
            /* xgettext:no-c-format */
            return _("%X");
        }
        else if (days_ago == 1) {
            /* Translators: this is the word Yesterday followed by
             * a time in locale format. i.e. "Yesterday 23:04:35" */
            /* xgettext:no-c-format */
            return _("Yesterday %X");
        }
        else if (days_ago > 1 && days_ago < 7) {
            /* Translators: this is the abbreviated name of the week day followed by
             * a time in locale format. i.e. "Monday 23:04:35" */
            /* xgettext:no-c-format */
            return _("%a %X");
        }
        else if (date.get_year () == now.get_year ()) {
            /* Translators: this is the day of the month followed
             * by the abbreviated month name followed by a time in
             * locale format i.e. "3 Feb 23:04:35" */
            /* xgettext:no-c-format */
            return _("%-e %b %X");
        }
        else {
            /* Translators: this is the day number followed
             * by the abbreviated month name followed by the year followed
             * by a time in locale format i.e. "3 Feb 2015 23:04:00" */
            /* xgettext:no-c-format */
            return _("%-e %b %Y %X");
        }
    }

    [GtkCallback]
    private void menu_notify_active_cb () {
        if (parent == null || parent.parent == null)
            return;

        if (menu_button.active)
            parent.parent.add_css_class ("has-open-popup");
        else
            parent.parent.remove_css_class ("has-open-popup");
    }
}
