// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/snapshot/snapshot-placeholder-row.ui")]
public class Highscore.SnapshotPlaceholderRow : Adw.Bin {
    static construct {
        set_css_name ("snapshot-row");
    }

    [GtkCallback]
    private void click_released_cb (Gtk.GestureClick gesture, int n_press, double x, double y) {
        var list_view = get_ancestor (typeof (Gtk.ListView)) as Gtk.ListView;
        list_view.activate (0);
    }
}
