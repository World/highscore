// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.SnapshotPlatformMetadata : Object {
    public abstract void load (KeyFile keyfile);
    public abstract void save (KeyFile keyfile);

    public static SnapshotPlatformMetadata? create (Game game) {
        var type = game.platform.snapshot_metadata_type;

        if (type == Type.NONE && game.platform.parent != null)
            type = game.platform.parent.snapshot_metadata_type;

        if (type == Type.NONE)
            return null;

        return Object.new (type) as SnapshotPlatformMetadata;
    }
}
