// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Snapshot : Object {
    [Flags]
    public enum IncompatibleFlags {
        CORE,
        VERSION,
        BIOS,
    }

    public unowned SnapshotManager manager { get; construct; }
    public File dir { get; construct; }

    public DateTime creation_date { get; private set; }
    public string core_name { get; private set; }
    public string core_version { get; private set; }
    public string? core_commit { get; private set; }
    public int state_version { get; private set; }
    public string? name { get; set; }
    public bool automatic { get; private set; }
    public IncompatibleFlags incompatible_flags { get; private set; }
    public Hs.Region region { get; private set; }
    public string[] controllers { get; private set; }

    public bool screenshot_flipped { get; private set; }
    public double screenshot_aspect_ratio { get; private set; }

    public HashTable<string, Firmware.PossibleChecksum?> firmware_checksums { get; private set; }
    public SnapshotPlatformMetadata? platform_metadata { get; private set; }

    private bool block_metadata_writes;

    public Snapshot (SnapshotManager manager, File dir) {
        Object (manager: manager, dir: dir);
    }

    construct {
        notify["name"].connect (() => {
            if (block_metadata_writes)
                return;

            automatic = (name == null);

            try {
                write_metadata ();
            } catch (Error e) {
                critical ("Failed to update snapshot name: %s", e.message);
            }
        });

        firmware_checksums = new HashTable<string, Firmware.PossibleChecksum?> (direct_hash, direct_equal);

        controllers = {};
    }

    private File get_save_location () {
        return dir.get_child ("save");
    }

    public File get_savestate_file () {
        return dir.get_child ("savestate");
    }

    private File get_screenshot_file () {
        return dir.get_child ("screenshot");
    }

    private File get_metadata_file () {
        return dir.get_child ("metadata");
    }

    public bool is_valid () {
        if (!dir.query_exists ())
            return false;

        if (!get_savestate_file ().query_exists ())
            return false;

        if (!get_screenshot_file ().query_exists ())
            return false;

        if (!get_metadata_file ().query_exists ())
            return false;

        return true;
    }

    public void ensure_dir () throws Error {
        if (!dir.query_exists ())
            dir.make_directory_with_parents ();
    }

    public string get_savestate_path () {
        return get_savestate_file ().get_path ();
    }

    public async void save_data (File location) throws Error {
        if (!location.query_exists ())
            return;

        var file = get_save_location ();

        if (file.query_exists ())
            yield FileUtils.delete_recursively (file);

        yield FileUtils.copy_recursively (location, file);
    }

    public async void load_data (File location) throws Error {
        var file = get_save_location ();

        if (location.query_exists ())
            yield FileUtils.delete_recursively (location);

        if (file.query_exists ())
            yield FileUtils.copy_recursively (file, location);
    }

    public Gdk.Texture get_screenshot () throws Error {
        var file = get_screenshot_file ();

        return Gdk.Texture.from_file (file);
    }

    public void save_screenshot (Gdk.Texture texture) throws Error {
        var downloader = new Gdk.TextureDownloader (texture);
        downloader.set_format (R8G8B8);

        size_t rowstride;
        var bytes = downloader.download_bytes (out rowstride);

        var pixbuf = new Gdk.Pixbuf.from_data (
            bytes.get_data (),
            RGB,
            false,
            8,
            texture.get_width (),
            texture.get_height (),
            (int) rowstride
        );

        var file = get_screenshot_file ();
        pixbuf.save (file.get_path (), "png", null);
    }

    public void load_metadata () throws Error {
        var metadata_file = get_metadata_file ();

        var keyfile = new KeyFile ();
        keyfile.load_from_file (metadata_file.get_path (), KeyFileFlags.NONE);

        var date_str = keyfile.get_string ("Metadata", "Creation Date");
        creation_date = new DateTime.from_iso8601 (date_str, null) ?? new DateTime.now ();

        core_name = keyfile.get_string ("Metadata", "Core");

        if (keyfile.has_key ("Metadata", "Core Version"))
            core_version = keyfile.get_string ("Metadata", "Core Version");
        else
            core_version = "";

        if (keyfile.has_key ("Metadata", "Core Commit"))
            core_commit = keyfile.get_string ("Metadata", "Core Commit");
        else
            core_commit = null;

        if (keyfile.has_key ("Metadata", "Name")) {
            block_metadata_writes = true;
            try {
                name = keyfile.get_string ("Metadata", "Name");
            } catch (Error e) {
                block_metadata_writes = false;
                throw e;
            }
            block_metadata_writes = false;
        }

        if (keyfile.has_key ("Metadata", "State Version"))
            state_version = keyfile.get_integer ("Metadata", "State Version");
        else
            state_version = 0;

        if (keyfile.has_key ("Metadata", "Automatic"))
            automatic = keyfile.get_boolean ("Metadata", "Automatic");
        else
            automatic = true;

        if (keyfile.has_key ("Metadata", "Region")) {
            var region_str = keyfile.get_string ("Metadata", "Region");

            if (region_str == "NTSC")
                region = NTSC;
            else if (region_str == "PAL")
                region = PAL;
            else
                region = UNKNOWN;
        } else {
            region = UNKNOWN;
        }

        if (keyfile.has_group ("Controllers")) {
            string[] controllers = {};

            for (int i = 0; i < ControllerManager.N_PLAYERS; i++) {
                var key_name = @"Player $(i + 1)";

                if (!keyfile.has_key ("Controllers", key_name))
                    break;

                controllers += keyfile.get_string ("Controllers", key_name);
            }

            this.controllers = controllers;
        } else {
            controllers = {};
        }

        screenshot_flipped = keyfile.get_boolean ("Screenshot", "Flipped");
        screenshot_aspect_ratio = keyfile.get_double ("Screenshot", "Aspect Ratio");

        var list = manager.game.platform.firmware_list;
        if (list != null && keyfile.has_group ("Firmware")) {
            foreach (var k in keyfile.get_keys ("Firmware")) {
                var sums = keyfile.get_string_list ("Firmware", k);
                if (sums.length != 2)
                    continue;
                firmware_checksums[k] = { sums[0], sums[1] };
            }
        }

        platform_metadata = SnapshotPlatformMetadata.create (manager.game);
        if (platform_metadata != null)
            platform_metadata.load (keyfile);
    }

    public void save_metadata (
        DateTime creation_date,
        Core core,
        string? name,
        Hs.Region region,
        string[] controllers,
        bool screenshot_flipped,
        double screenshot_aspect_ratio,
        Firmware[] firmware,
        SnapshotPlatformMetadata? platform_metadata
    ) throws Error {
        block_metadata_writes = true;

        this.creation_date = creation_date;
        this.core_name = core.filename;
        this.core_version = core.version;
        this.core_commit = core.commit;
        this.state_version = core.state_version;
        this.name = name;
        this.automatic = name == null;
        this.region = region;
        this.controllers = controllers;
        this.screenshot_flipped = screenshot_flipped;
        this.screenshot_aspect_ratio = screenshot_aspect_ratio;
        this.platform_metadata = platform_metadata;

        foreach (var f in firmware)
            firmware_checksums[f.id] = { f.get_md5sum (), f.get_sha512sum () };

        block_metadata_writes = false;

        write_metadata ();
    }

    private void write_metadata () throws Error {
        var keyfile = new KeyFile ();

        keyfile.set_string ("Metadata", "Creation Date", creation_date.to_string ());
        keyfile.set_string ("Metadata", "Core", core_name);
        keyfile.set_string ("Metadata", "Core Version", core_version);
        if (core_commit != null)
            keyfile.set_string ("Metadata", "Core Commit", core_commit);
        keyfile.set_boolean ("Metadata", "Automatic", automatic);

        if (name != null)
            keyfile.set_string ("Metadata", "Name", name);

        keyfile.set_integer ("Metadata", "State Version", state_version);

        foreach (var k in firmware_checksums.get_keys ()) {
            Firmware.PossibleChecksum sum = firmware_checksums[k];
            string[] sums = {};
            sums += sum.md5;
            sums += sum.sha512;
            keyfile.set_string_list ("Firmware", k, sums);
        }

        switch (region) {
            case NTSC:
                keyfile.set_string ("Metadata", "Region", "NTSC");
                break;
            case PAL:
                keyfile.set_string ("Metadata", "Region", "PAL");
                break;
            default:
                break;
        }

        for (int i = 0; i < controllers.length; i++)
            keyfile.set_string ("Controllers", @"Player $(i + 1)", controllers[i]);

        keyfile.set_boolean ("Screenshot", "Flipped", screenshot_flipped);
        keyfile.set_double ("Screenshot", "Aspect Ratio", screenshot_aspect_ratio);

        if (platform_metadata != null)
            platform_metadata.save (keyfile);

        var metadata_file = get_metadata_file ();

        if (metadata_file.query_exists ())
            metadata_file.@delete ();

        keyfile.save_to_file (metadata_file.get_path ());
    }

    public void set_incompatible (IncompatibleFlags flags) {
        incompatible_flags |= flags;
    }

    public bool is_compatible () {
        return incompatible_flags == 0;
    }

    public async void delete_files () throws Error {
        yield FileUtils.delete_recursively (dir);
    }

    public static int compare (Snapshot s1, Snapshot s2) {
        /* Inverting because we present snapshots newest to oldest */
        return -strcmp (s1.dir.peek_path (), s2.dir.peek_path ());
    }
}
