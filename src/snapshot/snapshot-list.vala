// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/snapshot/snapshot-list.ui")]
public class Highscore.SnapshotList : Adw.Bin {
    public signal void close ();
    public signal void load (Snapshot snapshot);
    public signal void preview (Snapshot? snapshot);

    public unowned SnapshotManager manager { get; construct; }
    public unowned ScreenshotProvider provider { get; construct; }
    public unowned Runner runner { get; construct; }
    public bool compact { get; set; }

    [GtkChild]
    private unowned Gtk.Stack empty_stack;
    [GtkChild]
    private unowned Gtk.SingleSelection selection;

    private unowned Snapshot? new_snapshot;

    public SnapshotList (SnapshotManager manager, ScreenshotProvider provider, Runner runner) {
        Object (manager: manager, provider: provider, runner: runner);
    }

    construct {
        selection.model = new PlaceholderModel (manager.get_snapshots ());

        action_set_enabled ("snapshot.load", false);

        items_changed_cb ();
    }

    public void deinit () {
        selection.model = null;
    }

    static construct {
        install_action ("snapshot.load", null, widget => {
            var self = widget as SnapshotList;
            var snapshot = self.get_selected_snapshot ();
            if (snapshot != self.new_snapshot)
                self.load (snapshot);
            else
                self.close ();
        });

        install_action ("snapshot.save", null, widget => {
            var self = widget as SnapshotList;
            self.save.begin ();
        });
    }

    [GtkCallback]
    private void close_cb () {
        close ();
    }

    private Snapshot? get_selected_snapshot () {
        if (selection.selected_item == null)
            return null;

        var item = selection.selected_item as PlaceholderModel.Item;
        if (item.is_placeholder)
            return null;

        return item.object as Snapshot;
    }

    [GtkCallback]
    private void activate_cb (uint position) {
        var item = selection.get_item (position) as PlaceholderModel.Item;

        if (item.is_placeholder) {
            save.begin ();
            return;
        }

        var snapshot = item.object as Snapshot;
        if (snapshot != new_snapshot)
            load (snapshot);
        else
            close ();
    }

    [GtkCallback]
    private void notify_selected_cb () {
        var snapshot = get_selected_snapshot ();

        action_set_enabled ("snapshot.load", snapshot != null);

        preview (snapshot);
    }

    [GtkCallback]
    private void items_changed_cb () {
        var model = selection.model as PlaceholderModel;

        if (model == null)
            return;

        bool empty = selection.get_n_items () == (model.placeholder_enabled ? 1 : 0);

        empty_stack.visible_child_name = empty ? "empty" : "snapshots";
    }

    [GtkCallback]
    private string get_row_page_cb (Snapshot? snapshot) {
        return snapshot == null ? "placeholder" : "snapshot";
    }

    private async void save () {
        try {
            new_snapshot = yield manager.save_snapshot (
                runner, provider, MARK_AS_AUTOLOAD
            );

            /* Select the new snapshot */
            uint n = selection.get_n_items ();
            for (uint i = 0; i < n; i++) {
                var item = selection.get_item (i) as PlaceholderModel.Item;

                if (item.is_placeholder)
                    continue;

                var snapshot = item.object as Snapshot;

                if (snapshot == new_snapshot) {
                    selection.selected = i;
                    break;
                }
            }

            var model = selection.model as PlaceholderModel;

            model.placeholder_enabled = false;
        } catch (Error e) {
            critical ("Failed to save state: %s", e.message);
        }
    }

    public bool has_just_saved () {
        return new_snapshot != null;
    }
}
