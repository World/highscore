// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public class Highscore.SnapshotThumbnail : Gtk.GLArea {
    public bool secondary { get; set; }

    private unowned Snapshot _snapshot;
    public new unowned Snapshot snapshot {
        get { return _snapshot; }
        set {
            _snapshot = value;

            if (snapshot != null) {
                screen_set = ScreenSet.create (THUMBNAIL, null, snapshot);

                if (secondary) {
                    set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0));

                    screen_set.secondary_layout_changed.connect (animate => {
                        set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0));
                    });
                } else {
                    set_screen_layout (screen_set.layout);

                    screen_set.layout_changed.connect (animate => {
                        set_screen_layout (screen_set.layout);
                    });
                }

                screens = screen_set.get_screens ();

                aspect_ratio = snapshot.screenshot_aspect_ratio;
                flipped = snapshot.screenshot_flipped;

                try {
                    current_frame = snapshot.get_screenshot ();
                    frame_changed = true;
                } catch (Error e) {
                    critical ("Failed to show snapshot: %s", e.message);
                }

                screen_set.filter_changed.connect (() => {
                    platform_filter_changed = true;
                    queue_render ();
                });

                platform_filter_changed = true;
                platform_filter_recreated = true;
            }

            queue_render ();
        }
    }

    private bool platform_filter_changed;
    private bool platform_filter_recreated;

    private Gdk.Texture? current_frame;
    private bool frame_changed;

    private ScreenSet screen_set;
    private Screen[]? screens;
    private ScreenLayout screen_layout;
    private bool screen_layout_changed;

    private double aspect_ratio;
    private bool flipped;

    private bool context_recreated;

    private GLVertexArray vertex_array;
    private GLVertexBuffer vertex_buffer;
    private GLElementBuffer element_buffer;
    private GLShader platform_shader;
    private GLShader shader;
    private GLTexture texture;
    private GLFramebuffer framebuffer;

    construct {
        overflow = HIDDEN;

        auto_render = false;
    }

    static construct {
        set_css_name ("snapshot-thumbnail");
    }

    private void set_screen_layout (ScreenLayout layout) {
        screen_layout = layout;
        screen_layout_changed = true;
        queue_render ();
    }

    public override void css_changed (Gtk.CssStyleChange change) {
        base.css_changed (change);

        queue_render ();
    }

    public override void realize () {
        base.realize ();

        make_current ();

        if (get_error () != null)
            return;

        vertex_array = new GLVertexArray ();
        vertex_array.bind ();

        vertex_buffer = new GLVertexBuffer ();
        vertex_buffer.bind ();
        vertex_buffer.set_data ({
             0,  1,
             1,  1,
             1,  0,
             0,  0,
        });

        element_buffer = new GLElementBuffer ();
        element_buffer.bind ();
        element_buffer.set_data ({
            0, 1, 2,
            2, 3, 0
        });

        texture = new GLTexture ();

        var shader_path = "/app/drey/Highscore/snapshot/thumbnail.glsl";
        var shader_src = ResourceUtils.load_to_string (shader_path);

        try {
            shader = new GLShader (get_context (), shader_src);
        } catch (GLShaderError e) {
            error ("Error in thumbnail shader: %s", e.message);
        }

        framebuffer = new GLFramebuffer ();

        context_recreated = true;

        get_native ().get_surface ().notify["scale"].connect (queue_render);
    }

    public override void unrealize () {
        get_native ().get_surface ().notify["scale"].disconnect (queue_render);

        make_current ();

        if (get_error () != null) {
            base.unrealize ();
            return;
        }

        vertex_array = null;
        vertex_buffer = null;
        element_buffer = null;
        shader = null;
        platform_shader = null;
        texture = null;
        framebuffer = null;

        base.unrealize ();
    }

    public override bool render (Gdk.GLContext context) {
        var error = get_error ();
        if (error != null)
            critical (error.message);

        glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
        glClear (GL_COLOR_BUFFER_BIT);

        draw_frame ();

        glFlush ();

        return Gdk.EVENT_STOP;
    }

    private void draw_frame () {
        if (current_frame == null)
            return;

        int scale = (int) Math.ceil (get_native ().get_surface ().scale);
        int widget_width = get_width () * scale;
        int widget_height = get_height () * scale;

        if (screen_layout_changed || context_recreated) {
            float w, h;
            screen_layout.measure (
                current_frame.width, current_frame.height, screens, out w, out h
            );

            int layout_width = (int) Math.ceil (w);
            int layout_height = (int) Math.ceil (h);

            if (layout_width != framebuffer.width ||
                layout_height != framebuffer.height ||
                context_recreated) {
                framebuffer.bind ();
                framebuffer.create_texture (
                    layout_width, layout_height, LINEAR, LINEAR, BORDER, RGBA8
                );
                framebuffer.unbind ();

                attach_buffers ();
            }
        }

        if (platform_filter_recreated || context_recreated)
            create_platform_filter ();

        var layout_result = screen_layout.layout (
            { framebuffer.width, framebuffer.height, 0, 0, 0, 0, },
            (float) current_frame.width / current_frame.height,
            screens
        );

        assert (screens.length == layout_result.length);

        if (frame_changed || context_recreated) {
            texture.bind ();
            texture.upload (current_frame);
            texture.unbind ();
        }

        if (frame_changed ||
            screen_layout_changed ||
            platform_filter_changed ||
            context_recreated) {
            framebuffer.bind (GL_DRAW_FRAMEBUFFER);

            glViewport (0, 0, framebuffer.width, framebuffer.height);

            glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
            glClear (GL_COLOR_BUFFER_BIT);

            platform_shader.bind ();
            platform_shader.set_attribute_pointer ("position", 2, GL_FLOAT);
            platform_shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

            texture.bind ();

            GLUtils.set_active_texture (0);
            texture.set_params (NEAREST, NEAREST, EDGE);
            platform_shader.set_uniform_int ("u_source", 0);

            for (int i = 0; i < screens.length; i++) {
                var screen = screens[i];
                var result = layout_result[i];

                if (!result.visible)
                    continue;

                var transform = new Gsk.Transform ();
                transform = transform.translate ({ -1, 1 });
                transform = transform.scale (2, -2);
                transform = transform.scale (1.0f / framebuffer.width, 1.0f / framebuffer.height);

                transform = transform.translate ({ framebuffer.width / 2.0f, framebuffer.height / 2.0f });
                transform = transform.rotate (result.angle);
                transform = transform.translate ({ -framebuffer.width / 2.0f, -framebuffer.height / 2.0f });

                transform = transform.transform (result.transform);
                transform = transform.scale (result.width, result.height);

                Graphene.Rect rect = {{ 0, 0 }, { 1, 1 }};
                var transformed_rect = transform.transform_bounds (rect);

                if (!transformed_rect.intersection ({{ -1, -1 }, { 2, 2 }}, null))
                    continue;

                var mvp = transform.to_matrix ();

                Graphene.Vec2 position = {}, size = {};
                position.init (screen.area.origin.x, screen.area.origin.y);
                size.init (screen.area.size.width, screen.area.size.height);

                platform_shader.set_uniform_vec2 ("u_screenPosition", position);
                platform_shader.set_uniform_vec2 ("u_screenSize", size);
                platform_shader.set_uniform_bool ("u_flipped", flipped);
                platform_shader.set_uniform_matrix ("u_mvp", mvp);

                screen_set.setup_filter (platform_shader, screen.id);

                element_buffer.draw ();
            }

            texture.unbind ();
            platform_shader.unbind ();
            framebuffer.unbind (GL_DRAW_FRAMEBUFFER);

            attach_buffers ();
        }

        glViewport (0, 0, widget_width, widget_height);

        shader.bind ();
        shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

        framebuffer.bind_texture ();
        shader.set_uniform_int ("u_source", 0);

        var transform = new Gsk.Transform ();
        transform = transform.translate ({ -1, -1 });
        transform = transform.scale (2, 2);

        float widget_aspect_ratio = (float) widget_width / widget_height;
        float layout_aspect_ratio = (float) framebuffer.width / framebuffer.height;

        layout_aspect_ratio *= (float) aspect_ratio;
        layout_aspect_ratio /= (float) texture.width / texture.height;

        float w = 1, h = 1;

        if (widget_aspect_ratio < layout_aspect_ratio)
            h = (float) widget_aspect_ratio / layout_aspect_ratio;
        else
            w = (float) layout_aspect_ratio / widget_aspect_ratio;

        transform = transform.translate ({ (1 - w) / 2f, (1 - h) / 2f });
        transform = transform.scale (w, h);

        var mvp = transform.to_matrix ();

        Graphene.Vec2 source_size = {};
        source_size.init (framebuffer.width, framebuffer.height);
        shader.set_uniform_vec2 ("u_sourceSize", source_size);
        shader.set_uniform_matrix ("u_mvp", mvp);

        element_buffer.draw ();

        framebuffer.unbind_texture ();
        shader.unbind ();

        frame_changed = false;
        screen_layout_changed = false;
        platform_filter_changed = false;
        platform_filter_recreated = false;
        context_recreated = false;
    }

    private void create_platform_filter () {
        var shared = ResourceUtils.load_to_string (
            "/app/drey/Highscore/runner/screen/platform-shared.glsl"
        );

        var main = ResourceUtils.load_to_string (
            screen_set.get_filter_path ()
        );

        try {
            platform_shader = new GLShader (context, @"$shared\n\n$main");
        } catch (GLShaderError e) {
            error ("Error in platform shader: %s", e.message);
        }
    }
}
