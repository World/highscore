// This file is part of Highscore. License: GPL-3.0-or-later

public errordomain Highscore.ParserError {
    INCOMPLETE_GAME,
}

public class Highscore.GameParser : Object {
    public Platform platform { get; construct; }
    public File file { get; construct; }

    public string? current_media { get; private set; }
    private string[]? media_set;

    public GameParser (Platform platform, File file) {
        Object (platform: platform, file: file);
    }

    public virtual bool parse () throws Error {
        return true;
    }

    protected void set_incomplete (string current_media, string[] media_set) {
        this.current_media = current_media;
        this.media_set = media_set;

        if (PARSERS in Debug.get_flags ()) {
            parser_debug (
                "Media %s is a part of a set: [%s]",
                current_media, string.joinv (", ", media_set)
            );
        }
    }

    protected void parser_debug (string format, ...) {
        if (!(PARSERS in Debug.get_flags ()))
            return;

        var l = va_list ();

        string? title = null;

        try {
            title = get_title ();
        } catch (Error e) {
            // Whatever, this is a debug print
            title = file.get_basename ();
        }

        message ("%s: %s: %s", platform.name, title, format.vprintf (l));
    }

    public bool get_incomplete () {
        return media_set != null;
    }

    public string[]? get_media_set () {
        return media_set;
    }

    public virtual string get_game_uid (Media[] media) throws Error {
        assert (media_set == null);
        assert (media.length == 1);

        return media[0].uid;
    }

    public virtual string get_media_uid () throws Error {
        return Fingerprint.get_uid (file, platform.id);
    }

    public string get_title () throws Error {
        return Filename.get_title (file);
    }

    public virtual GameMetadata? get_metadata () throws Error {
        return null;
    }

    public virtual string[]? get_extra_paths () throws Error {
        return null;
    }

    public Media create_media () throws Error {
        var uid = get_media_uid ();
        var extra_paths = get_extra_paths ();

        return new Media (uid, file, extra_paths);
    }

    public Game create_game (Media[] media) throws Error {
        var uid = get_game_uid (media);
        var title = get_title ();
        var metadata = get_metadata ();

        if (media.length > 1) {
            if (PARSERS in Debug.get_flags ()) {
                parser_debug (
                    "Created a game from media set [%s]",
                    string.joinv (", ", media_set)
                );
            }
        }

        return new Game (media, platform, uid, title, title, metadata, null);
    }

    public static GameParser create (Platform platform, File file) {
        var type = platform.parser_type;

        if (type == Type.NONE && platform.parent != null)
            type = platform.parent.parser_type;

        if (type == Type.NONE)
            type = typeof (GameParser);

        return Object.new (type, platform: platform, file: file) as GameParser;
    }
}
