// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Media : Object {
    public string uid { get; construct; }
    public File file { get; construct; }
    public string[] extra_paths { get; construct; }

    public Media (string uid, File file, string[]? extra_paths) {
        Object (uid: uid, file: file, extra_paths: extra_paths);
    }

    public static uint hash (Media key) {
        return str_hash (key.uid);
    }

    public static bool equal (Media a, Media b) {
        if (a == b)
            return true;

        return str_equal (a.uid, b.uid);
    }

    public static int compare_id (Media a, Media b) {
        if (a == b)
            return 0;

        return strcmp (a.uid, b.uid);
    }
}
