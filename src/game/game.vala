// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Game : Object {
    public string title { get; construct set; }
    public string default_title { get; construct; }
    public string uid { get; construct; }
    public Platform platform { get; construct; }
    public GameMetadata? metadata { get; construct; }
    public DateTime? last_played { get; construct set; }
    public File? cover { get; set; }

    private Media[] media;

    public Library.DeferredAction deferred_action { get; set; }

    public signal void cover_changed ();

    public Game (
        Media[] media,
        Platform platform,
        string uid,
        string title,
        string default_title,
        GameMetadata? metadata,
        DateTime? last_played
    ) {
        Object (
            platform: platform,
            uid: uid,
            title: title,
            default_title: default_title,
            metadata: metadata,
            last_played: last_played
        );

        this.media = media;
    }

    construct {
        notify["cover"].connect (() => cover_changed ());
    }

    public Media[] get_media () {
        return media;
    }

    public bool matches_search_terms (string[] search_terms) {
        if (search_terms.length != 0)
            foreach (var term in search_terms)
                if (!(term.casefold () in title.casefold ()))
                    return false;

        return true;
    }

    public File[] get_all_files () {
        File[] ret = {};

        foreach (var m in media) {
            if (m.file != null)
                ret += m.file;

            if (m.extra_paths != null) {
                foreach (var path in m.extra_paths)
                    ret += File.new_for_path (path);
            }
        }

        if (cover != null)
            ret += cover;

        return ret;
    }

    public static uint hash (Game key) {
        return str_hash (key.uid);
    }

    public static bool equal (Game a, Game b) {
        if (a == b)
            return true;

        return str_equal (a.uid, b.uid);
    }

    public static int compare_id (Game a, Game b) {
        if (a == b)
            return 0;

        return strcmp (a.uid, b.uid);
    }

    public static int compare (Game a, Game b) {
        var ret = a.title.collate (b.title);
        if (ret != 0)
            return ret;

        ret = Platform.compare (a.platform, b.platform);
        if (ret != 0)
            return ret;

        return compare_id (a, b);
    }

    public static int compare_recent (Game a, Game b) {
        if (a.last_played == null && b.last_played == null)
            return 0;

        if (a.last_played == null)
            return 1;

        if (b.last_played == null)
            return -1;

        var ret = a.last_played.compare (b.last_played);
        if (ret != 0)
            return -ret;

        return 0;
    }
}
