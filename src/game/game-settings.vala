// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameSettings : Object {
    public Game game { get; construct; }

    private SettingsBackend backend;

    private Settings settings;
    private Settings game_settings;

    public bool load_state_on_startup {
        get {
            if (load_state_on_startup_modified)
                return game_settings.get_boolean ("load-state-on-startup");

            return settings.get_boolean ("load-state-on-startup");
        }
        set {
            game_settings.set_boolean ("load-state-on-startup", value);
        }
    }

    public bool load_state_on_startup_modified { get; private set; }

    public string video_filter {
        owned get {
            var local = local_video_filter;
            if (local != "")
                return local;

            return settings.get_string ("video-filter");
        }
    }

    public string local_video_filter {
        owned get {
            return game_settings.get_string ("video-filter");
        }
        set {
            game_settings.set_string ("video-filter", value);
        }
    }

    public bool is_accurate_filter {
        get {
            return video_filter == "accurate";
        }
    }

    public bool interframe_blending {
        get {
            if (interframe_blending_modified)
                return game_settings.get_boolean ("interframe-blending");

            return settings.get_boolean ("interframe-blending");
        }
        set {
            game_settings.set_boolean ("interframe-blending", value);
        }
    }

    public bool interframe_blending_modified { get; private set; }

    public GameSettings (Game game) {
        Object (game: game);
    }

    construct {
        var settings_dir = get_settings_dir ();

        try {
            if (!settings_dir.query_exists ())
                settings_dir.make_directory_with_parents ();
        } catch (Error e) {
            error ("Failed to initialize settings dir: %s", e.message);
        }

        var settings_file = settings_dir.get_child (game.uid);

        backend = SettingsBackend.keyfile_settings_backend_new (
            settings_file.peek_path (),
            "/app/drey/Highscore/game/",
            "common"
        );

        settings = new Settings ("app.drey.Highscore");

        game_settings = new Settings.with_backend (
            "app.drey.Highscore.game", backend
        );

        settings.changed["load-state-on-startup"].connect (update_load_state_on_startup);
        settings.changed["video-filter"].connect (update_video_filter);
        settings.changed["interframe-blending"].connect (update_interframe_blending);

        game_settings.changed["load-state-on-startup"].connect (update_load_state_on_startup);
        game_settings.changed["video-filter"].connect (update_video_filter);
        game_settings.changed["interframe-blending"].connect (update_interframe_blending);

        update_load_state_on_startup ();
        update_video_filter ();
        update_interframe_blending ();
    }

    private File get_settings_dir () {
        var data_dir = Environment.get_user_data_dir ();
        var save_dir_path = Path.build_filename (data_dir, "highscore", "settings");

        return File.new_for_path (save_dir_path);
    }

    public Settings? get_platform () {
        var platform = game.platform;

        if (platform.parent != null)
            platform = platform.parent;

        var id = platform.id;
        var schema = @"app.drey.Highscore.game.platforms.$id";

        return new Settings.with_backend (schema, backend);
    }

    public Settings? get_addon_platform () {
        if (game.platform.parent == null)
            return null;

        var id = game.platform.id;
        var schema = @"app.drey.Highscore.game.platforms.$id";

        return new Settings.with_backend (schema, backend);
    }

    private void update_load_state_on_startup () {
        load_state_on_startup_modified = game_settings.get_user_value ("load-state-on-startup") != null;

        notify_property ("load-state-on-startup");
    }

    private void update_video_filter () {
        notify_property ("video-filter");
        notify_property ("is-accurate-filter");
    }

    private void update_interframe_blending () {
        interframe_blending_modified = game_settings.get_user_value ("interframe-blending") != null;

        notify_property ("interframe-blending");
    }

    public void reset_load_state_on_startup () {
        game_settings.reset ("load-state-on-startup");
    }

    public void reset_interframe_blending () {
        game_settings.reset ("interframe-blending");
    }
}
