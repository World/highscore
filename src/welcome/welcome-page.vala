// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.WelcomePage : Adw.Bin {
    public signal void next_page ();

    public bool block_prev { get; set; }

    static construct {
        install_action ("welcome.continue", null, widget => {
            var self = widget as WelcomePage;

            self.next_page ();
        });
    }
}
