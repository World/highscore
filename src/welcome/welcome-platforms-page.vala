// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/welcome/welcome-platforms-page.ui")]
public class Highscore.WelcomePlatformsPage : WelcomePage {
    [GtkChild]
    private unowned Adw.StatusPage status_page;
    [GtkChild]
    private unowned Adw.PreferencesGroup platforms_list;

    construct {
        // FIXME: Stop using status page here
        var swindow = status_page.get_first_child () as Gtk.ScrolledWindow;
        swindow.vadjustment.value_changed.connect (() => {
            var value = swindow.vadjustment.value;

            block_prev = (value > 0);
        });

        var platforms = PlatformRegister.get_register ().get_all_platforms ();

        foreach (var platform in platforms) {
            var row = new Adw.ActionRow () {
                title = platform.name,
            };

            var icon = new Gtk.Image.from_icon_name (platform.icon_name);
            icon.add_css_class ("welcome-platform-icon");

            var extensions = new Gtk.Label (
                string.joinv (", ", platform.get_extensions ())
            );

            row.add_prefix (icon);
            row.add_suffix (extensions);

            platforms_list.add (row);
        }
    }
}
