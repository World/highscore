// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/welcome/welcome-library-page.ui")]
public class Highscore.WelcomeLibraryPage : WelcomePage {
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned Gtk.ProgressBar progress;
    [GtkChild]
    private unowned Gtk.Label progress_label;
    [GtkChild]
    private unowned Adw.StatusPage done_page;

    private Cancellable? scan_cancellable;
    private uint n_games;

    private bool dir_selected;

    static construct {
        install_action ("welcome.select-library-dir", null, widget => {
            var page = widget as WelcomeLibraryPage;

            page.select_library_folder.begin ();
        });

        install_action ("welcome.cancel-scan", null, widget => {
            var page = widget as WelcomeLibraryPage;

            page.cancel_scan ();
        });
    }

    private async void select_library_folder () {
        var dialog = new Gtk.FileDialog () {
            title = _("Select Library Folder"),
            modal = true,
        };

        File folder = null;

        var window = get_root () as Gtk.Window;

        try {
            folder = yield dialog.select_folder (window, null);
        } catch (Gtk.DialogError e) {
            return;
        } catch (Error e) {
            critical ("Failed to select library folder: %s", e.message);
            return;
        }

        block_prev = true;

        var settings = new Settings ("app.drey.Highscore");
        settings.set_string ("library-dir", folder.get_path ());

        scan_cancellable = new Cancellable ();

        update_scan_progress (0, 0);
        stack.visible_child_name = "progress";

        var library = Library.get_instance ();
        yield library.rescan (update_scan_progress, scan_cancellable);

        if (scan_cancellable.is_cancelled ()) {
            stack.visible_child_name = dir_selected ? "done" : "select-dir";
        } else {
            done_page.description = _("Found %u games").printf (n_games);
            stack.visible_child_name = "done";

            dir_selected = true;
        }

        scan_cancellable = null;
    }

    private void cancel_scan () {
        if (scan_cancellable == null)
            return;

        scan_cancellable.cancel ();

        progress_label.label = _("Cancelling…");
    }

    private void update_scan_progress (uint n_games, double scan_progress) {
        if (scan_cancellable == null || scan_cancellable.is_cancelled ())
            return;

        progress_label.label = ngettext (
            "Found %u game",
            "Found %u games",
            n_games
        ).printf (n_games);

        progress.fraction = scan_progress;
        this.n_games = n_games;
    }
}
