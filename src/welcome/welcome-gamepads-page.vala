// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.WelcomeGamepadDialog : Adw.Dialog {
    public Gamepad gamepad { get; construct; }

    public WelcomeGamepadDialog (Gamepad gamepad) {
        Object (gamepad: gamepad);
    }

    construct {
        content_width = 640;

        var page = new PreferencesGamepadPage (gamepad);

        child = page;
    }
}

private class Highscore.WelcomeGamepadRow : Adw.ActionRow {
    public Gamepad gamepad { get; construct; }

    public WelcomeGamepadRow (Gamepad gamepad) {
        Object (gamepad: gamepad);
    }

    construct {
        activatable = true;

        gamepad.bind_property ("name", this, "title", SYNC_CREATE);

        var image = new Gtk.Image () {
            icon_name = "go-next-symbolic",
        };

        add_suffix (image);
    }
}

[GtkTemplate (ui = "/app/drey/Highscore/welcome/welcome-gamepads-page.ui")]
public class Highscore.WelcomeGamepadsPage : WelcomePage {
    public unowned bool has_gamepads { get; set; }

    [GtkChild]
    private unowned Adw.StatusPage status_page;
    [GtkChild]
    private unowned Gtk.ListBox gamepads_list;

    construct {
        // FIXME: Stop using status page here
        var swindow = status_page.get_first_child () as Gtk.ScrolledWindow;
        swindow.vadjustment.value_changed.connect (() => {
            var value = swindow.vadjustment.value;

            block_prev = (value > 0);
        });

        var gamepads = GamepadManager.get_instance ().gamepads;

        gamepads_list.bind_model (gamepads, item => {
            var gamepad = item as Gamepad;

            var row = new WelcomeGamepadRow (gamepad);

            row.activated.connect (() => {
                var dialog = new WelcomeGamepadDialog (gamepad);

                dialog.present (this);
            });

            return row;
        });

        gamepads.items_changed.connect (() => {
            if (gamepads.n_items > 0)
                has_gamepads = true;
        });

        if (gamepads.n_items > 0)
            has_gamepads = true;
    }
}
