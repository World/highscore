// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/welcome/welcome-dialog.ui")]
public class Highscore.WelcomeDialog : Adw.Dialog {
    public bool block_prev { get; set; }

    [GtkChild]
    private unowned Gtk.Stack stack;

    private WelcomePage? hiding_page;
    private WelcomePage? current_page;
    private BindingGroup bindings;

    construct {
        bindings = new BindingGroup ();

        bindings.bind (
            "block-prev", this,
            "block-prev", SYNC_CREATE
        );

        notify["block-prev"].connect (update_navigation);

        notify_visible_child_cb ();
    }

    static construct {
        install_action ("welcome.prev-page", null, widget => {
            var self = widget as WelcomeDialog;

            self.previous_page ();
        });

        install_action ("welcome.close", null, widget => {
            var self = widget as WelcomeDialog;

            self.close ();
        });
    }

    private void update_navigation () {
        var pages = stack.pages;

        uint index = find_selected_index ();
        uint n = pages.get_n_items ();

        bool has_prev = index != Gtk.INVALID_LIST_POSITION && index > 0;
        bool has_next = index != Gtk.INVALID_LIST_POSITION && index < n - 1;

        action_set_enabled ("welcome.prev-page", has_prev && !block_prev);

        can_close = !has_next && !block_prev;
    }

    [GtkCallback]
    private void notify_visible_child_cb () {
        if (current_page != null)
            current_page.next_page.disconnect (next_page);

        hiding_page = current_page;
        current_page = stack.visible_child as WelcomePage;

        bindings.source = current_page;

        if (current_page != null)
            current_page.next_page.connect (next_page);

        update_navigation ();
    }

    private uint find_selected_index () {
        var pages = stack.pages;

        uint n = pages.get_n_items ();

        if (n == 0)
            return Gtk.INVALID_LIST_POSITION;

        for (uint i = 0; i < n; i++) {
            if (pages.is_selected (i))
                return i;
        }

        assert_not_reached ();
    }

    private void previous_page () {
        var pages = stack.pages;

        uint index = find_selected_index ();
        assert (index != Gtk.INVALID_LIST_POSITION);

        if (index == 0)
            return;

        pages.select_item (index - 1, true);
    }

    private void next_page () {
        var pages = stack.pages;

        var index = find_selected_index ();
        assert (index != Gtk.INVALID_LIST_POSITION);

        if (index == pages.get_n_items () - 1)
            return;

        pages.select_item (index + 1, true);
    }
}
