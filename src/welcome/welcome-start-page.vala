// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/welcome/welcome-start-page.ui")]
public class Highscore.WelcomeStartPage : WelcomePage {
    [GtkChild]
    private unowned Adw.StatusPage status_page;

    construct {
        status_page.icon_name = Config.APPLICATION_ID;
    }
}
