// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/display/secondary-window.ui")]
public class Highscore.SecondaryWindow : Adw.ApplicationWindow {
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned FullscreenView fullscreen_view;
    [GtkChild]
    private unowned Gtk.Overlay main_overlay;
    [GtkChild]
    private unowned ErrorPage error_page;
    [GtkChild]
    private unowned Gtk.MenuButton menu_button;
    [GtkChild]
    private unowned Gtk.Revealer loading_revealer;
    [GtkChild]
    private unowned Gtk.Revealer menu_revealer;
    [GtkChild]
    private unowned OverlayMenuProxy menu_proxy;
    [GtkChild]
    private unowned Gtk.Stack header_stack;

    private GameView view;

    public unowned DisplayPage page { get; construct; }
    public string view_title { get; set; }
    public string subtitle { get; set; }
    public bool loading { get; private set; }
    public bool snapshots_open { get; set; }

    private unowned Runner runner;

    private Binding top_inset_binding;

    private Settings settings;

    public SecondaryWindow (DisplayPage page) {
        Object (page: page);
    }

    construct {
        var detector = TouchDetector.get_instance ();
        if (detector != null)
            detector.register (this);

        settings = new Settings ("app.drey.Highscore.state");

        settings.bind ("secondary-window-maximized",  this, "maximized",      GET | SET | GET_NO_CHANGES);
        settings.bind ("secondary-window-fullscreen", this, "fullscreened",   GET | SET | GET_NO_CHANGES);
        settings.bind ("secondary-window-width",      this, "default-width",  GET | SET | GET_NO_CHANGES);
        settings.bind ("secondary-window-height",     this, "default-height", GET | SET | GET_NO_CHANGES);

        if (fullscreened) {
            string monitor_connector = settings.get_string ("secondary-window-monitor");

            if (monitor_connector != "") {
                var monitors = get_display ().get_monitors ();

                for (uint i = 0; i < monitors.get_n_items (); i++) {
                    var monitor = monitors.get_item (i) as Gdk.Monitor;

                    if (monitor.valid && monitor.connector == monitor_connector) {
                        fullscreen_on_monitor (monitor);
                        break;
                    }
                }
            }
        }

        view = new GameView (page.game, true);
        main_overlay.child = view;

        view.bind_property ("top-inset",    loading_revealer, "margin-top",    SYNC_CREATE);
        view.bind_property ("bottom-inset", loading_revealer, "margin-bottom", SYNC_CREATE);

        view.bind_property ("top-inset",    menu_revealer, "margin-top",    SYNC_CREATE);
        view.bind_property ("bottom-inset", menu_revealer, "margin-bottom", SYNC_CREATE);

        runner = page.runner;
        view.runner = runner;
        view.clear_snapshot (false);

        if (page.loading) {
            var game_settings = new GameSettings (page.game);
            bool load_state = game_settings.load_state_on_startup;
            var snapshot = runner.initial_snapshot;

            loading = true;
            header_stack.visible_child_name = "loading";

            loading_revealer.visible = true;
            loading_revealer.reveal_child = true;

            if (snapshot.is_valid () && snapshot.is_compatible () && load_state)
                view.show_snapshot (snapshot);

            page.notify["loading"].connect (() => {
                if (page.loading)
                    return;

                header_stack.visible_child_name = "game";
                loading_revealer.reveal_child = false;
                loading = false;
            });
        } else {
            header_stack.visible_child_name = "game";
            loading_revealer.reveal_child = false;
        }

        if (runner != null)
            runner.stopped.connect (game_stopped);

        runner.game.bind_property ("title", this, "view-title", SYNC_CREATE);

        runner.notify["secondary-view-name"].connect (update_title);
        runner.notify["paused"].connect (update_title);
        runner.notify["snapshots-open"].connect (update_title);

        update_title ();

        notify["fullscreened"].connect (update_fullscreen);

        update_fullscreen ();

        fullscreen_view.track_menu_button (menu_button);

        menu_proxy.menu = page.get_overlay_menu ();
    }

    static construct {
        install_action ("game.screenshot", null, widget => {
            var self = widget as SecondaryWindow;

            Screenshot.take_screenshot.begin (self.view);
        });

        install_action ("game.toggle-fullscreen", null, widget => {
            var self = widget as SecondaryWindow;

            self.fullscreened = !self.fullscreened;
        });

        add_binding_action (Gdk.Key.W, CONTROL_MASK, "window.close", null);
    }

    private string make_subtitle () {
        string view_name = runner.secondary_view_name;
        bool paused = runner.paused;

        if (paused && !snapshots_open && !menu_revealer.reveal_child) {
            if (view_name != null && view_name != "")
                return _("%s – Paused").printf (view_name);

            return _("Paused");
        }

        return view_name;
    }

    private void update_title () {
        subtitle = make_subtitle ();

        if (subtitle == null || subtitle == "")
            title = view_title;
        else
            title = _("%s (%s)").printf (view_title, subtitle);
    }

    protected override bool close_request () {
        if (fullscreened)
            remember_monitor ();

        view.runner = null;

        return base.close_request ();
    }

    protected override void map () {
        base.map ();

        view.grab_focus ();
    }

    private void game_stopped (string? error) {
        if (error != null) {
            error_page.show_error (_("Game Crashed"), error);
            stack.visible_child_name = "error";

            fullscreened = false;

            action_set_enabled ("game.screenshot", false);
            action_set_enabled ("game.toggle-fullscreen", false);
        }

        runner.stopped.disconnect (game_stopped);

        view.runner = null;
    }

    private void remember_monitor () {
        var monitor = get_display ().get_monitor_at_surface (get_surface ());

        if (monitor != null)
            settings.set_string ("secondary-window-monitor", monitor.connector);
        else
            settings.reset ("secondary-window-monitor");
    }

    private void update_fullscreen () {
        if (fullscreened) {
            if (get_mapped ())
                remember_monitor ();

            if (top_inset_binding != null) {
                top_inset_binding.unbind ();
                top_inset_binding = null;
            }

            view.top_inset = 0;
            return;
        }

        if (top_inset_binding == null) {
            top_inset_binding = fullscreen_view.bind_property (
                "top-bar-height", view, "top-inset", SYNC_CREATE
            );
        }
    }

    [GtkCallback]
    private void spinner_revealed_cb () {
        if (!loading_revealer.child_revealed)
            loading_revealer.visible = false;
    }

    public void show_snapshot (Snapshot snapshot) {
        view.show_snapshot (snapshot);
    }

    public void clear_snapshot (bool now) {
        view.clear_snapshot (now);
    }

    public void open_menu () {
        menu_revealer.reveal_child = true;
        menu_proxy.grab_focus ();
    }

    public void close_menu () {
        menu_revealer.reveal_child = false;
        view.grab_focus ();
    }
}
