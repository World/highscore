// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.RumbleIndicator : Adw.Bin {
    private Runner _runner;
    public Runner runner {
        get { return _runner; }
        set {
            if (runner != null) {
                runner.weak_unref (runner_weak_notify_cb);

                runner.notify["n-rumble-indicators"].disconnect (update_n_indicators);
                runner.rumble_indicators_changed.disconnect (rumble_indicators_changed);
            }

            _runner = value;

            if (runner != null) {
                runner.weak_ref (runner_weak_notify_cb);

                runner.notify["n-rumble-indicators"].connect (update_n_indicators);
                runner.rumble_indicators_changed.connect (rumble_indicators_changed);
            }

            update_n_indicators ();
        }
    }

    private Gtk.Box box;
    private Gtk.Image[] indicators;

    construct {
        can_focus = false;
        can_target = false;
        visible = false;

        box = new Gtk.Box (VERTICAL, 32);
        child = box;

        indicators = {};
    }

    static construct {
        set_css_name ("rumble-indicator");
    }

    private void runner_weak_notify_cb () {
        _runner = null;
        notify_property ("runner");

        update_n_indicators ();
    }

    private void update_n_indicators () {
        foreach (var indicator in indicators)
            box.remove (indicator);

        indicators = {};

        if (runner == null) {
            visible = false;
            return;
        }

        for (int i = 0; i < runner.n_rumble_indicators; i++) {
            var indicator = new Gtk.Image.from_icon_name (
                "preferences-input-symbolic"
            );

            indicator.opacity = 0;

            box.append (indicator);
            indicators[i] = indicator;
        }

        visible = runner.n_rumble_indicators > 0;

        rumble_indicators_changed ();
    }

    private void rumble_indicators_changed () {
        if (runner == null)
            return;

        for (int i = 0; i < runner.n_rumble_indicators; i++) {
            double strong_mag, weak_mag;
            runner.get_rumble_indicator (i, out strong_mag, out weak_mag);

            indicators[i].opacity = (strong_mag + weak_mag) / 2;
        }
    }
}
