// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/display/overlay-menu/overlay-menu.ui")]
public class Highscore.OverlayMenu : Adw.Bin {
    public signal void resume ();

    [GtkChild]
    private unowned GamepadHintBar gamepad_hint_bar;
    [GtkChild]
    private unowned KeyboardHintBar keyboard_hint_bar;
    [GtkChild]
    private unowned Adw.ViewStack page_stack;

    public unowned Runner runner { get; set; }
    public MenuModel menu_model { get; set; }

    private OverlayMenuPage current_page;
    private OverlayMenuPageMenu root_page;

    private OverlayMenuAddin platform_addin;

    private string? platform_prefix;
    private ActionGroup? platform_group;
    private Settings settings;

    private GamepadController controller;

    construct {
        settings = new Settings ("app.drey.Highscore");

        controller = new GamepadController (this);
        gamepad_hint_bar.controller = controller;

        controller.bind_property (
            "select-label", keyboard_hint_bar, "select-label", DEFAULT
        );
        controller.bind_property (
            "cancel-label", keyboard_hint_bar, "cancel-label", DEFAULT
        );

        settings.bind (
            "pause-when-inactive", controller, "disable-when-unfocused", GET
        );

        controller.home.connect (() => resume ());
        controller.navigate.connect (navigate);
        controller.select.connect (select);
        controller.cancel.connect (cancel);
        controller.left_page.connect (left_page);
        controller.right_page.connect (right_page);

        controller.cancel_label = _("Back");

        root_page = new OverlayMenuPageMenu (controller, null, menu_model);

        root_page.open_submenu.connect (open_submenu);
        root_page.close.connect (() => resume ());

        page_stack.add (root_page);

        current_page = root_page;

        connect_page (root_page);
    }

    static construct {
        install_action ("menu.up", null, self => {
            var menu = self as OverlayMenu;

            menu.navigate (UP);
        });
        install_action ("menu.down", null, self => {
            var menu = self as OverlayMenu;

            menu.navigate (DOWN);
        });
        install_action ("menu.left", null, self => {
            var menu = self as OverlayMenu;

            menu.navigate (LEFT);
        });
        install_action ("menu.right", null, self => {
            var menu = self as OverlayMenu;

            menu.navigate (RIGHT);
        });
        install_action ("menu.back", null, self => {
            var menu = self as OverlayMenu;

            menu.cancel ();
        });
        install_action ("menu.resume", null, self => {
            var menu = self as OverlayMenu;

            menu.resume ();
        });
    }

    public void navigate (InputDirection direction) {
        current_page.navigate (direction);
    }

    [GtkCallback]
    public void select () {
        current_page.select ();
    }

    [GtkCallback]
    public void cancel () {
        current_page.cancel ();
    }

    public void left_page () {
        current_page.left_page ();
    }

    public void right_page () {
        current_page.right_page ();
    }

    protected override void map () {
        base.map ();

        platform_addin = runner.delegate.create_overlay_menu ();

        if (platform_addin != null) {
            platform_prefix = platform_addin.get_action_prefix ();
            platform_group = platform_addin.action_group;

            insert_action_group (platform_prefix, platform_group);

            platform_group.action_state_changed.connect (action_state_changed_cb);

            root_page.set_platform_addin (platform_addin);
        }
    }

    protected override void unmap () {
        if (platform_addin != null) {
            platform_group.action_state_changed.disconnect (action_state_changed_cb);

            insert_action_group (platform_prefix, null);
            platform_prefix = null;
            platform_group = null;

            root_page.set_platform_addin (null);
        }

        base.unmap ();
    }

    protected override bool grab_focus () {
        return current_page.grab_focus ();
    }

    private void open_submenu (string title, MenuModel model, string? special) {
        OverlayMenuPage page;

        if (special == "controls")
            page = new OverlayMenuPageControls (controller, runner);
        else
            page = new OverlayMenuPageMenu (controller, title, model);

        var prev_page = current_page;

        page.open_submenu.connect (open_submenu);
        page.close.connect (() => {
            disconnect_page (current_page);

            prev_page.add_css_class ("slide-left");

            current_page = prev_page;
            page_stack.visible_child = current_page;

            page.add_css_class ("slide-right");
            prev_page.remove_css_class ("slide-left");

            connect_page (prev_page);
        });

        disconnect_page (current_page);

        page.add_css_class ("slide-right");
        current_page.add_css_class ("slide-left");

        page_stack.add (page).title = title;
        current_page = page;
        page_stack.visible_child = current_page;

        page.remove_css_class ("slide-right");

        connect_page (page);
    }

    public void reset () {
        disconnect_page (current_page);

        root_page.remove_css_class ("slide-left");
        root_page.remove_css_class ("slide-right");

        current_page = root_page;
        page_stack.visible_child = current_page;

        root_page.reset ();

        connect_page (root_page);
    }

    public unowned VariantType? get_action_state_type (string action_name) {
        var parts = action_name.split (".");

        assert (parts.length == 2);

        var prefix = parts[0];
        var name = parts[1];

        if (prefix != platform_prefix)
            return null;

        return platform_group.get_action_state_type (name);
    }

    public Variant? get_action_state (string action_name) {
        var parts = action_name.split (".");

        assert (parts.length == 2);

        var prefix = parts[0];
        var name = parts[1];

        if (prefix != platform_prefix)
            return null;

        return platform_group.get_action_state (name);
    }

    private void action_state_changed_cb (string action_name, Variant state) {
        var full_name = @"$platform_prefix.$action_name";

        var pages = page_stack.pages;
        uint n = pages.get_n_items ();

        for (uint i = 0; i < n; i++) {
            var page = pages.get_item (i) as Adw.ViewStackPage;
            var menu_page = page.get_child () as OverlayMenuPage;

            menu_page.action_state_changed (full_name, state);
        }
    }

    private void trim_pages () {
        var pages = page_stack.pages;
        uint n = pages.get_n_items ();

        uint page_index = n;

        for (uint i = 0; i < n; i++) {
            var page = pages.get_item (i) as Adw.ViewStackPage;
            var menu_page = page.get_child () as OverlayMenuPage;

            if (menu_page == current_page) {
                page_index = i;
                break;
            }
        }

        for (uint i = page_index + 1; i < n; i++) {
            var page = pages.get_item (page_index + 1) as Adw.ViewStackPage;

            page_stack.remove (page.get_child ());
        }
    }

    private void disconnect_page (OverlayMenuPage page) {
        page.notify["enable-select"].disconnect (update_select_label);
    }

    private void connect_page (OverlayMenuPage page) {
        update_select_label (page, null);

        page.notify["enable-select"].connect (update_select_label);
    }

    private void update_select_label (Object object, ParamSpec? pspec) {
        var page = object as OverlayMenuPage;

        controller.select_label = (page.enable_select) ? _("Select") : "";
    }

    [GtkCallback]
    private void notify_transition_running_cb () {
        if (!Adw.get_enable_animations (page_stack) || page_stack.transition_running)
            return;

        trim_pages ();
    }

    [GtkCallback]
    private void notify_visible_child_cb () {
        if (Adw.get_enable_animations (page_stack) || page_stack.transition_running)
            return;

        Idle.add_once (trim_pages);
    }
}
