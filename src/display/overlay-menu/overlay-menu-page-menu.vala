// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/display/overlay-menu/overlay-menu-page-menu.ui")]
private class Highscore.OverlayMenuPageMenu : OverlayMenuPage {
    public string? title { get; construct; }
    public MenuModel model { get; construct; }

    [GtkChild]
    private unowned Gtk.ListBox listbox;

    private Gtk.ListBoxRow[] section_starts;
    private Gtk.ListBoxRow[] real_section_starts;
    private int n_rows;

    private OverlayMenuAddin? platform_addin;

    private GenericSet<string> state_actions;

    public OverlayMenuPageMenu (GamepadController controller, string? title, MenuModel model) {
        Object (controller: controller, title: title, model: model);
    }

    construct {
        listbox.set_header_func ((row, before) => {
            if (row in real_section_starts && before != null)
                row.set_header (new Gtk.Separator (HORIZONTAL));
            else
                row.set_header (null);
        });
    }

    protected override bool grab_focus () {
        var row = listbox.get_selected_row ();

        if (row != null)
            return row.grab_focus ();

        return listbox.grab_focus ();
    }

    public override void map () {
        base.map ();

        if (n_rows == 0)
            rebuild_menu ();
    }

    private void clear_menu () {
        while (true) {
            var row = listbox.get_row_at_index (0);
            if (row == null)
                return;

            listbox.remove (row);
        }
    }

    private void rebuild_menu () {
        section_starts = {};
        state_actions = new GenericSet<string> (str_hash, str_equal);

        clear_menu ();

        n_rows = 0;

        if (title != null) {
            listbox.append (new OverlayMenuRow (HEADER, title) {
                action_name = "menu.back",
            });
            n_rows++;

            add_css_class ("has-header");
        }

        int n = model.get_n_items ();
        for (int i = 0; i < n; i++) {
            var section = model.get_item_link (i, "section");
            if (section == null)
                error ("Items on top level are not allowed");

            string? special;
            model.get_item_attribute (i, "special", "s", out special);

            if (special == "platform") {
                n_rows += append_addin_sections ();
                continue;
            }

            n_rows += append_section (section);
        }

        invalidate_sections ();

        reset ();
    }

    private void invalidate_sections () {
        real_section_starts = {};

        for (int i = 0; i < section_starts.length; i++) {
            int start = section_starts[i].get_index ();
            int end;

            if (i < section_starts.length - 1)
                end = section_starts[i + 1].get_index ();
            else
                end = n_rows;

            for (int j = start; j < end; j++) {
                var row = listbox.get_row_at_index (j);

                if (row.visible && row.get_child_visible ()) {
                    real_section_starts += row;
                    break;
                }
            }
        }

        listbox.invalidate_headers ();
    }

    private int append_addin_sections () {
        if (platform_addin == null)
            return 0;

        var addin_model = platform_addin.menu_model;
        if (addin_model == null)
            return 0;

        int n_rows_added = 0;

        int n = addin_model.get_n_items ();
        for (int i = 0; i < n; i++) {
            var section = addin_model.get_item_link (i, "section");
            if (section == null)
                error ("Items on top level are not allowed");

            n_rows_added += append_section (section);
        }

        return n_rows_added;
    }

    private int append_section (MenuModel section_model) {
        int n_rows_added = 0;
        int n = section_model.get_n_items ();
        for (int i = 0; i < n; i++) {
            string label;
            section_model.get_item_attribute (i, "label", "s", out label);

            OverlayMenuRow row;

            var submenu = section_model.get_item_link (i, "submenu");
            if (submenu != null) {
                row = new OverlayMenuRow (SUBMENU, label);

                string? special;
                section_model.get_item_attribute (i, "special", "s", out special);

                row.activated.connect (() => open_submenu (label, submenu, special));
                listbox.append (row);
            } else {
                string? action;
                section_model.get_item_attribute (i, "action", "s", out action);

                Variant? target = null;
                target = section_model.get_item_attribute_value (i, "target", null);

                string? hidden_when;
                section_model.get_item_attribute (i, "hidden-when", "s", out hidden_when);

                OverlayMenuRow.Role role = GENERIC;

                if (action != null) {
                    var state_type = get_action_state_type (action);

                    if (state_type != null) {
                        if (state_type.equal (VariantType.BOOLEAN))
                            role = SWITCH;
                        else if (target != null)
                            role = RADIO;
                    }
                }

                row = new OverlayMenuRow (role, label);

                row.notify["visible"].connect (invalidate_sections);

                if (action != null)
                    row.set_action (action, target);

                if (role != GENERIC) {
                    var state = get_action_state (action);

                    if (role == RADIO)
                        row.radio_target = target;

                    row.set_state (state);

                    state_actions.add (action);
                }

                listbox.append (row);

                if (hidden_when == "action-disabled" || hidden_when == "action-missing")
                    row.bind_property ("sensitive", row, "visible", SYNC_CREATE);
            }

            if (i == 0)
                section_starts += row;

            n_rows_added++;
        }

        return n_rows_added;
    }

    [GtkCallback]
    private void row_activated_cb (Gtk.ListBoxRow row) {
        var menu_row = row as OverlayMenuRow;

        menu_row.activated ();
    }

    public override void select () {
        listbox.activate_cursor_row ();
    }

    public override void cancel () {
        close ();
    }

    private InputDirection get_start_direction () {
        if (get_direction () == RTL)
            return RIGHT;
        else
            return LEFT;
    }

    private InputDirection get_end_direction () {
        if (get_direction () == RTL)
            return LEFT;
        else
            return RIGHT;
    }

    public override void navigate (InputDirection direction) {
        if (title != null && direction == get_start_direction ()) {
            close ();
            return;
        }

        var row = listbox.get_selected_row ();

        if (row != null && direction == get_end_direction ()) {
            var menu_row = row as OverlayMenuRow;

            if (menu_row.role == SUBMENU)
                select ();
            else
                get_display ().beep ();

            return;
        }

        if (direction != UP && direction != DOWN) {
            get_display ().beep ();
            return;
        }

        int current_index;

        if (row != null)
            current_index = row.get_index ();
        else
            current_index = -1;

        while (true) {
            current_index += (direction == UP ? -1 : 1);

            if (current_index < 0)
                current_index = n_rows - 1;
            if (current_index >= n_rows)
                current_index = 0;

            var new_row = listbox.get_row_at_index (current_index);
            if (!new_row.sensitive || !new_row.visible || !new_row.get_child_visible ())
                continue;

            listbox.select_row (new_row);
            new_row.grab_focus ();
            break;
        }
    }

    public void reset () {
        var first_row = listbox.get_row_at_index (0);
        if (first_row != null) {
            listbox.select_row (first_row);
            first_row.grab_focus ();
        }
    }

    public void set_platform_addin (OverlayMenuAddin? addin) {
        platform_addin = addin;

        rebuild_menu ();
    }

    public unowned VariantType? get_action_state_type (string action_name) {
        var menu = get_ancestor (typeof (OverlayMenu)) as OverlayMenu;

        assert (menu != null);

        return menu.get_action_state_type (action_name);
    }

    public Variant? get_action_state (string action_name) {
        var menu = get_ancestor (typeof (OverlayMenu)) as OverlayMenu;

        assert (menu != null);

        return menu.get_action_state (action_name);
    }

    public override void action_state_changed (string name, Variant state) {
        if (!state_actions.contains (name))
            return;

        int i = 0;
        while (true) {
            var row = listbox.get_row_at_index (i++);
            if (row == null)
                return;

            if (row.action_name == name) {
                var menu_row = row as OverlayMenuRow;

                menu_row.set_state (state);
            }
        }
    }
}
