// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/display/overlay-menu/overlay-menu-page.ui")]
private class Highscore.OverlayMenuPage : Adw.Bin {
    public signal void open_submenu (string title, MenuModel submenu, string? special);
    public signal void close ();

    public bool enable_select { get; set; default = true; }

    public unowned GamepadController? controller { get; construct; }

    static construct {
        set_css_name ("page");
    }

    public virtual void select () {}

    public virtual void cancel () {
        close ();
    }

    public virtual void left_page () {}
    public virtual void right_page () {}

    public virtual void navigate (InputDirection direction) {}

    public virtual void action_state_changed (string name, Variant state) {}
}
