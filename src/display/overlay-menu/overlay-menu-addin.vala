// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.OverlayMenuAddin : Object, ActionMap {
    public MenuModel menu_model { get; protected set; }
    public SimpleActionGroup action_group { get; private set; }

    construct {
        action_group = new SimpleActionGroup ();
    }

    public void add_action (Action action) {
        action_group.add_action (action);
    }

    public void remove_action (string name) {
        action_group.remove_action (name);
    }

    public unowned Action? lookup_action (string name) {
        return action_group.lookup_action (name);
    }

    public virtual string get_action_prefix () {
        error ("Not implemented");
    }
}
