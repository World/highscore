// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayMenuMediaSwitcher : OverlayMenuAddin {
    public string prefix { get; construct; }
    public string label { get; construct; }
    public string[] media { get; construct; }
    public uint selected_media { get; set; }

    public OverlayMenuMediaSwitcher (string prefix, string label, string[] media) {
        Object (prefix: prefix, label: label, media: media);
    }

    construct {
        var section = new Menu ();

        for (int i = 0; i < media.length; i++)
            section.append (media[i], @"$prefix.switch-media(uint32 $i)");

        var submenu = new Menu ();
        submenu.append_section (null, section);

        section = new Menu ();
        section.append_submenu (label, submenu);

        var menu = new Menu ();
        menu.append_section (null, section);

        menu_model = menu;

        add_action (new PropertyAction ("switch-media", this, "selected-media"));
    }

    public override string get_action_prefix () {
        return prefix;
    }
}
