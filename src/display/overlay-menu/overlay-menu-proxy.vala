// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayMenuProxy : Adw.Bin {
    public unowned OverlayMenu menu { get; set; }

    private GamepadController controller;
    private Settings settings;

    construct {
        focusable = true;

        add_css_class ("overlay-menu");

        settings = new Settings ("app.drey.Highscore");

        controller = new GamepadController (this);

        settings.bind (
            "pause-when-inactive", controller, "enabled", GET
        );

        controller.home.connect (() => menu.resume ());
        controller.navigate.connect (menu.navigate);
        controller.select.connect (menu.select);
        controller.cancel.connect (menu.cancel);
        controller.left_page.connect (menu.left_page);
        controller.right_page.connect (menu.right_page);

        var shortcut_controller = new Gtk.ShortcutController ();

        add_arrow_shortcut (shortcut_controller, Gdk.Key.Up,    Gdk.Key.KP_Up,    UP);
        add_arrow_shortcut (shortcut_controller, Gdk.Key.Down,  Gdk.Key.KP_Down,  DOWN);
        add_arrow_shortcut (shortcut_controller, Gdk.Key.Left,  Gdk.Key.KP_Left,  LEFT);
        add_arrow_shortcut (shortcut_controller, Gdk.Key.Right, Gdk.Key.KP_Right, RIGHT);

        add_activate_shortcut (shortcut_controller, Gdk.Key.space);
        add_activate_shortcut (shortcut_controller, Gdk.Key.KP_Space);
        add_activate_shortcut (shortcut_controller, Gdk.Key.Return);
        add_activate_shortcut (shortcut_controller, Gdk.Key.ISO_Enter);
        add_activate_shortcut (shortcut_controller, Gdk.Key.KP_Enter);

        shortcut_controller.add_shortcut (new Gtk.Shortcut (
            new Gtk.KeyvalTrigger (Gdk.Key.Escape, 0),
            new Gtk.CallbackAction (() => {
                menu.cancel ();
                return Gdk.EVENT_STOP;
            })
        ));

        add_controller (shortcut_controller);
    }

    private inline void add_arrow_shortcut (
        Gtk.ShortcutController controller,
        uint keyval1,
        uint keyval2,
        InputDirection dir
    ) {
        controller.add_shortcut (new Gtk.Shortcut (
            new Gtk.AlternativeTrigger (
                new Gtk.KeyvalTrigger (keyval1, 0),
                new Gtk.KeyvalTrigger (keyval2, 0)
            ),
            new Gtk.CallbackAction (() => {
                menu.navigate (dir);
                return Gdk.EVENT_STOP;
            })
        ));
    }

    private inline void add_activate_shortcut (Gtk.ShortcutController controller, uint keyval) {
        controller.add_shortcut (new Gtk.Shortcut (
            new Gtk.KeyvalTrigger (keyval, 0),
            new Gtk.CallbackAction (() => {
                menu.select ();
                return Gdk.EVENT_STOP;
            })
        ));
    }
}
