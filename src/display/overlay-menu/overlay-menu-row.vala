// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayMenuRow : Gtk.ListBoxRow {
    public signal void activated ();

    public enum Role {
        GENERIC,
        HEADER,
        SUBMENU,
        SWITCH,
        RADIO,
    }

    public Role role { get; construct; }
    public string title { get; construct; }

    public Variant radio_target { get; set; }

    public Gtk.Image item_check;
    public Gtk.Switch item_switch;

    public OverlayMenuRow (Role role, string title) {
        Object (role: role, title: title);
    }

    construct {
        overflow = HIDDEN;

        add_css_class ("overlay-menu-row");

        var box = new Gtk.Box (HORIZONTAL, 0);

        var label = new Gtk.Label (null) {
            use_underline = true,
            xalign = 0,
            ellipsize = END,
            hexpand = true,
        };
        box.append (label);

        child = box;

        bind_property ("title", label, "label", SYNC_CREATE);

        state_flags_changed.connect (previous_flags => {
            var mask = get_state_flags () ^ previous_flags;
            if ((mask & Gtk.StateFlags.SELECTED) == 0)
                return;

            queue_draw ();
        });

        switch (role) {
            case GENERIC:
                break;
            case HEADER:
                box.prepend (new Gtk.Image.from_icon_name ("go-previous-symbolic"));
                add_css_class ("header");
                break;
            case SUBMENU:
                box.append (new Gtk.Image.from_icon_name ("go-next-symbolic"));
                add_css_class ("submenu");
                break;
            case SWITCH:
                item_switch = new Gtk.Switch () {
                    valign = CENTER,
                };
                item_switch.bind_property ("sensitive", this, "sensitive", SYNC_CREATE);
                box.append (item_switch);
                add_css_class ("switch");
                activated.connect (() => item_switch.activate ());
                break;
            case RADIO:
                item_check = new Gtk.Image.from_icon_name ("object-select-symbolic") {
                    opacity = 0,
                };
                box.append (item_check);
                add_css_class ("radio");
                break;
            default:
                assert_not_reached ();
        }
    }

    public void set_action (string name, Variant? target) {
        if (role == SWITCH) {
            item_switch.action_name = name;
            item_switch.action_target = target;
            return;
        }

        action_name = name;
        action_target = target;
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        if (!is_selected () || Adw.StyleManager.get_default ().high_contrast) {
            base.snapshot (snapshot);
            return;
        }

        var snapshot2 = new Gtk.Snapshot ();
        base.snapshot (snapshot2);
        var node = snapshot2.free_to_node ();

        if (node == null)
            return;

        snapshot.push_mask (INVERTED_ALPHA);

        snapshot.append_node (node);
        snapshot.pop ();

        snapshot.append_color (
            get_color (), {{ 0, 0 }, { get_width (), get_height () }}
        );
        snapshot.pop ();
    }

    public void set_state (Variant state) {
        if (role == SWITCH)
            item_switch.active = state.get_boolean ();

        if (role == RADIO)
            item_check.opacity = state.equal (radio_target) ? 1 : 0;
    }
}
