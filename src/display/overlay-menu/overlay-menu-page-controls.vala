// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/display/overlay-menu/overlay-menu-page-controls.ui")]
private class Highscore.OverlayMenuPageControls : OverlayMenuPage {
    public unowned Runner? runner { get; construct; }

    [GtkChild]
    private unowned Adw.Clamp preview_bin;

    private ControlsPreview preview;

    public OverlayMenuPageControls (GamepadController controller, Runner runner) {
        Object (controller: controller, runner: runner);
    }

    construct {
        preview = new ControlsPreview (controller, runner);
        preview_bin.child = preview;

        enable_select = false;
    }

    public override void cancel () {
        close ();
    }

    public override void left_page () {
        left_player ();
    }

    public override void right_page () {
        right_player ();
    }

    public override void navigate (InputDirection direction) {
        if (direction == LEFT)
            left_player ();

        if (direction == RIGHT)
            right_player ();
    }

    private void left_player () {
        if (get_direction () == LTR)
            preview.prev_player ();
        else
            preview.next_player ();
    }

    private void right_player () {
        if (get_direction () == LTR)
            preview.next_player ();
        else
            preview.prev_player ();
    }
}
