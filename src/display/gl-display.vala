// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public class Highscore.GLDisplay : Gtk.GLArea {
    public Game game { get; construct; }
    public bool secondary { get; construct; }

    public int top_inset { get; set; }
    public int bottom_inset { get; set; }
    public int left_inset { get; set; }
    public int right_inset { get; set; }

    private Runner _runner;
    public Runner runner {
        get { return _runner; }
        set {
            if (runner != null) {
                runner.weak_unref (runner_weak_notify_cb);

                runner.notify["dmabuf"].disconnect (update_dmabuf);

                runner.disconnect (redraw_id);
                redraw_id = 0;

                runner.disconnect (load_id);
                load_id = 0;
            }

            _runner = value;

            if (runner != null) {
                runner.weak_ref (runner_weak_notify_cb);

                set_screen_set (runner.screen_set);

                runner.notify["dmabuf"].connect (update_dmabuf);

                redraw_id = runner.redraw.connect (() => {
                    frame_number = runner.get_frame_number ();
                    region = runner.region;
                    aspect_ratio = runner.get_aspect_ratio ();
                    flipped = runner.flipped;
                    next_texture = runner.texture;

                    if (will_clear_snapshot) {
                        set_screen_set (runner.screen_set);
                        enable_frame_history = true;
                        will_clear_snapshot = false;
                    }

                    frame_changed = true;

                    queue_render ();
                });

                load_id = runner.loading_snapshot.connect (show_snapshot);

                if (runner.texture != null || runner.dmabuf != null) {
                    frame_number = runner.get_frame_number ();
                    region = runner.region;
                    aspect_ratio = runner.get_aspect_ratio ();
                    flipped = runner.flipped;
                    next_texture = runner.texture;

                    frame_changed = true;
                }

                update_dmabuf ();
            }

            queue_render ();
        }
    }

    private Gdk.Texture? next_texture;

    private ScreenSet screen_set;
    private Screen[]? screens;

    private bool screens_changed;

    private Hs.Region region;
    private double aspect_ratio;
    private bool flipped;

    private int last_width;
    private int last_height;

    private bool context_recreated;
    private bool will_clear_snapshot;

    private GLVertexArray vertex_array;
    private GLVertexBuffer vertex_buffer;
    private GLElementBuffer element_buffer;
    private GLShader platform_shader;

    private class FrameData {
        public GLTexture texture;
        public GLFramebuffer[] framebuffers;
        public GLFramebuffer[] blended_framebuffers;

        public FrameData (GLTexture texture, int n, bool blending) {
            this.texture = texture;
            framebuffers = {};

            if (blending)
                blended_framebuffers = {};

            set_n_screens (n, blending);
        }

        public void set_n_screens (int n, bool blending) {
            if (n > framebuffers.length) {
                for (int i = framebuffers.length; i < n; i++)
                    framebuffers += new GLFramebuffer ();
            } else if (n < framebuffers.length) {
                framebuffers = framebuffers[:n];
            }

            if (blending) {
                if (blended_framebuffers == null)
                    blended_framebuffers = {};

                if (n > blended_framebuffers.length) {
                    for (int i = blended_framebuffers.length; i < n; i++)
                        blended_framebuffers += new GLFramebuffer ();
                } else if (n < blended_framebuffers.length) {
                    blended_framebuffers = blended_framebuffers[:n];
                }
            } else {
                blended_framebuffers = null;
            }
        }
    }

    private FrameData? current_frame;
    private FrameData?[] frame_history;
    private bool enable_frame_history;
    private bool will_clear_history;

    private GLFramebuffer? history_framebuffer;
    private GLShader? history_shader;
    private FrameData? next_history_frame;

    private GLShader? blend_shader;

    private bool frame_changed;

    private ScreenLayout screen_layout;
    private ScreenLayout? prev_screen_layout;
    private Adw.SpringAnimation screen_layout_animation;

    private ulong redraw_id;
    private ulong load_id;
    private ulong screen_layout_id;

    private GameSettings settings;
    private Settings global_settings;

    private bool filter_changed;
    private bool platform_filter_changed;
    private bool platform_filter_recreated;

    private bool blending_changed;

    private VideoFilter video_filter;
    private VideoFilter? last_filter;
    public string video_filter_name { get; set; }
    public bool enable_crt { get; set; }
    public bool interframe_blending { get; set; }

    public bool dmabuf_changed;
    private GLTexture? dmabuf_texture;

    private uint frame_number;
    private uint last_frame_number;

    public GLDisplay (Game game, bool secondary) {
        Object (game: game, secondary: secondary);
    }

    construct {
        settings = new GameSettings (game);
        global_settings = new Settings ("app.drey.Highscore");

        settings.bind_property ("interframe-blending", this, "interframe-blending", SYNC_CREATE);
        settings.bind_property ("video-filter", this, "video-filter-name", SYNC_CREATE);

        global_settings.bind ("enable-crt", this, "enable-crt", GET);

        notify["video-filter-name"].connect (update_filter);
        notify["enable-crt"].connect (update_filter);
        notify["interframe-blending"].connect (() => {
            blending_changed = true;
            queue_render ();
        });
        notify["top-inset"].connect (queue_render);
        notify["bottom-inset"].connect (queue_render);
        notify["left-inset"].connect (queue_render);
        notify["right-inset"].connect (queue_render);

        screen_layout_animation = new Adw.SpringAnimation (
            this, 0, 1,
            new Adw.SpringParams (1, 1, 800),
            new Adw.CallbackAnimationTarget (value => {
                queue_render ();

                // FIXME: If the drag gesture is active, runner needs a notify
            })
        );
        screen_layout_animation.epsilon /= 10;
        screen_layout_animation.done.connect (() => prev_screen_layout = null);

        enable_frame_history = true;
    }

    private void update_dmabuf () {
        dmabuf_changed = true;

        queue_render ();
    }

    private string get_effective_filter () {
        if (video_filter_name == "antialiased")
            return "aann";

        if (video_filter_name == "sharp")
            return "nearest";

        if (video_filter_name == "blurry")
            return "bicubic";

        if (video_filter_name == "smooth")
            return "xbrz";

        if (video_filter_name == "accurate") {
            if (screen_set == null)
                return "nearest";

            var type = screen_set.get_screen_type (region);

            return type.get_filter (enable_crt);
        }

        return video_filter_name;
    }

    private VideoFilter create_filter (string name) {
        var register = VideoFilterRegister.get_instance ();

        var desc = register.get_filter (name);
        if (desc == null) {
            warning (
                "No such filter: %s, using nearest instead", name
            );

            return create_filter ("nearest");
        }

        return new VideoFilter (desc);
    }

    private void update_filter () {
        string current_name = null;
        if (video_filter != null)
            current_name = video_filter.name;

        var new_name = get_effective_filter ();

        if (current_name == new_name)
            return;

        video_filter = create_filter (new_name);
        filter_changed = true;

        update_history ();

        queue_render ();
    }

    ~GLDisplay () {
        runner = null;
    }

    private void runner_weak_notify_cb () {
        _runner = null;
        notify_property ("runner");
    }

    private void set_screen_layout (ScreenLayout layout, bool animate) {
        if (screen_layout_animation.state == PLAYING)
            screen_layout_animation.skip ();

        screens_changed = true;

        if (!animate) {
            screen_layout = layout;
            prev_screen_layout = null;
            queue_render ();
            return;
        }

        prev_screen_layout = screen_layout;
        screen_layout = layout;

        screen_layout_animation.play ();
    }

    private void update_history () {
        int length = 0;

        if (video_filter != null)
            length = video_filter.history_frames;

        if (interframe_blending)
            length++;

        set_history_length (length);
    }

    private void set_history_length (int length) {
        if (frame_history.length == length)
            return;

        if (length < frame_history.length) {
            frame_history = frame_history[:length];
            return;
        }

        int n = length - frame_history.length;
        for (int i = 0; i < n; i++)
            frame_history += null;
    }

    private void clear_history () {
        if (frame_history.length > 0 && frame_history[0] == null)
            return;

        for (int i = 0; i < frame_history.length; i++)
            frame_history = null;

        update_history ();
    }

    private void recreate_frame_framebuffers (FrameData frame, Screen[] screens) {
        var filter_mode = video_filter.filter_mode;
        var wrap_mode = video_filter.wrap_mode;

        for (int i = 0; i < screens.length; i++) {
            var screen = screens[i];

            int w = (int) Math.ceil (last_width * screen.area.size.width);
            int h = (int) Math.ceil (last_height * screen.area.size.height);

            frame.framebuffers[i].bind ();
            frame.framebuffers[i].create_texture (
                w, h, filter_mode, filter_mode, wrap_mode, RGBA8
            );
            frame.framebuffers[i].unbind ();

            if (interframe_blending && frame.blended_framebuffers != null) {
                frame.blended_framebuffers[i].bind ();
                frame.blended_framebuffers[i].create_texture (
                    w, h, filter_mode, filter_mode, wrap_mode, RGBA8
                );
                frame.blended_framebuffers[i].unbind ();
            }
        }
    }

    private FrameData create_frame (GLTexture texture) {
        var frame = new FrameData (texture, screens.length, interframe_blending);

        if (video_filter != null) {
            recreate_frame_framebuffers (frame, screens);
            attach_buffers ();
        }

        return frame;
    }

    private void copy_current_frame () {
        if (next_history_frame == null) {
            next_history_frame = create_frame (new GLTexture ());
            next_history_frame.texture.bind ();
            next_history_frame.texture.set_storage (
                current_frame.texture.width, current_frame.texture.height,
                RGBA8, GL_RGBA
            );
            next_history_frame.texture.set_params (NEAREST, NEAREST, EDGE);
            next_history_frame.texture.unbind ();
        }

        if (history_framebuffer == null)
            history_framebuffer = new GLFramebuffer ();

        if (history_shader == null) {
            var src = ResourceUtils.load_to_string (
                "/app/drey/Highscore/display/blit.glsl"
            );

            try {
                history_shader = new GLShader (context, src);
            } catch (GLShaderError e) {
                error ("Error in the blit shader: %s", e.message);
            }
        }

        history_shader.bind ();
        history_shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        history_shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

        history_framebuffer.bind ();
        history_framebuffer.attach_texture (next_history_frame.texture);
        history_framebuffer.unbind ();

        current_frame.texture.bind ();
        GLUtils.set_active_texture (0);
        history_shader.set_uniform_int ("u_source", 0);

        history_framebuffer.bind (GL_DRAW_FRAMEBUFFER);
        glViewport (
            0, 0, history_framebuffer.width, history_framebuffer.height
        );

        glClearColor (0.0f, 0.0f, 0.0f, 1.0f);
        glClear (GL_COLOR_BUFFER_BIT);

        element_buffer.draw ();

        history_framebuffer.unbind (GL_DRAW_FRAMEBUFFER);
        history_framebuffer.release_texture ();

        current_frame.texture.unbind ();

        history_shader.unbind ();

        attach_buffers ();
    }

    public override void css_changed (Gtk.CssStyleChange change) {
        base.css_changed (change);

        queue_render ();
    }

    public override void map () {
        base.map ();

        frame_changed = true;
        queue_render ();
    }

    public override void realize () {
        base.realize ();

        make_current ();

        if (get_error () != null)
            return;

        vertex_array = new GLVertexArray ();
        vertex_array.bind ();

        vertex_buffer = new GLVertexBuffer ();
        vertex_buffer.bind ();
        vertex_buffer.set_data ({
             0,  1,
             1,  1,
             1,  0,
             0,  0,
        });

        element_buffer = new GLElementBuffer ();
        element_buffer.bind ();
        element_buffer.set_data ({
            0, 1, 2,
            2, 3, 0
        });

        update_history ();

        context_recreated = true;

        get_native ().get_surface ().notify["scale"].connect (queue_render);
    }

    public override void unrealize () {
        get_native ().get_surface ().notify["scale"].disconnect (queue_render);

        make_current ();

        if (get_error () != null) {
            base.unrealize ();
            return;
        }

        video_filter.unrealize ();
        last_filter = null;
        filter_changed = true;

        vertex_array = null;
        vertex_buffer = null;
        element_buffer = null;
        platform_shader = null;

        current_frame = null;
        frame_history = {};
        history_framebuffer = null;
        history_shader = null;

        blend_shader = null;

        base.unrealize ();
    }

    public override bool render (Gdk.GLContext context) {
        var gl_error = get_error ();
        if (gl_error != null)
            critical (gl_error.message);

        uint frames_elapsed;

        if (frame_number >= last_frame_number)
            frames_elapsed = frame_number - last_frame_number;
        else
            frames_elapsed = SharedVideoBuffer.MAX_FRAME_NUMBER - last_frame_number + frame_number;

        last_frame_number = frame_number;

        if (filter_changed) {
            if (last_filter != null)
                last_filter.unrealize ();

            last_filter = video_filter;

            try {
                video_filter.realize (context);
            } catch (Error e) {
                if (video_filter_name == "nearest") {
                    // Nearest is our fallback shader. If it's broken, nothing we can do
                    error ("%s", e.message);
                } else {
                    warning ("%s", e.message);
                    last_filter = video_filter = create_filter ("nearest");

                    try {
                        video_filter.realize (context);
                    } catch (Error e2) {
                        error ("%s", e2.message);
                    }
                }
            }
        }

        glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
        glClear (GL_COLOR_BUFFER_BIT);

        draw_frame ();

        glFlush ();

        video_filter.end_frame (frames_elapsed);

        frame_changed = false;
        filter_changed = false;
        screens_changed = false;
        platform_filter_changed = false;
        platform_filter_recreated = false;
        blending_changed = false;
        context_recreated = false;
        will_clear_history = false;

        return Gdk.EVENT_STOP;
    }

    private void prepare_frames () {
        if (next_texture != null) {
            if (enable_frame_history && frame_history.length > 0) {
                var last_frame = frame_history[frame_history.length - 1];

                for (int i = frame_history.length - 1; i > 0; i--)
                    frame_history[i] = frame_history[i - 1];

                frame_history[0] = current_frame;
                current_frame = last_frame;
            }

            if (current_frame == null)
                current_frame = create_frame (new GLTexture ());

            if (current_frame.texture == dmabuf_texture)
                current_frame.texture = new GLTexture ();

            if (frame_changed) {
                current_frame.texture.bind ();
                current_frame.texture.upload (next_texture);
                current_frame.texture.unbind ();
            }
        } else if (dmabuf_texture != null) {
            FrameData last_frame = null;

            if (enable_frame_history && frame_history.length > 0) {
                last_frame = frame_history[frame_history.length - 1];

                for (int i = frame_history.length - 1; i > 0; i--)
                    frame_history[i] = frame_history[i - 1];
            }

            bool last_frame_copied = false;

            if (current_frame == null)
                current_frame = create_frame (dmabuf_texture);

            if (current_frame.texture != dmabuf_texture) {
                if (enable_frame_history && frame_history.length > 0) {
                    copy_current_frame ();
                    last_frame_copied = true;
                }

                current_frame.texture = dmabuf_texture;
            }

            if (enable_frame_history && frame_history.length > 0) {
                frame_history[0] = next_history_frame;
                next_history_frame = last_frame;

                if (!last_frame_copied)
                    copy_current_frame ();
            }
        }
    }

    private void draw_first_pass (
        FrameData frame,
        ScreenSet screen_set,
        Screen[] screens,
        ScreenLayout.Result[] layout_result
    ) {
        platform_shader.bind ();
        platform_shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        platform_shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

        for (int i = 0; i < screens.length; i++) {
            var screen = screens[i];
            var result = layout_result[i];

            if (!result.visible)
                continue;

            frame.texture.bind ();

            GLUtils.set_active_texture (0);
            frame.texture.set_params (NEAREST, NEAREST, EDGE);
            platform_shader.set_uniform_int ("u_source", 0);

            Graphene.Matrix mvp = {};
            mvp.init_identity ();
            mvp.scale (2, 2, 1);
            mvp.translate ({ -1, -1, 0 });

            Graphene.Vec2 position = {}, size = {};
            position.init (screen.area.origin.x, screen.area.origin.y);
            size.init (screen.area.size.width, screen.area.size.height);

            platform_shader.set_uniform_vec2 ("u_screenPosition", position);
            platform_shader.set_uniform_vec2 ("u_screenSize", size);
            platform_shader.set_uniform_bool ("u_flipped", flipped);
            platform_shader.set_uniform_matrix ("u_mvp", mvp);

            screen_set.setup_filter (platform_shader, screen.id);

            frame.framebuffers[i].bind (GL_DRAW_FRAMEBUFFER);
            glViewport (
                0, 0, frame.framebuffers[i].width, frame.framebuffers[i].height
            );

            glClearColor (0.0f, 0.0f, 0.0f, 1.0f);
            glClear (GL_COLOR_BUFFER_BIT);

            element_buffer.draw ();

            frame.framebuffers[i].unbind (GL_DRAW_FRAMEBUFFER);
            frame.texture.unbind ();
        }

        platform_shader.unbind ();
    }

    private void blend_frames (FrameData new_frame, FrameData old_frame) {
        if (blend_shader == null) {
            var src = ResourceUtils.load_to_string (
                "/app/drey/Highscore/display/blend.glsl"
            );

            try {
                blend_shader = new GLShader (context, src);
            } catch (GLShaderError e) {
                error ("Error in the blend shader: %s", e.message);
            }
        }

        blend_shader.bind ();
        blend_shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        blend_shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

        for (int i = 0; i < new_frame.blended_framebuffers.length; i++) {
            var target_fb = new_frame.blended_framebuffers[i];

            GLUtils.set_active_texture (0);
            old_frame.framebuffers[i].bind_texture ();
            blend_shader.set_uniform_int ("u_frame1", 0);

            GLUtils.set_active_texture (1);
            new_frame.framebuffers[i].bind_texture ();
            blend_shader.set_uniform_int ("u_frame2", 1);

            target_fb.bind (GL_DRAW_FRAMEBUFFER);
            glViewport (0, 0, target_fb.width, target_fb.height);

            glClearColor (0.0f, 0.0f, 0.0f, 1.0f);
            glClear (GL_COLOR_BUFFER_BIT);

            element_buffer.draw ();

            target_fb.unbind (GL_DRAW_FRAMEBUFFER);

            GLUtils.set_active_texture (1);
            new_frame.framebuffers[i].unbind ();

            GLUtils.set_active_texture (0);
            old_frame.framebuffers[i].unbind ();
        }

        blend_shader.unbind ();
    }

    private void draw_frame () {
        if (blending_changed)
            update_history ();

        if ((screens_changed || context_recreated || blending_changed) && screens != null) {
            if (current_frame != null)
                current_frame.set_n_screens (screens.length, interframe_blending);

            if (next_history_frame != null)
                next_history_frame.set_n_screens (screens.length, interframe_blending);

            foreach (var frame in frame_history) {
                if (frame == null)
                    continue;

                frame.set_n_screens (screens.length, interframe_blending);
            }
        }

        if (runner != null && dmabuf_changed) {
            if (runner.dmabuf != null)
                dmabuf_texture = new GLTexture.from_dmabuf (runner.dmabuf);
            else
                dmabuf_texture = null;

            dmabuf_changed = false;
        }

        if (will_clear_history)
            clear_history ();

        if (frame_changed)
            prepare_frames ();

        if (current_frame == null)
            return;

        bool framebuffers_recreated = false;

        int scale = (int) Math.ceil (get_native ().get_surface ().scale);
        int widget_width = get_width () * scale;
        int widget_height = get_height () * scale;

        double[] opacities;
        var layout_result = ScreenLayout.interpolate (
            prev_screen_layout,
            screen_layout,
            screen_layout_animation.value,
            {
                widget_width,
                widget_height,
                top_inset * scale,
                bottom_inset * scale,
                left_inset * scale,
                right_inset * scale,
            },
            (float) aspect_ratio,
            screens,
            out opacities
        );

        assert (screens.length == layout_result.length);

        if (platform_filter_recreated || context_recreated)
            create_platform_filter ();

        if (current_frame.texture.width != last_width ||
            current_frame.texture.height != last_height ||
            screens_changed ||
            filter_changed ||
            blending_changed ||
            context_recreated) {
            last_width = current_frame.texture.width;
            last_height = current_frame.texture.height;

            recreate_frame_framebuffers (current_frame, screens);

            if (next_history_frame != null)
                recreate_frame_framebuffers (next_history_frame, screens);

            foreach (var frame in frame_history) {
                if (frame == null)
                    continue;

                recreate_frame_framebuffers (frame, screens);
            }

            attach_buffers ();

            framebuffers_recreated = true;
        }

        VideoFilter.ScreenSize[] sizes = {};

        for (int i = 0; i < screens.length; i++) {
            var result = layout_result[i];

            VideoFilter.ScreenSize size = {
                current_frame.framebuffers[i].width,
                current_frame.framebuffers[i].height,
                (int) Math.ceil (result.width),
                (int) Math.ceil (result.height),
                (float) screens[i].get_effective_aspect_ratio ((float) aspect_ratio),
            };
            sizes += size;
        }

        video_filter.resize (widget_width, widget_height, sizes);

        int[] indices = new int[screens.length];
        for (int i = 0; i < screens.length; i++)
            indices[i] = i;

        qsort_with_data<int> (indices, sizeof (int), (a, b) => {
            if (opacities[a] < opacities[b])
                return -1;
            if (opacities[a] > opacities[b])
                return 1;
            return 0;
        });

        render_first_pass_and_blend (
            layout_result,
            framebuffers_recreated
        );

        render_filter_passes (
            widget_width,
            widget_height,
            layout_result,
            opacities,
            indices
        );
    }

    private void render_first_pass_and_blend (
        ScreenLayout.Result[] layout_result,
        bool framebuffers_recreated
    ) {
        draw_first_pass (current_frame, screen_set, screens, layout_result);

        if (enable_frame_history && frame_history.length > 0) {
            if (screens_changed || platform_filter_changed ||
                context_recreated || framebuffers_recreated) {
                foreach (var frame in frame_history) {
                    if (frame == null)
                        continue;

                    draw_first_pass (frame, screen_set, screens, layout_result);
                }
            } else if (dmabuf_texture != null &&
                       frame_history.length > 0 &&
                       frame_history[0] != null) {
                draw_first_pass (frame_history[0], screen_set, screens, layout_result);
            }

            if (interframe_blending && frame_history.length > 0)
                do_blending (framebuffers_recreated);
        }

        attach_buffers ();
    }

    private void do_blending (bool framebuffers_recreated) {
        if (frame_history[0] != null)
            blend_frames (current_frame, frame_history[0]);

        if (blending_changed || screens_changed || platform_filter_changed ||
            context_recreated || framebuffers_recreated) {
            for (int i = 0; i < frame_history.length - 1; i++) {
                if (frame_history[i] == null || frame_history[i + 1] == null)
                    continue;

                blend_frames (frame_history[i], frame_history[i + 1]);
            }
        } else if (dmabuf_texture != null &&
                   frame_history.length > 1 &&
                   frame_history[0] != null &&
                   frame_history[1] != null) {
            blend_frames (frame_history[0], frame_history[1]);
       }
    }

    private void render_filter_passes (
        int widget_width,
        int widget_height,
        ScreenLayout.Result[] layout_result,
        double[] opacities,
        int[] indices
    ) {
        bool[] screen_visible = {};

        int scale = (int) Math.ceil (get_native ().get_surface ().scale);

        for (int i = 0; i < screens.length; i++) {
            var result = layout_result[indices[i]];

            if (!result.visible) {
                screen_visible += false;
                continue;
            }

            var transform = new Gsk.Transform ();
            transform = transform.translate ({ -1, 1 });
            transform = transform.scale (2, -2);
            transform = transform.scale (1.0f / widget_width, 1.0f / widget_height);

            float top = top_inset * scale;
            float bottom = bottom_inset * scale;
            float left = left_inset * scale;
            float right = right_inset * scale;

            transform = transform.translate ({
                left + (widget_width - left - right) / 2.0f,
                top + (widget_height - top - bottom) / 2.0f
            });
            transform = transform.rotate (result.angle);
            transform = transform.translate ({
                -left - (widget_width - left - right) / 2.0f,
                -top - (widget_height - top - bottom) / 2.0f
            });

            transform = transform.transform (result.transform);
            transform = transform.scale (result.width, result.height);

            Graphene.Rect rect = {{ 0, 0 }, { 1, 1 }};
            var transformed_rect = transform.transform_bounds (rect);

            if (!transformed_rect.intersection ({{ -1, -1 }, { 2, 2 }}, null)) {
                screen_visible += false;
                continue;
            }

            screen_visible += true;

            video_filter.set_mvp (indices[i], transform.to_matrix ());
            video_filter.set_opacity (indices[i], opacities[indices[i]]);

            GLFramebuffer fb;

            if (interframe_blending &&
                frame_history.length > 0 &&
                frame_history[0] != null) {
                fb = current_frame.blended_framebuffers[indices[i]];
            } else {
                fb = current_frame.framebuffers[indices[i]];
            }

            GLFramebuffer[] history = {};

            for (int j = 0; j < video_filter.history_frames; j++) {
                if (frame_history[j] == null)
                    break;

                if (interframe_blending &&
                    frame_history.length > j + 1 &&
                    frame_history[j + 1] != null) {
                    history += frame_history[j].blended_framebuffers[indices[i]];
                } else {
                    history += frame_history[j].framebuffers[indices[i]];
                }
            }

            video_filter.set_screen_framebuffers (indices[i], fb, history);
        }

        int n_passes = video_filter.n_passes;
        for (int pass = 0; pass < n_passes; pass++) {
            if (pass == n_passes - 1)
                attach_buffers ();

            video_filter.bind (pass);

            for (int i = 0; i < screens.length; i++) {
                if (!screen_visible[i])
                    continue;

                video_filter.start_render (pass, indices[i]);

                element_buffer.draw ();

                video_filter.end_render (pass, indices[i]);
            }

            video_filter.unbind (pass);
        }
    }

    private void transform_coords_do (
        int? screen_id,
        double in_x,
        double in_y,
        out Screen? out_screen,
        out double out_x,
        out double out_y
    ) {
        if (current_frame == null) {
            out_screen = null;
            out_x = -1;
            out_y = -1;
            return;
        }

        int scale = (int) Math.ceil (get_native ().get_surface ().scale);
        int widget_width = get_width () * scale;
        int widget_height = get_height () * scale;

        float top = top_inset * scale;
        float bottom = bottom_inset * scale;
        float left = left_inset * scale;
        float right = right_inset * scale;

        var layout_info = ScreenLayout.interpolate (
            prev_screen_layout,
            screen_layout,
            screen_layout_animation.value,
            {
                widget_width,
                widget_height,
                top,
                bottom,
                left,
                right,
            },
            (float) aspect_ratio,
            screens,
            null
        );

        // Reverse direction since the last screen will end up drawn at the top
        for (int i = layout_info.length - 1; i >= 0; i--) {
            var transform = new Gsk.Transform ();

            if (screen_id != null && screen_id != screens[i].id)
                continue;

            transform = transform.scale (1 / layout_info[i].width, 1 / layout_info[i].height);
            transform = transform.transform (layout_info[i].transform.invert ());

            transform = transform.translate ({
                left + (widget_width - left - right) / 2.0f,
                top + (widget_height - top - bottom) / 2.0f
            });
            transform = transform.rotate (-layout_info[i].angle);
            transform = transform.translate ({
                -left - (widget_width - left - right) / 2.0f,
                -top - (widget_height - top - bottom) / 2.0f
            });

            transform = transform.scale (scale, scale);

            var point = transform.transform_point ({ (float) in_x, (float) in_y });

            if (video_filter_name == "accurate") {
                var type = screen_set.get_screen_type (region);
                float warped_x, warped_y;

                bool warped = type.warp_pointer (
                    enable_crt,
                    last_width * screens[i].area.size.width,
                    last_height * screens[i].area.size.height,
                    (float) screens[i].get_effective_aspect_ratio ((float) aspect_ratio),
                    (float) point.x, (float) point.y,
                    out warped_x, out warped_y
                );

                if (warped) {
                    point.x = warped_x;
                    point.y = warped_y;
                }
            }

            if (screen_id == null && (point.x < 0 || point.x > 1 || point.y < 0 || point.y > 1))
                continue;

            out_screen = screens[i];
            out_x = point.x.clamp (0, 1);
            out_y = point.y.clamp (0, 1);
            return;
        }

        out_screen = null;
        out_x = -1;
        out_y = -1;
    }

    public Screen? find_screen (double x, double y) {
        if (screens == null)
            return null;

        Screen? ret;
        transform_coords_do (null, x, y, out ret, null, null);
        return ret;
    }

    public void transform_coords (int screen_id, double x, double y, out double out_x, out double out_y) {
        transform_coords_do (screen_id, x, y, null, out out_x, out out_y);
    }

    public void show_snapshot (Snapshot snapshot) {
        try {
            region = snapshot.region;
            aspect_ratio = snapshot.screenshot_aspect_ratio;
            flipped = snapshot.screenshot_flipped;

            next_texture = snapshot.get_screenshot ();
            enable_frame_history = false;
            will_clear_history = true;

            frame_changed = true;
        } catch (Error e) {
            critical ("Failed to show snapshot: %s", e.message);
        }

        set_screen_set (ScreenSet.create (DISPLAY, null, snapshot));
    }

    public void clear_snapshot (bool now) {
        if (runner == null)
            return;

        if (now) {
            region = runner.region;
            aspect_ratio = runner.get_aspect_ratio ();
            flipped = runner.flipped;
            frame_number = runner.get_frame_number ();

            next_texture = runner.texture;

            frame_changed = true;
            enable_frame_history = true;

            set_screen_set (runner.screen_set);

            queue_render ();
        } else {
            will_clear_snapshot = true;
        }
    }

    private void set_screen_set (ScreenSet new_screen_set) {
        if (screen_set != null) {
            screen_set.disconnect (screen_layout_id);
            screen_layout_id = 0;
        }

        screen_set = new_screen_set;
        screens = screen_set.get_screens ();
        screens_changed = true;

        if (secondary) {
            set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0), false);

            screen_layout_id = screen_set.secondary_layout_changed.connect (animate => {
                set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0), animate);
            });
        } else {
            set_screen_layout (screen_set.layout, false);

            screen_layout_id = screen_set.layout_changed.connect (animate => {
                set_screen_layout (screen_set.layout, animate);
            });
        }

        screen_set.filter_changed.connect (() => {
            platform_filter_changed = true;

            queue_render ();
        });

        screen_set.screen_type_changed.connect (update_filter);

        platform_filter_recreated = true;
        platform_filter_changed = true;

        update_filter ();
        queue_render ();
    }

    private void create_platform_filter () {
        var shared = ResourceUtils.load_to_string (
            "/app/drey/Highscore/runner/screen/platform-shared.glsl"
        );

        var main = ResourceUtils.load_to_string (
            screen_set.get_filter_path ()
        );

        try {
            platform_shader = new GLShader (context, @"$shared\n\n$main");
        } catch (GLShaderError e) {
            error ("Error in platform shader: %s", e.message);
        }
    }

    private Gdk.Texture get_screen_texture (FrameData data, int index) {
        var fb = data.framebuffers[index];

        var builder = new Gdk.GLTextureBuilder ();
        builder.context = context;
        builder.format = R8G8B8X8;
        builder.width = (int) Math.ceil (fb.width);
        builder.height = (int) Math.ceil (fb.height);
        builder.id = fb.get_texture_id ();

        return builder.build (null, null);
    }

    public Gdk.Texture? take_raw_screenshot () {
        if (runner != null && runner.texture != null)
            return runner.texture;

        if (dmabuf_texture != null) {
            var builder = new Gdk.GLTextureBuilder ();
            builder.context = context;
            builder.format = R8G8B8;
            builder.width = dmabuf_texture.width;
            builder.height = dmabuf_texture.height;
            builder.id = dmabuf_texture.get_id ();

            return builder.build (null, null);
        }

        return next_texture;
    }

    public Gdk.Texture? take_screenshot () {
        if (current_frame == null)
            return null;

        var screenshot_screen_set = ScreenSet.create (SCREENSHOT, screen_set.runner, screen_set.snapshot);
        var screens = screenshot_screen_set.get_screens ();

        ScreenLayout screen_layout;

        if (secondary)
            screen_layout = screenshot_screen_set.secondary_layout ?? new SingleLayout (0);
        else
            screen_layout = screenshot_screen_set.layout;

        float screenshot_width, screenshot_height;

        screen_layout.measure (
            current_frame.texture.width, current_frame.texture.height, screens,
            out screenshot_width, out screenshot_height
        );

        screenshot_width = Math.roundf (screenshot_width);
        screenshot_height = Math.roundf (screenshot_height);

        var layout_result = screen_layout.layout (
            {
                screenshot_width,
                screenshot_height,
                0, 0, 0, 0,
            },
            current_frame.texture.width / (float) current_frame.texture.height,
            screens
        );

        make_current ();

        var frame_data = new FrameData (current_frame.texture, screens.length, false);
        recreate_frame_framebuffers (frame_data, screens);

        draw_first_pass (frame_data, screenshot_screen_set, screens, layout_result);

        var snapshot = new Gtk.Snapshot ();

        for (int i = 0; i < screens.length; i++) {
            var result = layout_result[i];

            if (!result.visible)
                continue;

            var texture = get_screen_texture (frame_data, i);

            snapshot.save ();

            snapshot.translate ({
                screenshot_width / 2.0f, screenshot_height / 2.0f
            });
            snapshot.rotate (result.angle);
            snapshot.translate ({
                -screenshot_width / 2.0f, -screenshot_height / 2.0f
            });

            snapshot.transform (result.transform);

            Graphene.Rect rect = {
                { 0, 0 }, { result.width, result.height }
            };

            snapshot.append_texture (texture, rect);

            snapshot.restore ();
        }

        var node = snapshot.free_to_node ();

        var renderer = new Gsk.CairoRenderer ();
        try {
            renderer.realize_for_display (get_display ());
        } catch (Error e) {
            error ("Failed to realize cover thumbnail renderer: %s", e.message);
        }

        var ret = renderer.render_texture (
            node, {{ 0, 0 }, { screenshot_width, screenshot_height }}
        );

        renderer.unrealize ();

        return ret;
    }
}
