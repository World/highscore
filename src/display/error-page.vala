// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.ErrorPage : Adw.Bin {
    private Adw.StatusPage status_page;

    construct {
        status_page = new Adw.StatusPage ();

        child = status_page;
    }

    public void show_error (
        string title,
        string description,
        string icon_name = "dialog-error-symbolic",
        string? button_label = null,
        string? button_action = null
    ) {
        status_page.icon_name = icon_name;
        status_page.title = title;
        status_page.description = description;

        if (button_label != null && button_action != null) {
            var button = new Gtk.Button.with_label (button_label) {
                halign = CENTER,
                can_shrink = true,
                use_underline = true,
                action_name = button_action
            };
            button.add_css_class ("pill");
            button.add_css_class ("suggested-action");

            status_page.child = button;
        } else {
            status_page.child = null;
        }
    }
}
