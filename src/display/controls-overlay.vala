// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/display/controls-overlay.ui")]
public class Highscore.ControlsOverlay : Adw.Bin {
    public signal void dismiss ();

    public unowned Runner? runner { get; construct; }
    public bool startup { get; construct; }

    [GtkChild]
    private unowned Adw.Bin preview_bin;
    [GtkChild]
    private unowned GamepadHintBar gamepad_hint_bar;
    [GtkChild]
    private unowned Gtk.Button continue_button;

    private ControlsPreview preview;

    public ControlsOverlay (Runner runner, bool startup) {
        Object (runner: runner, startup: startup);
    }

    construct {
        var controller = new GamepadController (this);
        gamepad_hint_bar.controller = controller;

        preview = new ControlsPreview (controller, runner);
        preview_bin.child = preview;

        controller.select.connect (() => dismiss ());
        controller.cancel.connect (() => dismiss ());
        controller.left_page.connect (left_player);
        controller.right_page.connect (right_player);
        controller.navigate.connect (direction => {
            if (direction == LEFT)
                left_player ();
            else if (direction == RIGHT)
                right_player ();
        });

        controller.cancel_label = startup ? _("Continue") : _("Back");
        continue_button.label = controller.cancel_label;
    }

    static construct {
        set_css_name ("controls-overlay");

        install_action ("overlay.dismiss", null, widget => {
            var self = widget as ControlsOverlay;

            self.dismiss ();
        });
        install_action ("overlay.left", null, widget => {
            var self = widget as ControlsOverlay;

            self.left_player ();
        });
        install_action ("overlay.right", null, widget => {
            var self = widget as ControlsOverlay;

            self.right_player ();
        });
    }

    protected override bool grab_focus () {
        return continue_button.grab_focus ();
    }

    private void left_player () {
        if (get_direction () == LTR)
            preview.prev_player ();
        else
            preview.next_player ();
    }

    private void right_player () {
        if (get_direction () == LTR)
            preview.next_player ();
        else
            preview.prev_player ();
    }
}
