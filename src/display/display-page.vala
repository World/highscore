// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/display/display-page.ui")]
public class Highscore.DisplayPage : Adw.NavigationPage {
    private const int AUTOSAVE_DELAY_MS = 5 * 60 * 1000;

    public signal void error ();
    public signal void add_toast (Adw.Toast toast);

    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned Gtk.Stack header_stack;
    [GtkChild]
    private unowned Gtk.Revealer loading_revealer;
    [GtkChild]
    private unowned Gtk.Revealer menu_revealer;
    [GtkChild]
    private unowned OverlayMenu overlay_menu;
    [GtkChild]
    private unowned Gtk.Revealer controls_revealer;
    [GtkChild]
    private unowned Gtk.Revealer notification_revealer;
    [GtkChild]
    private unowned FullscreenView fullscreen_view;
    [GtkChild]
    private unowned Gtk.Overlay main_overlay;
    [GtkChild]
    private unowned Adw.Bin platform_area;
    [GtkChild]
    private unowned ErrorPage error_page;
    [GtkChild]
    private unowned Gtk.MenuButton primary_menu_button;
    [GtkChild]
    private unowned Gtk.MenuButton secondary_menu_button;
    [GtkChild]
    private unowned Adw.MultiLayoutView multi_layout_view;
    [GtkChild]
    private unowned Adw.Bin snapshots_bin;

    public Game game { get; construct; }
    public bool standalone { get; construct; }

    public unowned Runner runner { get; private set; }
    public string window_title { get; private set; }
    public string subtitle { get; set; }
    public bool fullscreen { get; set; }
    public bool compact { get; set; }
    public bool snapshots_open { get; set; }

    public bool loading { get; set; }

    private GameView view;

    private SecondaryWindow? secondary_window;
    private ControlsOverlay? controls_overlay;

    private Settings settings;
    private Binding fullscreen_binding;
    private Binding top_inset_binding;

    private uint inhibit_cookie;
    private Gtk.ApplicationInhibitFlags inhibit_flags;

    private SnapshotManager snapshot_manager;

    private bool loading_snapshot;

    private uint unload_snapshots_idle_id;
    private uint save_timeout_cb_id;
    private uint show_controller_notification_id;

    private string? last_controller_type;

    public DisplayPage (Game game, bool standalone) {
        Object (game: game, standalone: standalone);
    }

    construct {
        view = new GameView (game, false);
        main_overlay.child = view;

        view.bind_property ("top-inset",    loading_revealer, "margin-top",    SYNC_CREATE);
        view.bind_property ("bottom-inset", loading_revealer, "margin-bottom", SYNC_CREATE);

        view.bind_property ("top-inset",    menu_revealer, "margin-top",    SYNC_CREATE);
        view.bind_property ("bottom-inset", menu_revealer, "margin-bottom", SYNC_CREATE);

        view.bind_property ("top-inset",    controls_revealer, "margin-top",    SYNC_CREATE);
        view.bind_property ("bottom-inset", controls_revealer, "margin-bottom", SYNC_CREATE);

        settings = new Settings ("app.drey.Highscore");

        settings.changed["pause-when-inactive"].connect (() => {
            if (runner == null || !runner.running)
                return;

            if (get_is_active ())
                runner.resume.begin ();
            else
                runner.pause.begin ();
        });
        settings.changed["autosave"].connect (() => {
            if (runner == null || !runner.running)
                return;

            update_autosave ();
        });

        notify["title"].connect (update_title);
        notify["subtitle"].connect (update_title);
        notify["loading"].connect (() => {
            if (loading)
                loading_revealer.visible = true;

            header_stack.visible_child_name = loading ? "loading" : "game";
            loading_revealer.reveal_child = loading;
            update_actions ();
        });

        var settings = new Settings ("app.drey.Highscore.state");

        fullscreen = settings.get_boolean (
            standalone ? "standalone-window-fullscreen" : "main-window-fullscreen"
        );

        notify["fullscreen"].connect (update_fullscreen);

        update_fullscreen ();

        loading = true;

        game.bind_property ("title", this, "title", SYNC_CREATE);

        fullscreen_view.track_menu_button (primary_menu_button);
        fullscreen_view.track_menu_button (secondary_menu_button);

        notify["snapshots-open"].connect (() => {
            if (snapshots_open) {
                if (secondary_window != null)
                    secondary_window.snapshots_open = true;

                update_active ();
                return;
            }

            if (!loading_snapshot) {
                view.clear_snapshot (false);

                if (secondary_window != null) {
                    secondary_window.snapshots_open = false;
                    secondary_window.clear_snapshot (false);
                }
            }

            Idle.add_once (() => view.grab_focus ());

            update_active ();
        });

        notify["compact"].connect (update_compact);

        update_compact ();
    }

    protected override void map () {
        base.map ();

        var window = get_root () as Gtk.Window;

        window.notify["is-active"].connect (update_active);
        window.notify["visible-dialog"].connect (update_active);

        action_set_enabled ("game.back", window is MainWindow);
    }

    protected override void unmap () {
        var window = get_root () as Gtk.Window;

        window.notify["is-active"].disconnect (update_active);
        window.notify["visible-dialog"].disconnect (update_active);

        base.unmap ();

        action_set_enabled ("game.back", false);
    }

    public override void showing () {
        var window = get_root () as Gtk.Window;

        fullscreen_binding = bind_property (
            "fullscreen", window,
            "fullscreened", SYNC_CREATE | BIDIRECTIONAL
        );
    }

    public override void hiding () {
        var window = get_root () as Gtk.Window;

        fullscreen_binding.unbind ();
        fullscreen_binding = null;

        window.unfullscreen ();
    }

    private void update_actions () {
        bool error = stack.visible_child_name == "error";
        bool has_autoload = snapshot_manager != null && snapshot_manager.autoload_snapshot != null;

        action_set_enabled ("game.restart",    !loading && !error);
        action_set_enabled ("game.save",       !loading && !error);
        action_set_enabled ("game.load",       !loading && !error && has_autoload);
        action_set_enabled ("game.snapshots",  !loading && !error);
        action_set_enabled ("game.screenshot", !loading && !error);
        action_set_enabled ("game.properties", !loading && !error);
        action_set_enabled ("game.toggle-fullscreen", !loading && !error);
    }

    private void update_fullscreen () {
        var settings = new Settings ("app.drey.Highscore.state");
        settings.set_boolean (
            standalone ? "standalone-window-fullscreen" : "main-window-fullscreen",
            fullscreen
        );

        if (fullscreen) {
            if (top_inset_binding != null) {
                top_inset_binding.unbind ();
                top_inset_binding = null;
            }

            view.top_inset = 0;
            return;
        }

        if (top_inset_binding == null) {
            top_inset_binding = fullscreen_view.bind_property (
                "top-bar-height", view, "top-inset", SYNC_CREATE
            );
        }
    }

    private void set_page (string page) {
        if (page == "game") {
            stack.visible_child_name = "game";
            view.grab_focus ();
            can_pop = false;
        } else if (page == "error") {
            stack.visible_child_name = "error";
            can_pop = true;
            fullscreen = false;

            error ();
        }

        update_actions ();
    }

    private async void save_and_pop () {
        if (loading && stack.visible_child_name == "game")
            return;

        yield save_and_close ();

        activate_action ("navigation.pop", null);
    }

    public async void save () {
        try {
            yield snapshot_manager.save_snapshot (runner, view, MARK_AS_AUTOLOAD);
            reset_autosave ();
        } catch (Error e) {
            DBusError.strip_remote_error (e);
            critical ("Failed to save state: %s", e.message);
            view.runner = null;
            runner = null;
        }
    }

    public async void save_and_close () {
        if (runner == null) {
            update_secondary_window ();
            return;
        }

        try {
            stop_autosave ();
            yield runner.pause ();
            var snapshot = yield snapshot_manager.save_snapshot (
                runner, view, AUTOMATIC
            );
            yield snapshot_manager.trim_autosaves ();
            view.show_snapshot (snapshot);
            yield runner.stop ();
            view.runner = null;
            runner = null;
            update_secondary_window ();
        } catch (Error e) {
            DBusError.strip_remote_error (e);
            critical ("Failed to save state and close: %s", e.message);
            view.runner = null;
            runner = null;
        }
    }

    private async void save_and_restart () {
        try {
            stop_autosave ();
            yield snapshot_manager.save_snapshot (
                runner, view, AUTOMATIC | MARK_AS_AUTOLOAD
            );
            yield snapshot_manager.trim_autosaves ();
            yield runner.reset (true);
            update_autosave ();
        } catch (Error e) {
            DBusError.strip_remote_error (e);
            critical ("Failed to save state and restart: %s", e.message);
        }

        view.grab_focus ();
    }

    private async void auto_save () {
        try {
            yield snapshot_manager.save_snapshot (runner, view, AUTOMATIC);
            yield snapshot_manager.trim_autosaves ();
        } catch (Error e) {
            DBusError.strip_remote_error (e);
            critical ("Failed to save the game: %s", e.message);

            add_toast (new Adw.Toast.format (
                _("Failed to save the game: %s"), e.message)
            );
        }
    }

    private async void load (Snapshot snapshot, SnapshotList? snapshot_list) {
        if (snapshot_list == null || !snapshot_list.has_just_saved ()) {
            try {
                yield snapshot_manager.save_snapshot (runner, view, AUTOMATIC);
            } catch (Error e) {
                DBusError.strip_remote_error (e);
                critical ("Failed to save before loading: %s", e.message);

                add_toast (new Adw.Toast.format (
                    _("Failed to save before loading: %s"), e.message)
                );
            }
        }

        loading_snapshot = true;

        if (snapshot_list != null)
            snapshot_list.close ();

        view.show_snapshot (snapshot);

        if (secondary_window != null)
            secondary_window.show_snapshot (snapshot);

        try {
            yield runner.load_snapshot (snapshot, true);
        } catch (Error e) {
            DBusError.strip_remote_error (e);
            critical ("Failed to load snapshot: %s", e.message);

            add_toast (new Adw.Toast.format (
                _("Failed to load snapshot: %s"), e.message)
            );
        }

        view.clear_snapshot (false);

        if (secondary_window != null)
            secondary_window.clear_snapshot (false);

        view.grab_focus ();

        loading_snapshot = false;
    }

    static construct {
        install_action ("game.back", null, widget => {
            var self = widget as DisplayPage;

            self.save_and_pop.begin ();
        });

        install_action ("game.exit", null, widget => {
            var self = widget as DisplayPage;

            if (self.standalone) {
                var root = self.get_root ();
                if (root == null)
                    return;

                assert (root is Gtk.Window);

                var window = root as Gtk.Window;
                window.close ();
                return;
            }

            self.save_and_pop.begin ();
        });

        install_action ("game.restart", null, widget => {
            var self = widget as DisplayPage;

            self.close_menu ();
            self.save_and_restart.begin ();
        });

        install_action ("game.save", null, widget => {
            var self = widget as DisplayPage;

            self.close_menu ();
            self.save.begin ();
        });

        install_action ("game.load", null, widget => {
            var self = widget as DisplayPage;

            var snapshot = self.snapshot_manager.autoload_snapshot;
            if (snapshot != null) {
                self.load.begin (snapshot, null, () => self.close_menu ());
            }
        });

        install_action ("game.snapshots", null, widget => {
            var self = widget as DisplayPage;

            self.show_snapshots ();
        });

        install_action ("game.show-controls", null, widget => {
            var self = widget as DisplayPage;

            self.show_controls (false);
        });

        install_action ("game.screenshot", null, widget => {
            var self = widget as DisplayPage;

            Screenshot.take_screenshot.begin (self.view);
        });

        install_action ("game.properties", null, widget => {
            var self = widget as DisplayPage;

            new PropertiesDialog (self.game).present (self);
        });

        install_action ("game.toggle-fullscreen", null, widget => {
            var self = widget as DisplayPage;

            self.fullscreen = !self.fullscreen;
        });

        install_action ("game.install-firmware", null, widget => {
            var self = widget as DisplayPage;

            var dialog = new PreferencesDialog ();
            dialog.show_page ("firmware");
            dialog.present (self);
            dialog.closed.connect (() => {
                if (dialog.firmware_installed) {
                    self.set_page ("game");
                    self.start_game.begin ();
                }
            });
        });
    }

    private void update_secondary_window () {
        if (runner != null && runner.screen_set.secondary_layout != null) {
            if (secondary_window != null)
                return;

            secondary_window = new SecondaryWindow (this);
            secondary_window.notify["is-active"].connect (update_active);
            secondary_window.notify["visible-dialog"].connect (update_active);

            secondary_window.close_request.connect (() => {
                if (runner != null)
                    runner.secondary_view_closed ();
                secondary_window = null;

                return Gdk.EVENT_PROPAGATE;
            });
        } else {
            if (secondary_window == null)
                return;

            secondary_window.close ();
            secondary_window = null;
        }
    }

    public async void start_game () {
        try {
            Library.get_instance ().update_last_played (game);
        } catch (Error e) {
            critical ("Failed to update last played: %s", e.message);
        }

        var game_settings = new GameSettings (game);
        bool load_state = game_settings.load_state_on_startup;

        var core = CoreRegister.get_register ().get_core_for_platform (
            game.platform
        );

        if (core == null) {
            error_page.show_error (
                _("Unable to run the game"),
                _("No matching core found")
            );
            set_page ("error");
            return;
        }

        if (snapshot_manager == null) {
            snapshot_manager = new SnapshotManager (game, core);
            snapshot_manager.notify["autoload-snapshot"].connect (update_actions);
            yield snapshot_manager.scan ();
        }

        var snapshot = snapshot_manager.get_latest (true);
        if (snapshot == null || !snapshot.is_compatible ())
            load_state = false;

        if (load_state)
            view.show_snapshot (snapshot);

        var runner = new Runner (core, game, snapshot, load_state);
        runner.stopped.connect (error => {
            uninhibit (LOGOUT);

            stop_autosave ();

            view.runner = null;
            this.runner = null;

            if (error == null)
                return;

            error_page.show_error (_("Game Crashed"), error);
            set_page ("error");
        });

        runner.home_pressed.connect (() => {
            overlay_menu.reset ();
            menu_revealer.reveal_child = true;

            update_active ();
            overlay_menu.grab_focus ();

            if (secondary_window != null)
                secondary_window.open_menu ();
        });

        this.runner = runner;
        view.runner = runner;
        view.clear_snapshot (false);
        view.grab_focus ();

        runner.screen_set.secondary_layout_changed.connect (() => {
            update_secondary_window ();

            if (secondary_window != null)
                secondary_window.present ();
        });

        update_secondary_window ();

        if (secondary_window != null)
            secondary_window.present ();

        try {
            yield runner.start ();
        } catch (Error e) {
            var remote = DBusError.get_remote_error (e);

            if (remote == "org.gnome.gitlab.alicem.libhighscore.Error.MissingBIOS") {
                error_page.show_error (
                    _("Missing Firmware"),
                    _("Install %s firmware to run the game").printf (game.platform.name),
                    "preferences-firmware-symbolic",
                    _("Install _Firmware"),
                    "game.install-firmware"
                );
            } else {
                DBusError.strip_remote_error (e);
                error_page.show_error (
                    _("Unable to run the game"),
                    _("Failed to start the game: %s").printf (e.message)
                );
            }

            set_page ("error");
            try {
                yield runner.stop ();
            } catch (Error e2) {
                DBusError.strip_remote_error (e2);
                critical ("Failed to stop core: %s", e2.message);
            }
            view.runner = null;
            this.runner = null;
            update_secondary_window ();
            return;
        }

        show_controller_notification ();

        runner.controller_type_changed.connect (() => {
            if (show_controller_notification_id > 0)
                Source.remove (show_controller_notification_id);

            show_controller_notification_id = Idle.add_once (() => {
                show_controller_notification ();
                show_controller_notification_id = 0;
            });
        });

        if (load_state) {
            try {
                if (!yield runner.load_snapshot (snapshot, false))
                    yield runner.reset (true);
            } catch (Error e) {
                DBusError.strip_remote_error (e);
                critical ("Failed to load snapshot: %s", e.message);

                add_toast (new Adw.Toast.format (
                    _("Failed to load snapshot: %s"), e.message)
                );

                try {
                    yield runner.reset (true);
                } catch (Error e) {
                    DBusError.strip_remote_error (e);
                    error_page.show_error (
                        _("Failed to reset the game"),
                        e.message
                    );
                    set_page ("error");
                    try {
                        yield runner.stop ();
                    } catch (Error e2) {
                        DBusError.strip_remote_error (e2);
                        critical ("Failed to stop core: %s", e2.message);
                    }
                    view.runner = null;
                    this.runner = null;
                    update_secondary_window ();

                    return;
                }
            }
        }

        try {
            if (settings.get_boolean ("pause-when-inactive") && !get_is_active ())
                yield runner.pause ();
        } catch (Error e) {
            DBusError.strip_remote_error (e);
            critical ("Failed to pause the game: %s", e.message);
        }

        platform_area.child = runner.create_header_widget (fullscreen_view);
        platform_area.visible = platform_area.child != null;

        runner.notify["paused"].connect (update_paused);
        runner.notify["view-name"].connect (update_subtitle);

        update_paused ();

        loading = false;

        inhibit (LOGOUT);

        update_autosave ();
        update_actions ();
    }

    private void update_active () {
        if (runner == null || !runner.running)
            return;

        if (get_is_active ())
            runner.resume.begin ();
        else
            runner.pause.begin ();
    }

    private bool get_is_active () {
        assert (get_root () is Adw.ApplicationWindow);

        if (snapshots_open || menu_revealer.reveal_child || controls_revealer.reveal_child)
            return false;

        if (!settings.get_boolean ("pause-when-inactive"))
            return true;

        var root = get_root () as Adw.ApplicationWindow;

        if (root.is_active && root.visible_dialog == null)
            return true;

        if (secondary_window != null &&
            secondary_window.is_active &&
            secondary_window.visible_dialog == null) {
            return true;
        }

        return false;
    }

    private string make_subtitle () {
        string view_name = runner.view_name;
        bool paused = runner.paused;

        if (paused && !snapshots_open && !menu_revealer.reveal_child) {
            if (view_name != null && view_name != "")
                return _("%s – Paused").printf (view_name);

            return _("Paused");
        }

        return view_name;
    }

    private void update_paused () {
        if (runner.paused)
            uninhibit (IDLE | SUSPEND);
        else
            inhibit (IDLE | SUSPEND);

        update_subtitle ();
        update_autosave ();
    }

    private void update_subtitle () {
        subtitle = make_subtitle ();
    }

    private void update_title () {
        if (subtitle == null || subtitle == "")
            window_title = title;
        else
            window_title = _("%s (%s)").printf (title, subtitle);
    }

    [GtkCallback]
    private void spinner_revealed_cb () {
        if (!loading_revealer.child_revealed)
            loading_revealer.visible = false;
    }

    [GtkCallback]
    private void menu_revealed_cb () {
        if (!menu_revealer.child_revealed)
            overlay_menu.reset ();
    }

    [GtkCallback]
    private void controls_revealed_cb () {
        if (!controls_revealer.child_revealed) {
            controls_revealer.child = null;
            controls_overlay = null;
        }
    }

    [GtkCallback]
    private void notification_revealed_cb () {
        if (!notification_revealer.child_revealed)
            notification_revealer.child = null;
    }

    private void inhibit (Gtk.ApplicationInhibitFlags flags) {
        set_inhibit_flags (inhibit_flags | flags);
    }

    private void uninhibit (Gtk.ApplicationInhibitFlags flags) {
        set_inhibit_flags (inhibit_flags & ~flags);
    }

    private void set_inhibit_flags (Gtk.ApplicationInhibitFlags new_flags) {
        if (new_flags == inhibit_flags)
            return;

        var window = get_root () as Gtk.Window;
        var app = window.application;

        uint new_cookie = app.inhibit (
            window, new_flags,
            /* Translators: This is displayed if the user tries to log out,
             * shutdown, or reboot while a game is running, %s is the game's
             * title */
            _("Playing %s").printf (game.title)
        );

        if (inhibit_cookie != 0)
            app.uninhibit (inhibit_cookie);

        inhibit_cookie = new_cookie;
        inhibit_flags = new_flags;
    }

    private void show_snapshots () {
        var snapshot_list = new SnapshotList (snapshot_manager, view, runner);

        bind_property ("compact", snapshot_list, "compact", SYNC_CREATE);

        snapshot_list.preview.connect (snapshot => {
            if (loading_snapshot)
                return;

            if (snapshot != null) {
                view.show_snapshot (snapshot);

                if (secondary_window != null)
                    secondary_window.show_snapshot (snapshot);
            } else {
                view.clear_snapshot (true);

                if (secondary_window != null)
                    secondary_window.clear_snapshot (true);
            }
        });
        snapshot_list.load.connect (snapshot => {
            load.begin (snapshot, snapshot_list);
            snapshot_manager.autoload_snapshot = snapshot;
        });

        snapshot_list.close.connect (() => snapshots_open = false);

        snapshot_list.unmap.connect (() => {
            if (unload_snapshots_idle_id > 0)
                Source.remove (unload_snapshots_idle_id);

            unload_snapshots_idle_id = Idle.add_once (() => {
                unload_snapshots_idle_id = 0;

                snapshot_list.deinit ();
                snapshots_bin.child = null;
            });
        });

        snapshots_bin.child = snapshot_list;
        snapshots_open = true;

        snapshot_list.grab_focus ();
    }

    private void show_controls (bool startup) {
        close_menu ();

        controls_overlay = new ControlsOverlay (runner, startup);
        controls_overlay.dismiss.connect (() => {
            controls_revealer.reveal_child = false;
            update_active ();
            view.grab_focus ();
        });

        controls_revealer.child = controls_overlay;
        controls_revealer.reveal_child = true;

        update_active ();
        controls_overlay.grab_focus ();
    }

    private void update_compact () {
        if (snapshots_open && unload_snapshots_idle_id > 0) {
            Source.remove (unload_snapshots_idle_id);
            unload_snapshots_idle_id = 0;
        }

        multi_layout_view.layout_name = compact ? "narrow" : "wide";
    }

    private void update_autosave () {
        bool enable_autosave = runner != null && runner.running &&
                               !snapshots_open && settings.get_boolean ("autosave");

        if (enable_autosave == (save_timeout_cb_id > 0))
            return;

        if (enable_autosave) {
            save_timeout_cb_id = Timeout.add (AUTOSAVE_DELAY_MS, () => {
                if (runner == null || !runner.running) {
                    save_timeout_cb_id = 0;
                    return Source.REMOVE;
                }

                auto_save.begin ();

                if (runner.paused) {
                    save_timeout_cb_id = 0;
                    return Source.REMOVE;
                }

                return Source.CONTINUE;
            });
        } else {
            stop_autosave ();
        }
    }

    private void stop_autosave () {
        if (save_timeout_cb_id > 0) {
            Source.remove (save_timeout_cb_id);
            save_timeout_cb_id = 0;
        }
    }

    private inline void reset_autosave () {
        stop_autosave ();
        update_autosave ();
    }

    private void close_menu () {
        menu_revealer.reveal_child = false;

        update_active ();

        view.grab_focus ();

        if (secondary_window != null)
            secondary_window.close_menu ();
    }

    [GtkCallback]
    private void menu_resume_cb () {
        close_menu ();
    }

    public OverlayMenu get_overlay_menu () {
        return overlay_menu;
    }

    private void show_controller_notification () {
        var manager = ControllerManager.get_instance ();

        if (manager.touch_enabled) {
            for (uint i = 0; i < runner.get_n_players (); i++) {
                if (manager.get_controller_type (i) == TOUCHSCREEN)
                    return;
            }
        }

        var controls = PlatformControls.create (game.platform);
        var controller_type = runner.get_controller_type (0);
        var controller = controls.get_controller (controller_type);

        if (last_controller_type != null) {
            var last_controller = controls.get_controller (last_controller_type);

            if (controller == last_controller)
                return;

            if (controller.similar_to == last_controller_type)
                return;

            if (last_controller.similar_to == controller_type)
                return;
        }

        var base_platform = game.platform;
        while (base_platform.parent != null)
            base_platform = base_platform.parent;

        var title = _("%s").printf (controller.name);
        var popup = new NotificationPopup (
            controller.icon_name ?? base_platform.icon_name,
            title,
            _("Click to View Controls")
        );
        popup.clicked.connect (() => {
            show_controls (true);
        });
        popup.dismissed.connect (() => {
            notification_revealer.reveal_child = false;
        });

        notification_revealer.child = popup;
        notification_revealer.reveal_child = true;

        popup.start_timer ();

        last_controller_type = controller_type;
    }
}
