// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameView : Adw.Bin, ScreenshotProvider {
    private const int CURSOR_HIDE_TIMEOUT = 1000;

    public Game game { get; construct; }
    public bool secondary { get; construct; }

    public bool uses_pointer { get; private set; }

    public int top_inset { get; set; }
    public int bottom_inset { get; set; }
    public int left_inset { get; set; }
    public int right_inset { get; set; }

    private Runner _runner;
    public Runner runner {
        get { return _runner; }
        set {
            if (runner != null) {
                runner.weak_unref (runner_weak_notify_cb);

                if (uses_pointer) {
                    disconnect_pointer ();
                    uses_pointer = false;
                }

                runner.disconnect (uses_pointer_id);
                uses_pointer_id = 0;

                runner.notify["touch-overlay-name"].disconnect (recreate_touch_overlay);
                runner.notify["loading"].disconnect (recreate_touch_overlay);
            }

            _runner = value;

            if (runner != null) {
                runner.weak_ref (runner_weak_notify_cb);

                if (runner.uses_pointer) {
                    connect_pointer ();
                    uses_pointer = true;
                }

                uses_pointer_id = runner.notify["uses-pointer"].connect (() => {
                    if (runner.uses_pointer)
                        connect_pointer ();
                    else
                        disconnect_pointer ();

                    uses_pointer = runner.uses_pointer;
                });

                runner.notify["touch-overlay-name"].connect (recreate_touch_overlay);
                runner.notify["loading"].connect (recreate_touch_overlay);
            }

            recreate_touch_overlay ();
        }
    }

    private Gtk.Overlay overlay;
    private GLDisplay display;
    private OverlayView? touch_overlay;

    private ulong uses_pointer_id;

    private Gtk.EventControllerMotion? motion_controller;
    private int? active_screen_id;
    private Runner.CursorType current_cursor;

    private uint cursor_timeout_id;

    private Gdk.EventSequence? first_sequence;
    private bool has_sequence;
    private bool pointer_pressed;

    public GameView (Game game, bool secondary) {
        Object (game: game, secondary: secondary);
    }

    construct {
        focusable = true;

        overflow = HIDDEN;

        display = new GLDisplay (game, secondary);
        bind_property ("runner", display, "runner", SYNC_CREATE);

        var offload = new Gtk.GraphicsOffload (display);
        // FIXME: offload.black_background = true; once vapi updates
        offload.@set ("black-background", true, null);

        overlay = new Gtk.Overlay ();
        overlay.child = offload;

        var indicator = new RumbleIndicator () {
            valign = END,
            halign = END,
        };

        bind_property ("runner",       indicator, "runner",        SYNC_CREATE);
        bind_property ("bottom-inset", indicator, "margin-bottom", SYNC_CREATE);
        overlay.add_overlay (indicator);

        child = overlay;

        var controller = new Gtk.EventControllerKey ();
        controller.key_pressed.connect ((keyval, keycode, state) => {
            if (runner == null)
                return Gdk.EVENT_PROPAGATE;

            return runner.key_pressed ((int) keycode - 8);
        });
        controller.key_released.connect ((keyval, keycode, state) => {
            if (runner == null)
                return;

            runner.key_released ((int) keycode - 8);
        });
        add_controller (controller);

        var focus_controller = new Gtk.EventControllerFocus ();
        focus_controller.leave.connect (() => {
            if (runner == null)
                return;

            runner.focus_leave ();
        });
        add_controller (focus_controller);

        motion_controller = new Gtk.EventControllerMotion ();
        motion_controller.enter.connect ((x, y) => {
            if (uses_pointer && active_screen_id == null) {
                var screen = display.find_screen (x, y);
                if (screen != null)
                    update_cursor (runner.get_cursor (screen));
                else
                    update_cursor (DEFAULT);
            }

            if (current_cursor == DEFAULT)
                cursor_moved ();
        });
        motion_controller.motion.connect ((x, y) => {
            if (uses_pointer && active_screen_id == null) {
                var screen = display.find_screen (x, y);
                if (screen != null)
                    update_cursor (runner.get_cursor (screen));
                else
                    update_cursor (DEFAULT);
            }

            if (current_cursor == DEFAULT)
                cursor_moved ();
        });
        motion_controller.leave.connect (() => {
            if (uses_pointer && active_screen_id == null) {
                update_cursor (DEFAULT);
            }

            if (current_cursor == DEFAULT)
                display.cursor = null;
        });

        add_controller (motion_controller);

        var legacy_controller = new Gtk.EventControllerLegacy ();
        legacy_controller.event.connect (event => {
            var type = event.get_event_type ();
            var sequence = event.get_event_sequence ();

            double x, y;

            if (event.get_position (out x, out y)) {
                var native = get_native ();

                double nx, ny;
                native.get_surface_transform (out nx, out ny);

                x -= nx;
                y -= ny;

                Graphene.Point point = {};
                var root = get_root () as Gtk.Widget;
                assert (root.compute_point (
                    this,
                    { (float) x, (float) y },
                    out point
                ));

                x = point.x;
                y = point.y;
            }

            if (type == BUTTON_PRESS || type == TOUCH_BEGIN) {
                if (!has_sequence) {
                    first_sequence = sequence;
                    has_sequence = true;
                }

                if (type == BUTTON_PRESS)
                    pointer_pressed = true;

                if (runner.uses_pointer && has_sequence && first_sequence == sequence) {
                    double screen_x, screen_y;
                    var screen = display.find_screen (x, y);

                    if (screen != null) {
                        display.transform_coords (
                            screen.id, x, y, out screen_x, out screen_y
                        );

                        if (runner.drag_begin (screen.id, screen_x, screen_y)) {
                            active_screen_id = screen.id;
                            update_cursor (runner.get_cursor (screen));

                            return Gdk.EVENT_STOP;
                        }
                    }
                }

                if (touch_overlay != null) {
                    Graphene.Point point = {};
                    assert (compute_point (
                        touch_overlay, { (float) x, (float) y }, out point
                    ));
                    touch_overlay.drag_begin (sequence, point.x, point.y);

                    return Gdk.EVENT_STOP;
                }

                return Gdk.EVENT_PROPAGATE;
            }

            if (type == MOTION_NOTIFY || type == TOUCH_UPDATE) {
                if (has_sequence &&
                    first_sequence == sequence &&
                    active_screen_id != null) {
                    double screen_x, screen_y;
                    display.transform_coords (
                        active_screen_id, x, y, out screen_x, out screen_y
                    );

                    runner.drag_update (screen_x, screen_y);

                    if (type == MOTION_NOTIFY)
                        return Gdk.EVENT_PROPAGATE;

                    return Gdk.EVENT_STOP;
                }

                if ((type == TOUCH_UPDATE || pointer_pressed) && touch_overlay != null) {
                    Graphene.Point point = {};
                    assert (compute_point (
                        touch_overlay, { (float) x, (float) y }, out point
                    ));
                    touch_overlay.drag_update (sequence, point.x, point.y);

                    if (type == MOTION_NOTIFY)
                        return Gdk.EVENT_PROPAGATE;

                    return Gdk.EVENT_STOP;
                }

                return Gdk.EVENT_PROPAGATE;
            }

            if (type == BUTTON_RELEASE || type == TOUCH_END) {
                if (type == BUTTON_RELEASE)
                    pointer_pressed = false;

                if (has_sequence && first_sequence == sequence) {
                    if (active_screen_id != null) {
                        var screen = display.find_screen (x, y);

                        runner.drag_end ();

                        if (screen == null || screen.id != active_screen_id)
                            update_cursor (DEFAULT);

                        active_screen_id = null;
                        has_sequence = false;

                        return Gdk.EVENT_STOP;
                    }

                    has_sequence = false;
                }

                if (touch_overlay != null) {
                    touch_overlay.drag_end (sequence);
                    return Gdk.EVENT_STOP;
                }

                return Gdk.EVENT_PROPAGATE;
            }

            if (type == TOUCH_CANCEL || type == GRAB_BROKEN) {
                if (has_sequence && first_sequence == sequence) {
                    if (active_screen_id != null) {
                        runner.drag_end ();
                        active_screen_id = null;
                        update_cursor (DEFAULT);
                        has_sequence = false;

                        return Gdk.EVENT_STOP;
                    }

                    has_sequence = false;
                }

                if (touch_overlay != null)
                    touch_overlay.drag_end (sequence);

                return Gdk.EVENT_PROPAGATE;
            }

            return Gdk.EVENT_PROPAGATE;
        });
        add_controller (legacy_controller);

        notify["top-inset"].connect (update_insets);
        notify["left-inset"].connect (update_insets);
        notify["right-inset"].connect (update_insets);
        notify["bottom-inset"].connect (update_insets);

        update_insets ();
    }

    ~GameView () {
        if (cursor_timeout_id > 0)
            Source.remove (cursor_timeout_id);

        runner = null;
    }

    static construct {
        set_css_name ("game-view");
    }

    private void recreate_touch_overlay () {
        if (runner == null)
            return;

        if (touch_overlay != null) {
            overlay.remove_overlay (touch_overlay);
            touch_overlay = null;
        }

        if (runner.touch_overlay_name == null) {
            update_insets ();
            return;
        }

        var register = OverlayRegister.get_instance ();
        var desc = register.get_overlay (runner.touch_overlay_name);

        if (desc == null)
            return;

        touch_overlay = new OverlayView (desc);

        bind_property ("top-inset", touch_overlay, "margin-top", SYNC_CREATE);
        bind_property ("bottom-inset", touch_overlay, "margin-bottom", SYNC_CREATE);

        touch_overlay.notify["top-inset"].connect (update_insets);
        touch_overlay.notify["bottom-inset"].connect (update_insets);
        touch_overlay.notify["left-inset"].connect (update_insets);
        touch_overlay.notify["right-inset"].connect (update_insets);

        touch_overlay.button_pressed.connect (element => {
            if (runner != null)
                runner.touch_button_pressed (element);
        });

        touch_overlay.button_released.connect (element => {
            if (runner != null)
                runner.touch_button_released (element);
        });

        touch_overlay.stick_moved.connect ((element, x, y) => {
            if (runner != null)
                runner.touch_stick_moved (element, x, y);
        });

        touch_overlay.clear_touch_controls.connect (() => {
            if (runner != null)
                runner.clear_touch_controls ();
        });

        overlay.add_overlay (touch_overlay);

        update_insets ();
    }

    private void update_insets () {
        if (touch_overlay != null) {
            display.top_inset    = top_inset +    (int) Math.ceil (touch_overlay.top_inset);
            display.left_inset   = left_inset +   (int) Math.ceil (touch_overlay.left_inset);
            display.right_inset  = right_inset +  (int) Math.ceil (touch_overlay.right_inset);
            display.bottom_inset = bottom_inset + (int) Math.ceil (touch_overlay.bottom_inset);
        } else {
            display.top_inset =    top_inset;
            display.left_inset =   left_inset;
            display.right_inset =  right_inset;
            display.bottom_inset = bottom_inset;
        }
    }

    private void runner_weak_notify_cb () {
        _runner = null;
        notify_property ("runner");
    }

    private void update_cursor (Runner.CursorType cursor) {
        if (current_cursor == cursor)
            return;

        current_cursor = cursor;

        switch (cursor) {
            case DEFAULT:
                display.cursor = null;
                break;
            case CROSSHAIR:
                display.cursor = new Gdk.Cursor.from_name ("crosshair", null);

                if (cursor_timeout_id > 0) {
                    Source.remove (cursor_timeout_id);
                    cursor_timeout_id = 0;
                }

                break;
            default:
                assert_not_reached ();
        }
    }

    private void cursor_moved () {
        if (current_cursor != DEFAULT)
            return;

        display.cursor = null;

        if (cursor_timeout_id > 0)
            Source.remove (cursor_timeout_id);

        cursor_timeout_id = Timeout.add_once (CURSOR_HIDE_TIMEOUT, () => {
            display.cursor = new Gdk.Cursor.from_name ("none", null);
            cursor_timeout_id = 0;
        });
    }

    private void connect_pointer () {
    }

    private void disconnect_pointer () {
        if (!uses_pointer)
            return;

        active_screen_id = null;
        update_cursor (DEFAULT);
    }

    public void show_snapshot (Snapshot snapshot) {
        display.show_snapshot (snapshot);
    }

    public void clear_snapshot (bool now) {
        display.clear_snapshot (now);
    }

    public Gdk.Texture? take_raw_screenshot () {
        return display.take_raw_screenshot ();
    }

    public Gdk.Texture? take_screenshot () {
        return display.take_screenshot ();
    }

    protected override bool focus (Gtk.DirectionType direction) {
        return true;
    }
}
