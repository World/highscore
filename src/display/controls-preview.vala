// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.ControlsPreviewPage : Adw.Bin {
    public Runner runner { get; construct; }
    public uint player { get; construct; }
    public string? controller_type { get; set; }

    private Gtk.Label title;
    private Gtk.Label subtitle;
    private Adw.Bin preview_bin;

    private Gamepad? gamepad;

    public ControlsPreviewPage (Runner runner, uint player) {
        Object (runner: runner, player: player);
    }

    construct {
        var box = new Gtk.Box (VERTICAL, 6);

        child = box;

        var title_str = _("Player %u").printf (player + 1);
        title = new Gtk.Label (title_str) {
            ellipsize = MIDDLE,
            // spacing + button width + margin
            margin_start = 12 + 66 + 12,
            margin_end = 12 + 66 + 12,
        };
        title.add_css_class ("title-1");
        title.add_css_class ("numeric");
        box.append (title);

        subtitle = new Gtk.Label (title_str) {
            ellipsize = MIDDLE,
            margin_start = 12,
            margin_end = 12,
        };
        subtitle.add_css_class ("title-4");
        box.append (subtitle);

        preview_bin = new Adw.Bin () {
            vexpand = true,
            margin_top = 12,
        };
        box.append (preview_bin);
    }

    static construct {
        set_css_name ("controls-preview-page");
    }

    ~ControlsPreviewPage () {
        clear_gamepad ();
    }

    private void clear_gamepad () {
        if (gamepad != null) {
            gamepad.notify["name"].disconnect (notify_gamepad_name_cb);
            gamepad = null;
        }
    }

    private void notify_gamepad_name_cb () {
        subtitle.label = gamepad.name;
    }

    public void show_gamepad (Gamepad gamepad) {
        if (this.gamepad == gamepad)
            return;

        clear_gamepad ();
        this.gamepad = gamepad;

        var preview = new GamepadMappingPreview (runner.game, gamepad);
        bind_property ("controller-type", preview, "controller-type", SYNC_CREATE);

        subtitle.label = gamepad.name;

        if (gamepad != null)
            gamepad.notify["name"].connect (notify_gamepad_name_cb);

        preview_bin.child = preview;
    }

    public void show_keyboard () {
        clear_gamepad ();

        var preview = new KeyboardMappingPreview (runner.game);
        bind_property ("controller-type", preview, "controller-type", SYNC_CREATE);

        subtitle.label = _("Keyboard");

        preview_bin.child = preview;
    }

    public void show_touch () {
        clear_gamepad ();

        var page = new Adw.StatusPage () {
            title = _("Touchscreen"),
            icon_name = "touchscreen-symbolic",
        };

        subtitle.label = null;
        preview_bin.child = page;
    }

    public void clear () {
        clear_gamepad ();

        var page = new Adw.StatusPage () {
            title = _("No Controller"),
            icon_name = "controller-missing-symbolic",
        };

        subtitle.label = null;
        preview_bin.child = page;
    }
}

[GtkTemplate (ui = "/app/drey/Highscore/display/controls-preview.ui")]
public class Highscore.ControlsPreview : Adw.Bin {
    public GamepadController controller { get; construct; }
    public Runner runner { get; construct; }

    [GtkChild]
    private unowned Gtk.Label buttons_placeholder_label;
    [GtkChild]
    private unowned Adw.ViewStack stack;
    [GtkChild]
    private unowned Gtk.Revealer prev_revealer;
    [GtkChild]
    private unowned Gtk.Revealer next_revealer;

    private uint n_players;
    private ControlsPreviewPage[] previews;

    public ControlsPreview (GamepadController controller, Runner runner) {
        Object (controller: controller, runner: runner);
    }

    construct {
        n_players = runner.get_n_players ();

        previews = {};

        buttons_placeholder_label.label = _("Player %u").printf (n_players);

        for (uint i = 0; i < n_players; i++) {
            var preview = new ControlsPreviewPage (runner, i);
            preview.controller_type = runner.get_controller_type (i);

            var page = stack.add (preview);
            page.name = @"player-$(i + 1)";
            page.title = _("Player %u").printf (i + 1);

            if (i > 0)
                preview.add_css_class ("slide-right");

            previews += preview;
        }

        var manager = ControllerManager.get_instance ();

        for (uint i = 0; i < n_players; i++)
            set_page_for_player (i);

        sync_actions ();

        manager.controller_changed.connect (controller_changed_cb);
        runner.controller_type_changed.connect (controller_type_changed_cb);
    }

    ~ControlsPreview () {
        var manager = ControllerManager.get_instance ();

        manager.controller_changed.disconnect (controller_changed_cb);
        runner.controller_type_changed.disconnect (controller_type_changed_cb);
    }

    static construct {
        set_css_name ("controls-preview");
    }

    public void prev_player () {
        int current_player = 0;

        for (int i = 0; i < n_players; i++) {
            if (previews[i] == stack.visible_child) {
                current_player = i;
                break;
            }
        }

        if (current_player <= 0)
            return;

        stack.visible_child = previews[current_player - 1];
    }

    public void next_player () {
        int current_player = 0;

        for (int i = 0; i < n_players; i++) {
            if (previews[i] == stack.visible_child) {
                current_player = i;
                break;
            }
        }

        if (current_player >= n_players - 1)
            return;

        stack.visible_child = previews[current_player + 1];
    }

    private void controller_changed_cb (uint player) {
        if (player >= n_players)
            return;

        set_page_for_player (player);
        sync_actions ();
    }

    private void controller_type_changed_cb (uint player) {
        if (player >= n_players)
            return;

        var preview = previews[player];

        preview.controller_type = runner.get_controller_type (player);
    }

    private void set_page_for_player (uint player) {
        assert (player < n_players);

        var manager = ControllerManager.get_instance ();

        var preview = previews[player];

        switch (manager.get_controller_type (player)) {
            case GAMEPAD:
                var gamepad = manager.get_gamepad (player);
                preview.show_gamepad (gamepad);
                stack.get_page (preview).visible = true;
                break;
            case KEYBOARD:
                preview.show_keyboard ();
                stack.get_page (preview).visible = true;
                break;
            case TOUCHSCREEN:
                preview.show_touch ();
                stack.get_page (preview).visible = true;
                break;
            case NONE:
                preview.clear ();
                stack.get_page (preview).visible = false;
                if (stack.visible_child == preview)
                    stack.visible_child = previews[0];
                break;
            default:
                assert_not_reached ();
        }
    }

    private void sync_actions () {
        uint current_player = 0;

        for (uint i = 0; i < n_players; i++) {
            if (previews[i] == stack.visible_child) {
                current_player = i;
                break;
            }
        }

        prev_revealer.reveal_child = current_player > 0;
        next_revealer.reveal_child = current_player < n_players - 1;
    }

    [GtkCallback]
    private void visible_child_cb () {
        if (previews.length < n_players)
            return;

        uint new_player = 0;

        for (uint i = 0; i < n_players; i++) {
            if (previews[i] == stack.visible_child) {
                new_player = i;
                break;
            }
        }

        for (uint i = 0; i < n_players; i++) {
            if (i < new_player) {
                previews[i].add_css_class ("slide-left");
                previews[i].remove_css_class ("slide-right");
            } else if (i > new_player) {
                previews[i].add_css_class ("slide-right");
                previews[i].remove_css_class ("slide-left");
            }
        }

        stack.visible_child.remove_css_class ("slide-left");
        stack.visible_child.remove_css_class ("slide-right");

        sync_actions ();
    }

    [GtkCallback]
    private void prev_player_clicked_cb () {
        prev_player ();
    }

    [GtkCallback]
    private void next_player_clicked_cb () {
        next_player ();
    }
}
