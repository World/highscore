// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/display/notification-popup.ui")]
public class Highscore.NotificationPopup : Adw.Bin {
    private const int TIMEOUT_MS = 3000;

    public signal void clicked ();
    public signal void dismissed ();

    public string icon_name { get; set; }
    public string title { get; set; }
    public string description { get; set; }

    private uint timeout_id;
    private bool hiding;

    public NotificationPopup (string icon_name, string title, string description) {
        Object (icon_name: icon_name, title: title, description: description);
    }

    static construct {
        set_css_name ("notification-popup");
    }

    ~NotificationPopup () {
        clear_timer ();
    }

    protected override void unmap () {
        clear_timer ();

        base.unmap ();
    }

    public void start_timer () {
        clear_timer ();

        timeout_id = Timeout.add_once (TIMEOUT_MS, () => {
            timeout_id = 0;
            hiding = true;

            dismissed ();
        });
    }

    public void clear_timer () {
        if (timeout_id > 0) {
            Source.remove (timeout_id);
            timeout_id = 0;
        }
    }

    [GtkCallback]
    private void clicked_cb () {
        clear_timer ();
        hiding = true;

        clicked ();
        dismissed ();
    }

    [GtkCallback]
    private void enter_cb (double x, double y) {
        clear_timer ();
    }

    [GtkCallback]
    private void motion_cb (double x, double y) {
        clear_timer ();
    }

    [GtkCallback]
    private void leave_cb () {
        if (!hiding)
            start_timer ();
    }
}
