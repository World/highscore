// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.FullscreenView : Adw.Bin, Gtk.Buildable {
    private const int FULLSCREEN_HIDE_DELAY = 300;
    private const int SHOW_HEADERBAR_DISTANCE_PX = 5;

    private Adw.ToolbarView toolbar_view;
    private Gtk.Overlay overlay;
    private Gtk.Revealer revealer;

    private uint timeout_id;

    private Gtk.Widget last_focus;
    private double last_y;
    private bool is_touch;

    private Gtk.Widget[] headers;
    private Gtk.MenuButton[] tracked_menu_buttons;

    public Gtk.Widget content {
        get { return overlay.child; }
        set { overlay.child = value; }
    }

    private bool _fullscreen;
    public bool fullscreen {
        get { return _fullscreen; }
        set {
            if (fullscreen == value)
                return;

            _fullscreen = value;

            update_style ();

            if (!autohide)
                return;

            if (fullscreen)
                update (false);
            else
                show_ui ();
        }
    }

    private bool _autohide;
    public bool autohide {
        get { return _autohide; }
        set {
            if (autohide == value)
                return;

            _autohide = value;

            update_style ();

            if (!fullscreen)
                return;

            if (autohide)
                hide_ui ();
            else
                show_ui ();
        }
    }

    public bool revealed {
        get { return toolbar_view.reveal_top_bars; }
    }

    public int top_bar_height {
        get { return toolbar_view.top_bar_height; }
    }

    construct {
        last_y = double.MAX;

        var scrim = new Adw.Bin ();
        scrim.add_css_class ("scrim");

        revealer = new Gtk.Revealer () {
            transition_type = CROSSFADE,
            valign = START,
            can_target = false,
            can_focus = false,
            child = scrim,
        };

        overlay = new Gtk.Overlay ();
        overlay.add_overlay (revealer);

        toolbar_view = new Adw.ToolbarView () {
            top_bar_style = RAISED_BORDER,
            bottom_bar_style = RAISED_BORDER,
            extend_content_to_top_edge = true,
            extend_content_to_bottom_edge = true,
            content = overlay,
        };

        toolbar_view.notify["reveal-top-bars"].connect (() => {
            notify_property ("revealed");
        });
        toolbar_view.notify["top-bar-height"].connect (() => {
            notify_property ("top-bar-height");
        });

        toolbar_view.bind_property (
            "reveal-top-bars", revealer, "reveal-child", SYNC_CREATE
        );

        child = toolbar_view;

        var controller = new Gtk.EventControllerMotion () {
            propagation_phase = CAPTURE,
        };
        controller.enter.connect ((x, y) => {
            is_touch = false;
            last_y = y;

            update (true);
        });
        controller.motion.connect ((x, y) => {
            is_touch = false;
            last_y = y;

            update (true);
        });

        add_controller (controller);

        var gesture = new Gtk.GestureClick () {
            propagation_phase = CAPTURE,
            touch_only = true,
        };
        gesture.pressed.connect ((n_press, x, y) => {
            gesture.set_state (DENIED);

            is_touch = true;

            if (y > get_titlebar_area_height ())
                update (true);
        });

        add_controller (gesture);

        headers = {};
        tracked_menu_buttons = {};

        add_css_class ("background");

        autohide = true;
        update_style ();
    }

    static construct {
        set_css_name ("fullscreen-view");
    }

    protected void add_child (Gtk.Builder builder, Object child, string? type) {
        if (type == "top" && child is Gtk.Widget)
            add_top_bar (child as Gtk.Widget);
        else
            base.add_child (builder, child, type);
    }

    protected override void dispose () {
        headers = {};

        base.dispose ();
    }

    private void notify_focus_widget_cb () {
        set_last_focus (root.get_focus ());
    }

    protected override void realize () {
        base.realize ();

        if (root != null && root is Gtk.Window) {
            root.notify["focus-widget"].connect (notify_focus_widget_cb);

            set_last_focus (root.get_focus ());
        } else {
            set_last_focus (null);
        }
    }

    protected override void unrealize () {
        if (root != null && root is Gtk.Window)
            root.notify["focus-widget"].disconnect (notify_focus_widget_cb);

            set_last_focus (null);

        base.unrealize ();
    }

    private void show_ui () {
        if (timeout_id != 0) {
            Source.remove (timeout_id);
            timeout_id = 0;
        }

        toolbar_view.reveal_top_bars = true;
        toolbar_view.reveal_bottom_bars = true;
    }

    private void hide_ui () {
        if (timeout_id != 0) {
            Source.remove (timeout_id);
            timeout_id = 0;
        }

        if (!fullscreen)
            return;

        toolbar_view.reveal_top_bars = false;
        toolbar_view.reveal_bottom_bars = false;

        toolbar_view.grab_focus ();
    }

    private void start_hide_timeout () {
        if (toolbar_view.reveal_top_bars)
            return;

        if (timeout_id > 0)
            return;

        timeout_id = Timeout.add_once (FULLSCREEN_HIDE_DELAY, () => {
            timeout_id = 0;

            hide_ui ();
        });
    }

    private bool is_descendant_of (Gtk.Widget? widget, Gtk.Widget target) {
        if (widget == null)
            return false;

        if (widget == target)
            return true;

        var parent = widget;

        while (parent != null && parent != target)
            parent = parent.parent;

        return parent == target;
    }

    private double get_titlebar_area_height () {
        double height = toolbar_view.top_bar_height;

        return double.max (height, SHOW_HEADERBAR_DISTANCE_PX);
    }

    private bool is_focusing_titlebar () {
        foreach (var header in headers) {
            if (is_descendant_of (last_focus, header))
                return true;
        }

        foreach (var button in tracked_menu_buttons) {
            if (button.active)
                return true;
        }

        return false;
    }

    private void update (bool hide_immediately) {
        if (!autohide || !fullscreen)
            return;

        if (!is_touch && last_y <= get_titlebar_area_height ()) {
            show_ui ();
            return;
        }

        if (last_focus != null && is_focusing_titlebar ())
            show_ui ();
        else if (hide_immediately)
            hide_ui ();
        else
            start_hide_timeout ();
    }

    private void set_last_focus (Gtk.Widget? widget) {
        last_focus = widget;

        update (true);
    }

    private void update_style () {
        if (autohide && fullscreen) {
            revealer.visible = true;
            toolbar_view.top_bar_style = FLAT;
            toolbar_view.bottom_bar_style = FLAT;
        } else {
            revealer.visible = false;
            toolbar_view.top_bar_style = RAISED_BORDER;
            toolbar_view.bottom_bar_style = RAISED_BORDER;
        }
    }

    public void add_top_bar (Gtk.Widget widget) {
        toolbar_view.add_top_bar (widget);

        headers += widget;
    }

    public void track_menu_button (Gtk.MenuButton button) {
        tracked_menu_buttons += button;

        button.notify["active"].connect (() => update (true));
    }
}
