#ifndef CORE
#  define out varying

#  ifdef VERTEX
#    define in attribute
#  else
#    define in varying
#  endif

#  define texture texture2D
#endif

#ifndef LEGACY
precision mediump float;
#endif

#ifdef VERTEX

in vec2 position;
in vec2 texCoord;

out vec2 v_texCoord;

void main() {
  v_texCoord = texCoord;

  gl_Position = vec4(position * 2.0 - 1.0, 0.0, 1.0);
}

#else // FRAGMENT

in vec2 v_texCoord;

#ifdef CORE
out vec4 outputColor;
#endif

uniform sampler2D u_source;

void main() {
#ifdef CORE
  outputColor = texture(u_source, v_texCoord);
#else
  gl_FragColor = texture(u_source, v_texCoord);
#endif
}

#endif