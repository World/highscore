// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/standalone-window.ui")]
public class Highscore.StandaloneWindow : Adw.ApplicationWindow, RunnerWindow {
    public signal void error_clicked ();

    [GtkChild]
    private unowned Adw.ToastOverlay toast_overlay;
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned Gtk.Stack loading_stack;
    [GtkChild]
    private unowned Gtk.Revealer loading_revealer;
    [GtkChild]
    private unowned ErrorPage error_page;

    private DisplayPage? display_page;

    public Runner? runner {
        get {
            if (display_page != null)
                return display_page.runner;

            return null;
        }
    }

    public StandaloneWindow (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        var settings = new Settings ("app.drey.Highscore.state");

        settings.bind ("standalone-window-maximized", this, "maximized",      GET | SET | GET_NO_CHANGES);
        settings.bind ("standalone-window-width",     this, "default-width",  GET | SET | GET_NO_CHANGES);
        settings.bind ("standalone-window-height",    this, "default-height", GET | SET | GET_NO_CHANGES);

        fullscreened = settings.get_boolean ("standalone-window-fullscreen");

        action_set_enabled ("game.add-to-library", false);

        var detector = TouchDetector.get_instance ();
        if (detector != null)
            detector.register (this);
    }

    static construct {
        install_action ("game.add-to-library", null, widget => {
            var self = widget as StandaloneWindow;

            self.add_to_library.begin ();
        });

        install_action ("window.error-clicked", null, widget => {
            var self = widget as StandaloneWindow;

            self.error_clicked ();
        });

        add_binding_action (Gdk.Key.W, CONTROL_MASK, "window.close", null);
    }

    public void show_error (string message) {
        show_error_with_button (message, null);
    }

    public void show_error_with_button (string message, string? button_label) {
        if (button_label != null) {
            error_page.show_error (
                _("Unable to run the game"),
                message,
                "dialog-error-symbolic",
                button_label,
                "window.error-clicked"
            );
        } else {
            error_page.show_error (_("Unable to run the game"), message);
        }

        stack.visible_child_name = "error";
        fullscreened = false;
    }

    public void reset () {
        stack.visible_child_name = "game";
    }

    public async void run_game (Game game) requires (display_page == null) {
        if (LibraryUtils.has_library_dir ()) {
            var library = Library.get_instance ();
            action_set_enabled ("game.add-to-library", !library.has_game (game));
        }

        display_page = new DisplayPage (game, true);
        display_page.add_toast.connect (add_toast);

        display_page.bind_property ("window-title", this, "title", SYNC_CREATE);

        display_page.notify["loading"].connect (() => {
            if (!display_page.loading)
                loading_revealer.reveal_child = false;
        });

        display_page.error.connect (() => {
            loading_revealer.reveal_child = false;
        });

        display_page.start_game.begin ();

        loading_stack.add_child (display_page);

        if (get_realized ())
            Idle.add_once (present_display_page);
        else
            present_display_page ();
    }

    private void present_display_page () {
        loading_stack.visible_child = display_page;
        display_page.showing ();
        display_page.shown ();
    }

    protected override bool close_request () {
        if (display_page != null) {
            display_page.save_and_close.begin (() => {
                display_page.hiding ();
                display_page.hidden ();
                display_page = null;
                close ();
            });
            return Gdk.EVENT_STOP;
        }

        return Gdk.EVENT_PROPAGATE;
    }

    private async void add_to_library () {
        var library = Library.get_instance ();

        try {
            yield library.import_game (runner.game);
        } catch (Error e) {
            toast_overlay.add_toast (
                new Adw.Toast.format (_("Failed to add game: %s"), e.message)
            );
            return;
        }

        action_set_enabled ("game.add-to-library", !library.has_game (runner.game));

        toast_overlay.add_toast (new Adw.Toast (_("Game Added")));
    }

    [GtkCallback]
    private void spinner_revealed_cb () {
        if (!loading_revealer.child_revealed)
            loading_revealer.visible = false;
    }

    private void add_toast (Adw.Toast toast) {
        toast_overlay.add_toast (toast);
    }
}
