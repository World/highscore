// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "org.sigxcpu.Feedback.Haptic")]
public interface Highscore.FeedbackdHapticProxy : DBusProxy {
    public abstract async bool vibrate (
        string appid,
        [DBus (signature = "sa(du)")] Variant pattern
    ) throws Error;
}

public class Highscore.FeedbackdRumble : Object {
    private static FeedbackdRumble instance;

    public bool available { get; private set; }

    private FeedbackdHapticProxy proxy;

    private FeedbackdRumble () {}

    public async void start () {
        try {
            proxy = yield Bus.get_proxy<FeedbackdHapticProxy> (
                SESSION, "org.sigxcpu.Feedback", "/org/sigxcpu/Feedback"
            );

            yield try_rumble (0, 0);

            available = true;
        } catch (Error e) {
            available = false;
            proxy = null;
        }
    }

    public async bool try_rumble (double magnitude, uint32 duration) throws Error {
        var child_type = new VariantType.tuple ({
            VariantType.DOUBLE, VariantType.UINT32
        });
        Variant[] children = {};

        if (duration > 0)
            children += new Variant ("(du)", magnitude, duration);

        var pattern = new Variant.array (child_type, children);

        return yield proxy.vibrate (Config.APPLICATION_ID, pattern);
    }

    public async bool rumble (double magnitude, uint32 duration) {
        try {
            return yield try_rumble (magnitude, duration);
        } catch (Error e) {
            warning ("Failed to rumble: %s", e.message);
            return false;
        }
    }

    public static FeedbackdRumble get_instance () {
        if (instance == null)
            instance = new FeedbackdRumble ();

        return instance;
    }
}
