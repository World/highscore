// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.ButtonHandler : ControlHandler {
    private const double STICK_DEADZONE = 0.5;
    private const double DIAGONAL_AREA = Math.PI / 6;

    private struct StickData {
        bool allowed_directions[InputDirection.N_DIRECTIONS];
        double x;
        double y;
        bool active;

        public bool is_direction_allowed (InputDirection? direction) {
            if (direction == null)
                return false;

            return allowed_directions[(InputDirection) direction];
        }
    }

    private Gee.HashSet<int>? pressed_keys;
    private Gee.HashSet<Gamepad.Button>? pressed_buttons;
    private StickData? sticks[Gamepad.N_STICKS];
    private GenericSet<string>? pressed_touch;

    public ButtonHandler (Runner runner, uint player, Control control) {
        this.runner = runner;
        this.player = player;
        this.control = control;
    }

    public override bool add_key (int keycode, string? qualifier) {
        if (qualifier != null)
            return false;

        if (pressed_keys == null)
            pressed_keys = new Gee.HashSet<int> ();

        return true;
    }

    public override bool add_button (Gamepad.Button button, string? qualifier) {
        if (qualifier != null)
            return false;

        if (pressed_buttons == null) {
            pressed_buttons = new Gee.HashSet<Gamepad.Button> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
        }

        return true;
    }

    public override bool add_stick (Gamepad.Stick stick, InputDirection? direction, string? qualifier) {
        assert (direction != null);

        if (sticks[stick] == null)
            sticks[stick] = {};

        InputDirection dir = direction;
        sticks[stick].allowed_directions[dir] = true;

        return true;
    }

    public override bool add_pointer (int screen_id) {
        return false;
    }

    public override bool add_touch (string element, Control.ControlType control_type, string? qualifier) {
        if (qualifier != null)
            return false;

        if (pressed_touch == null)
            pressed_touch = new GenericSet<string> (str_hash, str_equal);

        return true;
    }

    public override void key_pressed (int keycode) {
        if (pressed_keys == null)
            return;

        bool was_active = is_active ();

        pressed_keys.add (keycode);

        if (!was_active)
            notify_control ();
    }

    public override void key_released (int keycode) {
        if (pressed_keys == null)
            return;

        pressed_keys.remove (keycode);

        if (!is_active ())
            notify_control ();
    }

    public override void button_pressed (Gamepad.Button button) {
        if (pressed_buttons == null)
            return;

        bool was_active = is_active ();

        pressed_buttons.add (button);

        if (!was_active)
            notify_control ();
    }

    public override void button_released (Gamepad.Button button) {
        if (pressed_buttons == null)
            return;

        pressed_buttons.remove (button);

        if (!is_active ())
            notify_control ();
    }

    public override void stick_moved (Gamepad.Stick stick, double x, double y) {
        if (sticks[stick] == null)
            return;

        sticks[stick].x = x;
        sticks[stick].y = y;

        double length = Math.sqrt (x * x + y * y);
        double angle = Math.atan2 (y, x);

        bool was_active = sticks[stick].active;
        bool active = false;

        if (length > STICK_DEADZONE) {
            for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
                if (AnalogUtils.is_pointing_at_direction (angle, DIAGONAL_AREA, i) &&
                    sticks[stick].is_direction_allowed (i)) {
                    active = true;
                    break;
                }
            }
        }

        if (was_active == active)
            return;

        sticks[stick].active = active;

        notify_control ();
    }

    public override void drag_begin (double x, double y) {}
    public override void drag_update (double x, double y) {}
    public override void drag_end () {}

    public override void touch_button_pressed (string element, string? qualifier) {
        if (pressed_touch == null || qualifier != null)
            return;

        bool was_active = is_active ();

        pressed_touch.add (element);

        if (!was_active)
            notify_control ();
    }

    public override void touch_button_released (string element, string? qualifier) {
        if (pressed_touch == null || qualifier != null)
            return;

        bool was_active = is_active ();

        pressed_touch.remove (element);

        if (was_active)
            notify_control ();
    }

    public override void touch_stick_moved (string element, double x, double y) {
    }

    public override void touch_clear (string element) {
        if (pressed_touch != null)
            touch_button_released (element, null);
    }

    private bool is_active () {
        for (int i = 0; i < Gamepad.N_STICKS; i++) {
            if (sticks[i] == null)
                continue;

            if (sticks[i].active)
                return true;
        }

        return (pressed_keys != null && pressed_keys.size > 0) ||
               (pressed_buttons != null && pressed_buttons.size > 0) ||
               (pressed_touch != null && pressed_touch.length > 0);
    }

    private void notify_control () {
        var btn = control as ButtonControl;
        btn.activate (runner, player, is_active ());
    }
}
