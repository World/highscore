// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PointerHandler : ControlHandler {
    private int screen_id;

    public PointerHandler (Runner runner, uint player, Control control) {
        this.runner = runner;
        this.player = player;
        this.control = control;

        screen_id = -1;
    }

    public override bool add_key (int keycode, string? qualifier) {
        return false;
    }

    public override bool add_button (Gamepad.Button button, string? qualifier) {
        return false;
    }

    public override bool add_stick (Gamepad.Stick stick, InputDirection? direction, string? qualifier) {
        return false;
    }

    public override bool add_pointer (int screen_id) {
        if (screen_id >= 0)
            return false;

        this.screen_id = screen_id;
        return true;
    }

    public override bool add_touch (string element, Control.ControlType control_type, string? qualifier) {
        return false;
    }

    public override void key_pressed (int keycode) {}
    public override void key_released (int keycode) {}
    public override void button_pressed (Gamepad.Button button) {}
    public override void button_released (Gamepad.Button button) {}
    public override void stick_moved (Gamepad.Stick stick, double x, double y) {}

    public override void drag_begin (double x, double y) {
        var pointer = control as PointerControl;
        pointer.pressed (runner, player, x, y);
    }

    public override void drag_update (double x, double y) {
        var pointer = control as PointerControl;
        pointer.moved (runner, player, x, y);
    }

    public override void drag_end () {
        var pointer = control as PointerControl;
        pointer.released (runner, player);
    }

    public override void touch_button_pressed (string element, string? qualifier) {}
    public override void touch_button_released (string element, string? qualifier) {}
    public override void touch_stick_moved (string element, double x, double y) {}
    public override void touch_clear (string element) {}
}
