// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.DpadHandler : ControlHandler {
    private const double STICK_DEADZONE = 0.5;
    private const double DIAGONAL_AREA = Math.PI / 5;

    private struct StickData {
        bool active_directions[InputDirection.N_DIRECTIONS];
    }

    private Gee.HashSet<int>? up_keys;
    private Gee.HashSet<int>? down_keys;
    private Gee.HashSet<int>? left_keys;
    private Gee.HashSet<int>? right_keys;
    private HashTable<int, InputDirection> key_directions;

    private Gee.HashSet<Gamepad.Button>? up_buttons;
    private Gee.HashSet<Gamepad.Button>? down_buttons;
    private Gee.HashSet<Gamepad.Button>? left_buttons;
    private Gee.HashSet<Gamepad.Button>? right_buttons;
    private HashTable<Gamepad.Button, InputDirection> button_directions;
    private StickData? sticks[Gamepad.N_STICKS];

    private GenericSet<string>? touch_up;
    private GenericSet<string>? touch_down;
    private GenericSet<string>? touch_left;
    private GenericSet<string>? touch_right;
    private HashTable<string, InputDirection> touch_directions;
    private HashTable<string, StickData?> touch_sticks;

    private bool last_active[InputDirection.N_DIRECTIONS];

    public DpadHandler (Runner runner, uint player, Control control) {
        this.runner = runner;
        this.player = player;
        this.control = control;
    }

    private Gee.HashSet<int>? get_keys_for_direction (InputDirection direction) {
        switch (direction) {
            case UP:
                return up_keys;
            case DOWN:
                return down_keys;
            case LEFT:
                return left_keys;
            case RIGHT:
                return right_keys;
            default:
                assert_not_reached ();
        }
    }

    private Gee.HashSet<Gamepad.Button>? get_buttons_for_direction (InputDirection direction) {
        switch (direction) {
            case UP:
                return up_buttons;
            case DOWN:
                return down_buttons;
            case LEFT:
                return left_buttons;
            case RIGHT:
                return right_buttons;
            default:
                assert_not_reached ();
        }
    }

    private GenericSet<string>? get_touch_for_direction (InputDirection direction) {
        switch (direction) {
            case UP:
                return touch_up;
            case DOWN:
                return touch_down;
            case LEFT:
                return touch_left;
            case RIGHT:
                return touch_right;
            default:
                assert_not_reached ();
        }
    }

    public override bool add_key (int keycode, string? qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return false;

        if (up_keys == null) {
            up_keys = new Gee.HashSet<int> ();
            down_keys = new Gee.HashSet<int> ();
            left_keys = new Gee.HashSet<int> ();
            right_keys = new Gee.HashSet<int> ();
            key_directions = new HashTable<int, InputDirection> (direct_hash, direct_equal);
        }

        key_directions[keycode] = direction;
        return true;
    }

    public override bool add_button (Gamepad.Button button, string? qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return false;

        if (up_buttons == null) {
            up_buttons = new Gee.HashSet<Gamepad.Button> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
            down_buttons = new Gee.HashSet<Gamepad.Button> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
            left_buttons = new Gee.HashSet<Gamepad.Button> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
            right_buttons = new Gee.HashSet<Gamepad.Button> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
            button_directions = new HashTable<Gamepad.Button, InputDirection> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
        }

        button_directions[button] = direction;
        return true;
    }

    public override bool add_stick (Gamepad.Stick stick, InputDirection? direction, string? qualifier) {
        assert (direction == null);

        if (sticks[stick] == null)
            sticks[stick] = {};

        return true;
    }

    public override bool add_pointer (int screen_id) {
        return false;
    }

    public override bool add_touch (string element, Control.ControlType control_type, string? qualifier) {
        if (control_type == DPAD || control_type == BUTTON) {
            if (touch_up == null) {
                touch_up = new GenericSet<string> (str_hash, str_equal);
                touch_down = new GenericSet<string> (str_hash, str_equal);
                touch_left = new GenericSet<string> (str_hash, str_equal);
                touch_right = new GenericSet<string> (str_hash, str_equal);
                touch_directions = new HashTable<string, InputDirection> (
                    str_hash, str_equal
                );
            }

            if (control_type == BUTTON) {
                var direction = InputDirection.from_string (qualifier);
                if (direction == null)
                    return false;

                touch_directions[element] = direction;
            }

            return true;
        }

        if (control_type == STICK) {
            if (qualifier != null)
                return false;

            if (touch_sticks == null)
                touch_sticks = new HashTable<string, StickData?> (str_hash, str_equal);

            touch_sticks[element] = {};

            return true;
        }

        return false;
    }

    public override void key_pressed (int keycode) {
        if (!(keycode in key_directions))
            return;

        var direction = key_directions[keycode];
        bool was_active = is_active (direction);
        var keys = get_keys_for_direction (direction);

        keys.add (keycode);

        if (!was_active)
            notify_control ();
    }

    public override void key_released (int keycode) {
        if (!(keycode in key_directions))
            return;

        var direction = key_directions[keycode];
        var keys = get_keys_for_direction (direction);

        keys.remove (keycode);

        if (!is_active (direction))
            notify_control ();
    }

    public override void button_pressed (Gamepad.Button button) {
        if (!(button in button_directions))
            return;

        var direction = button_directions[button];
        bool was_active = is_active (direction);
        var buttons = get_buttons_for_direction (direction);

        buttons.add (button);

        if (!was_active)
            notify_control ();
    }

    public override void button_released (Gamepad.Button button) {
        if (!(button in button_directions))
            return;

        var direction = button_directions[button];
        var buttons = get_buttons_for_direction (direction);

        buttons.remove (button);

        if (!is_active (direction))
            notify_control ();
    }

    public override void drag_begin (double x, double y) {}
    public override void drag_update (double x, double y) {}
    public override void drag_end () {}

    public override void stick_moved (Gamepad.Stick stick, double x, double y) {
        double length = Math.sqrt (x * x + y * y);
        double angle = Math.atan2 (y, x);

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            bool active = length > STICK_DEADZONE &&
                AnalogUtils.is_pointing_at_direction (angle, DIAGONAL_AREA, i);

            sticks[stick].active_directions[i] = active;
        }

        notify_control ();
    }

    public override void touch_button_pressed (string element, string? qualifier) {
        InputDirection direction;

        if (qualifier != null) {
            var dir = InputDirection.from_string (qualifier);
            if (dir == null)
                return;

            direction = dir;
        } else {
            if (!(element in touch_directions))
                return;

            direction = touch_directions[element];
        }

        bool was_active = is_active (direction);
        var touch = get_touch_for_direction (direction);

        touch.add (element);

        if (!was_active)
            notify_control ();
    }

    public override void touch_button_released (string element, string? qualifier) {
        InputDirection direction;

        if (qualifier != null) {
            var dir = InputDirection.from_string (qualifier);
            if (dir == null)
                return;

            direction = dir;
        } else {
            if (!(element in touch_directions))
                return;

            direction = touch_directions[element];
        }

        var touch = get_touch_for_direction (direction);
        if (touch == null)
            return;

        touch.remove (element);

        if (!is_active (direction))
            notify_control ();
    }

    public override void touch_stick_moved (string element, double x, double y) {
        double length = Math.sqrt (x * x + y * y);
        double angle = Math.atan2 (y, x);

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            bool active = length > STICK_DEADZONE &&
                AnalogUtils.is_pointing_at_direction (angle, DIAGONAL_AREA, i);

            touch_sticks[element].active_directions[i] = active;
        }

        notify_control ();
    }

    public override void touch_clear (string element) {
        if (touch_directions != null) {
            foreach (var direction in InputDirection.all ())
                touch_button_released (element, direction.to_string ());
        }

        if (touch_sticks != null && element in touch_sticks)
            touch_stick_moved (element, 0, 0);
    }

    private bool is_active (InputDirection direction) {
        for (int i = 0; i < Gamepad.N_STICKS; i++) {
            if (sticks[i] == null)
                continue;

            if (sticks[i].active_directions[direction])
                return true;
        }

        if (touch_sticks != null) {
            foreach (var data in touch_sticks.get_values ()) {
                if (data.active_directions[direction])
                    return true;
            }
        }

        var keys = get_keys_for_direction (direction);
        var buttons = get_buttons_for_direction (direction);
        var touch = get_touch_for_direction (direction);

        return (keys != null && keys.size > 0) ||
               (buttons != null && buttons.size > 0) ||
               (touch != null && touch.length > 0);
    }

    private void notify_control () {
        notify_for_directions (UP, DOWN);
        notify_for_directions (LEFT, RIGHT);
    }

    private void notify_for_directions (InputDirection dir1, InputDirection dir2) {
        var dpad = control as DpadControl;

        bool old_active1 = last_active[dir1];
        bool old_active2 = last_active[dir2];

        bool new_active1 = is_active (dir1);
        bool new_active2 = is_active (dir2);

        if (old_active1 == new_active1 && old_active2 == new_active2)
            return;

        if (new_active1 && new_active2) {
            new_active1 = false;
            new_active2 = false;
        }

        if (old_active1 != new_active1)
            dpad.activate (runner, player, dir1, new_active1);

        if (old_active2 != new_active2)
            dpad.activate (runner, player, dir2, new_active2);

        last_active[dir1] = new_active1;
        last_active[dir2] = new_active2;
    }
}
