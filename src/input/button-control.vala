// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.ButtonControl : Control {
    public signal void activate (Runner runner, uint player, bool pressed);

    public ButtonControl (string id, string name) {
        Object (id: id, name: name);
    }

    public override ControlType get_control_type () {
        return BUTTON;
    }

    public override string get_name_for_qualifier (string qualifier) {
        return name;
    }
}
