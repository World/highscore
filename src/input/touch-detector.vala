// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.TouchDetector : Object {
    private static TouchDetector? instance;

    private Settings settings;
    private bool decided;

    private struct WidgetData {
        Gtk.Widget widget;
        Gtk.EventControllerKey key_controller;
        Gtk.GestureClick click_gesture;
    }

    private WidgetData[] widget_data;

    private TouchDetector () {
        settings = new Settings ("app.drey.Highscore");
        widget_data = {};
    }

    public void register (Gtk.Widget widget) {
        var key_controller = new Gtk.EventControllerKey () {
            propagation_phase = CAPTURE,
        };
        key_controller.key_pressed.connect ((keyval, keycode, state) => {
            decide (false);
            return Gdk.EVENT_PROPAGATE;
        });
        widget.add_controller (key_controller);

        var click_gesture = new Gtk.GestureClick () {
            propagation_phase = CAPTURE,
            button = 0,
        };
        click_gesture.pressed.connect (() => {
            decide (click_gesture.get_current_sequence () != null);
            click_gesture.set_state (DENIED);
        });
        widget.add_controller (click_gesture);

        WidgetData data = { widget, key_controller, click_gesture };
        widget_data += data;
    }

    private void decide (bool is_touch) {
        if (decided)
            return;

        if (is_touch)
            debug ("Enabling overlay controls");
        else
            debug ("Disabling overlay controls");

        settings.set_boolean ("touch-controls", is_touch);
        settings.set_boolean ("detecting-touch", false);

        decided = true;

        Idle.add_once (cleanup);
    }

    private void cleanup () {
        foreach (var data in widget_data) {
            data.widget.remove_controller (data.key_controller);
            data.widget.remove_controller (data.click_gesture);
        }

        instance = null;
    }

    public static TouchDetector? get_instance () {
        if (instance != null)
            return instance;

        var settings = new Settings ("app.drey.Highscore");
        if (!settings.get_boolean ("detecting-touch"))
            return null;

        instance = new TouchDetector ();
        return instance;
    }
}
