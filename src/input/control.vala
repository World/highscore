// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.Control : Object {
    public enum ControlType {
        BUTTON,
        DPAD,
        STICK,
        TRIGGER,
        POINTER,
        ROTARY;

        public static ControlType? from_string (string str) {
            if (str == "button")
                return BUTTON;
            if (str == "dpad")
                return DPAD;
            if (str == "stick")
                return STICK;
            if (str == "trigger")
                return TRIGGER;
            if (str == "pointer")
                return POINTER;
            if (str == "rotary")
                return ROTARY;

            return null;
        }
    }

    public string id { get; construct; }
    public string name { get; construct; }

    public unowned ControlSection section { get; set; }

    public abstract ControlType get_control_type ();

    public abstract string get_name_for_qualifier (string qualifier);
}
