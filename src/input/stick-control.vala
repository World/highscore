// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.StickControl : Control {
    public signal void moved (Runner runner, uint player, double x, double y);

    public StickControl (string id, string name) {
        Object (id: id, name: name);
    }

    public override ControlType get_control_type () {
        return STICK;
    }

    public override string get_name_for_qualifier (string qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return name;

        var dir_name = direction.get_display_name ();

        return "%s %s".printf (name, dir_name);
    }
}
