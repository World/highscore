// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PointerControl : Control {
    public signal void pressed (Runner runner, uint player, double x, double y);
    public signal void moved (Runner runner, uint player, double x, double y);
    public signal void released (Runner runner, uint player);

    public PointerControl (string id, string name) {
        Object (id: id, name: name);
    }

    public override ControlType get_control_type () {
        return POINTER;
    }

    public override string get_name_for_qualifier (string qualifier) {
        return name;
    }
}
