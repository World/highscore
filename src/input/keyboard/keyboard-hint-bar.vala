// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/input/keyboard/keyboard-hint-bar.ui")]
public class Highscore.KeyboardHintBar : Adw.Bin {
    public signal void select ();
    public signal void cancel ();

    [GtkChild]
    private unowned Gtk.Button select_btn;
    [GtkChild]
    private unowned Gtk.Button cancel_btn;

    public string select_label { get; set; default = ""; }
    public string cancel_label { get; set; default = ""; }

    construct {
        notify["select-label"].connect (() => {
            select_btn.visible = select_label != "";
        });

        notify["cancel-label"].connect (() => {
            cancel_btn.visible = cancel_label != "";
        });
    }

    [GtkCallback]
    private void select_click_cb () {
        select ();
    }

    [GtkCallback]
    private void cancel_click_cb () {
        cancel ();
    }
}
