// This file is part of Highscore. License: GPL-3.0-or-later

public errordomain Highscore.KeyError {
    INVALID_KEY,
}

namespace Highscore.KeyboardUtils {
    private uint keycode_to_keyval (int keycode) {
        uint keyval;

        bool ret = Gdk.Display.get_default ().translate_key (
            keycode + 8, 0, 0, out keyval, null, null, null
        );

        return ret ? keyval : 0;
    }

    public string get_key_name (int keycode) {
        uint keyval = keycode_to_keyval (keycode);

        return Gtk.accelerator_name_with_keycode (
            Gdk.Display.get_default (),
            keyval,
            keycode,
            0
        );
    }

    public string get_key_label (int keycode) {
        uint keyval = keycode_to_keyval (keycode);

        return Gtk.accelerator_get_label_with_keycode (
            Gdk.Display.get_default (),
            keyval,
            keycode,
            0
        );
    }

    public uint parse_keycode (string name) throws KeyError {
        uint[] codes;
        Gdk.ModifierType mods;

        Gtk.accelerator_parse_with_keycode (
            name,
            Gdk.Display.get_default (),
            null,
            out codes,
            out mods
        );

        if (mods != NO_MODIFIER_MASK || codes.length == 0)
            throw new KeyError.INVALID_KEY ("Invalid key: “%s”", name);

        return codes[0] - 8;
    }
}
