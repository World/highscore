// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlatformKeyboardMapping : Object {
    public struct Key {
        int keycode;
        string control;
    }

    private Key[] keys;
    private HashTable<string, Key?> control_to_key;

    construct {
        keys = {};
        control_to_key = new HashTable<string, Key?> (str_hash, str_equal);
    }

    public void map (int keycode, string control) {
        Key mapping = { keycode, control };

        keys += mapping;
        control_to_key[control] = mapping;
    }

    public Key[] list_key_mappings () {
        return keys;
    }

    public int get_key_for_control (string control, string? qualifier = null) {
        var qualified_control = control;

        if (qualifier != null)
            qualified_control = @"$control:$qualifier";

        if (qualified_control in control_to_key)
            return control_to_key[qualified_control].keycode;

        return -1;
    }
}
