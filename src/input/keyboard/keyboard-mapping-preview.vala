// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/input/keyboard/keyboard-mapping-preview.ui")]
public class Highscore.KeyboardMappingPreview : Adw.Bin {
    public Game game { get; construct; }
    public string? controller_type { get; set; }

    public bool compact { get; set; }

    private PlatformControls controls;
    private HashTable<string, Gtk.Box> sections;

    [GtkChild]
    private unowned Gtk.FlowBox controls_box;

    public KeyboardMappingPreview (Game game) {
        Object (game: game);
    }

    construct {
        controls = PlatformControls.create (game.platform);
        controller_type = controls.default_controller;

        sections = new HashTable<string, Gtk.Box> (str_hash, str_equal);

        notify["controller-type"].connect (update_ui);
        notify["compact"].connect (update_flowbox);

        update_ui ();
    }

    static construct {
        set_css_name ("keyboard-mapping-preview");
    }

    private void update_ui () {
        var global_controller = controls.global_controller;
        PlatformController controller = null;
        if (controller_type != null)
            controller = controls.get_controller (controller_type);

        PlatformKeyboardMapping? mapping, global_mapping;

        if (global_controller != null)
            global_mapping = global_controller.default_keyboard_mapping;
        else
            global_mapping = null;

        if (controller != null)
            mapping = controller.default_keyboard_mapping;
        else
            mapping = null;

        foreach (var section_widget in sections.get_values ())
            controls_box.remove (section_widget);

        sections.remove_all ();

        if (mapping != null)
            add_controls (controller, mapping);
        if (global_mapping != null)
            add_controls (global_controller, global_mapping);

        if (controller != null)
            add_pointer_controls (controller);
        if (global_controller != null)
            add_pointer_controls (global_controller);

        update_flowbox ();
    }

    private void update_flowbox () {
        if (compact) {
            controls_box.halign = CENTER;
            controls_box.max_children_per_line = 1;
        } else if (sections.size () < 3) {
            controls_box.halign = CENTER;
            controls_box.max_children_per_line = uint.max (1, sections.size ());
        } else {
            controls_box.halign = FILL;
            controls_box.max_children_per_line = 3;
        }
    }

    private void add_controls (PlatformController controller, PlatformKeyboardMapping mapping) {
        foreach (var section in controller.list_sections ()) {
            var box = new Gtk.Box (VERTICAL, 12);
            box.add_css_class ("section");
            box.add_css_class (section.id);

            sections[section.id] = box;

            controls_box.append (box);

            foreach (var control in section.list_controls ()) {
                if (control.get_control_type () != POINTER)
                    add_control (control, mapping);
            }
        }
    }

    private void add_pointer_controls (PlatformController controller) {
        var screens = controller.list_screens ();
        foreach (int screen_id in screens) {
            var control = controller.get_for_screen (screen_id);
            if (control == null)
                continue;

            add_control (control, null);
        }
    }

    private void add_control (Control control, PlatformKeyboardMapping? mapping) {
        switch (control.get_control_type ()) {
            case BUTTON:
            case TRIGGER:
                add_control_with_qualifier (control, null, mapping);
                break;
            case DPAD:
            case STICK:
                add_control_with_qualifier (control, "up",    mapping);
                add_control_with_qualifier (control, "left",  mapping);
                add_control_with_qualifier (control, "right", mapping);
                add_control_with_qualifier (control, "down",  mapping);
                break;
            case POINTER:
                add_pointer_control (control);
                break;
            case ROTARY:
                add_control_with_qualifier (control, "left",  mapping);
                add_control_with_qualifier (control, "right", mapping);
                break;
            default:
                assert_not_reached ();
        }
    }

    private void add_control_with_qualifier (Control control, string? qualifier, PlatformKeyboardMapping mapping) {
        var keycode = mapping.get_key_for_control (control.id, qualifier);
        var key_name = KeyboardUtils.get_key_name (keycode);
        string control_name;

        if (qualifier == null)
            control_name = control.name;
        else
            control_name = control.get_name_for_qualifier (qualifier);

        var box = new Gtk.Box (HORIZONTAL, 12);

        var shortcut_label = new Gtk.ShortcutLabel (key_name);
        var shortcut_mask = new MaskBin () {
            child = shortcut_label,
        };
        shortcut_mask.add_css_class ("shortcut-mask-bin");

        var mapping_label = new IconLabel (control_name);
        mapping_label.add_css_class ("numeric");

        box.append (shortcut_mask);
        box.append (mapping_label);

        var section_box = sections[control.section.id];
        assert (section_box != null);

        section_box.append (box);
    }

    private void add_pointer_control (Control control) {
        var box = new Gtk.Box (HORIZONTAL, 12);
        var icon = new Gtk.Image.from_icon_name ("pointer-input-symbolic");
        icon.add_css_class ("pointer-icon");

        var mapping_label = new IconLabel (control.name);
        mapping_label.add_css_class ("numeric");

        box.append (icon);
        box.append (mapping_label);

        var section_box = sections[control.section.id];
        assert (section_box != null);

        section_box.append (box);
    }
}
