// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.RotaryControl : Control {
    public signal void rotated (Runner runner, uint player, double delta);

    public RotaryControl (string id, string name) {
        Object (id: id, name: name);
    }

    public override ControlType get_control_type () {
        return ROTARY;
    }

    public override string get_name_for_qualifier (string qualifier) {
        var direction = RotaryDirection.from_string (qualifier);
        if (direction == null)
            return name;

        var dir_name = direction.get_display_name ();

        return "%s %s".printf (name, dir_name);
    }
}
