// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.RotaryHandler : ControlHandler {
    private Gee.HashSet<int>? pressed_keys;
    private HashTable<int, RotaryDirection> key_directions;

    private Gee.HashSet<Gamepad.Button>? pressed_buttons;
    private HashTable<Gamepad.Button, RotaryDirection> button_directions;
    private double stick_x[Gamepad.N_STICKS];
    private GenericSet<string>? touch_left;
    private GenericSet<string>? touch_right;
    private HashTable<string, RotaryDirection> touch_directions;
    private HashTable<string, double?> touch_sticks;

    public RotaryHandler (Runner runner, uint player, Control control) {
        this.runner = runner;
        this.player = player;
        this.control = control;
    }

    private GenericSet<string>? get_touch_for_direction (RotaryDirection direction) {
        switch (direction) {
            case LEFT:
                return touch_left;
            case RIGHT:
                return touch_right;
            default:
                assert_not_reached ();
        }
    }

    public override bool add_key (int keycode, string? qualifier) {
        var direction = RotaryDirection.from_string (qualifier);
        if (direction == null)
            return false;

        if (pressed_keys == null) {
            pressed_keys = new Gee.HashSet<int> ();
            key_directions = new HashTable<int, RotaryDirection> (direct_hash, direct_equal);
        }

        key_directions[keycode] = direction;
        return true;
    }

    public override bool add_button (Gamepad.Button button, string? qualifier) {
        var direction = RotaryDirection.from_string (qualifier);
        if (direction == null)
            return false;

        if (pressed_buttons == null) {
            pressed_buttons = new Gee.HashSet<Gamepad.Button> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
            button_directions = new HashTable<Gamepad.Button, RotaryDirection> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
        }

        button_directions[button] = direction;
        return true;
    }

    public override bool add_stick (Gamepad.Stick stick, InputDirection? direction, string? qualifier) {
        assert (direction == null);

        return true;
    }

    public override bool add_pointer (int screen_id) {
        return false;
    }

    public override bool add_touch (string element, Control.ControlType control_type, string? qualifier) {
        if (control_type == DPAD || control_type == BUTTON) {
            if (touch_left == null) {
                touch_left = new GenericSet<string> (str_hash, str_equal);
                touch_right = new GenericSet<string> (str_hash, str_equal);
                touch_directions = new HashTable<string, RotaryDirection> (
                    str_hash, str_equal
                );
            }

            if (control_type == BUTTON) {
                var direction = RotaryDirection.from_string (qualifier);
                if (direction == null)
                    return false;

                touch_directions[element] = direction;
            }

            return true;
        }

        if (control_type == STICK) {
            if (qualifier != null)
                return false;

            if (touch_sticks == null)
                touch_sticks = new HashTable<string, double?> (str_hash, str_equal);

            touch_sticks[element] = 0;

            return true;
        }

        return false;
    }

    public override void key_pressed (int keycode) {
        pressed_keys.add (keycode);

        notify_control ();
    }

    public override void key_released (int keycode) {
        pressed_keys.remove (keycode);

        notify_control ();
    }

    public override void button_pressed (Gamepad.Button button) {
        pressed_buttons.add (button);

        notify_control ();
    }

    public override void button_released (Gamepad.Button button) {
        pressed_buttons.remove (button);

        notify_control ();
    }

    public override void stick_moved (Gamepad.Stick stick, double x, double y) {
        stick_x[stick] = x;

        notify_control ();
    }

    public override void drag_begin (double x, double y) {}
    public override void drag_update (double x, double y) {}
    public override void drag_end () {}

    public override void touch_button_pressed (string element, string? qualifier) {
        RotaryDirection direction;

        if (qualifier != null) {
            var dir = RotaryDirection.from_string (qualifier);
            if (dir == null)
                return;

            direction = dir;
        } else {
            if (!(element in touch_directions))
                return;

            direction = touch_directions[element];
        }

        var touch = get_touch_for_direction (direction);

        touch.add (element);

        notify_control ();
    }

    public override void touch_button_released (string element, string? qualifier) {
        RotaryDirection direction;

        if (qualifier != null) {
            var dir = RotaryDirection.from_string (qualifier);
            if (dir == null)
                return;

            direction = dir;
        } else {
            if (!(element in touch_directions))
                return;

            direction = touch_directions[element];
        }

        var touch = get_touch_for_direction (direction);
        if (touch == null)
            return;

        touch.remove (element);

        notify_control ();
    }

    public override void touch_stick_moved (string element, double x, double y) {
        touch_sticks[element] = x;

        notify_control ();
    }

    public override void touch_clear (string element) {
        if (touch_directions != null) {
            foreach (var direction in RotaryDirection.all ())
                touch_button_released (element, direction.to_string ());
        }

        if (touch_sticks != null && element in touch_sticks)
            touch_stick_moved (element, 0, 0);
    }

    private void notify_control () {
        var rotary = control as RotaryControl;
        double delta = 0;
        double length = 0;

        if (pressed_keys != null) {
            foreach (var key in pressed_keys) {
                var direction = key_directions[key];

                delta += direction.get_direction ();
                length++;
            }
        }

        if (pressed_buttons != null) {
            foreach (var button in pressed_buttons) {
                var direction = button_directions[button];

                delta += direction.get_direction ();
                length++;
            }
        }

        if (touch_directions != null) {
            foreach (var direction in RotaryDirection.all ()) {
                var touch = get_touch_for_direction (direction);

                delta += touch.length * direction.get_direction ();
                length += touch.length;
            }
        }

        for (int i = 0; i < Gamepad.N_STICKS; i++) {
            delta += stick_x[i];

            if (stick_x[i] != 0)
                length++;
        }

        if (touch_sticks != null) {
            foreach (double stick_delta in touch_sticks.get_values ()) {
                delta += stick_delta;

                if (stick_delta != 0)
                    length++;
            }
        }

        if (length > 0)
            delta /= length;

        rotary.rotated (runner, player, delta);
    }
}
