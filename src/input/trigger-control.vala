// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.TriggerControl : Control {
    public signal void moved (Runner runner, uint player, double pressure);

    public TriggerControl (string id, string name) {
        Object (id: id, name: name);
    }

    public override ControlType get_control_type () {
        return TRIGGER;
    }

    public override string get_name_for_qualifier (string qualifier) {
        return name;
    }
}
