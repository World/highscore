// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/input/gamepad/gamepad-hint-bar.ui")]
public class Highscore.GamepadHintBar : Adw.Bin {
    [GtkChild]
    private unowned Gtk.ActionBar action_bar;
    [GtkChild]
    private unowned Gtk.Button select_btn;
    [GtkChild]
    private unowned Adw.ButtonContent select_content;
    [GtkChild]
    private unowned Gtk.Button cancel_btn;
    [GtkChild]
    private unowned Adw.ButtonContent cancel_content;

    public bool animate { get; set; default = true; }

    private BindingGroup controller_bindings;

    private GamepadController? _controller;
    public GamepadController? controller {
        get { return _controller; }
        set {
            if (controller == value)
                return;

            if (controller != null) {
                controller.buttons_changed.disconnect (update_ui);

                set_revealed (false);
            }

            _controller = value;

            if (controller != null) {
                controller.buttons_changed.connect (update_ui);

                update_ui ();
            }
        }
    }

    construct {
        controller_bindings = new BindingGroup ();

        controller_bindings.bind_property (
            "select-label", select_content, "label", SYNC_CREATE
        );

        controller_bindings.bind_property (
            "select-label", select_btn, "visible", SYNC_CREATE,
            (binding, from_value, ref to_value) => {
                var str = from_value.get_string ();
                to_value = (str != "");
                return true;
            }
        );

        controller_bindings.bind_property (
            "cancel-label", cancel_content, "label", SYNC_CREATE
        );

        controller_bindings.bind_property (
            "cancel-label", cancel_btn, "visible", SYNC_CREATE,
            (binding, from_value, ref to_value) => {
                var str = from_value.get_string ();
                to_value = (str != "");
                return true;
            }
        );

        bind_property ("controller", controller_bindings, "source", DEFAULT);

        notify["animate"].connect (() => {
            if (animate) {
                action_bar.revealed = visible;
                visible = true;
            } else {
                visible = action_bar.revealed;
                action_bar.revealed = true;
            }
        });
    }

    private void update_ui () {
        var gamepad = controller.main_gamepad;
        if (gamepad == null) {
            set_revealed (false);
            return;
        }

        select_content.icon_name = controller.get_select_icon_name ();
        cancel_content.icon_name = controller.get_cancel_icon_name ();

        set_revealed (true);
    }

    [GtkCallback]
    private void select_click_cb () {
        controller.select ();
    }

    [GtkCallback]
    private void cancel_click_cb () {
        controller.cancel ();
    }

    private void set_revealed (bool revealed) {
        if (animate)
            action_bar.revealed = revealed;
        else
            visible = revealed;
    }
}
