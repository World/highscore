// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadMapButtonSection : Adw.Bin {
    public const double DEADZONE = 0.5;

    public signal void save_and_close ();

    public GamepadMappingEditor editor { get; construct; }
    public Gamepad.Button button { get; construct; }
    public string icon_name { get; construct; }
    public string label { get; construct; }

    private unowned Gtk.Button unset_btn;

    public GamepadMapButtonSection (
        GamepadMappingEditor editor,
        Gamepad.Button button,
        string icon_name,
        string label
    ) {
        Object (editor: editor, button: button, icon_name: icon_name, label: label);
    }

    construct {
        var box = new Gtk.Box (VERTICAL, 18) {
            margin_top = 12,
            margin_bottom = 6,
            margin_start = 6,
            margin_end = 6,
        };

        var icon = new Gtk.Image.from_icon_name (icon_name) {
            pixel_size = 48,
        };
        icon.add_css_class ("dimmed");
        box.append (icon);

        box.append (new Gtk.Label (label));

        child = box;

        var unset_btn = new Gtk.Button.with_mnemonic (_("_Unset"));
        unset_btn.visible = editor.has_binding (button.get_sdl_name ());
        unset_btn.add_css_class ("destructive-action");
        box.append (unset_btn);

        unset_btn.clicked.connect (() => {
            editor.unmap (button.get_sdl_name ());
            unset_btn.visible = false;
            save_and_close ();
        });

        this.unset_btn = unset_btn;
    }

    public void gamepad_event_cb (Manette.Event event) {
        switch (event.get_event_type ()) {
            case EVENT_BUTTON_RELEASE:
                editor.map_to_button (
                    button.get_sdl_name (), event.get_hardware_index ()
                );
                unset_btn.visible = true;
                save_and_close ();
                break;

            case EVENT_ABSOLUTE:
                uint16 axis, index = event.get_hardware_index ();
                double value;
                event.get_absolute (out axis, out value);

                // Assume shoulder buttons are mapped to the whole axis, other buttons to only a half
                if (button == L || button == L2 || button == R || button == R2) {
                    if (value > -DEADZONE)
                        return;

                    editor.map_to_axis (button.get_sdl_name (), index, 0);
                    unset_btn.visible = true;
                    save_and_close ();
                    return;
                } else {
                    if (-DEADZONE < value < DEADZONE)
                        return;

                    editor.map_to_axis (
                        button.get_sdl_name (), index, value > 0 ? 1 : -1
                    );
                    unset_btn.visible = true;
                    save_and_close ();
                    return;
                }

            case EVENT_HAT:
                uint16 axis;
                int8 value;
                event.get_hat (out axis, out value);

                if (value == 0)
                    return;

                editor.map_to_hat (
                    button.get_sdl_name (), event.get_hardware_index (), value
                );
                unset_btn.visible = true;
                save_and_close ();
                break;

            default:
                return;
        }
    }
}
