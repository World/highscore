// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadMappingEditor : Object {
    public Gamepad gamepad { get; construct; }

    public bool modified {
        get {
            return device.has_user_mapping ();
        }
    }

    private Manette.Device device;
    private string[] ids;
    private HashTable<string, string> id_to_binding;
    private HashTable<string, string> binding_to_id;

    public GamepadMappingEditor (Gamepad gamepad) {
        Object (gamepad: gamepad);
    }

    construct {
        device = gamepad.device;

        ids = {};
        id_to_binding = new HashTable<string, string> (str_hash, str_equal);
        binding_to_id = new HashTable<string, string> (str_hash, str_equal);

        foreach (var button in Gamepad.Button.all ())
            ids += button.get_sdl_name ();

        foreach (var stick in Gamepad.Stick.all ()) {
            ids += stick.get_x_sdl_name ();
            ids += stick.get_y_sdl_name ();
        }

        GamepadMappingUtils.parse_mapping (gamepad, (id, value) => {
            id_to_binding[id] = value;
            binding_to_id[value] = id;
        });
    }

    public void save_mapping () {
        var str = "platform:Linux,";

        foreach (var id in ids) {
            var value = id_to_binding[id];

            if (value == null)
                continue;

            str += @"$id:$value,";
        }

        device.save_user_mapping (str);

        string[] mapped_ids = {};

        foreach (var id in ids) {
            if (id in id_to_binding)
                mapped_ids += id;
        }

        var missing = GamepadMappingUtils.get_missing_controls (mapped_ids);

        gamepad.set_missing (missing);

        notify_property ("modified");
    }

    public void reset_mapping () {
        device.remove_user_mapping ();
        gamepad.reset_missing ();

        id_to_binding.remove_all ();
        binding_to_id.remove_all ();

        GamepadMappingUtils.parse_mapping (gamepad, (id, value) => {
            id_to_binding[id] = value;
            binding_to_id[value] = id;
        });

        notify_property ("modified");
    }

    public bool has_binding (string id) {
        return id in id_to_binding;
    }

    private void remove_by_id (string id) {
        if (id in id_to_binding) {
            binding_to_id.remove (id_to_binding[id]);
            id_to_binding.remove (id);
        }
    }

    private void remove_by_value (string value) {
        if (value in binding_to_id) {
            id_to_binding.remove (binding_to_id[value]);
            binding_to_id.remove (value);
        }
    }

    private void map (string id, string value) {
        remove_by_id (id);
        remove_by_value (value);

        id_to_binding[id] = value;
        binding_to_id[value] = id;
    }

    public void map_to_button (string id, uint16 index) {
        map (id, @"b$index");
    }

    public void map_to_hat (string id, uint16 index, int8 value) {
        var dpad_index = index / 2;
        var dpad_axis = index % 2;
        var dpad_position = 1 << ((value + dpad_axis + 4) % 4);

        map (id, @"h$dpad_index.$dpad_position");
    }

    public void map_to_axis (string id, uint16 index, int range) {
        var whole_value = @"a$index";
        var neg_value = @"-$whole_value";
        var pos_value = @"+$whole_value";

        remove_by_id (id);

        if (range == 0) {
            remove_by_value (neg_value);
            remove_by_value (pos_value);

            map (id, whole_value);
            return;
        }

        remove_by_value (whole_value);

        map (id, range > 0 ? pos_value : neg_value);
    }

    public void unmap (string id) {
        id_to_binding.remove (id);
    }
}
