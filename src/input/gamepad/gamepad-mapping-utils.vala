// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.GamepadMappingUtils {
    public delegate void ParseMappingFunc (string id, string value);

    public void parse_mapping (Gamepad gamepad, ParseMappingFunc func) {
        var mapping = gamepad.device.get_mapping ();
        var parts = mapping.split (",");

        foreach (var part in parts) {
            if (!part.contains (":"))
                continue;

            var inner_parts = part.split (":");

            if (inner_parts.length != 2) {
                warning ("Invalid mapping: %s", part);
                continue;
            }

            if (inner_parts[0] == "platform")
                continue;

            var id = inner_parts[0];
            var value = inner_parts[1];

            func (id, value);
        }
    }

    public string[] get_missing_controls (string[] ids) {
        var id_set = new GenericSet<string> (str_hash, str_equal);
        foreach (var id in ids)
            id_set.add (id);

        string[] missing = {};

        if (!(Gamepad.Button.UP.get_sdl_name () in id_set) ||
            !(Gamepad.Button.DOWN.get_sdl_name () in id_set) ||
            !(Gamepad.Button.LEFT.get_sdl_name () in id_set) ||
            !(Gamepad.Button.RIGHT.get_sdl_name () in id_set)) {
            missing += "dpad";
        }

        foreach (var button in Gamepad.Button.all ()) {
            if (button == UP || button == DOWN || button == LEFT || button == RIGHT)
                continue;

            foreach (var stick in Gamepad.Stick.all ()) {
                if (button == stick.get_button ())
                    continue;
            }

            if (button.get_sdl_name () in id_set)
                continue;

            missing += button.get_id ();
        }

        foreach (var stick in Gamepad.Stick.all ()) {
            if (stick.get_x_sdl_name () in id_set ||
                stick.get_y_sdl_name () in id_set ||
                stick.get_button ().get_sdl_name () in id_set) {
                continue;
            }

            missing += stick.get_id ();
        }

        return missing;
    }
}
