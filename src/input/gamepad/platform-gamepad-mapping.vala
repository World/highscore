// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlatformGamepadMapping : Object {
    public struct Button {
        Gamepad.Button button;
        string control;
    }

    public struct Stick {
        Gamepad.Stick stick;
        string control;
    }

    public struct Layer {
        Gamepad.Button button;
        string name;
        PlatformGamepadMapping mapping;
    }

    private Button[] buttons;
    private Stick[] sticks;
    private Layer[] layers;

    construct {
        buttons = {};
        sticks = {};
        layers = {};
    }

    public void map_button (Gamepad.Button button, string control) {
        Button mapping = { button, control };

        buttons += mapping;
    }

    public void map_dpad (string dpad) {
        map_button (UP,    @"$dpad:up");
        map_button (DOWN,  @"$dpad:down");
        map_button (LEFT,  @"$dpad:left");
        map_button (RIGHT, @"$dpad:right");
    }

    public void map_stick (Gamepad.Stick stick, string control) {
        Stick mapping = { stick, control };

        sticks += mapping;
    }

    public void map_stick_to_buttons (
        Gamepad.Stick stick,
        string? up,
        string? down,
        string? left,
        string? right
    ) {
        string dest = "%s|%s|%s|%s".printf (
            up ?? "",
            down ?? "",
            left ?? "",
            right ?? ""
        );

        Stick mapping = { stick, dest };

        sticks += mapping;
    }

    public void add_layer (Gamepad.Button button, string name, PlatformGamepadMapping mapping) {
        Layer layer = { button, name, mapping };

        layers += layer;
    }

    public Button[] list_button_mappings () {
        return buttons;
    }

    public Stick[] list_stick_mappings () {
        return sticks;
    }

    public Layer[] list_layers () {
        return layers;
    }

    public string? get_mapping_for_dpad () {
        string? up = null, down = null, left = null, right = null;

        foreach (var mapping in buttons) {
            if (mapping.button == UP) {
                up = mapping.control;
                continue;
            }
            if (mapping.button == DOWN) {
                down = mapping.control;
                continue;
            }
            if (mapping.button == LEFT) {
                left = mapping.control;
                continue;
            }
            if (mapping.button == RIGHT) {
                right = mapping.control;
                continue;
            }
        }

        if (up == null || down == null || left == null || right == null)
            return null;

        up = up.split (":", 2)[0];
        down = down.split (":", 2)[0];
        left = left.split (":", 2)[0];
        right = right.split (":", 2)[0];

        if (up == down && down == left && left == right)
            return up;

        return null;
    }

    public string? get_mapping_for_button (Gamepad.Button button) {
        foreach (var mapping in buttons) {
            if (mapping.button == button)
                return mapping.control;
        }

        return null;
    }

    public PlatformGamepadMapping? get_layer_for_button (Gamepad.Button button) {
        foreach (var layer in layers) {
            if (layer.button == button)
                return layer.mapping;
        }

        return null;
    }

    public string? get_layer_name_for_button (Gamepad.Button button) {
        foreach (var layer in layers) {
            if (layer.button == button)
                return layer.name;
        }

        return null;
    }

    public string? get_mapping_for_stick (Gamepad.Stick stick) {
        foreach (var mapping in sticks) {
            if (mapping.stick == stick)
                return mapping.control;
        }

        return null;
    }

    public bool has_mapping_for_control (string control) {
        if (control == "dpad")
            return get_mapping_for_dpad () != null;

        foreach (var mapping in buttons) {
            if (mapping.button == UP || mapping.button == DOWN ||
                mapping.button == LEFT || mapping.button == RIGHT) {

                if (get_mapping_for_dpad () != null)
                    return false;

                if (mapping.button.get_id () == control)
                    return true;
            }

            if (mapping.button.get_id () == control)
                return true;
        }

        foreach (var mapping in sticks) {
            if (!control.has_prefix (mapping.stick.get_id ()))
                continue;

            bool has_separate_mappings = mapping.control.split ("|").length == 4;

            return (control.contains (":")) == has_separate_mappings;
        }

        return false;
    }
}
