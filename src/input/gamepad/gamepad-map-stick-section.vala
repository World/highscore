// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadMapStickSection : Adw.Bin {
    public const double DEADZONE = 0.5;

    public signal void save_and_close ();

    public GamepadMappingEditor editor { get; construct; }
    public Gamepad.Stick stick { get; construct; }
    public bool horiz { get; construct; }
    public string icon_name { get; construct; }
    public string label { get; construct; }

    private unowned Gtk.Button unset_btn;
    private string sdl_name;

    public GamepadMapStickSection (
        GamepadMappingEditor editor,
        Gamepad.Stick stick,
        bool horiz,
        string icon_name,
        string label
    ) {
        Object (editor: editor, stick: stick, horiz: horiz, icon_name: icon_name, label: label);
    }

    construct {
        var box = new Gtk.Box (VERTICAL, 18) {
            margin_top = 12,
            margin_bottom = 6,
            margin_start = 6,
            margin_end = 6,
        };

        sdl_name = horiz ? stick.get_x_sdl_name () : stick.get_y_sdl_name ();

        var icon = new Gtk.Image.from_icon_name (icon_name) {
            pixel_size = 48,
        };
        icon.add_css_class ("dimmed");
        box.append (icon);

        box.append (new Gtk.Label (label));

        child = box;

        var unset_btn = new Gtk.Button.with_mnemonic (_("_Unset"));
        unset_btn.visible = editor.has_binding (sdl_name);
        unset_btn.add_css_class ("destructive-action");
        box.append (unset_btn);

        unset_btn.clicked.connect (() => {
            editor.unmap (sdl_name);
            unset_btn.visible = false;
            save_and_close ();
        });

        this.unset_btn = unset_btn;
    }

    public void gamepad_event_cb (Manette.Event event) {
        switch (event.get_event_type ()) {
            case EVENT_ABSOLUTE:
                uint16 axis, index = event.get_hardware_index ();
                double value;
                event.get_absolute (out axis, out value);

                if (-DEADZONE < value < DEADZONE)
                    return;

                editor.map_to_axis (sdl_name, index, 0);
                unset_btn.visible = true;
                save_and_close ();
                break;

            default:
                return;
        }
    }
}
