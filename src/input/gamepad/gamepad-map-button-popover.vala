// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadMapButtonPopover : GamepadMapPopover {
    public Gamepad.Button button { get; construct; }

    private unowned GamepadMapButtonSection section;

    public GamepadMapButtonPopover (GamepadMappingEditor editor, GamepadType gamepad_type, Gamepad.Button button) {
        Object (editor: editor, gamepad_type: gamepad_type, button: button);
    }

    construct {
        var section = new GamepadMapButtonSection (
            editor, button, "gamepad-map-button",
            _("Press the %s button").printf (gamepad_type.get_button_name (button))
        );
        section.save_and_close.connect (() => {
            editor.save_mapping ();
            popdown ();
        });

        this.section = section;

        child = section;
    }

    protected override void gamepad_event_cb (Manette.Event event) {
        section.gamepad_event_cb (event);
    }
}
