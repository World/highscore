// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadType : Object {
    public enum SystemPosition {
        TOP_LEFT,
        TOP_CENTER,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_CENTER,
        BOTTOM_RIGHT;
    }

    private static GamepadType xbox_360;
    private static GamepadType xbox_one;
    private static GamepadType eight_bitdo;
    private static GamepadType steam_deck;
    private static GamepadType switch_pro;
    private static GamepadType dualshock4;

    public string id { get; construct; }
    public string name { get; construct; }

    public Gamepad.Button select_button { get; set; }
    public Gamepad.Button cancel_button { get; set; }

    private string? dpad_name;
    private string? dpad_icon;
    private string[] button_names;
    private string[] button_icons;
    private string[] stick_names;

    public bool left_stick_above_dpad { get; set; }
    public bool right_stick_above_buttons { get; set; }
    public SystemPosition select_position { get; set; default = BOTTOM_LEFT; }
    public SystemPosition start_position { get; set; default = BOTTOM_RIGHT; }
    public SystemPosition home_position { get; set; default = BOTTOM_CENTER; }

    public GamepadType (string id, string name) {
        Object (id: id, name: name);
    }

    construct {
        button_names = {};
        button_icons = {};
        stick_names = {};

        foreach (var button in Gamepad.Button.all ()) {
            button_names += null;
            button_icons += null;
        }
        foreach (var stick in Gamepad.Stick.all ())
            stick_names += null;
    }

    public static void init_types () {
        if (xbox_360 != null)
            return;

        var type = new GamepadType ("xbox-360", _("XBox 360"));
        type.set_dpad (_("Directional Pad"));
        type.add_button (SOUTH,  _("A"),           "a");
        type.add_button (EAST,   _("B"),           "b");
        type.add_button (WEST,   _("X"),           "x");
        type.add_button (NORTH,  _("Y"),           "y");
        type.add_button (SELECT, _("Back"),        "xbox-back");
        type.add_button (START,  _("Start"),       "xbox-start");
        type.add_button (L,      _("LB"),          "lb");
        type.add_button (R,      _("RB"),          "rb");
        type.add_button (L2,     _("LT"),          "lt");
        type.add_button (R2,     _("RT"),          "rt");
        type.add_button (L3,     _("L"));
        type.add_button (R3,     _("R"));
        type.add_button (HOME,   _("XBox Guide"),  "home");
        type.add_stick (LEFT,    _("Left Stick"));
        type.add_stick (RIGHT,   _("Right Stick"));
        type.select_button = SOUTH;
        type.cancel_button = EAST;
        type.left_stick_above_dpad = true;
        xbox_360 = type;

        type = new GamepadType ("xbox-one", _("XBox One"));
        type.set_dpad (_("Directional Pad"));
        type.add_button (SOUTH,  _("A"),           "a");
        type.add_button (EAST,   _("B"),           "b");
        type.add_button (WEST,   _("X"),           "x");
        type.add_button (NORTH,  _("Y"),           "y");
        type.add_button (SELECT, _("View"),        "xbox-view");
        type.add_button (START,  _("Menu"),        "xbox-menu");
        type.add_button (L,      _("LB"),          "lb");
        type.add_button (R,      _("RB"),          "rb");
        type.add_button (L2,     _("LT"),          "lt");
        type.add_button (R2,     _("RT"),          "rt");
        type.add_button (L3,     _("LS"));
        type.add_button (R3,     _("RS"));
        type.add_button (HOME,   _("XBox"),        "home");
        type.add_stick (LEFT,    _("Left Stick"));
        type.add_stick (RIGHT,   _("Right Stick"));
        type.select_button = SOUTH;
        type.cancel_button = EAST;
        type.left_stick_above_dpad = true;
        xbox_one = type;

        type = new GamepadType ("8bitdo", _("8BitDo"));
        type.set_dpad (_("D-Pad"));
        type.add_button (EAST,   _("A"),           "a");
        type.add_button (SOUTH,  _("B"),           "b");
        type.add_button (NORTH,  _("X"),           "x");
        type.add_button (WEST,   _("Y"),           "y");
        type.add_button (SELECT, _("Select"),      "8bitdo-select");
        type.add_button (START,  _("Start"),       "8bitdo-start");
        type.add_button (L,      _("L"),           "l");
        type.add_button (R,      _("R"),           "r");
        type.add_button (L2,     _("L2"),          "l2");
        type.add_button (R2,     _("R2"),          "r2");
        type.add_button (L3,     _("L3"));
        type.add_button (R3,     _("R3"));
        type.add_button (L4,     _("P2"),          "p2");
        type.add_button (R4,     _("P1"),          "p1");
        type.add_button (HOME,   _("8BitDo"),      "home");
        type.add_stick (LEFT,    _("Left Stick"));
        type.add_stick (RIGHT,   _("Right Stick"));
        type.select_button = EAST;
        type.cancel_button = SOUTH;
        type.home_position = BOTTOM_RIGHT;
        eight_bitdo = type;

        type = new GamepadType ("steam-deck", _("Steam Deck"));
        type.set_dpad (_("Directional Pad"));
        type.add_button (EAST,   _("B"),              "b");
        type.add_button (SOUTH,  _("A"),              "a");
        type.add_button (NORTH,  _("Y"),              "y");
        type.add_button (WEST,   _("X"),              "x");
        type.add_button (SELECT, _("View"),           "steam-view");
        type.add_button (START,  _("Menu"),           "steam-menu");
        type.add_button (L,      _("L1"),             "l1");
        type.add_button (R,      _("R1"),             "r1");
        type.add_button (L2,     _("L2"),             "l2");
        type.add_button (R2,     _("R2"),             "r2");
        type.add_button (L3,     _("Left Joystick"));
        type.add_button (R3,     _("Right Joystick"));
        type.add_button (L4,     _("L4"),             "l4");
        type.add_button (R4,     _("R4"),             "r4");
        type.add_button (L5,     _("L5"),             "l5");
        type.add_button (R5,     _("R5"),             "r5");
        // Translators: Steam button on Steam Deck
        type.add_button (HOME,   _("Steam"),          "steam-steam");
        // Translators: QAM stands for Quick Access Menu, the ••• button on Steam Deck
        type.add_button (MISC1,  _("QAM"),            "steam-qam");
        type.add_stick (LEFT,    _("Left Joystick"));
        type.add_stick (RIGHT,   _("Right Joystick"));
        type.select_button = SOUTH;
        type.cancel_button = EAST;
        type.select_position = TOP_LEFT;
        type.start_position = TOP_RIGHT;
        type.home_position = BOTTOM_LEFT;
        steam_deck = type;

        type = new GamepadType ("switch-pro", _("Switch Pro Controller"));
        type.set_dpad (_("Control Pad"));
        type.add_button (EAST,   _("A"),           "a");
        type.add_button (SOUTH,  _("B"),           "b");
        type.add_button (NORTH,  _("X"),           "x");
        type.add_button (WEST,   _("Y"),           "y");
        type.add_button (SELECT, _("-"),           "minus");
        type.add_button (START,  _("+"),           "plus");
        type.add_button (L,      _("L"),           "l");
        type.add_button (R,      _("R"),           "r");
        type.add_button (L2,     _("ZL"),          "zl");
        type.add_button (R2,     _("ZR"),          "zr");
        type.add_button (L3,     _("Left Stick"));
        type.add_button (R3,     _("Right Stick"));
        type.add_button (HOME,   _("Home"),        "home");
        type.add_stick (LEFT,    _("Left Stick"));
        type.add_stick (RIGHT,   _("Right Stick"));
        type.left_stick_above_dpad = true;
        type.select_button = EAST;
        type.cancel_button = SOUTH;
        type.home_position = BOTTOM_RIGHT;
        switch_pro = type;

        type = new GamepadType ("dualshock4", _("DualShock 4"));
        type.set_dpad (_("Directional Pad"));
        type.add_button (EAST,   _("Circle"),      "circle");
        type.add_button (SOUTH,  _("Cross"),       "cross");
        type.add_button (NORTH,  _("Triangle"),    "triangle");
        type.add_button (WEST,   _("Square"),      "square");
        type.add_button (SELECT, _("Share"),       "ps4-share");
        type.add_button (START,  _("Options"),     "ps4-options");
        type.add_button (L,      _("L1"),          "l1");
        type.add_button (R,      _("R1"),          "r1");
        type.add_button (L2,     _("L2"),          "l2");
        type.add_button (R2,     _("R2"),          "r2");
        type.add_button (L3,     _("L3"));
        type.add_button (R3,     _("R3"));
        type.add_button (HOME,   _("PlayStation"), "home");
        type.add_stick (LEFT,    _("Left Stick"));
        type.add_stick (RIGHT,   _("Right Stick"));
        type.select_position = TOP_LEFT;
        type.start_position = TOP_RIGHT;

        // Translators: Up until PS5 PlayStation controllers used the ⭕ button
        // as select and the ❌ button as cancel in Japan, and the other way
        // around in the rest of the world. This string can be "circle" if the
        // ⭕ button should be used as select (Japan) or "cross" if ❌ is
        // select. Other values are unsupported.
        string playstation_select = _("cross");

        if (playstation_select == "cross") {
            type.select_button = SOUTH;
            type.cancel_button = EAST;
        } else if (playstation_select == "circle") {
            type.select_button = EAST;
            type.cancel_button = SOUTH;
        } else {
            error ("Unsupported PlayStation select button value");
        }

        dualshock4 = type;
    }

    public static ListStore list_types () {
        var store = new ListStore (typeof (GamepadType));

        store.append (xbox_360);
        store.append (xbox_one);
        store.append (eight_bitdo);
        store.append (steam_deck);
        store.append (switch_pro);
        store.append (dualshock4);

        return store;
    }

    public static GamepadType? from_id (string id) {
        var store = list_types ();
        for (uint i = 0; i < store.get_n_items (); i++) {
            var t = store.get_item (i) as GamepadType;

            if (t.id == id)
                return t;
        }

        return null;
    }

    protected void set_dpad (string dpad_name) {
        this.dpad_name = dpad_name;
        this.dpad_icon = "gamepad-dpad-symbolic";

        foreach (var dir in InputDirection.all ()) {
            var button = dir.get_button ();

            button_names[button] = "%s %s".printf (
                dpad_name, dir.get_display_name ()
            );
            button_icons[button] = @"gamepad-dpad-$dir-symbolic";
        }
    }

    protected void add_button (Gamepad.Button button, string name, string? icon_name = null) {
        button_names[button] = name;

        if (icon_name != null)
            button_icons[button] = @"gamepad-button-$icon_name-symbolic";
    }

    protected void add_stick (Gamepad.Stick stick, string name) {
        stick_names[stick] = name;

        button_icons[stick.get_button ()] = "gamepad-stick-press-symbolic";
    }

    public bool has_button (Gamepad.Button button) {
        return button_names[button] != null;
    }

    public bool has_stick (Gamepad.Stick stick) {
        return stick_names[stick] != null;
    }

    public bool has_dpad () {
        return dpad_name != null;
    }

    public string? get_button_name (Gamepad.Button button) {
        return button_names[button];
    }

    public string? get_button_icon_name (Gamepad.Button button) {
        return button_icons[button];
    }

    public string? get_dpad_icon () {
        return dpad_icon;
    }

    public string? get_dpad_name () {
        return dpad_name;
    }

    public string get_stick_name (Gamepad.Stick stick) {
        return stick_names[stick];
    }

    public string get_stick_icon (Gamepad.Stick stick) {
        return "gamepad-stick-symbolic";
    }

    public string get_stick_icon_for_direction (Gamepad.Stick stick, InputDirection dir) {
        return @"gamepad-stick-$dir-symbolic";
    }

    public static unowned GamepadType? detect (string name) {
        var lower = name.strip ().ascii_down ();

        if (lower.contains ("xbox")) {
            if (lower.contains ("360"))
                return xbox_360;

            if (lower.contains ("one"))
                return xbox_one;

            return xbox_360;
        }

        if (lower.contains ("8bitdo"))
            return eight_bitdo;

        if (lower.contains ("nintendo"))
            return switch_pro;

        if (lower.contains ("steam deck"))
            return steam_deck;

        if (lower.contains ("dualshock") ||
            lower.contains ("sony") ||
            lower.contains ("dualsense")) {
            return dualshock4;
        }

        return null;
   }

    public static unowned GamepadType get_default () {
        return xbox_360;
    }

    public static unowned GamepadType? get_for_device_type (Manette.DeviceType device_type) {
        switch (device_type) {
            case STEAM_DECK:
                return steam_deck;
            case GENERIC:
            default:
                return null;
        }
    }
}
