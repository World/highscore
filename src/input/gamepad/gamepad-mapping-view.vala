// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/input/gamepad/gamepad-mapping-view.ui")]
public class Highscore.GamepadMappingView : Adw.Bin {
    public Gamepad? gamepad { get; set; }
    public GamepadType gamepad_type { get; set; }

    [GtkChild]
    private unowned Adw.EntryRow name_row;
    [GtkChild]
    private unowned Adw.ComboRow type_row;
    [GtkChild]
    private unowned Adw.SwitchRow swap_select_cancel_row;
    [GtkChild]
    private unowned GamepadLayoutView layout_view;
    [GtkChild]
    private unowned Gtk.Label hint_label;
    [GtkChild]
    private unowned Gtk.Label connect_label;

    private GamepadMappingEditor? editor;

    private BindingGroup gamepad_bindings;

    private ulong device_cb_id;
    private ulong modified_cb_id;
    private ulong editor_modified_cb_id;

    public bool modified {
        get { return (editor != null && editor.modified) || gamepad.modified; }
    }

    public bool supports_mapping {
        get {
            if (gamepad != null)
                return gamepad.supports_mapping;

            return false;
        }
    }

    construct {
        type_row.model = GamepadType.list_types ();
        type_row.bind_property ("selected-item", this, "gamepad-type", SYNC_CREATE);

        gamepad_bindings = new BindingGroup ();

        gamepad_bindings.bind (
            "name", name_row,
            "text", SYNC_CREATE | BIDIRECTIONAL
        );
        gamepad_bindings.bind_property (
            "gamepad-type", type_row,
            "selected", SYNC_CREATE | BIDIRECTIONAL,
            (binding, from_value, ref to_value) => {
                var t = from_value.get_object () as GamepadType;

                uint n = type_row.model.get_n_items ();
                uint index = Gtk.INVALID_LIST_POSITION;

                for (uint i = 0; i < n; i++) {
                    var tt = type_row.model.get_item (i) as GamepadType;
                    if (tt == t) {
                        index = i;
                        break;
                    }
                }
                to_value = index;
                return true;
            },
            (binding, from_value, ref to_value) => {
                to_value.take_object (type_row.selected_item);
                return true;
            }
        );
        gamepad_bindings.bind (
            "swap-select-cancel", swap_select_cancel_row,
            "active", SYNC_CREATE | BIDIRECTIONAL
        );

        bind_property ("gamepad",      layout_view, "gamepad",      DEFAULT);
        bind_property ("gamepad-type", layout_view, "gamepad-type", SYNC_CREATE);

        notify["gamepad"].connect (() => {
            if (device_cb_id > 0) {
                gamepad.disconnect (device_cb_id);
                device_cb_id = 0;
            }

            if (modified_cb_id > 0) {
                gamepad.disconnect (modified_cb_id);
                modified_cb_id = 0;
            }

            gamepad_bindings.source = gamepad;

            if (gamepad != null) {
                device_cb_id = gamepad.notify["device"].connect (update_editor);

                modified_cb_id = gamepad.notify["modified"].connect (() => {
                    notify_property ("modified");
                });

                notify_property ("supports-mapping");
            }

            update_editor ();
        });

        notify["gamepad-type"].connect (() => {
            if (gamepad_type == null)
                return;

            gamepad.gamepad_type = gamepad_type;
        });

        layout_view.button_clicked.connect ((button, x, y) => {
            if (editor == null)
                return;

            show_popover (new GamepadMapButtonPopover (editor, gamepad_type, button), x, y);
        });

        layout_view.dpad_clicked.connect ((x, y) => {
            if (editor == null)
                return;

            show_popover (new GamepadMapDpadPopover (editor, gamepad_type), x, y);
        });

        layout_view.stick_clicked.connect ((stick, x, y) => {
            if (editor == null)
                return;

            show_popover (new GamepadMapStickPopover (editor, gamepad_type, stick), x, y);
        });
    }

    private void update_editor () {
        if (editor_modified_cb_id > 0) {
            editor.disconnect (editor_modified_cb_id);
            editor_modified_cb_id = 0;
        }

        if (gamepad != null && gamepad.device != null && supports_mapping)
            editor = new GamepadMappingEditor (gamepad);
        else
            editor = null;

        if (editor != null) {
            editor_modified_cb_id = editor.notify["modified"].connect (() => {
                notify_property ("modified");
            });
        }

        notify_property ("modified");

        layout_view.sensitive = (gamepad != null && gamepad.device != null);
        hint_label.visible = (editor != null);
        connect_label.visible = (editor == null);
    }

    private void show_popover (GamepadMapPopover popover, double x, double y) {
        popover.closed.connect (() => {
            popover.disconnect_gamepad ();
            popover.unparent ();
        });

        Graphene.Point point = {};
        assert (layout_view.compute_point (this, { (float) x,  (float) y }, out point));

        popover.set_parent (this);
        popover.set_pointing_to ({ (int) point.x, (int) point.y, 0, 0 });
        popover.popup ();
    }

    public void reset () {
        if (supports_mapping)
            editor.reset_mapping ();
        gamepad.reset_type ();
        gamepad.reset_name ();
    }
}
