// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadMapStickPopover : GamepadMapPopover {
    public Gamepad.Stick stick { get; construct; }

    private unowned Adw.ViewStack stack;

    public GamepadMapStickPopover (GamepadMappingEditor editor, GamepadType gamepad_type, Gamepad.Stick stick) {
        Object (editor: editor, gamepad_type: gamepad_type, stick: stick);
    }

    private void add_button_page (
        Gamepad.Button button,
        string icon_name,
        string tooltip,
        string label
    ) {
        var section = new GamepadMapButtonSection (
            editor, button, icon_name, label
        );

        var page = stack.add (section);
        page.title = label;
        page.icon_name = icon_name;

        section.save_and_close.connect (() => editor.save_mapping ());
    }

    private void add_axis_page (
        bool horiz,
        string icon_name,
        string tooltip,
        string label
    ) {
        var section = new GamepadMapStickSection (
            editor, stick, horiz, icon_name, label
        );

        var page = stack.add (section);
        page.title = label;
        page.icon_name = icon_name;

        section.save_and_close.connect (() => editor.save_mapping ());
    }

    construct {
        var box = new Gtk.Box (VERTICAL, 6);

        var stack = new Adw.ViewStack () {
            vhomogeneous = false,
            enable_transitions = true,
        };

        this.stack = stack;

        add_axis_page (
            true, "gamepad-map-horizontal-symbolic",
            _("Horizontal"), _("Move the stick horizontally")
        );

        add_axis_page (
            false, "gamepad-map-vertical-symbolic",
            _("Vertical"), ("Move the stick vertically")
        );

        add_button_page (
            stick.get_button (), "gamepad-map-button-symbolic",
            _("Press"), _("Press the stick")
        );

        var switcher = new Adw.InlineViewSwitcher () {
            stack = stack,
            display_mode = ICONS,
        };

        box.append (switcher);
        box.append (stack);

        child = box;
    }

    protected override void gamepad_event_cb (Manette.Event event) {
        var child = stack.visible_child;

        if (child is GamepadMapStickSection) {
            var section = child as GamepadMapStickSection;

            section.gamepad_event_cb (event);
        }

        if (child is GamepadMapButtonSection) {
            var section = child as GamepadMapButtonSection;

            section.gamepad_event_cb (event);
        }
    }
}
