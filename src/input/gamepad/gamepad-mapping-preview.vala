// This file is part of Highscore. License: GPL-3.0-or-later

public enum Highscore.GamepadMappingPreviewSectionType {
    TOP_LEFT,
    TOP_RIGHT,
    FACE_TOP_LEFT,
    FACE_TOP_RIGHT,
    FACE_BOTTOM_LEFT,
    FACE_BOTTOM_RIGHT,
    SYSTEM_TOP_LEFT,
    SYSTEM_TOP_RIGHT,
    SYSTEM_BOTTOM_LEFT,
    SYSTEM_BOTTOM_RIGHT,
    BACK_LEFT,
    BACK_RIGHT,
    POINTER,
}

public class Highscore.GamepadMappingPreviewSection : Gtk.Box {
    public Gamepad gamepad { get; set; }
    public GamepadType gamepad_type { get; set; }
    public PlatformControls controls { get; set; }
    public string controller_type { get; set; }

    public GamepadMappingPreviewSectionType section_type { get; set; }
    public Gtk.PackType align { get; set; default = START; }
    public bool autohide { get; set; default = true; }

    private Gtk.Label heading;

    private Gtk.Box[] control_widgets;

    private PlatformController? controller;
    private PlatformController? global_controller;

    private PlatformGamepadMapping? mapping;
    private PlatformGamepadMapping? global_mapping;

    private class LayerData {
        public PlatformGamepadMapping mapping;
        public bool active;

        public LayerData (PlatformGamepadMapping mapping) {
            this.mapping = mapping;
        }
    }

    private LayerData[] layers;

    construct {
        orientation = VERTICAL;
        spacing = 6;

        visible = false;

        control_widgets = {};

        heading = new Gtk.Label (null) {
            xalign = 0,
            ellipsize = END,
            visible = false,
        };
        heading.add_css_class ("title-4");
        append (heading);

        notify["gamepad"].connect (rebuild_ui);
        notify["gamepad-type"].connect (rebuild_ui);
        notify["controls"].connect (rebuild_ui);
        notify["controller-type"].connect (rebuild_ui);

        notify["section-type"].connect (rebuild_ui);
        notify["align"].connect (rebuild_ui);
        notify["autohide"].connect (rebuild_ui);
    }

    private string[] get_system_controls (GamepadType.SystemPosition pos) {
        GamepadType.SystemPosition? secondary_pos = null;

        string[] ret = {};

        if (pos == TOP_RIGHT)
            secondary_pos = TOP_CENTER;
        else if (pos == BOTTOM_RIGHT)
            secondary_pos = BOTTOM_CENTER;

        if (gamepad_type.select_position == pos || gamepad_type.select_position == secondary_pos)
            ret += Gamepad.Button.SELECT.get_id ();
        if (gamepad_type.start_position == pos || gamepad_type.start_position == secondary_pos)
            ret += Gamepad.Button.START.get_id ();
        if (gamepad_type.home_position == pos || gamepad_type.home_position == secondary_pos)
            ret += Gamepad.Button.HOME.get_id ();

        return ret;
    }

    private string[] get_stick_controls (Gamepad.Stick stick) {
        var id = stick.get_id ();

        return {
            id,
            @"$id:up",
            @"$id:left",
            @"$id:right",
            @"$id:down",
            stick.get_button ().get_id ()
        };
    }

    private string[] get_possible_controls () {
        switch (section_type) {
            case TOP_LEFT:
                return { Gamepad.Button.L2.get_id (), Gamepad.Button.L.get_id () };
            case TOP_RIGHT:
                return { Gamepad.Button.R2.get_id (), Gamepad.Button.R.get_id () };
            case SYSTEM_TOP_LEFT:
                return get_system_controls (TOP_LEFT);
            case SYSTEM_TOP_RIGHT:
                return get_system_controls (TOP_RIGHT);
            case SYSTEM_BOTTOM_LEFT:
                return get_system_controls (BOTTOM_LEFT);
            case SYSTEM_BOTTOM_RIGHT:
                return get_system_controls (BOTTOM_RIGHT);
            case BACK_LEFT:
                return { Gamepad.Button.L4.get_id (), Gamepad.Button.L5.get_id () };
            case BACK_RIGHT:
                return { Gamepad.Button.R4.get_id (), Gamepad.Button.R5.get_id () };
            case FACE_TOP_LEFT:
                if (gamepad_type.left_stick_above_dpad) {
                    return get_stick_controls (LEFT);
                } else {
                    return {
                        "dpad",
                        Gamepad.Button.UP.get_id (), Gamepad.Button.DOWN.get_id (),
                        Gamepad.Button.LEFT.get_id (), Gamepad.Button.RIGHT.get_id ()
                    };
                }
            case FACE_BOTTOM_LEFT:
                if (gamepad_type.left_stick_above_dpad) {
                    return {
                        "dpad",
                        Gamepad.Button.UP.get_id (), Gamepad.Button.DOWN.get_id (),
                        Gamepad.Button.LEFT.get_id (), Gamepad.Button.RIGHT.get_id ()
                    };
                } else {
                    return get_stick_controls (LEFT);
                }
            case FACE_TOP_RIGHT:
                if (gamepad_type.right_stick_above_buttons) {
                    return get_stick_controls (RIGHT);
                } else {
                    return {
                        Gamepad.Button.NORTH.get_id (), Gamepad.Button.WEST.get_id (),
                        Gamepad.Button.EAST.get_id (), Gamepad.Button.SOUTH.get_id ()
                    };
                }
            case FACE_BOTTOM_RIGHT:
                if (gamepad_type.right_stick_above_buttons) {
                    return {
                        Gamepad.Button.NORTH.get_id (), Gamepad.Button.WEST.get_id (),
                        Gamepad.Button.EAST.get_id (), Gamepad.Button.SOUTH.get_id ()
                    };
                } else {
                    return get_stick_controls (RIGHT);
                }
            case POINTER:
                return {};
            default:
                assert_not_reached ();
        }
    }

    private void rebuild_ui () {
        if (gamepad == null || controls == null)
            return;

        foreach (var control in control_widgets)
            remove (control);

        control_widgets = {};

        global_controller = controls.global_controller;
        if (controller_type != null)
            controller = controls.get_controller (controller_type);

        if (global_controller != null)
            global_mapping = global_controller.default_gamepad_mapping;
        else
            global_mapping = null;

        if (controller != null)
            mapping = controller.default_gamepad_mapping;
        else
            mapping = null;

        if (autohide)
            visible = false;

        switch (section_type) {
            case TOP_LEFT:
                try_add_button (L2);
                try_add_button (L);
                break;
            case TOP_RIGHT:
                try_add_button (R2);
                try_add_button (R);
                break;
            case SYSTEM_TOP_LEFT:
                try_add_system (TOP_LEFT);
                break;
            case SYSTEM_TOP_RIGHT:
                try_add_system (TOP_RIGHT);
                try_add_system (TOP_CENTER);
                break;
            case SYSTEM_BOTTOM_LEFT:
                try_add_system (BOTTOM_LEFT);
                break;
            case SYSTEM_BOTTOM_RIGHT:
                try_add_system (BOTTOM_RIGHT);
                try_add_system (BOTTOM_CENTER);
                break;
            case BACK_LEFT:
                try_add_button (L4);
                try_add_button (L5);
                break;
            case BACK_RIGHT:
                try_add_button (R4);
                try_add_button (R5);
                break;
            case FACE_TOP_LEFT:
                if (gamepad_type.left_stick_above_dpad)
                    try_add_stick (LEFT);
                else
                    try_add_dpad ();
                break;
            case FACE_BOTTOM_LEFT:
                if (gamepad_type.left_stick_above_dpad)
                    try_add_dpad ();
                else
                    try_add_stick (LEFT);
                break;
            case FACE_TOP_RIGHT:
                if (gamepad_type.right_stick_above_buttons)
                    try_add_stick (RIGHT);
                else
                    try_add_face_buttons ();
                break;
            case FACE_BOTTOM_RIGHT:
                if (gamepad_type.right_stick_above_buttons)
                    try_add_face_buttons ();
                else
                    try_add_stick (RIGHT);
                break;
            case POINTER:
                if (controller != null)
                    try_add_pointer (controller);
                if (global_controller != null)
                    try_add_pointer (global_controller);
                break;
        }

        var controls = get_possible_controls ();

        int max_n_controls = control_widgets.length;
        int max_from_layers = 0;

        foreach (var control in controls) {
            foreach (var layer in layers) {
                if (layer.mapping.has_mapping_for_control (control)) {
                    max_from_layers++;
                    break;
                }
            }
        }

        max_n_controls = int.max (max_n_controls, max_from_layers);

        for (int i = control_widgets.length; i < max_n_controls; i++)
            add_padding ();

        heading.xalign = (align == END) ? 1.0f : 0.0f;

        if (!autohide)
            visible = true;
    }

    private void add_control (string? icon_name, string label, string? control_name) {
        if (control_name == null)
            return;

        var box = new Gtk.Box (HORIZONTAL, 12);
        if (align == START)
            box.halign = START;
        else
            box.halign = END;

        var prefix = new Gtk.Image.from_icon_name (icon_name ?? "a");

        prefix.add_css_class (
            icon_name.replace ("gamepad-", "").replace ("-symbolic", "")
        );

        if (control_name == null)
            prefix.add_css_class ("dimmed");

        if (align == START)
            box.append (prefix);

        if (control_name != null) {
            var mapping_label = new IconLabel (control_name);
            mapping_label.add_css_class ("numeric");
            box.append (mapping_label);
        }

        if (align == END)
            box.append (prefix);

        append (box);
        control_widgets += box;

        visible = true;
    }

    private void add_padding () {
        var box = new Gtk.Box (HORIZONTAL, 12);
        if (align == START)
            box.halign = START;
        else
            box.halign = END;

        var prefix = new Gtk.Image.from_icon_name ("a");

        box.append (prefix);

        box.opacity = 0;

        append (box);
        control_widgets += box;

        visible = true;
    }

    private string? get_control_name (PlatformController controller, string id_with_qualifier) {
        var parts = id_with_qualifier.split (":");

        if (parts[0] == null)
            return null;

        var control = controller.get_control (parts[0]);
        if (control == null)
            return null;

        if (parts.length > 1)
            return control.get_name_for_qualifier (parts[1]);

        return control.name;
    }

    private void try_add_button (Gamepad.Button button, bool hide_when_missing = false) {
        if (!gamepad.has_button (button))
            return;

        string? mapped_name = null;

        if (button == HOME) {
            mapped_name = _("Open Menu");
        } else {
            foreach (var layer in layers) {
                if (!layer.active)
                    continue;

                string? id = layer.mapping.get_mapping_for_button (button);
                if (id != null) {
                    mapped_name = get_control_name (controller, id);
                    break;
                }
            }

            if (mapped_name == null && mapping != null) {
                string? id = mapping.get_mapping_for_button (button);
                if (id != null)
                    mapped_name = get_control_name (controller, id);

                if (mapped_name == null)
                    mapped_name = mapping.get_layer_name_for_button (button);
            }

            if (mapped_name == null && global_mapping != null) {
                string? id = global_mapping.get_mapping_for_button (button);
                if (id != null)
                    mapped_name = get_control_name (global_controller, id);

                if (mapped_name == null)
                    mapped_name = global_mapping.get_layer_name_for_button (button);
            }
        }

        var icon_name = gamepad_type.get_button_icon_name (button);
        var name = gamepad_type.get_button_name (button);

        if (mapped_name == null && hide_when_missing)
            return;

        add_control (icon_name, name, mapped_name);
    }

    private void try_add_system (GamepadType.SystemPosition pos) {
        if (gamepad_type.select_position == pos)
            try_add_button (SELECT);
        if (gamepad_type.start_position == pos)
            try_add_button (START);
        if (gamepad_type.home_position == pos)
            try_add_button (HOME);
    }

    private void try_add_dpad () {
        if (!gamepad.has_dpad ())
            return;

        heading.label = gamepad_type.get_dpad_name ();
        heading.visible = true;

        var dpad_icon = gamepad_type.get_dpad_icon ();
        var dpad_name = gamepad_type.get_dpad_name ();

        foreach (var layer in layers) {
            if (!layer.active)
                continue;

            string? id = layer.mapping.get_mapping_for_dpad ();
            if (id != null) {
                var mapped_name = get_control_name (controller, id);
                add_control (dpad_icon, dpad_name, mapped_name);
                return;
            }
        }

        if (mapping != null) {
            string? id = mapping.get_mapping_for_dpad ();
            if (id != null) {
                var mapped_name = get_control_name (controller, id);
                add_control (dpad_icon, dpad_name, mapped_name);
                return;
            }
        }

        if (global_mapping != null) {
            string? id = global_mapping.get_mapping_for_dpad ();
            if (id != null) {
                var mapped_name = get_control_name (global_controller, id);
                add_control (dpad_icon, dpad_name, mapped_name);
                return;
            }
        }

        try_add_button (UP);
        try_add_button (LEFT);
        try_add_button (RIGHT);
        try_add_button (DOWN);
    }

    private void try_add_face_buttons () {
        heading.label = _("Face Buttons");
        heading.visible = true;

        try_add_button (NORTH);
        try_add_button (WEST);
        try_add_button (EAST);
        try_add_button (SOUTH);
    }

    private void try_add_stick (Gamepad.Stick stick) {
        heading.label = gamepad_type.get_stick_name (stick);
        heading.visible = true;

        bool found_stick = false;

        foreach (var layer in layers) {
            if (!layer.active)
                continue;

            string? id = layer.mapping.get_mapping_for_stick (stick);
            if (id != null) {
                try_add_stick_for_controller (stick, id, controller);
                found_stick = true;
                break;
            }
        }

        if (!found_stick && mapping != null) {
            string? id = mapping.get_mapping_for_stick (stick);
            if (id != null) {
                try_add_stick_for_controller (stick, id, controller);
                found_stick = true;
            }
        }

        if (!found_stick && global_mapping != null) {
            string? id = global_mapping.get_mapping_for_stick (stick);
            if (id != null) {
                try_add_stick_for_controller (stick, id, controller);
                found_stick = true;
            }
        }

        try_add_button (stick.get_button (), true);
    }

    private void try_add_stick_for_controller (Gamepad.Stick stick, string id, PlatformController controller) {
        var id_parts = id.split ("|");

        if (id_parts.length == 1) {
            var mapped_name = get_control_name (controller, id);
            if (mapped_name == null)
                return;

            var icon_name = gamepad_type.get_stick_icon (stick);
            var name = gamepad_type.get_stick_name (stick);

            add_control (icon_name, name, mapped_name);
        }

        if (id_parts.length == InputDirection.N_DIRECTIONS) {
            InputDirection[] dirs = { UP, LEFT, RIGHT, DOWN };

            foreach (var dir in dirs) {
                var mapped_name = get_control_name (controller, id_parts[dir]);
                if (mapped_name == null)
                    continue;

                var icon_name = gamepad_type.get_stick_icon_for_direction (stick, dir);
                var name = dir.get_display_name ();

                add_control (icon_name, name, mapped_name);
            }
        }
    }

    private void try_add_pointer (PlatformController controller) {
        heading.label = _("Pointer");
        heading.visible = true;

        var screens = controller.list_screens ();
        foreach (int screen_id in screens) {
            var control = controller.get_for_screen (screen_id);
            if (control == null)
                continue;

            add_control ("pointer-input-symbolic", _("Pointer"), control.name);
        }
    }

    public void set_layers (PlatformGamepadMapping[] layers, int[] active_layers) {
        this.layers = {};

        for (int i = 0; i < layers.length; i++) {
            var data = new LayerData (layers[i]);
            this.layers += data;
        }

        foreach (var i in active_layers)
            this.layers[i].active = true;

        rebuild_ui ();
    }

    public void set_layer_active (int i, bool active) {
        layers[i].active = active;

        rebuild_ui ();
    }
}

[GtkTemplate (ui = "/app/drey/Highscore/input/gamepad/gamepad-mapping-preview.ui")]
public class Highscore.GamepadMappingPreview : Adw.Bin {
    public Game game { get; construct; }
    public Gamepad gamepad { get; construct; }
    public string controller_type { get; set; }

    [GtkChild]
    private unowned GamepadLayoutView gamepad_view;

    [GtkChild]
    private unowned GamepadMappingPreviewSection top_left_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection top_right_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection face_top_left_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection face_top_right_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection face_bottom_left_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection face_bottom_right_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection system_top_left_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection system_top_right_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection system_bottom_left_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection system_bottom_right_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection back_left_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection back_right_section;
    [GtkChild]
    private unowned GamepadMappingPreviewSection pointer_section;

    private PlatformControls controls;
    private GamepadMappingPreviewSection[] sections;

    private class LayerData {
        public PlatformGamepadMapping mapping;
        public Gamepad.Button button;
        public bool active;

        public LayerData (PlatformGamepadMapping mapping, Gamepad.Button button) {
            this.mapping = mapping;
            this.button = button;
        }
    }

    private LayerData[] layers;

    public GamepadMappingPreview (Game game, Gamepad gamepad) {
        Object (game: game, gamepad: gamepad);
    }

    construct {
        sections = {};
        sections += top_left_section;
        sections += top_right_section;
        sections += face_top_left_section;
        sections += face_top_right_section;
        sections += face_bottom_left_section;
        sections += face_bottom_right_section;
        sections += system_top_left_section;
        sections += system_top_right_section;
        sections += system_bottom_left_section;
        sections += system_bottom_right_section;
        sections += back_left_section;
        sections += back_right_section;
        sections += pointer_section;

        gamepad_view.gamepad = gamepad;

        gamepad.bind_property (
            "gamepad-type", gamepad_view, "gamepad-type", SYNC_CREATE
        );

        controls = PlatformControls.create (game.platform);
        controller_type = controls.default_controller;

        foreach (var section in sections) {
            section.gamepad = gamepad;

            gamepad.bind_property (
                "gamepad-type", section, "gamepad-type", SYNC_CREATE
            );
        }

        notify["controller-type"].connect (() => {
            update_layers ();
            update_ui ();
        });

        gamepad.button_pressed.connect (button_pressed_cb);
        gamepad.button_released.connect (button_released_cb);

        update_layers ();
        update_ui ();
    }

    ~GamepadMappingPreview () {
        gamepad.button_pressed.disconnect (button_pressed_cb);
        gamepad.button_released.disconnect (button_released_cb);
    }

    static construct {
        set_css_name ("gamepad-mapping-preview");
    }

    private void update_layers () {
        layers = {};

        var global_controller = controls.global_controller;
        PlatformController controller = null;
        if (controller_type != null)
            controller = controls.get_controller (controller_type);

        PlatformGamepadMapping? mapping, global_mapping;

        if (global_controller != null)
            global_mapping = global_controller.default_gamepad_mapping;
        else
            global_mapping = null;

        if (controller != null)
            mapping = controller.default_gamepad_mapping;
        else
            mapping = null;

        PlatformGamepadMapping[] layer_mappings = {};
        int[] active_layers = {};

        if (mapping != null) {
            foreach (var l in mapping.list_layers ()) {
                var layer_data = new LayerData (l.mapping, l.button);

                layer_data.active = gamepad.get_button_pressed (l.button);

                if (layer_data.active)
                    active_layers += layers.length;

                layers += layer_data;
                layer_mappings += l.mapping;
            }
        }

        if (global_mapping != null) {
            foreach (var l in global_mapping.list_layers ()) {
                var layer_data = new LayerData (l.mapping, l.button);

                layer_data.active = gamepad.get_button_pressed (l.button);

                if (layer_data.active)
                    active_layers += layers.length;

                layers += layer_data;
                layer_mappings += l.mapping;
            }
        }

        foreach (var section in sections)
            section.set_layers (layer_mappings, active_layers);
    }

    private void update_ui () {
        update_dimming ();

        foreach (var section in sections) {
            section.controls = controls;
            section.controller_type = controller_type;
        }
    }

    private void update_dimming () {
        if (gamepad == null || controls == null)
            return;

        var global_controller = controls.global_controller;
        PlatformController controller = null;
        if (controller_type != null)
            controller = controls.get_controller (controller_type);

        PlatformGamepadMapping? mapping, global_mapping;

        if (global_controller != null)
            global_mapping = global_controller.default_gamepad_mapping;
        else
            global_mapping = null;

        if (controller != null)
            mapping = controller.default_gamepad_mapping;
        else
            mapping = null;

        string[] dimmed_controls = {};

        {
            bool found_dpad = false;

            if (mapping != null &&
                (mapping.get_mapping_for_dpad () != null ||
                 mapping.get_mapping_for_button (UP) != null ||
                 mapping.get_mapping_for_button (DOWN) != null ||
                 mapping.get_mapping_for_button (LEFT) != null ||
                 mapping.get_mapping_for_button (RIGHT) != null)) {
                found_dpad = true;
            }

            if (!found_dpad && global_mapping != null &&
                (global_mapping.get_mapping_for_dpad () != null ||
                 global_mapping.get_mapping_for_button (UP) != null ||
                 global_mapping.get_mapping_for_button (DOWN) != null ||
                 global_mapping.get_mapping_for_button (LEFT) != null ||
                 global_mapping.get_mapping_for_button (RIGHT) != null)) {
                found_dpad = true;
            }

            if (!found_dpad) {
                for (int i = layers.length - 1; i >= 0; i--) {
                    var layer = layers[i];

                    if (!layer.active)
                        continue;

                    if (layer.mapping.get_mapping_for_dpad () != null ||
                        layer.mapping.get_mapping_for_button (UP) != null ||
                        layer.mapping.get_mapping_for_button (DOWN) != null ||
                        layer.mapping.get_mapping_for_button (LEFT) != null ||
                        layer.mapping.get_mapping_for_button (RIGHT) != null) {
                        found_dpad = true;
                        break;
                    }
                }
            }

            if (!found_dpad)
                dimmed_controls += "dpad";
        }

        foreach (var button in Gamepad.Button.all ()) {
            if (button == UP || button == DOWN || button == LEFT || button == RIGHT)
                continue;

            if (button == HOME)
                continue;

            if (mapping != null) {
                if (mapping.get_mapping_for_button (button) != null)
                    continue;

                if (mapping.get_layer_for_button (button) != null)
                    continue;
            }

            if (global_mapping != null) {
                if (global_mapping.get_mapping_for_button (button) != null)
                    continue;

                if (global_mapping.get_layer_for_button (button) != null)
                    continue;
            }

            bool found_button = false;

            for (int i = layers.length - 1; i >= 0; i--) {
                var layer = layers[i];

                if (!layer.active)
                    continue;

                if (layer.mapping.get_mapping_for_button (button) != null) {
                    found_button = true;
                    break;
                }
            }

            if (found_button)
                continue;

            dimmed_controls += button.get_id ();
        }

        foreach (var stick in Gamepad.Stick.all ()) {
            if (mapping != null && mapping.get_mapping_for_stick (stick) != null)
                continue;

            if (global_mapping != null && global_mapping.get_mapping_for_stick (stick) != null)
                continue;

            bool found_stick = false;

            for (int i = layers.length - 1; i >= 0; i--) {
                var layer = layers[i];

                if (!layer.active)
                    continue;

                if (layer.mapping.get_mapping_for_stick (stick) != null) {
                    found_stick = true;
                    break;
                }
            }

            if (found_stick)
                continue;

            dimmed_controls += stick.get_id ();
        }

        gamepad_view.set_dimmed_controls (dimmed_controls);
    }

    private LayerData? find_layer_for_button (Gamepad.Button button, out int index) {
        for (int i = 0; i < layers.length; i++) {
            var layer = layers[i];
            if (layer.button == button) {
                index = i;
                return layer;
            }
        }

        index = -1;
        return null;
    }

    private void button_pressed_cb (Gamepad.Button button) {
        int index;
        var layer = find_layer_for_button (button, out index);
        if (layer == null)
            return;

        layer.active = true;

        update_dimming ();

        foreach (var section in sections)
            section.set_layer_active (index, true);
    }

    private void button_released_cb (Gamepad.Button button) {
        int index;
        var layer = find_layer_for_button (button, out index);
        if (layer == null)
            return;

        layer.active = false;

        update_dimming ();

        foreach (var section in sections)
            section.set_layer_active (index, false);
    }
}
