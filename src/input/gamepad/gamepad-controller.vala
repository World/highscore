// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadController : Object {
    private const int REPEAT_DELAY = 200;
    private const int REPEAT_INTERVAL_SLOW = 250;
    private const int REPEAT_INTERVAL_FAST = 50;
    private const double STICK_DEADZONE = 0.3;

    public signal void buttons_changed ();

    public signal void navigate (InputDirection direction);
    public signal void select ();
    public signal void cancel ();
    public signal void home ();
    public signal void left_page ();
    public signal void right_page ();

    public Gtk.Widget widget { get; construct; }

    public string select_label { get; set; default = ""; }
    public string cancel_label { get; set; default = ""; }

    public bool enabled { get; set; default = true; }
    public bool disable_when_unfocused { get; set; default = true; }

    public Gamepad? main_gamepad { get; private set; }
    public GamepadType? main_gamepad_type { get; private set; }

    private ulong gamepad_type_id;
    private ulong swapped_id;

    private uint repeat_delay_cb_id;
    private uint repeat_cb_id;

    private enum Button {
        SELECT,
        CANCEL,
        HOME,
        L,
        R
    }

    private const int N_BUTTONS = 3;

    private class PlayerData {
        public uint index;
        public Gamepad gamepad;
        public bool dpad_pressed[InputDirection.N_DIRECTIONS];
        public bool button_pressed[N_BUTTONS];
        public bool stick_active;
        public double stick_x;
        public double stick_y;
    }

    private PlayerData player_data[ControllerManager.N_PLAYERS];

    private bool input_enabled;
    private ControllerManager manager;

    public GamepadController (Gtk.Widget widget) {
        Object (widget: widget);
    }

    construct {
        manager = ControllerManager.get_instance ();

        manager.controller_changed.connect (controller_changed_cb);

        for (uint i = 0; i < player_data.length; i++) {
            player_data[i] = new PlayerData ();
            player_data[i].index = i;

            var gamepad = manager.get_gamepad (i);

            if (gamepad != null)
                set_gamepad (i, gamepad);
        }

        update_main_gamepad ();

        input_enabled = widget.get_mapped ();

        widget.map.connect (check_input_enabled);
        widget.unmap.connect (check_input_enabled);

        widget.realize.connect (realize_cb);
        widget.unrealize.connect (unrealize_cb);

        notify["enabled"].connect (check_input_enabled);
        notify["main-gamepad-type"].connect (() => buttons_changed ());

        if (widget.get_realized ())
            realize_cb ();

        check_input_enabled ();
    }

    ~GamepadController () {
        widget.map.disconnect (check_input_enabled);
        widget.unmap.disconnect (check_input_enabled);

        widget.realize.disconnect (realize_cb);
        widget.unrealize.disconnect (unrealize_cb);

        manager.controller_changed.connect (controller_changed_cb);
    }

    protected override void dispose () {
        if (gamepad_type_id > 0) {
            main_gamepad.disconnect (gamepad_type_id);
            gamepad_type_id = 0;
        }

        if (swapped_id > 0) {
            main_gamepad.disconnect (swapped_id);
            swapped_id = 0;
        }

        base.dispose ();
    }

    private PlayerData find_player_data (Gamepad gamepad) {
        for (uint i = 0; i < player_data.length; i++) {
            if (player_data[i].gamepad == gamepad)
                return player_data[i];
        }

        assert_not_reached ();
    }

    private bool get_button_pressed (Button button) {
        foreach (var data in player_data) {
            if (data.gamepad == null)
                continue;

            if (data.button_pressed[button])
                return true;
        }

        return false;
    }

    private void button_pressed_cb (Gamepad gamepad, Gamepad.Button button) {
        if (!input_enabled)
            return;

        foreach (var dir in InputDirection.all ()) {
            if (button == dir.get_button ()) {
                var data = find_player_data (gamepad);

                if (data.dpad_pressed[dir])
                    return;

                navigate (dir);
                start_repeat_delay ();
                data.dpad_pressed[dir] = true;
                break;
            }
        }

        if (button == gamepad.select_button) {
            var data = find_player_data (gamepad);
            data.button_pressed[Button.SELECT] = true;
            return;
        }

        if (button == gamepad.cancel_button) {
            var data = find_player_data (gamepad);
            data.button_pressed[Button.CANCEL] = true;
            return;
        }

        if (button == HOME) {
            var data = find_player_data (gamepad);
            data.button_pressed[Button.HOME] = true;
            return;
        }

        if (button == L) {
            var data = find_player_data (gamepad);
            if (!get_button_pressed (L))
                left_page ();
            data.button_pressed[Button.L] = true;
            return;
        }

        if (button == R) {
            var data = find_player_data (gamepad);
            if (!get_button_pressed (R))
                right_page ();
            data.button_pressed[Button.R] = true;
            return;
        }
    }

    private void button_released_cb (Gamepad gamepad, Gamepad.Button button) {
        if (!input_enabled)
            return;

        foreach (var dir in InputDirection.all ()) {
            if (button == dir.get_button ()) {
                var data = find_player_data (gamepad);

                if (!data.dpad_pressed[dir])
                    return;

                data.dpad_pressed[dir] = false;
                break;
            }
        }

        if (button == gamepad.select_button) {
            var data = find_player_data (gamepad);
            data.button_pressed[Button.SELECT] = false;

            if (!get_button_pressed (SELECT))
                select ();
            return;
        }

        if (button == gamepad.cancel_button) {
            var data = find_player_data (gamepad);
            data.button_pressed[Button.CANCEL] = false;

            if (!get_button_pressed (CANCEL))
                cancel ();
            return;
        }

        if (button == HOME) {
            var data = find_player_data (gamepad);
            data.button_pressed[Button.HOME] = false;

            if (!get_button_pressed (HOME))
                home ();
            return;
        }

        if (button == L) {
            var data = find_player_data (gamepad);
            data.button_pressed[Button.L] = false;
            return;
        }

        if (button == R) {
            var data = find_player_data (gamepad);
            data.button_pressed[Button.R] = false;
            return;
        }

        if (repeat_delay_cb_id > 0) {
            Source.remove (repeat_delay_cb_id);
            repeat_delay_cb_id = 0;
        }
    }

    private void stick_moved_cb (Gamepad gamepad, Gamepad.Stick stick, double x, double y) {
        if (!input_enabled)
            return;

        var data = find_player_data (gamepad);

        double distance = Math.sqrt (x * x + y * y);
        if (distance < STICK_DEADZONE) {
            data.stick_x = data.stick_y = 0;
            data.stick_active = false;
            return;
        }

        data.stick_x = x;
        data.stick_y = y;
        data.stick_active = true;

        start_repeat ();
    }

    private void start_repeat_delay () {
        if (repeat_cb_id > 0)
            return;

        if (repeat_delay_cb_id > 0)
            Source.remove (repeat_delay_cb_id);

        repeat_delay_cb_id = Timeout.add_once (REPEAT_DELAY, () => {
            repeat_delay_cb_id = 0;
            start_repeat ();
        });
    }

    private void start_repeat () {
        if (repeat_cb_id > 0)
            return;

        repeat_cb ();
    }

    private void repeat_cb () {
        int duration = REPEAT_INTERVAL_FAST;

        bool dpad_pressed[InputDirection.N_DIRECTIONS];
        double stick_x = 0, stick_y = 0;
        int stick_gamepads = 0;

        foreach (var data in player_data) {
            if (data.gamepad == null)
                continue;

            foreach (var dir in InputDirection.all ()) {
                if (data.dpad_pressed[dir])
                    dpad_pressed[dir] = true;
            }

            if (data.stick_active) {
                stick_x += data.stick_x;
                stick_y += data.stick_y;
                stick_gamepads++;
            }
        }

        stick_x /= stick_gamepads;
        stick_y /= stick_gamepads;

        double distance = Math.sqrt (stick_x * stick_x + stick_y * stick_y);
        double angle = Math.atan2 (stick_y, stick_x);
        InputDirection? stick_direction = null;

        foreach (var dir in InputDirection.all ()) {
            if (AnalogUtils.is_pointing_at_direction (angle, 0, dir)) {
                stick_direction = dir;
                break;
            }
        }

        // Normalize to [0-1]
        double stick_distance = (distance - STICK_DEADZONE) / (1 - STICK_DEADZONE);

        if (stick_direction != null) {
            navigate (stick_direction);

            double intensity = stick_distance.clamp (0, 1);

            duration = (int) Math.round (Adw.lerp (
                REPEAT_INTERVAL_SLOW, REPEAT_INTERVAL_FAST, intensity
            ));
        } else {
            int n_directions = 0;

            foreach (var dir in InputDirection.all ()) {
                if (dpad_pressed[dir] || dir == stick_direction) {
                    navigate (dir);
                    n_directions++;
                }
            }

            if (n_directions == 0) {
                repeat_cb_id = 0;
                return;
            }
        }

        repeat_cb_id = Timeout.add_once (duration, repeat_cb);
    }

    private void realize_cb () {
        var root = widget.root;

        if (root is Gtk.Window) {
            root.notify["is-active"].connect (check_input_enabled);
            root.notify["visible-dialog"].connect (check_input_enabled);
        }
    }

    private void unrealize_cb () {
        var root = widget.root;

        if (root is Gtk.Window) {
            root.notify["is-active"].disconnect (check_input_enabled);
            root.notify["visible-dialog"].disconnect (check_input_enabled);
        }
    }

    private void check_input_enabled () {
        bool mapped = widget.get_mapped ();
        bool active = false;
        Adw.Dialog visible_dialog = null;

        var root = widget.root;
        if (root is Gtk.Window) {
            var window = root as Gtk.Window;

            visible_dialog = WidgetUtils.get_visible_dialog (window);
            active = window.is_active;
        }

        if (disable_when_unfocused)
            input_enabled = enabled && mapped && active && visible_dialog == null;
        else
            input_enabled = enabled && mapped;

        if (input_enabled)
            return;

        foreach (var data in player_data) {
            foreach (var dir in InputDirection.all ())
                data.dpad_pressed[dir] = false;

            for (int i = 0; i < N_BUTTONS; i++)
                data.button_pressed[i] = false;

            data.stick_x = data.stick_y = 0;
        }

        if (repeat_delay_cb_id > 0) {
            Source.remove (repeat_delay_cb_id);
            repeat_delay_cb_id = 0;
        }

        if (repeat_cb_id > 0) {
            Source.remove (repeat_cb_id);
            repeat_cb_id = 0;
        }
    }

    private void controller_changed_cb (uint p) {
        set_gamepad (p, manager.get_gamepad (p));

        update_main_gamepad ();
    }

    private void set_gamepad (uint p, Gamepad? gamepad) {
        var prev_gamepad = player_data[p].gamepad;
        if (prev_gamepad != null) {
            prev_gamepad.button_pressed.disconnect (button_pressed_cb);
            prev_gamepad.button_released.disconnect (button_released_cb);
            prev_gamepad.stick_moved.disconnect (stick_moved_cb);

            for (int i = 0; i < N_BUTTONS; i++)
                player_data[p].button_pressed[i] = false;
        }

        player_data[p].gamepad = gamepad;

        if (gamepad != null) {
            gamepad.button_pressed.connect (button_pressed_cb);
            gamepad.button_released.connect (button_released_cb);
            gamepad.stick_moved.connect (stick_moved_cb);
        }
    }

    private void update_main_gamepad () {
        Gamepad? new_main_gamepad = null;

        for (int i = 0; i < player_data.length; i++) {
            if (player_data[i].gamepad != null) {
                new_main_gamepad = player_data[i].gamepad;
                break;
            }
        }

        if (main_gamepad != null) {
            main_gamepad.disconnect (gamepad_type_id);
            gamepad_type_id = 0;

            main_gamepad.disconnect (swapped_id);
            swapped_id = 0;
        }

        main_gamepad = new_main_gamepad;

        if (main_gamepad != null) {
            gamepad_type_id = main_gamepad.notify["gamepad-type"].connect (() => {
                main_gamepad_type = main_gamepad.gamepad_type;
            });

            swapped_id = main_gamepad.notify["swap-select-cancel"].connect (() => {
                buttons_changed ();
            });

            main_gamepad_type = main_gamepad.gamepad_type;
        } else {
            main_gamepad_type = null;
        }

        if (main_gamepad != null)
            widget.add_css_class ("gamepad-available");
        else
            widget.remove_css_class ("gamepad-available");
    }

    public string get_select_icon_name () {
        if (main_gamepad_type == null)
            return "";

        return main_gamepad_type.get_button_icon_name (main_gamepad.select_button);
    }

    public string get_cancel_icon_name () {
        if (main_gamepad_type == null)
            return "";

        return main_gamepad_type.get_button_icon_name (main_gamepad.cancel_button);
    }
}
