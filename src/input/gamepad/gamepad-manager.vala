// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadManager : Object {
    private static GamepadManager instance;

    private Manette.Monitor monitor;

    private HashTable<Manette.Device, Gamepad> connected_gamepads;
    private Settings settings;

    public ListStore gamepads { get; private set; }

    public signal void gamepad_added (Gamepad gamepad);
    public signal void gamepad_forgotten (Gamepad gamepad, uint index);

    public static GamepadManager get_instance () {
        if (instance == null)
            instance = new GamepadManager ();

        return instance;
    }

    construct {
        monitor = new Manette.Monitor ();

        gamepads = new ListStore (typeof (Gamepad));
        connected_gamepads = new HashTable<Manette.Device, Gamepad> (direct_hash, direct_equal);

        settings = new Settings ("app.drey.Highscore");

        var prev_gamepads = settings.get_strv ("gamepads");
        foreach (var guid in prev_gamepads)
            gamepads.append (new Gamepad (guid));

        var iterator = monitor.iterate ();
        Manette.Device device = null;

        while (iterator.next (out device))
            add_gamepad (device);

        monitor.device_connected.connect (add_gamepad);
        monitor.device_disconnected.connect (remove_gamepad);

        gamepads.items_changed.connect (save_gamepads);

        save_gamepads ();
    }

    private void save_gamepads () {
        string[] guids = {};
        uint n = gamepads.n_items;

        for (uint i = 0; i < n; i++) {
            var gamepad = gamepads.get_item (i) as Gamepad;
            guids += gamepad.guid;
        }

        settings.set_strv ("gamepads", guids);
    }

    private void add_gamepad (Manette.Device device) {
        uint index;
        var gamepad = new Gamepad (device.get_guid ());

        bool found = gamepads.find_with_equal_func (gamepad, (a, b) => {
            return ((Gamepad) a).guid == ((Gamepad) b).guid;
        }, out index);

        if (found)
            gamepad = gamepads.get_item (index) as Gamepad;
        else {
            gamepads.append (gamepad);
            gamepad_added (gamepad);
        }

        gamepad.connect_device (device);

        connected_gamepads[device] = gamepad;
    }

    public Gamepad? find_gamepad (string guid) {
        uint n = gamepads.n_items;

        for (uint i = 0; i < n; i++) {
            var g = gamepads.get_item (i) as Gamepad;

            if (g.guid == guid)
                return g;
        }

        return null;
    }

    private void remove_gamepad (Manette.Device device) {
        var gamepad = connected_gamepads[device];

        gamepad.disconnect_device ();

        connected_gamepads.remove (device);
    }

    public void forget_gamepad (Gamepad gamepad) requires (!gamepad.connected) {
        uint index;
        assert (gamepads.find (gamepad, out index));

        gamepads.remove (index);

        gamepad_forgotten (gamepad, index);
    }
}
