// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadLayoutView : Gtk.Widget {
    private const double PRESSED_BUTTON_SCALE = 0.9;
    private const float DPAD_TILT_ANGLE = 10;

    public signal void button_clicked (Gamepad.Button button, int x, int y);
    public signal void dpad_clicked (int x, int y);
    public signal void stick_clicked (Gamepad.Stick stick, int x, int y);

    private Rsvg.Handle? handle;
    private Rsvg.Rectangle viewport;
    private double global_scale;

    private string[] dimmed_controls;

    public bool show_hidden { get; set; }

    private bool _show_input;
    public bool show_input {
        get { return _show_input; }
        set {
            if (gamepad != null && show_input)
                unregister_input ();

            _show_input = value;

            if (gamepad != null && show_input)
                register_input ();

            queue_draw ();
        }
    }

    private Gamepad? _gamepad;
    public Gamepad gamepad {
        get { return _gamepad; }
        set {
            if (gamepad != null && show_input)
                unregister_input ();

            _gamepad = value;

            if (gamepad != null && show_input)
                register_input ();
        }
    }

    public GamepadType gamepad_type { get; set; }

    private Adw.TimedAnimation[] button_animations;

    construct {
        notify["gamepad-type"].connect (reload_image);

        button_animations = {};

        foreach (var button in Gamepad.Button.all ()) {
            var target = new Adw.CallbackAnimationTarget (queue_draw);

            button_animations += new Adw.TimedAnimation (
                this, 0, 1, 100, target
            ) {
                easing = EASE_OUT,
            };
        }

        var gesture = new Gtk.GestureClick ();
        gesture.released.connect (click_released_cb);
        add_controller (gesture);

        notify["show-hidden"].connect (queue_resize);
    }

    static construct {
        set_css_name ("gamepad-layout-view");
    }

    ~GamepadLayoutView () {
        if (gamepad != null && show_input)
            unregister_input ();
    }

    protected override Gtk.SizeRequestMode get_request_mode () {
        return HEIGHT_FOR_WIDTH;
    }

    private inline void union (ref Rsvg.Rectangle? rect1, Rsvg.Rectangle? rect2) {
        if (rect2 == null)
            return;

        if (rect1 == null) {
            rect1 = rect2;
            return;
        }

        double right1 = rect1.x + rect1.width;
        double right2 = rect2.x + rect2.width;
        double bottom1 = rect1.y + rect1.height;
        double bottom2 = rect2.y + rect2.height;

        rect1.x = double.min (rect1.x, rect2.x);
        rect1.y = double.min (rect1.y, rect2.y);
        rect1.width = double.max (right1, right2) - rect1.x;
        rect1.height = double.max (bottom1, bottom2) - rect1.y;
    }

    private Rsvg.Rectangle? get_element_logical_bounds (string element, Rsvg.Rectangle viewport) {
        Rsvg.Rectangle? ink_rect = null;
        var id = @"#$element";

        try {
            handle.get_geometry_for_layer (id, viewport, null, out ink_rect);
        } catch (Error e) {
            return null;
        }

        return ink_rect;
    }

    private Rsvg.Rectangle get_real_bounds (Rsvg.Rectangle viewport) {
        Rsvg.Rectangle? ret = null;

        if (show_hidden || gamepad.has_dpad ()) {
            union (ref ret, get_element_logical_bounds ("dpad", viewport));
            union (ref ret, get_element_logical_bounds ("dpad-base", viewport));
        }

        foreach (var button in Gamepad.Button.all ()) {
            if (button == UP || button == DOWN || button == LEFT || button == RIGHT)
                continue;

            if (!show_hidden && !gamepad.has_button (button))
                continue;

            var id = button.get_id ();

            union (ref ret, get_element_logical_bounds (id, viewport));
        }

        foreach (var stick in Gamepad.Stick.all ()) {
            if (!show_hidden && !gamepad.has_stick (stick))
                continue;

            var id = stick.get_id ();

            union (ref ret, get_element_logical_bounds (id, viewport));
            union (ref ret, get_element_logical_bounds (@"$id-base", viewport));
        }

        return ret;
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int minimum,
        out int natural,
        out int minimum_baseline,
        out int natural_baseline) {
        double image_width, image_height;

        handle.get_intrinsic_size_in_pixels (out image_width, out image_height);

        if (orientation == HORIZONTAL) {
            var bounds = get_real_bounds ({ 0, 0, image_width, image_height });

            minimum = 0;
            natural = (int) Math.ceil (bounds.width);
        } else {
            Rsvg.Rectangle viewport = {};

            viewport.x = 0;
            viewport.y = 0;

            if (for_size < 0) {
                viewport.width = image_width;
                viewport.height = image_height;
            } else {
                viewport.width = for_size;
                viewport.height = image_height * (double) for_size / image_width;
            }

            var bounds = get_real_bounds (viewport);

            minimum = 0;
            natural = (int) Math.ceil (bounds.height);
        }

        minimum_baseline = natural_baseline = -1;
    }

    protected override void size_allocate (int width, int height, int baseline) {
        double image_width, image_height;

        handle.get_intrinsic_size_in_pixels (out image_width, out image_height);

        viewport = get_real_bounds ({ 0, 0, image_width, image_height });

        global_scale = double.min (
            (double) height / viewport.height,
            (double) width / viewport.width
        );

        viewport.width *= global_scale;
        viewport.height *= global_scale;

        double scale = get_native ().get_surface ().scale;

        viewport.x = Math.round ((width - viewport.width) / 2 * scale) / scale;
        viewport.y = Math.round ((height - viewport.height) / 2 * scale) / scale;
    }

    protected override void realize () {
        base.realize ();

        get_native ().get_surface ().notify["scale"].connect (queue_draw);
    }

    protected override void unrealize () {
        get_native ().get_surface ().notify["scale"].disconnect (queue_draw);

        base.unrealize ();
    }

    private void button_changed (Gamepad.Button button) {
        bool pressed = gamepad.get_button_pressed (button);

        var anim = button_animations[button];

        anim.pause ();

        anim.value_from = anim.value;
        anim.value_to = pressed ? 1 : 0;
        anim.play ();
    }

    private void reload_image () {
        if (gamepad_type == null)
            return;

        var id = gamepad_type.id;
        var path = @"/app/drey/Highscore/input/gamepad/layouts/$id.svg";

        try {
            var bytes = resources_lookup_data (path, NONE);
            var data = bytes.get_data ();

            handle = new Rsvg.Handle.from_data (data);
        } catch (Error e) {
            error ("Failed to set up gamepad view: %s", e.message);
        }

        queue_resize ();
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        if (handle == null)
            return;

        var fg_color = get_color ();
        var hidden_color = get_color ();
        hidden_color.alpha *= 0.3f;

        Gdk.RGBA rgba;

        bool has_dpad = gamepad.has_dpad ();

        // Draw dpad
        if (show_hidden || has_dpad) {
            double up_value = button_animations[Gamepad.Button.UP].value;
            double down_value = button_animations[Gamepad.Button.DOWN].value;
            double left_value = button_animations[Gamepad.Button.LEFT].value;
            double right_value = button_animations[Gamepad.Button.RIGHT].value;

            has_dpad &= !("dpad" in dimmed_controls);

            float x_tilt = (float) (right_value - left_value);
            float y_tilt = (float) (up_value - down_value);

            var transform = new Gsk.Transform ();

            transform = transform.perspective (75);
            transform = transform.rotate_3d (x_tilt * DPAD_TILT_ANGLE, Graphene.Vec3.y_axis ());
            transform = transform.rotate_3d (y_tilt * DPAD_TILT_ANGLE, Graphene.Vec3.x_axis ());

            rgba = has_dpad ? fg_color : hidden_color;

            draw_element (snapshot, "dpad-base", rgba, null);
            draw_element (snapshot, "dpad", rgba, transform);
        }

        // Draw buttons
        foreach (var button in Gamepad.Button.all ()) {
            if (button == UP || button == DOWN || button == LEFT || button == RIGHT)
                continue;

            bool has_button = gamepad.has_button (button);
            if (!show_hidden && !has_button)
                continue;

            has_button &= !(button.get_id () in dimmed_controls);

            float scale = (float) Adw.lerp (
                1,
                PRESSED_BUTTON_SCALE,
                button_animations[button].value
            );

            var transform = new Gsk.Transform ();
            transform = transform.scale (scale, scale);

            rgba = has_button ? fg_color : hidden_color;

            draw_element (snapshot, button.get_id (), rgba, transform);
        }

        // Draw sticks
        foreach (var stick in Gamepad.Stick.all ()) {
            bool has_stick = gamepad.has_stick (stick);
            if (!show_hidden && !has_stick)
                continue;

            has_stick &= !(stick.get_id () in dimmed_controls);

            string id = stick.get_id ();
            string base_id = @"$id-base";
            float x, y;

            if (show_input) {
                x = (float) gamepad.get_stick_x (stick);
                y = (float) gamepad.get_stick_y (stick);
            } else {
                x = y = 0;
            }

            try {
                Rsvg.Rectangle base_rect, rect;
                handle.get_geometry_for_layer (@"#$base_id", viewport, out base_rect, null);
                handle.get_geometry_for_layer (@"#$id", viewport, out rect, null);

                x *= (float) ((base_rect.width - rect.width) / 2 * global_scale);
                y *= (float) ((base_rect.height - rect.height) / 2 * global_scale);
            } catch (Error e) {
                continue;
            }

            float scale = (float) Adw.lerp (
                1,
                PRESSED_BUTTON_SCALE,
                button_animations[stick.get_button ()].value
            );

            var transform = new Gsk.Transform ();
            transform = transform.translate ({ x, y });
            transform = transform.scale (scale, scale);

            rgba = has_stick ? fg_color : hidden_color;

            draw_element (snapshot, base_id, rgba, null);
            draw_element (snapshot, id, rgba, transform);
        }
    }

    private Rsvg.Rectangle? get_element_bounds (string element) {
        Rsvg.Rectangle? ink_rect = null;
        var id = @"#$element";

        try {
            handle.get_geometry_for_layer (id, viewport, out ink_rect, null);
        } catch (Error e) {
            return null;
        }

        double scale = get_native ().get_surface ().scale;

        ink_rect.x = Math.floor (((ink_rect.x - viewport.x) * global_scale + viewport.x) * scale) / scale;
        ink_rect.y = Math.floor (((ink_rect.y - viewport.y) * global_scale + viewport.y) * scale) / scale;
        ink_rect.width = Math.ceil (ink_rect.width * global_scale * scale) / scale;
        ink_rect.height = Math.ceil (ink_rect.height * global_scale * scale) / scale;

        return ink_rect;
    }

    private bool is_within (string element, double x, double y) {
        var bounds = get_element_bounds (element);

        if (bounds == null)
            return false;

        if (x < bounds.x)
            return false;
        if (y < bounds.y)
            return false;
        if (x > bounds.x + bounds.width)
            return false;
        if (y > bounds.y + bounds.height)
            return false;

        return true;
    }

    private bool get_center (string element, out int x, out int y) {
        var bounds = get_element_bounds (element);

        if (bounds == null) {
            x = y = -1;
            return false;
        }

        x = (int) Math.round (bounds.x + bounds.width / 2.0);
        y = (int) Math.round (bounds.y + bounds.height);
        return true;
    }

    private void draw_element (
        Gtk.Snapshot snapshot,
        string element,
        Gdk.RGBA color,
        Gsk.Transform? transform
    ) {
        var ink_rect = get_element_bounds (element);
        var id = @"#$element";

        if (ink_rect == null)
            return;

        snapshot.save ();

        snapshot.translate ({ (float) ink_rect.x, (float) ink_rect.y });

        if (transform != null) {
            snapshot.translate ({ (float) ink_rect.width / 2, (float) ink_rect.height / 2 });
            snapshot.transform (transform);
            snapshot.translate ({ (float) ink_rect.width / -2, (float) ink_rect.height / -2 });
        }

        snapshot.push_mask (ALPHA);

        var snapshot2 = new Gtk.Snapshot ();

        Graphene.Rect rect = {
            { 0, 0 }, { (float) ink_rect.width, (float) ink_rect.height }
        };
        var cr = snapshot2.append_cairo (rect);

        try {
            handle.render_element (cr, id, { 0, 0, ink_rect.width, ink_rect.height });
        } catch (Error e) {
            // ignore it since we're in the middle of making a mask node
        }

        snapshot.append_node (snapshot2.free_to_node ());

        snapshot.pop ();

        snapshot.append_color (color, rect);
        snapshot.pop ();

        snapshot.restore ();
    }

    private void click_released_cb (int n_press, double x, double y) {
        int center_x, center_y;

        if (is_within ("dpad", x, y)) {
            get_center ("dpad", out center_x, out center_y);
            dpad_clicked (center_x, center_y);
            return;
        }

        foreach (var button in Gamepad.Button.all ()) {
            if (button == UP || button == DOWN || button == LEFT || button == RIGHT)
                continue;

            string id = button.get_id ();

            if (is_within (id, x, y)) {
                get_center (id, out center_x, out center_y);
                button_clicked (button, center_x, center_y);
                return;
            }
        }

        foreach (var stick in Gamepad.Stick.all ()) {
            string id = stick.get_id ();
            id = @"$id-base";

            if (is_within (id, x, y)) {
                get_center (id, out center_x, out center_y);
                stick_clicked (stick, center_x, center_y);
                return;
            }
        }
    }

    private void register_input () {
        gamepad.button_pressed.connect (button_changed);
        gamepad.button_released.connect (button_changed);
        gamepad.stick_moved.connect (queue_draw);
        gamepad.missing_changed.connect (queue_draw);
    }

    private void unregister_input () {
        gamepad.button_pressed.disconnect (button_changed);
        gamepad.button_released.disconnect (button_changed);
        gamepad.stick_moved.disconnect (queue_draw);
        gamepad.missing_changed.disconnect (queue_draw);

        foreach (var button in Gamepad.Button.all ()) {
            var anim = button_animations[button];

            anim.pause ();
            anim.value_to = 0;
            anim.reset ();
        }
    }

    public void set_dimmed_controls (string[] controls) {
        dimmed_controls = controls;

        queue_draw ();
    }
}
