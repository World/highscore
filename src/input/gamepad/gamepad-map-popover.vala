// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.GamepadMapPopover : Gtk.Popover {
    public GamepadMappingEditor editor { get; construct; }
    public GamepadType gamepad_type { get; construct; }

    construct {
        editor.gamepad.device.event.connect (gamepad_event_cb);
    }

    public void disconnect_gamepad () {
        editor.gamepad.device.event.disconnect (gamepad_event_cb);
    }

    protected abstract void gamepad_event_cb (Manette.Event event);
}
