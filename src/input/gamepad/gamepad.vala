// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Gamepad : Object {
    public signal void button_pressed (Button button);
    public signal void button_released (Button button);
    public signal void stick_moved (Stick stick, double x, double y);

    public signal void missing_changed ();

    private struct ButtonData {
        string id;
        string sdl_name;
        int code;
    }

    public enum Button {
        UP, DOWN, LEFT, RIGHT, NORTH, SOUTH, WEST, EAST,
        SELECT, START, L, R, L2, R2, L3, R3, L4, R4, L5, R5,
        HOME, MISC1, MISC2, MISC3, MISC4, MISC5, MISC6, TOUCHPAD;

        public string get_id () {
            return BUTTON_DATA[this].id;
        }

        public int get_code () {
            return BUTTON_DATA[this].code;
        }

        public string get_sdl_name () {
            return BUTTON_DATA[this].sdl_name;
        }

        public static Button[] all () {
            return {
                UP, DOWN, LEFT, RIGHT, NORTH, SOUTH, WEST, EAST,
                SELECT, START, L, R, L2, R2, L3, R3, L4, R4, L5, R5,
                HOME, MISC1, MISC2, MISC3, MISC4, MISC5, MISC6, TOUCHPAD,
            };
        }

        public static uint hash (Button button) {
            return button;
        }

        public static bool equal (Button a, Button b) {
            return a == b;
        }
    }

    private const ButtonData[] BUTTON_DATA = {
        { "up",       "dpup",          Linux.Input.BTN_DPAD_UP },
        { "down",     "dpdown",        Linux.Input.BTN_DPAD_DOWN },
        { "left",     "dpleft",        Linux.Input.BTN_DPAD_LEFT },
        { "right",    "dpright",       Linux.Input.BTN_DPAD_RIGHT },
        { "north",    "y",             Linux.Input.BTN_NORTH },
        { "south",    "a",             Linux.Input.BTN_SOUTH },
        { "west",     "x",             Linux.Input.BTN_WEST },
        { "east",     "b",             Linux.Input.BTN_EAST },
        { "select",   "back",          Linux.Input.BTN_SELECT },
        { "start",    "start",         Linux.Input.BTN_START },
        { "l",        "leftshoulder",  Linux.Input.BTN_TL },
        { "r",        "rightshoulder", Linux.Input.BTN_TR },
        { "l2",       "lefttrigger",   Linux.Input.BTN_TL2 },
        { "r2",       "righttrigger",  Linux.Input.BTN_TR2 },
        { "l3",       "leftstick",     Linux.Input.BTN_THUMBL },
        { "r3",       "rightstick",    Linux.Input.BTN_THUMBR },
        { "l4",       "paddle2",       Linux.Input.BTN_TRIGGER_HAPPY1 },
        { "r4",       "paddle1",       Linux.Input.BTN_TRIGGER_HAPPY2 },
        { "l5",       "paddle4",       Linux.Input.BTN_TRIGGER_HAPPY3 },
        { "r5",       "paddle3",       Linux.Input.BTN_TRIGGER_HAPPY4 },
        { "home",     "guide",         Linux.Input.BTN_MODE },
        { "misc1",    "misc1",         Linux.Input.BTN_TRIGGER_HAPPY5 },
        { "misc2",    "misc2",         Linux.Input.BTN_TRIGGER_HAPPY6 },
        { "misc3",    "misc3",         Linux.Input.BTN_TRIGGER_HAPPY7 },
        { "misc4",    "misc4",         Linux.Input.BTN_TRIGGER_HAPPY8 },
        { "misc5",    "misc5",         Linux.Input.BTN_TRIGGER_HAPPY9 },
        { "misc6",    "misc6",         Linux.Input.BTN_TRIGGER_HAPPY10 },
        { "touchpad", "touchpad",      Linux.Input.BTN_TRIGGER_HAPPY11 },
    };

    public const int N_BUTTONS = 28;

    private struct StickData {
        string id;
        string x_sdl_name;
        string y_sdl_name;
        int x_code;
        int y_code;
        Button button;
    }

    public enum Stick {
        LEFT, RIGHT;

        public string get_id () {
            return STICK_DATA[this].id;
        }

        public int get_x_code () {
            return STICK_DATA[this].x_code;
        }

        public int get_y_code () {
            return STICK_DATA[this].y_code;
        }

        public string get_x_sdl_name () {
            return STICK_DATA[this].x_sdl_name;
        }

        public string get_y_sdl_name () {
            return STICK_DATA[this].y_sdl_name;
        }

        public Button get_button () {
            return STICK_DATA[this].button;
        }

        public static Stick[] all () {
            return { LEFT, RIGHT };
        }
    }

    private const StickData[] STICK_DATA = {
        { "left",  "leftx",  "lefty",  Linux.Input.ABS_X,  Linux.Input.ABS_Y,  L3 },
        { "right", "rightx", "righty", Linux.Input.ABS_RX, Linux.Input.ABS_RY, R3 },
    };

    public const int N_STICKS = 2;

    public string guid { get; construct; }

    public Manette.Device? device { get; private set; }
    public unowned GamepadType gamepad_type { get; set; }
    public string name { get; set; }
    public bool supports_rumble { get; private set; }
    public bool supports_mapping { get; private set; }
    public bool swap_select_cancel { get; set; }

    public Button select_button {
        get {
            if (swap_select_cancel)
                return gamepad_type.cancel_button;
            else
                return gamepad_type.select_button;
        }
    }
    public Button cancel_button {
        get {
            if (swap_select_cancel)
                return gamepad_type.select_button;
            else
                return gamepad_type.cancel_button;
        }
    }

    public bool connected {
        get { return device != null; }
    }

    private unowned GamepadType default_type;
    private string default_name;

    public bool modified {
        get {
            if (!connected)
                return false;

            return gamepad_type != default_type || name != default_name;
        }
    }

    private bool[] buttons;
    private double[] axes;

    private HashTable<int, Button> button_mapping;

    private GenericSet<string> missing_controls;
    private Settings settings;

    public Gamepad (string guid) {
        Object (guid: guid);
    }

    construct {
        button_mapping = new HashTable<int, Button> (direct_hash, direct_equal);
        missing_controls = new GenericSet<string> (str_hash, str_equal);

        foreach (var button in Button.all ())
            button_mapping[button.get_code ()] = button;

        buttons = new bool[N_BUTTONS];
        axes = new double[N_STICKS * 2];

        settings = new Settings.with_path (
            "app.drey.Highscore.gamepad",
            @"/app/drey/Highscore/gamepad/$guid/"
        );

        settings.bind_with_mapping (
            "type", this, "gamepad-type", DEFAULT,
            (value, variant, user_data) => {
                var str = variant.get_string ();
                var t = GamepadType.from_id (str);

                if (t == null) {
                    critical ("Unknown gamepad type: %s", str);
                    var default = GamepadType.get_default ();
                    value.set_object (default);
                } else {
                    value.set_object (t);
                }

                return true;
            },
            (value, variant_type, user_data) => {
                var t = value.get_object () as GamepadType;

                return t.id;
            },
            null, null
        );

        settings.bind ("swap-select-cancel", this, "swap-select-cancel", DEFAULT);
        settings.bind ("name", this, "name", DEFAULT);

        settings.changed["missing-controls"].connect (update_missing);

        supports_mapping = settings.get_boolean ("supports-mapping");

        notify["gamepad-type"].connect (() => notify_property ("modified"));
        notify["swap-select-cancel"].connect (() => notify_property ("modified"));
        notify["name"].connect (() => notify_property ("modified"));
        notify["connected"].connect (() => notify_property ("modified"));

        update_missing ();
    }

    private void update_missing () {
        missing_controls.remove_all ();

        var missing = get_missing_controls ();
        foreach (var control in missing)
            missing_controls.add (control);

        missing_changed ();
    }

    public bool get_button_pressed (Button button) {
        return buttons[button];
    }

    public double get_stick_x (Stick stick) {
        return axes[stick * 2];
    }

    public double get_stick_y (Stick stick) {
        return axes[stick * 2 + 1];
    }

    public void rumble (double strong_magnitude, double weak_magnitude, uint16 milliseconds) {
        if (!connected)
            return;

        uint16 strong_mag = (uint16) (strong_magnitude * uint16.MAX);
        uint16 weak_mag = (uint16) (weak_magnitude * uint16.MAX);
        milliseconds = uint16.min (milliseconds, int16.MAX);

        device.rumble (strong_mag, weak_mag, milliseconds);
    }

    private void button_pressed_cb (Manette.Event event) {
        uint16 button_code;

        if (!event.get_button (out button_code))
            return;

        if (!(button_code in button_mapping))
            return;

        var button = button_mapping[button_code];

        if (buttons[button])
            return;

        buttons[button] = true;

        button_pressed (button);
    }

    private void button_released_cb (Manette.Event event) {
        uint16 button_code;

        if (!event.get_button (out button_code))
            return;

        if (!(button_code in button_mapping))
            return;

        var button = button_mapping[button_code];

        if (!buttons[button])
            return;

        buttons[button] = false;

        button_released (button);
    }

    private void absolute_axis_cb (Manette.Event event) {
        int axis;
        double value;

        if (!event.get_absolute (out axis, out value))
            return;

        Stick? stick = null;
        bool y = false;

        foreach (var a in Stick.all ()) {
            var x_code = a.get_x_code ();
            var y_code = a.get_y_code ();

            if (axis == x_code || axis == y_code) {
                stick = a;
                y = axis == y_code;
                break;
            }
        }

        if (stick == null)
            return;

        axes[stick * 2 + (y ? 1 : 0)] = value;

        stick_moved (stick, axes[stick * 2], axes[stick * 2 + 1]);
    }

    public void reset_type () requires (connected) {
        gamepad_type = default_type;
    }

    public void reset_name () requires (connected) {
        name = default_name;
    }

    public void connect_device (Manette.Device device) {
        assert (guid == device.get_guid ());

        this.device = device;

        supports_mapping = device.supports_mapping ();

        if (settings.get_user_value ("supports-mapping") == null)
            settings.set_boolean ("supports-mapping", device.supports_mapping ());

        bool has_missing = settings.get_user_value ("missing-controls") != null;
        if (supports_mapping != has_missing)
            reset_missing ();

        device.button_press_event.connect (button_pressed_cb);
        device.button_release_event.connect (button_released_cb);
        device.absolute_axis_event.connect (absolute_axis_cb);

        default_name = device.get_name ().strip ();

        var device_type = device.get_device_type ();

        default_type = GamepadType.get_for_device_type (device_type);

        if (default_type == null)
            default_type = GamepadType.detect (default_name);

        if (default_type == null)
            default_type = GamepadType.get_default ();

        if (settings.get_user_value ("name") == null)
            settings.set_string ("name", default_name);

        if (settings.get_user_value ("type") == null)
            settings.set_string ("type", default_type.id);

        supports_rumble = device.has_rumble ();

        notify_property ("modified");
        notify_property ("connected");
    }

    public void disconnect_device () {
        device.button_press_event.disconnect (button_pressed_cb);
        device.button_release_event.disconnect (button_released_cb);
        device.absolute_axis_event.disconnect (absolute_axis_cb);

        for (int i = 0; i < buttons.length; i++) {
            if (buttons[i]) {
                buttons[i] = false;
                button_released (i);
            }
        }

        for (int i = 0; i < axes.length; i++)
            axes[i] = 0;

        foreach (var stick in Stick.all ())
            stick_moved (stick, 0, 0);

        device = null;

        notify_property ("connected");
    }

    public void reset_missing () {
        if (supports_mapping) {
            string[] ids = {};

            GamepadMappingUtils.parse_mapping (this, (id, value) => {
                ids += id;
            });

            var missing = GamepadMappingUtils.get_missing_controls (ids);

            set_missing (missing);
        } else {
            settings.reset ("missing-controls");
        }
    }

    public void set_missing (string[] missing) {
        settings.set_strv ("missing-controls", missing);
    }

    public bool has_control (string control) {
        return !(control in missing_controls);
    }

    public bool has_dpad () {
        if (!has_control ("dpad"))
            return false;

        return gamepad_type.has_dpad ();
    }

    public bool has_button (Button button) {
        if (!has_control (button.get_id ()))
            return false;

        return gamepad_type.has_button (button);
    }

    public bool has_stick (Stick stick) {
        if (!has_control (stick.get_id ()))
            return false;

        return gamepad_type.has_stick (stick);
    }

    public string[] get_missing_controls () {
        return settings.get_strv ("missing-controls");
    }

    public uint hash () {
        return str_hash (guid);
    }

    public static bool equal (Gamepad a, Gamepad b) {
        return a == b || a.guid == b.guid;
    }
}
