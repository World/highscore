// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadMapDpadPopover : GamepadMapPopover {
    public GamepadMapDpadPopover (GamepadMappingEditor editor, GamepadType gamepad_type) {
        Object (editor: editor, gamepad_type: gamepad_type);
    }

    private unowned Adw.ViewStack stack;

    private void add_page (
        Gamepad.Button button,
        string icon_name
    ) {
        var btn_name = gamepad_type.get_button_name (button);
        var label = _("Move %s").printf (btn_name);

        var section = new GamepadMapButtonSection (
            editor, button, icon_name, label
        );

        var page = stack.add (section);
        page.title = label;
        page.icon_name = icon_name;

        section.save_and_close.connect (() => editor.save_mapping ());
    }

    construct {
        var box = new Gtk.Box (VERTICAL, 6);

        var stack = new Adw.ViewStack () {
            vhomogeneous = false,
            enable_transitions = true,
        };

        this.stack = stack;

        add_page (UP,    "gamepad-map-up-symbolic");
        add_page (DOWN,  "gamepad-map-down-symbolic");
        add_page (LEFT,  "gamepad-map-left-symbolic");
        add_page (RIGHT, "gamepad-map-right-symbolic");

        var switcher = new Adw.InlineViewSwitcher () {
            stack = stack,
            display_mode = ICONS,
        };

        box.append (switcher);
        box.append (stack);

        child = box;
    }

    protected override void gamepad_event_cb (Manette.Event event) {
        var section = stack.visible_child as GamepadMapButtonSection;

        section.gamepad_event_cb (event);
    }
}
