// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GamepadHintIcon : Adw.Bin {
    private Gtk.Image icon;

    private Gamepad.Button _button;
    public Gamepad.Button button {
        get { return _button; }
        set {
            _button = value;

            update_ui ();
        }
    }

    private GamepadController? _controller;
    public GamepadController? controller {
        get { return _controller; }
        set {
            if (controller == value)
                return;

            if (controller != null) {
                controller.buttons_changed.disconnect (update_ui);

                visible = false;
            }

            _controller = value;

            if (controller != null) {
                controller.buttons_changed.connect (update_ui);

                update_ui ();
            }
        }
    }

    construct {
        icon = new Gtk.Image () {
            pixel_size = 24,
        };

        child = icon;
    }

    private void update_ui () {
        if (controller == null) {
            visible = false;
            return;
        }

        var gamepad = controller.main_gamepad;
        if (gamepad == null) {
            visible = false;
            return;
        }

        icon.icon_name = controller.main_gamepad_type.get_button_icon_name (button);

        visible = true;
    }
}
