// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.InputHandler : Object {
    public signal void home_pressed ();
    public signal void rumble_indicators_changed ();
    public signal void pointer_input_changed ();

    public unowned Runner runner { get; construct; }
    public bool paused { get; set; }

    public int n_rumble_indicators { get; private set; }
    public string? touch_overlay_name { get; private set; }
    public bool uses_pointer { get; private set; }

    private Gee.HashSet<int> pressed_keys;

    private PlatformControls controls;

    private bool running;

    private string? controller_types[ControllerManager.N_PLAYERS];
    private PlayerHandler? player_handlers[ControllerManager.N_PLAYERS];

    private HashTable<Gamepad, PlayerHandler> gamepad_handlers;
    private int keyboard_player;
    private int pointer_player;
    private int touch_player;

    private PlayerHandler[] needs_rumble_indicator;

    private bool rumble_enabled[ControllerManager.N_PLAYERS];

    private int freeze_controllers;
    private string? frozen_controllers[ControllerManager.N_PLAYERS];

    public InputHandler (Runner runner) {
        Object (runner: runner);

        pressed_keys = new Gee.HashSet<int> ();
        gamepad_handlers = new HashTable<Gamepad, PlayerHandler> (
            Gamepad.hash, Gamepad.equal
        );
        keyboard_player = -1;
        pointer_player = -1;
        touch_player = -1;

        controls = PlatformControls.create (runner.game.platform);

        var manager = ControllerManager.get_instance ();

        manager.controller_changed.connect (refresh_player);
    }

    ~InputHandler () {
        var manager = ControllerManager.get_instance ();

        manager.controller_changed.disconnect (refresh_player);
    }

    public string? get_controller_type (uint player) {
        return controller_types[player];
    }

    public void set_controller_type (uint player, string? type) {
        controller_types[player] = type;

        refresh_player (player);
    }

    private void refresh_player (uint player) {
        if (player >= controls.n_players)
            return;

        var manager = ControllerManager.get_instance ();
        bool pointer = player == 0;

        switch (manager.get_controller_type (player)) {
            case NONE:
                setup_player (player, null, false, pointer, false, null);
                break;
            case KEYBOARD:
                setup_player (player, controller_types[player], true, pointer, false, null);
                break;
            case TOUCHSCREEN:
                setup_player (player, controller_types[player], false, pointer, true, null);
                break;
            case GAMEPAD:
                var gamepad = manager.get_gamepad (player);
                setup_player (player, controller_types[player], false, pointer, false, gamepad);
                break;
            default:
                assert_not_reached ();
        }

        update_touch_overlay ();
        update_rumble_indicator ();
        update_uses_pointer ();
    }

    private void setup_player (uint player, string? type, bool keyboard, bool pointer, bool touch, Gamepad? gamepad) {
        var handler = player_handlers[player];

        if (handler != null) {
            if (type == handler.get_controller_type () &&
                keyboard == handler.enable_keyboard &&
                touch == handler.enable_touch &&
                gamepad == handler.gamepad) {
                return;
            }

            if (running)
                handler.ensure_released ();

            if (handler.gamepad != null && gamepad_handlers[handler.gamepad] == handler) {
                gamepad_handlers.remove (handler.gamepad);
                disconnect_gamepad (handler.gamepad);
            }

            if (handler.enable_keyboard && keyboard_player == player)
                keyboard_player = -1;

            if (handler.enable_pointer && pointer_player == player)
                pointer_player = -1;

            if (handler.enable_touch && touch_player == player)
                touch_player = -1;
        }

        if (keyboard)
            keyboard_player = (int) player;

        if (pointer)
            pointer_player = (int) player;

        if (touch)
            touch_player = (int) player;

        if (type == null) {
            handler = null;
        } else {
            handler = new PlayerHandler (this, controls, type, player, keyboard, pointer, touch, gamepad);

            if (gamepad != null) {
                gamepad_handlers[gamepad] = handler;
                connect_gamepad (handler.gamepad);
            }
        }

        player_handlers[player] = handler;

        if (running && handler != null)
            handler.ensure_pressed ();
    }

    private void connect_gamepad (Gamepad gamepad) {
        gamepad.button_pressed.connect (gamepad_button_pressed_cb);
        gamepad.button_released.connect (gamepad_button_released_cb);
        gamepad.stick_moved.connect (gamepad_stick_moved_cb);
    }

    private void disconnect_gamepad (Gamepad gamepad) {
        gamepad.button_pressed.disconnect (gamepad_button_pressed_cb);
        gamepad.button_released.disconnect (gamepad_button_released_cb);
        gamepad.stick_moved.disconnect (gamepad_stick_moved_cb);
    }

    private void gamepad_button_pressed_cb (Gamepad gamepad, Gamepad.Button button) {
        if (!running || paused)
            return;

        if (button == HOME)
            return;

        var handler = gamepad_handlers[gamepad];

        if (handler != null)
            handler.button_pressed (button);
    }

    private void gamepad_button_released_cb (Gamepad gamepad, Gamepad.Button button) {
        if (!running || paused)
            return;

        if (button == HOME) {
            home_pressed ();
            return;
        }

        var handler = gamepad_handlers[gamepad];

        if (handler != null)
            handler.button_released (button);
    }

    private void gamepad_stick_moved_cb (Gamepad gamepad, Gamepad.Stick stick, double x, double y) {
        if (!running || paused)
            return;

        var handler = gamepad_handlers[gamepad];

        if (handler != null)
            handler.stick_moved (stick, x, y);
    }

    public bool key_pressed (int keycode) {
        /* Prevent key repeat */
        if (keycode in pressed_keys)
            return Gdk.EVENT_PROPAGATE;

        pressed_keys.add (keycode);

        if (running && !paused && keycode == Linux.Input.KEY_ESC) {
            home_pressed ();
            return Gdk.EVENT_STOP;
        }

        if (running && !paused && keyboard_player >= 0) {
            var handler = player_handlers[keyboard_player];
            return handler.key_pressed (keycode);
        }

        return Gdk.EVENT_PROPAGATE;
    }

    public void key_released (int keycode) {
        if (running && !paused && keyboard_player >= 0) {
            var handler = player_handlers[keyboard_player];
            handler.key_released (keycode);
        }

        pressed_keys.remove (keycode);
    }

    public void focus_leave () {
        if (running && keyboard_player >= 0) {
            var handler = player_handlers[keyboard_player];

            foreach (int keycode in pressed_keys)
                handler.key_released (keycode);
        }

        pressed_keys.clear ();
    }

    public bool drag_begin (int screen_id, double x, double y) {
        if (!running || paused || pointer_player < 0)
            return Gdk.EVENT_PROPAGATE;

        var handler = player_handlers[pointer_player];
        return handler.drag_begin (screen_id, x, y);
    }

    public void drag_update (double x, double y) {
        if (!running || paused || pointer_player < 0)
            return;

        var handler = player_handlers[pointer_player];
        handler.drag_update (x, y);
    }

    public void drag_end () {
        if (!running || paused || pointer_player < 0)
            return;

        var handler = player_handlers[pointer_player];
        handler.drag_end ();
    }

    public bool get_key_pressed (int keycode) {
        return keycode in pressed_keys;
    }

    public void touch_button_pressed (string element) {
        if (!running || paused || touch_player < 0)
            return;

        if (element == "menu")
            return;

        var handler = player_handlers[touch_player];
        handler.touch_button_pressed (element);
    }

    public void touch_button_released (string element) {
        if (!running || paused || touch_player < 0)
            return;

        if (element == "menu") {
            home_pressed ();
            return;
        }

        var handler = player_handlers[touch_player];
        handler.touch_button_released (element);
    }

    public void touch_stick_moved (string element, double x, double y) {
        if (!running || paused || touch_player < 0)
            return;

        var handler = player_handlers[touch_player];
        handler.touch_stick_moved (element, x, y);
    }

    public void clear_touch_controls () {
        foreach (var handler in player_handlers) {
            if (handler != null)
                handler.ensure_released ();
        }
    }

    public void start () {
        running = true;

        foreach (var handler in player_handlers) {
            if (handler != null)
                handler.ensure_pressed ();
        }
    }

    public void set_rumble_enabled (uint player, bool enabled) {
        if (rumble_enabled[player] == enabled)
            return;

        rumble_enabled[player] = enabled;

        var handler = player_handlers[player];

        if (handler == null)
            return;

        if (!enabled)
            handler.rumble (0, 0, 0);

        update_rumble_indicator ();
        rumble_indicators_changed ();
    }

    public void rumble (uint player, double strong_magnitude, double weak_magnitude, uint16 milliseconds) {
        var handler = player_handlers[player];

        if (handler == null)
            return;

        handler.rumble (strong_magnitude, weak_magnitude, milliseconds);

        if (handler in needs_rumble_indicator)
            rumble_indicators_changed ();
    }

    public void stop_rumble () {
        foreach (var handler in player_handlers) {
            if (handler == null)
                continue;

            handler.rumble (0, 0, 0);
        }

        rumble_indicators_changed ();
    }

    private void update_touch_overlay () {
        if (touch_player == -1) {
            touch_overlay_name = null;
            return;
        }

        var type = controller_types[touch_player];
        var controller = controls.get_controller (type);

        if (controller == null) {
            touch_overlay_name = null;
            return;
        }

        touch_overlay_name = controller.touch_overlay_name;
    }

    private void update_rumble_indicator () {
        needs_rumble_indicator = {};

        foreach (var handler in player_handlers) {
            if (handler == null)
                continue;

            if (!rumble_enabled[handler.player])
                continue;

            if (handler.supports_rumble)
                continue;

            needs_rumble_indicator += handler;
        }

        n_rumble_indicators = needs_rumble_indicator.length;
    }

    private void update_uses_pointer () {
        if (pointer_player < 0) {
            uses_pointer = false;
            pointer_input_changed ();
            return;
        }

        var handler = player_handlers[pointer_player];
        if (handler == null) {
            uses_pointer = false;
            pointer_input_changed ();
            return;
        }

        uses_pointer = handler.uses_pointer;
        pointer_input_changed ();
    }

    public bool handles_screen (int screen_id) {
        if (!uses_pointer)
            return false;

        var handler = player_handlers[pointer_player];
        return handler.handles_screen (screen_id);
    }

    public void get_rumble_indicator (int i, out double strong_magnitude, out double weak_magnitude) {
        strong_magnitude = needs_rumble_indicator[i].rumble_strong;
        weak_magnitude = needs_rumble_indicator[i].rumble_weak;
    }

    public uint get_max_players () {
        return controls.n_players;
    }

    public void freeze_controller_notify () {
        if (freeze_controllers == 0) {
            for (uint p = 0; p < get_max_players (); p++)
                frozen_controllers[p] = controller_types[p];
        }

        freeze_controllers++;
    }

    public void reset_controller (uint p) {
        controller_types[p] = controls.default_controller;

        if (freeze_controllers == 0)
            refresh_player (p);
    }

    public void thaw_controller_notify () {
        freeze_controllers--;

        if (freeze_controllers == 0) {
            for (uint p = 0; p < get_max_players (); p++) {
                if (controller_types[p] != frozen_controllers[p])
                    refresh_player (p);
            }
        }
    }
}
