// This file is part of Highscore. License: GPL-3.0-or-later

public errordomain Highscore.MappingError {
    INVALID_CONTROL,
    INVALID_QUALIFIER,
    DUPLICATE_MAPPING,
    INVALID_MAPPING,
    INVALID_LAYER,
}
