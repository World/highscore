// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.DpadControl : Control {
    public signal void activate (Runner runner, uint player, InputDirection direction, bool pressed);

    public DpadControl (string id, string name) {
        Object (id: id, name: name);
    }

    public override ControlType get_control_type () {
        return DPAD;
    }

    public override string get_name_for_qualifier (string qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return name;

        var dir_name = direction.get_display_name ();

        return "%s %s".printf (name, dir_name);
    }
}
