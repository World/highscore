// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlayerHandler {
    private unowned InputHandler input_handler;
    private unowned Runner runner;
    private PlatformControls controls;
    private string controller_type;

    public uint player { get; private set; }
    public bool enable_keyboard { get; private set; }
    public bool enable_pointer { get; private set; }
    public bool enable_touch { get; private set; }
    public Gamepad? gamepad { get; private set; }

    public bool supports_rumble { get; private set; }
    public double rumble_strong { get; private set; }
    public double rumble_weak { get; private set; }

    public bool uses_pointer { get; private set; }

    private class StickData {
        public ControlHandler? handler;
        public ControlHandler? direction_handlers[InputDirection.N_DIRECTIONS];

        public string to_string () {
            if (handler != null)
                return handler.get_id ();

            string ids[InputDirection.N_DIRECTIONS];

            for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
                var handler = direction_handlers[i];

                if (handler != null)
                    ids[i] = direction_handlers[i].get_id ();
                else
                    ids[i] = "";
            }

            return string.joinv ("|", ids);
        }
    }

    private class LayerData {
        public Gamepad.Button? button;
        public int index;
        public bool active;

        public HashTable<Gamepad.Button, ControlHandler> button_handlers;
        public HashTable<Gamepad.Stick, StickData> stick_data;

        public LayerData (int index, Gamepad.Button? button) {
            this.index = index;
            this.button = button;

            button_handlers = new HashTable<Gamepad.Button, ControlHandler> (direct_hash, direct_equal);
            stick_data = new HashTable<Gamepad.Stick, StickData> (direct_hash, direct_equal);
        }
    }

    private HashTable<Control, ControlHandler> control_handlers;
    private HashTable<int, ControlHandler> key_handlers;
    private HashTable<int, ControlHandler> pointer_handlers;
    private HashTable<string, ControlHandler> touch_handlers;
    private LayerData[] layers;
    private int active_screen;

    public PlayerHandler (
        InputHandler input_handler,
        PlatformControls controls,
        string controller_type,
        uint player,
        bool enable_keyboard,
        bool enable_pointer,
        bool enable_touch,
        Gamepad? gamepad
    ) {
        this.input_handler = input_handler;
        this.runner = input_handler.runner;
        this.controls = controls;
        this.player = player;
        this.controller_type = controller_type;

        this.enable_keyboard = enable_keyboard;
        this.enable_pointer = enable_pointer;
        this.enable_touch = enable_touch;
        this.gamepad = gamepad;

        this.active_screen = -1;

        if (gamepad != null)
            supports_rumble = gamepad.supports_rumble;
        else if (enable_touch)
            supports_rumble = FeedbackdRumble.get_instance ().available;
        else
            supports_rumble = false;

        layers = {};

        layers += new LayerData (0, null);

        layers[0].active = true;

        control_handlers = new HashTable<Control, ControlHandler> (direct_hash, direct_equal);
        key_handlers = new HashTable<int, ControlHandler> (direct_hash, direct_equal);
        pointer_handlers = new HashTable<int, ControlHandler> (direct_hash, direct_equal);
        touch_handlers = new HashTable<string, ControlHandler> (str_hash, str_equal);

        var controller = controls.get_controller (controller_type);

        if (controller != null) {
            var keyboard_mapping = controller.default_keyboard_mapping;
            if (keyboard_mapping != null) {
                try {
                    load_keyboard_mapping (controller, keyboard_mapping);
                } catch (MappingError e) {
                    error ("Failed to load default keyboard mapping: %s", e.message);
                }
            }

            var gamepad_mapping = controller.default_gamepad_mapping;
            if (gamepad_mapping != null) {
                try {
                    load_gamepad_mapping (controller, gamepad_mapping, layers[0]);
                } catch (MappingError e) {
                    error ("Failed to load default gamepad mapping: %s", e.message);
                }
            }

            try {
                load_pointer_controls (controller);
            } catch (MappingError e) {
                error ("Failed to load pointer controls: %s", e.message);
            }
        }

        var global_controller = controls.global_controller;

        if (global_controller != null) {
            var keyboard_mapping = global_controller.default_keyboard_mapping;
            if (keyboard_mapping != null) {
                try {
                    load_keyboard_mapping (global_controller, keyboard_mapping);
                } catch (MappingError e) {
                    error ("Failed to load default keyboard mapping: %s", e.message);
                }
            }

            var gamepad_mapping = global_controller.default_gamepad_mapping;
            if (gamepad_mapping != null) {
                try {
                    load_gamepad_mapping (global_controller, gamepad_mapping, layers[0]);
                } catch (MappingError e) {
                    error ("Failed to load default gamepad mapping: %s", e.message);
                }
            }

            try {
                load_pointer_controls (global_controller);
            } catch (MappingError e) {
                error ("Failed to load pointer controls: %s", e.message);
            }
        }

        if (controller != null) {
            var overlay_name = controller.touch_overlay_name;
            if (overlay_name != null) {
                try {
                    load_touch_mapping (controller, global_controller, overlay_name);
                } catch (MappingError e) {
                    error ("Failed to load touch overlay: %s", e.message);
                }
            }
        }

        set_layer_active (layers[0], true);
        uses_pointer = (pointer_handlers.size () > 0);
    }

    private ControlHandler ensure_handler (Control control) {
        if (!(control in control_handlers)) {
            var handler = ControlHandler.create (runner, player, control);

            control_handlers[control] = handler;
        }

        return control_handlers[control];
    }

    private void load_keyboard_mapping (
        PlatformController controller,
        PlatformKeyboardMapping mapping
    ) throws MappingError {
        if (!enable_keyboard)
            return;

        foreach (var key in mapping.list_key_mappings ()) {
            var parts = key.control.split (":", 2);

            if (key.keycode in key_handlers) {
                var control1 = key_handlers[key.keycode].get_id ();
                var control2 = parts[0];
                throw new MappingError.DUPLICATE_MAPPING (
                    "Key %d cannot be mapped to both '%s' and '%s'",
                    key.keycode, control1, control2
                );
            }

            var control = controller.get_control (parts[0]);
            var qualifier = parts.length > 1 ? parts[1] : null;

            if (control == null) {
                throw new MappingError.INVALID_CONTROL (
                    "No such control: '%s'", key.control
                );
            }

            var handler = ensure_handler (control);
            if (!handler.add_key (key.keycode, qualifier)) {
                if (qualifier == null) {
                    throw new MappingError.INVALID_QUALIFIER (
                        "'%s' needs a qualifier", handler.get_id ()
                    );
                } else {
                    throw new MappingError.INVALID_QUALIFIER (
                        "Qualifier '%s' is not valid for '%s'",
                        qualifier, handler.get_id ()
                    );
                }
            }

            key_handlers[key.keycode] = handler;
        }
    }

    private void load_gamepad_mapping (
        PlatformController controller,
        PlatformGamepadMapping mapping,
        LayerData layer
    ) throws MappingError {
        if (gamepad == null)
            return;

        foreach (var button in mapping.list_button_mappings ())
            load_button_mapping (controller, layer, button.button, button.control);

        foreach (var stick in mapping.list_stick_mappings ()) {
            if (stick.stick in layer.stick_data) {
                var stick_id = stick.stick.get_id ();
                var control1 = layer.stick_data[stick.stick].to_string ();
                var control2 = stick.control;
                throw new MappingError.DUPLICATE_MAPPING (
                    "Stick '%s' cannot be mapped to both '%s' and '%s'",
                    stick_id, control1, control2
                );
            }

            var mapping_parts = stick.control.split ("|");

            if (mapping_parts.length != 1 && mapping_parts.length != InputDirection.N_DIRECTIONS) {
                var stick_id = stick.stick.get_id ();
                throw new MappingError.INVALID_MAPPING (
                    "Stick %s can only be mapped to a single stick, single d-pad or %d buttons",
                    stick_id, InputDirection.N_DIRECTIONS
                );
            }

            if (mapping_parts.length == 1)
                load_stick_to_single_control_mapping (controller, layer, stick.stick, mapping_parts[0]);
            else
                load_stick_to_buttons_mapping (controller, layer, stick.stick, mapping_parts);
        }

        foreach (var l in mapping.list_layers ()) {
            if (layer.index != 0) {
                throw new MappingError.INVALID_LAYER (
                    "Non-default layers cannot contain other layers"
                );
            }

            var layer_data = new LayerData (layers.length, l.button);
            load_gamepad_mapping (controller, l.mapping, layer_data);
            layers += layer_data;
        }
    }

    private void load_button_mapping (
        PlatformController controller,
        LayerData layer,
        Gamepad.Button button,
        string dest
    ) throws MappingError {
        var parts = dest.split (":", 2);

        var control = controller.get_control (parts[0]);
        var qualifier = parts.length > 1 ? parts[1] : null;

        if (control == null) {
            throw new MappingError.INVALID_CONTROL (
                "No such control: '%s'", parts[0]
            );
        }

        var handler = ensure_handler (control);
        if (!handler.add_button (button, qualifier)) {
            if (qualifier == null) {
                throw new MappingError.INVALID_QUALIFIER (
                    "'%s' needs a qualifier", handler.get_id ()
                );
            } else {
                throw new MappingError.INVALID_QUALIFIER (
                    "Qualifier '%s' is not valid for '%s'",
                    qualifier, handler.get_id ()
                );
            }
        }

        if (button in layer.button_handlers) {
            var button_id = button.get_id ();
            var control1 = layer.button_handlers[button].get_id ();
            var control2 = handler.get_id ();
            throw new MappingError.DUPLICATE_MAPPING (
                "Button '%s' cannot be mapped to both '%s' and '%s'",
                button_id, control1, control2
            );
        } else {
            layer.button_handlers[button] = handler;
        }
    }

    private void load_stick_to_single_control_mapping (
        PlatformController controller,
        LayerData layer,
        Gamepad.Stick stick,
        string dest
    ) throws MappingError {
        var parts = dest.split (":", 2);

        var control = controller.get_control (parts[0]);
        var qualifier = parts.length > 1 ? parts[1] : null;

        if (control == null) {
            throw new MappingError.INVALID_CONTROL (
                "No such control: '%s'", parts[0]
            );
        }

        if (control.get_control_type () == BUTTON) {
            var stick_id = stick.get_id ();
            throw new MappingError.INVALID_MAPPING (
                "Stick %s cannot be mapped to a single button", stick_id
            );
        }

        var handler = ensure_handler (control);
        if (!handler.add_stick (stick, null, qualifier)) {
            if (qualifier == null) {
                throw new MappingError.INVALID_QUALIFIER (
                    "'%s' needs a qualifier", handler.get_id ()
                );
            } else {
                throw new MappingError.INVALID_QUALIFIER (
                    "Qualifier '%s' is not valid for '%s'",
                    qualifier, handler.get_id ()
                );
            }
        }

        StickData data = new StickData ();
        data.handler = handler;

        layer.stick_data[stick] = data;
    }

    private void load_stick_to_buttons_mapping (
        PlatformController controller,
        LayerData layer,
        Gamepad.Stick stick,
        string[] dest
    ) throws MappingError {
        assert (dest.length == InputDirection.N_DIRECTIONS);

        StickData data = new StickData ();

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            if (dest[i] == "") {
                data.direction_handlers[i] = null;
                continue;
            }

            var parts = dest[i].split (":", 2);

            var control = controller.get_control (parts[0]);
            var qualifier = parts.length > 1 ? parts[1] : null;

            if (control == null) {
                throw new MappingError.INVALID_CONTROL (
                    "No such control: '%s'", parts[0]
                );
            }

            var handler = ensure_handler (control);
            if (!handler.add_stick (stick, i, qualifier)) {
                if (qualifier == null) {
                    throw new MappingError.INVALID_QUALIFIER (
                        "'%s' needs a qualifier", handler.get_id ()
                    );
                } else {
                    throw new MappingError.INVALID_QUALIFIER (
                        "Qualifier '%s' is not valid for '%s'",
                        qualifier, handler.get_id ()
                    );
                }
            }

            data.direction_handlers[i] = handler;
        }

        layer.stick_data[stick] = data;
    }

    private void load_touch_mapping (
        PlatformController controller,
        PlatformController? global_controller,
        string touch_overlay_name
    ) throws MappingError {
        if (!enable_touch)
            return;

        var overlay = OverlayRegister.get_instance ().get_overlay (touch_overlay_name);
        if (overlay == null)
            return;

        var layouts = overlay.get_layouts ();
        foreach (var layout_desc in layouts) {
            foreach (var plane_desc in layout_desc.planes) {
                foreach (var control_desc in plane_desc.controls) {
                    var parts = control_desc.id.split (":", 2);

                    if (parts[0] == "menu")
                        continue;

                    var control = controller.get_control (parts[0]);
                    if (control == null && global_controller != null)
                        control = global_controller.get_control (parts[0]);

                    var qualifier = parts.length > 1 ? parts[1] : null;

                    if (control == null) {
                        throw new MappingError.INVALID_CONTROL (
                            "No such control: '%s'", parts[0]
                        );
                    }

                    if (control_desc.element in touch_handlers)
                        continue;

                    var handler = ensure_handler (control);

                    var element = control_desc.element;
                    var type = control_desc.type;

                    if (!handler.add_touch (element, type, qualifier)) {
                        if (qualifier == null) {
                            throw new MappingError.INVALID_QUALIFIER (
                                "'%s' needs a qualifier", handler.get_id ()
                            );
                        } else {
                            throw new MappingError.INVALID_QUALIFIER (
                                "Qualifier '%s' is not valid for '%s'",
                                qualifier, handler.get_id ()
                            );
                        }
                    }

                    touch_handlers[element] = handler;
                }
            }
        }
    }

    private void load_pointer_controls (PlatformController controller) throws MappingError {
        var screens = controller.list_screens ();
        foreach (var screen in screens) {
            var control = controller.get_for_screen (screen);

            if (screen in pointer_handlers) {
                throw new MappingError.DUPLICATE_MAPPING (
                    "Multiple pointer controls on the screen %d: %s and %s",
                    screen, pointer_handlers[screen].get_id (), control.id
                );
            }

            var handler = ensure_handler (control);
            handler.add_pointer (screen);

            pointer_handlers[screen] = handler;
        }
    }

    private LayerData? find_layer_for_button (Gamepad.Button button) {
        for (int i = 1; i < layers.length; i++) {
            if (layers[i].button == button)
                return layers[i];
        }

        return null;
    }

    private LayerData? find_topmost_layer_for_button (Gamepad.Button button) {
        for (int i = layers.length - 1; i >= 0; i--) {
            var layer = layers[i];

            if (!layer.active)
                continue;

            if (!(button in layer.button_handlers))
                continue;

            return layer;
        }

        return null;
    }

    private LayerData? find_topmost_layer_for_stick (Gamepad.Stick stick) {
        for (int i = layers.length - 1; i >= 0; i--) {
            var layer = layers[i];

            if (!layer.active)
                continue;

            if (!(stick in layer.stick_data))
                continue;

            return layer;
        }

        return null;
    }

    public void button_pressed (Gamepad.Button button) {
        var layer_for_button = find_layer_for_button (button);
        if (layer_for_button != null) {
            set_layer_active (layer_for_button, true);
            return;
        }

        var layer = find_topmost_layer_for_button (button);
        if (layer == null)
            return;

        var handler = layer.button_handlers[button];
        if (handler == null)
            return;

        handler.button_pressed (button);
    }

    public void button_released (Gamepad.Button button) {
        var layer_for_button = find_layer_for_button (button);
        if (layer_for_button != null) {
            set_layer_active (layer_for_button, false);
            return;
        }

        var layer = find_topmost_layer_for_button (button);
        if (layer == null)
            return;

        var handler = layer.button_handlers[button];
        if (handler == null)
            return;

        handler.button_released (button);
    }

    public void stick_moved (Gamepad.Stick stick, double x, double y) {
        var layer = find_topmost_layer_for_stick (stick);
        if (layer == null)
            return;

        stick_moved_for_handler (layer.stick_data[stick], stick, x, y);
    }

    private void stick_moved_for_handler (StickData data, Gamepad.Stick stick, double x, double y) {
        if (data.handler != null) {
            data.handler.stick_moved (stick, x, y);
            return;
        }

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            var handler = data.direction_handlers[i];

            if (handler != null)
                handler.stick_moved (stick, x, y);
        }
    }

    private void set_layer_active (LayerData layer, bool active) {
        if (!active)
            layer.active = false;

        foreach (var button in layer.button_handlers.get_keys ()) {
            if (!gamepad.get_button_pressed (button))
                continue;

            var prev_layer = find_topmost_layer_for_button (button);
            if (prev_layer == null) {
                if (active)
                    layer.button_handlers[button].button_pressed (button);
                else
                    layer.button_handlers[button].button_released (button);

                continue;
            }

            if (prev_layer.index < layer.index) {
                if (active) {
                    prev_layer.button_handlers[button].button_released (button);
                    layer.button_handlers[button].button_pressed (button);
                } else {
                    layer.button_handlers[button].button_released (button);
                    prev_layer.button_handlers[button].button_pressed (button);
                }
            }
        }

        foreach (var stick in layer.stick_data.get_keys ()) {
            double x = gamepad.get_stick_x (stick);
            double y = gamepad.get_stick_y (stick);

            var data = layer.stick_data[stick];

            var prev_layer = find_topmost_layer_for_stick (stick);
            if (prev_layer == null) {
                stick_moved_for_handler (
                    data, stick, active ? x : 0, active ? y : 0
                );

                continue;
            }

            if (prev_layer.index < layer.index) {
                var prev_data = prev_layer.stick_data[stick];

                if (active) {
                    stick_moved_for_handler (prev_data, stick, 0, 0);
                    stick_moved_for_handler (data, stick, x, y);
                } else {
                    stick_moved_for_handler (data, stick, 0, 0);
                    stick_moved_for_handler (prev_data, stick, x, y);
                }
            }
        }

        if (active)
            layer.active = true;
    }

    public bool key_pressed (int keycode) {
        var handler = key_handlers[keycode];

        if (handler != null) {
            handler.key_pressed (keycode);
            return Gdk.EVENT_STOP;
        }

        return Gdk.EVENT_PROPAGATE;
    }

    public void key_released (int keycode) {
        var handler = key_handlers[keycode];

        if (handler != null)
            handler.key_released (keycode);
    }

    public bool drag_begin (int screen_id, double x, double y) {
        var handler = pointer_handlers[screen_id];

        if (handler != null) {
            handler.drag_begin (x, y);
            active_screen = screen_id;
            return Gdk.EVENT_STOP;
        }

        return Gdk.EVENT_PROPAGATE;
    }

    public void drag_update (double x, double y) {
        if (active_screen < 0)
            return;

        var handler = pointer_handlers[active_screen];

        if (handler != null)
            handler.drag_update (x, y);
    }

    public void drag_end () {
        if (active_screen < 0)
            return;

        var handler = pointer_handlers[active_screen];

        if (handler != null)
            handler.drag_end ();

        active_screen = -1;
    }

    public void touch_button_pressed (string element) {
        var parts = element.split (":", 2);

        var handler = touch_handlers[element];
        if (handler == null)
            handler = touch_handlers[parts[0]];
        if (handler == null)
            return;

        var qualifier = parts.length > 1 ? parts[1] : null;

        handler.touch_button_pressed (parts[0], qualifier);
    }

    public void touch_button_released (string element) {
        var parts = element.split (":", 2);

        var handler = touch_handlers[element];
        if (handler == null)
            handler = touch_handlers[parts[0]];
        if (handler == null)
            return;

        var qualifier = parts.length > 1 ? parts[1] : null;

        handler.touch_button_released (parts[0], qualifier);
    }

    public void touch_stick_moved (string element, double x, double y) {
        var handler = touch_handlers[element];

        if (handler != null)
            handler.touch_stick_moved (element, x, y);
    }

    private void ensure_state (bool active) {
        if (enable_touch && !active) {
            foreach (var element in touch_handlers.get_keys ()) {
                var handler = touch_handlers[element];
                handler.touch_clear (element);
            }
        }

        if (enable_keyboard) {
            foreach (int keycode in key_handlers.get_keys ()) {
                var handler = key_handlers[keycode];

                if (!input_handler.get_key_pressed (keycode))
                    continue;

                if (active)
                    handler.key_pressed (keycode);
                else
                    handler.key_released (keycode);
            }
        }

        if (gamepad != null) {
            for (int i = 1; i < layers.length; i++) {
                var layer = layers[i];

                set_layer_active (
                    layer, active && gamepad.get_button_pressed (layer.button)
                );
            }

            foreach (var layer in layers) {
                foreach (var button in layer.button_handlers.get_keys ()) {
                    var handler = layer.button_handlers[button];

                    if (!gamepad.get_button_pressed (button))
                        continue;

                    if (handler != null) {
                        if (active)
                            handler.button_pressed (button);
                        else
                            handler.button_released (button);
                    }
                }

                foreach (var stick in layer.stick_data.get_keys ()) {
                    var data = layer.stick_data[stick];

                    if (data.handler != null)
                        data.handler.stick_moved (stick, 0, 0);

                    if (data.direction_handlers != null) {
                        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
                            var handler = data.direction_handlers[i];

                            if (handler == null)
                                continue;

                            double x = active ? gamepad.get_stick_x (stick) : 0;
                            double y = active ? gamepad.get_stick_y (stick) : 0;

                            handler.stick_moved (stick, x, y);
                        }
                    }
                }
            }
        }
    }

    public void ensure_released () {
        ensure_state (false);
    }

    public void ensure_pressed () {
        ensure_state (true);
    }

    public string get_controller_type () {
        return controller_type;
    }

    public void rumble (double strong_magnitude, double weak_magnitude, uint16 milliseconds) {
        rumble_strong = strong_magnitude;
        rumble_weak = weak_magnitude;

        if (!supports_rumble)
            return;

        if (enable_touch) {
            var mobile_rumble = FeedbackdRumble.get_instance ();
            double magnitude = (strong_magnitude + weak_magnitude) / 2.0;

            mobile_rumble.rumble.begin (magnitude, milliseconds);
            return;
        }

        gamepad.rumble (strong_magnitude, weak_magnitude, milliseconds);
    }

    public bool handles_screen (int screen_id) {
        return uses_pointer && (screen_id in pointer_handlers);
    }
}
