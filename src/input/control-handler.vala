// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.ControlHandler {
    protected unowned Runner runner;
    protected uint player;
    protected Control control;

    public string get_id () {
        return control.id;
    }

    public abstract bool add_key (int keycode, string? qualifier);
    public abstract bool add_button (Gamepad.Button button, string? qualifier);
    public abstract bool add_stick (Gamepad.Stick stick, InputDirection? direction, string? qualifier);
    public abstract bool add_pointer (int screen_id);
    public abstract bool add_touch (string element, Control.ControlType control_type, string? qualifier);

    public abstract void key_pressed (int keycode);
    public abstract void key_released (int keycode);

    public abstract void button_pressed (Gamepad.Button button);
    public abstract void button_released (Gamepad.Button button);
    public abstract void stick_moved (Gamepad.Stick stick, double x, double y);

    public abstract void drag_begin (double x, double y);
    public abstract void drag_update (double x, double y);
    public abstract void drag_end ();

    public abstract void touch_button_pressed (string element, string? qualifier);
    public abstract void touch_button_released (string element, string? qualifier);
    public abstract void touch_stick_moved (string element, double x, double y);
    public abstract void touch_clear (string element);

    public static ControlHandler create (Runner runner, uint player, Control control) {
        switch (control.get_control_type ()) {
            case BUTTON:
               return new ButtonHandler (runner, player, control);
            case DPAD:
                return new DpadHandler (runner, player, control);
            case STICK:
                return new StickHandler (runner, player, control);
            case TRIGGER:
                return new TriggerHandler (runner, player, control);
            case POINTER:
                return new PointerHandler (runner, player, control);
            case ROTARY:
                return new RotaryHandler (runner, player, control);
            default:
                assert_not_reached ();
        }
    }
}
