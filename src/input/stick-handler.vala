// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.StickHandler : ControlHandler {
    private Gee.HashSet<int>? pressed_keys;
    private HashTable<int, InputDirection> key_directions;

    private struct StickData {
        double x;
        double y;
    }

    private Gee.HashSet<Gamepad.Button>? pressed_buttons;
    private HashTable<Gamepad.Button, InputDirection> button_directions;
    private StickData sticks[Gamepad.N_STICKS];
    private bool touch_active[InputDirection.N_DIRECTIONS];
    private GenericSet<string>? touch_up;
    private GenericSet<string>? touch_down;
    private GenericSet<string>? touch_left;
    private GenericSet<string>? touch_right;
    private HashTable<string, InputDirection> touch_directions;
    private HashTable<string, StickData?> touch_sticks;

    public StickHandler (Runner runner, uint player, Control control) {
        this.runner = runner;
        this.player = player;
        this.control = control;
    }

    private GenericSet<string>? get_touch_for_direction (InputDirection direction) {
        switch (direction) {
            case UP:
                return touch_up;
            case DOWN:
                return touch_down;
            case LEFT:
                return touch_left;
            case RIGHT:
                return touch_right;
            default:
                assert_not_reached ();
        }
    }

    public override bool add_key (int keycode, string? qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return false;

        if (pressed_keys == null) {
            pressed_keys = new Gee.HashSet<int> ();
            key_directions = new HashTable<int, InputDirection> (direct_hash, direct_equal);
        }

        key_directions[keycode] = direction;
        return true;
    }

    public override bool add_button (Gamepad.Button button, string? qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return false;

        if (pressed_buttons == null) {
            pressed_buttons = new Gee.HashSet<Gamepad.Button> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
            button_directions = new HashTable<Gamepad.Button, InputDirection> (
                Gamepad.Button.hash, Gamepad.Button.equal
            );
        }

        button_directions[button] = direction;
        return true;
    }

    public override bool add_stick (Gamepad.Stick stick, InputDirection? direction, string? qualifier) {
        assert (direction == null);

        return true;
    }

    public override bool add_pointer (int screen_id) {
        return false;
    }

    public override bool add_touch (string element, Control.ControlType control_type, string? qualifier) {
        if (control_type == DPAD || control_type == BUTTON) {
            if (touch_up == null) {
                touch_up = new GenericSet<string> (str_hash, str_equal);
                touch_down = new GenericSet<string> (str_hash, str_equal);
                touch_left = new GenericSet<string> (str_hash, str_equal);
                touch_right = new GenericSet<string> (str_hash, str_equal);
                touch_directions = new HashTable<string, InputDirection> (
                    str_hash, str_equal
                );
            }

            if (control_type == BUTTON) {
                var direction = InputDirection.from_string (qualifier);
                if (direction == null)
                    return false;

                touch_directions[element] = direction;
            }

            return true;
        }

        if (control_type == STICK) {
            if (qualifier != null)
                return false;

            if (touch_sticks == null)
                touch_sticks = new HashTable<string, StickData?> (str_hash, str_equal);

            touch_sticks[element] = {};

            return true;
        }

        return false;
    }

    public override void key_pressed (int keycode) {
        pressed_keys.add (keycode);

        notify_control ();
    }

    public override void key_released (int keycode) {
        pressed_keys.remove (keycode);

        notify_control ();
    }

    public override void button_pressed (Gamepad.Button button) {
        pressed_buttons.add (button);

        notify_control ();
    }

    public override void button_released (Gamepad.Button button) {
        pressed_buttons.remove (button);

        notify_control ();
    }

    public override void stick_moved (Gamepad.Stick stick, double x, double y) {
        sticks[stick] = { x, y };

        notify_control ();
    }

    public override void drag_begin (double x, double y) {}
    public override void drag_update (double x, double y) {}
    public override void drag_end () {}

    public override void touch_button_pressed (string element, string? qualifier) {
        InputDirection direction;

        if (qualifier != null) {
            var dir = InputDirection.from_string (qualifier);
            if (dir == null)
                return;

            direction = dir;
        } else {
            if (!(element in touch_directions))
                return;

            direction = touch_directions[element];
        }

        var touch = get_touch_for_direction (direction);

        touch.add (element);

        notify_control ();
    }

    public override void touch_button_released (string element, string? qualifier) {
        InputDirection direction;

        if (qualifier != null) {
            var dir = InputDirection.from_string (qualifier);
            if (dir == null)
                return;

            direction = dir;
        } else {
            if (!(element in touch_directions))
                return;

            direction = touch_directions[element];
        }

        var touch = get_touch_for_direction (direction);
        if (touch == null)
            return;

        touch.remove (element);

        notify_control ();
    }

    public override void touch_stick_moved (string element, double x, double y) {
        touch_sticks[element] = { x, y };

        notify_control ();
    }

    public override void touch_clear (string element) {
        if (touch_directions != null) {
            foreach (var direction in InputDirection.all ())
                touch_button_released (element, direction.to_string ());
        }

        if (touch_sticks != null && element in touch_sticks)
            touch_stick_moved (element, 0, 0);
    }

    private void notify_control () {
        var stick = control as StickControl;

        double x = 0, y = 0;

        if (pressed_keys != null) {
            foreach (var key in pressed_keys) {
                var direction = key_directions[key];

                x += direction.get_x ();
                y += direction.get_y ();
            }
        }

        if (pressed_buttons != null) {
            foreach (var button in pressed_buttons) {
                var direction = button_directions[button];

                x += direction.get_x ();
                y += direction.get_y ();
            }
        }

        if (touch_directions != null) {
            foreach (var direction in InputDirection.all ()) {
                var touch = get_touch_for_direction (direction);

                x += touch.length * direction.get_x ();
                y += touch.length * direction.get_y ();
            }
        }

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            InputDirection direction = (InputDirection) i;

            if (touch_active[i]) {
                x += direction.get_x ();
                y += direction.get_y ();
            }
        }

        double length = Math.sqrt (x * x + y * y);
        if (length > 1) {
            x /= length;
            y /= length;
        }

        for (int i = 0; i < Gamepad.N_STICKS; i++) {
            x += sticks[i].x;
            y += sticks[i].y;
        }

        if (touch_sticks != null) {
            foreach (var stick_data in touch_sticks.get_values ()) {
                x += stick_data.x;
                y += stick_data.y;
            }
        }

        length = Math.sqrt (x * x + y * y);
        if (length > 1) {
            x /= length;
            y /= length;
        }

        stick.moved (runner, player, x, y);
    }
}
