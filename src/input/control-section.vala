// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.ControlSection : Object {
    public unowned PlatformController controller { get; construct; }
    public string id { get; construct; }

    private Control[] controls;
    private HashTable<string, Control> id_to_control;

    public ControlSection (PlatformController controller, string id) {
        Object (controller: controller, id: id);
    }

    construct {
        controls = {};
        id_to_control = new HashTable<string, Control> (str_hash, str_equal);
    }

    public Control? get_control (string id) {
        return id_to_control[id];
    }

    public Control[] list_controls () {
        return controls;
    }

    public void add_control (Control control) requires (!(control.id in id_to_control)) {
        controls += control;
        id_to_control[control.id] = control;
        control.section = this;
    }
}
