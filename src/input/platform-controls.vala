// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlatformControls : Object {
    public string default_controller { get; protected set; }
    public PlatformController? global_controller { get; protected set; }

    public uint n_players { get; set; default = 1; }

    private HashTable<string, PlatformController> controllers;

    construct {
        controllers = new HashTable<string, PlatformController> (str_hash, str_equal);
    }

    protected void add_controller (PlatformController controller) {
        controllers[controller.id] = controller;
    }

    public PlatformController? get_controller (string id) {
        return controllers[id];
    }

    public static PlatformControls create (Platform platform) {
        var type = platform.controls_type;

        if (type == Type.NONE && platform.parent != null)
            type = platform.parent.controls_type;

        if (type == Type.NONE)
            type = typeof (PlatformControls);

        return Object.new (type) as PlatformControls;
    }
}
