// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.ControllerManager : Object {
    public const uint N_PLAYERS = 5;

    public signal void controller_changed (uint player);

    public enum ControllerType {
        NONE,
        KEYBOARD,
        TOUCHSCREEN,
        GAMEPAD,
    }

    private struct ControllerData {
        bool automatic;
        ControllerType? type;
        Gamepad? gamepad;

        public string to_string () {
            if (automatic)
                return "auto";

            switch (type) {
                case NONE:
                    return "none";
                case KEYBOARD:
                    return "keyboard";
                case TOUCHSCREEN:
                    return "touchscreen";
                case GAMEPAD:
                    return gamepad.guid;
                default:
                    assert_not_reached ();
            }
        }

        private string get_display_string_do () {
            if (type == null)
                return "";

            switch (type) {
                case NONE:
                    return _("None");
                case KEYBOARD:
                    return _("Keyboard");
                case TOUCHSCREEN:
                    return _("Touchscreen");
                case GAMEPAD:
                    return gamepad.name;
                default:
                    assert_not_reached ();
            }
        }

        public string to_display_string () {
            if (automatic)
                return _("Auto (%s)").printf (get_display_string_do ());

            return get_display_string_do ();
        }
    }

    private static ControllerManager instance;

    private Settings settings;
    private ControllerData controller_data[N_PLAYERS];

    public bool touch_enabled { get; set; }

    public static ControllerManager get_instance () {
        if (instance == null)
            instance = new ControllerManager ();

        return instance;
    }

    construct {
        settings = new Settings ("app.drey.Highscore");

        settings.bind ("touch-controls", this, "touch-enabled", GET);

        var gamepad_manager = GamepadManager.get_instance ();
        var controllers = settings.get_strv ("controllers");

        for (int i = 0; i < N_PLAYERS; i++) {
            ControllerData data = {};

            if (i >= controllers.length || controllers[i] == "auto") {
                data.automatic = true;
            } else if (controllers[i] == "none") {
                data.type = NONE;
            } else if (controllers[i] == "keyboard") {
                data.type = KEYBOARD;
            } else if (controllers[i] == "touchscreen") {
                data.type = TOUCHSCREEN;
            } else {
                var gamepad = gamepad_manager.find_gamepad (controllers[i]);

                if (gamepad == null) {
                    data.automatic = true;
                } else {
                    data.type = GAMEPAD;
                    data.gamepad = gamepad;
                }
            }

            controller_data[i] = data;
        }

        update_touch_cb ();

        notify["touch-enabled"].connect (update_touch_cb);

        uint n = gamepad_manager.gamepads.n_items;
        for (uint i = 0; i < n; i++) {
            var gamepad = gamepad_manager.gamepads.get_item (i) as Gamepad;

            connect_to_gamepad (gamepad);
        }

        gamepad_manager.gamepad_added.connect (connect_to_gamepad);
        gamepad_manager.gamepad_forgotten.connect (disconnect_from_gamepad);
    }

    ~ControllerManager () {
        var gamepad_manager = GamepadManager.get_instance ();

        gamepad_manager.gamepad_added.disconnect (connect_to_gamepad);
        gamepad_manager.gamepad_forgotten.disconnect (disconnect_from_gamepad);
    }

    public bool get_automatic (uint player) {
        return controller_data[player].automatic;
    }

    public ControllerType get_controller_type (uint player) {
        return controller_data[player].type;
    }

    public Gamepad? get_gamepad (uint player) {
        return controller_data[player].gamepad;
    }

    public string get_display_string (uint player) {
        return controller_data[player].to_display_string ();
    }

    public void set_controller (uint player, bool automatic, ControllerType? type, Gamepad? gamepad) {
        assert (player < N_PLAYERS);

        if (automatic) {
            assert (type == null);
            assert (gamepad == null);
        } else if (type == GAMEPAD) {
            assert (gamepad != null);
        } else {
            assert (type != null);
            assert (gamepad == null);
        }

        if (type == KEYBOARD || type == TOUCHSCREEN || type == GAMEPAD) {
            for (uint p = 0; p < N_PLAYERS; p++) {
                if (p == player)
                    continue;

                if (get_controller_type (p) != type)
                    continue;

                if (get_automatic (p) != automatic)
                    continue;

                if (type != GAMEPAD || gamepad == get_gamepad (p)) {
                    controller_data[p] = controller_data[player];
                    controller_changed (p);
                    break;
                }
            }
        }

        controller_data[player] = { automatic, type, gamepad };

        save_controllers ();
        refresh_auto_controllers ();

        controller_changed (player);
    }

    private void save_controllers () {
        string[] controllers = {};

        foreach (var data in controller_data)
            controllers += data.to_string ();

        settings.set_strv ("controllers", controllers);
    }

    public void connect_to_gamepad (Gamepad gamepad) {
        gamepad.notify["connected"].connect (refresh_auto_controllers);
    }

    public void disconnect_from_gamepad (Gamepad gamepad, uint index) {
        gamepad.notify["connected"].disconnect (refresh_auto_controllers);

        for (uint p = 0; p < N_PLAYERS; p++) {
            if (get_gamepad (p) == gamepad)
                set_controller (p, true, null, null);
        }
    }

    private void refresh_auto_controllers () {
        var gamepad_manager = GamepadManager.get_instance ();
        Gamepad[] used_gamepads = {}, available_gamepads = {};
        bool keyboard_available = true;
        bool touchscreen_available = touch_enabled;

        for (uint player = 0; player < N_PLAYERS; player++) {
            if (get_automatic (player))
                continue;

            var type = get_controller_type (player);

            if (type == KEYBOARD) {
                keyboard_available = false;
                continue;
            }

            if (type == TOUCHSCREEN) {
                touchscreen_available = false;
                continue;
            }

            if (type == GAMEPAD) {
                used_gamepads += get_gamepad (player);
                continue;
            }
        }

        uint n = gamepad_manager.gamepads.n_items;
        for (uint i = 0; i < n; i++) {
            var gamepad = gamepad_manager.gamepads.get_item (i) as Gamepad;

            if (gamepad.connected && !(gamepad in used_gamepads))
                available_gamepads += gamepad;
        }

        if (available_gamepads.length > 0)
            touchscreen_available = false;

        uint index = 0;
        for (uint player = 0; player < N_PLAYERS; player++) {
            if (!get_automatic (player))
                continue;

            ControllerData data = {};
            data.automatic = true;

            if (touchscreen_available && available_gamepads.length == 0) {
                data.type = TOUCHSCREEN;
                touchscreen_available = false;
            } else if (index < available_gamepads.length) {
                data.type = GAMEPAD;
                data.gamepad = available_gamepads[index];
            } else if (keyboard_available) {
                data.type = KEYBOARD;
                keyboard_available = false;
            } else {
                data.type = NONE;
            }

            var old_data = controller_data[player];
            if (data.type != old_data.type || data.gamepad != old_data.gamepad) {
                controller_data[player] = data;
                controller_changed (player);
            }

            index++;
        }
    }

    private void update_touch_cb () {
        if (!touch_enabled) {
            for (int i = 0; i < N_PLAYERS; i++) {
                if (controller_data[i].type == TOUCHSCREEN)
                    set_controller (i, true, null, null);
            }
        }

        refresh_auto_controllers ();
    }
}
