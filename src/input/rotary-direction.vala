// This file is part of Highscore. License: GPL-3.0-or-later

public enum Highscore.RotaryDirection {
    LEFT,
    RIGHT;

    public const int N_DIRECTIONS = 2;

    public static RotaryDirection? from_string (string str) {
        if (str == "left")
            return LEFT;
        if (str == "right")
            return RIGHT;
        return null;
    }

    public string to_string () {
        switch (this) {
            case LEFT:
                return "left";
            case RIGHT:
                return "right";
            default:
                assert_not_reached ();
        }
    }

    public string get_display_name () {
        switch (this) {
            case LEFT:
                return _("Left");
            case RIGHT:
                return _("Right");
            default:
                assert_not_reached ();
        }
    }

    public double get_direction () {
        switch (this) {
            case LEFT:
                return -1;
            case RIGHT:
                return 1;
            default:
                assert_not_reached ();
        }
    }

    public static RotaryDirection[] all () {
        return { LEFT, RIGHT };
    }
}
