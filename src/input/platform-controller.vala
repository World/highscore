// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.PlatformController : Object {
    public string id { get; construct; }
    public string name { get; construct; }
    public string? icon_name { get; construct; }
    public PlatformKeyboardMapping default_keyboard_mapping { get; set; }
    public PlatformGamepadMapping default_gamepad_mapping { get; set; }
    public string? touch_overlay_name { get; set; }

    private ControlSection[] sections;
    private HashTable<string, ControlSection> id_to_section;
    private HashTable<string, ControlSection> control_id_to_section;
    private HashTable<int, Control> pointer_controls;

    public string? similar_to { get; set; default = null; }

    public PlatformController (string id, string name, string? icon_name = null) {
        Object (id: id, name: name, icon_name: icon_name);

        sections = {};
        id_to_section = new HashTable<string, ControlSection> (str_hash, str_equal);
        control_id_to_section = new HashTable<string, ControlSection> (str_hash, str_equal);
        pointer_controls = new HashTable<int, Control> (direct_hash, direct_equal);

        touch_overlay_name = id;
    }

    public void add_section (string id) requires (!(id in id_to_section)) {
        var section = new ControlSection (this, id);
        sections += section;
        id_to_section[id] = section;
    }

    public void add_sections (...) {
        var l = va_list ();

        while (true) {
            string? id = l.arg ();
            if (id == null)
                break;

            add_section (id);
        }
    }

    public Control? get_control (string id) {
        var section = control_id_to_section[id];

        return section?.get_control (id);
    }

    public ControlSection[] list_sections () {
        return sections;
    }

    public int[] list_screens () {
        var ret = new GenericArray<int> (pointer_controls.size ());

        foreach (int screen in pointer_controls.get_keys ())
            ret.add (screen);

        ret.sort ((a, b) => {
            if (a < b)
                return -1;
            if (a > b)
                return 1;
            return 0;
        });

        return ret.steal ();
    }

    public void add_control (string section_id, Control control) requires (get_control (control.id) == null) {
        var section = id_to_section[section_id];
        assert (section != null);

        control_id_to_section[control.id] = section;
        section.add_control (control);
    }

    public Control get_for_screen (int screen_id) {
        return pointer_controls[screen_id];
    }

    public void set_for_screen (int screen_id, Control control) requires (get_control (control.id) != null) {
        pointer_controls[screen_id] = control;
    }
}
