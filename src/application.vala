// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Application : Adw.Application {
    private bool games_loaded;

    public weak MainWindow main_window;

    public Application () {
        Object (
            application_id: Config.APPLICATION_ID,
            flags: ApplicationFlags.HANDLES_OPEN | ApplicationFlags.HANDLES_COMMAND_LINE
        );
    }

    construct {
        ActionEntry[] action_entries = {
            { "open", on_open_action },
            { "about", on_about_action },
            { "preferences", on_preferences_action },
            { "quit", on_quit_action }
        };
        add_action_entries (action_entries, this);
        set_accels_for_action ("app.open",        {"<primary>o"});
        set_accels_for_action ("app.preferences", {"<primary>comma"});
        set_accels_for_action ("app.quit",        {"<primary>q"});

        OptionEntry[] options = {
            { "uid",            'u',  NONE, STRING,         null, N_("Launch the game with specified uid") },
            { "search",         's',  NONE, STRING,         null, N_("Show games with the given filter") },
            { "version",        'v',  NONE, NONE,           null, N_("Show the current version of Highscore") },
            { OPTION_REMAINING, 0,    NONE, FILENAME_ARRAY, null, null, N_("[FILE…]") },
            { null }
        };
        add_main_option_entries (options);
    }

    protected override void startup () {
        set_resource_base_path ("/app/drey/Highscore");

        base.startup ();

        style_manager.color_scheme = PREFER_DARK;

        FeedbackdRumble.get_instance ().start.begin ();
    }

    protected override int handle_local_options (VariantDict options) {
        if (options.contains ("version")) {
            print ("%s\n", Config.VERSION);
            return 0;
        }

        return -1;
    }

    public override int command_line (ApplicationCommandLine command_line) {
        var options = command_line.get_options_dict ();

        if (options.contains ("uid")) {
            var uid_value = options.lookup_value ("uid", VariantType.STRING);
            var uid = uid_value.get_string ();

            string? search_terms = null;

            if (options.contains ("search")) {
                var search_value = options.lookup_value (
                    "search", VariantType.STRING
                );
                search_terms = search_value.get_string ();
           }

            hold ();
            run_by_uid.begin (uid, search_terms, () => release ());

            return 0;
        }

        if (options.contains ("search")) {
            var search_value = options.lookup_value (
                "search", VariantType.STRING
            );
            var search_terms = search_value.get_string ();

            activate ();
            main_window.run_search (search_terms);

            return 0;
        }

        if (options.contains (OPTION_REMAINING)) {
            File[] files = {};

            var remaining_value = options.lookup_value (
                OPTION_REMAINING, VariantType.BYTESTRING_ARRAY
            );
            var remaining = remaining_value.get_bytestring_array ();

            foreach (var filename in remaining)
                files += command_line.create_file_for_arg (filename);

            open (files, "");

            return 0;
        }

        activate ();

        return 0;
    }

    protected override void activate () {
        base.activate ();

        if (main_window == null) {
            var win = new MainWindow (this);
            main_window = win;
        }

        main_window.present ();

        load_games.begin (() => main_window.show_games ());
    }

    public async void load_games () {
        if (games_loaded)
            return;

        bool show_welcome = !LibraryUtils.has_library_dir ();

        if (WELCOME in Debug.get_flags ())
            show_welcome = true;

        if (show_welcome) {
            var dialog = new WelcomeDialog ();

            dialog.closed.connect (() => load_games.callback ());

            dialog.present (main_window);

            yield;
        } else {
            var library = Library.get_instance ();

            yield library.load_games ();
        }

        games_loaded = true;
    }

    protected override void open (File[] files, string hint) {
        hold ();
        run_files.begin (files);
        release ();
    }

    private void on_open_action () {
        open_action_do.begin ();
    }

    private async void open_action_do () {
        try {
            var files = yield open_files ();

            if (files.length == 0)
                return;

            yield run_files (files);
        } catch (Error e) {
            var window = new StandaloneWindow (this);
            window.show_error (_("Failed to open file: %s").printf (e.message));
            window.present ();
        }
    }

    private async File[] open_files () throws Error {
        var dialog = new Gtk.FileDialog ();
        ListModel? files_model = null;

        var register = PlatformRegister.get_register ();

        var filters = new ListStore (typeof (Gtk.FileFilter));
        var all_filter = new Gtk.FileFilter ();
        all_filter.name = _("All Games");

        foreach (var mime_type in register.list_mime_types ())
            all_filter.add_mime_type (mime_type);

        filters.append (all_filter);

        foreach (var platform in register.get_all_platforms ()) {
            var filter = new Gtk.FileFilter ();
            filter.name = platform.name;

            foreach (var mime_type in platform.get_mime_types ())
                filter.add_mime_type (mime_type);

            filters.append (filter);
        }

        dialog.filters = filters;

        dialog.title = _("Open Game");

        try {
            files_model = yield dialog.open_multiple (active_window, null);
        } catch (Gtk.DialogError e) {
            return {};
        }

        if (files_model == null)
            return {};

        File[] files = {};

        for (uint i = 0; i < files_model.get_n_items (); i++)
            files += files_model.get_item (i) as File;

        return files;
    }

    public RunnerWindow? get_window_for_game (Game game) {
        foreach (var window in get_windows ()) {
            if (!(window is RunnerWindow))
                continue;

            var runner_window = window as RunnerWindow;
            var runner = runner_window.runner;

            if (runner == null)
                continue;

            var running_game = runner.game;

            if (running_game == null)
                continue;

            if (running_game.uid == game.uid)
                return runner_window;
        }

        return null;
    }

    public bool try_focus_existing_window (Game game) {
        var window = get_window_for_game (game);

        if (window == null)
            return false;

        window.present ();
        return true;
    }

    private async void run_files_error_clicked (GameLoader loader, StandaloneWindow window) {
        try {
            var new_files = yield open_files ();

            loader.add_files (new_files);
        } catch (Error e) {
            window.show_error (_("Failed to open file: %s").printf (e.message));
        }
    }

    private async void run_files (owned File[] files) {
        Game? game = null;

        hold ();

        var window = new StandaloneWindow (this);
        window.present ();

        if (LibraryUtils.has_library_dir ())
            yield load_games ();

        var loader = new GameLoader ();
        loader.add_files (files);

        window.error_clicked.connect (() => {
            run_files_error_clicked.begin (loader, window, () => {
                run_files.callback ();
            });
        });

        do {
            window.reset ();

            Error? error = null;

            try {
                game = yield loader.try_load ();
            } catch (Error e) {
                error = e;
            }

            if (!window.visible) {
                release ();
                return;
            }

            if (error != null) {
                try {
                    throw error;
                } catch (GameLoaderError.MISSING_MEDIA e) {
                    window.show_error_with_button (
                        e.message,
                        _("_Add Files")
                    );

                    ulong close_request_cb_id = window.close_request.connect (() => {
                        window = null;
                        run_files.callback ();
                        return Gdk.EVENT_PROPAGATE;
                    });

                    yield;

                    if (window == null) {
                        release ();
                        return;
                    } else {
                        window.disconnect (close_request_cb_id);
                    }
                } catch (Error e) {
                    window.show_error (e.message);

                    release ();
                    return;
                }
            }
        } while (game == null);

        var library_game = Library.get_instance ().find_game_by_uid (game.uid);
        if (library_game != null)
            game = library_game;

        if (try_focus_existing_window (game)) {
            window.close ();
            release ();
            return;
        }

        yield window.run_game (game);
        release ();
    }

    public async void run_game_standalone (Game game) {
        if (try_focus_existing_window (game))
            return;

        var window = new StandaloneWindow (this);

        yield window.run_game (game);
        window.present ();
    }

    private async void run_by_uid (string uid, string? search_terms) {
        yield load_games ();

        var game = Library.get_instance ().find_game_by_uid (uid);

        if (game == null) {
            print ("No game found with uid: %s\n", uid);
            return;
        }

        if (try_focus_existing_window (game))
            return;

        if (main_window == null) {
            var win = new MainWindow (this);
            main_window = win;

            main_window.show_games ();
        }

        if (main_window != null && main_window.runner == null) {
            if (search_terms != null)
                main_window.run_search (search_terms);

            yield main_window.run_game (game);
            main_window.present ();

            return;
        }

        yield run_game_standalone (game);
    }


    private void on_about_action () {
        var appdata = "/app/drey/Highscore/app.drey.Highscore.metainfo.xml";
        string[] developers = { _("Alice Mikhaylenko") };

        var about = new Adw.AboutDialog.from_appdata (appdata, null) {
            version = @"$(Config.VERSION)$(Config.VERSION_SUFFIX)",
            developers = developers,
            copyright = "© 2023-2025 Alice Mikhaylenko",
            translator_credits = _("translator-credits"),
            debug_info = DebugInfo.get_debug_info (),
            debug_info_filename = "highscore-debug-info.txt",
        };

        about.add_link (_("_Chat"), "https://matrix.to/#/#highscore:gnome.org");

        var cores = CoreRegister.get_register ().get_cores ();

        foreach (var core in cores) {
            about.add_credit_section (core.name, core.authors);

            about.add_legal_section (
                core.name,
                string.joinv ("\n", core.copyright),
                LicenseUtils.spdx_to_license (core.license),
                null
            );
        }

        var filters = VideoFilterRegister.get_instance ().get_unique_internal_filters ();

        foreach (var filter in filters) {
            var title = _("%s Shader").printf (filter.internal_name ?? filter.display_name);
            var authors = filter.authors;
            var copyright = filter.copyright;
            var license = filter.license;
            var license_text = filter.license_text;

            if (authors != null)
                about.add_credit_section (title, authors);

            if (authors != null || copyright != null || license != UNKNOWN || license_text != null) {
                string? copyright_str = null;

                if (copyright != null)
                    copyright_str = string.joinv ("\n", copyright);
                else if (authors != null)
                    copyright_str = _("By %s").printf (string.joinv (", ", authors));

                about.add_legal_section (title, copyright_str, license, license_text);
            }
        }

        about.add_credit_section (
            // Translators: Game Boy Color, Game Boy Advance, Nintendo DS
            _("GBC, GBA, NDS Color Correction Shaders"),
            { "hunterk", "Pokefan531" }
        );

        about.present (active_window);
    }

    private void on_preferences_action () {
        new PreferencesDialog ().present (active_window);
    }

    private void on_quit_action () {
        if (WidgetUtils.get_visible_dialog (active_window) != null)
            return;

        var windows = get_windows ().copy ();

        foreach (var w in windows) {
            while (true) {
                var dialog = WidgetUtils.get_visible_dialog (w);

                if (dialog == null)
                    break;

                if (!dialog.close ())
                    return;
            }

            w.close ();
        }
    }
}
