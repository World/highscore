// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.LibraryUtils {
    public bool has_library_dir () {
        var settings = new Settings ("app.drey.Highscore");

        return settings.get_string ("library-dir") != "";
    }

    public File get_cover_cache_dir () {
        var cache_path = Path.build_filename (
            Environment.get_user_cache_dir (), "highscore", "covers"
        );

        return File.new_for_path (cache_path);
    }

    public File get_cover_override (Game game) {
        var override_path = Path.build_filename (
            Environment.get_user_data_dir (),
            "highscore",
            "covers",
            game.uid
        );

        return File.new_for_path (override_path);
    }

    public File? find_cover (Game game, Cancellable? cancellable) {
        foreach (var media in game.get_media ()) {
            var cover = find_cover_for_media (media, cancellable);
            if (cover != null)
                return cover;
        }

        return null;
    }

    private File? find_cover_for_media (Media media, Cancellable? cancellable) {
        var cover = find_cover_for_file (media.file, cancellable);
        if (cover != null)
            return cover;

        if (cancellable != null && cancellable.is_cancelled ())
            return null;

        File? main_parent = null;

        foreach (var path in media.extra_paths) {
            if (cancellable != null && cancellable.is_cancelled ())
                return null;

            var file = File.new_for_path (path);
            var parent = file.get_parent ();

            if (main_parent == null)
                main_parent = media.file.get_parent ();

            if (parent != null && main_parent != null && parent.equal (main_parent))
                continue;

            cover = find_cover_for_file (file, cancellable);
            if (cover != null)
                return cover;
        }

        return null;
    }

    public File? find_cover_for_file (File file, Cancellable? cancellable) {
        var parent = file.get_parent ();
        if (parent == null)
            return null;

        if (cancellable != null && cancellable.is_cancelled ())
            return null;

        FileEnumerator enumerator;

        try {
            enumerator = parent.enumerate_children (
                FileAttribute.STANDARD_NAME + "," +
                FileAttribute.STANDARD_TYPE + "," +
                FileAttribute.STANDARD_CONTENT_TYPE,
                NOFOLLOW_SYMLINKS,
                cancellable
            );
        } catch (Error e) {
            critical (
                "Failed to search for covers in %s: %s",
                parent.peek_path (), e.message
            );
            return null;
        }

        FileInfo info = null;

        var rom_name = file.get_basename ();
        string rom_basename;
        FileUtils.split_filename (rom_name, out rom_basename, null);

        File[] covers = {};

        while (true) {
            try {
                info = enumerator.next_file (cancellable);
            } catch (Error e) {
                critical (
                    "Failed to enumerate file: %s", e.message
                );
            }

            if (cancellable != null && cancellable.is_cancelled ())
                return null;

            if (info == null)
                break;

            var name = info.get_name ();
            var file_type = info.get_file_type ();

            if (file_type == DIRECTORY)
                continue;

            if (name == rom_name)
                continue;

            string basename;
            FileUtils.split_filename (name, out basename, null);

            if (basename == rom_basename ||
                basename == "cover" ||
                basename == "folder") {
                var content_type = info.get_content_type ();
                var mime_type = ContentType.get_mime_type (content_type);

                if (mime_type != "image/png" &&
                    mime_type != "image/jpeg" &&
                    mime_type != "image/tiff") {
                    continue;
                }

                covers += parent.get_child (name);
            }
        }

        if (covers.length == 0)
            return null;

        qsort_with_data <File> (covers, sizeof (File), (a, b) => {
            return strcmp (a.peek_path (), b.peek_path ());
        });

        return covers[0];
    }

    public string? get_common_name (Media[] media) {
        string[] names = {};

        foreach (var m in media) {
            string basename;

            FileUtils.split_filename (
                m.file.get_basename (), out basename, null
            );

            names += basename;
        }

        assert (names.length > 0);

        if (names.length == 1)
            return names[0];

        int min_length = names[0].char_count ();

        for (int i = 1; i < names.length; i++) {
            var str1 = names[0];
            var str2 = names[i];

            if (str1 == str2) {
                min_length = int.min (min_length, str1.char_count ());
                continue;
            }

            var components1 = str1.split ("(");
            var components2 = str2.split ("(");

            for (int j = 0; j < int.min (components1.length, components2.length); j++) {
                var c1 = components1[j].strip ();
                var c2 = components2[j].strip ();

                if (c1 != c2) {
                    min_length = int.min (min_length, str1.index_of (c1) - 1);
                    break;
                }
            }
        }

        if (min_length <= 0)
            return null;

        int len = names[0].index_of_nth_char (min_length);
        var ret = names[0].substring (0, len).strip ();

        if (ret == "")
            return null;

        return ret;
    }
}
