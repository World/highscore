// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.CoverLoader : Object {
    private const float DIMMING = 0.2f;

    private static CoverLoader? instance;

    private Thread<void> thread;
    private Gsk.CairoRenderer renderer;
    private AsyncQueue<CoverRequest?> request_queue;

    private delegate void CoverRequestCallback (Gdk.Texture? texture);

    private struct CoverRequest {
        Game? game;
        int size;
        Cancellable cancellable;
        unowned CoverRequestCallback callback;
    }

    public static CoverLoader get_instance () {
        if (instance == null)
            instance = new CoverLoader ();

        return instance;
    }

    construct {
        request_queue = new AsyncQueue<CoverRequest?> ();
        renderer = new Gsk.CairoRenderer ();

        try {
            renderer.realize_for_display (Gdk.Display.get_default ());
        } catch (Error e) {
            error ("Failed to realize cover thumbnail renderer: %s", e.message);
        }

        thread = new Thread<void> (null, run_thread);
    }

    ~CoverLoader () {
        request_queue.push ({ null, 0, null, null });

        thread.join ();
        thread = null;

        renderer.unrealize ();
    }

    private void run_thread () {
        while (true) {
            var request = request_queue.pop ();

            if (request.game == null)
                return;

            if (request.callback == null)
                continue;

            var texture = load_cover (request);

            request.callback (texture);
        }
    }

    private File get_cache_file (CoverRequest request) {
        var cache_path = Path.build_filename (
            LibraryUtils.get_cover_cache_dir ().get_path (),
            request.size.to_string (),
            @"$(request.game.uid).png"
        );

        return File.new_for_path (cache_path);
    }

    private Gdk.Texture? get_cached_cover (CoverRequest request) {
        var file = get_cache_file (request);
        if (!file.query_exists ())
            return null;

        try {
            return Gdk.Texture.from_file (file);
        } catch (Error e) {
            critical (
                "Failed to load texture for %s from cache: %s",
                request.game.uid, e.message
            );
            return null;
        }
    }

    private Gdk.Texture? get_override_cover (CoverRequest request) {
        var file = LibraryUtils.get_cover_override (request.game);
        if (!file.query_exists ())
            return null;

        try {
            return Gdk.Texture.from_file (file);
        } catch (Error e) {
            critical (
                "Failed to load texture for %s from overrides: %s",
                request.game.uid, e.message
            );
            return null;
        }
    }

    private Gdk.Texture? get_local_cover (CoverRequest request) {
        var cover_file = request.game.cover;
        if (cover_file == null)
            return null;

        try {
            return Gdk.Texture.from_file (cover_file);
        } catch (Error e) {
            critical (
                "Failed to load texture for %s from %s: %s",
                request.game.uid, cover_file.peek_path (), e.message
            );
            return null;
        }
    }

    private void snapshot_thumbnail (Gtk.Snapshot snapshot, Gdk.Texture cover, int size) {
        float aspect_ratio = (float) cover.width / (float) cover.height;
        Graphene.Rect cover_rect = {};

        float width = size, height = size;

        if (aspect_ratio < 1)
            width = aspect_ratio * (float) size;
        else
            height = (float) size / aspect_ratio;

        if (width >= size - 2)
            width = size;

        if (height >= size - 2)
            height = size;

        cover_rect.origin.x = (size - width) / 2.0f;
        cover_rect.origin.y = (size - height) / 2.0f;
        cover_rect.size.width = width;
        cover_rect.size.height = height;

        snapshot.push_clip ({{ 0, 0 }, { size, size }});

        snapshot.append_color ({ 0, 0, 0, 1 }, {{ 0, 0 }, { size, size }});

        if (width < size || height < size) {
            Graphene.Rect outer_rect = {};

            float blur_radius = size / 4;

            if (aspect_ratio < 1) {
                outer_rect.size.width = size + blur_radius * 2;
                outer_rect.size.height = outer_rect.size.width / aspect_ratio;
            } else {
                outer_rect.size.height = size + blur_radius * 2;
                outer_rect.size.width = aspect_ratio * outer_rect.size.height;
            }

            outer_rect.origin.x = (size - outer_rect.size.width) / 2.0f;
            outer_rect.origin.y = (size - outer_rect.size.height) / 2.0f;

            snapshot.push_blur (blur_radius);

            snapshot.append_texture (cover, outer_rect);

            snapshot.pop ();

            snapshot.append_color ({ 0, 0, 0, DIMMING }, outer_rect);
        }

        snapshot.append_scaled_texture (cover, TRILINEAR, cover_rect);

        snapshot.pop ();
    }

    private Gdk.Texture? cache_cover (CoverRequest request, Gdk.Texture cover) {
        var snapshot = new Gtk.Snapshot ();

        snapshot_thumbnail (snapshot, cover, request.size);

        var node = snapshot.free_to_node ();
        var texture = renderer.render_texture (
            node, {{ 0, 0 }, { request.size, request.size }}
        );

        var file = get_cache_file (request);

        try {
            var dir = file.get_parent ();
            if (!dir.query_exists ())
                dir.make_directory_with_parents ();
        } catch (Error e) {
            critical ("Failed to create cover cache: %s", e.message);
            return texture;
        }

        texture.save_to_png (file.get_path ());

        return texture;
    }

    private Gdk.Texture? load_cover (CoverRequest request) {
        if (request.cancellable.is_cancelled ())
            return null;

        var cached_cover = get_cached_cover (request);
        if (cached_cover != null)
            return cached_cover;

        if (request.cancellable.is_cancelled ())
            return null;

        var override_cover = get_override_cover (request);
        if (override_cover != null)
            return cache_cover (request, override_cover);

        if (request.cancellable.is_cancelled ())
            return null;

        var local_cover = get_local_cover (request);
        if (local_cover != null)
            return cache_cover (request, local_cover);

        return null;
    }

    public async Gdk.Texture? request_cover (Game game, int size, Cancellable cancellable) {
        Gdk.Texture? texture = null;
        CoverRequestCallback callback = t => {
            texture = t;

            Idle.add_once (() => request_cover.callback ());
        };

        request_queue.push ({ game, size, cancellable, callback });
        yield;

        return texture;
    }
}
