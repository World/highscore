// This file is part of Highscore. License: GPL-3.0-or-later

errordomain Highscore.GameLoaderError {
    NOT_A_GAME,
    MISSING_MEDIA,
}

private class Highscore.GameLoader : Object {
    private File[] files;

    private GenericSet<string> uris;

    construct {
        files = {};
        uris = new GenericSet<string> (str_hash, str_equal);
    }

    public void add_files (File[] new_files) {
        foreach (var f in new_files) {
            var uri = f.get_uri ();

            if (uri in uris)
                continue;

            files += f;
            uris.add (uri);
        }
    }

    public async Game try_load () throws Error requires (files.length > 0) {
        Mutex mutex = {};

        File main_file = files[0];

        Game game = null;
        var media_set_parsers = new HashTable<string, GameParser> (str_hash, str_equal);
        GameParser main_parser = null;

        var pool = new ThreadPool<File>.with_owned_data (
            file => {
                string mime_type;

                try {
                    var info = file.query_info (
                        FileAttribute.STANDARD_CONTENT_TYPE,
                        NONE
                    );

                    var content_type = info.get_content_type ();
                    mime_type = ContentType.get_mime_type (content_type);
                } catch (Error e) {
                    critical (
                        "Failed to get MIME type from %s: %s\n",
                        file.get_path (), e.message
                    );

                    return;
                }

                var platforms = PlatformRegister.get_register ().list_platforms_for_mime_type (mime_type);

                foreach (var platform in platforms) {
                    try {
                        var parser = GameParser.create (platform, file);

                        if (parser.parse ()) {
                            if (parser.get_incomplete ()) {
                                mutex.lock ();

                                if (parser.current_media in media_set_parsers) {
                                    mutex.unlock ();
                                    return;
                                }

                                if (file == main_file)
                                    main_parser = parser;

                                media_set_parsers[parser.current_media] = parser;
                                mutex.unlock ();
                                return;
                            }

                            var media = parser.create_media ();
                            var local_game = parser.create_game ({ media });

                            mutex.lock ();

                            if (game == null && local_game != null)
                                game = local_game;

                            mutex.unlock ();

                            return;
                        }
                    } catch (Error e) {
                        critical (
                            "Failed to parse the game %s: %s\n",
                            file.get_path (), e.message
                        );
                        continue;
                    }
                }
            }, int.min ((int) get_num_processors (), files.length), true
        );

        foreach (var file in files)
            pool.add (file);

        new Thread<void> ("wait-thread", () => {
            ThreadPool.free ((owned) pool, false, true);
            try_load.callback ();
        });

        yield;

        if (main_parser != null) {
            Media[] media = {};

            var media_set = main_parser.get_media_set ();
            foreach (var id in media_set) {
                if (!(id in media_set_parsers)) {
                    media += null;
                    continue;
                }

                try {
                    media += media_set_parsers[id].create_media ();
                } catch (Error e) {
                    critical ("Failed to create media: %s", e.message);
                }
            }

            string[] missing_media = {};
            for (int i = 0; i < media.length; i++) {
                if (media[i] == null)
                    missing_media += _("disc %d").printf (i + 1);
            }

            if (missing_media.length > 0) {
                var joined = string.joinv (", ", missing_media);

                throw new GameLoaderError.MISSING_MEDIA (
                    _("Missing the following media: %s"), joined
                );
            }

            game = main_parser.create_game (media);
        }

        if (game == null)
            throw new GameLoaderError.NOT_A_GAME (_("Unsupported format"));

        var library = Library.get_instance ();

        game.title = library.find_title (game) ?? game.default_title;
        game.cover = LibraryUtils.find_cover (game, null);

        return game;
    }
}
