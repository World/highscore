// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.GameCover : Gtk.Widget {
    private const int MAX_COVER_SIZE = 256;

    private Game _game;
    public Game game {
        get { return _game; }
        set {
            if (game == value)
                return;

            if (game != null)
                game.cover_changed.disconnect (reload_cover_cb);

            _game = value;

            if (game != null) {
                empty.icon_name = game.platform.icon_name;
                stack.visible_child = empty_bin;
                cover1.paintable = null;
                cover2.paintable = null;

                reload_cover_cb ();

                game.cover_changed.connect (reload_cover_cb);
            } else {
                cancellable.cancel ();
            }
        }
    }

    public bool width_for_height { get; set; }

    private Gtk.Stack stack;
    private Adw.Bin empty_bin;
    private Gtk.Image empty;
    private Gtk.Image cover1;
    private Gtk.Image cover2;

    private uint map_idle_id;
    private Cancellable cancellable;
    private bool has_cover;

    construct {
        overflow = HIDDEN;

        stack = new Gtk.Stack () {
            transition_type = CROSSFADE,
        };

        empty = new Gtk.Image () {
            halign = CENTER,
            valign = CENTER,
        };

        empty_bin = new Adw.Bin () {
            child = empty,
        };
        empty_bin.add_css_class ("empty");
        stack.add_child (empty_bin);

        cover1 = new Gtk.Image ();
        stack.add_child (cover1);

        cover2 = new Gtk.Image ();
        stack.add_child (cover2);

        stack.visible_child = empty_bin;

        stack.set_parent (this);

        notify["width-for-height"].connect (queue_resize);

        cancellable = new Cancellable ();
    }

    static construct {
        set_css_name ("game-cover");
    }

    private void reload_cover_cb () {
        has_cover = false;

        if (get_mapped ())
            start_loading_cover.begin ();
    }

    private async void start_loading_cover () {
        cancellable.cancel ();
        cancellable.reset ();

        assert (get_native () != null);

        double scale = get_native ().get_surface ().scale;
        int size = (int) Math.ceil (MAX_COVER_SIZE * scale);

        var texture = yield CoverLoader.get_instance ().request_cover (
            game, size, cancellable
        );

        if (texture == null) {
            stack.visible_child = empty_bin;
        } else {
            var new_cover = (stack.visible_child == cover1) ? cover2 : cover1;

            new_cover.paintable = texture;
            stack.visible_child = new_cover;
        }

        has_cover = true;
    }

    protected override void map () {
        base.map ();

        if (has_cover)
            return;

        map_idle_id = Idle.add_once (() => {
            map_idle_id = 0;

            if (game == null)
                return;

            start_loading_cover.begin ();
        });
    }

    protected override void unmap () {
        if (map_idle_id > 0) {
            Source.remove (map_idle_id);
            map_idle_id = 0;
        }

        cancellable.cancel ();
        cancellable.reset ();

        base.unmap ();
    }

    protected override void realize () {
        base.realize ();

        get_native ().get_surface ().notify["scale"].connect (reload_cover_cb);
    }

    protected override void unrealize () {
        get_native ().get_surface ().notify["scale"].disconnect (reload_cover_cb);

        base.unrealize ();
    }

    protected override void dispose () {
        if (stack != null) {
            stack.unparent ();
            stack = null;
            empty = null;
            empty_bin = null;
            cover1 = null;
            cover2 = null;
        }

        base.dispose ();
    }

    protected override Gtk.SizeRequestMode get_request_mode () {
        if (width_for_height)
            return WIDTH_FOR_HEIGHT;
        else
            return HEIGHT_FOR_WIDTH;
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int minimum,
        out int natural,
        out int minimum_baseline,
        out int natural_baseline
    ) {
        stack.measure (orientation, for_size, out minimum, null, null, null);

        minimum = int.max (minimum, 64);

        if ((orientation == HORIZONTAL) == width_for_height)
            natural = for_size >= 0 ? for_size : 64;
        else
            natural = int.min (minimum, 64);

        minimum_baseline = natural_baseline = -1;
    }

    protected override void size_allocate (int width, int height, int baseline) {
        stack.allocate (width, height, baseline, null);
   }
}
