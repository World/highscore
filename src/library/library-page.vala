// This file is part of Highscore. License: GPL-3.0-or-later

public enum Highscore.LibraryViewType {
    ALL,
    RECENT,
    PLATFORM
}

private class Highscore.LibrarySidebarRow : Gtk.ListBoxRow {
    public LibraryViewType view_type { get; construct; }
    public Platform? platform { get; construct; }

    private Gtk.Button firmware_button;

    public LibrarySidebarRow (LibraryViewType view_type, Platform? platform) {
        Object (view_type: view_type, platform: platform);
    }

    construct {
        height_request = 44;

        string name, icon_name;

        switch (view_type) {
            case ALL:
                name = _("All Games");
                icon_name = "all-games-symbolic";
                break;
            case RECENT:
                name = _("Recently Played");
                icon_name = "recent-games-symbolic";
                break;
            case PLATFORM:
                assert (platform != null);
                name = platform.name;
                icon_name = platform.icon_name;
                break;
            default:
                assert_not_reached ();
        }

        var icon = new Gtk.Image.from_icon_name (icon_name) {
            pixel_size = 24,
        };
        icon.add_css_class ("sidebar-icon");

        var label = new Gtk.Label (name) {
            xalign = 0,
            ellipsize = END,
            hexpand = true,
        };

        var box = new Gtk.Box (HORIZONTAL, 12) {
            margin_start = 3,
        };
        box.append (icon);
        box.append (label);

        child = box;

        if (platform != null && platform.firmware_list != null) {
            firmware_button = new Gtk.Button.from_icon_name ("dialog-warning-symbolic") {
                visible = false,
                valign = CENTER,
                tooltip_text = _("Firmware Missing"),
                action_name = "library.open-firmware-preferences",
            };
            firmware_button.add_css_class ("sidebar-button");

            box.append (firmware_button);

            update_firmware_icon ();

            platform.firmware_list.changed.connect (update_firmware_icon);
        }
    }

    private void update_firmware_icon () {
        try {
            platform.firmware_list.check ();
            firmware_button.visible = false;
            tooltip_text = null;
        } catch (Error e) {
            firmware_button.visible = true;
            tooltip_text = e.message;
        }
    }
}

[GtkTemplate (ui = "/app/drey/Highscore/library/library-page.ui")]
public class Highscore.LibraryPage : Adw.NavigationPage {
    public signal void game_activated (Game game);
    public signal void add_toast (Adw.Toast toast);

    [GtkChild]
    private unowned Gtk.Stack loading_stack;
    [GtkChild]
    private unowned Gtk.ProgressBar progress;
    [GtkChild]
    private unowned Gtk.Label progress_label;
    [GtkChild]
    private unowned Adw.OverlaySplitView split_view;
    [GtkChild]
    private unowned Gtk.ListBox sidebar;
    [GtkChild]
    private unowned Adw.WindowTitle content_title;
    [GtkChild]
    private unowned Gtk.SearchBar search_bar;
    [GtkChild]
    private unowned Gtk.SearchEntry search_entry;
    [GtkChild]
    private unowned Gtk.Stack content_stack;
    [GtkChild]
    private unowned Adw.StatusPage empty_page;
    [GtkChild]
    private unowned Gtk.Box empty_page_child;
    [GtkChild]
    private unowned Gtk.Label rom_hint;
    [GtkChild]
    private unowned Gtk.GridView grid_view;
    [GtkChild]
    private unowned GamepadHintBar gamepad_hint_bar;

    public LibraryViewType current_view_type { get; private set; }
    public Platform? current_platform { get; private set; }

    private Settings settings;

    private LibrarySidebarRow all_games_row;
    private LibrarySidebarRow recent_row;

    private Gtk.StringFilter search_filter;
    private Gtk.CustomFilter view_filter;
    private Gtk.CustomSorter game_sorter;
    private Gtk.SingleSelection selection;

    public bool compact { get; set; }
    public bool search_mode { get; set; }

    public bool show_empty_platforms { get; set; }

    private LibraryModel model;
    private bool can_type_to_search;

    private ulong model_items_changed_id;
    private ulong model_notify_empty_id;

    private bool focusing_sidebar;

    private Cancellable? rescan_cancellable;
    private Adw.Toast? undo_toast;

    construct {
        settings = new Settings ("app.drey.Highscore");

        settings.bind ("show-empty-platforms", this, "show-empty-platforms", GET);

        var controller = new GamepadController (this);
        gamepad_hint_bar.controller = controller;

        controller.navigate.connect (dir => {
            bool is_rtl = get_direction () == RTL;

            if (is_rtl && (dir == LEFT || dir == RIGHT))
                dir = dir.opposite ();

            if (focusing_sidebar) {
                // We're in sidebar, handle that
                var selected_row = sidebar.get_selected_row ();

                if (selected_row == null)
                    return;

                var selected = selected_row.get_index ();

                switch (dir) {
                    case UP:
                        while (true) {
                            selected--;

                            var new_row = sidebar.get_row_at_index (selected);
                            if (new_row == null || (new_row.visible && new_row.sensitive))
                                break;
                        }
                        break;
                    case DOWN:
                        while (true) {
                            selected++;

                            var new_row = sidebar.get_row_at_index (selected);
                            if (new_row == null || (new_row.visible && new_row.sensitive))
                                break;
                        }
                        break;
                    case LEFT:
                        get_display ().beep ();
                        break;
                    case RIGHT:
                        focus_grid ();
                        break;
                    default:
                        assert_not_reached ();
                }

                var new_row = sidebar.get_row_at_index (selected);

                if (new_row != null) {
                    sidebar.select_row (new_row);
                    sidebar.get_selected_row ().grab_focus ();
                    show_platform (new_row);
                } else {
                    get_display ().beep ();
                }

                return;
            }

            uint selected = selection.selected;

            if (selected == Gtk.INVALID_LIST_POSITION) {
                grid_view.scroll_to (0, SELECT, null);
                return;
            }

            int columns = get_columns ();
            int delta = 0;

            switch (dir) {
                case UP:
                    if (selected >= columns)
                        delta = -columns;
                    break;
                case DOWN:
                    if (selected + columns < selection.n_items)
                        delta = columns;
                    break;
                case LEFT:
                    if (selected > 0)
                        delta = -1;
                    break;
                case RIGHT:
                    if (selected < selection.n_items - 1)
                        delta = 1;
                    break;
                default:
                    assert_not_reached ();
            }

            if (delta == 0)
                get_display ().beep ();
            else
                grid_view.scroll_to (selected + delta, SELECT, null);
        });

        controller.select.connect (() => {
            if (focusing_sidebar) {
                focus_grid ();
                return;
            }

            uint selected = selection.selected;
            if (selected == Gtk.INVALID_LIST_POSITION)
                return;

            grid_view.activate (selected);
        });

        controller.cancel.connect (() => {
            if (focusing_sidebar)
                return;

            set_focusing_sidebar (true);

            sidebar.get_selected_row ().grab_focus ();

            if (split_view.collapsed)
                split_view.show_sidebar = true;
        });

        notify["compact"].connect (() => {
            if (compact)
                grid_view.add_css_class ("compact");
            else
                grid_view.remove_css_class ("compact");
        });

        search_bar.connect_entry (search_entry);

        var expr = new Gtk.PropertyExpression (typeof (Game), null, "title");
        search_filter = new Gtk.StringFilter (expr);
        view_filter = new Gtk.CustomFilter (item => {
            var game = item as Game;

            bool trashing = game.deferred_action == MOVE_TO_TRASH;

            switch (current_view_type) {
                case ALL:
                    return !trashing;
                case RECENT:
                    return !trashing && game.last_played != null &&
                           game.deferred_action != REMOVE_FROM_RECENT;
                case PLATFORM:
                    return !trashing && game.platform == current_platform;
                default:
                    assert_not_reached ();
            }
        });

        game_sorter = new Gtk.CustomSorter ((a, b) => {
            var game_a = a as Game;
            var game_b = b as Game;

            if (current_view_type == RECENT) {
                int ret = Game.compare_recent (game_a, game_b);
                if (ret != 0)
                    return ret;
            }

            return Game.compare (game_a, game_b);
        });

        all_games_row = new LibrarySidebarRow (ALL, null);
        sidebar.append (all_games_row);

        recent_row = new LibrarySidebarRow (RECENT, null);
        sidebar.append (recent_row);

        var platforms = PlatformRegister.get_register ().get_all_platforms ();
        foreach (var platform in platforms)
            sidebar.append (new LibrarySidebarRow (PLATFORM, platform));

        sidebar.set_header_func ((row, before) => {
            if (before == recent_row)
                row.set_header (new Gtk.Separator (HORIZONTAL));
            else
                row.set_header (null);
        });

        sidebar.select_row (all_games_row);
        sidebar_row_activated_cb (all_games_row);

        var library = Library.get_instance ();
        library.game_added.connect (() => {
            view_filter.changed (LESS_STRICT);
        });
        library.game_renamed.connect (() => {
            game_sorter.changed (DIFFERENT);
        });
        library.game_removed.connect (() => {
            view_filter.changed (MORE_STRICT);
        });
        library.recent_added.connect (() => {
            if (current_view_type == RECENT)
                view_filter.changed (LESS_STRICT);
        });
        library.recent_changed.connect (() => {
            if (current_view_type == RECENT)
                game_sorter.changed (DIFFERENT);
        });
        library.recent_removed.connect (() => {
            if (current_view_type == RECENT)
                view_filter.changed (MORE_STRICT);
        });
        library.deferred_error.connect ((message, error) => {
            critical (message);
            add_toast (new Adw.Toast (message));
        });

        set_focusing_sidebar (false);

        notify["show-empty-platforms"].connect (update_visibility);
    }

    static construct {
        install_action ("library.open-folder", null, widget => {
            var self = widget as LibraryPage;
            self.open_library_folder ();
        });

        install_action ("library.rescan", null, widget => {
            var self = widget as LibraryPage;
            self.rescan.begin ();
        });

        install_action ("library.cancel-rescan", null, widget => {
            var self = widget as LibraryPage;
            self.cancel_rescan ();
        });

        install_action ("library.open-firmware-preferences", null, widget => {
            var self = widget as LibraryPage;

            var dialog = new PreferencesDialog ();
            dialog.show_page ("firmware");
            dialog.present (self);
        });

        install_property_action ("library.search", "search-mode");

        add_binding_action (
            Gdk.Key.F, Gdk.ModifierType.CONTROL_MASK, "library.search", null
        );
    }

    protected override void map () {
        base.map ();

        if (can_type_to_search)
            search_bar.key_capture_widget = get_root () as Gtk.Widget;
    }

    protected override void shown () {
        if (get_mapped ())
            search_bar.key_capture_widget = get_root () as Gtk.Widget;

        can_type_to_search = true;
    }

    protected override void hiding () {
        can_type_to_search = false;

        search_bar.key_capture_widget = null;
    }

    [GtkCallback]
    private void grid_activate_cb (uint position) {
        var model = grid_view.model;

        var game = model.get_item (position) as Game;

        game_activated (game);
    }

    private void open_library_folder () {
        var settings = new Settings ("app.drey.Highscore");

        var library_path = settings.get_string ("library-dir");
        var library_file = File.new_for_path (library_path);

        var root = get_root ();

        if (root == null)
            return;

        var window = root as Gtk.Window;

        var launcher = new Gtk.FileLauncher (library_file);

        launcher.launch.begin (window, null);
    }

    private void update_scan_progress (uint n_games, double scan_progress) {
        if (rescan_cancellable == null || rescan_cancellable.is_cancelled ())
            return;

        progress_label.label = ngettext (
            "Found %u game",
            "Found %u games",
            n_games
        ).printf (n_games);

        progress.fraction = scan_progress;
    }

    private async void rescan () {
        rescan_cancellable = new Cancellable ();

        var library = Library.get_instance ();

        progress_label.visible = true;
        update_scan_progress (0, 0);

        yield WidgetUtils.change_stack_child_by_name (loading_stack, "scanning");

        if (rescan_cancellable.is_cancelled ()) {
            loading_stack.visible_child_name = "content";
            rescan_cancellable = null;
            return;
        }

        if (model_items_changed_id > 0) {
            model.disconnect (model_items_changed_id);
            model_items_changed_id = 0;
        }
        if (model_notify_empty_id > 0) {
            model.disconnect (model_notify_empty_id);
            model_notify_empty_id = 0;
        }

        grid_view.model = null;
        model = null;

        yield library.rescan (update_scan_progress, rescan_cancellable);

        show_games ();

        rescan_cancellable = null;
    }

    private void cancel_rescan () {
        if (rescan_cancellable == null)
            return;

        rescan_cancellable.cancel ();

        progress_label.label = _("Cancelling…");
    }

    public void show_games () {
        if (grid_view.model != null)
            return;

        model = Library.get_instance ().model;
        var sort_model = new Gtk.SortListModel (model, game_sorter);
        var multi_filter = new Gtk.EveryFilter ();
        multi_filter.append (view_filter);
        multi_filter.append (search_filter);
        var filter_model = new Gtk.FilterListModel (sort_model, multi_filter);
        selection = new Gtk.SingleSelection (filter_model) {
            autoselect = false,
            can_unselect = true,
        };

        selection.selected = Gtk.INVALID_LIST_POSITION;

        update_visibility ();

        model_items_changed_id = model.items_changed.connect (() => {
            update_visibility ();
            update_empty_stack (filter_model);
        });

        search_filter.changed.connect (() => {
            update_visibility ();
            update_empty_stack (filter_model);
        });

        grid_view.model = selection;

        loading_stack.visible_child_name = "content";

        filter_model.notify["n-items"].connect (() => {
            update_empty_stack (filter_model);
        });

        update_empty_stack (filter_model);

        if (gamepad_hint_bar.controller.main_gamepad != null)
            focus_grid ();
    }

    private string get_platform_hint (Platform platform) {
        var extensions = platform.get_extensions ();

        string[] bold_extensions = {};

        foreach (var ext in extensions)
            bold_extensions += @"<b>$ext</b>";

        return _("Game file extensions: %s").printf (
            string.joinv (", ", bold_extensions)
        );
    }

    private void show_platform (Gtk.ListBoxRow? row) {
        var sidebar_row = row as LibrarySidebarRow;

        var last_view_type = current_view_type;

        if (sidebar_row != null)
            current_view_type = sidebar_row.view_type;
        else
            current_view_type = ALL;

        switch (current_view_type) {
            case ALL:
                current_platform = null;
                content_title.title = _("All Games");
                empty_page.icon_name = "all-games-symbolic";
                empty_page.title = _("No Games Found");
                empty_page.description = _("Add some games to your library");
                empty_page_child.visible = true;
                rom_hint.label = "";
                rom_hint.visible = false;
                break;
            case RECENT:
                current_platform = null;
                content_title.title = _("Recently Played");
                empty_page.icon_name = "recent-games-symbolic";
                empty_page.title = _("No Recent Games");
                empty_page.description = _("Games will appear here if you play them");
                empty_page_child.visible = false;
                rom_hint.label = "";
                rom_hint.visible = false;
                break;
            case PLATFORM:
                current_platform = sidebar_row.platform;
                content_title.title = sidebar_row.platform.name;
                empty_page.icon_name = current_platform.icon_name;
                empty_page.title = _("No Games Found");
                empty_page.description = _("Add some games to your library");
                empty_page_child.visible = true;
                rom_hint.label = get_platform_hint (current_platform);
                rom_hint.visible = true;
                break;
            default:
                assert_not_reached ();
        }

        view_filter.changed (DIFFERENT);

        if ((current_view_type == RECENT) != (last_view_type == RECENT))
            game_sorter.changed (DIFFERENT);

        if (selection != null && selection.n_items > 0)
            grid_view.scroll_to (0, SELECT, null);
    }

    [GtkCallback]
    private void sidebar_row_activated_cb (Gtk.ListBoxRow? row) {
        show_platform (row);

        if (split_view.collapsed)
            split_view.show_sidebar = false;
    }

    [GtkCallback]
    private void search_changed_cb () {
        search_filter.search = search_entry.text;
    }

    private void update_visibility () {
        if (model == null)
            return;

        var visible_platforms = new GenericSet<Platform> (Platform.hash, Platform.equal);
        var enabled_platforms = new GenericSet<Platform> (Platform.hash, Platform.equal);
        bool has_recent = false;

        bool search_enabled = search_filter.search != null && search_filter.search != "";

        if (search_enabled || !show_empty_platforms) {
            var n_games = model.get_n_items ();
            for (uint i = 0; i < n_games; i++) {
                var game = model.get_item (i) as Game;

                if (!show_empty_platforms && !(game.platform in visible_platforms))
                    visible_platforms.add (game.platform);

                if (search_enabled) {
                    if (game.platform in enabled_platforms && has_recent)
                        continue;

                    if (!search_filter.match (game))
                        continue;

                    enabled_platforms.add (game.platform);

                    if (game.last_played != null)
                        has_recent = true;
                }
            }
        }

        for (int i = 0; true; i++) {
            var row = sidebar.get_row_at_index (i);

            if (row == null)
                break;

            if (row == all_games_row || row == recent_row)
                continue;

            var sidebar_row = row as LibrarySidebarRow;

            row.visible = show_empty_platforms || sidebar_row.platform in visible_platforms;
            row.sensitive = !search_enabled || sidebar_row.platform in enabled_platforms;
        }

        all_games_row.sensitive = !search_enabled || enabled_platforms.length > 0;
        recent_row.sensitive = !search_enabled || has_recent;

        var selected = sidebar.get_selected_row ();

        if (selected == null || !selected.sensitive || !selected.visible) {
            if (all_games_row.sensitive) {
                sidebar.select_row (all_games_row);
                sidebar_row_activated_cb (all_games_row);
            } else {
                sidebar.unselect_all ();
                sidebar_row_activated_cb (null);
            }
        }
    }

    private void update_empty_stack (ListModel model) {
        bool search_enabled = search_filter.search != null && search_filter.search != "";

        if (model.get_n_items () > 0)
            content_stack.visible_child_name = "content";
        else if (search_enabled)
            content_stack.visible_child_name = "empty-search";
        else
            content_stack.visible_child_name = "empty";

        update_gamepad_hints ();
    }

    private int get_columns () {
        var first_child = grid_view.get_first_child ();
        if (first_child == null)
            return 0;

        int total_width = grid_view.get_width ();
        int item_width = first_child.get_width ();

        return (total_width / item_width).clamp (
            (int) grid_view.min_columns, (int) grid_view.max_columns
        );
    }

    private void update_gamepad_hints () {
        if (focusing_sidebar) {
            gamepad_hint_bar.controller.select_label = _("Select");
            gamepad_hint_bar.controller.cancel_label = "";
        } else if (content_stack.visible_child_name != "content") {
            gamepad_hint_bar.controller.select_label = "";
            gamepad_hint_bar.controller.cancel_label = _("Back");
        } else {
            gamepad_hint_bar.controller.select_label = _("Run");
            gamepad_hint_bar.controller.cancel_label = _("Back");
        }
    }

    private void set_focusing_sidebar (bool sidebar) {
        focusing_sidebar = sidebar;

        if (focusing_sidebar)
            add_css_class ("focusing-sidebar");
        else
            remove_css_class ("focusing-sidebar");

        split_view.pin_sidebar = focusing_sidebar;

        update_gamepad_hints ();
    }

    private void focus_grid () {
        set_focusing_sidebar (false);

        grid_view.grab_focus ();

        uint selected = selection.selected;

        if (selected == Gtk.INVALID_LIST_POSITION && selection.n_items > 0)
            grid_view.scroll_to (0, SELECT, null);

        if (split_view.collapsed)
            split_view.show_sidebar = false;
    }

    public void run_search (string terms) {
        search_entry.text = terms;
        search_entry.set_position (-1);
        search_bar.search_mode_enabled = true;
    }

    private void undo_cb () {
        Library.get_instance ().undo ();

        undo_toast = null;
    }

    private void undo_dismissed_cb () {
        Library.get_instance ().commit.begin ();

        undo_toast = null;
    }

    private async void start_deferred_action (
        Game game,
        uint position,
        Library.DeferredAction action
    ) {
        var library = Library.get_instance ();
        uint n_items = selection.get_n_items ();

        if (n_items > 1 && position == n_items - 1)
            position--;

        switch (action) {
            case REMOVE_FROM_RECENT:
                yield library.remove_from_recent (game);
                break;
            case MOVE_TO_TRASH:
                yield library.move_to_trash (game);
                break;
            default:
                assert_not_reached ();
        }

        if (n_items > 1)
            grid_view.scroll_to (position, FOCUS, null);

        if (undo_toast == null) {
            var title = Markup.escape_text (game.title);
            string str;

            switch (action) {
                case REMOVE_FROM_RECENT:
                    str = _("‘%s’ removed from recent").printf (title);
                    break;
                case MOVE_TO_TRASH:
                    str = _("‘%s’ moved to trash").printf (title);
                    break;
                default:
                    assert_not_reached ();
            }

            undo_toast = new Adw.Toast (str) {
                priority = HIGH,
                button_label = _("_Undo"),
            };

            undo_toast.button_clicked.connect (undo_cb);
            undo_toast.dismissed.connect (undo_dismissed_cb);

            add_toast (undo_toast);
        } else {
            switch (action) {
                case REMOVE_FROM_RECENT:
                    undo_toast.title = ngettext (
                        "<span font_features='tnum=1'>%d</span> game removed from recent",
                        "<span font_features='tnum=1'>%d</span> games removed from recent",
                        library.n_deferred_games
                    ).printf (library.n_deferred_games);
                    break;
                case MOVE_TO_TRASH:
                    undo_toast.title = ngettext (
                        "<span font_features='tnum=1'>%d</span> game moved to trash",
                        "<span font_features='tnum=1'>%d</span> games moved to trash",
                        library.n_deferred_games
                    ).printf (library.n_deferred_games);
                    break;
                default:
                    assert_not_reached ();
            }

            add_toast (undo_toast);
        }
    }

    public async void remove_from_recent (Game game, uint position) {
        yield start_deferred_action (game, position, REMOVE_FROM_RECENT);
    }

    public async void move_to_trash (Game game, uint position) {
        var app = GLib.Application.get_default () as Application;
        var window = app.get_window_for_game (game);
        if (window != null)
            return;

        bool can_trash;

        try {
            can_trash = yield Library.get_instance ().can_trash (game);
        } catch (Error e) {
            add_toast (new Adw.Toast.format (
                _("Failed to move to trash: %s"), e.message
            ));
            return;
        }

        if (!can_trash) {
            yield delete_game (game, position);
            return;
        }

        yield start_deferred_action (game, position, MOVE_TO_TRASH);
    }

    private async void delete_game (Game game, uint position) {
        var dialog = new Adw.AlertDialog (
            _("Delete ‘%s’?").printf (game.title),
            _("This game cannot be moved to trash. Permanently delete it instead?")
        );

        dialog.add_response ("cancel", _("_Cancel"));
        dialog.add_response ("delete", _("_Delete"));

        dialog.set_response_appearance ("delete", DESTRUCTIVE);
        dialog.default_response = "delete";

        var response = yield dialog.choose (this, null);

        if (response == "delete") {
            var library = Library.get_instance ();
            uint n_items = selection.get_n_items ();

            if (n_items > 1 && position == n_items - 1)
                position--;

            try {
                yield library.delete_game (game);
            } catch (Error e) {
                add_toast (new Adw.Toast.format (
                    "Failed to delete '%s': %s",
                    Markup.escape_text (game.title),
                    e.message
                ));

                return;
            }

            if (n_items > 1)
                grid_view.scroll_to (position, FOCUS, null);
        }
    }
}
