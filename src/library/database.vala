// This file is part of Highscore. License: GPL-3.0-or-later

private class Highscore.Database : Object {
    public const int VERSION = 2;

    public delegate void GameLoadedCallback (Game game);

    private Sqlite.Database database;

    private const string CREATE_GAMES_TABLE_QUERY = """
        CREATE TABLE IF NOT EXISTS games (
            id INTEGER PRIMARY KEY NOT NULL,
            uid TEXT NOT NULL UNIQUE,
            title TEXT NOT NULL,
            platform TEXT NOT NULL,
            metadata TEXT NULL,
            cover_path TEXT NULL
        );
    """;

    private const string CREATE_MEDIA_TABLE_QUERY = """
        CREATE TABLE IF NOT EXISTS media (
            id INTEGER PRIMARY KEY NOT NULL,
            game_uid TEXT NOT NULL,
            position INTEGER NOT NULL,
            uid TEXT NOT NULL UNIQUE,
            path TEXT NOT NULL,
            extra_paths TEXT NULL,
            CONSTRAINT fk_game_uid
                FOREIGN KEY (game_uid)
                REFERENCES games (uid)
                ON DELETE CASCADE,
            UNIQUE (game_uid, position)
        );
    """;

    private const string CREATE_TITLES_TABLE_QUERY = """
        CREATE TABLE IF NOT EXISTS titles (
            id INTEGER PRIMARY KEY NOT NULL,
            uid TEXT NOT NULL UNIQUE,
            title TEXT NOT NULL
        );
    """;

    private const string CREATE_RECENT_TABLE_QUERY = """
        CREATE TABLE IF NOT EXISTS recent (
            id INTEGER PRIMARY KEY NOT NULL,
            uid TEXT NOT NULL UNIQUE,
            last_played TEXT NOT NULL
        );
    """;

    private const string CREATE_GAMES_UID_INDEX_QUERY = """
        CREATE UNIQUE INDEX IF NOT EXISTS idx_games_uid ON games (uid);
    """;

    private const string CREATE_MEDIA_UID_INDEX_QUERY = """
        CREATE UNIQUE INDEX IF NOT EXISTS idx_media_uid ON media (uid);
    """;

    private const string CREATE_MEDIA_GAME_UID_INDEX_QUERY = """
        CREATE INDEX IF NOT EXISTS idx_media_game_uid ON media (game_uid);
    """;

    private const string CREATE_TITLES_UID_INDEX_QUERY = """
        CREATE UNIQUE INDEX IF NOT EXISTS idx_titles_uid ON titles (uid);
    """;

    private const string CREATE_RECENT_UID_INDEX_QUERY = """
        CREATE UNIQUE INDEX IF NOT EXISTS idx_recent_uid ON recent (uid);
    """;

    private const string ADD_GAME_QUERY = """
        INSERT INTO games (
            uid, title, platform, cover_path, metadata
        ) VALUES (
            $UID, $TITLE, $PLATFORM, $COVER_PATH, $METADATA
        );
    """;

    private const string UPDATE_GAME_QUERY = """
        UPDATE games SET
            title = $TITLE,
            platform = $PLATFORM,
            cover_path = $COVER_PATH,
            metadata = $METADATA
        WHERE uid = $UID;
    """;

    private const string REMOVE_GAME_QUERY = """
        DELETE FROM games WHERE uid = $UID;
    """;

    private const string ADD_MEDIA_QUERY = """
        INSERT INTO media (
            game_uid, position, uid, path, extra_paths
        ) VALUES (
            $GAME_UID, $POSITION, $UID, $PATH, $EXTRA_PATHS
        );
    """;

    private const string UPDATE_MEDIA_QUERY = """
        UPDATE media SET
            path = $PATH,
            extra_paths = $EXTRA_PATHS
        WHERE uid = $UID;
    """;

    private const string LIST_GAMES_QUERY = """
        SELECT
            games.uid,
            titles.title,
            games.title,
            platform,
            cover_path,
            metadata,
            recent.last_played,
            media.uid,
            media.path,
            media.extra_paths
        FROM games
            LEFT JOIN titles ON games.uid == titles.uid
            LEFT JOIN recent ON games.uid == recent.uid
            LEFT JOIN media ON games.uid == media.game_uid
        ORDER BY games.uid, media.position;
    """;

    private const string UPDATE_TITLE_QUERY = """
        INSERT INTO titles (uid, title)
        VALUES ($UID, $TITLE)
        ON CONFLICT(uid) DO UPDATE SET title = $TITLE;
    """;

    private const string REMOVE_TITLE_QUERY = """
        DELETE FROM titles WHERE uid = $UID;
    """;

    private const string FIND_TITLE_QUERY = """
        SELECT title FROM titles WHERE uid = $UID;
    """;

    private const string FIND_GAME_BY_MEDIA_QUERY = """
        SELECT game_uid FROM media WHERE uid = $UID;
    """;

    private const string UPDATE_RECENT_QUERY = """
        INSERT INTO recent (uid, last_played)
        VALUES ($UID, $LAST_PLAYED)
        ON CONFLICT(uid) DO UPDATE SET last_played = $LAST_PLAYED;
    """;

    private const string REMOVE_RECENT_QUERY = """
        DELETE FROM recent WHERE uid = $UID;
    """;

    private Sqlite.Statement add_game_query;
    private Sqlite.Statement update_game_query;
    private Sqlite.Statement remove_game_query;
    private Sqlite.Statement list_games_query;

    private Sqlite.Statement add_media_query;
    private Sqlite.Statement update_media_query;

    private Sqlite.Statement update_title_query;
    private Sqlite.Statement remove_title_query;
    private Sqlite.Statement find_title_query;

    private Sqlite.Statement update_recent_query;
    private Sqlite.Statement remove_recent_query;

    private Sqlite.Statement find_game_by_media_query;

    public Database (string path) throws DatabaseError {
        var db_file = File.new_for_path (path);

        try {
            db_file.get_parent ().make_directory_with_parents ();
        } catch (IOError.EXISTS e) {
        } catch (Error e) {
            throw new DatabaseError.COULDNT_OPEN ("Failed to create the directory: %s", e.message);
        }

        bool newly_created = !db_file.query_exists ();

        if (Sqlite.Database.open (path, out database) != Sqlite.OK)
            throw new DatabaseError.COULDNT_OPEN ("Couldn’t open the database for “%s”.", path);

        int version = 0;

        DatabaseUtils.exec (database, "PRAGMA foreign_keys = ON;", null);

        if (newly_created) {
            DatabaseUtils.exec (database, @"PRAGMA user_version = $VERSION;", null);
        } else {
            DatabaseUtils.exec (database, "PRAGMA user_version", (n_columns, values, column_names) => {
                assert (n_columns == 1);

                version = int.parse (values[0]);

                return 0;
            });

            if (version < VERSION) {
                message ("Updating database from version %d to %d", version, VERSION);

                if (version <= 0) {
                    DatabaseUtils.exec (database,
                        "ALTER TABLE games ADD cover_path TEXT NULL;",
                    null);
                }

                if (version <= 1) {
                    DatabaseUtils.exec (database, CREATE_MEDIA_TABLE_QUERY, null);

                    DatabaseUtils.exec (database, """
                        INSERT INTO media (game_uid, position, uid, path, extra_paths)
                        SELECT uid, 0, uid, path, extra_paths FROM games;
                    """, null);

                    DatabaseUtils.exec (database,
                        "ALTER TABLE games DROP COLUMN path;",
                    null);

                    DatabaseUtils.exec (database,
                        "ALTER TABLE games DROP COLUMN extra_paths;",
                    null);
                }

                DatabaseUtils.exec (database, @"PRAGMA user_version = $VERSION;", null);
            }
        }

        DatabaseUtils.exec (database, CREATE_GAMES_TABLE_QUERY, null);
        DatabaseUtils.exec (database, CREATE_GAMES_UID_INDEX_QUERY, null);

        DatabaseUtils.exec (database, CREATE_MEDIA_TABLE_QUERY, null);
        DatabaseUtils.exec (database, CREATE_MEDIA_UID_INDEX_QUERY, null);
        DatabaseUtils.exec (database, CREATE_MEDIA_GAME_UID_INDEX_QUERY, null);

        DatabaseUtils.exec (database, CREATE_TITLES_TABLE_QUERY, null);
        DatabaseUtils.exec (database, CREATE_TITLES_UID_INDEX_QUERY, null);

        DatabaseUtils.exec (database, CREATE_RECENT_TABLE_QUERY, null);
        DatabaseUtils.exec (database, CREATE_RECENT_UID_INDEX_QUERY, null);

        add_game_query = DatabaseUtils.prepare (database, ADD_GAME_QUERY);
        update_game_query = DatabaseUtils.prepare (database, UPDATE_GAME_QUERY);
        remove_game_query = DatabaseUtils.prepare (database, REMOVE_GAME_QUERY);
        list_games_query = DatabaseUtils.prepare (database, LIST_GAMES_QUERY);

        add_media_query = DatabaseUtils.prepare (database, ADD_MEDIA_QUERY);
        update_media_query = DatabaseUtils.prepare (database, UPDATE_MEDIA_QUERY);

        update_title_query = DatabaseUtils.prepare (database, UPDATE_TITLE_QUERY);
        remove_title_query = DatabaseUtils.prepare (database, REMOVE_TITLE_QUERY);
        find_title_query = DatabaseUtils.prepare (database, FIND_TITLE_QUERY);

        update_recent_query = DatabaseUtils.prepare (database, UPDATE_RECENT_QUERY);
        remove_recent_query = DatabaseUtils.prepare (database, REMOVE_RECENT_QUERY);

        find_game_by_media_query = DatabaseUtils.prepare (database, FIND_GAME_BY_MEDIA_QUERY);
    }

    private string? serialize_extra_paths (Media media) {
        var paths = media.extra_paths;

        if (paths == null)
            return null;

        return new Variant.bytestring_array (paths).print (false);
    }

    private string? serialize_metadata (Game game) {
        if (game.metadata == null)
            return null;

        var variant = game.metadata.serialize ();

        if (variant == null)
            return null;

        return variant.print (false);
    }

    private string[]? deserialize_extra_paths (string? str) throws DatabaseError {
        if (str == null)
            return null;

        Variant variant;

        try {
            variant = Variant.parse (VariantType.BYTESTRING_ARRAY, str);
         } catch (VariantParseError e) {
            throw new DatabaseError.INVALID_GAME (
                "Failed to parse extra paths '%s': %s", str, e.message
            );
        }

        if (variant == null)
            return null;

        return variant.get_bytestring_array ();
    }

    private GameMetadata? deserialize_metadata (Platform platform, string? str) throws DatabaseError {
        var metadata = GameMetadata.create (platform);
        if (metadata != null) {
            Variant variant;

            try {
                var type = metadata.serialize_type ();
                variant = Variant.parse (type, str);
            } catch (VariantParseError e) {
                throw new DatabaseError.INVALID_GAME (
                    "Failed to parse metadata '%s': %s", str, e.message
                );
            }

            metadata.deserialize (variant);
        }

        return metadata;
    }

    public bool add_game (Game game) throws DatabaseError {
        var game_uid = game.uid;
        var default_title = game.default_title;
        var platform = game.platform.id;
        var cover_path = game.cover != null ? game.cover.get_path () : null;
        var metadata = serialize_metadata (game);

        DatabaseUtils.exec (database, "BEGIN TRANSACTION;", null);

        add_game_query.reset ();
        DatabaseUtils.bind_text (add_game_query, "$UID", game_uid);
        DatabaseUtils.bind_text (add_game_query, "$TITLE", default_title);
        DatabaseUtils.bind_text (add_game_query, "$PLATFORM", platform);
        DatabaseUtils.bind_text (add_game_query, "$COVER_PATH", cover_path);
        DatabaseUtils.bind_text (add_game_query, "$METADATA", metadata);

        var result = add_game_query.step ();

        if (result == Sqlite.CONSTRAINT) {
            DatabaseUtils.exec (database, "ROLLBACK;", null);
            return false;
        }

        if (result != Sqlite.DONE) {
            DatabaseUtils.exec (database, "ROLLBACK;", null);

            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't add game (%s, %s, %s, %s)",
                game_uid, default_title, platform, metadata
            );
        }

        int position = 0;

        foreach (var media in game.get_media ()) {
            var media_uid = media.uid;
            var path = media.file.get_path ();
            var extra_paths = serialize_extra_paths (media);

            add_media_query.reset ();
            DatabaseUtils.bind_text (add_media_query, "$GAME_UID", game_uid);
            DatabaseUtils.bind_int  (add_media_query, "$POSITION", position++);
            DatabaseUtils.bind_text (add_media_query, "$UID", media_uid);
            DatabaseUtils.bind_text (add_media_query, "$PATH", path);
            DatabaseUtils.bind_text (add_media_query, "$EXTRA_PATHS", extra_paths);

            result = add_media_query.step ();

            if (result == Sqlite.CONSTRAINT) {
                DatabaseUtils.exec (database, "ROLLBACK;", null);
                return false;
            }

            if (result != Sqlite.DONE) {
                DatabaseUtils.exec (database, "ROLLBACK;", null);

                throw new DatabaseError.EXECUTION_FAILED (
                    "Couldn't add media (%s, %s, %s, %s)",
                    game_uid, media_uid, path, extra_paths
                );
            }
        }

        DatabaseUtils.exec (database, "COMMIT;", null);

        return true;
    }

    public void update_game (Game game) throws DatabaseError {
        var uid = game.uid;
        var default_title = game.default_title;
        var platform = game.platform.id;
        var cover_path = game.cover != null ? game.cover.get_path () : null;
        var metadata = serialize_metadata (game);

        DatabaseUtils.exec (database, "BEGIN TRANSACTION;", null);

        update_game_query.reset ();
        DatabaseUtils.bind_text (update_game_query, "$UID", uid);
        DatabaseUtils.bind_text (update_game_query, "$TITLE", default_title);
        DatabaseUtils.bind_text (update_game_query, "$PLATFORM", platform);
        DatabaseUtils.bind_text (update_game_query, "$COVER_PATH", cover_path);
        DatabaseUtils.bind_text (update_game_query, "$METADATA", metadata);

        if (update_game_query.step () != Sqlite.DONE) {
            DatabaseUtils.exec (database, "ROLLBACK;", null);

            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't update game (%s, %s, %s, %s)",
                uid, default_title, platform, metadata
            );
        }

        foreach (var media in game.get_media ()) {
            var media_uid = media.uid;
            var path = media.file.get_path ();
            var extra_paths = serialize_extra_paths (media);

            update_media_query.reset ();
            DatabaseUtils.bind_text (update_media_query, "$UID", media_uid);
            DatabaseUtils.bind_text (update_media_query, "$PATH", path);
            DatabaseUtils.bind_text (update_media_query, "$EXTRA_PATHS", extra_paths);

            if (update_media_query.step () != Sqlite.DONE) {
                DatabaseUtils.exec (database, "ROLLBACK;", null);

                throw new DatabaseError.EXECUTION_FAILED (
                    "Couldn't update media (%s, %s, %s)",
                    media_uid, path, extra_paths
                );
            }
        }

        DatabaseUtils.exec (database, "COMMIT;", null);
    }

    public void remove_game (Game game, bool remove_metadata) throws DatabaseError {
        remove_game_by_uid (game.uid);

        if (remove_metadata)
            remove_from_recent (game);
    }

    public void load_games (
        GameLoadedCallback game_callback
    ) throws DatabaseError {
        list_games_query.reset ();

        string[] invalid_games = {};
        Media[] media = {};

        string uid = null, title = null, default_title = null, platform_id = null;
        string cover_path = null, metadata = null, last_played = null;

        while (list_games_query.step () == Sqlite.ROW) {
            var new_uid = list_games_query.column_text (0);

            if (uid != null && new_uid != uid && !(uid in invalid_games)) {
                try {
                    var game = create_game (
                        media, uid, title, default_title, platform_id, metadata, last_played
                    );

                    if (cover_path != null)
                        game.cover = File.new_for_path (cover_path);

                    game_callback (game);

                    media = {};
                } catch (DatabaseError.INVALID_GAME error) {
                    debug ("Couldn't load game %s: %s", title, error.message);
                    invalid_games += uid;
                    media = {};
                }
            }

            if (new_uid != uid) {
                uid = new_uid;
                title = list_games_query.column_text (1);
                default_title = list_games_query.column_text (2);
                platform_id = list_games_query.column_text (3);
                cover_path = list_games_query.column_text (4);
                metadata = list_games_query.column_text (5);
                last_played = list_games_query.column_text (6);

                if (title == null)
                    title = default_title;
            }

            var media_uid = list_games_query.column_text (7);
            var path = list_games_query.column_text (8);
            var extra_paths = list_games_query.column_text (9);

            try {
                media += create_media (media_uid, path, extra_paths);
            } catch (DatabaseError.INVALID_GAME error) {
                debug ("Couldn't load media %s: %s", media_uid, error.message);
                invalid_games += new_uid;
            }
        }

        if (uid != null) {
            try {
                var game = create_game (
                    media, uid, title, default_title, platform_id, metadata, last_played
                );

                if (cover_path != null)
                    game.cover = File.new_for_path (cover_path);

                game_callback (game);
            } catch (DatabaseError.INVALID_GAME error) {
                debug ("Couldn't load game %s: %s", title, error.message);
                invalid_games += uid;
            }
        }

        foreach (var invalid_uid in invalid_games)
            remove_game_by_uid (invalid_uid);
    }

    private void remove_game_by_uid (string uid) throws DatabaseError {
        remove_game_query.reset ();

        DatabaseUtils.bind_text (remove_game_query, "$UID", uid);

        var result = remove_game_query.step ();
        if (result == Sqlite.ROW)
            return;

        if (result != Sqlite.DONE)
            throw new DatabaseError.EXECUTION_FAILED ("Failed to delete game '%s'", uid);
    }

    private Media create_media (
        string uid,
        string path,
        string? extra_paths_str
    ) throws DatabaseError {
        var file = File.new_for_path (path);
        if (!file.query_exists ())
            throw new DatabaseError.INVALID_GAME ("File doesn't exist %s", path);

        var extra_paths = deserialize_extra_paths (extra_paths_str);
        foreach (var extra_path in extra_paths) {
            var extra_file = File.new_for_path (extra_path);
            if (!extra_file.query_exists ())
                throw new DatabaseError.INVALID_GAME ("Extra file doesn't exist %s", path);
        }

        return new Media (uid, file, extra_paths);
    }

    private Game create_game (
        Media[] media,
        string uid,
        string title,
        string default_title,
        string platform_id,
        string? metadata_str,
        string? last_played_str
    ) throws DatabaseError {
        var platform = PlatformRegister.get_register ().get_platform (platform_id);
        if (platform == null)
            throw new DatabaseError.INVALID_GAME ("Unknown platform: %s", platform_id);

        var metadata = deserialize_metadata (platform, metadata_str);

        DateTime? last_played;
        if (last_played_str != null)
            last_played = new DateTime.from_iso8601 (last_played_str, null);
        else
            last_played = null;

        return new Game (media, platform, uid, title, default_title, metadata, last_played);
    }

    public void rename_game (Game game, string title) throws DatabaseError {
        update_title_query.reset ();
        DatabaseUtils.bind_text (update_title_query, "$UID", game.uid);
        DatabaseUtils.bind_text (update_title_query, "$TITLE", title);

        var result = update_title_query.step ();

        if (result != Sqlite.DONE) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't rename game '%s' to '%s'",
                game.title, title
            );
        }
    }

    public void reset_game_title (Game game) throws DatabaseError {
        remove_title_query.reset ();
        DatabaseUtils.bind_text (remove_title_query, "$UID", game.uid);

        if (remove_title_query.step () != Sqlite.DONE) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't reset title for the game '%s'",
                game.title
            );
        }
    }

    public string? find_game_title (Game game) throws Error {
        find_title_query.reset ();
        DatabaseUtils.bind_text (find_title_query, "$UID", game.uid);

        if (find_title_query.step () == Sqlite.ROW)
            return find_title_query.column_text (0);

        return null;
    }

    public string? find_game_uid_from_media (Media media) throws Error {
        find_game_by_media_query.reset ();
        DatabaseUtils.bind_text (find_game_by_media_query, "$UID", media.uid);

        if (find_game_by_media_query.step () == Sqlite.ROW)
            return find_game_by_media_query.column_text (0);

        return null;
    }

    public void update_last_played (Game game) throws Error {
        var last_played = game.last_played.format_iso8601 ();

        update_recent_query.reset ();
        DatabaseUtils.bind_text (update_recent_query, "$UID", game.uid);
        DatabaseUtils.bind_text (update_recent_query, "$LAST_PLAYED", last_played);

        var result = update_recent_query.step ();

        if (result != Sqlite.DONE) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't update last played time for '%s' to '%s'",
                game.title, last_played
            );
        }
    }

    public void remove_from_recent (Game game) throws DatabaseError {
        remove_recent_query.reset ();
        DatabaseUtils.bind_text (remove_recent_query, "$UID", game.uid);

        if (remove_recent_query.step () != Sqlite.DONE) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't remove game '%s' from recent", game.title
            );
        }
    }
}
