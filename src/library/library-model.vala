// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.LibraryModel : Object, ListModel {
    private ListStore? games;

    public bool empty { get; private set; default = true; }

    public LibraryModel () {
        items_changed.connect ((pos, removed, added) => {
            empty = get_n_items () == 0;
        });
    }

    public Type get_item_type () {
        return typeof (Game);
    }

    public uint get_n_items () {
        if (games == null)
            return 0;

        return games.get_n_items ();
    }

    public Object? get_item (uint index) {
        if (games == null)
            return null;

        return games.get_item (index);
    }

    public void set_games (ListStore? games) {
        uint old_items = get_n_items ();
        uint new_items = games.get_n_items ();

        this.games = games;

        items_changed (0, old_items, new_items);
    }
}
