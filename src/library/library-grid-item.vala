// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/library/library-grid-item.ui")]
public class Highscore.LibraryGridItem : ContextMenuBin {
    public Game game { get; set; }
    public uint position { get; set; }

    static construct {
        install_action ("game.open-in-new-window", null, widget => {
            var self = widget as LibraryGridItem;
            self.open_in_new_window ();
        });

        install_action ("game.remove-from-recent", null, widget => {
            var self = widget as LibraryGridItem;
            self.remove_from_recent ();
        });

        install_action ("game.move-to-trash", null, widget => {
            var self = widget as LibraryGridItem;
            self.move_to_trash ();
        });

        install_action ("game.properties", null, widget => {
            var self = widget as LibraryGridItem;
            self.open_properties ();
        });

        set_css_name ("library-grid-item");
    }

    private void open_in_new_window () {
        var app = GLib.Application.get_default () as Application;

        app.run_game_standalone.begin (game);
    }

    private void remove_from_recent () {
        Idle.add_once (() => {
            var library_page = get_ancestor (typeof (LibraryPage)) as LibraryPage;
            library_page.remove_from_recent.begin (game, position);
        });
    }

    private void move_to_trash () {
        Idle.add_once (() => {
            var library_page = get_ancestor (typeof (LibraryPage)) as LibraryPage;
            library_page.move_to_trash.begin (game, position);
        });
    }

    private void open_properties () {
        new PropertiesDialog (game).present (this);
    }

    [GtkCallback]
    private void click_released_cb (Gtk.GestureClick gesture, int n_press, double x, double y) {
        var library_page = get_ancestor (typeof (LibraryPage)) as LibraryPage;
        library_page.game_activated (game);
    }

    [GtkCallback]
    private void setup_menu_cb () {
        var library_page = get_ancestor (typeof (LibraryPage)) as LibraryPage;
        bool is_recent = library_page.current_view_type == RECENT;

        action_set_enabled ("game.remove-from-recent", is_recent);

        var app = GLib.Application.get_default () as Application;
        var window = app.get_window_for_game (game);

        action_set_enabled ("game.move-to-trash", window == null);
    }
}
