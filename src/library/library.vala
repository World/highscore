// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Library : Object {
    public delegate void ScanProgressCallback (uint n_games, double percentage);

    public signal void game_added ();
    public signal void game_renamed (Game game);
    public signal void game_removed ();

    public signal void recent_added ();
    public signal void recent_removed ();
    public signal void recent_changed ();

    public signal void deferred_error (string message, Error e);

    private static Library instance;

    private Database database;

    private ListStore games;
    private HashTable<string, Game> games_table;

    public enum DeferredAction {
        NONE,
        REMOVE_FROM_RECENT,
        MOVE_TO_TRASH,
    }

    private Game[] deferred_games;

    public DeferredAction deferred_action {
        get; private set; default = NONE;
    }

    public int n_deferred_games {
        get { return deferred_games.length; }
    }

    public LibraryModel model { get; private set; }

    public static Library get_instance () {
        if (instance == null)
            instance = new Library ();

        return instance;
    }

    construct {
        var data_dir = Environment.get_user_data_dir ();
        var db_path = Path.build_filename (data_dir, "highscore", "library.sqlite3");

        try {
            database = new Database (db_path);
        } catch (DatabaseError e) {
            error ("Failed to create the database: %s", e.message);
        }

        model = new LibraryModel ();

        deferred_games = {};
    }

    public async void load_games () {
        var games = new ListStore (typeof (Game));
        var games_table = new HashTable<string, Game> (str_hash, str_equal);

        new Thread<void> ("library-load-thread", () => {
            try {
                database.load_games (game => {
                    games.append (game);
                    games_table[game.uid] = game;
                });
            } catch (DatabaseError e) {
                critical ("Failed to load games from the database: %s", e.message);
            }

            load_games.callback ();
        });

        yield;

        model.set_games (games);
        this.games = games;
        this.games_table = games_table;
    }

    public async void rescan (ScanProgressCallback? callback, Cancellable? cancellable = null) {
        var settings = new Settings ("app.drey.Highscore");
        var library_path = settings.get_string ("library-dir");
        var library_dir = File.new_for_path (library_path);

        var new_games = new ListStore (typeof (Game));
        var new_games_table = new HashTable<string, Game> (str_hash, str_equal);
        uint n_games = 0;
        uint n_files = 0;
        uint n_scanned = 0;

        new Thread<void> ("library-scan-thread", () => {
            var cache_dir = LibraryUtils.get_cover_cache_dir ();

            string[] sizes = {};

            if (cache_dir.query_exists ()) {
                try {
                    var enumerator = cache_dir.enumerate_children (
                        FileAttribute.STANDARD_NAME,
                        FileQueryInfoFlags.NONE,
                        cancellable
                    );

                    if (cancellable.is_cancelled ())
                        return;

                    for (var info = enumerator.next_file (); info != null; info = enumerator.next_file ())
                        sizes += info.get_name ();
                } catch (Error e) {
                    critical ("Failed to enumerate cache dirs: %s", e.message);
                }
            }

            var scanner = new GameScanner (library_dir, cancellable, game => {
                if (game.uid in new_games_table)
                    return;

                new_games.append (game);
                new_games_table[game.uid] = game;

                try {
                    if (has_game (game) || !database.add_game (game))
                        database.update_game (game);
                } catch (DatabaseError e) {
                    critical ("Failed to add game: %s", e.message);
                }

                try {
                    var old_game = find_game_by_uid (game.uid);

                    if (game.cover == null ||
                        old_game == null ||
                        old_game.cover == null ||
                        !game.cover.equal (old_game.cover)) {
                        invalidate_cache_for_sizes (game, sizes);
                    } else {
                        maybe_invalidate_cache (game, sizes);
                    }
                } catch (Error e) {
                    critical ("Failed to invalidate cover cache: %s", e.message);
                }
            }, (n_f, n_s, n_g) => {
                AtomicUint.set (ref n_games, n_g);
                AtomicUint.set (ref n_files, n_f);
                AtomicUint.set (ref n_scanned, n_s);

                if (callback != null) {
                    Idle.add_once (() => {
                        var n = AtomicUint.get (ref n_games);
                        var f = AtomicUint.get (ref n_files);
                        var s = AtomicUint.get (ref n_scanned);

                        callback (n, (double) s / (double) f);
                    });
                }
            });

            scanner.scan ();

            if (cancellable != null && cancellable.is_cancelled ()) {
                rescan.callback ();
                return;
            }

            new_games.sort ((CompareDataFunc<Object>) Game.compare_id);

            int i = 0, j = 0;

            if (games != null) {
                uint n = games.get_n_items ();
                while (i < n) {
                    var old_game = games.get_item (i) as Game;

                    if (j >= new_games.get_n_items ()) {
                        i++;
                        try {
                            database.remove_game (old_game, false);
                        } catch (DatabaseError e) {
                            critical ("Failed to remove game: %s", e.message);
                        }
                        continue;
                    }

                    var new_game = new_games.get_item (j) as Game;

                    if (old_game.uid == new_game.uid) {
                        i++;
                        j++;

                        if (old_game.title != old_game.default_title)
                            new_game.title = old_game.title;

                        continue;
                    }

                    if (strcmp (new_game.uid, old_game.uid) > 0) {
                        i++;
                        try {
                            database.remove_game (old_game, false);
                        } catch (DatabaseError e) {
                            critical ("Failed to remove game: %s", e.message);
                        }

                        try {
                            invalidate_cache_for_sizes (old_game, sizes);
                        } catch (Error e) {
                            critical ("Failed to remove cover: %s", e.message);
                        }
                        continue;
                    }

                    j++;
                }
            }

            rescan.callback ();
        });

        yield;

        if (cancellable != null && cancellable.is_cancelled ())
            return;

        callback (n_games, (double) n_scanned / (double) n_files);

        model.set_games (new_games);
        games = new_games;
        games_table = new_games_table;
    }

    private void maybe_invalidate_cache (Game game, string[] sizes) throws Error {
        if (game.cover == null)
            return;

        var cover_info = game.cover.query_info (FileAttribute.TIME_CHANGED, NONE);
        uint64 cover_time = cover_info.get_attribute_uint64 (FileAttribute.TIME_CHANGED);

        var cache_dir = LibraryUtils.get_cover_cache_dir ();

        foreach (var size in sizes) {
            var size_dir = cache_dir.get_child (size);
            var image = size_dir.get_child (@"$(game.uid).png");

            if (!image.query_exists ())
                continue;

            var image_info = image.query_info (FileAttribute.TIME_CREATED, NONE);
            uint64 image_time = image_info.get_attribute_uint64 (FileAttribute.TIME_CREATED);

            if (cover_time > image_time)
                image.delete ();
        }
    }

    private void invalidate_cache_for_sizes (Game game, string[] sizes) throws Error {
        var cache_dir = LibraryUtils.get_cover_cache_dir ();

        foreach (var size in sizes) {
            var size_dir = cache_dir.get_child (size);
            var image = size_dir.get_child (@"$(game.uid).png");

            if (!image.query_exists ())
                continue;

            image.delete ();
        }
    }

    public async void invalidate_cover_cache (Game game) throws Error {
        var cache_dir = LibraryUtils.get_cover_cache_dir ();

        var enumerator = yield cache_dir.enumerate_children_async (
            FileAttribute.STANDARD_NAME, FileQueryInfoFlags.NONE
        );

        for (var info = enumerator.next_file (); info != null; info = enumerator.next_file ()) {
            var name = info.get_name ();
            var size_dir = cache_dir.get_child (name);
            var image = size_dir.get_child (@"$(game.uid).png");

            if (!image.query_exists ())
                continue;

            yield image.delete_async ();
        }

        game.cover_changed ();
    }

    public async void override_cover (Game game, File file) throws Error {
        var info = file.query_info (
            FileAttribute.STANDARD_CONTENT_TYPE,
            NONE
        );

        var content_type = info.get_content_type ();
        var mime_type = ContentType.get_mime_type (content_type);

        if (mime_type != "image/png" &&
            mime_type != "image/jpeg" &&
            mime_type != "image/tiff") {
            throw new LibraryError.INVALID_COVER_FORMAT ("Invalid cover format");
        }

        var override_file = LibraryUtils.get_cover_override (game);

        var parent = override_file.get_parent ();
        if (!parent.query_exists ())
            parent.make_directory_with_parents ();

        yield file.copy_async (override_file, OVERWRITE);
        yield invalidate_cover_cache (game);
    }

    public async void remove_cover_override (Game game) throws Error {
        var override_file = LibraryUtils.get_cover_override (game);

        yield override_file.delete_async ();
        yield invalidate_cover_cache (game);
    }

    public bool has_cover_override (Game game) {
        var override_file = LibraryUtils.get_cover_override (game);

        return override_file.query_exists ();
    }

    public bool has_game (Game game) {
        if (games_table == null)
            return false;

        return game.uid in games_table;
    }

    public Game? find_game_by_uid (string uid) {
        if (games_table == null)
            return null;

        return games_table[uid];
    }

    public async void import_game (Game game) throws Error {
        var settings = new Settings ("app.drey.Highscore");
        var library_path = settings.get_string ("library-dir");

        var library_dir = File.new_for_path (library_path);

        bool needs_copy = false;

        Media[] imported_media = {};

        foreach (var media in game.get_media ()) {
            if (!media.file.has_prefix (library_dir)) {
                needs_copy = true;
                break;
            }

            if (!needs_copy && media.extra_paths != null) {
                foreach (var extra_path in media.extra_paths) {
                    var extra_file = File.new_for_path (extra_path);

                    if (!extra_file.has_prefix (library_dir)) {
                        needs_copy = true;
                        break;
                    }
                }
            }

            if (needs_copy)
                break;
        }

        File dest_dir = null;

        if (needs_copy) {
            var imports_dir = FileUtils.get_directory_safely (library_dir, "Imports");
            var platform_dir = FileUtils.get_directory_safely (imports_dir, game.platform.name);

            var media = game.get_media ();

            if (media.length > 1 || media[0].extra_paths != null || game.cover != null) {
                var name = LibraryUtils.get_common_name (media) ?? game.title;

                dest_dir = FileUtils.get_directory_safely (platform_dir, name);
            } else {
                dest_dir = platform_dir;
            }

            if (!dest_dir.query_exists ())
                dest_dir.make_directory_with_parents ();
        }

        foreach (var media in game.get_media ()) {
            if (!needs_copy) {
                imported_media += new Media (media.uid, media.file, media.extra_paths);
                continue;
            }

            string[]? extra_paths = null;

            var rom_name = media.file.get_basename ();
            var dest = FileUtils.get_file_with_unique_name (dest_dir, rom_name);

            yield media.file.copy_async (dest, NONE);

            if (media.extra_paths != null) {
                extra_paths = {};

                foreach (var extra_path in media.extra_paths) {
                    var extra_file = File.new_for_path (extra_path);
                    var extra_dest = dest_dir.get_child (extra_file.get_basename ());

                    yield extra_file.copy_async (extra_dest, NONE);

                    extra_paths += extra_dest.get_path ();
                }
            }

            imported_media += new Media (media.uid, dest, extra_paths);
        }

        if (needs_copy && game.cover != null) {
            var info = game.cover.query_info (
                FileAttribute.STANDARD_CONTENT_TYPE,
                NONE
            );

            var content_type = info.get_content_type ();
            var mime_type = ContentType.get_mime_type (content_type);
            string cover_extension;

            if (mime_type == "image/png")
                cover_extension = "png";
            else if (mime_type == "image/jpeg")
                cover_extension = "jpg";
            else if (mime_type == "image/tiff")
                cover_extension = "tif";
            else
                assert_not_reached ();

            var dest = FileUtils.replace_extension (
                imported_media[0].file, cover_extension
            );

            yield game.cover.copy_async (dest, NONE);
        }

        var imported_game = new Game (
            imported_media,
            game.platform,
            game.uid,
            game.title,
            game.default_title,
            game.metadata,
            game.last_played
        );

        imported_game.cover = game.cover;

        database.add_game (imported_game);

        uint pos = games.insert_sorted (
            imported_game, (CompareDataFunc<Object>) Game.compare_id
        );
        games_table[imported_game.uid] = imported_game;

        model.items_changed (pos, 0, 1);

        recent_added ();
    }

    public void rename_game (Game game, string? title) throws Error {
        if (title == null)
            title = game.default_title;

        if (title == game.default_title)
            database.reset_game_title (game);
        else
            database.rename_game (game, title);

        game.title = title;

        game_renamed (game);
    }

    public string? find_title (Game game) throws Error {
        return database.find_game_title (game);
    }

    public Game? find_game_by_media (Media media) throws Error {
        var game_uid = database.find_game_uid_from_media (media);

        if (game_uid == null)
            return null;

        return find_game_by_uid (game_uid);
    }

    public void update_last_played (Game game) throws Error {
        bool newly_added = game.last_played == null;

        game.last_played = new DateTime.now_utc ();

        if (game.deferred_action == REMOVE_FROM_RECENT) {
            game.deferred_action = NONE;
            recent_added ();
        }

        database.update_last_played (game);

        if (newly_added)
            recent_added ();
        else
            recent_changed ();
    }

    public async void remove_from_recent (Game game) {
        if (deferred_action != REMOVE_FROM_RECENT) {
            if (deferred_action != NONE)
                yield commit ();

            deferred_action = REMOVE_FROM_RECENT;
        }

        game.deferred_action = deferred_action;
        deferred_games += game;

        recent_removed ();
    }

    public async bool can_trash (Game game) throws Error {
        var files = game.get_all_files ();

        foreach (var file in files) {
            var info = yield file.query_info_async (
                FileAttribute.ACCESS_CAN_TRASH,
                NONE
            );

            if (!info.get_attribute_boolean (FileAttribute.ACCESS_CAN_TRASH))
                return false;
        }

        return true;
    }

    public async void move_to_trash (Game game) {
        if (deferred_action != MOVE_TO_TRASH) {
            if (deferred_action != NONE)
                yield commit ();

            deferred_action = MOVE_TO_TRASH;
        }

        game.deferred_action = deferred_action;
        deferred_games += game;

        game_removed ();
    }

    public void undo () {
        foreach (var game in deferred_games)
            game.deferred_action = NONE;

        if (deferred_action == REMOVE_FROM_RECENT)
            recent_added ();

        if (deferred_action == MOVE_TO_TRASH)
            game_added ();

        deferred_games = {};
        deferred_action = NONE;
    }

    public async void commit () {
        if (deferred_action == NONE)
            return;

        var deferred_games = this.deferred_games;
        this.deferred_games = {};

        var action = deferred_action;
        deferred_action = NONE;

        if (action == REMOVE_FROM_RECENT) {
            foreach (var game in deferred_games) {
                try {
                    if (game.deferred_action == REMOVE_FROM_RECENT)
                        database.remove_from_recent (game);
                } catch (Error e) {
                    critical ("Failed to remove from recent: %s", e.message);
                }

                game.last_played = null;
                game.deferred_action = NONE;
            }
        }

        if (action == MOVE_TO_TRASH) {
            foreach (var game in deferred_games) {
                try {
                    yield trash_game (game);
                } catch (Error error) {
                    deferred_error (
                        _("Failed to move '%s' to trash: %s").printf (
                            Markup.escape_text (game.title), error.message
                        ),
                        error
                    );
                }
            }
        }
    }

    private async void trash_game (Game game) throws Error {
        uint position;

        assert (games.find_with_equal_func (game, (EqualFunc<Game>) Game.equal, out position));
        games.remove (position);

        games_table.remove (game.uid);
        database.remove_game (game, true);

        var files = game.get_all_files ();
        foreach (var file in files)
            yield file.trash_async ();

        model.items_changed (position, 1, 0);
    }

    public async void delete_game (Game game) throws Error {
        uint position;

        assert (games.find_with_equal_func (game, (EqualFunc<Game>) Game.equal, out position));
        games.remove (position);

        games_table.remove (game.uid);
        database.remove_game (game, true);

        var files = game.get_all_files ();
        foreach (var file in files)
            yield file.delete_async ();

        model.items_changed (position, 1, 0);
    }
}

private errordomain Highscore.LibraryError {
    INVALID_COVER_FORMAT
}
