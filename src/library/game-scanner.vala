// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.GameScanner : Object {
    public delegate void ProgressCallback (uint n_files, uint n_scanned, uint n_games);
    public delegate void GameCallback (Game game);

    private delegate void ScanFunc (File file, FileInfo info);

    private File library_dir;
    private unowned GameCallback game_cb;
    private unowned ProgressCallback progress_cb;

    private Cancellable cancellable;

    private uint n_files;
    private uint n_scanned;
    private uint n_games;

    private Gee.TreeSet<string> mime_types;

    private List<File> found_files;
    private HashTable<string, GameParser> media_set_parsers;

    private Mutex game_lock;

    public GameScanner (
        File library_dir,
        Cancellable? cancellable,
        GameCallback game_cb,
        ProgressCallback progress_cb
    ) {
        this.library_dir = library_dir;
        this.cancellable = cancellable;
        this.game_cb = game_cb;
        this.progress_cb = progress_cb;
    }

    public void scan () {
        n_files = 0;
        n_scanned = 0;
        n_games = 0;
        found_files = new List<File> ();
        media_set_parsers = new HashTable<string, GameParser> (str_hash, str_equal);

        mime_types = PlatformRegister.get_register ().list_mime_types ();

        if (cancellable != null && cancellable.is_cancelled ())
            return;

        try {
            var pool = new ThreadPool<File>.with_owned_data (
                file => scan_file (file), (int) get_num_processors (), true
            );

            if (cancellable != null) {
                cancellable.cancelled.connect (() => {
                    ThreadPool.free ((owned) pool, true, false);
                });
            }

            scan_directory (library_dir, count_file);

            found_files.reverse ();

            foreach (var file in found_files)
                pool.add (file);

            if (cancellable != null && !cancellable.is_cancelled ())
                ThreadPool.free ((owned) pool, false, true);
        } catch (ThreadError e) {
            critical ("Thread error: %s", e.message);
        }

        combine_media_sets ();
    }

    private void scan_directory (File directory, ScanFunc func) {
        FileEnumerator enumerator;

        try {
            enumerator = directory.enumerate_children (
                FileAttribute.STANDARD_NAME + "," +
                FileAttribute.STANDARD_TYPE + "," +
                FileAttribute.STANDARD_CONTENT_TYPE,
                NOFOLLOW_SYMLINKS,
                cancellable
            );
        } catch (Error e) {
            critical (
                "Failed to index directory %s: %s",
                directory.get_path (), e.message
            );
            return;
        }

        FileInfo info = null;

        while (true) {
            try {
                info = enumerator.next_file (cancellable);
            } catch (Error e) {
                critical (
                    "Failed to enumerate file: %s", e.message
                );
            }

            if (info == null)
                break;

            if (cancellable != null && cancellable.is_cancelled ())
                break;

            var name = info.get_name ();
            var file_type = info.get_file_type ();

            if (file_type == DIRECTORY)
                scan_directory (directory.get_child (name), func);
            else
                func (directory.get_child (name), info);
        }
    }

    private void count_file (File file, FileInfo info) {
        var content_type = info.get_content_type ();
        var mime_type = ContentType.get_mime_type (content_type);

        if (!(mime_type in mime_types))
            return;

        n_files++;
        found_files.prepend (file);
    }

    private void scan_file (File file) {
        string mime_type;

        if (cancellable != null && cancellable.is_cancelled ())
            return;

        try {
            var info = file.query_info (
                FileAttribute.STANDARD_CONTENT_TYPE,
                NOFOLLOW_SYMLINKS,
                cancellable
            );
            var content_type = info.get_content_type ();
            mime_type = ContentType.get_mime_type (content_type);
        } catch (Error e) {
            critical (
                "Failed to get MIME type from %s: %s", file.get_path (), e.message
            );
            return;
        }

        n_scanned++;

        var register = PlatformRegister.get_register ();
        var platforms = register.list_platforms_for_mime_type (mime_type);
        Game? game = null;

        foreach (var platform in platforms) {
            try {
                var parser = GameParser.create (platform, file);

                if (parser.parse ()) {
                    if (parser.get_incomplete ()) {
                        if (parser.current_media in media_set_parsers) {
                            report_progress ();
                            return;
                        }

                        media_set_parsers[parser.current_media] = parser;
                        report_progress ();
                        return;
                    }

                    var media = parser.create_media ();

                    game = parser.create_game ({ media });
                    break;
                }
            } catch (Error e) {
                critical (
                    "Failed to parse the game %s: %s\n",
                    file.get_path (), e.message
                );
                continue;
            }
        }

        if (cancellable != null && cancellable.is_cancelled ())
            return;

        add_game (game);
        report_progress ();
    }

    private void add_game (Game? game) {
        if (game == null)
            return;

        game.cover = LibraryUtils.find_cover (game, cancellable);
        n_games++;

        game_lock.lock ();
        game_cb (game);
        game_lock.unlock ();
    }

    private void report_progress () {
        progress_cb (n_files, n_scanned, n_games);
    }

    private void combine_media_sets () {
        media_set_parsers.foreach_steal ((media_id, parser) => {
            var media_set = parser.get_media_set ();

            Media[] media = {};

            foreach (var id in media_set) {
                if (!(id in media_set_parsers))
                    return true;

                try {
                    media += media_set_parsers[id].create_media ();
                } catch (Error e) {
                    critical ("Failed to create media: %s", e.message);
                }
            }

            try {
                var game = parser.create_game (media);

                add_game (game);
                report_progress ();
            } catch (Error e) {
                critical ("Failed to create game: %s", e.message);
            }

            return true;
        });

        if (PARSERS in Debug.get_flags ()) {
            media_set_parsers.foreach ((media_id, parser) => {
               message ("Unused media after scanning: %s", media_id);
           });
        }
    }
}
