// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.DatabaseUtils {
    public void exec (Sqlite.Database database, string query, Sqlite.Callback? callback) throws DatabaseError {
        string error_message;

        if (database.exec (query, callback, out error_message) != Sqlite.OK) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Execution failed: %s", error_message
            );
        }
    }

    public static Sqlite.Statement prepare (Sqlite.Database database, string query) throws DatabaseError {
        Sqlite.Statement statement;
        if (database.prepare_v2 (query, query.length, out statement) != Sqlite.OK) {
            throw new DatabaseError.PREPARATION_FAILED (
                "Preparation failed: %s", database.errmsg ()
            );
        }

        return statement;
    }

    public static void bind_text (Sqlite.Statement statement, string parameter, string? text) throws DatabaseError {
        var position = statement.bind_parameter_index (parameter);
        if (position <= 0) {
            throw new DatabaseError.BINDING_FAILED (
                "Couldn't bind text to the parameter “%s”, unexpected position: %d",
                parameter, position
            );
        }

        if (text != null)
            statement.bind_text (position, text);
        else
            statement.bind_null (position);
    }

    public static void bind_int (Sqlite.Statement statement, string parameter, int value) throws DatabaseError {
        var position = statement.bind_parameter_index (parameter);
        if (position <= 0) {
            throw new DatabaseError.BINDING_FAILED (
                "Couldn't bind text to the parameter “%s”, unexpected position: %d",
                parameter, position
            );
        }

        statement.bind_int (position, value);
    }
}
