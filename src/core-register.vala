// This file is part of Highscore. License: GPL-3.0-or-later

public errordomain Highscore.CoreError {
    INVALID_CORE_DESCRIPTOR,
    NOT_A_CORE,
}

public class Highscore.Core : Object {
    public string filename { get; private set; }
    public string name { get; private set; }
    public string version { get; private set; }
    public string? commit { get; private set; }
    public string[] authors { get; private set; }
    public string[] copyright { get; private set; }
    public string license { get; private set; }
    public int state_version { get; private set; }

    private Hs.Platform[] platforms;

    public Core (string core_filename) throws CoreError {
        assert (Module.supported ());

        filename = core_filename;

        try {
            var keyfile_path = Path.build_filename (
                Config.CORES_DIR,
                @"$core_filename.highscore"
            );

            var keyfile = new KeyFile ();
            keyfile.load_from_file (keyfile_path, KeyFileFlags.NONE);
            name = keyfile.get_string ("Highscore", "Name");
            version = keyfile.get_string ("Highscore", "Version");

            if (keyfile.has_key ("Highscore", "Commit")) {
                var commit = keyfile.get_string ("Highscore", "Commit");
                if (commit != "")
                    this.commit = commit;
            }

            authors = keyfile.get_string_list ("Highscore", "Authors");
            copyright = keyfile.get_string_list ("Highscore", "Copyright");
            license = keyfile.get_string ("Highscore", "License");
            var platform_names = keyfile.get_string_list ("Highscore", "Platforms");

            if (keyfile.has_key ("Highscore", "StateVersion"))
                state_version = keyfile.get_integer ("Highscore", "StateVersion");
            else
                state_version = 0;

            platforms = {};

            foreach (var platform_name in platform_names)
                platforms += Hs.Platform.get_from_name (platform_name);
        }
        catch (Error e) {
            throw new CoreError.INVALID_CORE_DESCRIPTOR ("Invalid core descriptor: %s", e.message);
        }
    }

    public Hs.Platform[] get_platforms () {
        return platforms;
    }

    public static int compare (Core a, Core b) {
        return a.name.collate (b.name);
    }
}

public class Highscore.CoreRegister : Object {
    private static CoreRegister instance;
    private HashTable<string, Core> cores;

    private CoreRegister () {
        cores = new HashTable<string, Core> (str_hash, str_equal);

        try {
            var directory = File.new_for_path (Config.CORES_DIR);
            var enumerator = directory.enumerate_children (FileAttribute.STANDARD_NAME, 0);

            FileInfo info;
            while ((info = enumerator.next_file ()) != null) {
                var name = info.get_name ();
                if (!name.has_suffix (".highscore"))
                    continue;

                // Remove the ".highscore" suffix
                name = name.substring (0, name.length - 10);

                try {
                    cores[name] = new Core (name);
                } catch (Error e) {
                    critical (
                        "Error when parsing '%s' descriptor: %s", name, e.message
                    );
                }
            }
        }
        catch (Error e) {
            debug (e.message);
        }
    }

    public static CoreRegister get_register () {
        if (instance == null)
            instance = new CoreRegister ();

        return instance;
    }

    public Core[] get_cores () {
        var values = cores.get_values ();
        var result = new Gee.ArrayList<Core> ();

        foreach (var core in values)
            result.add (core);

        result.sort (Core.compare);

        return result.to_array ();
    }

    public Core[] find_cores_for_platform (Platform platform) {
        var values = cores.get_values ();
        var result = new Gee.ArrayList<Core> ();

        foreach (var core in values) {
            var platforms = core.get_platforms ();

            if (platform.platform in platforms)
                result.add (core);
        }

        result.sort (Core.compare);

        return result.to_array ();
    }

    public Core? get_core_for_platform (Platform platform) {
        var settings = new Settings.with_path (
            "app.drey.Highscore.platform",
            @"/app/drey/Highscore/platform/$(platform.id)/"
        );

        var preferred_core = settings.get_string ("preferred-core");

        if (preferred_core == "auto") {
            var cores = find_cores_for_platform (platform);

            if (cores.length > 0)
                return cores[0];

            return null;
        }

        return cores[preferred_core];
    }
}
