// This file is part of Highscore. License: GPL-3.0-or-later

public errordomain Highscore.FirmwareError {
    MISSING,
    WRONG_MD5,
    WRONG_SHA512
}

public class Highscore.Firmware : Object {
    public struct PossibleChecksum {
        string md5;
        string sha512;
    }

    public string id { get; construct; }
    public string name { get; construct; }

    public string filename { get; set; }
    private PossibleChecksum[] checksums;

    public signal void changed ();

    public Firmware (string id, string name) {
        Object (id: id, name: name);

        checksums = {};
    }

    public File get_file () {
        var data_dir = Environment.get_user_data_dir ();
        var bios_path = Path.build_filename (data_dir, "highscore", "bios");
        var bios_dir = File.new_for_path (bios_path);

        return bios_dir.get_child (filename);
    }

    public string? get_current_md5 () {
        var file = get_file ();

        if (!file.query_exists ())
            return null;

        Bytes bytes;
        try {
            bytes = file.load_bytes (null, null);
        } catch (Error e) {
            critical ("Failed to get md5: %s", e.message);
            return null;
        }

        return Checksum.compute_for_bytes (MD5, bytes);
    }

    public string? get_current_sha512 () {
        var file = get_file ();

        if (!file.query_exists ())
            return null;

        Bytes bytes;
        try {
            bytes = file.load_bytes (null, null);
        } catch (Error e) {
            critical ("Failed to get sha512: %s", e.message);
            return null;
        }

        return Checksum.compute_for_bytes (SHA512, bytes);
    }

    public int get_version () {
        var file = get_file ();

        if (!file.query_exists ())
            return -1;

        Bytes bytes;
        try {
            bytes = file.load_bytes (null, null);
        } catch (Error e) {
            critical ("Failed to get version: %s", e.message);
            return -1;
        }

        var md5 = Checksum.compute_for_bytes (MD5, bytes);
        var sha512 = Checksum.compute_for_bytes (SHA512, bytes);

        for (int i = 0; i < checksums.length; i++) {
            var checksum = checksums[i];

            if (md5 == checksum.md5 && sha512 == checksum.sha512)
                return i;
        }

        return -1;
    }

    public void add_checksum (string md5, string sha512) {
        PossibleChecksum checksum = { md5, sha512 };

        checksums += checksum;
    }

    public bool has_multiple_checksums () {
        return checksums.length > 1;
    }

    private void check_possible_checksum (
        string md5,
        string sha512,
        PossibleChecksum sum
    ) throws FirmwareError {
        if (md5 != sum.md5)
            throw new FirmwareError.WRONG_MD5 ("Wrong MD5 sum");

        if (sha512 != sum.sha512)
            throw new FirmwareError.WRONG_SHA512 ("Wrong SHA-512 sum");
    }

    public void check () throws Error {
        var file = get_file ();

        if (!file.query_exists ())
            throw new FirmwareError.MISSING ("Firmware is missing");

        var bytes = file.load_bytes (null, null);
        var md5 = Checksum.compute_for_bytes (MD5, bytes);
        var sha512 = Checksum.compute_for_bytes (SHA512, bytes);

        foreach (var checksum in checksums) {
            try {
                check_possible_checksum (md5, sha512, checksum);
                return;
            } catch (FirmwareError e) {
                continue;
            }
        }

        throw new FirmwareError.WRONG_SHA512 ("Wrong checksum");
    }

    public string get_md5sum () throws Error {
        var file = get_file ();

        if (!file.query_exists ())
            throw new FirmwareError.MISSING ("Firmware is missing");

        var bytes = file.load_bytes (null, null);
        return Checksum.compute_for_bytes (MD5, bytes);
    }

    public string get_sha512sum () throws Error {
        var file = get_file ();

        if (!file.query_exists ())
            throw new FirmwareError.MISSING ("Firmware is missing");

        var bytes = file.load_bytes (null, null);
        return Checksum.compute_for_bytes (SHA512, bytes);
    }

    public bool matches (string? md5, string? sha512) {
        assert (md5 != null || sha512 != null);

        foreach (var checksum in checksums) {
            bool md5_matches = md5 == null || checksum.md5 == md5;
            bool sha512_matches = sha512 == null || checksum.sha512 == sha512;

            if (md5_matches && sha512_matches)
                return true;
        }

        return false;
   }
}
