// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.FirmwareList : Object {
    public Platform platform { get; construct; }

    private Firmware[] firmware;

    public signal void changed (Firmware firmware);

    construct {
        firmware = {};
    }

    public Firmware[] list_firmware () {
        return firmware;
    }

    public Firmware? get_firmware (string id) {
        foreach (var f in firmware) {
            if (f.id == id)
                return f;
        }
        return null;
    }

    protected void add_firmware (Firmware firmware) {
        this.firmware += firmware;

        firmware.changed.connect (() => changed (firmware));
    }

    public static FirmwareList? create (Platform platform) {
        var type = platform.firmware_list_type;

        if (type == Type.NONE && platform.parent != null)
            type = platform.parent.firmware_list_type;

        if (type == Type.NONE)
            return null;

        return Object.new (type, platform: platform) as FirmwareList;
    }

    public void check () throws Error {
        foreach (var f in firmware)
            f.check ();
    }

    public virtual async void load (string id, File file, Runner runner) throws Error {}

    public virtual async Firmware[] get_used_firmware (Runner runner) throws Error {
        return firmware;
    }
}
