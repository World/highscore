// This file is part of Highscore. License: GPL-3.0-or-later

public errordomain Highscore.VideoFilterError {
    INVALID_FILTER,
    DUPLICATE_PASS_NAME,
    MISSING_NAME,
    INVALID_HISTORY,
    INVALID_PASSES,
    INVALID_DIMENSION,
    INVALID_VECTOR,
    INVALID_FILTER_MODE,
    INVALID_WRAP_MODE,
    INVALID_FORMAT,
    INVALID_MODULO,
    INVALID_FILE,
    INVALID_UNIFORM,
    COMPILE_FAILURE,
}

public class Highscore.VideoFilterDescription : Object {
    private static Regex? dimension_regex = null;
    private static Regex? vec2_regex = null;
    private static Regex? vec3_regex = null;
    private static Regex? vec4_regex = null;
    private const string FLOAT_REGEX = "(-?[0-9]+\\.[0-9]+)";

    private const VideoFilterDimensionType DEFAULT_DIMENSION = FILL;
    private const GLFilterMode DEFAULT_FILTER = NEAREST;
    private const GLWrapMode DEFAULT_WRAP = BORDER;
    private const GLInternalTextureFormat DEFAULT_FORMAT = RGBA8;
    private const int DEFAULT_MODULO = 300;

    public string name { get; private set; }
    public string display_name { get; private set; }
    public bool is_internal { get; private set; }
    public string? internal_name { get; private set; }
    public string[]? authors { get; private set; }
    public string[]? copyright { get; private set; }
    public Gtk.License license { get; private set; }
    public string? license_text { get; private set; }
    public string[]? sources { get; private set; }
    public int history_frames { get; private set; }

    private Pass[] passes;

    public struct Pass {
        string? name;
        VideoFilterDimensionType width_type;
        float width;
        VideoFilterDimensionType height_type;
        float height;
        GLFilterMode filter_mode;
        GLWrapMode wrap_mode;
        GLInternalTextureFormat format;
        int modulo;
        string? vertex;
        string? fragment;
        HashTable<string, VideoFilterUniform> uniforms;
        bool implicit;
        File dir;
    }

    public VideoFilterDescription (File dir, string name) throws VideoFilterError {
        this.name = name;

        try {
            var path = Path.build_filename (dir.peek_path (), @"$name.filter");
            var keyfile = new KeyFile ();
            keyfile.load_from_file (path, KeyFileFlags.NONE);

            if (keyfile.has_key ("Metadata", "Name"))
                display_name = keyfile.get_string ("Metadata", "Name");
            else
                throw new VideoFilterError.MISSING_NAME ("Filter must have a name");

            if (keyfile.has_key ("Metadata", "Internal"))
                is_internal = keyfile.get_boolean ("Metadata", "Internal");
            else
                is_internal = false;

            if (keyfile.has_key ("Metadata", "InternalName"))
                internal_name = keyfile.get_string ("Metadata", "InternalName");
            else
                internal_name = null;

            if (keyfile.has_key ("Metadata", "Authors"))
                authors = keyfile.get_string_list ("Metadata", "Authors");
            else
                authors = null;

            if (keyfile.has_key ("Metadata", "Copyright"))
                copyright = keyfile.get_string_list ("Metadata", "Copyright");
            else
                copyright = null;

            string? license_type_str;
            if (keyfile.has_key ("Metadata", "License"))
                license_type_str = keyfile.get_string ("Metadata", "License");
            else
                license_type_str = null;

            if (keyfile.has_key ("Metadata", "LicencePath")) {
                var license_path = keyfile.get_string ("Metadata", "LicencePath");

                try {
                    license_text = read_file_with_dir (dir, license_path).strip ();
                } catch (Error e) {
                    throw new VideoFilterError.INVALID_FILE (
                        "Failed to read license: %s", e.message
                    );
                }
            } else {
                license_text = null;
            }

            if (license_type_str != null)
                license = LicenseUtils.spdx_to_license (license_type_str);
            else if (license_text != null)
                license = CUSTOM;
            else
                license = UNKNOWN;

            if (keyfile.has_key ("Metadata", "Sources"))
                sources = keyfile.get_string_list ("Metadata", "Sources");
            else
                sources = null;

            int n_passes = 1;

            if (keyfile.has_key ("Metadata", "Passes")) {
                n_passes = keyfile.get_integer ("Metadata", "Passes");

                if (n_passes < 1) {
                    throw new VideoFilterError.INVALID_PASSES (
                        "Filter must have 1 or more passes, specified %d", n_passes
                    );
                }
            }

            if (keyfile.has_key ("Metadata", "HistoryFrames")) {
                history_frames = keyfile.get_integer ("Metadata", "HistoryFrames");

                if (history_frames < 0) {
                    throw new VideoFilterError.INVALID_HISTORY (
                        "Filter cannot have a negative number of history " +
                        "frames, specified %d", n_passes
                    );
                }
            } else {
                history_frames = 0;
            }

            passes = {};
            var pass_names = new GenericSet<string> (str_hash, str_equal);

            for (int i = 0; i < n_passes; i++) {
                var pass = try_parse_pass (dir, keyfile, i);

                if (pass.name != null) {
                    if (pass.name in pass_names) {
                        throw new VideoFilterError.DUPLICATE_PASS_NAME (
                            "Duplicate pass name: %s", pass.name
                        );
                    }

                    pass_names.add (pass.name);
                }

                passes += pass;
            }
        } catch (KeyFileError e) {
            throw new VideoFilterError.INVALID_FILTER ("Invalid filter: %s", e.message);
        } catch (FileError e) {
            throw new VideoFilterError.INVALID_FILTER ("Invalid filter: %s", e.message);
        }

        add_final_pass ();
    }

    public VideoFilterDescription.concatenate (VideoFilterDescription[] children) throws VideoFilterError {
        string[] names = {};

        foreach (var child in children)
            names += child.name;

        name = string.joinv (":", names);

        string[] display_names = {};
        string[]? authors = null;
        string[]? copyright = null;
        string[]? license_texts = null;
        string[]? sources = null;

        history_frames = 0;
        passes = {};

        var pass_names = new GenericSet<string> (str_hash, str_equal);

        foreach (var child in children) {
            is_internal = true;

            display_names += child.display_name;

            if (child.authors != null) {
                if (authors == null)
                    authors = {};

                foreach (var author in child.authors)
                    authors += author;
            }

            if (child.copyright != null) {
                if (copyright == null)
                    copyright = {};

                foreach (var copyright_line in child.copyright)
                    copyright += copyright_line;
            }

            if (child.license != UNKNOWN) {
                if (license_texts == null)
                    license_texts = {};

                license_texts += child.license.to_string ();
            } else if (child.license_text != null) {
                if (license_texts == null)
                    license_texts = {};

                license_texts += child.license_text;
            }

            if (child.sources != null) {
                if (sources == null)
                    sources = {};

                foreach (var source in child.sources)
                    sources += source;
            }

            history_frames = int.max (history_frames, child.history_frames);

            foreach (var pass in child.passes) {
                if (pass.implicit)
                    continue;

                if (pass.name != null) {
                    if (pass.name in pass_names) {
                        throw new VideoFilterError.DUPLICATE_PASS_NAME (
                            "Duplicate pass name: %s", pass.name
                        );
                    }

                    pass_names.add (pass.name);
                }

                passes += pass;
            }
        }

        display_name = string.joinv (" + ", display_names);

        this.authors = authors;
        this.copyright = copyright;
        this.sources = sources;

        if (license_texts != null)
            license_text = string.joinv ("\n\n", license_texts);

        add_final_pass ();
    }

    private void add_final_pass () {
        var last_pass = passes[passes.length - 1];

        if (last_pass.width_type == FILL && last_pass.height_type == FILL)
            return;

        Pass final_pass = {
            null, DEFAULT_DIMENSION, 0, DEFAULT_DIMENSION, 0,
            DEFAULT_FILTER, DEFAULT_WRAP, DEFAULT_FORMAT, DEFAULT_MODULO
        };

        final_pass.implicit = true;
        final_pass.dir = last_pass.dir;

        passes += final_pass;
    }

    private Pass try_parse_pass (File dir, KeyFile keyfile, int i) throws KeyFileError, VideoFilterError {
        Pass ret = {};
        var group_name = @"Pass $(i + 1)";

        ret.dir = dir;

        if (keyfile.has_key (group_name, "Name"))
            ret.name = keyfile.get_string (group_name, "Name").strip ();
        else
            ret.name = null;

        if (keyfile.has_key (group_name, "Width")) {
            string width_str = keyfile.get_string (group_name, "Width");
            ret.width = parse_dimension (width_str, out ret.width_type);
        } else {
            ret.width = 0;
            ret.width_type = FILL;
        }

        if (keyfile.has_key (group_name, "Height")) {
            string height_str = keyfile.get_string (group_name, "Height");
            ret.height = parse_dimension (height_str, out ret.height_type);
        } else {
            ret.height = -1;
            ret.height_type = FILL;
        }

        if (keyfile.has_key (group_name, "Filter")) {
            string filter_str = keyfile.get_string (group_name, "Filter");
            var filter_mode = GLFilterMode.from_string (filter_str);

            if (filter_mode == null) {
                throw new VideoFilterError.INVALID_FILTER_MODE (
                    "Invalid filter mode: %s", filter_str
                );
            } else {
                ret.filter_mode = filter_mode;
            }
        } else {
            ret.filter_mode = DEFAULT_FILTER;
        }

        if (keyfile.has_key (group_name, "Wrap")) {
            string wrap_str = keyfile.get_string (group_name, "Wrap");
            var wrap_mode = GLWrapMode.from_string (wrap_str);

            if (wrap_mode == null) {
                throw new VideoFilterError.INVALID_WRAP_MODE (
                    "Invalid wrap mode: %s", wrap_str
                );
            } else {
                ret.wrap_mode = wrap_mode;
            }
        } else {
            ret.wrap_mode = DEFAULT_WRAP;
        }

        if (keyfile.has_key (group_name, "Format")) {
            string format_str = keyfile.get_string (group_name, "Format");
            var format = GLInternalTextureFormat.from_string (format_str);

            if (format == null) {
                throw new VideoFilterError.INVALID_FORMAT (
                    "Invalid format: %s", format_str
                );
            } else {
                ret.format = format;
            }
        } else {
            ret.format = DEFAULT_FORMAT;
        }

        if (keyfile.has_key (group_name, "Modulo")) {
            ret.modulo = keyfile.get_integer (group_name, "Modulo");

            if (ret.modulo < 1) {
                throw new VideoFilterError.INVALID_MODULO (
                    "Modulo must be greater than 0, specified %d", ret.modulo
                );
            }
        } else {
            ret.modulo = DEFAULT_MODULO;
        }

        if (keyfile.has_key (group_name, "Vertex")) {
            ret.vertex = keyfile.get_string (group_name, "Vertex");

            ensure_file (dir, ret.vertex);
        } else {
            ret.vertex = null;
        }

        if (keyfile.has_key (group_name, "Fragment")) {
            ret.fragment = keyfile.get_string (group_name, "Fragment");

            ensure_file (dir, ret.fragment);
        } else {
            ret.fragment = null;
        }

        ret.uniforms = new HashTable<string, VideoFilterUniform> (str_hash, str_equal);

        var keys = keyfile.get_keys (group_name);

        foreach (var key in keys) {
            if (!key.has_prefix ("Uniform "))
                continue;

            var param_name = key.substring (8).strip ();
            var str_value = keyfile.get_string (group_name, key).strip ();

            if (str_value == "true") {
                ret.uniforms[param_name] = new VideoFilterUniform.bool (true);
                continue;
            }

            if (str_value == "false") {
                ret.uniforms[param_name] = new VideoFilterUniform.bool (false);
                continue;
            }

            try {
                int value = keyfile.get_integer (group_name, key);

                ret.uniforms[param_name] = new VideoFilterUniform.int (value);
                continue;
            } catch (Error e) {
                // Do nothing, we should try it as double as well
            }

            try {
                float value = (float) keyfile.get_double (group_name, key);

                ret.uniforms[param_name] = new VideoFilterUniform.float (value);
                continue;
            } catch (Error e) {
            }

            if (str_value.has_prefix ("vec2")) {
                var vec = parse_vec2 (str_value);

                ret.uniforms[param_name] = new VideoFilterUniform.vec2 (vec);
                continue;
            }

            if (str_value.has_prefix ("vec3")) {
                var vec = parse_vec3 (str_value);

                ret.uniforms[param_name] = new VideoFilterUniform.vec3 (vec);
                continue;
            }

            if (str_value.has_prefix ("vec4")) {
                var vec = parse_vec4 (str_value);

                ret.uniforms[param_name] = new VideoFilterUniform.vec4 (vec);
                continue;
            }

            throw new VideoFilterError.INVALID_UNIFORM (
                "Uniform %s for pass %d has invalid value: %s", param_name, i + 1, str_value
            );
        }

        return ret;
    }

    private float parse_dimension (
        string dimension_str,
        out VideoFilterDimensionType dimension_type
    ) throws VideoFilterError {
        if (dimension_str == "fill") {
            dimension_type = FILL;
            return 0;
        }

        float value = 0;

        if (dimension_regex == null) {
            try {
                dimension_regex = new Regex ("^([0-9]+(\\.[0-9]+)?)(%|px)$");
            } catch (Error e) {
                error ("Invalid regex: %s", e.message);
            }
        }

        MatchInfo info;
        if (dimension_regex.match (dimension_str, 0, out info)) {
            string value_str = info.fetch (1);
            string prefix = info.fetch (3);

            if (prefix == "%")
                dimension_type = PERCENTAGE;
            else
                dimension_type = ABSOLUTE;

            value = float.parse (value_str);
        } else {
            throw new VideoFilterError.INVALID_DIMENSION (
                "Invalid dimension: %s", dimension_str
            );
        }

        if (dimension_type == PERCENTAGE)
            return value / 100.0f;

        return value;
    }

    private Graphene.Vec2 parse_vec2 (string str) throws VideoFilterError {
        if (vec2_regex == null) {
            try {
                vec2_regex = new Regex (
                    "^vec2\\(\\s*" +
                        FLOAT_REGEX + "\\s*,\\s*" +
                        FLOAT_REGEX +
                    "\\s*\\)$"
                );
            } catch (Error e) {
                error ("Invalid regex: %s", e.message);
            }
        }

        float x, y;

        MatchInfo info;
        if (vec2_regex.match (str, 0, out info)) {
            string x_str = info.fetch (1);
            string y_str = info.fetch (2);

            x = float.parse (x_str);
            y = float.parse (y_str);
        } else {
            throw new VideoFilterError.INVALID_VECTOR (
                "Invalid vec2: %s", str
            );
        }

        Graphene.Vec2 ret = {};
        ret.init (x, y);

        return ret;
    }

    private Graphene.Vec3 parse_vec3 (string str) throws VideoFilterError {
        if (vec3_regex == null) {
            try {
                vec3_regex = new Regex (
                    "^vec3\\(\\s*" +
                        FLOAT_REGEX + "\\s*,\\s*" +
                        FLOAT_REGEX + "\\s*,\\s*" +
                        FLOAT_REGEX +
                    "\\s*\\)$"
                );
            } catch (Error e) {
                error ("Invalid regex: %s", e.message);
            }
        }

        float x, y, z;

        MatchInfo info;
        if (vec3_regex.match (str, 0, out info)) {
            string x_str = info.fetch (1);
            string y_str = info.fetch (2);
            string z_str = info.fetch (3);

            x = float.parse (x_str);
            y = float.parse (y_str);
            z = float.parse (z_str);
        } else {
            throw new VideoFilterError.INVALID_VECTOR (
                "Invalid vec3: %s", str
            );
        }

        Graphene.Vec3 ret = {};
        ret.init (x, y, z);

        return ret;
    }

    private Graphene.Vec4 parse_vec4 (string str) throws VideoFilterError {
        if (vec4_regex == null) {
            try {
                vec4_regex = new Regex (
                    "^vec4\\(\\s*" +
                        FLOAT_REGEX + "\\s*,\\s*" +
                        FLOAT_REGEX + "\\s*,\\s*" +
                        FLOAT_REGEX + "\\s*,\\s*" +
                        FLOAT_REGEX +
                    "\\s*\\)$"
                );
            } catch (Error e) {
                error ("Invalid regex: %s", e.message);
            }
        }

        float x, y, z, w;

        MatchInfo info;
        if (vec4_regex.match (str, 0, out info)) {
            string x_str = info.fetch (1);
            string y_str = info.fetch (2);
            string z_str = info.fetch (3);
            string w_str = info.fetch (4);

            x = float.parse (x_str);
            y = float.parse (y_str);
            z = float.parse (z_str);
            w = float.parse (w_str);
        } else {
            throw new VideoFilterError.INVALID_VECTOR (
                "Invalid vec4: %s", str
            );
        }

        Graphene.Vec4 ret = {};
        ret.init (x, y, z, w);

        return ret;
    }

    public string read_file (Pass pass, string file_path) throws Error {
        return read_file_with_dir (pass.dir, file_path);
    }

    public string read_file_with_dir (File dir, string file_path) throws Error {
        var file = File.new_build_filename (dir.peek_path (), file_path);

        uint8[] contents;
        file.load_contents (null, out contents, null);

        return (string) contents;
    }

    private void ensure_file (File dir, string file_path) throws VideoFilterError {
        var file = File.new_build_filename (dir.peek_path (), file_path);

        if (!file.query_exists ()) {
            throw new VideoFilterError.INVALID_FILE (
                "File %s doesn't exist", file_path
            );
        }
    }

    public Pass[] get_passes () {
        return passes;
    }

    public static int compare (VideoFilterDescription a, VideoFilterDescription b) {
        return a.display_name.collate (b.display_name);
    }
}
