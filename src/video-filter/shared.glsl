#ifndef CORE
#  define out varying

#  ifdef VERTEX
#    define in attribute
#  else
#    define in varying
#  endif

#  define texture texture2D
#endif

#if !defined(LEGACY)
precision highp float;
#endif

uniform sampler2D u_original;
uniform vec4 u_originalSize;

uniform sampler2D u_source;
uniform vec4 u_sourceSize;

uniform vec4 u_outputSize;
uniform vec4 u_viewportSize;
uniform float u_aspectRatio;
uniform int u_phase;

#ifdef VERTEX

in vec2 position;
in vec2 texCoord;

out vec2 v_texCoord;

uniform mat4 u_mvp;

void hs_main();

void main() {
  v_texCoord = texCoord;

  gl_Position = u_mvp * vec4(position, 0, 1);

  hs_main();
}

#else // FRAGMENT

in vec2 v_texCoord;

#ifdef CORE
out vec4 outputColor;
#endif

uniform float u_opacity;

vec4 hs_main();

void main() {
  vec4 result = hs_main() * u_opacity;

#ifdef CORE
  outputColor = result;
#else
  gl_FragColor = result;
#endif
}

#endif