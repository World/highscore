// This file is part of Highscore. License: GPL-3.0-or-later

using GL;

public enum Highscore.VideoFilterDimensionType {
    FILL,
    PERCENTAGE,
    ABSOLUTE
}

public class Highscore.VideoFilter : Object {
    public const string DEFAULT_VERTEX_SHADER = """
        void hs_main() {}
    """;

    public const string DEFAULT_FRAGMENT_SHADER = """
        vec4 hs_main() {
            return texture(u_source, v_texCoord);
        }
    """;

    public VideoFilterDescription description { get; construct; }

    public string name {
        get { return description.name; }
    }

    public int n_passes {
        get { return passes.length; }
    }

    public GLFilterMode filter_mode {
        get { return description.get_passes ()[0].filter_mode; }
    }

    public GLWrapMode wrap_mode {
        get { return description.get_passes ()[0].wrap_mode; }
    }

    public int history_frames {
        get { return description.history_frames; }
    }

    private class ScreenData {
        public Graphene.Matrix mvp;
        public double opacity;

        public GLFramebuffer framebuffer;
        public GLFramebuffer[] history;

        public int viewport_width;
        public int viewport_height;

        public float aspect_ratio;
    }

    private class PassScreenData {
        public GLFramebuffer? framebuffer;

        public int output_width;
        public int output_height;
    }

    private class PassData {
        public int phase;

        public string vertex;
        public string fragment;

        public GLShader shader;

        public PassScreenData[] screens;

        public bool is_final;
    }

    private int view_width;
    private int view_height;

    private ScreenData[] screens;
    private PassData[] passes;
    private int n_screens;

    private bool realized;

    public VideoFilter (VideoFilterDescription description) {
        Object (description: description);
    }

    construct {
        foreach (var desc in description.get_passes ()) {
            var data = new PassData ();

            if (desc.vertex != null) {
                try {
                    data.vertex = description.read_file (desc, desc.vertex);
                } catch (Error e) {
                    critical (
                        "Failed to read vertex shader %s: %s",
                        desc.vertex, e.message
                    );

                    data.vertex = DEFAULT_VERTEX_SHADER;
                }
            } else {
                data.vertex = DEFAULT_VERTEX_SHADER;
            }

            if (desc.fragment != null) {
                try {
                    data.fragment = description.read_file (desc, desc.fragment);
                } catch (Error e) {
                    critical (
                        "Failed to read fragment shader %s: %s",
                        desc.fragment, e.message
                    );

                    data.fragment = DEFAULT_FRAGMENT_SHADER;
                }
            } else {
                data.fragment = DEFAULT_FRAGMENT_SHADER;
            }

            data.is_final = false;

            passes += data;
            data.screens = {};
        }

        passes[passes.length - 1].is_final = true;

        screens = {};
    }

    public void realize (Gdk.GLContext context) throws VideoFilterError {
        var shared = ResourceUtils.load_to_string (
            "/app/drey/Highscore/video-filter/shared.glsl"
        );

        for (int i = 0; i < passes.length; i++) {
            var data = passes[i];

            var builder = new StringBuilder ();

            builder.append (shared);

            builder.append ("\n\n");

            foreach (var pass_desc in description.get_passes ()) {
                if (pass_desc.name != null) {
                    builder.append_printf (
                        "uniform sampler2D u_passOutput%s;\n", pass_desc.name
                    );
                    builder.append_printf (
                        "uniform vec4 u_passOutput%sSize;\n", pass_desc.name
                    );
                    builder.append ("\n");
                }
            }

            for (int j = 0; j < history_frames; j++) {
                builder.append_printf ("uniform sampler2D u_history%d;\n", j + 1);
                builder.append_printf ("uniform vec4 u_history%dSize;\n", j + 1);
                builder.append ("\n");
            }

            builder.append ("#ifdef VERTEX");
            builder.append ("\n\n");

            builder.append (data.vertex);

            builder.append ("\n\n");
            builder.append ("#else // FRAGMENT");
            builder.append ("\n\n");

            builder.append (data.fragment);

            builder.append ("\n\n");
            builder.append ("#endif");

            try {
                data.shader = new GLShader (context, builder.free_and_steal ());
            } catch (GLShaderError e) {
                throw new VideoFilterError.COMPILE_FAILURE (
                    "Error in %s filter: %s", description.name, e.message
                );
            }
        }

        realized = true;
    }

    public void unrealize () {
        for (int i = 0; i < passes.length; i++) {
            var data = passes[i];

            data.shader = null;
            data.screens = {};
        }

        screens = {};
        n_screens = 0;

        realized = false;
    }

    private void set_n_screens (int n) {
        if (n_screens == n)
            return;

        n_screens = n;

        if (n_screens > screens.length) {
            for (int i = screens.length; i < n_screens; i++) {
                screens += new ScreenData ();

                foreach (var pass in passes) {
                    var data = new PassScreenData ();

                    if (!pass.is_final)
                        data.framebuffer = new GLFramebuffer ();

                    pass.screens += data;
                }
            }
        } else if (n_screens < screens.length) {
            screens = screens[:n_screens];

            foreach (var pass in passes)
                pass.screens = pass.screens[:n_screens];
        }
    }

    public struct ScreenSize {
        int width;
        int height;
        int viewport_width;
        int viewport_height;
        float aspect_ratio;
    }

    public void resize (int view_width, int view_height, ScreenSize[] screen_sizes) requires (realized) {
        set_n_screens (screen_sizes.length);

        this.view_width = view_width;
        this.view_height = view_height;

        for (int i = 0; i < screens.length; i++) {
            float width = screen_sizes[i].width;
            float height = screen_sizes[i].height;

            screens[i].viewport_width = screen_sizes[i].viewport_width;
            screens[i].viewport_height = screen_sizes[i].viewport_height;

            screens[i].aspect_ratio = (float) screen_sizes[i].aspect_ratio;

            for (int j = 0; j < passes.length; j++) {
                var desc = description.get_passes ()[j];
                var data = passes[j];
                var screen = data.screens[i];

                switch (desc.width_type) {
                    case FILL:
                        width = screen_sizes[i].viewport_width;
                        break;
                    case PERCENTAGE:
                        width *= desc.width;
                        break;
                    case ABSOLUTE:
                        width = desc.width;
                        break;
                }

                switch (desc.height_type) {
                    case FILL:
                        height = screen_sizes[i].viewport_height;
                        break;
                    case PERCENTAGE:
                        height *= desc.height;
                        break;
                    case ABSOLUTE:
                        height = desc.height;
                        break;
                }

                int prev_output_width = screen.output_width;
                int prev_output_height = screen.output_height;

                screen.output_width = (int) Math.ceil (width);
                screen.output_height = (int) Math.ceil (height);

                if (data.is_final)
                    continue;

                var next_desc = description.get_passes ()[j + 1];

                if (screen.output_width != prev_output_width ||
                    screen.output_height != prev_output_height) {
                    screen.framebuffer.bind ();
                    screen.framebuffer.create_texture (
                        screen.output_width, screen.output_height,
                        next_desc.filter_mode, next_desc.filter_mode,
                        next_desc.wrap_mode, desc.format
                    );
                    screen.framebuffer.unbind ();
                }
            }
        }
    }

    public void bind (int pass) requires (realized) {
        var data = passes[pass];

        data.shader.bind ();
        data.shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        data.shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);
    }

    public void unbind (int pass) requires (realized) {
        var data = passes[pass];

        data.shader.unbind ();
    }

    private inline void bind_fb_texture (GLShader shader, int tex, string name, GLFramebuffer framebuffer) {
        GLUtils.set_active_texture (tex);
        framebuffer.bind_texture ();
        shader.set_uniform_int (name, tex);

        int width = framebuffer.width;
        int height = framebuffer.height;

        Graphene.Vec4 size = {};
        size.init (width, height, 1.0f / width, 1.0f / height);
        shader.set_uniform_vec4 (@"$(name)Size", size);
    }

    private inline void unbind_fb_texture (int tex, GLFramebuffer framebuffer) {
        GLUtils.set_active_texture (tex);
        framebuffer.unbind_texture ();
    }

    public void start_render (int pass, int screen) requires (realized) {
        var screen_data = screens[screen];
        var desc = description.get_passes ()[pass];
        var pass_data = passes[pass];
        var pass_screen_data = pass_data.screens[screen];
        var shader = pass_data.shader;

        int next_texture = 0;
        bind_fb_texture (shader, next_texture, "u_original", screen_data.framebuffer);

        if (pass > 0) {
            var prev_pass_data = passes[pass - 1];
            var prev_pass_desc = description.get_passes ()[pass - 1];
            var prev_pass_screen_data = prev_pass_data.screens[screen];
            bind_fb_texture (
                shader, ++next_texture, "u_source",
                prev_pass_screen_data.framebuffer
            );
            bind_fb_texture (
                shader, next_texture, @"u_passOutput$(pass)",
                prev_pass_screen_data.framebuffer
            );

            if (prev_pass_desc.name != null) {
                bind_fb_texture (
                    shader, next_texture, @"u_passOutput$(prev_pass_desc.name)",
                    prev_pass_screen_data.framebuffer
                );
            }

            for (int i = 0; i < pass - 1; i++) {
                var nth_pass_data = passes[i];
                var nth_pass_desc = description.get_passes ()[i];
                var nth_pass_screen_data = nth_pass_data.screens[screen];

                next_texture++;

                bind_fb_texture (
                    shader, next_texture, @"u_passOutput$(i + 1)",
                    nth_pass_screen_data.framebuffer
                );

                if (nth_pass_desc.name != null) {
                    bind_fb_texture (
                        shader, next_texture, @"u_passOutput$(nth_pass_desc.name)",
                        nth_pass_screen_data.framebuffer
                    );
                }
            }
        } else {
            bind_fb_texture (shader, next_texture, "u_source", screen_data.framebuffer);
        }

        for (int j = 0; j < history_frames; j++) {
            GLFramebuffer fb;

            if (screen_data.history.length == 0)
                fb = screen_data.framebuffer;
            else if (j >= screen_data.history.length)
                fb = screen_data.history[screen_data.history.length - 1];
            else
                fb = screen_data.history[j];

            bind_fb_texture (shader, ++next_texture, @"u_history$(j + 1)", fb);
        }

        if (pass_data.is_final) {
            glViewport (0, 0, view_width, view_height);

            shader.set_uniform_matrix ("u_mvp", screen_data.mvp);
            shader.set_uniform_float ("u_opacity", (float) screen_data.opacity);
        } else {
            pass_screen_data.framebuffer.bind (GL_DRAW_FRAMEBUFFER);

            glViewport (0, 0, pass_screen_data.output_width, pass_screen_data.output_height);

            Graphene.Matrix mvp = {};
            mvp.init_identity ();
            mvp.scale (2, 2, 1);
            mvp.translate ({ -1, -1, 0 });
            shader.set_uniform_matrix ("u_mvp", mvp);

            shader.set_uniform_float ("u_opacity", 1.0f);
        }

        Graphene.Vec4 size = {};

        size.init (
            pass_screen_data.output_width,
            pass_screen_data.output_height,
            1.0f / pass_screen_data.output_width,
            1.0f / pass_screen_data.output_height
        );
        shader.set_uniform_vec4 ("u_outputSize", size);

        size.init (
            screen_data.viewport_width,
            screen_data.viewport_height,
            1.0f / screen_data.viewport_width,
            1.0f / screen_data.viewport_height
        );
        shader.set_uniform_vec4 ("u_viewportSize", size);

        shader.set_uniform_float ("u_aspectRatio", screen_data.aspect_ratio);

        shader.set_uniform_int ("u_phase", pass_data.phase);

        if (desc.uniforms != null) {
            foreach (var key in desc.uniforms.get_keys ()) {
                unowned var uniform = desc.uniforms[key];

                switch (uniform.get_type ()) {
                    case BOOLEAN:
                        shader.set_uniform_bool (key, uniform.get_bool ());
                        break;
                    case INT:
                        shader.set_uniform_int (key, uniform.get_int ());
                        break;
                    case FLOAT:
                        shader.set_uniform_float (key, uniform.get_float ());
                        break;
                    case VEC2:
                        shader.set_uniform_vec2 (key, uniform.get_vec2 ());
                        break;
                    case VEC3:
                        shader.set_uniform_vec3 (key, uniform.get_vec3 ());
                        break;
                    case VEC4:
                        shader.set_uniform_vec4 (key, uniform.get_vec4 ());
                        break;
                    default:
                        assert_not_reached ();
                }
            }
        }
    }

    public void end_render (int pass, int screen) requires (realized) {
        var pass_data = passes[pass];
        var pass_screen_data = pass_data.screens[screen];
        var screen_data = screens[screen];

        int next_texture = 0;
        unbind_fb_texture (next_texture, screen_data.framebuffer);

        if (pass > 0) {
            var prev_pass_data = passes[pass - 1];
            var prev_pass_screen_data = prev_pass_data.screens[screen];
            unbind_fb_texture (
                ++next_texture, prev_pass_screen_data.framebuffer
            );

            for (int i = 0; i < pass - 1; i++) {
                var nth_pass_data = passes[i];
                var nth_pass_screen_data = nth_pass_data.screens[screen];

                unbind_fb_texture (
                    ++next_texture, nth_pass_screen_data.framebuffer
                );
            }
        }

        GLUtils.set_active_texture (0);

        if (!pass_data.is_final)
            pass_screen_data.framebuffer.unbind (GL_DRAW_FRAMEBUFFER);
    }

    public void end_frame (uint frames_elapsed) requires (realized) {
        for (int i = 0; i < passes.length; i++) {
            var desc = description.get_passes ()[i];
            var data = passes[i];

            data.phase = (data.phase + (int) frames_elapsed) % desc.modulo;
        }
    }

    public void set_mvp (int screen, Graphene.Matrix? mvp) requires (realized) {
        screens[screen].mvp = mvp;
    }

    public void set_opacity (int screen, double opacity) requires (realized) {
        screens[screen].opacity = opacity;
    }

    public void set_screen_framebuffers (
        int screen,
        GLFramebuffer framebuffer,
        GLFramebuffer[] history
    ) requires (realized) {
        screens[screen].framebuffer = framebuffer;
        screens[screen].history = history;
    }
}
