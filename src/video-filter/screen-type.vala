// This file is part of Highscore. License: GPL-3.0-or-later

public enum Highscore.ScreenType {
    UNKNOWN,

    // NTSC
    NTSC,
    NTSC_ATARI,
    NTSC_NES,
    NTSC_SUPER_NES,
    NTSC_SEGA_8BIT,
    NTSC_MEGA_DRIVE,
    NTSC_SEGA_SATURN,

    // PAL
    PAL,

    // LCD (monochrome)
    GAME_BOY_DMG,
    GAME_BOY_POCKET,
    GAME_BOY_LIGHT,
    NEO_GEO_POCKET,
    WONDERSWAN,

    // LCD (color)
    LCD_RGB,
    LCD_BGR,

    GAME_BOY_COLOR,
    GAME_BOY_ADVANCE,
    GAME_BOY_ADVANCE_BACKLIT,
    NEO_GEO_POCKET_COLOR,
    NINTENDO_DS;

    private inline string wrap_ntsc (string? name, bool enable_crt) {
        string ntsc;

        if (name != null)
            ntsc = @"ntsc-$name";
        else
            ntsc = "ntsc";

        if (enable_crt)
            return @"interlace | $ntsc | crt-cyclon-ntsc";
        else
            return @"interlace | $ntsc | interlace-finalize | aann";
    }

    private inline string wrap_pal (bool enable_crt) {
        if (enable_crt)
            return "interlace | pal | crt-cyclon-pal";
        else
            return "interlace | pal | interlace-finalize | aann";
    }

    public string get_filter (bool enable_crt) {
        switch (this) {
            case NTSC:
                return wrap_ntsc (null, enable_crt);
            case NTSC_ATARI:
                return wrap_ntsc ("atari", enable_crt);
            case NTSC_NES:
                return wrap_ntsc ("nes", enable_crt);
            case NTSC_SUPER_NES:
                return wrap_ntsc ("super-nes", enable_crt);
            case NTSC_SEGA_8BIT:
                return wrap_ntsc ("sega-8bit", enable_crt);
            case NTSC_MEGA_DRIVE:
                return wrap_ntsc ("mega-drive", enable_crt);
            case NTSC_SEGA_SATURN:
                return wrap_ntsc ("sega-saturn", enable_crt);

            case PAL:
                return wrap_pal (enable_crt);

            case GAME_BOY_DMG:
                return "lcd-game-boy-dmg";
            case GAME_BOY_POCKET:
                return "lcd-game-boy-pocket";
            case GAME_BOY_LIGHT:
                return "lcd-game-boy-light";
            case NEO_GEO_POCKET:
                return "lcd-neo-geo-pocket";
            case WONDERSWAN:
                return "lcd-wonderswan";

            case LCD_RGB:
                return "lcd-rgb";
            case LCD_BGR:
                return "lcd-bgr";

            case GAME_BOY_COLOR:
                return "lcd-game-boy-color";
            case GAME_BOY_ADVANCE:
                return "lcd-game-boy-advance";
            case GAME_BOY_ADVANCE_BACKLIT:
                return "lcd-game-boy-advance-backlit";
            case NEO_GEO_POCKET_COLOR:
                return "lcd-neo-geo-pocket-color";
            case NINTENDO_DS:
                return "lcd-nintendo-ds";
            case UNKNOWN:
                return "aann";
            default:
                assert_not_reached ();
        }
    }

    // Adapted from CRT-Cyclon shader
    // See warp() in data/filters/shared/crt-cyclon/crt-cyclon.fs
    private void warp_crt (
        float width,
        float height,
        float aspect_ratio,
        float x,
        float y,
        out float out_x,
        out float out_y
    ) {
        const float TV_ASPECT_RATIO = 4.0f / 3.0f;
        const float WARP_X = 0;
        const float WARP_Y = 0.02f;

        Graphene.Vec2 coords = {}, vec = {}, letterboxing = {};

        coords.init (x, y);

        if (aspect_ratio > TV_ASPECT_RATIO)
            letterboxing.init (1, TV_ASPECT_RATIO / aspect_ratio);
        else
            letterboxing.init (aspect_ratio / TV_ASPECT_RATIO, 1);

        coords = coords.scale (2);
        coords = coords.subtract (Graphene.Vec2.one ());
        coords = coords.multiply (letterboxing);

        vec.init (
            1 + coords.get_y () * coords.get_y () * WARP_X,
            1 + coords.get_x () * coords.get_x () * WARP_Y
        );
        coords = coords.multiply (vec);

        coords = coords.divide (letterboxing);
        coords = coords.add (Graphene.Vec2.one ());
        coords = coords.scale (0.5f);

        out_x = coords.get_x ();
        out_y = coords.get_y ();
    }

    public bool warp_pointer (
        bool enable_crt,
        float width,
        float height,
        float aspect_ratio,
        float x,
        float y,
        out float out_x,
        out float out_y
    ) {
        switch (this) {
            case NTSC:
            case NTSC_ATARI:
            case NTSC_NES:
            case NTSC_SUPER_NES:
            case NTSC_SEGA_8BIT:
            case NTSC_MEGA_DRIVE:
            case NTSC_SEGA_SATURN:
            case PAL:
                if (enable_crt) {
                    warp_crt (
                        width, height, aspect_ratio,
                        x, y, out out_x, out out_y
                    );
                    return true;
                } else {
                    out_x = -1;
                    out_y = -1;
                    return false;
                }

            default:
                out_x = -1;
                out_y = -1;
                return false;
        }
    }
}
