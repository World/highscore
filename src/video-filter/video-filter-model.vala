// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.VideoFilterModel : Object, ListModel, Gtk.SectionModel {
    private const uint N_BUILTIN = 5;

    public class Item : Object {
        public string id { get; set; }
        public string name { get; set; }

        public Item (string id, string name) {
            Object (id: id, name: name);
        }
    }

    private ListStore data;
    private int n_internal_filters;

    public bool show_default { get; construct; default = false; }

    public VideoFilterModel (bool show_default) {
        Object (show_default: show_default);
    }

    construct {
        var register = VideoFilterRegister.get_instance ();

        data = new ListStore (typeof (Item));

        if (show_default)
            data.append (new Item ("", _("Use Default")));

        data.append (new Item ("antialiased", _("Anti-Aliased")));
        data.append (new Item ("sharp",       _("Sharp")));
        data.append (new Item ("blurry",      _("Blurry")));
        data.append (new Item ("smooth",      _("Smooth")));
        data.append (new Item ("accurate",    _("Accurate")));

        if (show_default)
            assert (data.get_n_items () == N_BUILTIN + 1);
        else
            assert (data.get_n_items () == N_BUILTIN);

        if (FILTERS in Debug.get_flags ()) {
            var internal_filters = register.get_internal_filters ();
            foreach (var filter in internal_filters)
                data.append (new Item (filter.name, filter.display_name));

            n_internal_filters = internal_filters.length;
        } else {
            n_internal_filters = 0;
        }

        var user_filters = register.get_user_filters ();
        foreach (var filter in user_filters)
            data.append (new Item (filter.name, filter.display_name));
    }

    public Type get_item_type () {
        return typeof (Item);
    }

    public uint get_n_items () {
        return data.get_n_items ();
    }

    public Object? get_item (uint i) {
        return data.get_item (i);
    }

    public void get_section (uint position, out uint start, out uint end) {
        if (show_default && position == 0) {
            start = 0;
            end = 1;
            return;
        }

        int builtin_start = show_default ? 1 : 0;

        if (position < N_BUILTIN + builtin_start) {
            start = builtin_start;
            end = builtin_start + N_BUILTIN;
            return;
        }

        if (position < n_internal_filters + N_BUILTIN + builtin_start) {
            start = builtin_start + N_BUILTIN;
            end = builtin_start + n_internal_filters + N_BUILTIN;
            return;
        }

        start = builtin_start + n_internal_filters + N_BUILTIN;
        end = get_n_items ();
    }

    public uint find_by_id (string id) {
        uint n = get_n_items ();
        for (uint i = 0; i < n; i++) {
            var item = get_item (i) as Item;

            if (id == item.id)
                return i;
        }

        return Gtk.INVALID_LIST_POSITION;
    }
}
