// This file is part of Highscore. License: GPL-3.0-or-later

[Compact (opaque = true)]
public class Highscore.VideoFilterUniform {
    public enum ValueType {
        BOOLEAN,
        INT,
        FLOAT,
        VEC2,
        VEC3,
        VEC4;
    }

    private ValueType value_type;

    private bool bool_value;
    private int int_value;
    private float float_value;
    private Graphene.Vec2 vec2_value;
    private Graphene.Vec3 vec3_value;
    private Graphene.Vec4 vec4_value;

    public VideoFilterUniform.bool (bool value) {
        value_type = BOOLEAN;
        bool_value = value;
    }

    public VideoFilterUniform.int (int value) {
        value_type = INT;
        int_value = value;
    }

    public VideoFilterUniform.float (float value) {
        value_type = FLOAT;
        float_value = value;
    }

    public VideoFilterUniform.vec2 (Graphene.Vec2 value) {
        value_type = VEC2;
        vec2_value = value;
    }

    public VideoFilterUniform.vec3 (Graphene.Vec3 value) {
        value_type = VEC3;
        vec3_value = value;
    }

    public VideoFilterUniform.vec4 (Graphene.Vec4 value) {
        value_type = VEC4;
        vec4_value = value;
    }

    public ValueType get_type () {
        return value_type;
    }

    public bool get_bool () requires (value_type == BOOLEAN) {
        return bool_value;
    }

    public int get_int () requires (value_type == INT) {
        return int_value;
    }

    public float get_float () requires (value_type == FLOAT) {
        return float_value;
    }

    public Graphene.Vec2 get_vec2 () requires (value_type == VEC2) {
        return vec2_value;
    }

    public Graphene.Vec3 get_vec3 () requires (value_type == VEC3) {
        return vec3_value;
    }

    public Graphene.Vec4 get_vec4 () requires (value_type == VEC4) {
        return vec4_value;
    }
}
