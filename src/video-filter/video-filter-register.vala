// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.VideoFilterRegister : Object {
    private static VideoFilterRegister instance;
    private HashTable<string, VideoFilterDescription> filters;

    private VideoFilterRegister () {
        filters = new HashTable<string, VideoFilterDescription> (str_hash, str_equal);

        try {
            var system_dir = File.new_for_path (Config.FILTERS_DIR);
            var user_dir = File.new_build_filename (
                Environment.get_user_data_dir (), "highscore", "filters"
            );

            assert (system_dir.query_exists ());

            try {
                user_dir.make_directory_with_parents ();
            } catch (IOError.EXISTS e) {
            }

            enumerate_dir (system_dir);
            enumerate_dir (user_dir);
        } catch (Error e) {
            critical ("%s", e.message);
        }

        assert (get_filter ("nearest") != null);
        assert (get_filter ("aann")    != null);
        assert (get_filter ("bicubic") != null);
        assert (get_filter ("xbrz")    != null);
    }

    private void enumerate_dir (File dir) throws Error {
        var enumerator = dir.enumerate_children (
            FileAttribute.STANDARD_NAME + "," + FileAttribute.STANDARD_TYPE, 0
        );

        FileInfo info;
        while ((info = enumerator.next_file ()) != null) {
            var name = info.get_name ();

            if (info.get_file_type () == FileType.DIRECTORY) {
                enumerate_dir (dir.get_child (name));

                continue;
            }

            if (!name.has_suffix (".filter"))
                continue;

            // Remove the ".filter" suffix
            name = name.substring (0, name.length - 7);

            if (name in filters) {
                critical ("Duplicate filter: %s", name);
                return;
            }

            try {
                filters[name] = new VideoFilterDescription (dir, name);
            } catch (Error e) {
                critical (
                    "Error when parsing '%s' filter: %s", name, e.message
                );
            }
        }
    }

    public static VideoFilterRegister get_instance () {
        if (instance == null)
            instance = new VideoFilterRegister ();

        return instance;
    }

    public VideoFilterDescription? get_filter (string name) {
        var parts = name.split ("|");

        if (parts.length == 1)
            return filters[name.strip ()];

        VideoFilterDescription[] children = {};
        foreach (var part in parts) {
            var child = get_filter (part);
            if (child == null)
                return null;

            children += child;
        }

        try {
            return new VideoFilterDescription.concatenate (children);
        } catch (Error e) {
            critical ("Failed to combine filters '%s': %s", name, e.message);
            return null;
        }
    }

    public VideoFilterDescription[] get_internal_filters () {
        var values = filters.get_values ();
        var result = new Gee.ArrayList<VideoFilterDescription> ();

        foreach (var filter in values) {
            if (filter.is_internal)
                result.add (filter);
        }

        result.sort (VideoFilterDescription.compare);

        return result.to_array ();
    }

    public VideoFilterDescription[] get_unique_internal_filters () {
        var all_filters = get_internal_filters ();
        VideoFilterDescription[] ret = {};

        var names = new GenericSet<string> (str_hash, str_equal);

        foreach (var filter in all_filters) {
            var name = filter.internal_name;

            if (name != null) {
                if (name in names)
                    continue;

                names.add (name);
            }

            ret += filter;
        }

        return ret;
    }

    public VideoFilterDescription[] get_user_filters () {
        var values = filters.get_values ();
        var result = new Gee.ArrayList<VideoFilterDescription> ();

        foreach (var filter in values) {
            if (!filter.is_internal)
                result.add (filter);
        }

        result.sort (VideoFilterDescription.compare);

        return result.to_array ();
    }
}
