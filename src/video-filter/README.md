# Description file example:

```ini
# An example filter description
[Metadata]

# Filter name
Name=Example

# Set to true if this shader is internal and shouldn't be shown in the UI, other than about dialog
# Custom filters should not use this
# Internal=false

# An internal name, shown in about dialog
# If multiple filters have the same name, only one will be shown i
# InternalName=Example

# List of authors, separated with semicolons. If not present, the information will not be shown
Authors=Jane Doe;

# Copyright info, list of lines separated with semicolons. If not present, the information will not be shown
Copyright=Copyright (C) 2024 Jane Doe;

# SPDX license identifier. Supported licenses:
#   GPL-2.0-only, GPL-3.0-only, LGPL-3.0-only, LGPL-2.1-only, AGPL-3.0-only,
#   GPL-2.0-or-later, GPL-3.0-or-later, LGPL-2.1-or-later, LGPL-3.0-or-later, AGPL-3.0-or-later,
#   0BSD, BSD-2-Clause, BSD-3-Clause, MIT, Artistic-2.0, Apache-2.0 MPL-2.0
# For other licences, or multi-licensing, use LicencePath instead
License=GPL-3.0-or-later

# Custom license path. Use if the license isn't in the list above
# LicencePath=example.license

# Link to the original shader(s) if they are ported from elsewhere, separated with semicolons
Sources=https://example.org;

# Number of history frames to pass to the shaders. If not specified, assume 0 frames
# HistoryFrames=2

# Number of shader passes. Add a [Pass N] group for each of them, see [Pass 1] below for an example
# If not present, assume 1 pass
# Passes=1


# Data for the first pass
[Pass 1]

# Name of the pass - allows the pass output to be referenced in future passes by name
# Names must be unique between different passes
# Name=Example

# Width of the output: "fill" to fill the entire width, a percentage of the
# source width or an absolute value ending in px. Examples: fill, 200%, 1024px
# Width=fill

# Height of the output: "fill" to fill the entire height, a percentage of the
# source height or an absolute value ending in px. Examples: fill, 200%, 1024px
# Height=fill

# Filter of the source texture. Values: "nearest" (pixelated) or "linear" (blurred)
# Filter=linear

# Wrap mode of the source texture, determines how sampling it outside the [0-1] range is handled
# Values: "border" (use a border color), "edge" (clamp texture coordinates), repeat" (repeat the texture)
# Wrap=border

# Format of the output texture. Values: "rgba8", "rgb10a2", "rgba12", "rgba16", "rgba16f", "rgba32f"
# Only change the format if you need higher quality than the default format
# Format=rgba8

# The number of frames before phase is reset to 0
# Modulo=300

# Vertex shader. If omitted, uses a minimal shader
# Vertex=example.vs

# Fragment shader. If omitted, uses a minimal shader
# Fragment=example.fs

# Keys that start from "Uniform" define uniforms
# Supported types: bool, int, float, vec2, vec3, vec4
# bool values can be: true, false
# float values must have a point, otherwise it will be interpreted as int
# vector types must have float components
# Uniform BoolValue = true
# Uniform IntValue = 10
# Uniform FloatValue = 0.1
# Uniform Vec2Value = vec2(1.0, 2.0)
# Uniform Vec3Value = vec3(-1.0, 0.0, 1.0)
# Uniform Vec4Value = vec4(1.0, 1.0, 1.0, 1.0)
```

# Uniforms

These uniforms are always available both from the vertex and fragment shader.

```glsl
// All of the following vectors contain the following values:
// width, height, 1.0 / width, 1.0 / height

// Original texture
uniform sampler2D u_original;
uniform vec4 u_originalSize;

// Source texture for this pass - same as original texture for the first pass,
// output of the previous pass for later passes
uniform sampler2D u_source;
uniform vec4 u_sourceSize;

// Output from the previous passes can be accessed by their name. For example,
// if a pass is called Example, the texture is u_passOutputExample and the size
// is u_passOutputExampleSize.
uniform sampler2D u_passOutputNAME;
uniform vec4 u_passOutputNAMESize;

// History of the original texture. u_history1 corresponds to the previous frame,
// u_history2 to the one before that and so on. The number of available frames
// is determined by the HistoryFrames value in the metadata
uniform sampler2D u_historyN;
uniform vec4 u_historyNSize;

uniform vec4 u_outputSize; // Output size for this pass
uniform vec4 u_viewportSize; // Final viewport size after the last pass

uniform float u_aspectRatio; // Aspect ratio of the game screen
uniform int u_phase; // A number that gets incremented every frame
```

# Vertex shader

The position and `v_texCoord` are calculated automatically, so filters only need
to calculate custom parameters to pass to the fragment shader.

## Additional uniforms

```glsl
uniform mat4 u_mvp; // The transform matrix for this screen
```

## Inputs

```glsl
in vec2 position; // Vertex position
in vec2 texCoord; // Texture coordinates in [0-1] range
```

## Outputs

```glsl
out vec2 v_texCoord; // Texture coordinates in [0-1] range
```

## Minimal implementation

If a vertex shader is not specified, the following will be used:

```glsl
void hs_main() {}
```

## Overriding default values

The default value are calculated as follows. Filters are allowed to change them
from `hs_main()`, though it's rarely needed.

```glsl
  v_texCoord = texCoord;
  gl_Position = u_mvp * vec4(position, 0, 1);
```

# Fragment shader

## Inputs:

```glsl
in vec2 v_texCoord; // Texture coordinates in [0-1] range
```

## Minimal implementation

If a vertex shader is not specified, the following will be used:

```glsl
vec4 hs_main() {
  return texture(u_source, v_texCoord);
}
```

Fragment shader returns the resulting color directly from `hs_main()`.
