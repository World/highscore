// This file is part of Highscore. License: GPL-3.0-or-later

int main (string[] args) {
    Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.GNOMELOCALEDIR);
    Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain (Config.GETTEXT_PACKAGE);

    try {
        Mirage.initialize ();
    } catch (Error e) {
        error ("Failed to initialize libMirage");
    }

    try {
        Lfb.init (Config.APPLICATION_ID);

        if (Lfb.get_feedback_profile () != null)
            // We don't want sound effects on buttons, ever
            Lfb.set_feedback_profile ("quiet");
        else
            Lfb.uninit ();
    } catch (Error e) {
        error ("Failed to initialize libfeedback");
    }

    Highscore.Debug.parse (false);
    Highscore.GamepadType.init_types ();

    var app = new Highscore.Application ();

    return app.run (args);
}
