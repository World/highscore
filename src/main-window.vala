// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/app/drey/Highscore/main-window.ui")]
public class Highscore.MainWindow : Adw.ApplicationWindow, RunnerWindow {
    [GtkChild]
    private unowned Adw.ToastOverlay toast_overlay;
    [GtkChild]
    private unowned Adw.NavigationView nav_view;
    [GtkChild]
    private unowned LibraryPage library_page;

    private DisplayPage? display_page;

    public Runner? runner {
        get {
            if (display_page != null)
                return display_page.runner;

            return null;
        }
    }

    public MainWindow (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        var settings = new Settings ("app.drey.Highscore.state");

        settings.bind ("main-window-maximized", this, "maximized",      GET | SET | GET_NO_CHANGES);
        settings.bind ("main-window-width",     this, "default-width",  GET | SET | GET_NO_CHANGES);
        settings.bind ("main-window-height",    this, "default-height", GET | SET | GET_NO_CHANGES);

        var detector = TouchDetector.get_instance ();
        if (detector != null)
            detector.register (this);
    }

    static construct {
        add_binding_action (Gdk.Key.W, CONTROL_MASK, "window.close", null);
    }

    protected override bool close_request () {
        if (display_page != null) {
            display_page.save_and_close.begin (() => {
                display_page = null;
                close ();
            });
            return Gdk.EVENT_STOP;
        }

        return Gdk.EVENT_PROPAGATE;
    }

    [GtkCallback]
    private async void game_activated_cb (Game game) {
        var app = application as Application;
        if (app.try_focus_existing_window (game))
            return;

        if (display_page != null)
            return;

        display_page = new DisplayPage (game, false);
        display_page.add_toast.connect (add_toast);
        display_page.hidden.connect (() => {
            display_page = null;
            title = _("Highscore");
        });

        display_page.bind_property ("window-title", this, "title", SYNC_CREATE);

        nav_view.push (display_page);

        toast_overlay.dismiss_all ();

        yield display_page.start_game ();
    }

    public void show_games () {
        library_page.show_games ();
    }

    public void run_search (string terms) {
        library_page.run_search (terms);
    }

    public async void run_game (Game game) {
        yield game_activated_cb (game);
    }

    [GtkCallback]
    private void add_toast (Adw.Toast toast) {
        toast_overlay.add_toast (toast);
    }
}
