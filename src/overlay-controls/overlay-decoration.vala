// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayDecoration : Gtk.Widget {
    public OverlayDescription.DecorationData desc { get; construct; }
    public Rsvg.Handle handle { get; construct; }
    public string suffix { get; construct; }

    private Gsk.RenderNode? child_node;
    private int last_width;
    private int last_height;

    public OverlayDecoration (Rsvg.Handle handle, OverlayDescription.DecorationData desc, string suffix) {
        Object (handle: handle, desc: desc, suffix: suffix);
    }

    static construct {
        set_css_name ("overlay-decoration");
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        int w = get_width ();
        int h = get_height ();

        if (w != last_width || h != last_height)
            child_node = null;

        if (child_node == null) {
            var snapshot2 = new Gtk.Snapshot ();
            var cr = snapshot2.append_cairo ({{ 0, 0 }, { w, h }});

            try {
                handle.render_element (cr, @"#$(desc.element)$suffix", { 0, 0, w, h });
            } catch (Error e) {
                critical ("%s", e.message);
            }

            child_node = snapshot2.free_to_node ();

            last_width = w;
            last_height = h;
        }

        snapshot.append_node (child_node);
    }
}
