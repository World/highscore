// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayRegister : Object {
    private static OverlayRegister instance;

    private HashTable<string, OverlayDescription> overlays;

    private OverlayRegister () {
        overlays = new HashTable<string, OverlayDescription> (str_hash, str_equal);

        try {
            var system_dir = File.new_for_path (Config.OVERLAYS_DIR);
            var user_dir = File.new_build_filename (
                Environment.get_user_data_dir (), "highscore", "overlays"
            );

            assert (system_dir.query_exists ());

            try {
                user_dir.make_directory_with_parents ();
            } catch (IOError.EXISTS e) {
            }

            try {
                enumerate_dir (system_dir);
            } catch (Error e) {
                error ("Error in a built-in overlay: %s", e.message);
            }

            enumerate_dir (user_dir);
        } catch (Error e) {
            critical ("%s", e.message);
        }
    }

    private void enumerate_dir (File dir) throws Error {
        var enumerator = dir.enumerate_children (
            FileAttribute.STANDARD_NAME + "," + FileAttribute.STANDARD_TYPE, 0
        );

        var new_overlays = new HashTable<string, OverlayDescription> (str_hash, str_equal);

        FileInfo info;
        while ((info = enumerator.next_file ()) != null) {
            var name = info.get_name ();

            if (info.get_file_type () == FileType.DIRECTORY) {
                enumerate_dir (dir.get_child (name));

                continue;
            }

            if (!name.has_suffix (".overlay"))
                continue;

            // Remove the ".overlay" suffix
            name = name.substring (0, name.length - 8);

            if (name in new_overlays) {
                critical ("Duplicate overlay: %s", name);
                return;
            }

            try {
                var desc = new OverlayDescription (dir, name);
                overlays[name] = desc;
                new_overlays[name] = desc;
            } catch (Error e) {
                critical (
                    "Error when parsing '%s' overlay: %s", name, e.message
                );
            }
        }
    }

    public static OverlayRegister get_instance () {
        if (instance == null)
            instance = new OverlayRegister ();

        return instance;
    }

    public OverlayDescription? get_overlay (string name) {
        return overlays[name];
    }
}
