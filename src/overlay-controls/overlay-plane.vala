// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayPlane : Gtk.Widget {
    public signal void button_pressed (string element);
    public signal void button_released (string element);
    public signal void stick_moved (string element, double x, double y);

    public OverlayDescription.PlaneData desc { get; construct; }
    public Rsvg.Handle handle { get; construct; }
    public string suffix { get; construct; }

    private OverlayControl[] controls;
    private OverlayDecoration[] decorations;

    private GenericSet<Gdk.EventSequence> sequences;
    private bool active_pointer;

    private HashTable<Gdk.EventSequence, OverlayControl?> grabbed_controls;
    private OverlayControl? pointer_grabbed_control;

    private Gsk.RenderNode? child_node;
    private bool child_node_invalidated;
    private int last_width;
    private int last_height;

    public OverlayPlane (Rsvg.Handle handle, OverlayDescription.PlaneData desc, string suffix) {
        Object (handle: handle, desc: desc, suffix: suffix);
    }

    construct {
        child_node_invalidated = true;

        grabbed_controls = new HashTable<Gdk.EventSequence, OverlayControl?> (direct_hash, direct_equal);
        sequences = new GenericSet<Gdk.EventSequence> (direct_hash, direct_equal);

        decorations = {};

        foreach (var data in desc.decorations) {
            if (!handle.has_sub (@"#$(data.element)$suffix")) {
                warning ("Nonexistent decoration on plane %s: %s", desc.name, data.element);
                continue;
            }

            var decoration = new OverlayDecoration (handle, data, suffix);

            decoration.set_parent (this);
            decorations += decoration;
        }

        controls = {};

        foreach (var data in desc.controls) {
            OverlayControl control;

            if (!handle.has_sub (@"#$(data.element)$suffix")) {
                warning ("Nonexistent control on plane %s: %s", desc.name, data.element);
                continue;
            }

            switch (data.type) {
                case BUTTON:
                case TRIGGER:
                    control = create_button (data);
                    break;
                case DPAD:
                    control = create_dpad (data);
                    break;
                case STICK:
                    control = create_stick (data);
                    break;
                case ROTARY:
                case POINTER:
                default:
                    assert_not_reached ();
            }

            control.set_parent (this);
            controls += control;
        }

        if (!desc.opaque)
            add_css_class ("transparent");
    }

    private OverlayControl create_button (OverlayDescription.ControlData data) {
        var button = new OverlayButton (handle, data, suffix);

        button.button_pressed.connect (() => {
            button_pressed (data.element);
        });
        button.button_released.connect (() => {
            button_released (data.element);
        });

        return button;
    }

    private OverlayControl create_dpad (OverlayDescription.ControlData data) {
        var dpad = new OverlayDpad (handle, data, suffix);

        dpad.direction_pressed.connect (dir => {
            button_pressed (@"$(data.element):$dir");
        });
        dpad.direction_released.connect (dir => {
            button_released (@"$(data.element):$dir");
        });

        return dpad;
    }

    private OverlayControl create_stick (OverlayDescription.ControlData data) {
        var stick = new OverlayStick (handle, data, suffix);

        stick.stick_moved.connect ((x, y) => {
            stick_moved (data.element, x, y);
        });

        return stick;
    }

    static construct {
        set_css_name ("overlay-plane");
    }

    protected override void dispose () {
        foreach (var decoration in decorations)
            decoration.unparent ();

        foreach (var control in controls)
            control.unparent ();

        decorations = {};
        controls = {};

        base.dispose ();
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int minimum,
        out int natural,
        out int minimum_baseline,
        out int natural_baseline
    ) {
        minimum = natural = 0;

        foreach (var control in controls) {
            int child_min, child_nat;

            if (!control.should_layout ())
                continue;

            control.measure (
                orientation, for_size,
                out child_min, out child_nat, null, null
            );

            minimum = int.max (minimum, child_min);
            natural = int.max (natural, child_nat);
        }

        foreach (var decoration in decorations) {
            int child_min, child_nat;

            if (!decoration.should_layout ())
                continue;

            decoration.measure (
                orientation, for_size,
                out child_min, out child_nat, null, null
            );

            minimum = int.max (minimum, child_min);
            natural = int.max (natural, child_nat);
        }

        minimum_baseline = natural_baseline = -1;
    }

    private Rsvg.Rectangle? get_element_logical_bounds (string element, string suffix, Rsvg.Rectangle viewport) {
        Rsvg.Rectangle? ink_rect = null;
        var id = @"#$element$suffix";

        try {
            handle.get_geometry_for_layer (id, viewport, null, out ink_rect);
        } catch (Error e) {
            return null;
        }

        return ink_rect;
    }

    private inline void allocate_child (
        Gtk.Widget child,
        string element,
        string suffix,
        int width,
        int height,
        Rsvg.Rectangle viewport,
        Rsvg.Rectangle? plane_rect
    ) {
        if (!child.should_layout ())
            return;

        var rect = get_element_logical_bounds (element, suffix, viewport);

        rect.x -= plane_rect.x;
        rect.y -= plane_rect.y;

        rect.x *= width / plane_rect.width;
        rect.y *= height / plane_rect.height;
        rect.width *= width / plane_rect.width;
        rect.height *= height / plane_rect.height;

        var transform = new Gsk.Transform ();
        transform = transform.translate ({
            (float) Math.round (rect.x), (float) Math.round (rect.y)
        });

        int w = (int) Math.round (rect.width);
        int h = (int) Math.round (rect.height);

        child.allocate (w, h, -1, transform);
    }

    protected override void size_allocate (int width, int height, int baseline) {
        double image_width, image_height;

        handle.get_intrinsic_size_in_pixels (out image_width, out image_height);

        Rsvg.Rectangle viewport = { 0, 0, image_width, image_height };
        var plane_rect = get_element_logical_bounds (desc.name, suffix, viewport);

        foreach (var decoration in decorations) {
            allocate_child (
                decoration, decoration.desc.element, decoration.suffix,
                width, height, viewport, plane_rect
            );
        }

        foreach (var control in controls) {
            allocate_child (
                control, control.desc.element, control.suffix,
                width, height, viewport, plane_rect
            );
        }
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        int w = get_width ();
        int h = get_height ();

        if (!desc.opaque) {
            base.snapshot (snapshot);
            return;
        }

        if (w != last_width || h != last_height) {
            child_node = null;
            child_node_invalidated = true;
        }

        if (child_node_invalidated) {
            var snapshot2 = new Gtk.Snapshot ();
            var cr = snapshot2.append_cairo ({{ 0, 0 }, { w, h }});

            try {
                handle.render_element (cr, @"#$(desc.name)$suffix", { 0, 0, w, h });
            } catch (Error e) {
                critical ("%s", e.message);
            }

            child_node = snapshot2.free_to_node ();

            last_width = w;
            last_height = h;
        }

        if (child_node != null)
            snapshot.append_node (child_node);

        var cr = snapshot.append_cairo ({{ 0, 0 }, { w, h }});

        try {
            handle.render_element (cr, @"#$(desc.name)$suffix", { 0, 0, w, h });
        } catch (Error e) {
            critical ("%s", e.message);
        }

        base.snapshot (snapshot);
    }

    private void cancel_other_controls (Gdk.EventSequence? sequence) {
        OverlayControl? grabbed_control = null;

        if (sequence == null)
            grabbed_control = pointer_grabbed_control;
        else
            grabbed_control = grabbed_controls[sequence];

        foreach (var control in controls) {
            if (control == grabbed_control)
                continue;

            if (control.is_active (sequence))
                control.drag_end (sequence);
        }
    }

    public bool drag_begin (Gdk.EventSequence? sequence, double x, double y) {
        if (sequence == null)
            assert (pointer_grabbed_control == null);
        else
            assert (!(sequence in grabbed_controls));

        bool grabbed = false;

        foreach (var control in controls) {
            if (!control.should_layout ())
                continue;

            Graphene.Point point = {};

            assert (compute_point (control, { (float) x, (float) y }, out point));

            if (point.x < 0 ||
                point.y < 0 ||
                point.x > control.get_width () ||
                point.y > control.get_height () ||
                !control.contains (point.x, point.y)) {
                continue;
            }

            if (!control.is_active (sequence)) {
                control.drag_begin (sequence, point.x, point.y);

                if (control.grab_on_press) {
                    if (sequence == null)
                        pointer_grabbed_control = control;
                    else
                        grabbed_controls[sequence] = control;

                    cancel_other_controls (sequence);

                    grabbed = true;
                    break;
                }
            }
        }

        if (sequence == null)
            active_pointer = true;
        else
            sequences.add (sequence);

        return grabbed;
    }

    public void drag_update (Gdk.EventSequence? sequence, double x, double y) {
        assert (is_active (sequence));

        OverlayControl? grabbed_control = null;

        if (sequence == null)
            grabbed_control = pointer_grabbed_control;
        else
            grabbed_control = grabbed_controls[sequence];

        if (grabbed_control != null) {
            Graphene.Point point = {};

            assert (compute_point (grabbed_control, { (float) x, (float) y }, out point));

            grabbed_control.drag_update (sequence, point.x, point.y);

            return;
        }

        foreach (var control in controls) {
            if (!control.should_layout ())
                continue;

            Graphene.Point point = {};

            assert (compute_point (control, { (float) x, (float) y }, out point));

            if (point.x < 0 ||
                point.y < 0 ||
                point.x > control.get_width () ||
                point.y > control.get_height () ||
                !control.contains (point.x, point.y)) {
                if (control.is_active (sequence))
                    control.drag_end (sequence);
                continue;
            }

            if (control.is_active (sequence))
                control.drag_update (sequence, point.x, point.y);
            else if (!control.grab_on_press)
                control.drag_begin (sequence, point.x, point.y);
        }
    }

    public void drag_end (Gdk.EventSequence? sequence) {
        if (sequence == null)
            active_pointer = false;
        else
            sequences.remove (sequence);

        if (sequence == null && pointer_grabbed_control != null) {
            pointer_grabbed_control.drag_end (sequence);
            pointer_grabbed_control = null;
            return;
        }

        if (sequence in grabbed_controls) {
            grabbed_controls[sequence].drag_end (sequence);
            grabbed_controls.remove (sequence);
        }

        foreach (var control in controls) {
            if (!control.should_layout ())
                continue;

            if (control.is_active (sequence))
                control.drag_end (sequence);
        }
    }

    public bool is_active (Gdk.EventSequence? sequence) {
        if (sequence == null)
            return active_pointer;

        return sequence in sequences;
    }
}
