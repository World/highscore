// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayView : Adw.Bin {
    public signal void button_pressed (string element);
    public signal void button_released (string element);
    public signal void stick_moved (string element, double x, double y);
    public signal void clear_touch_controls ();

    public OverlayDescription desc { get; construct; }

    public double top_inset { get; private set; }
    public double bottom_inset { get; private set; }
    public double left_inset { get; private set; }
    public double right_inset { get; private set; }

    private Adw.BreakpointBin bin;
    private OverlayLayoutView current_view;

    private HashTable<Adw.Breakpoint, OverlayDescription.LayoutData?> layouts;

    public OverlayView (OverlayDescription desc) {
        Object (desc: desc);
    }

    construct {
        can_focus = false;
        can_target = false;

        bin = new Adw.BreakpointBin () {
            width_request = 1,
            height_request = 1,
        };

        child = bin;

        layouts = new HashTable<Adw.Breakpoint, OverlayDescription.LayoutData?> (direct_hash, direct_equal);

        foreach (var layout in desc.get_layouts ()) {
            if (layout.name == desc.default_layout.name)
                continue;

            var breakpoint = new Adw.Breakpoint (layout.condition);
            layouts[breakpoint] = layout;
            bin.add_breakpoint (breakpoint);
        }

        bin.notify["current-breakpoint"].connect (rebuild);

        rebuild ();
    }

    private void rebuild () {
        OverlayDescription.LayoutData layout;

        if (bin.current_breakpoint == null)
            layout = desc.default_layout;
        else
            layout = layouts[bin.current_breakpoint];

        if (current_view != null)
            clear_touch_controls ();

        if (layout.file == null) {
            current_view = null;
            bin.child = new Adw.Bin ();
        } else {
            current_view = new OverlayLayoutView (layout);
            bin.child = current_view;
        }

        if (current_view != null) {
            current_view.button_pressed.connect (element => button_pressed (element));
            current_view.button_released.connect (element => button_released (element));
            current_view.stick_moved.connect ((element, x, y) => stick_moved (element, x, y));

            current_view.notify["top-inset"].connect (() => {
                top_inset = current_view.top_inset;
            });
            current_view.notify["bottom-inset"].connect (() => {
                bottom_inset = current_view.bottom_inset;
            });
            current_view.notify["left-inset"].connect (() => {
                left_inset = current_view.left_inset;
            });
            current_view.notify["right-inset"].connect (() => {
                right_inset = current_view.right_inset;
            });
        } else {
            top_inset = bottom_inset = left_inset = right_inset = 0;
        }
    }

    public void drag_begin (Gdk.EventSequence? sequence, double x, double y) {
        current_view.drag_begin (sequence, x, y);
    }

    public void drag_update (Gdk.EventSequence? sequence, double x, double y) {
        current_view.drag_update (sequence, x, y);
    }

    public void drag_end (Gdk.EventSequence? sequence) {
        current_view.drag_end (sequence);
    }
}
