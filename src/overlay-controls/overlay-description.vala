// This file is part of Highscore. License: GPL-3.0-or-later

public errordomain Highscore.OverlayError {
    INVALID_OVERLAY,
    MISSING_LAYOUTS,
    MISSING_PLANES,
    INVALID_CONDITION,
    INVALID_PATH,
    INVALID_SCALE,
    INVALID_INSET,
    INVALID_CONTROL_TYPE,
    INVALID_HITBOX,
    DUPLICATE_LAYOUT_NAME,
}

public class Highscore.OverlayDescription : Object {
    public File dir { get; construct; }
    public string name { get; construct; }

    public enum HitboxType {
        RECT,
        ROUND,
    }

    public struct LayoutData {
        string name;
        Adw.BreakpointCondition? condition;
        File file;
        PlaneData[] planes;

        PlaneData? top_inset_plane;
        PlaneData? bottom_inset_plane;
        PlaneData? left_inset_plane;
        PlaneData? right_inset_plane;

        string suffix;
    }

    public struct PlaneData {
        string name;

        double xalign;
        double yalign;

        double min_scale;
        double max_scale;

        bool opaque;
        bool background;
        ControlData[] controls;
        DecorationData[] decorations;
    }

    public struct ControlData {
        string element;
        Control.ControlType type;
        HitboxType hitbox;
        string? appearance;
        string id;
    }

    public struct DecorationData {
        string element;
    }

    private LayoutData[] layouts;
    private bool has_default_layout;

    public LayoutData default_layout { get; private set; }

    public OverlayDescription (File dir, string name) throws OverlayError {
        Object (dir: dir, name: name);

        try {
            var path = Path.build_filename (dir.peek_path (), @"$name.overlay");
            var keyfile = new KeyFile ();
            keyfile.load_from_file (path, KeyFileFlags.NONE);

            string[] layout_names = {};
            layouts = {};

            if (keyfile.has_key ("Metadata", "Layouts")) {
                layout_names = keyfile.get_string_list ("Metadata", "Layouts");
            } else {
                throw new OverlayError.MISSING_LAYOUTS (
                    "Layouts must be specified"
                );
            }

            var layout_names_set = new GenericSet<string> (str_hash, str_equal);

            foreach (var layout_name in layout_names) {
                var layout = try_parse_layout (keyfile, layout_name);

                if (layout.condition == null) {
                    if (has_default_layout) {
                        throw new OverlayError.INVALID_CONDITION (
                            "Non-default layouts must have a condition"
                        );
                    }

                    default_layout = layout;
                    has_default_layout = true;
                }

                if (layout.name != null) {
                    if (layout.name in layout_names_set) {
                        throw new OverlayError.DUPLICATE_LAYOUT_NAME (
                            "Duplicate layout name: %s", layout.name
                        );
                    }

                    layout_names_set.add (layout.name);
                }

                layouts += layout;
            }

            if (!has_default_layout)
                throw new OverlayError.INVALID_CONDITION ("Missing default layout");
        } catch (KeyFileError e) {
            throw new OverlayError.INVALID_OVERLAY ("Invalid overlay: %s", e.message);
        } catch (FileError e) {
            throw new OverlayError.INVALID_OVERLAY ("Invalid overlay: %s", e.message);
        }
    }

    private LayoutData try_parse_layout (KeyFile keyfile, string name) throws KeyFileError, OverlayError {
        LayoutData ret = {};
        var group_name = @"Layout $name";

        ret.name = name;

        if (!keyfile.has_group (group_name)) {
            ret.condition = null;
            ret.planes = {};
            ret.file = null;
            ret.suffix = "";

            return ret;
        }

        if (keyfile.has_key (group_name, "Condition")) {
            var condition_str = keyfile.get_string (group_name, "Condition");
            ret.condition = Adw.BreakpointCondition.parse (condition_str);
        } else {
            ret.condition = null;
        }

        if (keyfile.has_key (group_name, "Suffix"))
            ret.suffix = keyfile.get_string (group_name, "Suffix");
        else
            ret.suffix = "";

        string[] plane_names = {};
        PlaneData[] planes = {};

        string? inset_plane_names[4];

        if (keyfile.has_key (group_name, "TopInsetPlane"))
            inset_plane_names[InputDirection.UP] = keyfile.get_string (group_name, "TopInsetPlane");
        else
            inset_plane_names[InputDirection.UP] = null;

        if (keyfile.has_key (group_name, "BottomInsetPlane"))
            inset_plane_names[InputDirection.DOWN] = keyfile.get_string (group_name, "BottomInsetPlane");
        else
            inset_plane_names[InputDirection.DOWN] = null;

        if (keyfile.has_key (group_name, "LeftInsetPlane"))
            inset_plane_names[InputDirection.LEFT] = keyfile.get_string (group_name, "LeftInsetPlane");
        else
            inset_plane_names[InputDirection.LEFT] = null;

        if (keyfile.has_key (group_name, "RightInsetPlane"))
            inset_plane_names[InputDirection.RIGHT] = keyfile.get_string (group_name, "RightInsetPlane");
        else
            inset_plane_names[InputDirection.RIGHT] = null;

        if (keyfile.has_key (group_name, "Planes")) {
            plane_names = keyfile.get_string_list (group_name, "Planes");
        } else {
            plane_names = {};
        }

        foreach (var plane_name in plane_names) {
            var data = try_parse_plane (keyfile, name, plane_name);

            if (data.name == inset_plane_names[InputDirection.UP])
                ret.top_inset_plane = data;
            if (data.name == inset_plane_names[InputDirection.DOWN])
                ret.bottom_inset_plane = data;
            if (data.name == inset_plane_names[InputDirection.LEFT])
                ret.left_inset_plane = data;
            if (data.name == inset_plane_names[InputDirection.RIGHT])
                ret.right_inset_plane = data;

            planes += data;
        }

        ret.planes = planes;

        if (keyfile.has_key (group_name, "Path")) {
            var layout_path = keyfile.get_string (group_name, "Path");

            try {
                ret.file = lookup_file (layout_path);

                if (!ret.file.query_exists ()) {
                    throw new OverlayError.INVALID_PATH (
                        "File %s doesn't exist", layout_path
                    );
                }
            } catch (Error e) {
                throw new OverlayError.INVALID_PATH (
                    "Failed to get layout path: %s", e.message
                );
            }
        } else if (planes.length > 0) {
            throw new OverlayError.INVALID_PATH (
                "Path is not specified"
            );
        } else {
            ret.file = null;
        }

        return ret;
    }

    private PlaneData try_parse_plane (
        KeyFile keyfile,
        string layout_name,
        string name
    ) throws KeyFileError, OverlayError {
        PlaneData ret = {};
        var group_name = @"Plane $layout_name:$name";

        if (!keyfile.has_group (group_name))
            group_name = @"Plane $name";

        ret.name = name;

        if (keyfile.has_key (group_name, "XAlign"))
            ret.xalign = keyfile.get_double (group_name, "XAlign").clamp (0, 1);
        else
            ret.xalign = 0.5;

        if (keyfile.has_key (group_name, "YAlign"))
            ret.yalign = keyfile.get_double (group_name, "YAlign").clamp (0, 1);
        else
            ret.yalign = 0.5;

        if (keyfile.has_key (group_name, "MinScale")) {
            ret.min_scale = keyfile.get_double (group_name, "MinScale");

            if (ret.min_scale <= 0.0)
                throw new OverlayError.INVALID_SCALE ("Minimum scale must be greater than 0");
        } else {
            ret.min_scale = 1.0;
        }

        if (keyfile.has_key (group_name, "MaxScale")) {
            ret.max_scale = keyfile.get_double (group_name, "MaxScale");

            if (ret.max_scale <= 0.0)
                throw new OverlayError.INVALID_SCALE ("Maximum scale must be greater than 0");
        } else {
            ret.max_scale = 2.0;
        }

        if (keyfile.has_key (group_name, "Opaque"))
            ret.opaque = keyfile.get_boolean (group_name, "Opaque");
        else
            ret.opaque = false;

        string[] control_names;
        ControlData[] controls = {};

        if (keyfile.has_key (group_name, "Controls"))
            control_names = keyfile.get_string_list (group_name, "Controls");
        else
            control_names = {};

        foreach (var control_name in control_names) {
            var data = try_parse_control (keyfile, name, control_name);

            controls += data;
        }

        ret.controls = controls;

        string[] decoration_names;
        DecorationData[] decorations = {};

        if (keyfile.has_key (group_name, "Decorations"))
            decoration_names = keyfile.get_string_list (group_name, "Decorations");
        else
            decoration_names = {};

        foreach (var decoration_name in decoration_names) {
            DecorationData data = {};

            data.element = decoration_name;

            decorations += data;
        }

        ret.decorations = decorations;

        return ret;
    }

    private ControlData try_parse_control (
        KeyFile keyfile,
        string plane_name,
        string control_name
    ) throws KeyFileError, OverlayError {
        ControlData ret = {};

        ret.element = control_name;

        var control_group_name = @"Control $plane_name:$control_name";

        if (!keyfile.has_group (control_group_name))
            control_group_name = @"Control $control_name";

        bool has_type = false, has_id = false;

        if (keyfile.has_group (control_group_name)) {
            if (keyfile.has_key (control_group_name, "Type")) {
                var type_str = keyfile.get_string (control_group_name, "Type").strip ();
                var type = Control.ControlType.from_string (type_str);

                if (type == null || type == POINTER) {
                    throw new OverlayError.INVALID_CONTROL_TYPE (
                        "Invalid control type: %s", type_str
                    );
                }

                ret.type = type;
                has_type = true;
            }

            if (keyfile.has_key (control_group_name, "Hitbox")) {
                var hitbox = keyfile.get_string (control_group_name, "Hitbox").strip ();

                if (hitbox == "rect")
                    ret.hitbox = RECT;
                else if (hitbox == "round")
                    ret.hitbox = ROUND;
                else
                    throw new OverlayError.INVALID_HITBOX ("Invalid hitbox: %s", hitbox);
            } else {
                ret.hitbox = RECT;
            }

            if (keyfile.has_key (control_group_name, "Appearance"))
                ret.appearance = keyfile.get_string (control_group_name, "Appearance");
            else
                ret.appearance = null;

            if (keyfile.has_key (control_group_name, "ID")) {
                ret.id = keyfile.get_string (control_group_name, "ID");
                has_id = true;
            }
        }

        if (keyfile.has_group (control_group_name)) {
            if (keyfile.has_key (control_group_name, "Type")) {
                var type_str = keyfile.get_string (control_group_name, "Type").strip ();
                var type = Control.ControlType.from_string (type_str);

                if (type == null || type == POINTER) {
                    throw new OverlayError.INVALID_CONTROL_TYPE (
                        "Invalid control type: %s", type_str
                    );
                }

                ret.type = type;
                has_type = true;
            }

            if (keyfile.has_key (control_group_name, "Hitbox")) {
                var hitbox = keyfile.get_string (control_group_name, "Hitbox").strip ();

                if (hitbox == "rect")
                    ret.hitbox = RECT;
                else if (hitbox == "round")
                    ret.hitbox = ROUND;
                else
                    throw new OverlayError.INVALID_HITBOX ("Invalid hitbox: %s", hitbox);
            } else {
                ret.hitbox = RECT;
            }

            if (keyfile.has_key (control_group_name, "Appearance"))
                ret.appearance = keyfile.get_string (control_group_name, "Appearance");
            else
                ret.appearance = null;

            if (keyfile.has_key (control_group_name, "ID")) {
                ret.id = keyfile.get_string (control_group_name, "ID");
                has_id = true;
            }
        }

        if (!has_id)
            ret.id = ret.element;

        if (!has_type)
            ret.type = guess_control_type (ret.id);

        return ret;
    }

    private Control.ControlType guess_control_type (string id) {
        if (id == "dpad" || id == "joystick" || id == "joypad")
            return DPAD;

        if (id == "stick" || id.has_suffix ("-stick"))
            return STICK;

        if (id == "knob" || id == "paddle")
            return ROTARY;

        return BUTTON;
    }

    private File lookup_file (string file_path) throws Error {
        return File.new_build_filename (dir.peek_path (), file_path);
    }

    public LayoutData[] get_layouts () {
        return layouts;
    }
}
