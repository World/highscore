// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayButton : OverlayControl {
    private const uint TRANSITION_MS = 100;
    private const double PRESSED_SCALE = 0.9;

    public signal void button_pressed ();
    public signal void button_released ();

    private Adw.TimedAnimation animation;

    public OverlayButton (Rsvg.Handle handle, OverlayDescription.ControlData desc, string suffix) {
        Object (handle: handle, desc: desc, suffix: suffix);
    }

    construct {
        add_css_class ("button");

        animation = new Adw.TimedAnimation (
            this, 0, 1, TRANSITION_MS,
            new Adw.CallbackAnimationTarget (queue_draw)
        ) {
            easing = EASE_OUT,
        };
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        float scale = (float) Adw.lerp (1.0, PRESSED_SCALE, animation.value);
        int w = get_width ();
        int h = get_height ();

        snapshot.translate ({ w / 2.0f, h / 2.0f });
        snapshot.scale (scale, scale);
        snapshot.translate ({ -w / 2.0f, -h / 2.0f });

        base.snapshot (snapshot);
    }

    private void animate_change (bool pressed) {
        set_state_flags (ACTIVE, pressed);

        animation.pause ();

        animation.value_from = animation.value;
        animation.value_to = pressed ? 1 : 0;
        animation.play ();

        haptic_feedback.begin (pressed);
    }

    public override void pressed (double x, double y) {
        button_pressed ();
        animate_change (true);
    }

    public override void released () {
        button_released ();
        animate_change (false);
    }
}
