// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayDpad : OverlayControl {
    private const double DEADZONE = 0.25;
    private const double CORNER_SIZE = 1f / 3f;
    private const uint TRANSITION_MS = 100;
    private const float PERSPECTIVE_MULTIPLIER = 0.5f;
    private const float TILT_ANGLE = 5;

    public signal void direction_pressed (InputDirection direction);
    public signal void direction_released (InputDirection direction);

    private Adw.TimedAnimation x_animation;
    private Adw.TimedAnimation y_animation;

    private bool active_directions[InputDirection.N_DIRECTIONS];

    public OverlayDpad (Rsvg.Handle handle, OverlayDescription.ControlData desc, string suffix) {
        Object (handle: handle, desc: desc, suffix: suffix);
    }

    construct {
        grab_on_press = true;

        add_css_class ("dpad");

        x_animation = new Adw.TimedAnimation (
            this, 0, 1, TRANSITION_MS,
            new Adw.CallbackAnimationTarget (queue_draw)
        ) {
            easing = EASE_OUT,
        };

        y_animation = new Adw.TimedAnimation (
            this, 0, 1, TRANSITION_MS,
            new Adw.CallbackAnimationTarget (queue_draw)
        ) {
            easing = EASE_OUT,
        };
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        float x = (float) x_animation.value;
        float y = (float) y_animation.value;
        int w = get_width ();
        int h = get_height ();

        if (desc.appearance == "thumbstick") {
            float distance = float.max (x.abs (), y.abs ());
            float angle = Math.atan2f (y, x);

            x = Math.cosf (angle) * distance;
            y = Math.sinf (angle) * distance;

            float size = (w + h) / 24.0f;
            snapshot.translate ({ x * size, y * size });
        } else {
            float size = (w + h) / 2.0f;

            snapshot.translate ({ w / 2.0f, h / 2.0f });
            snapshot.perspective (size * PERSPECTIVE_MULTIPLIER);
            snapshot.rotate_3d (x * TILT_ANGLE, Graphene.Vec3.y_axis ());
            snapshot.rotate_3d (-y * TILT_ANGLE, Graphene.Vec3.x_axis ());
            snapshot.translate ({ -w / 2.0f, -h / 2.0f });
        }

        base.snapshot (snapshot);
    }

    public override void moved (double x, double y) {
        bool active[InputDirection.N_DIRECTIONS];

        x = (x - 0.5) * 2;
        y = (y - 0.5) * 2;

        double distance = Math.sqrt (x * x + y * y);
        double angle = Math.atan2 (y, x);

        if (x.abs () > (1 - CORNER_SIZE * 2) && y.abs () > (1 - CORNER_SIZE * 2)) {
            active[InputDirection.UP]    = y < 0;
            active[InputDirection.DOWN]  = y > 0;
            active[InputDirection.LEFT]  = x < 0;
            active[InputDirection.RIGHT] = x > 0;
        } else if (distance > DEADZONE) {
            for (int i = 0; i < InputDirection.N_DIRECTIONS; i++)
                active[i] = AnalogUtils.is_pointing_at_direction (angle, 0, i);
        } else {
            for (int i = 0; i < 4; i++)
                active[i] = false;
        }

        notify_directions (active);
    }

    public override void released () {
        bool active[InputDirection.N_DIRECTIONS] = {};

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++)
            active[i] = false;

        notify_directions (active);
    }

    private void notify_directions (bool active[InputDirection.N_DIRECTIONS]) {
        bool x_changed = false;
        bool y_changed = false;

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            if (active_directions[i] == active[i])
                continue;

            active_directions[i] = active[i];

            InputDirection direction = (InputDirection) i;

            if (direction == LEFT || direction == RIGHT)
                x_changed = true;
            else
                y_changed = true;

            if (active[i]) {
                add_css_class (direction.to_string ());
                direction_pressed (direction);
            } else {
                remove_css_class (direction.to_string ());
                direction_released (direction);
            }
        }

        if (x_changed) {
            double x = 0;

            if (active[InputDirection.LEFT])
                x--;
            if (active[InputDirection.RIGHT])
                x++;

            x_animation.pause ();

            x_animation.value_from = x_animation.value;
            x_animation.value_to = x;
            x_animation.play ();
        }

        if (y_changed) {
            double y = 0;

            if (active[InputDirection.UP])
                y--;
            if (active[InputDirection.DOWN])
                y++;

            y_animation.pause ();

            y_animation.value_from = y_animation.value;
            y_animation.value_to = y;
            y_animation.play ();
        }

        if (x_changed || y_changed) {
            bool released = true;

            if (active[InputDirection.UP] != active[InputDirection.DOWN])
                released = false;
            if (active[InputDirection.LEFT] != active[InputDirection.RIGHT])
                released = false;

            haptic_feedback.begin (!released);
        }
    }
}
