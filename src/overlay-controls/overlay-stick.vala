// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayStick : OverlayControl {
    private const double MAX_DISTANCE = 1.0 / 4.0;
    private const double DEADZONE = 0.05;

    public signal void stick_moved (double x, double y);

    private double drag_x;
    private double drag_y;

    private double initial_x;
    private double initial_y;

    private bool was_active;

    public OverlayStick (Rsvg.Handle handle, OverlayDescription.ControlData desc, string suffix) {
        Object (handle: handle, desc: desc, suffix: suffix);
    }

    construct {
        grab_on_press = true;

        add_css_class ("stick");
    }

    private bool get_stick_coords (out double x, out double y) {
        x = (drag_x - initial_x);
        y = (drag_y - initial_y);

        double distance = Math.sqrt (x * x + y * y);

        if (distance < DEADZONE) {
            x = y = 0;
            return false;
        }

        distance = double.min (distance, MAX_DISTANCE);

        double angle = Math.atan2 (y, x);

        x = Math.cos (angle) * distance;
        y = Math.sin (angle) * distance;

        return true;
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        int w = get_width ();
        int h = get_height ();
        double scale = get_native ().get_surface ().scale;
        double x, y;

        get_stick_coords (out x, out y);

        x = Math.round (w * x * scale) / scale;
        y = Math.round (h * y * scale) / scale;

        snapshot.translate ({ (float) x, (float) y });

        base.snapshot (snapshot);
    }

    public override void pressed (double x, double y) {
        initial_x = x;
        initial_y = y;
    }

    public override void moved (double x, double y) {
        drag_x = x;
        drag_y = y;

        notify_moved ();
    }

    public override void released () {
        drag_x = drag_y = initial_x = initial_y = 0;

        notify_moved ();
    }

    private void notify_moved () {
        double x, y;

        bool active = get_stick_coords (out x, out y);

        stick_moved (x / MAX_DISTANCE, y / MAX_DISTANCE);
        queue_draw ();

        if (active != was_active) {
            was_active = active;

            haptic_feedback.begin (active);
        }
    }
}
