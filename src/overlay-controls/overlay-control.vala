// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.OverlayControl : Gtk.Widget {
    public OverlayDescription.ControlData desc { get; construct; }
    public Rsvg.Handle handle { get; construct; }
    public string suffix { get; construct; }

    public bool grab_on_press { get; set; default = false; }

    public bool feedback { get; set; }

    private GenericSet<Gdk.EventSequence> sequences;
    private bool active_pointer;

    private Gsk.RenderNode? child_node;
    private int last_width;
    private int last_height;

    private Settings settings;

    construct {
        settings = new Settings ("app.drey.Highscore");

        settings.bind ("touch-feedback", this, "feedback", GET);

        sequences = new GenericSet<Gdk.EventSequence> (direct_hash, direct_equal);

        if (desc.hitbox == ROUND)
            add_css_class ("round");
    }

    static construct {
        set_css_name ("overlay-control");
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        int w = get_width ();
        int h = get_height ();

        if (w != last_width || h != last_height)
            child_node = null;

        if (child_node == null) {
            var snapshot2 = new Gtk.Snapshot ();
            var cr = snapshot2.append_cairo ({{ 0, 0 }, { w, h }});

            try {
                handle.render_element (cr, @"#$(desc.element)$suffix", { 0, 0, w, h });
            } catch (Error e) {
                critical ("%s", e.message);
            }

            child_node = snapshot2.free_to_node ();

            last_width = w;
            last_height = h;
        }

        snapshot.append_node (child_node);
    }

    public void drag_begin (Gdk.EventSequence? sequence, double x, double y) {
        if (!is_active (sequence)) {
            pressed (x / get_width (), y / get_height ());
            moved (x / get_width (), y / get_height ());
        }

        if (sequence == null)
            active_pointer = true;
        else
            sequences.add (sequence);
    }

    public void drag_update (Gdk.EventSequence? sequence, double x, double y) {
        assert (is_active (sequence));

        moved (x / get_width (), y / get_height ());
    }

    public void drag_end (Gdk.EventSequence? sequence) {
        if (sequence == null)
            active_pointer = false;
        else
            sequences.remove (sequence);

        if (!is_active (sequence))
            released ();
    }

    public bool is_active (Gdk.EventSequence? sequence) {
        if (sequence == null)
            return active_pointer;

        return sequence in sequences;
    }

    public virtual void pressed (double x, double y) {}
    public virtual void moved (double x, double y) {}
    public virtual void released () {}

    public async void haptic_feedback (bool pressed) {
        if (!feedback || !Lfb.is_initted ())
            return;

        try {
            var event = new Lfb.Event (
                pressed ? "button-pressed" : "button-released"
            );

            yield event.trigger_feedback_async (null);
        } catch (Error e) {
            critical ("Error playing feedback: %s", e.message);
        }
    }
}
