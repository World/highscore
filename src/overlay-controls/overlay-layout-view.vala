// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.OverlayLayoutView : Gtk.Widget {
    public signal void button_pressed (string element);
    public signal void button_released (string element);
    public signal void stick_moved (string element, double x, double y);

    public int top_inset { get; private set; default = -1; }
    public int bottom_inset { get; private set; default = -1; }
    public int left_inset { get; private set; default = -1; }
    public int right_inset { get; private set; default = -1; }

    private HashTable<Gdk.EventSequence, OverlayPlane?> grabbed_planes;
    private OverlayPlane? pointer_grabbed_plane;

    public OverlayDescription.LayoutData desc { get; construct; }

    private Rsvg.Handle handle;
    private OverlayPlane[] planes;

    public OverlayLayoutView (OverlayDescription.LayoutData desc) {
        Object (desc: desc);
    }

    construct {
        grabbed_planes = new HashTable<Gdk.EventSequence, OverlayPlane?> (direct_hash, direct_equal);

        try {
            handle = new Rsvg.Handle.from_file (desc.file.peek_path ());
        } catch (Error e) {
            error ("Failed to set up overlay: %s", e.message);
        }

        foreach (var plane in desc.planes) {
            if (!handle.has_sub (@"#$(plane.name)$(desc.suffix)")) {
                warning ("Nonexistent plane: %s", plane.name);
                continue;
            }

            var plane_widget = new OverlayPlane (handle, plane, desc.suffix);

            plane_widget.button_pressed.connect (element => button_pressed (element));
            plane_widget.button_released.connect (element => button_released (element));
            plane_widget.stick_moved.connect ((element, x, y) => stick_moved (element, x, y));

            plane_widget.set_parent (this);

            planes += plane_widget;
        }
    }

    static construct {
        set_css_name ("overlay-view");
    }

    protected override void dispose () {
        foreach (var plane in planes)
            plane.unparent ();

        planes = {};

        base.dispose ();
    }

    private Rsvg.Rectangle? get_element_logical_bounds (string element, string suffix, Rsvg.Rectangle viewport) {
        Rsvg.Rectangle? ink_rect = null;
        var id = @"#$element$suffix";

        try {
            handle.get_geometry_for_layer (id, viewport, null, out ink_rect);
        } catch (Error e) {
            return null;
        }

        return ink_rect;
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int minimum,
        out int natural,
        out int minimum_baseline,
        out int natural_baseline
    ) {
        minimum = natural = 0;

        foreach (var plane in planes) {
            if (!plane.should_layout ())
                continue;

            int child_min, child_nat;

            plane.measure (
                orientation, for_size,
                out child_min, out child_nat, null, null
            );

            minimum = int.max (minimum, child_min);
            natural = int.max (natural, child_nat);
        }

        minimum_baseline = natural_baseline = -1;
    }

    protected override void size_allocate (int width, int height, int baseline) {
        double image_width, image_height;

        handle.get_intrinsic_size_in_pixels (out image_width, out image_height);

        Rsvg.Rectangle viewport = { 0, 0, image_width, image_height };

        int top = 0;
        int bottom = 0;
        int left = 0;
        int right = 0;

        foreach (var plane in planes) {
            if (!plane.should_layout ())
                continue;

            var plane_desc = plane.desc;

            var rect = get_element_logical_bounds (plane_desc.name, plane.suffix, viewport);

            double scale = double.min (
                (double) width / rect.width,
                (double) height / rect.height
            );

            scale = scale.clamp (plane_desc.min_scale, plane_desc.max_scale);

            int w = (int) Math.round (rect.width * scale);
            int h = (int) Math.round (rect.height * scale);

            int x = (int) Math.round (Adw.lerp (0, width - w, plane_desc.xalign));
            int y = (int) Math.round (Adw.lerp (0, height - h, plane_desc.yalign));

            if (desc.top_inset_plane != null && plane_desc.name == desc.top_inset_plane.name)
                top = h;
            if (desc.bottom_inset_plane != null && plane_desc.name == desc.bottom_inset_plane.name)
                bottom = h;
            if (desc.left_inset_plane != null && plane_desc.name == desc.left_inset_plane.name)
                left = w;
            if (desc.right_inset_plane != null && plane_desc.name == desc.right_inset_plane.name)
                right = w;

            var transform = new Gsk.Transform ();
            transform = transform.translate ({ x, y });

            plane.allocate (w, h, -1, transform);
        }

        top_inset = top;
        bottom_inset = bottom;
        left_inset = left;
        right_inset = right;
    }

    public void drag_begin (Gdk.EventSequence? sequence, double x, double y) {
        if (sequence == null)
            assert (pointer_grabbed_plane == null);
        else
            assert (!(sequence in grabbed_planes));

        foreach (var plane in planes) {
            if (!plane.should_layout ())
                continue;

            Graphene.Point point = {};

            assert (compute_point (plane, { (float) x, (float) y }, out point));

            if (point.x < 0 ||
                point.y < 0 ||
                point.x > plane.get_width () ||
                point.y > plane.get_height ()) {
                continue;
            }

            if (!plane.is_active (sequence)) {
                if (plane.drag_begin (sequence, point.x, point.y)) {
                    if (sequence == null)
                        pointer_grabbed_plane = plane;
                    else
                        grabbed_planes[sequence] = plane;

                    break;
                }
            }
        }
    }

    public void drag_update (Gdk.EventSequence? sequence, double x, double y) {
        OverlayPlane? grabbed_plane = null;

        if (sequence == null)
            grabbed_plane = pointer_grabbed_plane;
        else
            grabbed_plane = grabbed_planes[sequence];

        if (grabbed_plane != null) {
            Graphene.Point point = {};

            assert (compute_point (grabbed_plane, { (float) x, (float) y }, out point));

            grabbed_plane.drag_update (sequence, point.x, point.y);

            return;
        }

        foreach (var plane in planes) {
            if (!plane.should_layout ())
                continue;

            Graphene.Point point = {};

            assert (compute_point (plane, { (float) x, (float) y }, out point));

            if (point.x < 0 ||
                point.y < 0 ||
                point.x > plane.get_width () ||
                point.y > plane.get_height ()) {
                if (plane.is_active (sequence))
                    plane.drag_end (sequence);
                continue;
            }

            if (plane.is_active (sequence))
                plane.drag_update (sequence, point.x, point.y);
            else
                plane.drag_begin (sequence, point.x, point.y);
        }
    }

    public void drag_end (Gdk.EventSequence? sequence) {
        if (sequence == null && pointer_grabbed_plane != null) {
            pointer_grabbed_plane.drag_end (sequence);
            pointer_grabbed_plane = null;
            return;
        }

        if (sequence in grabbed_planes) {
            grabbed_planes[sequence].drag_end (sequence);
            grabbed_planes.remove (sequence);
        }

        foreach (var plane in planes) {
            if (!plane.should_layout ())
                continue;

            if (plane.is_active (sequence))
                plane.drag_end (sequence);
        }
    }
}
