// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.Debug {
    [Flags]
    public enum Flags {
        BACKTRACE,
        VALGRIND,
        WELCOME,
        NO_DMABUF,
        FILTERS,
        PARSERS;

        public string get_name () {
            switch (this) {
                case BACKTRACE:
                    return "backtrace";
                case VALGRIND:
                    return "valgrind";
                case WELCOME:
                    return "welcome";
                case NO_DMABUF:
                    return "no-dmabuf";
                case FILTERS:
                    return "filters";
                case PARSERS:
                    return "parsers";
                default:
                    assert_not_reached ();
            }
        }

        public string get_description () {
            switch (this) {
                case BACKTRACE:
                    return "Print backtrace when subprocess crashes";
                case VALGRIND:
                    return "Run subprocess in Valgrind";
                case WELCOME:
                    return "Always show the welcome dialog";
                case NO_DMABUF:
                    return "Disable dmabuf rendering";
                case FILTERS:
                    return "Show internal video filters in preferences";
                case PARSERS:
                    return "Print information about game parsing";
                default:
                    assert_not_reached ();
            }
        }

        public static Flags? parse_name (string name) {
            if (name == "backtrace")
                return BACKTRACE;
            if (name == "valgrind")
                return VALGRIND;
            if (name == "welcome")
                return WELCOME;
            if (name == "no-dmabuf")
                return NO_DMABUF;
            if (name == "filters")
                return FILTERS;
            if (name == "parsers")
                return PARSERS;

            return null;
        }

        public static Flags[] all () {
            return {
                BACKTRACE, VALGRIND, WELCOME, NO_DMABUF, FILTERS, PARSERS
            };
        }
    }

    private static Flags flags;

    public static Flags get_flags () {
        return flags;
    }

    public void parse (bool silent) {
        var value = Environment.get_variable ("HIGHSCORE_DEBUG");
        bool help = false;

        flags = 0;

        if (value == null)
            return;

        var names = value.split (":");

        foreach (var name in names) {
            if (name == "help") {
                help = true;
                continue;
            }

            var flag = Flags.parse_name (name);

            if (flag != null)
                flags |= flag;
            else if (!silent)
                print ("Unknown value: \"%s\". Try HIGHSCORE_DEBUG=help\n", name);
        }

        if (!silent && help) {
            print ("Supported HIGHSCORE_DEBUG values:\n");

            foreach (var flag in Flags.all ())
                print ("  %-15s %s\n", flag.get_name (), flag.get_description ());

            print ("\n");
            print ("Multiple values can be given, separated by :.\n");
        }
    }
}
