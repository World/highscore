// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore {
    public extern int memfd_create (string name);

    public Bytes create_bytes_real (uint8[] data) {
        return new Bytes (data);
    }

    [CCode (cname="highscore_create_bytes_real")]
    public extern Bytes create_bytes (void* data, size_t size);

    public UnixInputStream wrap_fd (FileDescriptorBased fd_based) {
        return new UnixInputStream (fd_based.get_fd (), false);
    }

    public int unwrap_fd (UnixInputStream stream) {
        stream.close_fd = false;

        int fd = stream.get_fd ();

        try {
            stream.close ();
        } catch (Error e) {
            critical ("Failed to close FD wrapper stream: %s", e.message);
        }

        return fd;
    }

    public inline void set_bit (ref uint32 bitfield, int offset, bool value) {
        if (value)
            bitfield |= 1 << offset;
        else
            bitfield &= ~(1 << offset);
    }
}
