// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore {
#if RUNNER_PROCESS
    public uint register_platform_server (Runner runner, Hs.Platform platform, DBusConnection connection, string path) throws Error {
        var name = platform.get_name ();
        var core = runner.core;

        switch (platform) {
            case ATARI_2600:
                var server = Object.new (typeof (Atari2600.Server), runner: runner, core: core, null) as Atari2600.Server;
                return connection.register_object<Atari2600.Server> (
                    @"$path$name", server
                );
            case ATARI_7800:
                var server = Object.new (typeof (Atari7800.Server), runner: runner, core: core, null) as Atari7800.Server;
                return connection.register_object<Atari7800.Server> (
                    @"$path$name", server
                );
            case ATARI_LYNX:
                var server = Object.new (typeof (AtariLynx.Server), runner: runner, core: core, null) as AtariLynx.Server;
                return connection.register_object<AtariLynx.Server> (
                    @"$path$name", server
                );
            case FAMICOM_DISK_SYSTEM:
                var server = Object.new (typeof (Fds.Server), runner: runner, core: core, null) as Fds.Server;
                return connection.register_object<Fds.Server> (
                    @"$path$name", server
                );
            case GAME_BOY:
                var server = Object.new (typeof (GameBoy.Server), runner: runner, core: core, null) as GameBoy.Server;
                return connection.register_object<GameBoy.Server> (
                    @"$path$name", server
                );
            case NES:
                var server = Object.new (typeof (Nes.Server), runner: runner, core: core, null) as Nes.Server;
                return connection.register_object<Nes.Server> (
                    @"$path$name", server
                );
            case MASTER_SYSTEM:
                var server = Object.new (typeof (MasterSystem.Server), runner: runner, core: core, null) as MasterSystem.Server;
                return connection.register_object<MasterSystem.Server> (
                    @"$path$name", server
                );
            case MEGA_DRIVE:
                var server = Object.new (typeof (MegaDrive.Server), runner: runner, core: core, null) as MegaDrive.Server;
                return connection.register_object<MegaDrive.Server> (
                    @"$path$name", server
                );
            case NINTENDO_64:
                var server = Object.new (typeof (Nintendo64.Server), runner: runner, core: core, null) as Nintendo64.Server;
                return connection.register_object<Nintendo64.Server> (
                    @"$path$name", server
                );
            case PC_ENGINE:
                var server = Object.new (typeof (PcEngine.Server), runner: runner, core: core, null) as PcEngine.Server;
                return connection.register_object<PcEngine.Server> (
                    @"$path$name", server
                );
            case PC_ENGINE_CD:
                var server = Object.new (typeof (PcEngineCd.Server), runner: runner, core: core, null) as PcEngineCd.Server;
                return connection.register_object<PcEngineCd.Server> (
                    @"$path$name", server
                );
            case PLAYSTATION:
                var server = Object.new (typeof (PlayStation.Server), runner: runner, core: core, null) as PlayStation.Server;
                return connection.register_object<PlayStation.Server> (
                    @"$path$name", server
                );
            case SEGA_SATURN:
                var server = Object.new (typeof (SegaSaturn.Server), runner: runner, core: core, null) as SegaSaturn.Server;
                return connection.register_object<SegaSaturn.Server> (
                    @"$path$name", server
                );
            case SG1000:
                var server = Object.new (typeof (Sg1000.Server), runner: runner, core: core, null) as Sg1000.Server;
                return connection.register_object<Sg1000.Server> (
                    @"$path$name", server
                );
            case SUPER_NES:
                var server = Object.new (typeof (SuperNes.Server), runner: runner, core: core, null) as SuperNes.Server;
                return connection.register_object<SuperNes.Server> (
                    @"$path$name", server
                );
            default:
                return 0;
        }
    }
#endif

#if UI_PROCESS
    public async Object? create_platform_proxy (Hs.Platform platform, DBusConnection connection, string path) throws Error {
        var name = platform.get_name ();

        switch (platform) {
            case ATARI_2600:
                return yield connection.get_proxy<Atari2600.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case ATARI_7800:
                return yield connection.get_proxy<Atari7800.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case ATARI_LYNX:
                return yield connection.get_proxy<AtariLynx.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case FAMICOM_DISK_SYSTEM:
                return yield connection.get_proxy<Fds.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case GAME_BOY:
                return yield connection.get_proxy<GameBoy.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case MASTER_SYSTEM:
                return yield connection.get_proxy<MasterSystem.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case MEGA_DRIVE:
                return yield connection.get_proxy<MegaDrive.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case NES:
                return yield connection.get_proxy<Nes.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case NINTENDO_64:
                return yield connection.get_proxy<Nintendo64.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case PC_ENGINE:
                return yield connection.get_proxy<PcEngine.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case PC_ENGINE_CD:
                return yield connection.get_proxy<PcEngineCd.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case PLAYSTATION:
                return yield connection.get_proxy<PlayStation.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case SEGA_SATURN:
                return yield connection.get_proxy<SegaSaturn.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case SG1000:
                return yield connection.get_proxy<Sg1000.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            case SUPER_NES:
                return yield connection.get_proxy<SuperNes.Proxy> (
                    null, @"$path$name", DO_NOT_LOAD_PROPERTIES
                );
            default:
                return null;
        }
    }
#endif
}
