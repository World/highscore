// This file is part of Highscore. License: GPL-3.0-or-later

#include <glib.h>

#include <epoxy/gl.h>
#include <epoxy/egl.h>
#include <unistd.h>

#if RUNNER_PROCESS

gboolean
highscore_dmabuf_check_available (void)
{
  EGLDisplay display = eglGetCurrentDisplay ();
  EGLContext context = eglGetCurrentContext ();

  if (display == EGL_NO_DISPLAY)
    return FALSE;

  if (context == EGL_NO_CONTEXT)
    return FALSE;

  if (epoxy_egl_version (display) < 15)
    return FALSE;

  if (!epoxy_has_egl_extension (display, "EGL_MESA_image_dma_buf_export"))
    return FALSE;

  if (!epoxy_has_egl_extension (display, "EGL_EXT_image_dma_buf_import"))
    return FALSE;

  if (!epoxy_has_egl_extension (display, "EGL_EXT_image_dma_buf_import_modifiers"))
    return FALSE;

  return TRUE;
}

gboolean
highscore_dmabuf_export_dmabuf (GLuint        texture,
                                GLint        *width,
                                GLint        *height,
                                EGLint       *fourcc,
                                EGLuint64KHR *modifier,
                                EGLint       *offset,
                                EGLint       *stride,
                                int          *fd)
{
  EGLDisplay display = eglGetCurrentDisplay ();
  EGLContext context = eglGetCurrentContext ();
  EGLBoolean result;
  EGLImage image;
  EGLint num_planes;
  EGLAttrib attrs[3];
  int i = 0;

  attrs[i++] = EGL_IMAGE_PRESERVED;
  attrs[i++] = EGL_TRUE;

  attrs[i++] = EGL_NONE;

  image = eglCreateImage (display,
                          context,
                          EGL_GL_TEXTURE_2D,
                          (EGLClientBuffer)(guint64) texture,
                          attrs);
  g_assert (image != EGL_NO_IMAGE);

  result = eglExportDMABUFImageQueryMESA (display,
                                          image,
                                          fourcc,
                                          &num_planes,
                                          modifier);

  if (!result) {
    g_warning ("eglExportDMABUFImageQueryMESA failed");
    eglDestroyImage (display, image);
    return FALSE;
  }

  if (num_planes > 1) {
    g_warning ("Multi-planar textures are not supported"); // TODO
    eglDestroyImage (display, image);
    return FALSE;
  }

  result = eglExportDMABUFImageMESA (display,
                                     image,
                                     fd,
                                     stride,
                                     offset);
  if (!result) {
    g_warning ("eglExportDMABUFImageMESA failed");
    eglDestroyImage (display, image);
    return FALSE;
  }

  glGetTexLevelParameteriv (GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, width);
  glGetTexLevelParameteriv (GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, height);

  eglDestroyImage (display, image);
  return TRUE;
}

#else

GLuint
highscore_dmabuf_import_dmabuf (GLint        width,
                                GLint        height,
                                EGLint       fourcc,
                                EGLuint64KHR modifier,
                                EGLint       offset,
                                EGLint       stride,
                                int          fd)
{
  EGLDisplay display = eglGetCurrentDisplay ();
  GLuint texture;

  EGLAttrib const attrs[] = {
    EGL_IMAGE_PRESERVED,                EGL_TRUE,
    EGL_WIDTH,                          width,
    EGL_HEIGHT,                         height,
    EGL_LINUX_DRM_FOURCC_EXT,           fourcc,
    EGL_DMA_BUF_PLANE0_FD_EXT,          fd,
    EGL_DMA_BUF_PLANE0_OFFSET_EXT,      offset,
    EGL_DMA_BUF_PLANE0_PITCH_EXT,       stride,
    EGL_DMA_BUF_PLANE0_MODIFIER_LO_EXT, modifier & 0xFFFFFFFF,
    EGL_DMA_BUF_PLANE0_MODIFIER_HI_EXT, modifier >> 32,
    EGL_NONE
  };
  EGLImage image;

  image = eglCreateImage (display,
                          EGL_NO_CONTEXT,
                          EGL_LINUX_DMA_BUF_EXT,
                          NULL,
                          attrs);
  g_assert (image != EGL_NO_IMAGE);

  glGenTextures (1, &texture);
  glBindTexture (GL_TEXTURE_2D, texture);
  glEGLImageTargetTexture2DOES (GL_TEXTURE_2D, image);
  glBindTexture (GL_TEXTURE_2D, 0);

  eglDestroyImageKHR (display, image);

  return texture;
}

#endif
