// This file is part of Highscore. License: GPL-3.0-or-later

[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename = "config.h")]
namespace Config {
    public const string APPLICATION_ID;
    public const string GETTEXT_PACKAGE;
    public const string GNOMELOCALEDIR;
    public const string VERSION;
    public const string VERSION_SUFFIX;
    public const string VCS_TAG;
    public const string PROFILE;
    public const string CORES_DIR;
    public const string FILTERS_DIR;
    public const string OVERLAYS_DIR;
    public const string RUNNER_PATH;
}
