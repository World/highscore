// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.SharedInputBuffer : Object, FileDescriptorBased {
    private int fd;
    private void* data;
    private size_t size;

    private struct Metadata {
        PosixExt.Semaphore semaphore;
        size_t size;
    }

    private Metadata* metadata;
    private Hs.InputState* input_state;

#if RUNNER_PROCESS
    public SharedInputBuffer (Hs.Platform platform) {
        fd = memfd_create ("[highscore-runner input buffer]");

        resize (sizeof (Hs.InputState));

        if (metadata.semaphore.init (1, 1) != 0)
            critical ("Failed to init semaphore: %s", strerror (Posix.errno));
    }
#else
    public SharedInputBuffer (UnixInputStream stream) {
        fd = unwrap_fd (stream);

        resize (size);
    }
#endif

    ~SharedInputBuffer () {
#if RUNNER_PROCESS
            if (metadata.semaphore.destroy () != 0)
                critical ("Failed to destroy semaphore: %s", strerror (Posix.errno));
#endif

        Posix.munmap (data, size);
        Posix.close (fd);
    }

    private void resize (size_t size) {
        size += sizeof (Metadata);

        if (data != null) {
            if (size == this.size)
                return;

            Posix.munmap (data, this.size);
            data = null;
        }

        this.size = size;

        if (Posix.ftruncate (fd, size) != 0)
            critical ("Failed to truncate framebuffer: %s", strerror (Posix.errno));

        data = Posix.mmap (
            null, size,
            Posix.PROT_READ | Posix.PROT_WRITE,
            Posix.MAP_SHARED,
            fd, 0
        );

        metadata = data;
        input_state = &data[sizeof (Metadata)];

        metadata.size = size;
    }

    public int get_fd () {
        return fd;
    }

    private void lock () {
        if (metadata.semaphore.wait () != 0)
            critical ("Failed to lock semaphore: %s", strerror (Posix.errno));
    }

    private void unlock () {
        if (metadata.semaphore.post () != 0)
            critical ("Failed to unlock semaphore: %s", strerror (Posix.errno));
    }

#if RUNNER_PROCESS
    public Hs.InputState copy_input_state () {
        @lock ();
        var ret = *input_state;
        @unlock ();

        return ret;
    }
#else
    public delegate void ModifyInputStateFunc (Hs.InputState* state);

    public void modify_input_state (ModifyInputStateFunc func) {
        @lock ();
        func (input_state);
        @unlock ();
    }
#endif
}
