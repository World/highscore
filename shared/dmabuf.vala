// This file is part of Highscore. License: GPL-3.0-or-later

using GL;
using EGL;

public errordomain Highscore.DmabufError {
    UNAVAILABLE,
    EXPORT_FAILED,
}

public class Highscore.Dmabuf : Object, FileDescriptorBased {
    private int width;
    private int height;
    private int fourcc;
    private uint64 modifier;
    private int offset;
    private int stride;
    private int fd;

#if RUNNER_PROCESS
    public Dmabuf.export (GLuint texture) throws DmabufError {
        if (NO_DMABUF in Debug.get_flags ())
            throw new DmabufError.UNAVAILABLE ("Dmabuf support disabled");

        if (!check_available ())
            throw new DmabufError.UNAVAILABLE ("Dmabuf support unavailable");

        bool ret = export_dmabuf (
            texture,
            out width,
            out height,
            out fourcc,
            out modifier,
            out offset,
            out stride,
            out fd
        );

        if (!ret)
            throw new DmabufError.EXPORT_FAILED ("Dmabuf export failed");

        debug ("Exported a %d×%d dmabuf texture", width, height);
    }
#else
    public Dmabuf (int fd, Variant metadata) {
        this.fd = fd;

        metadata.@get (
            "(iiitii)",
            out width, out height, out fourcc,
            out modifier, out offset, out stride
        );
    }

    public GLuint import () {
        GLuint tex = import_dmabuf (
            width, height, fourcc, modifier, offset, stride, fd
        );

        debug ("Imported a %d×%d dmabuf texture", width, height);

        return tex;
    }
#endif

    ~Dmabuf () {
        if (fd > 0)
            Posix.close (fd);
    }

#if RUNNER_PROCESS
    public Variant serialize_metadata () {
        return new Variant (
            "(iiitii)", width, height, fourcc, modifier, offset, stride
        );
    }
#endif

    public int get_width () {
        return width;
    }

    public int get_height () {
        return height;
    }

    public int get_fd () {
        return fd;
    }

#if RUNNER_PROCESS
    private static extern bool check_available ();

    private static extern bool export_dmabuf (
        GLuint texture,
        out GLint width,
        out GLint height,
        out EGLint fourcc,
        out EGLuint64KHR modifier,
        out EGLint offset,
        out EGLint stride,
        out int fd
    );
#else
    private static extern GLuint import_dmabuf (
        GLint width,
        GLint height,
        EGLint fourcc,
        EGLuint64KHR modifier,
        EGLint offset,
        EGLint stride,
        uint fd
    );
#endif
}
