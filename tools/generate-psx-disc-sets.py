#!/bin/env python3

# Usage: ./generate-psx-disc-sets.py > ../src/platforms/playstation/playstation-disc-sets-data.vala

import os, requests, sys
from bs4 import BeautifulSoup

BASE_URL = 'http://psxdatacenter.com/'

disc_sets = []

class DiscSet:
    def __init__(self, discs, title):
        self.discs = discs
        self.title = title

# These sets consist entirely of games found elsewhere, so we can remove them
dup_sets = [
  # Arc The Lad Collection  -  [ 6 Discs ] [E]
  ['SLUS-01224', 'SLUS-01252', 'SLUS-01253', 'SLUS-01254', 'SLUS-01255', 'SLUS-01256'],

  # Bust-A-Move 2 + Bust-A-Move 3 Dx - [2 Discs] [E]
  ['SLES-04153', 'SLES-14153'],

  # Capcom Generations - [3 Discs] [E]
  ['SLES-02098', 'SLES-12098', 'SLES-22098'],

  # Capcom Generations - [4 Discs] [E]
  ['SLES-01881', 'SLES-11881', 'SLES-21881', 'SLES-31881'],

  # Dino Crisis 5th Anniversary [3 Discs] [J]
  # CPCS-00701 is unique but it's a DVD with interviews
  ['CPCS-00701', 'SLPM-86903', 'SLPM-87234'],

  # Final Fantasy Anthology - [2 Discs] [E]
  ['SCES-03840', 'SCES-13840'],
  ['SLUS-00879', 'SLUS-00900'],

  # Final Fantasy Chronicles - [2 Discs] [E]
  ['SLUS-01360', 'SLUS-01363'],

  # Final Fantasy Collection - [3 Discs] [J]
  ['SCPS-45388', 'SCPS-45389', 'SCPS-45390'],

  # Final Fantasy Collection - [3 Discs] [J]
  ['SLPS-01948', 'SLPS-01949', 'SLPS-01950'],

  # Final Fantasy Collection - Anniversary Package - [3 Discs] [J]
  ['SLPS-01945', 'SLPS-01946', 'SLPS-01947'],

  # Final Fantasy Origins - [2 Discs] [E]
  ['SLES-04034', 'SLES-14034'],

  # Final Fantasy I & II - Premium Package - [2 Discs] [J]
  ['SLPS-03500', 'SLPS-03501'],

  # Front Mission History - [3 Discs] [J]
  ['SLPM-87330', 'SLPM-87331', 'SLPM-87332'],

  # Grand Theft Auto [Collector's Edition Set] - [3 Discs] [E][F][G][I][S]
  ['SLES-00032', 'SLES-03389', 'SLES-01404'],

  # Grand Theft Auto - Collector's Edition - [3 Discs] [E]
  ['SLUS-00106', 'SLUS-00789', 'SLUS-00846'],

  # Grand Theft Auto Director's Cut - [2 Discs] [E]
  ['SLUS-00106', 'SLUS-00846'],

  # Langrisser IV & V - [2 Discs]
  ['SLPS-01818', 'SLPS-01819'],

  # Le Concert FF+PP - Fortisimmo & Pianissimo - [2 Discs] [J]
  ['SLPS-02925', 'SLPS-02926'],

  # Mahjong Kurabu & Cadillac [Honkakuha De 2000] [2 Discs] [J]
  ['SLPS-02088', 'SLPS-02089'],

  # Metal Gear Solid [Premium Package Sai Hakkou Kinen] - [3 Discs] [J]
  ['SLPM-86111', 'SLPM-86112', 'SLPM-86113'],

  # Shogi Club & Cadillac - [2 Discs] [J]
  ['SLPS-02086', 'SLPS-02087'],

  # Strider 2 - [2 Discs] [E]
  ['SLES-02867', 'SLES-12867'],
  ['SLUS-01142', 'SLUS-01163'],

  # Strider Hiryu 1 & 2 - [2 Discs] [J]
  ['SLPS-02620', 'SLPS-02621'],

  # Super Black Bass 2 & The Blue Marlin [2 Discs] [J]
  ['SLPM-86717', 'SLPM-86718'],

  # Thunder Storm LX-3 & Roadblaster - [2 Discs] [E][J]
  ['SLPS-00094', 'SLPS-00095'],

  # Time Gal & Ninja Hayate - [2 Discs] [J]
  ['SLPS-00383', 'SLPS-00384'],

  # Tokyo Majin Gakuen Kenpu-Cyou Emaki - [4 Discs] [J]
  ['SLPS-02834', 'SLPS-02835', 'SLPS-02836', 'SLPS-02837'],

  # Twilight Syndrome - Special - [2 Discs] [J]
  ['SLPS-01442', 'SLPS-01443'],

  # Yukyu Gensoukyoku - Perpetual Collection - [5 Discs] [J]
  ['SLPS-02325', 'SLPS-02326', 'SLPS-02327', 'SLPS-02328', 'SLPS-02329'],
]

# These sets have wrong info
wrong_sets = [
  # Disney Action Games Collectors' Edition [3 Discs] [E]
  # This ID is not actually used for the games; and we handle each game
  # individually anyway
  (['SLUS-07013CE'], ['SLUS-01536', 'SLUS-01537', 'SLUS-01538']),

  # Overblood 2 [2 Discs] [J]
  # SCPS-45190 is duplicated with Dead or Alive
  # Source: https://pastebin.com/KsuaKa8M
  (['SCPS-45190', 'SCPS-45191'], ['SCPS-45191', 'SCPS-45192']),

  # Waku Puyo Dungeon Expert
  # The game is single disc, disc 2 doesn't seem to exist
  # Source: http://wiki.redump.org/index.php?title=Sony_PlayStation_-_Asia_Undumped_Discs
  (['SLPS-01966', 'SLPS-01967'], ['SLPS-01966']),

  # The Raven Project [2 Discs] [E][F][G]
  # The list contains a typo
  (['SLES-00039', 'SLES-00039'], ['SLES-00039', 'SLES-10039']),
]

# These are bonus discs, unrelated to the game they came with
# Usually demos of other games
bonus_discs = [
  # Astronoka Preview
  # Included in: Star Ocean - The Second Story - [3 Discs] [J]
  'SLPM-86107',

  # Itadaki Street - Gorgeous King Demo
  # Included in: Astronoka - [2 Discs] [J]
  'SLPM-86089',

  # V-Rally 2 & Driver Demos
  # Included in: Bakusou Dekotora Densetsu 2 (Art Truck Battle 2) - [2 Discs] [J]
  'SLPS-02466',

  # Biohazard Complete Disc
  # Included in: Biohazard - Director's Cut - Dual Shock - [2 Discs] [J]
  'SLPS-01513',

  # Biohazard 2 Demo
  # Included in: Biohazard - Director's Cut - [2 Discs] [J]
  'SLPS-00999',

  # Disc 1: Disney Presents - Tigger's Honey Hunt [E]
  # Disc 2: Disney/Pixar - A Bug's Life [E]
  # Disc 3: Disney's Tarzan [E]
  # Disney Action Games Collectors' Edition [E] is a set of independent games,
  # we should treat them as such
  'SLUS-01536', 'SLUS-01537', 'SLUS-01538',

  # Final Fantasy VII Sample CD
  # Included in: Tobal No.1 - [2 Discs] [J]
  'SLPS-00401',

  # Final Fantasy VII - Interactive Sampler CD (DEMO)
  # Included in: Tobal No.1 - [2 Discs] [E]
  'SCUS-94961',

  # Final Fantasy X Demo
  # Included in: Final Fantasy Vi  -  [ 2 Discs ] [E]
  'SCED-50642',

  # Genso Suikoden II Demo
  # Included in: Metal Gear Solid - [3 Discs] [J]
  'SCPS-45319', 'SCPS-45322', 'SLPM-86116',

  # Genso Suikoden II Demo
  # Included in: Metal Gear Solid [Premium Package] - [3 Discs] [J]
  'SLPM-86113',

  # Gran Turismo 2 Bonus PlayStation Disc
  # Included in: Gran Turismo 2 - The Real Driving Simulator - [3 Discs] [E]
  'SCUS-94588',

  # Lunar 2 - Eternal Blue Complete Demo [E]
  # Included in: Vanguard Bandits [E]
  'SLUS-90083',

  # The Making of Lunar - Silver Star Story
  # Included in: Lunar - Silver Star Story Complete - [3 Discs] [E]
  'SLUS-00921',

  # The Making of: Lunar 2 - Eternal Blue Complete
  # Included in: Lunar 2 - Eternal Blue Complete - [4 Discs] [E]
  'SLUS-01257',

  # Megaman Legends 2 Demo [E]
  # Included in: The Misadventures of Tron Bonne [E]
  'SLUS-90078',

  # Metal Gear Solid Pilot Disc
  # Included in: Jikkyou Pawafuru Puroyakyu '98 - Kaimakuban - [2 Discs] [J]
  'SLPM-86098',

  # Persona 2: Eternal Punishment Bonus Disc
  # Included in: Persona 2: Eternal Punishment
  'SLUS-01339',

  # Persona 2: Innocent Sin demo
  # Included in: Devil Summoner - Soul Hackers [3 Discs] [J]
  'SLPS-01923',

  # Rockman Dash Demo
  # Included in: Rockman X4 [Playstation The Best] - [2 Discs] [J]
  'SLPS-91107',

  # Resident Evil 2 Demo
  # Included in: Resident Evil - Director's Cut - [2 Discs] [E]
  'SLUS-90009',

  # Resident Evil 3 - Nemesis (DEMO)
  # Included in: Dino Crisis - [ 2 Discs ] [E]
  'SLUS-90064',

  # Ridge Racer Hi-Spec Demo
  # Included in: Ridge Racer Type 4 - [ 2 Discs ] [E][F][G][I][S]
  'SCED-01832',

  # SquareSoft '98 Collector's CD Vol.1 - Xenogears (DEMO)
  # Included in: Parasite Eve - [3 Discs] [E]
  # Accidentally included in: Parasite Eve 2 - [2 Discs] [E]
  'SLUS-90028',

  # SquareSoft '98 Collector's CD Vol.2 - Final Fantasy VIII (DEMO)
  # Included in: Brave Fencer Musashi - [2 Discs] [E]
  'SLUS-90029',

  # SquareSoft 2000 Collector's CD Vol. 3
  # Preview movies: Legend of Mana, Chrono Cross, SaGa Frontier 2, Chocobo's Dungeon 2
  # Interactive: Threads of Fate, Front Mission 3, Chocobo Racing
  # Included in: Vagrant Story [E]
  'SLUS-90075',

  # Square's Preview 3 (Final Fantasy VII demo)
  # Included in: Brave Fence Musashiden - [2 Discs] [J]
  'SLPS-01491',

  # Square's Preview 4
  # Included in: Saga Frontier 2 - [2 Discs] [J]
  'SCPS-45301', 'SLPS-01991',

  # Square's Preview 5 (Chrono Cross Demo, Front Mission 3 Demo, Dewprism Demo)
  # Included in: Seiken Densetsu - Legend Of Mana - [2 Discs] [J]
  'SCPS-45417', 'SLPS-02171',

  # Zipangujima - Unmei wa Saikoro ga Kimeru!? (Demo)
  # Included in: Remote Control Dandy - [2 Discs] [J]
  'SLPS-02244',
]

new_sets = [
  # Part of: Arc The Lad Collection - [6 Discs] [E]
  DiscSet(['SLUS-01253', 'SLUS-01254'], 'Arc The Lad III - [ 2 Discs ] [E]'),

  # Part of: Tokyo Majin Gakuen Kenpu-Cyou Emaki - [4 Discs] [J]
  DiscSet(['SLPS-02834', 'SLPS-02835'], 'Tokyo Majin Gakuen Ken Kaze Tobari - [ 2 Discs ] [J]'),
]

def get_with_cache(url):
    cache_path = os.path.join('cache', url)
    cache_dir = os.path.dirname(cache_path)

    if not os.path.isdir(cache_dir):
        os.makedirs(cache_dir)

    if os.path.isfile(cache_path):
        with open(cache_path, 'rb') as file:
            return file.read()

    sys.stderr.write('Downloading: {}\n'.format(url))
    page = requests.get(BASE_URL + url)
    with open(cache_path, 'wb') as file:
        file.write(page.content)

    return page.content

def scrape_list(url):
    page_content = get_with_cache(url)
    page_content = page_content.replace(b'<tr>\n<tr>', b'<tr>')
    page_content = page_content.replace(b'</br>', b'<br>')

    soup = BeautifulSoup(page_content, 'html.parser')

    tables = soup.find_all('table', class_='sectiontable')

    results = []

    for table in tables:
        rows = table.find_all('tr')
        for row in rows:
            serial_cell = row.find_all('td', class_='col2')[0]
            discs = serial_cell.contents

            title_cell = row.find_all('td', class_='col3')[0]
            title = title_cell.contents[0].strip().title()
            title = title.replace("'S", "'s")
            title = title.replace('\n', '')
            title = title.replace('\t', '')
            title = title.replace('  ', ' ')

            lang_cell = row.find_all('td', class_='col4')[0]
            lang = lang_cell.contents[0].strip ();

            # The page has an unclosed </td> and we have to trim the rest
            try:
                broken_index = discs.index('</td')
            except ValueError:
                broken_index = -1

            if broken_index >= 0:
                discs = discs[:broken_index]

            discs = [s.string for s in discs if s.string != None]

            # The page has a duplicated <tr> and we get the same game twice
            if len(results) > 0 and results[0].discs == discs:
                continue

            disc_set = DiscSet(discs, title + ' ' + lang)

            results.append(disc_set)

    return results

def find_set(sets, discs):
    for s in sets:
        if s.discs == discs:
            return s

    sys.stderr.write('Nonexistent disc set: {}\n'.format(discs))
    return None

def fix_data(sets):
    for discs in dup_sets:
        s = find_set(sets, discs)
        if s == None:
            continue

        sets.remove(s)
        sys.stderr.write('Removed disc set: {}\n'.format(s.title))

    for wrong, correct in wrong_sets:
        s = find_set(sets, wrong)
        if s == None:
            continue

        s.discs = correct
        sys.stderr.write('Fixed disc set: {}\n'.format(s.title))

    for disc in bonus_discs:
        for s in sets:
            if disc in s.discs:
                s.discs.remove(disc)
                sys.stderr.write('Removed bonus disc: {} from: {}\n'.format(disc, s.title))

    for s in new_sets:
        sets.append(s)
        sys.stderr.write('Added disc set: {}\n'.format(s.title))

    return sets

def remove_single_disc_games(sets):
    ret = []

    for s in sets:
        if len(s.discs) > 1:
            ret.append(s)

    return ret

jp_sets = scrape_list('jlist.html')
eu_sets = scrape_list('plist.html')
us_sets = scrape_list('ulist.html')

sets = jp_sets + eu_sets + us_sets
sets = fix_data(sets)
sets = remove_single_disc_games(sets)
sets = sorted(sets, key=lambda x: x.title)

max_length = max(len(s.discs) for s in sets)

for s in sets:
    for d in s.discs:
        for ss in sets:
            if ss == s:
                continue

            for dd in ss.discs:
                if d == dd:
                    sys.stderr.write('Duplicate disc: {}, present in both {} and {}\n'.format(d, s.title, ss.title))

index = []
for i, s in enumerate(sets):
    for d in s.discs:
        entry = (d, i, s.title)
        index.append(entry)

index = sorted(index, key = lambda x: x[0])

index_discs = [e[0] for e in index]
for s in sets:
    s.disc_indices = [index_discs.index(d) for d in s.discs]

print('''// This file is part of Highscore. License: GPL-3.0-or-later

// This file is generated, see tools/generate-psx-disc-sets.py
namespace Highscore.PlayStation.DiscSets {{
    public struct DiscIdIndexEntry {{
        string disc_id;
        int disc_set_index;
    }}

    public struct DiscSet {{
        int n_discs;
        int disc_ids[{}];
    }}

    public const DiscIdIndexEntry[] DISC_ID_INDEX = {{'''.format(max_length))

for entry in index:
    disc, i, title = entry
    print('        {{ "{}", {} }}, // {}'.format(disc, i, title))

print('    };')
print()
print('    public const DiscSet[] DISC_SETS = {')

for s in sets:
    n_discs = len(s.discs)
    set_str = ', '.join(str(id) for id in s.disc_indices)
    print('        {{ {}, {{ {} }}}}, // {}'.format(n_discs, set_str, s.title))

print('    };')
print('}')
