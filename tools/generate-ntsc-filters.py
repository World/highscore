#!/bin/env python3

from math import sin, cos, pi

TAPS=24
CHROMA_TAPS = 12

# Lanczos kernel
def luma_tap(i: int, taps: int, a: float, b: float) -> float:
    x = float(taps - i) * b

    if i == taps:
        return 1.0

    return a * sin(pi*x) * sin(pi*x/a) / (pi*x)**2

# Blackman window
def chroma_tap(i: int, taps: int) -> float:
    x = float(i + taps - TAPS) / float(taps * 2)

    if i + taps - TAPS < 0:
        return 0.0

    return 0.42659 - 0.49656 * cos(2.0 * pi * x) + 0.076849 * cos(4.0 * pi * x)

for i in range(0, TAPS + 1):
    print('    luma_filter[{}] = {:6f};'.format(i, luma_tap(i, TAPS, 2.75, 0.155)))

print()

for i in range(0, TAPS + 1):
    print('    luma_filter[{}] = {:6f};'.format(i, luma_tap(i, TAPS, 2.75, 0.155 / 256.0 * 320.0)))

print()

for i in range(0, TAPS + 1):
    print('  chroma_filter[{}] = {:9f};'.format(i, chroma_tap(i, CHROMA_TAPS)))
