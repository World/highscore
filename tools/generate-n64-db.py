#!/bin/env python3

# Usage: ./generate-n64-db.py > ../src/platforms/nintendo-64/nintendo-64-overrides.vala

import requests

INI_URL = 'https://raw.githubusercontent.com/mupen64plus/mupen64plus-core/master/data/mupen64plus.ini'

page = requests.get(INI_URL)
lines = page.text.splitlines()

groups = []
group = None
dd = False

for l in lines:
    if l == '; 64DD Games':
        dd = True
        continue

    if l.startswith(';') or l == '':
        continue

    if l.startswith('['):
        if group != None:
            groups.append(group)
        group = []

    group.append(l)

    if dd and l.startswith('['):
        group.append('64DD=Yes')

if group != None:
    groups.append(group)

sections = {}

for g in groups:
    name = g[0].strip()[1:-1]

    if name in sections:
        section = sections[name]
    else:
        section = {}
        sections[name] = section

    for l in g[1:]:
        keyval = l.strip().split('=')
        assert(len(keyval) == 2)

        key = keyval[0]
        val = keyval[1]

        section[key] = val

def extract_data(section):
    if 'GoodName' in section:
        name = section['GoodName']
    else:
        name = None

    if 'RefMD5' in section:
        refmd5 = section['RefMD5']
    else:
        refmd5 = None

    if refmd5 != None:
        section = sections[refmd5]

    if 'Players' in section:
        players = int(section['Players'])
    else:
        players = 4

    if 'Mempak' in section:
        mempak = section['Mempak'] == 'Yes'
    else:
        mempak = False

    if 'Rumble' in section:
        rumblepak = section['Rumble'] == 'Yes'
    else:
        rumblepak = False

    if 'Transferpak' in section:
        transferpak = section['Transferpak'] == 'Yes'
    else:
        transferpak = False

    if 'Biopak' in section:
        biopak = section['Biopak'] == 'Yes'
    else:
        biopak = False

    paks = []
    if mempak:
        paks.append('MEMORY_PAK')
    if rumblepak:
        paks.append('RUMBLE_PAK')
    if transferpak:
        paks.append('TRANSFER_PAK')
    if biopak:
        paks.append('BIO_SENSOR')

    if len(paks) == 0:
        paks.append('0')

    return name, players, ' | '.join(paks)

md5_sorted = sorted(sections.keys())

crc_sections = {}
for section in sections.values():
    if not 'CRC' in section:
        continue

    crc = int(section['CRC'].replace(' ', ''), 16)
    crc_sections[crc] = section

crc_sorted = sorted(crc_sections.keys())

print('''// This file is part of Highscore. License: GPL-3.0-or-later

// This file is generated, see tools/generate-n64-db.py
namespace Highscore.Nintendo64.Overrides {
    public struct GameDataMD5 {
        string md5;
        uint8 players;
        SupportedPaks paks;
    }

    public struct GameDataCRC {
        uint64 crc;
        uint8 players;
        SupportedPaks paks;
    }
''')

print('    public const GameDataMD5[] GAME_DATA_MD5 = {')
for md5 in md5_sorted:
    s = sections[md5]
    name, players, paks = extract_data(s)
    md5 = md5.lower()
    if name != None:
        print('        {{ "{}", {}, {} }}, // {}'.format(md5, players, paks, name))
    else:
        print('        {{ "{}", {}, {} }},'.format(md5, players, paks))
print('    };')
print('')

print('    public const GameDataCRC[] GAME_DATA_CRC = {')
for crc in crc_sorted:
    s = crc_sections[crc]
    name, players, paks = extract_data(s)
    if name != None:
        print('        {{ 0x{:016X}, {}, {} }}, // {}'.format(crc, players, paks, name))
    else:
        print('        {{ 0x{:016X}, {}, {} }},'.format(crc, players, paks))
print('    };')
print('}')
