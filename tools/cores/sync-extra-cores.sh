#!/bin/bash

echo 'Syncing Kronos...'
cd kronos
git checkout extui-align
git pull upstream extui-align
git checkout highscore
git merge extui-align
cd ..
echo

echo 'Syncing melonDS...'
cd melonDS
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
