#!/bin/bash

echo 'Syncing blastem...'
cd blastem-highscore
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
echo

echo 'Syncing bsnes...'
cd bsnes
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
echo

echo 'Syncing DeSmuME...'
cd desmume
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
echo

echo 'Syncing Gearsystem...'
cd Gearsystem
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
echo

echo 'Syncing Mednafen...'
cd mednafen-highscore
MEDNAFEN_LATEST_FILENAME=`curl -s 'https://raw.githubusercontent.com/mednafen/mednafen.github.io/master/releases/files/mednafen-latest.tar.xz'`
MEDNAFEN_LATEST=`echo $MEDNAFEN_LATEST_FILENAME | grep -Po '(?<=mednafen-).*(?=.tar.[gxb]z2?)'`
MEDNAFEN_CURRENT=`head -n 1 'ChangeLog' | grep -Po '(?<=-- ).*(?=: --)'`
if [[ $MEDNAFEN_CURRENT != $MEDNAFEN_LATEST ]]; then
  echo "New version available: $MEDNAFEN_LATEST"
  wget "https://raw.githubusercontent.com/mednafen/mednafen.github.io/master/releases/files/$MEDNAFEN_LATEST_FILENAME"
else
  echo "Up to date: $MEDNAFEN_CURRENT"
fi
cd ..
echo

echo 'Syncing mGBA...'
cd mgba
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
echo

echo 'Syncing Nestopia...'
cd nestopia
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
echo

echo 'Syncing ProSystem...'
cd prosystem
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
echo

echo 'Syncing Stella...'
cd stella
git checkout master
git pull upstream master
git checkout highscore
git merge master
cd ..
