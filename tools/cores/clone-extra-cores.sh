#!/bin/bash

# Clones additional cores (WIP or unstable)

if [ ! -d 'kronos' ]; then
  git clone https://github.com/alice-mkh/kronos
  cd kronos
  git remote add upstream https://github.com/FCare/Kronos
  git fetch upstream
  cd ..
fi

if [ ! -d 'melonDS' ]; then
  git clone https://github.com/alice-mkh/melonDS
  cd melonDS
  git remote add upstream https://github.com/melonDS-emu/melonDS
  git fetch upstream
  cd ..
fi
