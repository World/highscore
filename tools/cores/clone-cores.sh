#!/bin/bash

# Clones all of the cores, adding the upstream repository as 'upstream' remote where appropriate

if [ ! -d 'blastem-highscore' ]; then
  git clone https://github.com/alice-mkh/blastem-highscore
  cd blastem-highscore
  git remote add upstream hg::https://www.retrodev.com/repos/blastem
  git fetch upstream
  git gc --aggressive
  cd ..
fi

if [ ! -d 'bsnes' ]; then
  git clone https://github.com/alice-mkh/bsnes
  cd bsnes
  git remote add upstream https://github.com/bsnes-emu/bsnes
  git fetch upstream
  cd ..
fi

if [ ! -d 'desmume' ]; then
  git clone https://github.com/alice-mkh/desmume
  cd desmume
  git remote add upstream https://github.com/TASEmulators/desmume
  git fetch upstream
  cd ..
fi

if [ ! -d 'desmume' ]; then
  git clone https://github.com/alice-mkh/desmume
  cd desmume
  git remote add upstream https://github.com/TASEmulators/desmume
  git fetch upstream
  cd ..
fi

if [ ! -d 'Gearsystem' ]; then
  git clone https://github.com/alice-mkh/Gearsystem
  cd Gearsystem
  git remote add upstream https://github.com/drhelius/Gearsystem
  git fetch upstream
  cd ..
fi

if [ ! -d 'mednafen-highscore' ]; then
  git clone https://github.com/alice-mkh/mednafen-highscore
fi

if [ ! -d 'mgba' ]; then
  git clone https://github.com/alice-mkh/mgba
  cd mgba
  git remote add upstream https://github.com/mgba-emu/mgba
  git fetch upstream
  cd ..
fi

if [ ! -d 'mupen64plus-highscore' ]; then
  git clone https://github.com/alice-mkh/mupen64plus-highscore
  cd mupen64plus-highscore
  ./clone-deps.sh
  cd ..
fi

if [ ! -d 'nestopia' ]; then
  git clone https://gitlab.com/alice-m/nestopia.git
  cd nestopia
  git remote add upstream https://gitlab.com/jgemu/nestopia.git
  git fetch upstream
  cd ..
fi

if [ ! -d 'prosystem' ]; then
  git clone https://gitlab.com/alice-m/prosystem.git
  cd prosystem
  git remote add upstream https://gitlab.com/jgemu/prosystem.git
  git fetch upstream
  cd ..
fi

if [ ! -d 'stella' ]; then
  git clone https://github.com/alice-mkh/stella
  cd stella
  git remote add upstream https://github.com/stella-emu/stella
  git fetch upstream
  cd ..
fi
