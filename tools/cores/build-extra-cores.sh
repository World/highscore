#!/bin/bash

echo "Building kronos..."
pushd kronos/yabause/src/highscore
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
echo

echo "Building melonDS..."
pushd melonDS
mkdir build
cd build
cmake .. -DBUILD_QT_SDL=OFF -DBUILD_HIGHSCORE=ON -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=RelWithDebInfo -DENABLE_JIT=ON
cmake --build . --parallel
sudo cmake --install .
cd ..
popd
