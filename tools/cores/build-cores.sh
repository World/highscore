#!/bin/bash

echo "Building blastem..."
pushd blastem-highscore/highscore
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
echo

echo "Building bsnes..."
pushd bsnes/bsnes
make -j 4 target=highscore binary=library build=performance local=false platform=linux
sudo make target=highscore binary=library build=performance libdir=/usr/lib64 install local=false platform=linux
popd
echo

echo "Building DeSmuME..."
pushd desmume/desmume/src/frontend/highscore
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
echo

echo "Building Gearsystem..."
pushd Gearsystem/platforms/highscore
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
echo

echo "Building Mednafen..."
pushd mednafen-highscore/highscore
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
echo

echo "Building mGBA..."
pushd mgba
mkdir build
cd build
cmake .. -DENABLE_DEBUGGERS=OFF -DUSE_EDITLINE=OFF -DENABLE_GDB_STUB=OFF -DUSE_ZLIB=OFF -DUSE_MINIZIP=OFF -DUSE_PNG=OFF -DUSE_LIBZIP=OFF -DUSE_SQLITE3=OFF -DUSE_ELF=OFF -DUSE_LUA=OFF -DUSE_JSON_C=OFF -DUSE_LZMA=OFF -DUSE_DISCORD_RPC=OFF -DENABLE_SCRIPTING=OFF -DBUILD_QT=OFF -DBUILD_SDL=OFF -DBUILD_HIGHSCORE=ON -DSKIP_LIBRARY=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr
cmake --build . --parallel
sudo cmake --install .
cd ..
popd
echo

echo "Building Mupen64Plus..."
pushd mupen64plus-highscore
./build-deps.sh
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
echo

echo "Building Nestopia-JG..."
pushd nestopia/highscore
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
echo

echo "Building Prosystem-JG..."
pushd prosystem/highscore
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
echo

echo "Building Stella..."
pushd stella/src/os/highscore
meson setup build --prefix=/usr --optimization=2
ninja -C build
sudo ninja -C build install
popd
