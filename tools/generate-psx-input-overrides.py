#!/bin/env python3

# Usage: ./generate-psx-input-overrides.py > ../src/platforms/playstation/playstation-overrides-input.vala

import os, requests, sys
from bs4 import BeautifulSoup

BASE_URL = 'http://psxdatacenter.com/'

# These sets consist entirely of games found elsewhere, so we can remove them
dup_sets = [
  # Dino Crisis 5th Anniversary [3 Discs] [J]
  # CPCS-00701 is unique but it's a DVD with interviews
  ['CPCS-00701', 'SLPM-86903', 'SLPM-87234'],
]

# These sets have wrong info
wrong_sets = [
  # Disney Action Games Collectors' Edition [3 Discs] [E]
  # This ID is not actually used for the games; and we handle each game
  # individually anyway
  (['SLUS-07013CE'], ['SLUS-01536', 'SLUS-01537', 'SLUS-01538']),

  # Overblood 2 [2 Discs] [J]
  # SCPS-45190 is duplicated with Dead or Alive
  # Source: https://pastebin.com/KsuaKa8M
  (['SCPS-45190', 'SCPS-45191'], ['SCPS-45191', 'SCPS-45192']),

  # Waku Puyo Dungeon Expert
  # The game is single disc, disc 2 doesn't seem to exist
  # Source: http://wiki.redump.org/index.php?title=Sony_PlayStation_-_Asia_Undumped_Discs
  (['SLPS-01966', 'SLPS-01967'], ['SLPS-01966']),

  # The Raven Project [2 Discs] [E][F][G]
  # The list contains a typo
  (['SLES-00039', 'SLES-00039'], ['SLES-00039', 'SLES-10039']),
]

# These sets have wrong info URLs
wrong_urls = [
  # Atlantis - The Lost Tales [3 Discs] [E]
  # Typoed as a dup of the next row
  (['SLES-01291', 'SLES-11291', 'SLES-21291'], 'games/P/A/SLES-01291.html'),

  # World Cup Golf - Professional Edition [F]
  # Typoed as a dup of the previous row
  (['SLES-00138'], 'games/P/W/SLES-00138.html'),
]

ignore_urls = [
  # V-Rally - Championship Edition '97 [E][F][G]
  # Use platinum version as a reference
  'games/P/V/SLES-00250.html',

  # TOCA - Touring Car Championship [E]
  # Use platinum version as a reference
  'games/P/T/SLES-00376.html',

  # WarGames - Defcon 1 [E][F][G]
  # Use the version with more languages as a reference
  'games/P/W/SLES-00978.html',

  # Metal Gear Solid [Premium Package Sai Hakkou Kinen] [3 Discs] [J]
  # Use the regular premium package version
  'games/J/M/SLPM-86111-2.html',

  # Kero Kero King [Limited Edition] [J]
  # Use the regular version
  'games/J/K/SLPM-86621LE.html',

  # tmp
  'games/P/G/SLES-00032CE.html',
  'games/P/A/SLES-01416.html',
  'games/P/A/SLES-01748.html',
  'games/U/G/SLUS-00106CE.html',
  'games/U/G/SLUS-00106DC.html',
]

# These are bonus discs, unrelated to the game they came with
# Usually demos of other games
bonus_discs = [
  # Included in: Star Ocean - The Second Story - [3 Discs] [J]
  ('SLPM-86107', 'Astronoka Preview [J]', 'games/J/A/SLPM-86088.html'),

  # Included in: Astronoka - [2 Discs] [J]
  ('SLPM-86089', 'Itadaki Street - Gorgeous King Demo[J]', 'games/J/I/SLPM-86120.html'),

  # Included in: Bakusou Dekotora Densetsu 2 (Art Truck Battle 2) - [2 Discs] [J]
  ('SLPS-02466', 'V-Rally 2 & Driver Demos [J]', 'games/J/V/SLPS-02516.html'), # or games/J/D/SLPS-02613.html

  # Included in: Biohazard - Director's Cut - Dual Shock - [2 Discs] [J]
  # Likely uses the same controller as the main game
  ('SLPS-01513', 'Biohazard Complete Disc [J]', 'games/J/B/SLPS-01512.html'),

  # Included in: Biohazard - Director's Cut - [2 Discs] [J]
  ('SLPS-00999', 'Biohazard 2 Demo [J]', 'games/J/B/SLPS-01222.html'),

  # Disc 1: Disney Presents - Tigger's Honey Hunt [E]
  # Disc 2: Disney/Pixar - A Bug's Life [E]
  # Disc 3: Disney's Tarzan [E]
  # Disney Action Games Collectors' Edition [E] is a set of independent games,
  # we should treat them as such
  ('SLUS-01536', 'Disney Presents - Tigger\'s Honey Hunt [E]', 'games/U/D/SLUS-01210.html'),
  ('SLUS-01537', 'Disney/Pixar - A Bug\'s Life [E]', 'games/U/D/SCUS-94288.html'),
  ('SLUS-01538', 'Disney\'s Tarzan [E]', 'games/U/D/SCUS-94456.html'),

  # Included in: Tobal No.1 - [2 Discs] [J]
  ('SLPS-00401', 'Final Fantasy VII Sample CD [J]', 'games/J/F/SLPS-00700.html'),

  # Included in: Tobal No.1 - [2 Discs] [E]
  ('SCUS-94961', 'Final Fantasy VII - Interactive Sampler CD (DEMO) [J]', 'games/U/F/SCUS-94163.html'),

  # Included in: Final Fantasy Vi - [2 Discs] [E]
  # It's a PlayStation 2 game
  ('SCED-50642', 'Final Fantasy X Demo [E]', None),

  # Included in: Metal Gear Solid - [3 Discs] [J]
  ('SCPS-45319', 'Genso Suikoden II Demo [J]', 'games/J/G/SCPS-45369.html'),
  ('SCPS-45322', 'Genso Suikoden II Demo [J]', 'games/J/G/SCPS-45369.html'),
  ('SLPM-86116', 'Genso Suikoden II Demo [J]', 'games/J/G/SLPM-86168.html'),

  # Included in: Metal Gear Solid [Premium Package] - [3 Discs] [J]
  ('SLPM-86113', 'Genso Suikoden II Demo [J]', 'games/J/G/SLPM-86168.html'),

  # Included in: Gran Turismo 2 - The Real Driving Simulator - [3 Discs] [E]
  ('SCUS-94588', 'Gran Turismo 2 Bonus PlayStation Disc [E]', None),

  # Included in: Vanguard Bandits [E]
  ('SLUS-90083', 'Lunar 2 - Eternal Blue Complete Demo [E]', 'games/U/L/SLUS-01071.html'),

  # Included in: Lunar - Silver Star Story Complete - [3 Discs] [E]
  ('SLUS-00921', 'The Making of Lunar - Silver Star Story [E]', 'games/U/L/SLUS-00628.html'),

  # Included in: Lunar 2 - Eternal Blue Complete - [4 Discs] [E]
  ('SLUS-01257', 'The Making of: Lunar 2 - Eternal Blue Complete [E]', 'games/U/L/SLUS-01071.html'),

  # Included in: The Misadventures of Tron Bonne [E]
  ('SLUS-90078', 'Megaman Legends 2 Demo [E]', 'games/U/M/SLUS-01140.html'),

  # Included in: Jikkyou Pawafuru Puroyakyu '98 - Kaimakuban - [2 Discs] [J]
  ('SLPM-86098', 'Metal Gear Solid Pilot Disc [E]', 'games/J/M/SLPM-86485.html'),

  # Included in: Persona 2: Eternal Punishment
  ('SLUS-01339', 'Persona 2: Eternal Punishment Bonus Disc [E]', 'games/U/P/SLUS-01158.html'),

  # Included in: Devil Summoner - Soul Hackers [3 Discs] [J]
  ('SLPS-01923', 'Persona 2: Innocent Sin Demo [J]', 'games/J/P/SLPS-02100.html'),

  # Included in: Rockman X4 [Playstation The Best] - [2 Discs] [J]
  ('SLPS-91107', 'Rockman Dash Demo [J]', 'games/J/R/SLPS-91135.html'),

  # Included in: Resident Evil - Director's Cut - [2 Discs] [E]
  ('SLUS-90009', 'Resident Evil 2 Demo [E]', 'games/U/R/SLUS-00421.html'),

  # Included in: Dino Crisis - [ 2 Discs ] [E]
  ('SLUS-90064', 'Resident Evil 3 - Nemesis (DEMO) [E]' ,'games/U/R/SLUS-00923.html'),

  # Included in: Ridge Racer Type 4 - [ 2 Discs ] [E][F][G][I][S]
  ('SCED-01832', 'Ridge Racer Hi-Spec Demo [E][F][G][I][S]', 'games/P/R/SCES-01706.html'),

  # Included in: Parasite Eve - [3 Discs] [E]
  # Accidentally included in: Parasite Eve 2 - [2 Discs] [E]
  ('SLUS-90028', 'SquareSoft \'98 Collector\'s CD Vol.1 - Xenogears (DEMO) [E]', 'games/U/X/SLUS-00664.html'),

  # Included in: Brave Fencer Musashi - [2 Discs] [E]
  ('SLUS-90029', 'SquareSoft \'98 Collector\'s CD Vol.2 - Final Fantasy VIII (DEMO) [E]', 'games/U/F/SLUS-00892.html'),

  # Preview movies: Legend of Mana, Chrono Cross, SaGa Frontier 2, Chocobo's Dungeon 2
  # Interactive: Threads of Fate, Front Mission 3, Chocobo Racing
  # Included in: Vagrant Story [E]
  ('SLUS-90075', 'SquareSoft 2000 Collector\'s CD Vol. 3', 'games/U/T/SLUS-01019.html'),

  # Final Fantasy VII demo
  # Included in: Brave Fence Musashiden - [2 Discs] [J]
  ('SLPS-01491', 'Square\'s Preview 3 [J]', 'games/J/F/SLPS-00700.html'),

  # Included in: Saga Frontier 2 - [2 Discs] [J]
  ('SCPS-45301', 'Square\'s Preview 4 [J]', 'games/J/S/SCPS-45300.html'),
  ('SLPS-01991', 'Square\'s Preview 4 [J]', 'games/J/S/SLPS-01990.html'),

  # Chrono Cross Demo, Front Mission 3 Demo, Dewprism Demo
  # Included in: Seiken Densetsu - Legend Of Mana - [2 Discs] [J]
  ('SCPS-45417', 'Square\'s Preview 5 [J]', 'games/J/S/SCPS-45416.html'),
  ('SLPS-02171', 'Square\'s Preview 5 [J]', 'games/J/S/SLPS-02170.html'),

  # Included in: Remote Control Dandy - [2 Discs] [J]
  ('SLPS-02244', 'Zipangujima - Unmei wa Saikoro ga Kimeru!? (Demo) [J]', 'games/J/Z/SLPS-02260.html'),
]

class GameData:
    def __init__(self, discs, title, info_url):
        self.discs = discs
        self.title = title
        self.info_url = info_url
        self.standard_controller = False
        self.analog_controller = False
        self.dualshock = False;

def get_with_cache(url):
    cache_path = os.path.join('cache', url)
    cache_dir = os.path.dirname(cache_path)

    if not os.path.isdir(cache_dir):
        os.makedirs(cache_dir)

    if os.path.isfile(cache_path):
        with open(cache_path, 'rb') as file:
            return file.read()

    sys.stderr.write('Downloading: {}\n'.format(url))
    page = requests.get(BASE_URL + url)
    with open(cache_path, 'wb') as file:
        file.write(page.content)

    return page.content

def scrape_list(url):
    page_content = get_with_cache(url)
    page_content = page_content.replace(b'<tr>\n<tr>', b'<tr>')
    page_content = page_content.replace(b'</br>', b'<br>')

    soup = BeautifulSoup(page_content, 'html.parser')

    tables = soup.find_all('table', class_='sectiontable')

    results = []

    for table in tables:
        rows = table.find_all('tr')
        for row in rows:
            info_cell = row.find_all('td', class_='col1')[0]
            info_a = info_cell.find('a')
            if info_a == None:
                continue

            info_url = info_a['href']

            serial_cell = row.find_all('td', class_='col2')[0]
            discs = serial_cell.contents

            title_cell = row.find_all('td', class_='col3')[0]
            title = title_cell.contents[0].strip().title()
            title = title.replace("'S", "'s")
            title = title.replace('\n', '')
            title = title.replace('\t', '')
            title = title.replace('  ', ' ')

            lang_cell = row.find_all('td', class_='col4')[0]
            lang = lang_cell.contents[0].strip ();

            # The page has an unclosed </td> and we have to trim the rest
            try:
                broken_index = discs.index('</td')
            except ValueError:
                broken_index = -1

            if broken_index >= 0:
                discs = discs[:broken_index]

            discs = [s.string for s in discs if s.string != None]
            data = GameData(discs, title + ' ' + lang, info_url)

            results.append(data)

    return results

def find_set(sets, discs):
    for s in sets:
        if s.discs == discs:
            return s

    sys.stderr.write('Nonexistent game: {}\n'.format(discs))
    return None

def find_set_by_url(sets, url):
    for s in sets:
        if s.info_url == url:
            return s

    sys.stderr.write('Nonexistent game with the URL: {}\n'.format(url))
    return None

def fix_data(sets):
    for discs in dup_sets:
        s = find_set(sets, discs)
        if s == None:
            continue

        sets.remove(s)
        sys.stderr.write('Removed disc set: {}\n'.format(s.title))

    for wrong, correct in wrong_sets:
        s = find_set(sets, wrong)
        if s == None:
            continue

        s.discs = correct
        sys.stderr.write('Fixed discs: {}\n'.format(s.title))

    for discs, url in wrong_urls:
        s = find_set(sets, discs)
        if s == None:
            continue

        s.info_url = url
        sys.stderr.write('Fixed URL: {}\n'.format(s.title))

    for url in ignore_urls:
        s = find_set_by_url(sets, url)
        if s == None:
            continue

        sets.remove(s)
        sys.stderr.write('Removed set: {}\n'.format(s.title))

    new_sets = []
    new_discs = []
    for disc, title, url in bonus_discs:
        for s in sets:
            if disc in s.discs:
                s.discs.remove(disc)
                if url == None:
                    sys.stderr.write('Removed bonus disc: {} from: {}\n'.format(disc, s.title))
                elif not disc in new_discs:
                    sys.stderr.write('Separated bonus disc: {} from: {}\n'.format(disc, s.title))
                    new_data = GameData([disc], title, url)
                    new_sets.append(new_data)
                    new_discs.append(disc)

    for s in new_sets:
        sets.append(s)

    return sets

def scrape_info(game):
    page_content = get_with_cache(game.info_url)

    soup = BeautifulSoup(page_content, 'html.parser')

    table = soup.find('table', id='table19')
    if table == None:
        sys.stderr.write('Missing the table on {}\n'.format(game.title))
        return

    rows = table.find_all('tr')
    if len(rows) != 9:
        sys.stderr.write('Incorrect amount of rows on {}\n'.format(game.title))
        return

#    tds = rows[0].find_all('td')
#    if tds[0].contents[0].strip() != "Number Of Players":
#        sys.stderr.write('Missing number of players on {}\n'.format(game.title))

#    players = tds[1].contents[0].strip()
#    print(players)

    tds = rows[2].find_all('td')
    if tds[0].contents[0].strip() != 'Compatible Controllers Tested':
        sys.stderr.write('Missing compatible controllers on {}\n'.format(game.title))
    controllers1 = tds[1].contents[0].strip()
    controllers2 = list(tds[1].contents[1])[0].strip()

    controllers = controllers1 + ' ' + controllers2
    controllers = controllers.replace('\r', '')
    controllers = controllers.replace('\n', ' / ')
    controllers = controllers.replace('\t', '')
    controllers = controllers.replace('  ', ' ')
    controllers = controllers.lower()
    controllers = controllers.replace('anaog', 'analog')
    controllers = controllers.replace('standar ', 'standard ')
    controllers = controllers.replace('dual shock', 'dualshock')
    controllers = controllers.replace('dual-shock', 'dualshock')

#    tds = rows[3].find_all('td')
#    if tds[0].contents[0].strip() != 'Compatible Light Guns':
#        sys.stderr.write('Missing compatible light guns on {}\n'.format(game.title))
#    lightguns = tds[1].contents[0].strip()

#    tds = rows[4].find_all('td')
#    if tds[0].contents[0].strip() != 'Other Compatible Controllers':
#        sys.stderr.write('Missing other compatible controllers on {}\n'.format(game.title))
#    other_controllers = tds[1].contents[0].strip()

#    tds = rows[5].find_all('td')
#    if tds[0].contents[0].strip() != 'Special Controllers Included Or':
#        sys.stderr.write('Missing special controllers on {}\n'.format(game.title))
#    special_controllers = tds[1].contents[0].strip()

    tables = soup.find_all('table', id='table16')
    if len(tables) != 3:
        sys.stderr.write('Missing controls on {}\n'.format(game.title))
        return

    table = tables[1]

    controls = ' '.join(table.stripped_strings).lower()
    has_analog = 'analog' in controls

    game.standard_controller = 'standard' in controllers
    game.analog_controller = 'analog' in controllers
    game.dualshock = 'dualshock' in controllers

    # Some games are marked as compatible with analog, but don't
    # actually use analog sticks for anything. Mark them as digital-only
    if game.standard_controller and game.analog_controller:
        game.analog_controller = has_analog

jp_sets = scrape_list('jlist.html')
eu_sets = scrape_list('plist.html')
us_sets = scrape_list('ulist.html')

sets = jp_sets + eu_sets + us_sets
sets = fix_data(sets)

for i, s in enumerate(sets):
    if i % 100 == 99:
        sys.stderr.write('Processing: {:4} / {}\n'.format(i + 1, len(sets)))
    scrape_info(s)

for s in sets:
    for d in s.discs:
        for ss in sets:
            if ss == s:
                continue

            for dd in ss.discs:
                if d == dd:
                    sys.stderr.write('Duplicate disc: {}, present in both {} and {}\n'.format(d, s.title, ss.title))

discs = []

for set in sets:
    for i, disc in enumerate(set.discs):
        if len(set.discs) == 1:
            title = set.title
        else:
            title = '{} (Disc {})'.format(set.title, i + 1)

        discs.append((disc, title, set.standard_controller, set.analog_controller, set.dualshock))

discs = sorted(discs, key=lambda x: x[0])

print('''// This file is part of Highscore. License: GPL-3.0-or-later

// This file is generated, see tools/generate-psx-controller-list.py
namespace Highscore.PlayStation.Overrides {
    public struct GameInputEntry {
        string disc_id;
        ControllerFlags controllers;
    }

    private const GameInputEntry[] GAME_INPUT_DATA = {''')

for disc, title, standard, analog, dualshock in discs:
    flags = []

    if standard and analog:
        continue

    if standard:
        flags.append('DIGITAL')
    if analog:
        flags.append('ANALOG')
    if dualshock:
        flags.append('DUALSHOCK')

    flags_str = ' | '.join(flags)

    print('        {{ "{}", {} }}, // {}'.format(disc, flags_str, title))

print('    };')
print('}')
