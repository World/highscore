// NES PAL composite signal simulation for RetroArch
// shader by r57shell
// thanks to feos & HardWareMan & NewRisingSun

// LICENSE: PUBLIC DOMAIN

// Quality considerations

// there are three main options:
// USE_RAW (R), USE_DELAY_LINE (D), USE_COLORIMETRY (C)
// here is table of quality in decreasing order:
// RDC, RD, RC, DC, D, C

// Source: https://github.com/hizzlekizzle/quark-shaders/tree/master/PAL-r57shell.shader

#define saturate(c) clamp(c, 0.0, 1.0)
#define mul(x,y) (y*x)

// TWEAKS start

// use delay line technique
// without delay line technique, color would interleave
// to avoid this, set HueRotation to zero.
#define USE_DELAY_LINE

// use this if you need to swap even/odd V sign.
// sign of V changes each scanline
// so if some scanline is positive, then next is negative
// and if you want to match picture
// to actual running PAL NES on TV
// you may want to have this option, to change signs
// if they don't match
//#define SWAP_VSIGN

// compensate filter width
// it will make width of picture shorter
// to make picture right border visible
#define COMPENSATE_WIDTH

// NTSC standard gamma = 2.2
// PAL standard gamma = 2.8
// according to many sources, very unlikely gamma of TV is 2.8
// most likely gamma of PAL TV is in range 2.4-2.5

#define Ywidth 12
#define Uwidth 23
#define Vwidth 23

int Mwidth = int(max(float(Ywidth), max(float(Uwidth), float(Vwidth))));

// touch this only if you know what you doing
const float Phase_Y = 2.; // mod(341*10,12)
const float Phase_One = 0.; // alternating phases.
const float Phase_Two = 8.;

// this is using following matrixes.
// it provides more scientific approach
// by conversion into linear XYZ space
// and back to sRGB.
// it's using Gamma setting too.
// define USE_GAMMA is not required.
#define USE_COLORIMETRY

const mat3 RGB_to_XYZ =
mat3(
  0.4306190, 0.3415419, 0.1783091,
  0.2220379, 0.7066384, 0.0713236,
  0.0201853, 0.1295504, 0.9390944
);

const mat3 XYZ_to_sRGB =
mat3(
   3.2406, -1.5372, -0.4986,
  -0.9689,  1.8758,  0.0415,
   0.0557, -0.2040,  1.0570
);

// TWEAKS end

// NTSC standard gamma = 2.2
// PAL standard gamma = 2.8
// according to many sources, very unlikely gamma of TV is 2.8
// most likely gamma of PAL TV is in range 2.4-2.5
uniform float Gamma; // 2.5

uniform float Brightness; // 0.0
uniform float Contrast;   // 1.0
uniform float Saturation; // 1.0
uniform float HueShift;   // -2.5

// rotation of hue due to luma level.
uniform float HueRotation; // 2.0

const float YUV_u = 0.492;
const float YUV_v = 0.877;

const mat3 RGB_to_YUV =
mat3(
  vec3( 0.299, 0.587, 0.114), //Y
  vec3(-0.299,-0.587, 0.886)*YUV_u, //B-Y
  vec3( 0.701,-0.587,-0.114)*YUV_v //R-Y
);

#ifdef USE_DELAY_LINE
const float comb_line = 1.;
#else
const float comb_line = 2.;
#endif

const float RGB_y = 1.0;
const float RGB_u = comb_line/YUV_u;
const float RGB_v = comb_line/YUV_v;

mat3 YUV_to_RGB =
mat3(
  vec3(1., 1., 1.)*RGB_y,
  vec3(0., -0.114/0.587, 1.)*RGB_u,
  vec3(1., -0.299/0.587, 0.)*RGB_v
);

const float pi = 3.1415926535897932384626433832795;

float sinn(float x)
{
  return sin(/*mod(x,24)*/x*(pi*2./24.));
}

float coss(float x)
{
  return cos(/*mod(x,24)*/x*(pi*2./24.));
}

vec3 monitor(vec2 p)
{
   vec2 size = u_sourceSize.xy;
  // align vertical coord to center of texel
  vec2 uv = vec2(
#ifdef COMPENSATE_WIDTH
    p.x+p.x*(float(Ywidth)/(float(Ywidth)-1.0))/size.x,
#else
    p.x,
#endif
    (floor(p.y*u_sourceSize.y)+0.5)/u_sourceSize.y);
#ifdef USE_DELAY_LINE
  vec2 sh = (u_sourceSize.xy/u_sourceSize.xy/size)*vec2(14./10.,-1.0);
#endif
  vec2 pc = uv*u_sourceSize.xy/u_sourceSize.xy*size*vec2(10.,1.);
  float alpha = dot(floor(vec2(pc.x,pc.y)),vec2(2.,Phase_Y*2.));
  alpha += Phase_One*2.;

  // 1/size.x of screen in uv coords = u_sourceSize.x/u_sourceSize.x/size.x;
  // then 1/10*size.x of screen:
  float ustep = u_sourceSize.x/u_sourceSize.x/size.x/10.;

  float border = u_sourceSize.x/u_sourceSize.x;
  float ss = 2.0;
#ifdef SWAP_VSIGN
#define PAL_SWITCH(A) A < 1.
#else
#define PAL_SWITCH(A) A > 1.
#endif
  if (PAL_SWITCH(mod(uv.y*u_sourceSize.y/u_sourceSize.y*size.y,2.0)))
  {
    // cos(pi-alpha) = -cos(alpha)
    // sin(pi-alpha) = sin(alpha)
    // pi - alpha
    alpha = -alpha+12012.0;
    ss = -2.0;
  }

  float ysum = 0., usum = 0., vsum = 0.;
  for (int i=0; i<Mwidth; ++i)
  {
    vec4 res = texture(u_source, uv);
      vec3 yuv = mul(RGB_to_YUV, res.xyz);
    float a1 = alpha+(HueShift+2.5)*2.-yuv.x*ss*HueRotation;
    float sig = yuv.x+dot(yuv.yz,sign(vec2(sinn(a1),coss(a1))));
#ifdef USE_DELAY_LINE
    vec4 res1 = texture(u_source, uv+sh);
    vec3 yuv1 = mul(RGB_to_YUV, res1.xyz);
    float a2 = (HueShift+2.5)*2.+12012.0-alpha+yuv.x*ss*HueRotation;
    float sig1 = yuv1.x+dot(yuv1.yz,sign(vec2(sinn(a2),coss(a2))));
#endif
    if (i < Ywidth)
      ysum += sig;

#ifdef USE_DELAY_LINE
    if (i < Uwidth)
      usum += (sig+sig1)*sinn(alpha);
    if (i < Vwidth)
      vsum += (sig-sig1)*coss(alpha);
#else
    if (i < Uwidth)
      usum += sig*sinn(alpha);
    if (i < Vwidth)
      vsum += sig*coss(alpha);
#endif
    alpha -= ss;
    uv.x -= ustep;
  }

  ysum *= Contrast/float(Ywidth);
  usum *= Contrast*Saturation/float(Uwidth);
  vsum *= Contrast*Saturation/float(Vwidth);

  vec3 rgb = mul(vec3(ysum+Brightness*float(Ywidth),usum,vsum), YUV_to_RGB);

#ifdef USE_COLORIMETRY
  vec3 rgb1 = saturate(rgb);
  rgb = pow(rgb1, vec3(Gamma, Gamma, Gamma));
#endif

#ifdef USE_COLORIMETRY
  vec3 xyz1 = mul(RGB_to_XYZ,rgb);
  vec3 srgb = saturate(mul(XYZ_to_sRGB,xyz1));
  vec3 a1 = 12.92*srgb;
  vec3 a2 = 1.055*pow(srgb,vec3(1./2.4))-0.055;
  vec3 ssrgb;
   ssrgb.x = (srgb.x<0.0031308?a1.x:a2.x);
   ssrgb.y = (srgb.y<0.0031308?a1.y:a2.y);
   ssrgb.z = (srgb.z<0.0031308?a1.z:a2.z);
  return ssrgb;
#else
  return rgb;
#endif
}

vec4 hs_main() {
  return vec4(monitor(v_texCoord), 1.);
}
