// Monochrome LCD shader
// Author: Alice Mikhaylenko
// This shader emulates a monochrome LCD screen
// License: GPL-3.0-or-later

// Converts input to monochrome and adjusts gamma to work on a monochrome LCD

// Input may be monochrome or colored, the shader will convert it to monochrome
// according to the NTSC luminance formula. You can specify the minimum and
// maximum luminance that everything will get mapped into. This can be useful
// when the input already has a palette applied to it that needs to be undone
// first.
uniform float MinInputLuminance;
uniform float MaxInputLuminance;
uniform float Gamma;

vec4 hs_main() {
  vec3 color = texture(u_source, v_texCoord).rgb;

  float luminance = 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;

  luminance = clamp(luminance, MinInputLuminance, MaxInputLuminance);
  luminance = (luminance - MinInputLuminance) / (MaxInputLuminance - MinInputLuminance);

  float opacity = 1.0 - luminance;
  opacity = pow(opacity, 1.0 / Gamma);

  return vec4(opacity, opacity, opacity, 1.0);
}