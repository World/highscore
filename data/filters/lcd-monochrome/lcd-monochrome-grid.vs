// Monochrome LCD shader
// Author: Alice Mikhaylenko
// This shader emulates a monochrome LCD screen
// License: GPL-3.0-or-later

out vec2 integerSize;
out vec2 pixelSize;
out float scaleFactor;

void hs_main() {
  scaleFactor = max(floor((u_outputSize.y + 1.0) / u_passOutputDecolorSize.y), 1.0);

  integerSize = u_passOutputDecolorSize.xy * scaleFactor;
  pixelSize = vec2(scaleFactor);

  gl_Position = u_mvp * vec4((position * integerSize - vec2(1.0)) * u_outputSize.zw, 0, 1);
}