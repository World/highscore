// Monochrome LCD shader
// Author: Alice Mikhaylenko
// This shader emulates a monochrome LCD screen
// License: GPL-3.0-or-later

// Takes the image with the grid and upscales it to the actual size, replaces
// the transient colors with the final ones and adds a shadow

in vec2 integerSize;

uniform vec3 BackgroundColor;
uniform vec3 ForegroundColor;
uniform float OutputGamma;
uniform float MinimumOpacity;
uniform float ShadowStrength;
uniform vec2 ShadowOffset;

vec4 hs_main() {
  // Not a color, red and green channel are opacities
  vec4 inputData = texture(u_passOutputGrid, v_texCoord * u_outputSize.zw * integerSize);

  float pixelOpacity = min(inputData.r, 1.0);
  float gridOpacity = inputData.g;
  float shadowOpacity = texture(u_passOutputShadow, v_texCoord - ShadowOffset * u_passOutputShadowSize.zw).r;

  pixelOpacity = pow(pixelOpacity, OutputGamma);
  pixelOpacity = mix(MinimumOpacity, 1.0, pixelOpacity);

  vec3 color = mix(BackgroundColor, vec3(0.0), shadowOpacity * ShadowStrength);
  color = mix(color, ForegroundColor, pixelOpacity * (1.0 - gridOpacity));

  return vec4(color, 1.0);
}