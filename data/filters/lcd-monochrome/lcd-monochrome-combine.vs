// Monochrome LCD shader
// Author: Alice Mikhaylenko
// This shader emulates a monochrome LCD screen
// License: GPL-3.0-or-later

out vec2 integerSize;

void hs_main() {
  float scaleFactor = max(floor((u_outputSize.y + 1.0) / u_passOutputDecolorSize.y), 1.0);

  integerSize = u_passOutputDecolorSize.xy * scaleFactor - vec2(1.0);
}