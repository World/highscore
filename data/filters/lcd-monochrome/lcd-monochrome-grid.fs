// Monochrome LCD shader
// Author: Alice Mikhaylenko
// This shader emulates a monochrome LCD screen
// License: GPL-3.0-or-later

// Apply grid to the decolor pass output

in vec2 integerSize;
in vec2 pixelSize;
in float scaleFactor;

uniform float GridStrength;

#define FULL_SIZE 4.0

vec4 hs_main() {
  vec2 pos = v_texCoord * integerSize;
  vec2 posWithinPixel = mod(v_texCoord * integerSize, pixelSize);
  vec2 pixelCenter = floor(pos) + vec2(0.5) - 1.0;

  float pixelOpacity = texture(u_passOutputDecolor, pixelCenter / integerSize).r;
  float gridOpacity = 0.0;

  if (floor(posWithinPixel.x) < 0.5 || floor(posWithinPixel.y) < 0.5) {
    // When the output is small the lines will get thicker; compensate for that
    float gridMultiplier = min(scaleFactor / FULL_SIZE, 1.0);

    gridOpacity = mix(0.0, GridStrength, gridMultiplier);
  }

  return vec4(pixelOpacity, gridOpacity, 0.0, 0.0);
}