uniform vec3 RedSubpixel;   // vec3(1.0, 0.0, 0.0)
uniform vec3 GreenSubpixel; // vec3(0.0, 1.0, 0.0)
uniform vec3 BlueSubpixel;  // vec3(0.0, 0.0, 1.0)
uniform float Gain;         // 1.0
uniform float Gamma;        // 3.0
uniform float OutGamma;     // 2.2
uniform float BlackLevel;   // 0.05
uniform float Ambient;      // 0.0
uniform bool BGR;           // false

#define fetch_offset(coord) (pow(vec3(Gain) * get_texel(coord).rgb + vec3(BlackLevel), vec3(Gamma)) + vec3(Ambient))

vec4 get_texel(vec2 coord)
{
    return texture(u_source, (coord + vec2(0.5, 0.5)) * u_sourceSize.zw);
}

float intsmear_func(float z, float coeffs[7])
{
    float z2 = z*z;
    float zn = z;
    float ret = 0.0;
    for (int i = 0; i < 7; i++) {
        ret += zn*coeffs[i];
        zn *= z2;
    }
    return ret;
}

float intsmear(float x, float dx, float d, float coeffs[7])
{
    float zl = clamp((x-dx*0.5)/d,-1.0,1.0);
    float zh = clamp((x+dx*0.5)/d,-1.0,1.0);
    return d * ( intsmear_func(zh,coeffs) - intsmear_func(zl,coeffs) )/dx;
}

vec4 hs_main()
{
    // integral of (1 - x^2 - x^4 + x^6)^2
    float coeffs_x[7];
    coeffs_x[0] = 1.0;
    coeffs_x[1] = -2.0/3.0;
    coeffs_x[2] = -1.0/5.0;
    coeffs_x[3] = 4.0/7.0;
    coeffs_x[4] = -1.0/9.0;
    coeffs_x[5] = -2.0/11.0;
    coeffs_x[6] = 1.0/13.0;

    // integral of (1 - 2x^4 + x^6)^2
    float coeffs_y[7];
    coeffs_y[0] = 1.0;
    coeffs_y[1] = 0.0;
    coeffs_y[2] = -4.0/5.0;
    coeffs_y[3] = 2.0/7.0;
    coeffs_y[4] = 4.0/9.0;
    coeffs_y[5] = -4.0/11.0;
    coeffs_y[6] = 1.0/13.0;

    vec2 texelSize = 1.0 / u_sourceSize.xy;
    vec2 range = u_sourceSize.xy / (u_outputSize.xy * u_sourceSize.xy);

    vec3 cred   = pow(RedSubpixel, vec3(OutGamma));
    vec3 cgreen = pow(GreenSubpixel, vec3(OutGamma));
    vec3 cblue  = pow(BlueSubpixel, vec3(OutGamma));

    vec2 tli = vec2(floor(v_texCoord/texelSize-vec2(0.4999)));

    vec3 lcol, rcol;
    float subpix = (v_texCoord.x/texelSize.x - 0.4999 - tli.x)*3.0;
    float rsubpix = range.x/texelSize.x * 3.0;

    lcol = vec3(intsmear(subpix+1.0, rsubpix, 1.5, coeffs_x),
                intsmear(subpix    , rsubpix, 1.5, coeffs_x),
                intsmear(subpix-1.0, rsubpix, 1.5, coeffs_x));
    rcol = vec3(intsmear(subpix-2.0, rsubpix, 1.5, coeffs_x),
                intsmear(subpix-3.0, rsubpix, 1.5, coeffs_x),
                intsmear(subpix-4.0, rsubpix, 1.5, coeffs_x));

    if (BGR) {
        lcol.rgb = lcol.bgr;
        rcol.rgb = rcol.bgr;
    }

    float tcol, bcol;
    subpix = v_texCoord.y/texelSize.y - 0.4999 - tli.y;
    rsubpix = range.y/texelSize.y;
    tcol = intsmear(subpix    ,rsubpix, 0.63, coeffs_y);
    bcol = intsmear(subpix-1.0,rsubpix, 0.63, coeffs_y);

    vec3 topLeftColor     = fetch_offset(tli + vec2(0,0)) * lcol * vec3(tcol);
    vec3 bottomRightColor = fetch_offset(tli + vec2(1,1)) * rcol * vec3(bcol);
    vec3 bottomLeftColor  = fetch_offset(tli + vec2(0,1)) * lcol * vec3(bcol);
    vec3 topRightColor    = fetch_offset(tli + vec2(1,0)) * rcol * vec3(tcol);

    vec3 averageColor = topLeftColor + bottomRightColor + bottomLeftColor + topRightColor;

    averageColor = mat3(cred, cgreen, cblue) * averageColor;

    return vec4(pow(averageColor, vec3(1.0/OutGamma)),0.0);
}
