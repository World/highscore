uniform float BlendStrength;

float snap_to_scanline(float y, float height, float offset) {
  return (floor(y * 2.0 * height + 0.5) + offset) / height;
}

vec4 hs_main() {
  if (u_originalSize.y > 360.0) {
    vec2 uv = v_texCoord;
    float offset = uv.y > 0.5 ? 0.0 : 1.0;

    uv.y = snap_to_scanline(uv.y, u_sourceSize.y, offset);

    return texture(u_source, uv);
  }

  return texture(u_source, v_texCoord);
}