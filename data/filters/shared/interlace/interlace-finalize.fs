uniform float BlendStrength;

vec4 process_field(float multiplier, float offset, float scanlineOffset) {
  vec2 pos = v_texCoord - vec2(0.0, scanlineOffset * u_sourceSize.w);

  pos.y = max(0.0, min(pos.y * multiplier + offset, 1.0));

  return texture(u_source, pos);
}

vec4 hs_main() {
  if (u_originalSize.y > 360.0) {
    vec4 field1 = process_field(0.5, u_phase == 0 ? 0.5 : 0.0, u_phase == 0 ? 0.0 : 1.0);
    vec4 field2 = process_field(0.5, u_phase == 0 ? 0.0 : 0.5, u_phase == 0 ? 1.0 : 0.0);

    return mix(field1, field2, BlendStrength * 0.5);
  }

  return texture(u_source, v_texCoord);
}