/*
    Response Time
    Based on the response time function from Harlequin's Game Boy and LCD shaders
 
    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.
*/

uniform float ResponseTime; // 0.333, goes from 0.0 to 1.0

// Frame sampling definitions
#define curr_rgb  texture(u_source,   v_texCoord).rgb
#define prev0_rgb texture(u_history1, v_texCoord).rgb
#define prev1_rgb texture(u_history2, v_texCoord).rgb
#define prev2_rgb texture(u_history3, v_texCoord).rgb
#define prev3_rgb texture(u_history4, v_texCoord).rgb
#define prev4_rgb texture(u_history5, v_texCoord).rgb
#define prev5_rgb texture(u_history6, v_texCoord).rgb
#define prev6_rgb texture(u_history7, v_texCoord).rgb
#define prev7_rgb texture(u_history8, v_texCoord).rgb
#define prev8_rgb texture(u_history9, v_texCoord).rgb
#define prev9_rgb texture(u_history10, v_texCoord).rgb
#define prev10_rgb texture(u_history11, v_texCoord).rgb
#define prev11_rgb texture(u_history12, v_texCoord).rgb
#define prev12_rgb texture(u_history13, v_texCoord).rgb
#define prev13_rgb texture(u_history14, v_texCoord).rgb
#define prev14_rgb texture(u_history15, v_texCoord).rgb

vec4 hs_main()
{
    // Sample color from the current and previous frames, apply response time modifier
    // Response time effect implmented through an exponential dropoff algorithm
    vec3 input_rgb = curr_rgb;
    input_rgb += (prev0_rgb - input_rgb) * ResponseTime;
    input_rgb += (prev1_rgb - input_rgb) * pow(ResponseTime, 2.0);
    input_rgb += (prev2_rgb - input_rgb) * pow(ResponseTime, 3.0);
    input_rgb += (prev3_rgb - input_rgb) * pow(ResponseTime, 4.0);
    input_rgb += (prev4_rgb - input_rgb) * pow(ResponseTime, 5.0);
    input_rgb += (prev5_rgb - input_rgb) * pow(ResponseTime, 6.0);
    input_rgb += (prev6_rgb - input_rgb) * pow(ResponseTime, 7.0);
    input_rgb += (prev7_rgb - input_rgb) * pow(ResponseTime, 8.0);
    input_rgb += (prev8_rgb - input_rgb) * pow(ResponseTime, 9.0);
    input_rgb += (prev9_rgb - input_rgb) * pow(ResponseTime, 10.0);
    input_rgb += (prev10_rgb - input_rgb) * pow(ResponseTime, 11.0);
    input_rgb += (prev11_rgb - input_rgb) * pow(ResponseTime, 12.0);
    input_rgb += (prev12_rgb - input_rgb) * pow(ResponseTime, 13.0);
    input_rgb += (prev13_rgb - input_rgb) * pow(ResponseTime, 14.0);
    input_rgb += (prev14_rgb - input_rgb) * pow(ResponseTime, 15.0);

    return vec4(input_rgb, 0.0);
}
