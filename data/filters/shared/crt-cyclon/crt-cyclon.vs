uniform float TVAspectRatio;

out vec2 letterboxingScale;

void hs_main() {
  v_texCoord *= 1.0001;

  if (u_aspectRatio > TVAspectRatio)
    letterboxingScale.y = TVAspectRatio / u_aspectRatio;
  else
    letterboxingScale.x = u_aspectRatio / TVAspectRatio;
}