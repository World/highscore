/*

DariusG presents

'crt-Cyclon'

Why? Because it's speedy!

A super-fast shader based on the magnificent crt-Geom, optimized for full speed
on a Xiaomi Note 3 Pro cellphone (around 170(?) gflops gpu or so)

This shader uses parts from:
crt-Geom (scanlines)
Quillez (main filter)
Grade (some primaries)
Dogway's inverse Gamma
Masks-slot-color handling, tricks etc are mine.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

*/

/* Modified to remove the bezel since we don't support external textures */

uniform float ScanlineWeight;       // 0.3, goes from 0.2 to 0.6

// Mask
uniform int MaskType;                // -1: None, 0: CGWG, 1: RGB
uniform float MaskSize;              // 1.0, goes from 1.0 to 2.0
uniform bool SlotMask;               // Enable slot mask, default: true
uniform float SlotMaskWidth;         // 3.0, goes between 2.0 and 3.0
uniform bool SubpixelBGR;            // Swap subpixel order, default: fa;se
uniform float MaskBrightnessDark;    // 0.3, goes from 0.0 to 1.0
uniform float MaskBrightnessBright;  // 0.75, goes from 0.0 to 1.0

// Geometry
uniform vec2 Zoom;                   // vec2(0.0, 0.0), goes from -1.0 to 1.0
uniform vec2 Center;                 // vec2(0.0, 0.0), goes from -5.0 to 5.0
uniform vec2 Warp;                   // vec2(0.02, 0.01), goes from 0.0 to 0.25
uniform bool Vignette;               // Enable vignette, default: true

// Color
uniform float ScanMaskBrightnessDep; // 0.2, goes between 0.0 and 0.333
uniform int ColorSpace;              // 0 - sRGB, 1 - PAL, 2 - NTSC-U, 3 - NTSC-J
uniform bool ExternalGamma;          // default: false
uniform float Saturation;            // 1.0, goes from 0.0 to 2.0
uniform float Brightness;            // 1.0, Sega fix: 1.06, goes from 0.0 to 2.0
uniform float BlackLevel;            // 0.0, goes from -0.2 to 0.2
uniform vec3 HueAdjustment;          // vec3(0.0, 0.0, 0.0), G<->R, B<->R, B<->G, goes from -0.25 to 0.25

// Convergence
uniform float ConvergenceStrength;   // 0.0, goes from 0.0 to 0.5
uniform vec3 Convergence;            // vec3(0.0, 0.0, 0.0), RGB components, goes from -1.0 to 1.0
uniform bool PotatoBoost;            // Simple gamma, adjust mask, default: false

uniform float BlendStrength;

in vec2 letterboxingScale;

#define pi 3.1415926535897932384626433

#define blck (1.0)/(1.0-BlackLevel);


vec3 Mask(vec2 pos, float CGWG) {
  vec3 mask = vec3(CGWG);

  if (MaskType == 0) {
    if (PotatoBoost)
      return vec3( (1.0-CGWG)*sin(pos.x*pi)+CGWG);
    else {
      float m = fract(pos.x*0.5);

      if (m<0.5) mask.rb = vec2(1.0);
      else mask.g = 1.0;

      return mask;
    }
  }

  if (MaskType == 1){
    if (PotatoBoost)
      return vec3( (1.0-CGWG)*sin(pos.x*pi*0.6667)+CGWG);
    else {
      float m = fract(pos.x*0.3333);

      if (m<0.3333) mask.rgb = (!SubpixelBGR) ? vec3(mask.r, mask.g, 1.0) : vec3(1.0, mask.g, mask.b);
      else if (m<0.6666)         mask.g = 1.0;
      else          mask.rgb = (!SubpixelBGR) ? vec3(1.0, mask.g, mask.b) : vec3(mask.r, mask.g, 1.0);
      return mask;
    }
  }

  else return vec3(1.0);
}

float scanlineWeights(float distance, vec3 color, float x) {
  // "wid" controls the width of the scanline beam, for each RGB
  // channel The "weights" lines basically specify the formula
  // that gives you the profile of the beam, i.e. the intensity as
  // a function of distance from the vertical center of the
  // scanline. In this case, it is gaussian if width=2, and
  // becomes nongaussian for larger widths. Ideally this should
  // be normalized so that the integral across the beam is
  // independent of its width. That is, for a narrower beam
  // "weights" should have a higher peak at the center of the
  // scanline than for a wider beam.
  float wid = ScanlineWeight + 0.15 * dot(color, vec3(0.25-0.8*x));   //0.8 vignette strength
  float weights = distance / wid;
  return 0.4 * exp(-weights * weights ) / wid;
}

#define pwr vec3(1.0/((-1.0*ScanlineWeight+1.0)*(-0.8*CGWG+1.0))-1.2)
// Returns gamma corrected output, compensated for scanline+mask embedded gamma
vec3 inv_gamma(vec3 col, vec3 power) {
  vec3 cir  = col-1.0;
       cir *= cir;
       col  = mix(sqrt(col),sqrt(1.0-cir),power);
  return col;
}

// standard 6500k
mat3 PAL = mat3(
1.0740  ,   -0.0574 ,   -0.0119 ,
0.0384  ,   0.9699  ,   -0.0059 ,
-0.0079 ,   0.0204  ,   0.9884  );

// standard 6500k
mat3 NTSC = mat3(
0.9318  ,   0.0412  ,   0.0217  ,
0.0135  ,   0.9711  ,   0.0148  ,
0.0055  ,   -0.0143 ,   1.0085  );

// standard 8500k
mat3 NTSC_J = mat3(
0.9501  ,   -0.0431 ,   0.0857  ,
0.0265  ,   0.9278  ,   0.0432  ,
0.0011  ,   -0.0206 ,   1.3153  );

vec3 slot(vec2 pos) {
  float h = fract(pos.x/SlotMaskWidth);
  float v = fract(pos.y);

  float odd;
  if (v<0.5) odd = 0.0; else odd = 1.0;

  if (odd == 0.0) {
    if (h<0.5) return vec3(0.5);
    else return vec3(1.5);
  } else if (odd == 1.0) {
    if (h<0.5) return vec3(1.5);
    else return vec3(0.5);
  }
}

vec2 warp(vec2 pos) {
  pos = pos*2.0-1.0;
  pos *= letterboxingScale;
  pos *= vec2(1.0+pos.y*pos.y*Warp.x, 1.0+pos.x*pos.x*Warp.y);
  pos /= letterboxingScale;
  pos = pos*0.5+0.5;

  return pos;
}

vec3 process_field(float multiplier, float offset, float scanlineOffset) {
  vec2 pos = warp(v_texCoord - vec2(0.0, scanlineOffset * u_originalSize.w));
  float bounds = 1.0;

  float scanline = u_originalSize.w * multiplier;
  bounds = max(0.0, min(1.0, min (pos.y, 1.0 - pos.y) / scanline + 0.5));

  pos.y = max(0.0, min(pos.y * multiplier + offset, 1.0));

  vec2 bpos = pos;
  vec2 ps = u_originalSize.zw;
  vec2 dx = vec2(ps.x,0.0);

  // Quilez
  vec2 ogl2 = pos*u_originalSize.xy;
  vec2 i = floor(pos*u_originalSize.xy) + 0.5;
  float f = ogl2.y - i.y;
  pos.y = (i.y + 4.0*f*f*f)*ps.y; // smooth

  // Vignette
  float x = 0.0;
  if (Vignette) {
    x = v_texCoord.x-0.5;
    x = x*x;
  }

  // Convergence
  vec3  res0 = texture(u_source,pos).rgb;
  float resr = texture(u_source,pos + dx*Convergence.r).r;
  float resb = texture(u_source,pos + dx*Convergence.b).b;
  float resg = texture(u_source,pos + dx*Convergence.g).g;

  vec3 res = vec3(  res0.r*(1.0-ConvergenceStrength) +  resr*ConvergenceStrength,
                    res0.g*(1.0-ConvergenceStrength) +  resg*ConvergenceStrength,
                    res0.b*(1.0-ConvergenceStrength) +  resb*ConvergenceStrength
                 );

  res = mix(vec3(0.0), res, vec3(bounds));

  float s = fract(bpos.y*u_originalSize.y-0.5);
  // Calculate CRT-Geom scanlines weight and apply
  float weight  = scanlineWeights(s, res, x);
  float weight2 = scanlineWeights(1.0-s, res, x);
  res *= weight + weight2;

  return res;
}

vec4 hs_main() {
  // Hue matrix inside main() to avoid GLES error
  float RG = HueAdjustment.x;
  float RB = HueAdjustment.y;
  float GB = HueAdjustment.z;
  mat3 hue = mat3(
      1.0, -RG, -RB,
      RG, 1.0, -GB,
      RB, GB, 1.0
  );

  vec3 res;
  if (u_originalSize.y > 360.0) {
    vec3 res0 = process_field(0.5, u_phase == 0 ? 0.5 : 0.0, u_phase == 0 ? 0.0 : 1.0);
    vec3 res1 = process_field(0.5, u_phase == 0 ? 0.0 : 0.5, u_phase == 0 ? 1.0 : 0.0);

    res = mix(res0, res1, BlendStrength * 0.5);
  } else {
    res = process_field(1.0, 0.0, 0.0);
  }

  float l = dot(vec3(ScanMaskBrightnessDep),res);

  // Color Spaces
  if(!ExternalGamma) res *= res;
  if (ColorSpace != 0) {
    if (ColorSpace == 1) res *= PAL;
    if (ColorSpace == 2) res *= NTSC;
    if (ColorSpace == 3) res *= NTSC_J;

    // Apply CRT-like luminances
    res /= vec3(0.24,0.69,0.07);
    res *= vec3(0.29,0.6,0.11);
    res = clamp(res,0.0,1.0);
  }

  // Masks
  vec2 xy = v_texCoord*u_outputSize.xy/MaskSize;
  float CGWG = mix(MaskBrightnessDark, MaskBrightnessBright, l);
  res *= Mask(xy, CGWG);

  // Apply slot mask on top of Trinitron-like mask
  if (SlotMask) res *= mix(slot(xy/2.0),vec3(1.0),CGWG);

  if (!PotatoBoost) res = inv_gamma(res,pwr);
  else {res = sqrt(res); res *= mix(1.3,1.1,l);}

  // Saturation
  float lum = dot(vec3(0.29,0.60,0.11),res);
  res = mix(vec3(lum),res,Saturation);

  // Brightness, Hue and Black Level
  res *= Brightness;
  res *= hue;
  res -= vec3(BlackLevel);
  res *= blck;

  return vec4(res,1.0);
}