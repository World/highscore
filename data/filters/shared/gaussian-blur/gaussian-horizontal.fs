/*
   Gaussian blur - horizontal pass, dynamic range, resizable

   Copyright (C) 2020 guest(r) - guest.r@gmail.com

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

uniform float Size; // 5.0
uniform float Sigma; // 1.0

#define invsqrsigma (1.0/(2.0*Sigma*Sigma))

float gaussian(float x) {
  return exp(-x*x*invsqrsigma);
}

vec4 hs_main() {
  float f = fract(u_sourceSize.x * v_texCoord.x);
  f = 0.5 - f;
  vec2 tex = (floor(u_sourceSize.xy * v_texCoord) + 0.5) * u_sourceSize.zw;
  vec3 color = vec3(0.0);
  vec2 dx  = vec2(1.0 / u_sourceSize.x, 0.0);

  float w;
  float wsum = 0.0;
  vec3 pixel;
  float n = -Size;

  do {
    pixel  = texture(u_source, tex + n*dx).rgb;
    w      = gaussian(n+f);
    color  = color + w * pixel;
    wsum   = wsum + w;
    n = n + 1.0;
  } while (n <= Size);

  color = color / wsum;

  return vec4(color, 1.0);
}