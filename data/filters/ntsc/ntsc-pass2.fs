// NTSC-Adaptive
// based on Themaister's NTSC shader

uniform float AntiRinging; // 0.5, goes from 0.0 to 1.0
uniform float Gamma;       // 1.0, goes from 0.25 to 2.5

in float auto_res;

#define PI 3.14159265

// For performance reasons, the luma and chroma filter tap values are precomputed
// Set FILTER_DEBUG to 1 to compute them dynamically, then they can be tweaked.
// Once done, adjust tools/generate-ntsc-filters.py, run it, copy the output
// into the shader and set it back to 0
#define FILTER_DEBUG 0

#define TAPS 24
#define CHROMA_TAPS 12

vec3 fetch_offset(vec3 dx) {
  return vec3(texture(u_source, v_texCoord + dx.xz).x +
              texture(u_source, v_texCoord - dx.xz).x,
              texture(u_source, v_texCoord + dx.yz).yz +
              texture(u_source, v_texCoord - dx.yz).yz);
}

const mat3 yiq2rgb_mat = mat3(
  1.0, 0.956, 0.6210,
  1.0, -0.2720, -0.6474,
  1.0, -1.1060, 1.7046
);

vec3 yiq2rgb(vec3 yiq) {
  return yiq * yiq2rgb_mat;
}

#if FILTER_DEBUG
// Lanczos kernel
float luma_tap(int i, int taps, float a, float b) {
  float x = float(taps - i) * b;

  if (i == taps)
    return 1.0;

  return a * sin(PI*x) * sin(PI*x/a) / (PI*PI*x*x);
}

// Blackman window
float chroma_tap(int i, int taps) {
  float x = float(i + taps - TAPS) / float(taps * 2);

  if (i + taps - TAPS < 0)
    return 0.0;

  return 0.42659 - 0.49656 * cos(2.0 * PI * x) + 0.076849 * cos(4.0 * PI * x);
}
#endif

vec4 hs_main() {
  float luma_filter[TAPS + 1];
  float chroma_filter[TAPS + 1];

  if (u_originalSize.x * auto_res > 288.0) {
#if FILTER_DEBUG
    int i;
    for (i = 0; i <= TAPS; i++)
      luma_filter[i] = luma_tap(i, TAPS, 2.75, 0.155);
#else
    luma_filter[0] = 0.013883;
    luma_filter[1] = 0.017223;
    luma_filter[2] = 0.015752;
    luma_filter[3] = 0.010301;
    luma_filter[4] = 0.003488;
    luma_filter[5] = -0.001220;
    luma_filter[6] = -0.001002;
    luma_filter[7] = 0.004791;
    luma_filter[8] = 0.013726;
    luma_filter[9] = 0.020510;
    luma_filter[10] = 0.018529;
    luma_filter[11] = 0.002406;
    luma_filter[12] = -0.029162;
    luma_filter[13] = -0.071266;
    luma_filter[14] = -0.112267;
    luma_filter[15] = -0.135426;
    luma_filter[16] = -0.122577;
    luma_filter[17] = -0.059059;
    luma_filter[18] = 0.061389;
    luma_filter[19] = 0.233235;
    luma_filter[20] = 0.438427;
    luma_filter[21] = 0.648852;
    luma_filter[22] = 0.831611;
    luma_filter[23] = 0.955933;
    luma_filter[24] = 1.000000;
#endif
  } else {
#if FILTER_DEBUG
    int i;
    for (i = 0; i <= TAPS; i++)
      luma_filter[i] = luma_tap(i, TAPS, 2.75, 0.155 / 256.0 * 320.0);
#else
    luma_filter[0] = -0.009478;
    luma_filter[1] = -0.012916;
    luma_filter[2] = -0.011123;
    luma_filter[3] = -0.003600;
    luma_filter[4] = 0.006814;
    luma_filter[5] = 0.015139;
    luma_filter[6] = 0.017084;
    luma_filter[7] = 0.011918;
    luma_filter[8] = 0.003488;
    luma_filter[9] = -0.001700;
    luma_filter[10] = 0.001274;
    luma_filter[11] = 0.011448;
    luma_filter[12] = 0.020510;
    luma_filter[13] = 0.015965;
    luma_filter[14] = -0.011592;
    luma_filter[15] = -0.060281;
    luma_filter[16] = -0.112267;
    luma_filter[17] = -0.136262;
    luma_filter[18] = -0.097790;
    luma_filter[19] = 0.026023;
    luma_filter[20] = 0.233235;
    luma_filter[21] = 0.491796;
    luma_filter[22] = 0.745747;
    luma_filter[23] = 0.931733;
    luma_filter[24] = 1.000000;
#endif
  }

#if FILTER_DEBUG
  {
    int i;
    for (i = 0; i <= TAPS; i++)
      chroma_filter[i] = chroma_tap(i, CHROMA_TAPS);
  }
#else
  chroma_filter[0] =  0.000000;
  chroma_filter[1] =  0.000000;
  chroma_filter[2] =  0.000000;
  chroma_filter[3] =  0.000000;
  chroma_filter[4] =  0.000000;
  chroma_filter[5] =  0.000000;
  chroma_filter[6] =  0.000000;
  chroma_filter[7] =  0.000000;
  chroma_filter[8] =  0.000000;
  chroma_filter[9] =  0.000000;
  chroma_filter[10] =  0.000000;
  chroma_filter[11] =  0.000000;
  chroma_filter[12] =  0.006879;
  chroma_filter[13] =  0.013503;
  chroma_filter[14] =  0.034981;
  chroma_filter[15] =  0.075469;
  chroma_filter[16] =  0.139885;
  chroma_filter[17] =  0.231518;
  chroma_filter[18] =  0.349741;
  chroma_filter[19] =  0.488556;
  chroma_filter[20] =  0.636445;
  chroma_filter[21] =  0.777711;
  chroma_filter[22] =  0.895048;
  chroma_filter[23] =  0.972783;
  chroma_filter[24] =  0.999999;
#endif

  vec2 one_x = 0.25*u_originalSize.zz / auto_res;
  vec3 signal = vec3(0.0);

  int i = 0;
  vec3 wsum = vec3(0.0);
  vec3 tmp  = wsum;

  vec3 dx = vec3(one_x.x, one_x.y, 0.0);
  vec3 dx1 = dx;

  for (i = 0; i < TAPS; i++) {
    dx1.xy = float(i - TAPS)*dx.xy;
    tmp = vec3(luma_filter[i], vec2(chroma_filter[i]));
    wsum = wsum + tmp;
    signal += fetch_offset(dx1) * tmp;
  }

  tmp = vec3(luma_filter[TAPS], vec2(chroma_filter[TAPS]));
  wsum = wsum + wsum + tmp;
  signal += texture(u_source, v_texCoord).xyz * tmp;
  signal = signal / wsum;

  if (AntiRinging > 0.05) {
    vec2 dx = vec2(u_originalSize.z / min(auto_res, 1.0), 0.0);
    float a = texture(u_source, v_texCoord - 1.5*dx).a;
    float b = texture(u_source, v_texCoord - 0.5*dx).a;
    float c = texture(u_source, v_texCoord + 1.5*dx).a;
    float d = texture(u_source, v_texCoord + 0.5*dx).a;
    float e = texture(u_source, v_texCoord         ).a;
    signal.x = mix(signal.x, clamp(signal.x, min(min(min(a,b),min(c,d)),e), max(max(max(a,b),max(c,d)),e)), AntiRinging);
  }

  signal.x = clamp(signal.x, 0.0, 1.0);
  signal.x = pow(signal.x, 1.0/Gamma);
  signal.x = clamp(signal.x, -1.0, 1.0);

  vec3 rgb = yiq2rgb(signal);

  return vec4(rgb, 1.0);
}