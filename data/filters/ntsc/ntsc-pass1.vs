// NTSC-Adaptive
// based on Themaister's NTSC shader

uniform float Saturation;  // 1.0
uniform float Brightness;  // 1.0
uniform float Artifacting; // 0.0 for S-Video, 1.0 for composite, 2.0 for RF
uniform float Fringing;    // 0.0 for S-Video, 1.0 for composite, 2.0 for RF

out mat3 mix_mat;
out float auto_res;
out vec2 pix_no;

void hs_main() {
  if (u_originalSize.x < 200.0)
    auto_res = 2.0;
  else if (u_originalSize.x > 450.0)
    auto_res = 0.5;
  else
    auto_res = 1.0;

  float res = min(auto_res, 1.0);
  vec2 scale = u_outputSize.xy * u_sourceSize.zw;

  pix_no = v_texCoord * u_originalSize.xy * vec2(res, 1.0) * scale;

  mix_mat = mat3(
    Brightness, Fringing, Fringing,
    Artifacting, 2.0 * Saturation, 0.0,
    Artifacting, 0.0, 2.0 * Saturation
  );
}