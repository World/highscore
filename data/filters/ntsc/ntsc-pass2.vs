// NTSC-Adaptive
// based on Themaister's NTSC shader

out float auto_res;

void hs_main() {
  if (u_originalSize.x < 200.0)
    auto_res = 2.0;
  else if (u_originalSize.x > 450.0)
    auto_res = 0.5;
  else
    auto_res = 1.0;

  v_texCoord = texCoord - vec2(0.25 * u_sourceSize.z/auto_res, 0.0); // Compensate for decimate-by-2.
}