// NTSC-Adaptive
// based on Themaister's NTSC shader

uniform float Gamma;   // 1.0
uniform int Mode;

#define PI 3.14159265

#define MODE_ATARI 1
#define MODE_SEGA_8BIT 2
#define MODE_MEGA_DRIVE 3
#define MODE_SEGA_SATURN 4
#define MODE_NES 5
#define MODE_SUPER_NES 6
#define MODE_OTHER 7

in mat3 mix_mat;
in float auto_res;
in vec2 pix_no;

const mat3 yiq_mat = mat3(
  0.2989, 0.5870, 0.1140,
  0.5959, -0.2744, -0.3216,
  0.2115, -0.5229, 0.3114
);

vec3 rgb2yiq(vec3 col) {
  return col * yiq_mat;
}

vec4 hs_main() {
  vec3 col = texture(u_source, v_texCoord).rgb;

  vec3 yiq = rgb2yiq(col);
  yiq.x = pow(yiq.x, Gamma);
  float lum = yiq.x;

  float field, line;

  if (u_originalSize.y > 360.0) {
    // Switch field and restart line count for the bottom half when interlaced
    if (v_texCoord.y > 0.5) {
      field = u_phase == 1 ? 0.0 : 1.0;
      line = pix_no.y - u_originalSize.y * 0.5;
    } else {
      field = float(u_phase);
      line = pix_no.y;
    }
  } else {
    field = float(u_phase);
    line = pix_no.y;
  }

  float chroma_mod_freq, chroma_phase;

  if (Mode == MODE_ATARI) {
    chroma_mod_freq = PI / 4.0;
    chroma_phase = 0.0;
  } else if (Mode == MODE_SEGA_8BIT) {
    chroma_mod_freq = PI / 3.0;
    chroma_phase = PI / 2.0;
  } else if (Mode == MODE_MEGA_DRIVE) {
    if (u_originalSize.x > 288.0) {
      // 320px mode
      chroma_mod_freq = 4.0 * PI / 15.0;
      chroma_phase = 13.0 * PI / 15.0;
    } else {
      // 256px mode
      chroma_mod_freq = PI / 3.0;
      chroma_phase = 0.0;
    }
  } else if (Mode == MODE_SEGA_SATURN) {
    chroma_mod_freq = 4.0 * PI / 15.0;
    chroma_phase = PI * mod(line + field, 2.0);
  } else if (Mode == MODE_NES) {
    chroma_mod_freq = PI / 3.0;
    chroma_phase = 2.0 * PI / 3.0 * mod(line + field, 3.0);
  } else if (Mode == MODE_SUPER_NES) {
    chroma_mod_freq = PI / 3.0;
    chroma_phase = 2.0 * PI / 3.0 * mod(line, 3.0);
  } else { // PC Engine, Nintendo 64, PlayStation
    chroma_mod_freq = PI / 3.0;
    chroma_phase = PI * mod(line + field, 2.0);
  }

  float mod_phase = chroma_phase + pix_no.x * chroma_mod_freq;

  float i_mod = cos(mod_phase);
  float q_mod = sin(mod_phase);

  yiq.yz *= vec2(i_mod, q_mod); // Modulate.
  yiq *= mix_mat; // Cross-talk.
  yiq.yz *= vec2(i_mod, q_mod); // Demodulate.

  return vec4(yiq, lum);
}