// This file is part of Highscore. License: GPL-3.0-or-later

namespace PosixExt {
    [CCode (cname = "sem_t", cheader_filename = "semaphore.h", cprefix = "SEM_", lower_case_cprefix = "sem_", has_type_id = false, copy_function = "", free_function = "")]
    public struct Semaphore {
        public int init (int phared, uint value);
        public int destroy ();
        public int wait ();
        public int post ();
    }
}
