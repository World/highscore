// Epoxy is used from gl.vapi, and without this file Meson
// will complain about missing vapi, so provide one

namespace Epoxy {
    [CCode (cheader_filename = "epoxy/egl.h")]
    public bool has_egl_extension (EGL.EGLDisplay dpy, string extension);

    [CCode (cheader_filename = "epoxy/egl.h")]
    public int egl_version (EGL.EGLDisplay dpy);

    [CCode (cheader_filename = "epoxy/egl.h")]
    public bool has_egl ();

    [CCode (cheader_filename = "epoxy/gl.h")]
    public int gl_version ();
}
